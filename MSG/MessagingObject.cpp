// MessagingObject.cpp: implementation of the CMessagingObject class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>  //remove
#include "MessagingObject.h"
#include "Messager.h"
#include "../TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifndef NULL
#define NULL 0
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessagingObject::CMessagingObject()
{
//	m_lpfnDM = NULL;
	m_ucType = MSG_DESTTYPE_UNDEF;
	m_ucStatus = MSG_DESTSTATUS_UNINIT;
	m_pszDestName = NULL;
	m_pszParams = NULL;
	m_pmsgr = NULL;
	m_pszMsgDest = NULL; // just use defaults
	m_bEnableMessaging = true;
}

CMessagingObject::~CMessagingObject()
{
	if(m_pszDestName) free(m_pszDestName);
	if(m_pszParams) free(m_pszParams);
	if(m_pszMsgDest) free(m_pszMsgDest);
}

int  CMessagingObject::InitializeMessaging(void* pmsgr)
{
	if(pmsgr!=NULL)
	{
		m_pmsgr=pmsgr;  //sets the function address so the object can send back messages to the msg dispatcher
		return 0; // success
	}
	return -1; //failure
}
/*
int CMessagingObject::InitializeMessaging(LPFNDM lpfnDM)
{

	if(lpfnDM!=NULL)
	{
		m_lpfnDM=lpfnDM;  //sets the function address so the object can send back messages to the msg dispatcher
		return 0; // success
	}
	return -1; //failure
}
*/
void CMessagingObject::UninitializeMessaging()
{
	m_pmsgr=NULL;  // access to the function is controlled by the NULLification of this func address.
}

void CMessagingObject::Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((m_pmsgr)&&(m_bEnableMessaging))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)  //override
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}
		else
		if(m_pszMsgDest) // default
		{
			ulBufferLen  = strlen(m_pszMsgDest);
			pszEncodedDest = EncodeTen(m_pszMsgDest, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

/*
		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch = (char*)malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if((pszEncodedMsg)&&(strlen(pszEncodedMsg)))
			{
				ulBufferLen = strlen(pszEncodedMsg);
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				ulMaxBufferLen += ulBufferLen;
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if((pszEncodedCaller)&&(strlen(pszEncodedCaller)))
			{
				ulBufferLen = strlen(pszEncodedCaller);
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				ulMaxBufferLen += ulBufferLen;
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if((pszEncodedDest)&&(strlen(pszEncodedDest)))
			{
				ulBufferLen = strlen(pszEncodedDest);
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				ulMaxBufferLen += ulBufferLen;
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			ulFlags|=MSG_SIGNAL_ENCODED;
*/

		// had to do the following to deal with double decoding... argh
		((CMessager*)m_pmsgr)->DM(ulFlags, pszDestinations, pszCaller, pszMessage);
//		((CMessager*)m_pmsgr)->DM(ulFlags, pszEncodedDest, pszEncodedCaller, pszEncodedMsg);

		if(pszEncodedDest)		free(pszEncodedDest);
		if(pszEncodedCaller)	free(pszEncodedCaller);
		if(pszEncodedMsg)			free(pszEncodedMsg);
//		((CMessager*)m_pmsgr)->DM(ulFlags, NULL, NULL, pch);
//			free(pch);
	//	}
	}
}

int CMessagingObject::HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	// this function MUST be overridden by objects to handle messages in their own particular way.
	// ALL strings passed in must be zero encoded, zero terminated.
	return 0;
}

int CMessagingObject::Modify(char* pchParams, unsigned long* pulBufferLen)
{
	// this function MUST be overridden by objects to make modifications in their own particular way.
	return 0;
}

char*  CMessagingObject::EncodeZero(char* pchInputBuffer, unsigned long* pulBufferLen, unsigned long ulFlags)
{
	if((pchInputBuffer==NULL)||(pulBufferLen==NULL)) { return NULL; }

	CBufferUtil bu;
	unsigned long ulNewBufferLen = (*pulBufferLen) + bu.CountChar(pchInputBuffer, *pulBufferLen, 0);
	ulNewBufferLen += bu.CountChar(pchInputBuffer, *pulBufferLen, ZERO_ENCODE_CTRLCHAR); // control char

	if(ulFlags&ZERO_ENCODE_USEZEROTERM) ulNewBufferLen; 
	
	char* pch = (char*)calloc(ulNewBufferLen+1, sizeof(char));//add one for terminating zero

//char foo[256]; sprintf(foo,"ZERO: %ld, %ld", pch, pchInputBuffer); AfxMessageBox(foo);

	if(pch!=NULL)
	{
		unsigned long j=0;
		for(unsigned long i=0; i<*pulBufferLen; i++)
		{
			switch(*(pchInputBuffer+i))
			{
			case 0:											{	*(pch+j) = ZERO_ENCODE_CTRLCHAR; j++; *(pch+j) = ZERO_ENCODE_ZEROCHAR; } break;
			case ZERO_ENCODE_CTRLCHAR:	{ *(pch+j) = ZERO_ENCODE_CTRLCHAR; j++; *(pch+j) = ZERO_ENCODE_CTRLCHAR; } break;
			default:										{ *(pch+j) = *(pchInputBuffer+i); } break;
			}
			j++;
		}
		
		if(ulFlags&ZERO_ENCODE_USEZEROTERM) *(pch+j)=0; //terminating zero

		*pulBufferLen=ulNewBufferLen;
		return pch;
	}
	*pulBufferLen=0;
	return NULL;
}

char*  CMessagingObject::DecodeZero(char* pchInputBuffer, unsigned long* pulBufferLen)
{
	if((pchInputBuffer==NULL)||(pulBufferLen==NULL)) { return NULL; }

	// the decoded buffer will always be smaller, so we are safe allocating a larger buffer.
	char* pch = (char*) malloc(*pulBufferLen);
	if(pch!=NULL)
	{
		unsigned long j=0;
		for(unsigned long i=0; i<*pulBufferLen; i++)
		{
			switch(*(pchInputBuffer+i))
			{
			case ZERO_ENCODE_CTRLCHAR:
				{
					i++;
					switch(*(pchInputBuffer+i))
					{
						case ZERO_ENCODE_CTRLCHAR:	*(pch+j) = ZERO_ENCODE_CTRLCHAR; break;
						case ZERO_ENCODE_ZEROCHAR:	*(pch+j) = 0; break;
						default:  *(pch+j) = *(pchInputBuffer+i); break;
					}
				}	break;
			default:  *(pch+j) = *(pchInputBuffer+i); break;
			}
			j++;
		}
		*pulBufferLen=j; // but, we can pass the valid data buffer size out.
		return pch;
	}
	*pulBufferLen=0;
	return NULL;
}

//utility
char*  CMessagingObject::EncodeTen(char* pchInputBuffer, unsigned long* pulBufferLen)
{
	if((pchInputBuffer==NULL)||(pulBufferLen==NULL)) { return NULL; }

	CBufferUtil bu;
	unsigned long ulNewBufferLen = (*pulBufferLen) + bu.CountChar(pchInputBuffer, *pulBufferLen, 10); 
	ulNewBufferLen += bu.CountChar(pchInputBuffer, *pulBufferLen, TEN_ENCODE_CTRLCHAR); // control char

	char* pch = (char*)malloc(ulNewBufferLen+1);// give it a term zero
//	char foo[256]; sprintf(foo,"TEN: %ld, %ld", pch, pchInputBuffer); AfxMessageBox(foo);

	if(pch!=NULL)
	{
		unsigned long j=0;
		for(unsigned long i=0; i<*pulBufferLen; i++)
		{
			switch(*(pchInputBuffer+i))
			{
			case 10:									{	*(pch+j) = TEN_ENCODE_CTRLCHAR; j++; *(pch+j) = TEN_ENCODE_TENCHAR; } break;
			case TEN_ENCODE_CTRLCHAR:	{ *(pch+j) = TEN_ENCODE_CTRLCHAR; j++; *(pch+j) = TEN_ENCODE_CTRLCHAR; } break;
			default:									{ *(pch+j) = *(pchInputBuffer+i); } break;
			}
			j++;
		}
		*pulBufferLen=ulNewBufferLen;
		//memset(pch+ulNewBufferLen, 0 ,1); // give it a term zero
		*(pch+j)=0; //terminating zero
		return pch;
	}
	*pulBufferLen=0;
	return NULL;
}

char*  CMessagingObject::DecodeTen(char* pchInputBuffer, unsigned long* pulBufferLen, unsigned long ulFlags)
{
	if((pchInputBuffer==NULL)||(pulBufferLen==NULL)) { return NULL; }

	// the decoded buffer will always be smaller, so we are safe allocating a larger buffer.
	char* pch = (char*) malloc(*pulBufferLen+1);
	if(pch!=NULL)
	{
		unsigned long j=0;
		for(unsigned long i=0; i<*pulBufferLen; i++)
		{
			switch(*(pchInputBuffer+i))
			{
			case TEN_ENCODE_CTRLCHAR:
				{
					i++;
					switch(*(pchInputBuffer+i))
					{
					case TEN_ENCODE_CTRLCHAR:	*(pch+j) = TEN_ENCODE_CTRLCHAR; break;
					case TEN_ENCODE_TENCHAR:	if(ulFlags==TEN_DECODE_CRLF) { *(pch+j) = 13; j++; }  *(pch+j) = 10; break;
					default:  *(pch+j) = *(pchInputBuffer+i); break;
					}
				}	break;
			default:  *(pch+j) = *(pchInputBuffer+i); break;
			}
			j++;
		}
		*pulBufferLen=j; // but, we can pass the valid data buffer size out.
		memset(pch+j, 0 ,1); // and give it a term zero to be safe
		return pch;
	}
	*pulBufferLen=0;
	return NULL;
}
