#ifndef MSG_DEFINES_INCLUDED
#define MSG_DEFINES_INCLUDED

//typedef void (__cdecl* LPFNDM)(char*, unsigned long); //pointer to function, dispatch pre-formatted message.
#define MSG_ERRORSTRING_LEN	512

// Flag specifiers:

// icons, where appropriate
#define MSG_ICONNONE								0x00000000  // default, nothing
#define MSG_ICONQUESTION						0x00000001  // ? icon
#define MSG_ICONEXCLAMATION					0x00000002  // ! icon
#define MSG_ICONERROR								0x00000003  // X icon
#define MSG_ICONSTOP								0x00000004  // stop sign icon
#define MSG_ICONHAND								0x00000005  // hand icon
#define MSG_ICONINFO								0x00000006  // (i) icon 
#define MSG_ICONUSER1								0x00000007  
#define MSG_ICONUSER2								0x00000008  
#define MSG_ICONMASK								0x0000000f  // mask for this bit

// severity/priority
#define MSG_PRI_NORMAL              0x00000000  // default, just info
#define MSG_PRI_LOW                 0x00000010  // non-critical warnings
#define MSG_PRI_MEDIUM              0x00000020  // warning for something that may become critical if not corrected
#define MSG_PRI_HIGH                0x00000030  // immediate error, but not show-stopper
#define MSG_PRI_CRITICAL            0x00000040  // critical, causes exit
#define MSG_PRI_MASK							  0x000000f0  // priority mask

// text is 0x0000ff00 register
#define MSG_TEXTNORMAL							0x00000000  //default
#define MSG_TEXTBOLD								0x00000100  
#define MSG_TEXTITALIC							0x00000200  
#define MSG_TEXTUNDERLINE						0x00000400  
#define MSG_TEXTSTRIKEOUT						0x00000800  
#define MSG_TEXTJUSTIFY_DEFAULT			0x00000000  // default, depends on item
#define MSG_TEXTJUSTIFY_LEFT				0x00001000
#define MSG_TEXTJUSTIFY_CENTER			0x00002000
#define MSG_TEXTJUSTIFY_RIGHT				0x00004000

// messagebox
#define MSG_MB_ALWAYSONTOP					0x00008000

// signalled directives
#define MSG_SIGNAL_REGDEST_FILE			0x00010000
#define MSG_SIGNAL_REGDEST_UI				0x00020000
#define MSG_SIGNAL_REGDEST_EMAIL		0x00040000
#define MSG_SIGNAL_REGDEST_NET			0x00080000

#define MSG_SIGNAL_NODEFDEST				0x00100000 // suppress default destinations

// message is already encoded
#define MSG_SIGNAL_ENCODED					0x80000000
#define MSG_SIGNAL_POSTDELAY_50			0x01000000
#define MSG_SIGNAL_POSTDELAY_100		0x02000000
#define MSG_SIGNAL_POSTDELAY_200		0x03000000
#define MSG_SIGNAL_POSTDELAY_500		0x04000000
#define MSG_SIGNAL_POSTDELAY_1000		0x05000000
#define MSG_SIGNAL_POSTDELAY_2000		0x06000000
#define MSG_SIGNAL_POSTDELAY_5000		0x07000000
#define MSG_SIGNAL_POSTDELAY_10000	0x08000000
#define MSG_SIGNAL_POSTDELAY_MASK		0x0f000000

// destinations are in the register 0xfff00000
// enumerated destinations.

// destination types
#define MSG_DESTTYPE_UNDEF					0x00  // undefined
#define MSG_DESTTYPE_INI						0x01  // settings file
#define MSG_DESTTYPE_LOG						0x02  // log file
#define MSG_DESTTYPE_DB							0x03  // database handler
#define MSG_DESTTYPE_UI							0x04  // user interface handler
#define MSG_DESTTYPE_EMAIL					0x05  // email handler
#define MSG_DESTTYPE_NET						0x06  // network listener

#define MSG_DESTTYPE_MAXVAL					0x06  // this define may change in future.
#define MSG_DESTTYPE_DEFAULT				0x80  // ORable.

// destination types
#define MSG_DESTSTATUS_UNINIT				0x00  // uninitialized
#define MSG_DESTSTATUS_INIT					0x01  // initialized

#endif //MSG_DEFINES_INCLUDED