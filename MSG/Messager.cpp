// Messager.cpp: implementation of the CDest and CMessager classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>  //remove this.
#include <process.h>
#include "Messager.h"
#include "../TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "../TXT/LogUtil.h"
#include "../SMTP/SMTPUtil.h"
//#include "../TXT/FileUtil.h"
//#include "../MFC/ODBC/DBUtil.h"

//CMessager* g_pmsgr=NULL;

/*
//////////////////////////////////////////////////////////////////////
// globals for setup of messenger feedback call
//////////////////////////////////////////////////////////////////////
void DispatchEncodedMessage(char* pszMsg, unsigned long ulFlags) // feedback messages from message objects.
{
	if(g_pmsgr)
	{
		ulFlags|=MSG_SIGNAL_ENCODED;
		g_pmsgr->DM(ulFlags, NULL, NULL, pszMsg);
	}
}

void InitMessager(CMessager* pmsgr)
{
	g_pmsgr = pmsgr;
}

*/

// CMessager
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessager::CMessager()
{
	m_ppDest = NULL;
	m_ucNumDest=0;
	m_mobj.InitializeMessaging(this);
	m_bKillThread = false;
	m_bThreadStarted = false;
	m_bStackInUse = false;
//	m_bDispatchInUse = false;
	m_pszMessageStack = NULL;
	m_ulReqCount=0;
	m_ulSvcCount=0;
	InitializeCriticalSection(&m_crit);
	InitializeCriticalSection(&m_critDispatch);
	
	_beginthread( MessagerThread, 0, (void*)(this) );  // begin a separate thread to handle messages in the stack
}

CMessager::~CMessager()
{
	m_bKillThread = true;
	while(m_bThreadStarted) Sleep(1);  // wait for thread to finish out writing msgs

	DeleteCriticalSection(&m_crit);
	if(m_ppDest!=NULL)
	{
		unsigned char i=0;
		while(i<m_ucNumDest){ if(m_ppDest[i]) delete m_ppDest[i]; i++; } // have to explicitly free the mem
		delete [] m_ppDest;
	}
	DeleteCriticalSection(&m_critDispatch);
}


// Dispatch Message
void CMessager::DM(unsigned long ulFlags, char* pszDest, char* pszCaller, char* pszMsg, ...) // ... formatted
{
	EnterCriticalSection(&m_crit);
	_timeb timestamp;
	_ftime( &timestamp );

	CBufferUtil bu;
	if((pszDest)&&(bu.CountChar(pszDest, strlen(pszDest), 10)>0))
	{
		LeaveCriticalSection(&m_crit);
		return;// else out of spec, must be encoded
	}
	if((pszCaller)&&(bu.CountChar(pszCaller, strlen(pszCaller), 10)>0)) 	
	{
		LeaveCriticalSection(&m_crit);
		return;// else out of spec, must be encoded
	}

	bool bEncoded = false;
	unsigned long ulDelay = 0;
	
	if(ulFlags&MSG_SIGNAL_ENCODED) { bEncoded = true; ulFlags &= ~MSG_SIGNAL_ENCODED; }
	if(ulFlags&MSG_SIGNAL_POSTDELAY_MASK) { ulDelay = ulFlags; ulFlags &= ~MSG_SIGNAL_POSTDELAY_MASK; }

	char* pszEncodedMessage = NULL;
	unsigned long ulBufferLength = 0;

  char buffer[MAX_MESSAGE_LENGTH];
	if(!bEncoded)
	{
		va_list marker;
		// create the formatted output string
		va_start(marker, pszMsg); // Initialize variable arguments.

		try
		{
			_vsnprintf((char *) buffer, MAX_MESSAGE_LENGTH-1, pszMsg, (va_list) marker);
		}
		catch	(...)
		{
			LeaveCriticalSection(&m_crit);
			return;
		}
		va_end( marker );             // Reset variable arguments.

//		if(bu.CountChar(buffer, strlen(buffer), 10)>0) return;// else out of spec, must be encoded

		//we can encode it here and be forgiving...
		ulBufferLength = strlen(buffer);
		pszEncodedMessage = m_mobj.EncodeTen(buffer, &ulBufferLength);
		if(pszEncodedMessage==NULL)
		{
			LeaveCriticalSection(&m_crit);
			return; 
		}// no buffer allocated
	}
	unsigned long ulReqCount = m_ulReqCount; // the "ID" of this function call.
	m_ulReqCount++; // increment the request for the next possible call.

//	while((m_ulSvcCount<ulReqCount)||(m_bStackInUse))
//	{
//		// wait.
//		Sleep(1);
//	}

	m_bStackInUse = true;

	// we need to add and encode the flags, to make
	//[6 bytes timestamp + 4 bytes flags (+ enc if nec), char 10 and char 0 encoded][char 10][variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10]

	char* pch = (char*)malloc(32);// theoretical max is 20, if every byte needs encoding, plus 1 for zero term
	ulBufferLength = 0;
	if(pch)
	{
		*(pch+(ulBufferLength++))= (unsigned char)((timestamp.time>>24)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((timestamp.time>>16)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((timestamp.time>>8)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((timestamp.time)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((timestamp.millitm>>8)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((timestamp.millitm)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((ulFlags>>24)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((ulFlags>>16)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((ulFlags>>8)&(0xff));
		*(pch+(ulBufferLength++))= (unsigned char)((ulFlags)&(0xff));
		*(pch+(ulBufferLength))= 0;
//AfxMessageBox("X");

		char* pszEncodedNumerics = NULL;
		pszEncodedNumerics = m_mobj.EncodeTen(pch, &ulBufferLength);
		if(pszEncodedNumerics)
		{
			free(pch);
			pch = m_mobj.EncodeZero(pszEncodedNumerics, &ulBufferLength, ZERO_ENCODE_USEZEROTERM);
		}
		else
		{
			free(pch);
			pch = NULL;
		}

		if(pch)
		{
			if(pszEncodedNumerics) free(pszEncodedNumerics); 
			pszEncodedNumerics=NULL;
			if(m_pszMessageStack==NULL)
			{
				ulBufferLength = 0;
			}
			else
			{
				ulBufferLength = strlen(m_pszMessageStack);
			}
//AfxMessageBox("X");

			if(bEncoded)
				pszEncodedNumerics = (char*)malloc(ulBufferLength+((pszMsg==NULL)?0:strlen(pszMsg))+((pch==NULL)?0:strlen(pch))+2); // one extra delim and a term zero
			else
				pszEncodedNumerics = (char*)malloc(ulBufferLength
					+((pszEncodedMessage==NULL)?0:strlen(pszEncodedMessage))
					+((pszCaller==NULL)?0:strlen(pszCaller))
					+((pszDest==NULL)?0:strlen(pszDest))
					+((pch==NULL)?0:strlen(pch))
					+5);// four delim and a term zero.

			if(pszEncodedNumerics)
			{
				if(m_pszMessageStack!=NULL)
				{
					if(ulBufferLength) memcpy(pszEncodedNumerics, m_pszMessageStack, ulBufferLength);
					free(m_pszMessageStack);
				}
				m_pszMessageStack = pszEncodedNumerics;
				if((pch)&&(strlen(pch))){ memcpy(m_pszMessageStack+ulBufferLength, pch, strlen(pch)); ulBufferLength+=strlen(pch);}
				memset(m_pszMessageStack+ulBufferLength, 10, 1); ulBufferLength++;
				if(bEncoded)
				{
					if((pszMsg)&&(strlen(pszMsg))){ memcpy(m_pszMessageStack+ulBufferLength, pszMsg, strlen(pszMsg)); ulBufferLength+=strlen(pszMsg);}
				}
				else
				{
					if((pszEncodedMessage)&&(strlen(pszEncodedMessage))){ memcpy(m_pszMessageStack+ulBufferLength, pszEncodedMessage, strlen(pszEncodedMessage)); ulBufferLength+=strlen(pszEncodedMessage); }
					memset(m_pszMessageStack+ulBufferLength, 10, 1); ulBufferLength++;
					if((pszCaller)&&(strlen(pszCaller))){ memcpy(m_pszMessageStack+ulBufferLength, pszCaller, strlen(pszCaller)); ulBufferLength+=strlen(pszCaller); }
					memset(m_pszMessageStack+ulBufferLength, 10, 1); ulBufferLength++;
					if((pszDest)&&(strlen(pszDest))){ memcpy(m_pszMessageStack+ulBufferLength, pszDest, strlen(pszDest)); ulBufferLength+=strlen(pszDest); }
					memset(m_pszMessageStack+ulBufferLength, 10, 1); ulBufferLength++;
				}
				memset(m_pszMessageStack+ulBufferLength, 0, 1); // terminate the whole buffer.
//				AfxMessageBox(m_pszMessageStack);
			}
			free(pch);
		}
	}
	if(pszEncodedMessage) free(pszEncodedMessage);

	m_bStackInUse = false; // release buffer access
	m_ulSvcCount++;
	if(m_ulSvcCount == m_ulReqCount) // all requests serviced, can reset counter
		m_ulSvcCount = m_ulReqCount = 0;
	LeaveCriticalSection(&m_crit);

	//add a delay at end if specified
	if(ulDelay&MSG_SIGNAL_POSTDELAY_MASK)
	{
		if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_50) Sleep(50);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_100) Sleep(100);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_200) Sleep(200);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_500) Sleep(500);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_1000) Sleep(1000);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_2000) Sleep(2000);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_5000) Sleep(5000);
		else if((ulDelay&MSG_SIGNAL_POSTDELAY_MASK)==MSG_SIGNAL_POSTDELAY_10000) Sleep(10000);
	}
}

// core.  
int CMessager::AddDestination(unsigned char ucType, char* pszDestName, char* pszParams, char* pszInfo)
{
	if( ((ucType&0x0f)==MSG_DESTTYPE_UNDEF)
		||((ucType&0x0f)>MSG_DESTTYPE_MAXVAL)
		||(pszDestName==NULL) 
		||(strlen(pszDestName)<=0) )
		return MSG_ERROR_BADARG;

	EnterCriticalSection(&m_critDispatch);
//	while(m_bDispatchInUse) Sleep(1);
//	m_bDispatchInUse = true;

	if(GetDestIndex(pszDestName)>=0)
	{
//		m_bDispatchInUse = false;
		LeaveCriticalSection(&m_critDispatch);
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: could not add new object, object [%s] already exists.", pszDestName);
		return MSG_ERROR_EXISTS;
	}

	// have to check params for conflict
	if(ParamCheck(ucType, pszParams)<MSG_SUCCESS)
	{
//		m_bDispatchInUse = false;
		LeaveCriticalSection(&m_critDispatch);
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: could not add new object, parameters conflict with an existing object.");
		return MSG_ERROR_EXISTS;
	}

	// allocate space for the new destination.
	CMessagingObject** ppDest = new CMessagingObject*[m_ucNumDest+1];  // array of destinations

	if(ppDest==NULL)
	{
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: out of memory - could not add new object.");
		m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: out of memory - could not add new object.", "Messager:AddDestination");
//		m_bDispatchInUse = false;
		LeaveCriticalSection(&m_critDispatch);
		return MSG_ERROR_MEMEX;
	}

	if(m_ppDest)
	{
		unsigned char ucNumDest=0;
		while(ucNumDest<m_ucNumDest)
		{
			ppDest[ucNumDest]=m_ppDest[ucNumDest];
			ucNumDest++;
		}
		//memcpy(ppDest, m_ppDest, sizeof(CMessagingObject*)*m_ucNumDest);
		delete [] m_ppDest;
	}

	m_ppDest = ppDest;

	// do specific type checks
	switch(ucType&0x0f)
	{
	case MSG_DESTTYPE_LOG://						0x02  // log file
		{
			if((pszParams==NULL)||(strlen(pszParams)<=0))
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: bad params for file object.");
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: bad params for file object.", "Messager:AddDestination");
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);
				return MSG_ERROR_BADARG; // we need at least the filename
			}
			m_ppDest[m_ucNumDest] = (CMessagingObject*) new CLogUtil;
			if(m_ppDest[m_ucNumDest])
			{
				// for logfiles, we need params, and they must be in this format:
				//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
			}
			else
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: out of memory - could not add log handler.");
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: out of memory - could not add log handler.", "Messager:AddDestination");
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);
				return MSG_ERROR_MEMEX;
			}

		} break; 

 // for the time being, only Log files have been coded to Handle messages.  OK NOW, email too.
	case MSG_DESTTYPE_EMAIL://					0x05  // email handler
		{
			if((pszParams==NULL)||(strlen(pszParams)<=0))
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: bad params for smtp object.");
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: bad params for smtp object.", "Messager:AddDestination");
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);
				return MSG_ERROR_BADARG; // we need at least the filename
			}
			m_ppDest[m_ucNumDest] = (CMessagingObject*) new CSMTPUtil;
			if(m_ppDest[m_ucNumDest])
			{
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, x means none, and f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// again, x means none, and f means all
				// xf means ignore severity but send all types to the address list.
				// fx means ignore type but send all severity codes to the address list.
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.
			}
			else
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: out of memory - could not add smtp handler.");
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: out of memory - could not add smtp handler.", "Messager:AddDestination");
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);
				return MSG_ERROR_MEMEX;
			}
		} break;

	case MSG_DESTTYPE_DB://							0x03  // database handler
	case MSG_DESTTYPE_UI://							0x04  // user interface handler
	case MSG_DESTTYPE_NET://						0x06  // network listener
	case MSG_DESTTYPE_UNDEF://					0x00  // undefined
	case MSG_DESTTYPE_INI://						0x01  // settings file
	default: break;
	}


	m_ppDest[m_ucNumDest]->m_pszDestName = (char*)malloc(strlen(pszDestName)+1);
	if(m_ppDest[m_ucNumDest]->m_pszDestName==NULL)
	{
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: out of memory - could not add new object name.");
		m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: out of memory - could not add new object name.", "Messager:AddDestination");
		if(m_ppDest[m_ucNumDest]) delete m_ppDest[m_ucNumDest];
//		m_bDispatchInUse = false;
		LeaveCriticalSection(&m_critDispatch);
		return MSG_ERROR_MEMEX;
	}

	strcpy(m_ppDest[m_ucNumDest]->m_pszDestName, pszDestName);

	if(pszParams!=NULL)
	{
		m_ppDest[m_ucNumDest]->m_pszParams = (char*)malloc(strlen(pszParams)+1);
		if(m_ppDest[m_ucNumDest]->m_pszParams==NULL)
		{
			if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: out of memory - could not add new object params.");
			m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "AddDestination: out of memory - could not add new object params.", "Messager:AddDestination");
			if(m_ppDest[m_ucNumDest]) delete m_ppDest[m_ucNumDest];
			LeaveCriticalSection(&m_critDispatch);
//			m_bDispatchInUse = false;
			return MSG_ERROR_MEMEX;
		}

		strcpy(m_ppDest[m_ucNumDest]->m_pszParams, pszParams);
	}
	else m_ppDest[m_ucNumDest]->m_pszParams = NULL;

	m_ppDest[m_ucNumDest]->m_ucType = ucType;

	// now, deal with initializing the new object, type - specific
	switch(ucType&0x0f)
	{
	case MSG_DESTTYPE_LOG://						0x02  // log file
		{
			// for logfiles, we need params, and they must be in this format:
			//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs

			int nRV = ((CLogUtil*)m_ppDest[m_ucNumDest])->OpenLog(m_ppDest[m_ucNumDest]->m_pszParams);
			if(nRV!=LOG_SUCCESS)
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: Log file could not be opened with [%s] (%d).", m_ppDest[m_ucNumDest]->m_pszParams, nRV );
				char errorstring[MSG_ERRORSTRING_LEN];
				_snprintf(errorstring, MSG_ERRORSTRING_LEN-1, "AddDestination: Log file could not be opened with [%s] (%d).", m_ppDest[m_ucNumDest]->m_pszParams, nRV );
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "Messager:AddDestination");
				if(m_ppDest[m_ucNumDest]) delete m_ppDest[m_ucNumDest];
				m_ppDest[m_ucNumDest]=NULL;
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);

				return MSG_ERROR;
			}
		} break; 

 // for the time being, only Log files have been coded to Handle messages.  now email too
	case MSG_DESTTYPE_EMAIL://					0x05  // email handler
		{
			int nRV = ((CSMTPUtil*)m_ppDest[m_ucNumDest])->InitializeThread(m_ppDest[m_ucNumDest]->m_pszParams);
			if(nRV!=LOG_SUCCESS)
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "AddDestination: SMTP object could not be initialized with [%s] (%d).", m_ppDest[m_ucNumDest]->m_pszParams, nRV );
				char errorstring[MSG_ERRORSTRING_LEN];
				_snprintf(errorstring, MSG_ERRORSTRING_LEN-1, "AddDestination: SMTP object could not be initialized with [%s] (%d).", m_ppDest[m_ucNumDest]->m_pszParams, nRV );
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "Messager:AddDestination");
				if(m_ppDest[m_ucNumDest]) delete m_ppDest[m_ucNumDest];
				m_ppDest[m_ucNumDest]=NULL;
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);

				return MSG_ERROR;
			}
		} break;
	case MSG_DESTTYPE_DB://							0x03  // database handler
	case MSG_DESTTYPE_UI://							0x04  // user interface handler
	case MSG_DESTTYPE_NET://						0x06  // network listener
	case MSG_DESTTYPE_UNDEF://					0x00  // undefined
	case MSG_DESTTYPE_INI://						0x01  // settings file
	default: break;
	}

	// if we made it here, we are set.
	m_ppDest[m_ucNumDest]->m_ucStatus = MSG_DESTSTATUS_INIT;
	m_ppDest[m_ucNumDest]->InitializeMessaging(this);

	m_ucNumDest++;

	LeaveCriticalSection(&m_critDispatch);
//	m_bDispatchInUse = false;
	return MSG_SUCCESS;
}

int CMessager::RemoveDestination(char* pszDestName, char* pszInfo)
{
	if( (pszDestName==NULL) 
		||(strlen(pszDestName)<=0) )
		return MSG_ERROR_BADARG;

//	while(m_bDispatchInUse) Sleep(1);
//	m_bDispatchInUse = true;
	EnterCriticalSection(&m_critDispatch);

	int nIndex = GetDestIndex(pszDestName);
	if(nIndex<0)
	{
		LeaveCriticalSection(&m_critDispatch);
//		m_bDispatchInUse = false;
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "RemoveDestination: could not remove object, object [%s] does not exist.", pszDestName);
		return MSG_ERROR_NOTEXISTS;
	}

	// now, deal with uninitializing the object, type - specific
	switch((m_ppDest[nIndex]->m_ucType)&0x0f)
	{
	case MSG_DESTTYPE_LOG://						0x02  // log file
		{
			//((CLogUtil*)m_ppDest[nIndex])->CloseLog(); // dont need this, the deconstructor calls it.
		} break; 

 // for the time being, only Log files have been coded to Handle messages. - now email too
	case MSG_DESTTYPE_EMAIL://					0x05  // email handler
		{
			//((CSMTPUtil*)m_ppDest[nIndex])->KillThread(); // dont need this, the deconstructor calls it.
		} break; 
	case MSG_DESTTYPE_DB://							0x03  // database handler
	case MSG_DESTTYPE_UI://							0x04  // user interface handler
	case MSG_DESTTYPE_NET://						0x06  // network listener
	case MSG_DESTTYPE_UNDEF://					0x00  // undefined
	case MSG_DESTTYPE_INI://						0x01  // settings file
	default: break;
	}

	delete m_ppDest[nIndex]; // get rid of the actual object.
	m_ppDest[nIndex] = NULL;
	m_ucNumDest--;

	//remove it from the pointer array
	if(m_ucNumDest<=0)
	{
		if(m_ppDest) delete [] m_ppDest;
		m_ppDest = NULL;
	}
	else
	{
		// there are some destinations left
		while(nIndex<m_ucNumDest)
		{
			m_ppDest[nIndex]=m_ppDest[nIndex+1];
			nIndex++;
			//memcpy(m_ppDest, &m_ppDest[nIndex+1], sizeof(CMessagingObject*)*(m_ucNumDest-nIndex));
		}

		// allocate space for the smaller set of destinations.
		CMessagingObject** ppDest = new CMessagingObject*[m_ucNumDest];  // array of destinations

		if(ppDest!=NULL)
		{
			nIndex=0;
			while(nIndex<m_ucNumDest)
			{
				ppDest[nIndex]=m_ppDest[nIndex];
				nIndex++;
			}
//			memcpy(ppDest, m_ppDest, sizeof(CMessagingObject*)*m_ucNumDest);
			delete [] m_ppDest;
			m_ppDest = ppDest;
		} //else 		// its OK, its already removed from the master list 
	}

//	m_bDispatchInUse = false;
	LeaveCriticalSection(&m_critDispatch);

	return MSG_SUCCESS;
}

int CMessager::ModifyDestination(char* pszDestName, char* pszParams, unsigned char ucType, char* pszInfo)
{
	if( (pszDestName==NULL) 
		||(strlen(pszDestName)<=0) )
		return MSG_ERROR_BADARG;

	EnterCriticalSection(&m_critDispatch);

//	while(m_bDispatchInUse) Sleep(1);
//	m_bDispatchInUse = true;

	int nIndex = GetDestIndex(pszDestName);
	if(nIndex<0)
	{
		LeaveCriticalSection(&m_critDispatch);
//		m_bDispatchInUse = false;
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "ModifyDestination: could not modify object, object [%s] does not exist.", pszDestName);
		return MSG_ERROR_NOTEXISTS;
	}

	// have to check params for conflict
	if(ParamCheck(ucType, pszParams)<MSG_SUCCESS)
	{
		LeaveCriticalSection(&m_critDispatch);
//		m_bDispatchInUse = false;
		if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "ModifyDestination: could not modify object, parameters conflict with an existing object.");
		return MSG_ERROR_EXISTS;
	}

	// now, deal with uninitializing the object, type - specific
	switch((m_ppDest[nIndex]->m_ucType)&0x0f)
	{
	case MSG_DESTTYPE_LOG://						0x02  // log file
		{
			if((pszParams==NULL)||(strlen(pszParams)<=0))
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "ModifyDestination: bad params for file object.");
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "ModifyDestination: bad params for file object.", "Messager:ModifyDestination");
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);
				return MSG_ERROR_BADARG; // we need at least the filename
			}
			((CLogUtil*)m_ppDest[nIndex])->CloseLog();
		} break; 

 // for the time being, only Log files have been coded to Handle messages.
	case MSG_DESTTYPE_EMAIL://					0x05  // email handler
		{
			if((pszParams==NULL)||(strlen(pszParams)<=0))
			{
				if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "ModifyDestination: bad params for smtp object.");
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "ModifyDestination: bad params for smtp object.", "Messager:ModifyDestination");
//				m_bDispatchInUse = false;
				LeaveCriticalSection(&m_critDispatch);
				return MSG_ERROR_BADARG; // we need at least the filename
			}
			((CSMTPUtil*)m_ppDest[nIndex])->KillThread();
		} break;

	case MSG_DESTTYPE_DB://							0x03  // database handler
	case MSG_DESTTYPE_UI://							0x04  // user interface handler
	case MSG_DESTTYPE_NET://						0x06  // network listener
	case MSG_DESTTYPE_UNDEF://					0x00  // undefined
	case MSG_DESTTYPE_INI://						0x01  // settings file
	default: break;
	}

	
	//m_ppDest[m_ucNumDest]->m_ucStatus = MSG_DESTSTATUS_UNINIT; this index was probably just wrong.
	m_ppDest[nIndex]->m_ucStatus = MSG_DESTSTATUS_UNINIT;
	char* pszOldParams = 	m_ppDest[nIndex]->m_pszParams;

	//now, reinitialize with the new params
	int nRV = MSG_SUCCESS;
	bool bReInitOld = false;
	if(pszParams!=NULL)
	{
		m_ppDest[nIndex]->m_pszParams = (char*)malloc(strlen(pszParams)+1);
		if(m_ppDest[nIndex]->m_pszParams==NULL)
		{
			if(pszInfo) _snprintf(pszInfo, MSG_ERRORSTRING_LEN-1, "ModifyDestination: out of memory - could not modify object params.");
			m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, "ModifyDestination: out of memory - could not modify object params.", "Messager:ModifyDestination");
			bReInitOld = true;
			nRV =  MSG_ERROR_MEMEX;
		}

		strcpy(m_ppDest[nIndex]->m_pszParams, pszParams);
	}
	else m_ppDest[nIndex]->m_pszParams = NULL;

	if(ucType!=MSG_DESTTYPE_UNDEF)
	{
		// modify does not change type, only default status
		if(ucType&MSG_DESTTYPE_DEFAULT) 
			m_ppDest[nIndex]->m_ucType |= MSG_DESTTYPE_DEFAULT;  // add def flag
		else
			m_ppDest[nIndex]->m_ucType &= ~MSG_DESTTYPE_DEFAULT; // remove def flag
	}

	// now, deal with re-initializing the object, type - specific
	switch(ucType&0x0f)
	{
	case MSG_DESTTYPE_LOG://						0x02  // log file
		{
			// for logfiles, we need params, and they must be in this format:
			// File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
			if(bReInitOld)
			{	
				if(m_ppDest[nIndex]->m_pszParams) free(m_ppDest[nIndex]->m_pszParams); // get rid of new params
				m_ppDest[nIndex]->m_pszParams = pszOldParams; // back to old
			}

			nRV = ((CLogUtil*)m_ppDest[nIndex])->OpenLog(m_ppDest[nIndex]->m_pszParams);
			if(nRV!=LOG_SUCCESS)
			{
				if(pszInfo)
				{ 
					if(bReInitOld) strcat(pszInfo, "\n");
					_snprintf(pszInfo, MSG_ERRORSTRING_LEN-1-strlen(pszInfo), "ModifyDestination: Log file could not be opened with [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
				}
				char errorstring[MSG_ERRORSTRING_LEN];
				_snprintf(errorstring, MSG_ERRORSTRING_LEN-1, "ModifyDestination: Log file could not be opened with [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "Messager:ModifyDestination");
				if(!bReInitOld) // we had an error with the new settings, try again with old.
				{
					if(m_ppDest[nIndex]->m_pszParams) free(m_ppDest[nIndex]->m_pszParams); // get rid of new params
					m_ppDest[nIndex]->m_pszParams = pszOldParams; // back to old

					nRV = ((CLogUtil*)m_ppDest[nIndex])->OpenLog(m_ppDest[nIndex]->m_pszParams);
					if(nRV!=LOG_SUCCESS)
					{
						if(pszInfo)
						{
							strcat(pszInfo, "\n");
							_snprintf(pszInfo, MSG_ERRORSTRING_LEN-1-strlen(pszInfo), "ModifyDestination: Log file could not be re-opened with old params [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
						}
						char errorstring[MSG_ERRORSTRING_LEN];
						_snprintf(errorstring, MSG_ERRORSTRING_LEN-1, "ModifyDestination: Log file could not be re-opened with old params [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
						m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "Messager:ModifyDestination");
					}
					else
					{
						m_ppDest[nIndex]->m_ucStatus = MSG_DESTSTATUS_INIT;  // init with old settings.
					}
				}
				nRV = MSG_ERROR; // it's an error in any case
			}
			else
			{
				m_ppDest[nIndex]->m_ucStatus = MSG_DESTSTATUS_INIT;
			}
		} break; 

 // for the time being, only Log files have been coded to Handle messages. now email too
	case MSG_DESTTYPE_EMAIL://					0x05  // email handler
		{
			if(bReInitOld)
			{	
				if(m_ppDest[nIndex]->m_pszParams) free(m_ppDest[nIndex]->m_pszParams); // get rid of new params
				m_ppDest[nIndex]->m_pszParams = pszOldParams; // back to old
			}

			nRV = ((CSMTPUtil*)m_ppDest[nIndex])->InitializeThread(m_ppDest[nIndex]->m_pszParams);
			if(nRV!=SMTP_SUCCESS)
			{
				if(pszInfo)
				{ 
					if(bReInitOld) strcat(pszInfo, "\n");
					_snprintf(pszInfo, MSG_ERRORSTRING_LEN-1-strlen(pszInfo), "ModifyDestination: SMTP object could not be initialized with [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
				}
				char errorstring[MSG_ERRORSTRING_LEN];
				_snprintf(errorstring, MSG_ERRORSTRING_LEN-1, "ModifyDestination: SMTP object could not be initialized with [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
				m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "Messager:ModifyDestination");
				if(!bReInitOld) // we had an error with the new settings, try again with old.
				{
					if(m_ppDest[nIndex]->m_pszParams) free(m_ppDest[nIndex]->m_pszParams); // get rid of new params
					m_ppDest[nIndex]->m_pszParams = pszOldParams; // back to old

					nRV = ((CSMTPUtil*)m_ppDest[nIndex])->InitializeThread(m_ppDest[nIndex]->m_pszParams);
					if(nRV!=SMTP_SUCCESS)
					{
						if(pszInfo)
						{
							strcat(pszInfo, "\n");
							_snprintf(pszInfo, MSG_ERRORSTRING_LEN-1-strlen(pszInfo), "ModifyDestination: SMTP object could not be re-initialized with old params [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
						}
						char errorstring[MSG_ERRORSTRING_LEN];
						_snprintf(errorstring, MSG_ERRORSTRING_LEN-1, "ModifyDestination: SMTP object could not be re-initialized with old params [%s] (%d).", m_ppDest[nIndex]->m_pszParams, nRV );
						m_mobj.Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "Messager:ModifyDestination");
					}
					else
					{
						m_ppDest[nIndex]->m_ucStatus = MSG_DESTSTATUS_INIT;  // init with old settings.
					}
				}
				nRV = MSG_ERROR; // it's an error in any case
			}
			else
			{
				m_ppDest[nIndex]->m_ucStatus = MSG_DESTSTATUS_INIT;
			}
		} break;
	case MSG_DESTTYPE_DB://							0x03  // database handler
	case MSG_DESTTYPE_UI://							0x04  // user interface handler
	case MSG_DESTTYPE_NET://						0x06  // network listener
	case MSG_DESTTYPE_UNDEF://					0x00  // undefined
	case MSG_DESTTYPE_INI://						0x01  // settings file
	default: break;
	}

	LeaveCriticalSection(&m_critDispatch);
//	m_bDispatchInUse = false;
	return nRV;
}

int CMessager::GetDestIndex(char* pszDestName)
{
	int nRV = MSG_ERROR; //i.e. not found

	if((pszDestName==NULL)||(strlen(pszDestName)<=0)) return MSG_ERROR_BADARG;

	if((m_ppDest!=NULL)&&(m_ucNumDest>0))
	{
		unsigned char ucIndex = 0;
		while(ucIndex<m_ucNumDest)
		{
			if((m_ppDest[ucIndex])&&(m_ppDest[ucIndex]->m_pszDestName)&&(strlen(m_ppDest[ucIndex]->m_pszDestName)>0))
			{
				if(strcmp(m_ppDest[ucIndex]->m_pszDestName, pszDestName)==0) return ucIndex;
			}
			ucIndex++;
		}
	}

	return nRV;
}

int CMessager::ParamCheck(unsigned char ucType, char* pszParams)
{
	int nRV = MSG_SUCCESS; //i.e. no conflict

	// all this stuff really should be in the code for each module itself but hey, no time to do it right.
	// it just works so that is OK.

	if((m_ppDest)&&(m_ucNumDest>0))
	{
		switch(ucType&0x0f)
		{
		case MSG_DESTTYPE_LOG://						0x02  // log file
			{
				if(pszParams==NULL) return MSG_ERROR; // cant compare, and anyway not valid.
				// have to check any other registered params for logs in case theres a conflict;
				unsigned char i=0;
				while(i<m_ucNumDest)
				{
				// we have to make sure that the filename isnt going to be the same as another
				// for logfiles, we need params, and they must be in this format:
				//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
					if((m_ppDest[i])&&(m_ppDest[i]->m_pszParams)&&(((m_ppDest[i]->m_ucType)&0x0f)==MSG_DESTTYPE_LOG))
					{
						CSafeBufferUtil sbu, sbuIn;
//AfxMessageBox(m_ppDest[i]->m_pszParams);
//AfxMessageBox(pszParams);
						char* pch = sbu.Token(m_ppDest[i]->m_pszParams, strlen(m_ppDest[i]->m_pszParams), "|", MODE_SINGLEDELIM);
						char* pchIn = sbuIn.Token(pszParams, strlen(pszParams), "|", MODE_SINGLEDELIM);
//AfxMessageBox(pch);
//AfxMessageBox(pchIn);
						if((pch)&&(pchIn))
						{
							char* pchRS = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
							char* pchRSIn = sbuIn.Token(NULL, NULL, "|", MODE_SINGLEDELIM);

							if((pchRS)&&(pchRSIn))
							{
								if(strcmp(pchRS, pchRSIn)==0)  // rotate specs are the same
								{
									if(strcmp(pch, pchIn)==0)  // filenames are the same
									{
										// have to check overrride.
										char* pchCFS = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										char* pchCFSIn = sbuIn.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchCFS)&&(pchCFSIn))
										{
											if(strcmp(pchCFS, pchCFSIn)==0)  // filenames are the same
												return MSG_ERROR; 
										}
										else
										{
											if((pchCFS==NULL)&&(pchCFSIn==NULL))
												return MSG_ERROR;  // no override to save us
											// else one of them exists, so we can proceed
										}
									}
								}  // else the filenames will be different, so OK.
							}
							else  // defaults are being used
							{
								if((pchRS)||(pchRSIn))
								{
									if(pchRS)
									{
										if(strncmp(pchRS, "Y", 1)==0)
										{
											if(strlen(pchRS)>1)
											{
												if(strncmp(pchRS, "YM", 2)==0) return MSG_ERROR; //it's the same (YM is default)
											}
											else return MSG_ERROR; //it's the same (YM is default)
										}
									}
									else
									{
										if(strncmp(pchRSIn, "Y", 1)==0)
										{
											if(strlen(pchRSIn)>1)
											{
												if(strncmp(pchRSIn, "YM", 2)==0) return MSG_ERROR; //it's the same (YM is default)
											}
											else return MSG_ERROR; //it's the same (YM is default)
										}
									}
								}
							}
						}
						else return MSG_ERROR; // cant compare, and anyway not valid.
					}
					i++;
				}
			} break;
		case MSG_DESTTYPE_EMAIL://					0x05  // email handler
			{
				if(pszParams==NULL) return MSG_ERROR; // cant compare, and anyway not valid.
				// have to check any other registered params for logs in case theres a conflict;
				unsigned char i=0;
				while(i<m_ucNumDest)
				{
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
					if((m_ppDest[i])&&(m_ppDest[i]->m_pszParams)&&(((m_ppDest[i]->m_ucType)&0x0f)==MSG_DESTTYPE_EMAIL))
					{
						CSafeBufferUtil sbuIn;
//AfxMessageBox(m_ppDest[i]->m_pszParams);
//AfxMessageBox(pszParams);
						char* pchIn = sbuIn.Token(pszParams, strlen(pszParams), "|", MODE_SINGLEDELIM);
//AfxMessageBox(pch);
//AfxMessageBox(pchIn);
						if((pchIn)&&(strlen(pchIn)))
						{
//							char* pchRS = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
							char* pchRSIn = sbuIn.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
//AfxMessageBox(pchRSIn);

							if((pchRSIn)&&(strlen(pchRSIn)))
							{
								//success

							}	else return MSG_ERROR; // not valid distribution list.
					
						}
						else return MSG_ERROR; // not valid server name.
					}
					i++;
				}
			} break;
		case MSG_DESTTYPE_DB://							0x03  // database handler
		case MSG_DESTTYPE_UI://							0x04  // user interface handler
		case MSG_DESTTYPE_NET://						0x06  // network listener
		case MSG_DESTTYPE_UNDEF://					0x00  // undefined
		case MSG_DESTTYPE_INI://						0x01  // settings file
		default: break;
		}
	}

	return nRV;
}


void MessagerThread(void* pvArgs)
{	
	CMessager* pmsgr = (CMessager*)pvArgs;
	if(pmsgr)
	{
		pmsgr->m_bThreadStarted = true;
		CMessagingObject msg;  // we create a new messaging object to use the decode functions in this thread, 
		// so there is no overlap with any calls of the pmsgr object (just in case)

//AfxMessageBox("T");
		
		while (!pmsgr->m_bKillThread)
		{
			// check buffer for new messages, and ability to write them.
			if((!pmsgr->m_bStackInUse)&&(pmsgr->m_pszMessageStack!=NULL))
			{
				pmsgr->m_bStackInUse=true;
				EnterCriticalSection(&pmsgr->m_crit);

				char* pszMessageStack = (char*)malloc(strlen(pmsgr->m_pszMessageStack)+1);
				if(pszMessageStack)
				{ 
					strcpy(pszMessageStack, pmsgr->m_pszMessageStack); // make a direct copy, manip this one on our own time.
					free(pmsgr->m_pszMessageStack);  // clear it out
					pmsgr->m_pszMessageStack = NULL;
				}
				LeaveCriticalSection(&pmsgr->m_crit);
				pmsgr->m_bStackInUse=false;

				if(pszMessageStack)  // means we have a buffer to parse!
				{ 
					char* pszStack = pszMessageStack;  // have to preserve the original pointer to free at end
//AfxMessageBox(pszMessageStack);
					// parse it.
					// (this is the only thread that may call the HandleMessage functions)
					while(strlen(pszMessageStack))  // traverse the stack.
					{
						char* pchMarker = pszMessageStack;
						char* pchDelim = NULL;
						char* pszNumerics = NULL;
						char* pszMsg = NULL;
						char* pszCaller = NULL;
						char* pszDest = NULL;
						unsigned long ulBufferLen;
						unsigned long ulFlags = 0;

						pchDelim = strchr(pchMarker, 10);
						ulBufferLen = pchDelim - pchMarker;
						pszMsg = msg.DecodeZero(pchMarker, &ulBufferLen);
						if(pszMsg)
						{
							pszNumerics = msg.DecodeTen(pszMsg, &ulBufferLen);
							free(pszMsg);
						}
						pchMarker = pchDelim+1;

						_timeb timestamp;
						if(pszNumerics)
						{
							char* pch = pszNumerics;
							timestamp.time  = (0xff000000&((*pch&0xff)<<24)); pch++;
							timestamp.time |= (0x00ff0000&((*pch&0xff)<<16)); pch++;
							timestamp.time |= (0x0000ff00&((*pch&0xff)<<8));  pch++;
							timestamp.time |= (0x000000ff&(*pch&0xff));				pch++;

							timestamp.millitm  = (0xff00&((*pch&0xff)<<8)); pch++;
							timestamp.millitm |= (0x00ff&(*pch&0xff));			pch++;

							ulFlags  = (0xff000000&((*pch&0xff)<<24));	pch++;
							ulFlags |= (0x00ff0000&((*pch&0xff)<<16));	pch++;
							ulFlags |= (0x0000ff00&((*pch&0xff)<<8));		pch++;
							ulFlags |= (0x000000ff&(*pch&0xff));				pch++;
							
							free(pszNumerics);
						}
						else
						{
							_ftime( &timestamp );  // fake, but close, at least
						}
					
						pchDelim = strchr(pchMarker, 10);
						ulBufferLen = pchDelim - pchMarker;
						pszMsg = msg.DecodeTen(pchMarker, &ulBufferLen);
						pchMarker = pchDelim+1;
					
						pchDelim = strchr(pchMarker, 10);
						ulBufferLen = pchDelim - pchMarker;
						pszCaller = msg.DecodeTen(pchMarker, &ulBufferLen); 
						pchMarker = pchDelim+1;
					
						pchDelim = strchr(pchMarker, 10);
						ulBufferLen = pchDelim - pchMarker;
						pszDest = msg.DecodeTen(pchMarker, &ulBufferLen);	
						pszMessageStack = pchDelim+1;
					
						// now, dispatch this message to all the destinations that we want.
//						while(pmsgr->m_bDispatchInUse) Sleep(1);
//						pmsgr->m_bDispatchInUse = true;

						EnterCriticalSection(&pmsgr->m_critDispatch);

						//traverse the list:
						if((pmsgr->m_ppDest!=NULL)&&(pmsgr->m_ucNumDest>0))
						{
							unsigned char ucDestIndex = 0;
							while(ucDestIndex<pmsgr->m_ucNumDest)
							{
								if(pmsgr->m_ppDest[ucDestIndex]!=NULL)
								{
									if(pmsgr->m_ppDest[ucDestIndex]->m_ucStatus&MSG_DESTSTATUS_INIT)
									{
										bool bCheckDest = false;
										if(pmsgr->m_ppDest[ucDestIndex]->m_ucType&MSG_DESTTYPE_DEFAULT)
										{
											if(ulFlags&MSG_SIGNAL_NODEFDEST)
											{
												// send it if its in the list only.
												bCheckDest = true;
											}
											else  // send it, its a default
											{
												switch(pmsgr->m_ppDest[ucDestIndex]->m_ucType&0x0f)
												{
												case MSG_DESTTYPE_LOG://						0x02  // log file
													{
														((CLogUtil*)pmsgr->m_ppDest[ucDestIndex])->HandleMessage(timestamp, ulFlags, pszMsg, pszCaller, pszDest);
													}	break;
												case MSG_DESTTYPE_EMAIL://					0x05  // email handler
													{
														((CSMTPUtil*)pmsgr->m_ppDest[ucDestIndex])->HandleMessage(timestamp, ulFlags, pszMsg, pszCaller, pszDest);
													} break;
												case MSG_DESTTYPE_INI://						0x01  // settings file
												case MSG_DESTTYPE_DB://							0x03  // database handler
												case MSG_DESTTYPE_UI://							0x04  // user interface handler
												case MSG_DESTTYPE_NET://						0x06  // network listener
												case MSG_DESTTYPE_UNDEF://					0x00  // undefined
												default :
													{
														pmsgr->m_ppDest[ucDestIndex]->HandleMessage(timestamp, ulFlags, pszMsg, pszCaller, pszDest);
													}	break;
												}
											}
										}
										else  // not a default, have to check to see if it's in the list.
										{
											bCheckDest = true;
										}

										if((bCheckDest)&&(pszDest)&&(strlen(pszDest)))
										{
											// pszDest is a comma separated list of destination names.
											if((pmsgr->m_ppDest[ucDestIndex]->m_pszDestName)&&(strlen(pmsgr->m_ppDest[ucDestIndex]->m_pszDestName)))
											{
//												bCheckDest = false;
												CSafeBufferUtil sbu;

												pchMarker = sbu.Token(pszDest, strlen(pszDest), ",");
												while(pchMarker)
												{
													// remove whitespace
													while((*pchMarker!=0)&&(isspace(*pchMarker))) pchMarker++;
													char* pch=pchMarker+strlen(pchMarker)-1;
													while((pch>pchMarker)&&(isspace(*pch))) { *pch=0; pch--;}

													if(strlen(pchMarker))
													{
														if(strcmp(pmsgr->m_ppDest[ucDestIndex]->m_pszDestName, pchMarker)==0)
														{
//AfxMessageBox("calling");
															switch(pmsgr->m_ppDest[ucDestIndex]->m_ucType&0x0f)
															{
															case MSG_DESTTYPE_LOG://						0x02  // log file
																{
																	((CLogUtil*)pmsgr->m_ppDest[ucDestIndex])->HandleMessage(timestamp, ulFlags, pszMsg, pszCaller, pszDest);
																}	break;
															case MSG_DESTTYPE_EMAIL://					0x05  // email handler
																{
																	((CSMTPUtil*)pmsgr->m_ppDest[ucDestIndex])->HandleMessage(timestamp, ulFlags, pszMsg, pszCaller, pszDest);
																}	break;
															case MSG_DESTTYPE_INI://						0x01  // settings file
															case MSG_DESTTYPE_DB://							0x03  // database handler
															case MSG_DESTTYPE_UI://							0x04  // user interface handler
															case MSG_DESTTYPE_NET://						0x06  // network listener
															case MSG_DESTTYPE_UNDEF://					0x00  // undefined
															default :
																{
																	pmsgr->m_ppDest[ucDestIndex]->HandleMessage(timestamp, ulFlags, pszMsg, pszCaller, pszDest);
																}	break;
															}
														}
													}
													pchMarker = sbu.Token(NULL, 0, ",");
												}
											}
										}
									}
								}
								ucDestIndex++;
							}
						}

						LeaveCriticalSection(&pmsgr->m_critDispatch);
//						pmsgr->m_bDispatchInUse = false;

						if(pszMsg) free(pszMsg);
						if(pszCaller) free(pszCaller);
						if(pszDest) free(pszDest);
					}
					free(pszStack);
				}
			}
			Sleep(1);// give the processor a break;
		}
		pmsgr->m_bThreadStarted = false;
	}
}

