// Messager.h: interface for the CMessager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGER_H__B7BF37FC_32DC_4A28_9E71_5FDD747271BB__INCLUDED_)
#define AFX_MESSAGER_H__B7BF37FC_32DC_4A28_9E71_5FDD747271BB__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "../MSG/MessagingObject.h"

// return values:
#define MSG_SUCCESS					0
#define MSG_ERROR						-1
#define MSG_ERROR_BADARG		-2
#define MSG_ERROR_NOTEXISTS	-3
#define MSG_ERROR_EXISTS		-4
#define MSG_ERROR_MEMEX			-5

// constants
#define MAX_MESSAGE_LENGTH 4096

// MessageDispatcher Callback
typedef int (__stdcall* LPFNDM)( unsigned long, char*, char*, char* );
//LPFNDM  DispatchMessage( unsigned long ulFlags, char* pszDestination, char* pszCaller, char* pszMessage );


// CMessager
class CMessager //: public CMessagingObject  // we make it a messaging object to give it the encode decode functions primarily. // ok, instead we have a member now.
{
public:
	CMessagingObject m_mobj;  // for encoding functions
	CMessagingObject** m_ppDest;  // array of destinations
	unsigned char m_ucNumDest;

	// message store and monitoring thread
	bool m_bKillThread;
	bool m_bThreadStarted;
	bool m_bStackInUse;
//	bool m_bDispatchInUse;
	char* m_pszMessageStack;
	unsigned long m_ulReqCount;
	unsigned long m_ulSvcCount;
	CRITICAL_SECTION m_crit;  //  critical section.for managing calls to message queue
	CRITICAL_SECTION m_critDispatch;  //  critical section.for managing calls to dispatcher

public:
	CMessager();
	virtual ~CMessager();

	// Dispatch Message
	void DM(unsigned long ulFlags, char* pszDest, char* pszCaller, char* pszMsg, ...); // ... formatted

	// core.  
	int AddDestination(unsigned char ucType, char* pszDestName, char* pszParams, char* pszInfo=NULL);
	int RemoveDestination(char* pszDestName, char* pszInfo=NULL);
	int ModifyDestination(char* pszDestName, char* pszParams, unsigned char ucType=MSG_DESTTYPE_UNDEF, char* pszInfo=NULL);

	int GetDestIndex(char* pszDestName);
	int ParamCheck(unsigned char ucType, char* pszParams);

};


// globals protos for setup of messenger feedback call
//void DispatchEncodedMessage(char* pszMsg, unsigned long ulFlags); // feedback messages from message objects.
//void InitMessager(CMessager* pmsgr);

// globals
void MessagerThread(void* pvArgs);


#endif // !defined(AFX_MESSAGER_H__B7BF37FC_32DC_4A28_9E71_5FDD747271BB__INCLUDED_)
