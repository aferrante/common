// MessagingObject.h: interface for the CMessagingObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGINGOBJECT_H__8A5D3309_DCEA_4321_B728_94CD7B0A8474__INCLUDED_)
#define AFX_MESSAGINGOBJECT_H__8A5D3309_DCEA_4321_B728_94CD7B0A8474__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "msg.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/timeb.h>

#define ZERO_ENCODE_CTRLCHAR			'z'
#define ZERO_ENCODE_ZEROCHAR			'Z'
#define ZERO_ENCODE_NOZEROTERM		0x00000000
#define ZERO_ENCODE_USEZEROTERM		0x00000001
#define TEN_ENCODE_CTRLCHAR				'|'
#define TEN_ENCODE_TENCHAR				'~'
#define TEN_DECODE_LFONLY					0x00000000  // just decode char 10.
#define TEN_DECODE_CRLF					  0x00000001  // decode as CRLF


class CMessagingObject  
{
public:
//	LPFNDM m_lpfnDM;  // instead of this pointer, use the following void *
	void* m_pmsgr;  // cast this inside. ( a pointer to the one and only CMessager object )
	char* m_pszDestName;
	char* m_pszParams;
	// messaging
	char* m_pszMsgDest;  //setup default message destination
	unsigned char m_ucStatus;
	unsigned char m_ucType;
	bool m_bEnableMessaging;  // defaults to true, but you can turn off for modules you dont want to hear from!

public:
	CMessagingObject();
	virtual ~CMessagingObject();

// to communicate back to dispatcher
//	int  InitializeMessaging(LPFNDM lpfnDM);
	int  InitializeMessaging(void* pmsgr);
	void UninitializeMessaging();
	void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);

// dispatcher calls
	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);
	int Modify(char* pchParams, unsigned long* pulBufferLen);

// utility
	char*  EncodeZero(char* pchInputBuffer, unsigned long* pulBufferLen, unsigned long ulFlags=ZERO_ENCODE_NOZEROTERM);
	char*  DecodeZero(char* pchInputBuffer, unsigned long* pulBufferLen);
	char*  EncodeTen(char* pchInputBuffer, unsigned long* pulBufferLen);
	char*  DecodeTen(char* pchInputBuffer, unsigned long* pulBufferLen, unsigned long ulFlags=TEN_DECODE_LFONLY);
};

#endif // !defined(AFX_MESSAGINGOBJECT_H__8A5D3309_DCEA_4321_B728_94CD7B0A8474__INCLUDED_)
