#ifndef BMP_HELPERS_INCLUDED
#define BMP_HELPERS_INCLUDED


#ifndef UNIVERSAL_CUSTOM_DEFINES_AND_TYPES_INCLUDED 
//if the common defines file has not been included somewhere else, just define
// the few defines used in this file below...

///////////////////// 
//defines

// These are damned useful:
#ifndef max
#define max(a,b)            ((a > b) ? a : b)
#endif

#ifndef min
#define min(a,b)            ((a < b) ? a : b)
#endif                    

//used with FontSettings_t
//styles
#ifndef FSS_REGULAR
#define FSS_REGULAR 0
#endif
#ifndef FSS_BOLD
#define FSS_BOLD 1
#endif
#ifndef FSS_ITALIC
#define FSS_ITALIC 2
#endif
//effects
#ifndef FSE_NONE
#define FSE_NONE 0
#endif
#ifndef FSE_UNDERLINE 
#define FSE_UNDERLINE 1
#endif
#ifndef FSE_STRIKEOUT 
#define FSE_STRIKEOUT 2
#endif

#endif //UNIVERSAL_CUSTOM_DEFINES_AND_TYPES_INCLUDED

///////////////////// 
//Function prototypes
//
//  A calling function, presumably has a window and a DC associated with it.
//  It can be fed in to the following functions directly, or it can be "gotten"
//  by putting the following lines into the calling code before the function calls are made.
//	HDC hdc;
//	hdc=GetDC()->GetSafeHdc();

class CBmpUtil  
{
public:
	static POINT ptNULL;

public:
	CBmpUtil();
	virtual ~CBmpUtil();
	HBITMAP	ButtonBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor);
	HBITMAP	ReplaceColorBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cReplacer, COLORREF cToBeReplaced=CLR_INVALID);
	HBITMAP ScaleBitmap(HDC hdc, HBITMAP hBitmap, float dx, float dy);
	HBITMAP FitBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, BOOL bMaintainAspectRatio=FALSE);
	HBITMAP FitBitmap(HDC hdc, HBITMAP hBitmap, SIZE sizeBounds, BOOL bMaintainAspectRatio=FALSE); //override
	HBITMAP FitBitmap(HDC hdc, HBITMAP hBitmap, RECT rcBounds, BOOL bMaintainAspectRatio=FALSE); //override
	HBITMAP FitButtonBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, COLORREF cTransparentColor, BOOL bMaintainAspectRatio=FALSE);
	HBITMAP FitButtonBitmap(HDC hdc, HBITMAP hBitmap, SIZE sizeBounds, COLORREF cTransparentColor, BOOL bMaintainAspectRatio=FALSE); //override
	HBITMAP FitButtonBitmap(HDC hdc, HBITMAP hBitmap, RECT rcBounds, COLORREF cTransparentColor, BOOL bMaintainAspectRatio=FALSE); //override
	HBITMAP AddBorderBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cColor=CLR_INVALID, int nWidth=1); 
	HBITMAP AddButtonBorderBitmap(HDC hdc, HBITMAP hBitmap, int nWidth); 
	HBITMAP TextOutBitmap(HDC hdc, char* pszText, char* pszFontFace="Arial", int nPoints=100, int nStyle=0,int nEffects=0, COLORREF cTextColor=RGB(0,0,0), COLORREF cBG=RGB(255,255,255), int nBorder=0);
	HBITMAP TextOutBitmap(HDC hdc, char* pszText, HFONT hFont, COLORREF cTextColor=RGB(0,0,0), COLORREF cBG=RGB(255,255,255), int nBorder=0);
	HBITMAP TrimBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cColor=CLR_INVALID);
	HBITMAP ColorBitmap(HDC hdc, SIZE size, COLORREF cColor=CLR_INVALID);
	HBITMAP ColorBitmap(HDC hdc, RECT rect, COLORREF cColor=CLR_INVALID); //override
	HBITMAP ColorBitmap(HDC hdc, int nWidth, int nHeight, COLORREF cColor=CLR_INVALID); //override
	HBITMAP RotateBitmap(HDC hdc, HBITMAP hBitmap, double dAng, BOOL bRadians=FALSE, COLORREF cBG=CLR_INVALID);
	HBITMAP SupersetBitmap(HDC hdc, HBITMAP hBitmap1, HBITMAP hBitmap2, POINT ptRelOrigin=ptNULL, COLORREF cBG=CLR_INVALID);
	HBITMAP SubsetBitmap(HDC hdc, HBITMAP hBitmap, RECT rcRect, COLORREF cBG=CLR_INVALID);
	HBITMAP SubsetBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, POINT ptTopLeft=ptNULL,COLORREF cBG=CLR_INVALID); //override
	HBITMAP SubsetBitmap(HDC hdc, HBITMAP hBitmap, SIZE size, POINT ptTopLeft=ptNULL,COLORREF cBG=CLR_INVALID); //override
	HBITMAP SubsetBitmap(HDC hdc, HBITMAP hBitmap, POINT ptTopLeft, POINT ptBottomRight,COLORREF cBG=CLR_INVALID); //override
	HBITMAP PutRectBitmap(HDC hdc, HBITMAP hBitmap, RECT rcRect, COLORREF cColor=CLR_INVALID);
	HBITMAP PutRectBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, POINT ptTopLeft=ptNULL, COLORREF cColor=CLR_INVALID);
	HBITMAP PutRectBitmap(HDC hdc, HBITMAP hBitmap, SIZE size, POINT ptTopLeft=ptNULL, COLORREF cColor=CLR_INVALID);
	HBITMAP PutRectBitmap(HDC hdc, HBITMAP hBitmap, POINT ptTopLeft, POINT ptBottomRight, COLORREF cColor=CLR_INVALID);
	HBITMAP	ReturnTransparentBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor, RECT rcRect, POINT pt=ptNULL  );
	HBITMAP	ReturnTransparentBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor, POINT pt=ptNULL ); //override
	HBITMAP	ReturnTransparentBitmap(HDC hdc, HBITMAP hBitmap, HBITMAP hBitmapBG, COLORREF cTransparentColor, POINT pt=ptNULL );//override
	BOOL		TransparentBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor, POINT pt=ptNULL );
	COLORREF GetTopLeftColor(HDC hdc, HBITMAP hBitmap);
};


#endif //BMP_HELPERS_INCLUDED defined

