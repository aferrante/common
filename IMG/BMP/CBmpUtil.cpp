// Cbmp.cpp... made from bmp.cpp 12-June-2004 gkl (turned into a class)

// bmp.cpp
// by Kenji Larsen
//
// History:
// 7-25 November 2001  gkl  Library of bitmap functions
//  
// 

#include <windows.h> // using MFC
#include <stdlib.h>
#include <math.h>

#include "CBmpUtil.h"

//Todo: remove MFC

// constructor
CBmpUtil::CBmpUtil()
{
	ptNULL.x = 0;
	ptNULL.y = 0;
}

//destructor
CBmpUtil::~CBmpUtil()
{
}


/////////////////////   
// FUNCTIONS
//

/////////////
//  ButtonBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that has one color in it that will be the
//    "transparent color"
//  cTransparentColor, the colorref of the "transparent color".  Pixels of this color
//  in the source bitmap will be replaced by the COLOR_3DFACE color (used for buttons).

//  ButtonBitmap returns a handle to a bitmap that has the colors replaced, or
//    NULL if operation is unsuccessful


HBITMAP CBmpUtil::ButtonBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor)
{
	return ReplaceColorBitmap(hdc, hBitmap, GetSysColor(COLOR_3DFACE), cTransparentColor);
}

/////////////
//  ReplaceColorBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that has one color in it that will be the
//    "color to be replaced"
//  cReplacer, the colorref of the new color, which replaces the "color to be replaced".
//  cToBeReplaced, the colorref of the "color to be replaced".  Pixels of this color
//    in the source bitmap will be replaced by the cReplacer color  
//    ifcToBeReplaced is omitted or CLR_INVALID, the entire bitmap is painted the cReplacer color.

//  ReplaceColorBitmap returns a handle to a bitmap that has the colors replaced,
//    a handle to the original bmp if the replacer color is not valid, or
//    NULL if operation is unsuccessful


HBITMAP CBmpUtil::ReplaceColorBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cReplacer, COLORREF cToBeReplaced)
{
	if (cReplacer==CLR_INVALID) return hBitmap;
	BITMAP bm;
	HDC hdcTemp;
	HBITMAP	hbm,hbmOldTemp;

	hdcTemp = CreateCompatibleDC(hdc);

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	SetMapMode(hdcTemp, GetMapMode(hdc));

	hbm = CreateCompatibleBitmap(hdc, bm.bmWidth, bm.bmHeight);

	hbmOldTemp = (HBITMAP)SelectObject(hdcTemp,hbm);

	SelectObject(hdcTemp,CreateSolidBrush(cReplacer));
	PatBlt(hdcTemp, 0, 0, bm.bmWidth, bm.bmHeight, PATCOPY ); 

	if (cToBeReplaced!=CLR_INVALID) TransparentBitmap(hdcTemp, hBitmap, cToBeReplaced, ptNULL);

	// clean it up.
//	DeleteObject(SelectObject(hdcTemp,hbmOldTemp));
	DeleteDC(hdcTemp);

	return hbm;
}

////////////
//  ScaleBitmap is something like StretchBlt(), but it returns a
//  handle to the new scaled bitmap.  This function is simpler to 
//  call because it transforms but does not locate.
//  dx and dy are the x and y axis scale factors (1.0 = 100%)
//


HBITMAP CBmpUtil::ScaleBitmap(HDC hdc, HBITMAP hBitmap, float dx, float dy)
{
   BITMAP     bm;
   HBITMAP    hbmSc;
   HDC        hdcTemp, hdcSc;
   POINT      ptSize,ptNewSize;
    
   hdcTemp = CreateCompatibleDC(hdc);
   SelectObject(hdcTemp, hBitmap);   // Select the bitmap

   GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

   ptSize.x = bm.bmWidth;            // Get width of bitmap
   ptSize.y = bm.bmHeight;           // Get height of bitmap
   DPtoLP(hdcTemp, &ptSize, 1);      // Convert from device
                                     // to logical points

   SetMapMode(hdcTemp, GetMapMode(hdc));
   hdcSc = CreateCompatibleDC(hdc);
   
   ptNewSize.x = (int)((double)ptSize.x*dx);      // width of new bitmap
   ptNewSize.y = (int)((double)ptSize.y*dy);      // height of new bitmap
      

   hbmSc = CreateCompatibleBitmap(hdc, ptNewSize.x, ptNewSize.y);
   SelectObject(hdcSc,hbmSc);

    // Do it.
   StretchBlt(hdcSc, 0, 0, ptNewSize.x, ptNewSize.y, 
              hdcTemp, 0, 0, ptSize.x, ptSize.y, SRCCOPY);

   // clean it up.
   DeleteDC(hdcTemp);
   DeleteDC(hdcSc);
   return hbmSc;
}      


/////////////
//  FitBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that will be scaled
//	hWnd, a pointer to the window in which it will be fit (client area), or
//  rcBounds, a rectangle to fit the bitmap to (an override), or
//  sizeBounds, a size to fit the bitmap to in pixels (an override).

//  you can use this function like this:

//  CBitmap bmp;  bmp.LoadBitmap(IDB_BITMAP); 
//  ((CStatic*)GetDlgItem(IDC_STATIC))->
//     SetBitmap(FitBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), GetDlgItem(IDC_STATIC));
// or
//  ((CButton*)GetDlgItem(IDC_BUTTON))->
//     SetBitmap(FitBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), GetDlgItem(IDC_BUTTON));

//  FitBitmap returns handle to a bitmap that has been fit
//  or, NULL if operation is unsuccessful


HBITMAP CBmpUtil::FitBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, BOOL bMaintainAspectRatio)
{
	RECT rc;
	SIZE size;
	GetClientRect(hWnd, &rc);
	size.cx = abs(rc.right-rc.left);
	size.cy = abs(rc.bottom-rc.top);
	return FitBitmap(hdc, hBitmap, size, bMaintainAspectRatio);
}

HBITMAP CBmpUtil::FitBitmap(HDC hdc, HBITMAP hBitmap, RECT rcBounds, BOOL bMaintainAspectRatio)
{
	SIZE size;
	size.cx = abs(rcBounds.right-rcBounds.left);
	size.cy = abs(rcBounds.bottom-rcBounds.top);
	return FitBitmap(hdc, hBitmap, size, bMaintainAspectRatio);
}

HBITMAP CBmpUtil::FitBitmap(HDC hdc, HBITMAP hBitmap, SIZE szBounds, BOOL bMaintainAspectRatio)
{

	BITMAP     bm;
	HDC        hdcTemp;

	hdcTemp = CreateCompatibleDC(hdc);
	SelectObject(hdcTemp, hBitmap);   // Select the bitmap

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	DeleteDC(hdcTemp);

	float scalex, scaley;
	scalex=((float)szBounds.cx/(float)bm.bmWidth);
	scaley=((float)szBounds.cy/(float)bm.bmHeight);

	if(bMaintainAspectRatio)
	{
		if (scalex < scaley) scaley = scalex;
		else scalex = scaley;
	}
	return ScaleBitmap(hdc, hBitmap, scalex, scaley );
}

/////////////
//  FitButtonBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that will be scaled 
//    (pixels of cTransparentColor in the original bmp will be replaced with
//    the COLOR_3DFACE color.)
//	hWnd, a pointer to the window in which it will be fit (client area), or
//  rcBounds, a rectangle to fit the bitmap to (an override), or
//  sizeBounds, a size to fit the bitmap to in pixels (an override).

//  you can use this function like this:

//  CBitmap bmp;  bmp.LoadBitmap(IDB_BITMAP); 
//  ((CStatic*)GetDlgItem(IDC_STATIC))->
//     SetBitmap(FitButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), GetDlgItem(IDC_STATIC), RGB(192,192,192));
// or
//  ((CButton*)GetDlgItem(IDC_BUTTON))->
//     SetBitmap(FitButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), GetDlgItem(IDC_BUTTON), RGB(192,192,192));

//  FitButtonBitmap returns handle to a bitmap that has been fit
//  or, NULL if operation is unsuccessful

HBITMAP CBmpUtil::FitButtonBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, COLORREF cTransparentColor, BOOL bMaintainAspectRatio)
{
	RECT rc;
	SIZE size;
	GetClientRect(hWnd, &rc);
	size.cx = abs(rc.right-rc.left);
	size.cy = abs(rc.bottom-rc.top);
	return FitButtonBitmap(hdc, hBitmap, size, cTransparentColor,bMaintainAspectRatio);
}

HBITMAP CBmpUtil::FitButtonBitmap(HDC hdc, HBITMAP hBitmap, RECT rcBounds, COLORREF cTransparentColor, BOOL bMaintainAspectRatio)
{
	SIZE size;
	size.cx = abs(rcBounds.right-rcBounds.left);
	size.cy = abs(rcBounds.bottom-rcBounds.top);
	return FitButtonBitmap(hdc, hBitmap, size, cTransparentColor, bMaintainAspectRatio);
}

HBITMAP CBmpUtil::FitButtonBitmap(HDC hdc, HBITMAP hBitmap, SIZE sizeBounds, COLORREF cTransparentColor, BOOL bMaintainAspectRatio)
{
	HBITMAP hbmp;
	hbmp = ReplaceColorBitmap(hdc, hBitmap, GetSysColor(COLOR_3DFACE), cTransparentColor);
	if (hbmp!=NULL)
	{
		return FitBitmap(hdc, hbmp, sizeBounds, bMaintainAspectRatio);
	}
	else
	return NULL;
}

/////////////
//  AddBorderBitmap and AddButtonBorderBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that will be have a border added
//	cColor, the color of the border, if omitted or CLR_INVALID, the top left pixel color of
//     the source bitmap is used.  THis arg is omitted in AddButtonBorderBitmap
//     and the COLOR_3DFACE color is used.
//  nWidth, the width of the border.  If omitted default value is 1

//  AddBorderBitmap returns handle to a bitmap that has a border of nWidth width,
//    the handle to the original bitmap if width is <= 0,
//    NULL if operation is unsuccessful

HBITMAP CBmpUtil::AddBorderBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cColor, int nWidth) 
{

	if (nWidth<=0) return hBitmap;

	BITMAP     bm;
	HBITMAP    hbmB;
	HDC        hdcTemp, hdcB;
	POINT      ptSize,ptNewSize;

	hdcTemp = CreateCompatibleDC(hdc);
	SelectObject(hdcTemp, hBitmap);   // Select the bitmap

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	ptSize.x = bm.bmWidth;            // Get width of bitmap
	ptSize.y = bm.bmHeight;           // Get height of bitmap
	DPtoLP(hdcTemp, &ptSize, 1);      // Convert from device
																	 // to logical points

	SetMapMode(hdcTemp, GetMapMode(hdc));
	hdcB = CreateCompatibleDC(hdc);

	ptNewSize.x = ptSize.x+nWidth+nWidth;      // width of new bitmap
	ptNewSize.y = ptSize.y+nWidth+nWidth;      // height of new bitmap
  

	hbmB = CreateCompatibleBitmap(hdc, ptNewSize.x, ptNewSize.y);
	SelectObject(hdcB,hbmB);

	if(cColor!=CLR_INVALID) SelectObject(hdcB,CreateSolidBrush(cColor));
	else
	{
		COLORREF crTemp;
		crTemp=GetPixel(hdcTemp,0,0);
		if(crTemp!=CLR_INVALID) SelectObject(hdcB,CreateSolidBrush(crTemp));
		else SelectObject(hdcB,CreateSolidBrush(GetSysColor(COLOR_3DFACE)));
	}
	PatBlt(hdcB, 0, 0, ptNewSize.x, ptNewSize.y, PATCOPY ); 

	// Do it.
	BitBlt(hdcB, nWidth, nWidth, ptSize.x, ptSize.y, 
						hdcTemp, 0, 0, SRCCOPY);

	// clean it up.
	DeleteDC(hdcTemp);
	DeleteDC(hdcB);
	return hbmB;
}      

HBITMAP CBmpUtil::AddButtonBorderBitmap(HDC hdc, HBITMAP hBitmap, int nWidth)
{return AddBorderBitmap( hdc, hBitmap, GetSysColor(COLOR_3DFACE), nWidth);}

/////////////
//  TextOutBitmap takes as arguments:
//  hdc, a specific device context.
//  szText, the text to be rendered
//  font, the font to render the text in, ot the following individual font attribs:
//  szFontFace, the text to be rendered
//  nPoints, the point size oftext to be rendered, in tenth point units. 8 point = 80 nPoints.
//  nStyle, the styles of text to be rendered (bold, italic) - uses common defs FSS_BOLD, FSS_ITALIC
//  nEffects, the effects of text to be rendered (underline, strikeout) - uses common defs FSE_UNDERLINE, FSE_STRIKEOUT
//  cTextColor, the color of the text to be rendered.  If CLR_INVALID, no text is rendered, but a BGcolor bitmap will be returned of the proper size.
//	cBG, the color of the background. if CLR_INVALID, the COLOR_3DFACE color is used.
//  nBorder, the width of the border (of BG color).  If omitted default value is 0

//  TextOutBitmap returns handle to a bitmap that has the rendered text, 
//    of size text bounding box+border, NULL if operation is unsuccessful


HBITMAP CBmpUtil::TextOutBitmap(HDC hdc, char* pszText, char* pszFontFace, int nPoints, int nStyle, int nEffects, COLORREF cTextColor, COLORREF cBG, int nBorder)
{
	HFONT hFont;
	int nWeight;
	if(nStyle&FSS_BOLD) nWeight = FW_BOLD;
	else nWeight = FW_NORMAL;
	hFont = CreateFont(MulDiv((int)((float)nPoints/10.0),-GetDeviceCaps(hdc,LOGPIXELSY),72),
		0,0,0,nWeight,nStyle&FSS_ITALIC,nEffects&FSE_UNDERLINE,nEffects&FSE_STRIKEOUT,ANSI_CHARSET,
		 OUT_STROKE_PRECIS,CLIP_STROKE_PRECIS,PROOF_QUALITY,
		 VARIABLE_PITCH|FF_DONTCARE,pszFontFace);
	
	if(hFont==NULL)
	{
		//AfxMessageBox("Font Error 01: Font could not be created"); 
		return NULL;
	}
	else
		return TextOutBitmap(hdc, pszText, hFont, cTextColor, cBG, nBorder);
}

HBITMAP CBmpUtil::TextOutBitmap(HDC hdc, char* pszText, HFONT hFont, COLORREF cTextColor, COLORREF cBG, int nBorder)
{
	if ((pszText==NULL)||(strlen(pszText)<=0)||(hdc==NULL)||(hFont==NULL)) 
	{
		//AfxMessageBox("Font Error 02: init"); 
		return NULL;
	}

	HBITMAP    hbmTemp,hbmOut;
	HDC        hdcMem;
	SIZE      size;
	
	hdcMem = CreateCompatibleDC(hdc); //dc for returnable bmp
	if (hdcMem==NULL) return NULL;
	SetMapMode(hdcMem, GetMapMode(hdc));

	HFONT hOldFont = (HFONT)(SelectObject(hdcMem, hFont));
//	size=GetTextExtent(hdcMem, szText);

	SetBkMode(hdcMem, TRANSPARENT); 
	// Bracket begin a path 
	BeginPath(hdcMem); 
	// Send some text out 
	TextOut(hdcMem, 0, 0, pszText, strlen(pszText)); 
	// Bracket end a path 
	EndPath(hdcMem); 

	HRGN hRgn = PathToRegion(hdcMem);
	RGNDATA rgnData;

	GetRegionData( hRgn, sizeof(RGNDATA), &rgnData); 
 
	size.cx = abs(rgnData.rdh.rcBound.right - rgnData.rdh.rcBound.left)*2;
	size.cy = abs(rgnData.rdh.rcBound.bottom - rgnData.rdh.rcBound.top)*2;

	hbmTemp = ColorBitmap(hdc, size, cBG);  //double sized bitmap!

//	return hbmTemp;
	if (hbmTemp==NULL) return NULL;

	SelectObject(hdcMem,hbmTemp);
	if(cBG!=CLR_INVALID) SetBkColor(hdcMem,cBG);
	else	{cBG=GetSysColor(COLOR_3DFACE); SetBkColor(hdcMem,cBG);}

	SetBkMode(hdcMem,TRANSPARENT);
	COLORREF cText;
	if((cTextColor==cBG) ||(cTextColor==CLR_INVALID)) //case for temp text color.
		cText=RGB(max(GetRValue(cBG)-127,32),GetGValue(cBG),GetBValue(cBG)); // i.e. something diff from cBG
	else 	cText=cTextColor;

	SetTextAlign(hdcMem,TA_LEFT|TA_TOP);
	SetTextColor(hdcMem,cText);
	TextOut(hdcMem, size.cx/2, size.cy/2, pszText, strlen(pszText)); 

	// clean it up.
	DeleteObject(hFont);
	DeleteDC(hdcMem);

	// return the right thing
	hbmOut = AddBorderBitmap(hdc, TrimBitmap(hdc, hbmTemp), cBG, nBorder);
	DeleteObject(hbmTemp);

	if((cTextColor==cBG) ||(cTextColor==CLR_INVALID)) //case for temp text color.
		return ReplaceColorBitmap(hdc, hbmOut, cBG);
	else
		return hbmOut;
}

/////////////////
//  TrimBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source Bitmap.
//  cColor, the "border color"
//  TrimBitmap just checks the border for uninterrupted rows
//    of pixels of the color cColor, then strips them off.
//    If cColor is CLR_INVALID or omitted, the function uses the color 
//    of the top left pixel in the source bitmap
//    It is essentially an autocropping function.

//  TrimBitmap returns handle to a bitmap that has been autocropped, or
//    NULL if operation is unsuccessful
HBITMAP CBmpUtil::TrimBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cColor)
{
   BITMAP     bm;
   HBITMAP    hbmTrim;
   HDC        hdcTemp, hdcTrim;
   POINT      ptSize,ptNewSize;
   int i,j,ymin,ymax,xmin,xmax;  // counters, max/mins
   int xxf,xnf,yxf,ynf;          // found flags.
 
   hdcTemp = CreateCompatibleDC(hdc);
   SelectObject(hdcTemp, hBitmap);   // Select the bitmap

   GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

   ptSize.x = bm.bmWidth;            // Get width of bitmap
   ptSize.y = bm.bmHeight;           // Get height of bitmap
   DPtoLP(hdcTemp, &ptSize, 1);      // Convert from device
                                     // to logical points

   SetMapMode(hdcTemp, GetMapMode(hdc));
   hdcTrim = CreateCompatibleDC(hdc);

	 if (cColor==CLR_INVALID)	cColor=GetPixel(hdcTemp,0,0); //use top left pixel color
	 if (cColor==CLR_INVALID)	return NULL; // if still no color, just exit.

   xnf = i = 0;
   while (!xnf)
   {
     for (j=0;j<ptSize.y;j++)
     {
        if (GetPixel(hdcTemp,i,j) != cColor)
        {xmin = i; xnf = 1;}
     }
     i++;
   }

   xxf = i = 0;
   while (!xxf)
   {
     for (j=0;j<ptSize.y;j++)
     {
        if ((GetPixel(hdcTemp,ptSize.x-i-1,j) != cColor)||(ptSize.x-i-1<=xmin))
        {xmax = ptSize.x-i-1; xxf = 1;}
     }
     i++;
   }
   
   ynf = i = 0;
   while (!ynf)
   {
     for (j=0;j<ptSize.x;j++)
     {
        if (GetPixel(hdcTemp,j,i) != cColor)
        {ymin = i; ynf = 1;}
     }
     i++;
   }
   
   yxf = i = 0;
   while (!yxf)
   {
     for (j=0;j<ptSize.x;j++)
     {
        if ((GetPixel(hdcTemp,j,ptSize.y-i-1) != cColor)||(ptSize.y-i-1<=ymin))
        {ymax = ptSize.y-i-1; yxf = 1;}
     }
     i++;
   }

   ptNewSize.x = xmax-xmin+1;      // width of new bitmap
   ptNewSize.y = ymax-ymin+1;      // height of new bitmap
      
   hbmTrim = CreateCompatibleBitmap(hdc, ptNewSize.x, ptNewSize.y);
   SelectObject(hdcTrim,hbmTrim);

   // Do it.
   BitBlt(hdcTrim, 0, 0, ptNewSize.x, ptNewSize.y, hdcTemp, xmin, ymin, SRCCOPY);

   // clean it up.
   DeleteDC(hdcTemp);
   DeleteDC(hdcTrim);
   return hbmTrim;
}   

/////////////
//  ColorBitmap takes as arguments:
//  hdc, a specific device context.
//	size, the size of the bitmap in SIZE form, or
//	rect, the size of the bitmap in RECT form, or
//	nWidth, the width of the bitmap, and
//	nHeight, the height of the bitmap.
//	cColor, the color of whole bitmap. if CLR_INVALID, the COLOR_3DFACE color is used.

//  ColorBitmap returns handle to a single-color bitmap of the specified size, or
//    NULL if operation is unsuccessful

HBITMAP CBmpUtil::ColorBitmap(HDC hdc, RECT rect, COLORREF cColor) //override
{
	SIZE size;
	size.cx = abs(rect.right-rect.left);
	size.cy = abs(rect.bottom-rect.top);
	return ColorBitmap(hdc, size, cColor);
}
HBITMAP CBmpUtil::ColorBitmap(HDC hdc, int nWidth, int nHeight, COLORREF cColor) //override
{	
	SIZE size;
	size.cx = abs(nWidth);
	size.cy = abs(nHeight);
	return ColorBitmap(hdc, size, cColor);
}

HBITMAP CBmpUtil::ColorBitmap(HDC hdc, SIZE size, COLORREF cColor)
{
	if ((size.cx<=0)||(size.cy<=0)||(hdc==NULL)) 	return NULL;

	HBITMAP    hbmB, hbmOut;
	HDC        hdcB, hdcMem;

	hdcB = CreateCompatibleDC(hdc);  //dc for discardable bmp
	hdcMem = CreateCompatibleDC(hdc); //dc for returnable bmp
	if ((hdcB==NULL)||(hdcMem==NULL))	{		return NULL; 	}
	SetMapMode(hdcB, GetMapMode(hdc));
	SetMapMode(hdcMem, GetMapMode(hdc));

  hbmB = CreateDiscardableBitmap( hdcB, size.cx, size.cy );

	if (hbmB==NULL)	{	return NULL; }

	SelectObject(hdcB,hbmB);

	if(cColor!=CLR_INVALID) SelectObject(hdcB,CreateSolidBrush(cColor));
	else	SelectObject(hdcB,CreateSolidBrush(GetSysColor(COLOR_3DFACE)));

	PatBlt(hdcB, 0, 0, size.cx, size.cy, PATCOPY ); 

	hbmOut = CreateCompatibleBitmap(hdc, size.cx, size.cy);
	SelectObject(hdcMem,hbmOut);
	BitBlt(hdcMem, 0, 0, size.cx, size.cy, hdcB, 0, 0, SRCCOPY); // copy from discardable to returnable.

	// clean it up.
	DeleteDC(hdcMem);
	DeleteDC(hdcB);
	DeleteObject(hbmB);
	return hbmOut;
}

// todo:   from here down, remove MFC

/////////////
//  RotateBitmap takes as arguments:
//  hdc, a specific device context.
//	hBitmap, a handle to the source bitmap
//	dAng, a rotation angle
//	bRadians, TRUE or FALSE to indicate if the dAng value is degrees or radians. default FALSE.
//	cBG, the color of the background. The background is the area in the corners that now exist
//    because of the rotation.
//    if CLR_INVALID, the COLOR_3DFACE color is used.

//  RotateBitmap returns handle handle to a bitmap which is a rotated
//    version of the original, or
//    NULL if operation is unsuccessful

//  Note: be careful. This function returns the handle to a bitmap which is a rotated
//  version of the original.  However, the new bitmap is always equal or larger than the orig.
//  This function Rotates the bitmap about its own center, then does an autocrop to reduce any
//  extra lines of cBG that might exist around the periphery.  The trimming is relatively 
//  minor, but exists because the resulting bitmap is made a little larger than necessary
//  to compensate for rounding and precision errors.  At the end of the function the return
//  value can be changed such that the return bitmap is not trimmed.
//
HBITMAP CBmpUtil::RotateBitmap(HDC hdc, HBITMAP hBitmap, double dAng, BOOL bRadians, COLORREF cBG)
{
	int nSpecialCase=0;
	BITMAP     bm;
	HBITMAP    hbm;
	HDC        hdcTemp, hdcRot;
	POINT      ptSize, ptNewSize; 
	int i,j;
	double cosa;
	double sina;

	hdcTemp = CreateCompatibleDC(hdc);
	SelectObject(hdcTemp, hBitmap);   // Select the bitmap

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	ptSize.x = bm.bmWidth;            // Get width of bitmap
	ptSize.y = bm.bmHeight;           // Get height of bitmap

	DPtoLP(hdcTemp, &ptSize, 1);      // Convert from device
																	 // to logical points
	SetMapMode(hdcTemp, GetMapMode(hdc));
	hdcRot  =  CreateCompatibleDC(hdc);
	SetMapMode(hdcRot, GetMapMode(hdc));

	if(!bRadians) //do a check for special case 90-degree increments
	{
		//first make it in the range (0.0-359.999)
		while(dAng>=360.0) {dAng-=360.0;}
		while(dAng<0.0) {dAng+=360.0;}

		if(dAng==0.0){cosa=1.0;sina=0.0; ptNewSize.x=ptSize.x; ptNewSize.y=ptSize.y; }
		else if(dAng==90.0){cosa=0.0;sina=1.0; ptNewSize.x=ptSize.y; ptNewSize.y=ptSize.x; }
		else if(dAng==180.0){cosa=-1.0;sina=0.0; ptNewSize.x=ptSize.x; ptNewSize.y=ptSize.y; }
		else if(dAng==270.0){cosa=0.0;sina=-1.0; ptNewSize.x=ptSize.y; ptNewSize.y=ptSize.x; }
		else 
		{
			dAng*=-0.017453292519943295769236907684886; //pi/180 - now we have radians
			bRadians=TRUE;
		}
	}

	if(bRadians)
	{
		cosa = cos(dAng);
		sina = sin(dAng);
		int x1,x2,y1,y2;
		x1 = (int)(((double)bm.bmWidth*cosa + (double)bm.bmHeight*sina)/2.0);
		y1 = (int)(((double)bm.bmHeight*cosa - (double)bm.bmWidth*sina)/2.0);
		x2 = (int)(((double)bm.bmHeight*sina - (double)bm.bmWidth*cosa)/2.0);
		y2 = (int)(((double)bm.bmHeight*cosa + (double)bm.bmWidth*sina)/2.0);

		ptNewSize.x=max(abs(x1),abs(x2))*2+5;  //+5 for int truncation.
		ptNewSize.y=max(abs(y1),abs(y2))*2+5;
	}

//  hbm = CreateCompatibleBitmap(hdc, ptNewSize.x, ptNewSize.y);
  hbm = ColorBitmap(hdc, ptNewSize.x, ptNewSize.y,cBG);

	SelectObject(hdcRot,hbm);

	//rotate!  
	double xtrans = (double)ptNewSize.x/2.0;
	double ytrans = (double)ptNewSize.y/2.0;
	double xtransback = (double)ptSize.x/2.0+0.5;  //0.5 to make int be round
	double ytransback = (double)ptSize.y/2.0+0.5;
	double ix,jy;
	int testx;
	int testy;

	// fill the larger destination bitmap pixel by pixel.
	// It would be faster to calculate where the source pixels map to on the target,
	// but it results in varying grids of cBG dots depending on different
	// rotation, due to floating point error when rounding.
	for (i=0;i<ptNewSize.x;i++)
	{
		for (j=0;j<ptNewSize.y;j++)
		{  
			ix=(double)i-xtrans;
			jy=(double)j-ytrans;
			testx = (int)(ix*cosa + jy*sina+ xtransback);
			testy = (int)(jy*cosa - ix*sina+ ytransback);
			if ((testx < 0)||
					(testx >= ptSize.x)||  
					(testy < 0)||  
					(testy >= ptSize.y))
			{
//don't need if using ColorBitmap()above avoid slow SetPixel
//				if(cBG!=CLR_INVALID) SetPixel(hdcRot,i,j,cBG);
//				else SetPixel(hdcRot,i,j,GetSysColor(COLOR_3DFACE));
			}
			else
			{
				SetPixel(hdcRot,i,j,GetPixel(hdcTemp,testx,testy));
			}
		}
	}     

	// clean it up.
	DeleteDC(hdcTemp);
	DeleteDC(hdcRot);
	
//	return hbm;  //or the following for a trimmed border
	return TrimBitmap(hdc, hbm);
/*
  // have the manual choice.
	if(AfxMessageBox("Trim?",MB_YESNO)==IDYES) 
		return TrimBitmap(hdc, hbm);
	else
		return hbm;
*/
}                      

/////////////
//  SupersetBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap1, a handle to a source bitmap 
//  hBitmap2, a handle to a second source bitmap 
//	ptRelOrigin, the x and y coords of where the top left corner of the second bitmap
//    will be in relation to the top left corner of the first bitmap.  A NULL pt will
//    assume (right edge of first bitmap , top of first bitmap) i.e. next to each other     
//	cBG, the color of the background. The background is any area that now exists because 
//    of positional and/or size differences between the two source bitmaps
//    if CLR_INVALID, the COLOR_3DFACE color is used.

//  SupersetBitmap returns a handle to a bitmap which contains both of the source bitmaps,
//    where the second is placed in relation to the first.  If there is overlap, the 
//    second bitmap is placed "over" the first.  It is possible to get the leftmost bitmap
//    be "over" the rightmost by giving the ptRelOrigin negative values.
//    SupersetBitmap returns NULL if operation is unsuccessful
//
HBITMAP CBmpUtil::SupersetBitmap(HDC hdc, HBITMAP hBitmap1, HBITMAP hBitmap2, POINT ptRelOrigin, COLORREF cBG)
{
	 BITMAP     bm1,bm2;
   HBITMAP    hbm;
   HDC        hdcTemp, hdcOut;
   POINT      ptNewSize, ptB1=ptNULL, ptB2=ptRelOrigin;
    
   hdcTemp = CreateCompatibleDC(hdc);
   hdcOut = CreateCompatibleDC(hdc);

   GetObject(hBitmap1, sizeof(BITMAP), (LPSTR)&bm1);
   GetObject(hBitmap2, sizeof(BITMAP), (LPSTR)&bm2);

   SetMapMode(hdcTemp, GetMapMode(hdc));
   SetMapMode(hdcOut, GetMapMode(hdc));

   if(ptB2.x<0) {ptB1.x-=ptB2.x; ptB2.x=0;}
   if(ptB2.y<0) {ptB1.y-=ptB2.y; ptB2.y=0;}

   ptNewSize.x = max(ptB1.x+bm1.bmWidth,ptB2.x+bm2.bmWidth);      // width of new bitmap
   ptNewSize.y = max(ptB1.y+bm1.bmHeight,ptB2.y+bm2.bmHeight);      // height of new bitmap
      
   hbm = ColorBitmap(hdc, ptNewSize.x, ptNewSize.y,cBG);
   SelectObject(hdcOut,hbm);

    // Do it.
   SelectObject(hdcTemp, hBitmap1);   // Select the bitmap
   BitBlt(hdcOut, ptB1.x, ptB1.y, bm1.bmWidth, bm1.bmHeight, hdcTemp, 0, 0,  SRCCOPY);
   SelectObject(hdcTemp, hBitmap2);   // Select the bitmap
   BitBlt(hdcOut, ptB2.x, ptB2.y, bm2.bmWidth, bm2.bmHeight, hdcTemp, 0, 0,  SRCCOPY);

   // clean it up.
   DeleteDC(hdcTemp);
   DeleteDC(hdcOut);
   return hbm;

}


/////////////
//  SubsetBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap 
//	rcRect, the coords defining a rectangle inside the bitmap, indicating which
//    part of the source bitmap will become the subset bitmap.  If cBG is omitted,
//    any rectangle that exceeds the bounds of the source bitmap will be truncated to the
//    source bitmap edge.  If cBG is specified, the part of the rectangle extending beyond
//    the source bitmap, if any, will be filled in with cBG.  or, rcRect may be defined 
//    using certain pairs of the following arguments:
//  hWnd, a pointer to a window whose client area will define the size of rcRect,
//  size, a SIZE object whose members describe the width and height of rcRect, 
//  ptTopLeft, a point which will define the relational location of the top left corner 
//    of rcRect on the source bitmap.  Identical rules for the use of cBG apply.
//  ptBottomRight, a point which will define the relational location of the bottom right corner 
//    of rcRect on the source bitmap.  
//	cBG, the color of the background.  If CLR_INVALID, the size of the returned bitmap
//    is truncated such that no background color is shown.

//  SubsetBitmap returns a handle to a bitmap which contains a subset bitmap, or
//    NULL if operation is unsuccessful
//
HBITMAP CBmpUtil::SubsetBitmap(HDC hdc, HBITMAP hBitmap, RECT rcRect, COLORREF cBG)
{
	if ((hdc==NULL)||(hBitmap==NULL)) return NULL;

	BITMAP     bm;
	HBITMAP    hbm;
	HDC        hdcTemp, hdcOut;
	RECT rcClipped;

	rcClipped=rcRect;

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	hdcTemp = CreateCompatibleDC(hdc);
	hdcOut = CreateCompatibleDC(hdc);

	SetMapMode(hdcTemp, GetMapMode(hdc));
	SetMapMode(hdcOut, GetMapMode(hdc));
	
	if (rcClipped.right>bm.bmWidth) {rcClipped.right=bm.bmWidth; }
	if (rcClipped.left>bm.bmWidth) {rcClipped.left=bm.bmWidth;}
	if (rcClipped.right<0) {rcClipped.right=0;}
	if (rcClipped.left<0) {rcClipped.left=0; }
	if (rcClipped.bottom>bm.bmHeight) {rcClipped.bottom=bm.bmHeight; }
	if (rcClipped.top>bm.bmHeight) {rcClipped.top=bm.bmHeight; }
	if (rcClipped.bottom<0) {rcClipped.bottom=0;}
	if (rcClipped.top<0) {rcClipped.top=0; }

	SelectObject(hdcTemp, hBitmap);   // Select the bitmap

/*
	SIZE size;
	size.cx = abs(rcRect.right-rcRect.left);
	size.cy = abs(rcRect.bottom-rcRect.top);
*/

	if (cBG==CLR_INVALID)
	{
		if (((rcClipped.right-rcClipped.left)<=0)||((rcClipped.bottom-rcClipped.top)<=0)) return NULL;
		hbm = ColorBitmap(hdc, rcClipped,cBG);
		SelectObject(hdcOut,hbm);
		BitBlt(hdcOut, 0 ,0,(rcClipped.right-rcClipped.left), (rcClipped.bottom-rcClipped.top), hdcTemp, rcClipped.left, rcClipped.top, SRCCOPY);
	}
	else
	{
		if (((rcRect.right-rcRect.left)<=0)||((rcRect.bottom-rcRect.top)<=0)) return NULL;
		hbm = ColorBitmap(hdc, rcRect,cBG);
		SelectObject(hdcOut,hbm);
		BitBlt(hdcOut, rcClipped.left-rcRect.left ,rcClipped.top-rcRect.top,(rcClipped.right-rcClipped.left), (rcClipped.bottom-rcClipped.top), 
			hdcTemp, rcClipped.left, rcClipped.top, SRCCOPY);
	}

	// clean it up.
	DeleteDC(hdcTemp);
	DeleteDC(hdcOut);
	return hbm;
}

HBITMAP CBmpUtil::SubsetBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, POINT ptTopLeft, COLORREF cBG)
{
	RECT rc;
	GetClientRect(hWnd, &rc);
	rc.left+=ptTopLeft.x; rc.right+=ptTopLeft.x;  rc.top+=ptTopLeft.y; rc.bottom+=ptTopLeft.y;
	return SubsetBitmap(hdc, hBitmap, rc, cBG);
}

HBITMAP CBmpUtil::SubsetBitmap(HDC hdc, HBITMAP hBitmap, SIZE size, POINT ptTopLeft,COLORREF cBG) 
{ 
	RECT rc;  
	rc.left =   ptTopLeft.x;
	rc.top =    ptTopLeft.y;
	rc.right =  ptTopLeft.x+size.cx;
	rc.bottom = ptTopLeft.y+size.cy;
	return SubsetBitmap(hdc, hBitmap, rc, cBG);
}

HBITMAP CBmpUtil::SubsetBitmap(HDC hdc, HBITMAP hBitmap, POINT ptTopLeft, POINT ptBottomRight,COLORREF cBG) 
{ 
	RECT rc;  
	rc.left =   ptTopLeft.x;
	rc.top =    ptTopLeft.y;
	rc.right =  ptBottomRight.x;
	rc.bottom = ptBottomRight.y;
	return SubsetBitmap(hdc, hBitmap, rc, cBG);
}

/////////////
//  PutRectBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap 
//	rcRect, the coords defining a rectangle inside the bitmap, indicating which
//    part of the source bitmap will be filled in with cColor.  
//    Any rectangle that exceeds the bounds of the source bitmap will be truncated to the
//    source bitmap edge.  
//    or, rcRect may be defined using certain pairs of the following arguments:
//  hWnd, a pointer to a window whose client area will define the size of rcRect,
//  size, a SIZE object whose members describe the width and height of rcRect, 
//  ptTopLeft, a point which will define the relational location of the top left corner 
//    of rcRect on the source bitmap.  Identical rules for the use of cBG apply.
//  ptBottomRight, a point which will define the relational location of the bottom right 
//    corner of rcRect on the source bitmap.  
//	cColor, the color of the rectangle.  If CLR_INVALID, the COLOR_3DFACE color is used.

//  PutRectBitmap returns a handle to a bitmap which contains rectangle of the 
//    specified color on top of the source bitmap, or the original bitmap if the
//    rectangle is completely out of bounds (will clip the rectangle if partially out),
//    NULL if operation is unsuccessful
//
HBITMAP CBmpUtil::PutRectBitmap(HDC hdc, HBITMAP hBitmap, RECT rcRect, COLORREF cColor)
{
	if ((hdc==NULL)||(hBitmap==NULL)) return NULL;

	BITMAP     bm;
	HBITMAP    hbm;
	int x,y;
	if (rcRect.left<0) x=-rcRect.left; else x=0;
	if (rcRect.top<0) y=-rcRect.top; else y=0;

	POINT pt;
	pt.x = rcRect.left;
	pt.y = rcRect.top;

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	RECT rc;  
	rc.left =   x;
	rc.top =    y;
	rc.right =  x+bm.bmWidth;
	rc.bottom = y+bm.bmHeight;

	hbm = SubsetBitmap(hdc, 
		SupersetBitmap(hdc,hBitmap,ColorBitmap(hdc, rcRect,cColor),pt,cColor),
		rc,cColor);
	return hbm;
}

HBITMAP CBmpUtil::PutRectBitmap(HDC hdc, HBITMAP hBitmap, HWND hWnd, POINT ptTopLeft, COLORREF cColor)
{
	RECT rc;
	GetClientRect(hWnd, &rc);
	rc.left+=ptTopLeft.x; rc.right+=ptTopLeft.x;  rc.top+=ptTopLeft.y; rc.bottom+=ptTopLeft.y;
	return PutRectBitmap(hdc, hBitmap, rc, cColor);
}

HBITMAP CBmpUtil::PutRectBitmap(HDC hdc, HBITMAP hBitmap, SIZE size, POINT ptTopLeft,COLORREF cColor) 
{
	RECT rc;  
	rc.left =   ptTopLeft.x;
	rc.top =    ptTopLeft.y;
	rc.right =  ptTopLeft.x+size.cx;
	rc.bottom = ptTopLeft.y+size.cy;

	return PutRectBitmap(hdc, hBitmap, rc, cColor);
}

HBITMAP CBmpUtil::PutRectBitmap(HDC hdc, HBITMAP hBitmap, POINT ptTopLeft, POINT ptBottomRight,COLORREF cColor) 
{ 
	RECT rc;  
	rc.left =   ptTopLeft.x;
	rc.top =    ptTopLeft.y;
	rc.right =  ptBottomRight.x;
	rc.bottom = ptBottomRight.y;
	return PutRectBitmap(hdc, hBitmap, rc, cColor);
}

/////////////
//  ReturnTransparentBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that has one color in it that will be the
//    "transparent color"
//  hBitmapBG, a handle to a source bitmap that will be the background image
//	rcRect, the rectangle on the hdc that a background image wil be taken from,
//	pt, the x and y coords of where on the HDC the "background image" is taken,
//    over which the source pic will be placed with transparency, or
//    the point relative to the source background image where the top left corner 
//    of the source bitmap will be placed.
//		omitting pt will assume 0,0
//  cTransparentColor, the colorref of the "transparent color".  Pixels of this color
//		in the source bitmap will be replaced by whatever is originally in the hdc, at 
//    the pixel location offset by the pt coordinates.

//  ReturnTransparentBitmap returns a handle to a bitmap which the source bitmap, on top of
//    the contents of hdc at point pt, with transparency,
//    NULL if operation is unsuccessful or if cTransparentColor is CLR_INVALID.
//
HBITMAP CBmpUtil::ReturnTransparentBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor, RECT rcRect, POINT pt  )
{
	HDC hdcTemp;
	HBITMAP hbm,hbmOldTemp;
	BOOL bResult=TRUE;

	hdcTemp = CreateCompatibleDC(hdc);
	if (hdcTemp==NULL) return NULL;
	SetMapMode(hdcTemp, GetMapMode(hdc));

	hbm = CreateCompatibleBitmap(hdc, abs(rcRect.right-rcRect.left), abs(rcRect.bottom-rcRect.top));
	if (hbm==NULL) return NULL;

	hbmOldTemp = (HBITMAP)SelectObject(hdcTemp,hbm);
	BitBlt(hdcTemp, 0,0, abs(rcRect.right-rcRect.left), abs(rcRect.bottom-rcRect.top), hdc, rcRect.left, rcRect.top, SRCCOPY);
	bResult = TransparentBitmap(hdcTemp, hBitmap, cTransparentColor, pt);

//	DeleteObject(SelectObject(hdcTemp, hbmOldTemp));

	DeleteDC(hdcTemp);
	if(bResult)
		return hbm;
	else 
		return NULL;
}

HBITMAP CBmpUtil::ReturnTransparentBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor, POINT pt )
{
  BITMAP     bm;
  GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);
	RECT rcRect;
	rcRect.left = pt.x; rcRect.top=pt.y;
	rcRect.right = pt.x+bm.bmWidth; rcRect.bottom=pt.y+bm.bmHeight;
	return	ReturnTransparentBitmap(hdc, hBitmap, cTransparentColor, rcRect, ptNULL  );
}

HBITMAP CBmpUtil::ReturnTransparentBitmap(HDC hdc, HBITMAP hBitmap, HBITMAP hBitmapBG, COLORREF cTransparentColor, POINT pt )
{
	HDC hdcTemp,hdcBG;
	HBITMAP hbm,hbmOldBG,hbmOldTemp;
  BITMAP     bm;
	BOOL bResult=TRUE;

	hdcTemp = CreateCompatibleDC(hdc);
	hdcBG = CreateCompatibleDC(hdc);
	if ((hdcTemp==NULL)||(hdcBG==NULL)) return NULL;
	SetMapMode(hdcTemp, GetMapMode(hdc));
	SetMapMode(hdcBG, GetMapMode(hdc));
  hbmOldBG = (HBITMAP)SelectObject(hdcBG, hBitmapBG);   // Select the bitmap

  GetObject(hBitmapBG, sizeof(BITMAP), (LPSTR)&bm);

	hbm = CreateCompatibleBitmap(hdc, bm.bmWidth, bm.bmHeight);
	if (hbm==NULL) return NULL;

	hbmOldTemp = (HBITMAP)SelectObject(hdcTemp,hbm);
//	SelectObject(hdcBG,hBitmapBG);
	BitBlt(hdcTemp, 0, 0, bm.bmWidth, bm.bmHeight, hdcBG, 0, 0, SRCCOPY);
	bResult = TransparentBitmap(hdcTemp, hBitmap, cTransparentColor, pt);

//	DeleteObject(SelectObject(hdcTemp, hbmOldTemp));
//	DeleteObject(SelectObject(hdcBG, hBitmapBG));

	DeleteDC(hdcTemp);
	DeleteDC(hdcBG);
	if(bResult)
		return hbm;
	else 
		return NULL;
}

/////////////
//  TransparentBitmap takes as arguments:
//  hdc, a specific device context.
//  hBitmap, a handle to a source bitmap that has one color in it that will be the
//    "transparent color"
//	pt, the x and y coords of where on the HDC the source pic will be blitted.
//		omitting pt will assume 0,0.     
//  cTransparentColor, the colorref of the "transparent color".  Pixels of this color
//		in the source bitmap will be replaced by whatever is originally in the hdc, at 
//    the pixel location offset by the pt coordinates.

//  TransparentBitmap returns TRUE if things supposedly go OK, FALSE if not.


BOOL CBmpUtil::TransparentBitmap(HDC hdc, HBITMAP hBitmap, COLORREF cTransparentColor, POINT pt)
{

	if((cTransparentColor==CLR_INVALID)||
			(hBitmap==NULL)||
			(hdc==NULL))  return FALSE;

	BITMAP     bm;
	COLORREF   cColor;
	HBITMAP    hbmAndBack, hbmAndObject, hbmAndMem, hbmSave;
	HBITMAP    hbmBackOld, hbmObjectOld, hbmMemOld, hbmSaveOld;
	HDC        hdcMem, hdcBack, hdcObject, hdcTemp, hdcSave;
	POINT      ptSize;


	hdcTemp = CreateCompatibleDC(hdc);
	SelectObject(hdcTemp, hBitmap);   // Select the bitmap

	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

	ptSize.x = bm.bmWidth;            // Get width of bitmap
	ptSize.y = bm.bmHeight;           // Get height of bitmap
	DPtoLP(hdcTemp, &ptSize, 1);      // Convert from device
																	 // to logical points

	// Create some DCs to hold temporary data.
	hdcBack   = CreateCompatibleDC(hdc);
	hdcObject = CreateCompatibleDC(hdc);
	hdcMem    = CreateCompatibleDC(hdc);
	hdcSave   = CreateCompatibleDC(hdc);

	// Create a bitmap for each DC. DCs are required for a number of
	// GDI functions.

	// Monochrome DC

	hbmAndBack   = CreateBitmap(ptSize.x, ptSize.y, 1, 1, NULL);

	// Monochrome DC
	hbmAndObject = CreateBitmap(ptSize.x, ptSize.y, 1, 1, NULL);

	hbmAndMem    = CreateCompatibleBitmap(hdc, ptSize.x, ptSize.y);
	hbmSave      = CreateCompatibleBitmap(hdc, ptSize.x, ptSize.y);

	// Each DC must select a bitmap object to store pixel data.
	hbmBackOld   = (HBITMAP) SelectObject(hdcBack, hbmAndBack);
	hbmObjectOld = (HBITMAP) SelectObject(hdcObject, hbmAndObject);
	hbmMemOld    = (HBITMAP) SelectObject(hdcMem, hbmAndMem);
	hbmSaveOld   = (HBITMAP) SelectObject(hdcSave, hbmSave);

	// Set proper mapping mode.
	SetMapMode(hdcTemp, GetMapMode(hdc));

	// Save the bitmap sent here, because it will be overwritten.
	BitBlt(hdcSave, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);

	// Set the background color of the source DC to the color.
	// contained in the parts of the bitmap that should be transparent
	cColor = SetBkColor(hdcTemp, cTransparentColor);

	// Create the object mask for the bitmap by performing a BitBlt()
	// from the source bitmap to a monochrome bitmap.
	BitBlt(hdcObject, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0,
				SRCCOPY);

	// Set the background color of the source DC back to the original
	// color.
	SetBkColor(hdcTemp, cColor);

	// Create the inverse of the object mask.
	BitBlt(hdcBack, 0, 0, ptSize.x, ptSize.y, hdcObject, 0, 0,
				NOTSRCCOPY);

	// Copy the background of the main DC to the destination.
	BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdc, pt.x, pt.y,
				SRCCOPY);

	// Mask out the places where the bitmap will be placed.
	BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcObject, 0, 0, SRCAND);

	// Mask out the transparent colored pixels on the bitmap.
	BitBlt(hdcTemp, 0, 0, ptSize.x, ptSize.y, hdcBack, 0, 0, SRCAND);

	// XOR the bitmap with the background on the destination DC.
	BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCPAINT);

	// Copy the destination to the inputted device context.
	BitBlt(hdc, pt.x, pt.y, ptSize.x, ptSize.y, hdcMem, 0, 0,
				SRCCOPY);

	// Place the original bitmap back into the bitmap sent here.
	BitBlt(hdcTemp, 0, 0, ptSize.x, ptSize.y, hdcSave, 0, 0, SRCCOPY);

	// Delete the memory bitmaps.
	
	DeleteObject(SelectObject(hdcBack, hbmBackOld));
	DeleteObject(SelectObject(hdcObject, hbmObjectOld));
	DeleteObject(SelectObject(hdcMem, hbmMemOld));
	DeleteObject(SelectObject(hdcSave, hbmSaveOld));
	
	// Delete the memory DCs.
	DeleteDC(hdcMem);
	DeleteDC(hdcBack);
	DeleteDC(hdcObject);
	DeleteDC(hdcSave);
	DeleteDC(hdcTemp);

	return TRUE;  //gotta add error handling eventually...
}      

COLORREF CBmpUtil::GetTopLeftColor(HDC hdc, HBITMAP hBitmap)
{
	HDC hdcTemp;
	COLORREF cReturn;
	HBITMAP hbmOld;
	hdcTemp = CreateCompatibleDC(hdc);
	hbmOld = (HBITMAP)SelectObject(hdcTemp, hBitmap);   // Select the bitmap
	cReturn = GetPixel(hdcTemp,0,0);
//	DeleteObject(SelectObject(hdcTemp, hbmOld));
	DeleteDC(hdcTemp);

	return cReturn;
}

