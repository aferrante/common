// 
// CALErrors.h
//
#ifndef _CALERRORS_H
#define _CALERRORS_H

#include <winerror.h>
#ifndef __MIDL__
#include <ole2.h>
#endif

#ifdef _CALNS_
namespace CAL {
#endif

// Define the reserved facility bits
#define FACILITY_RESERVED_BIT	0x40000000
#define FACILITY_CUSTOMER_BIT	0x20000000
#define FACILITY_NT_BIT			0x10000000

// Define the facility codes
// These will overlap with NT facilities, but since the
// customer bit will be set there shouldn't be a problem.
#define FACILITY_CAL	1

typedef long ErrorCode;

#ifdef _TCL_


#define __CE(e, d) (0xa0010000 | e | d)

#else

// Map a CAL error value into an HRESULT
#define CALERROR(err)	(MAKE_HRESULT(SEVERITY_ERROR, FACILITY_CAL, err) | FACILITY_CUSTOMER_BIT)

// CAL Error Codes
// Error Codes are COM Style
#define __CE( e, d ) (CALERROR( e ) | d )

#endif

typedef enum ecalErrCodes {

	//------  Connection/Configuration Error Section ------
	ecalErrConnectionSection		= __CE(0x1000, 0),

	//                 Connect Error Group                   
	ecalErrConnectGroup				= __CE(ecalErrConnectionSection, 0x100),
	ecalErrNoAvailBoard				= __CE(ecalErrConnectGroup, 1),
	ecalErrNoAvailOutput			= __CE(ecalErrConnectGroup, 2),
	ecalErrInvalidBoard				= __CE(ecalErrConnectGroup, 3),
	ecalErrInvalidOutput			= __CE(ecalErrConnectGroup, 4),
	ecalErrInvalidFormat			= __CE(ecalErrConnectGroup, 5),
	ecalErrInvalidConnection		= __CE(ecalErrConnectGroup, 6),
	ecalErrInvalidBuffer			= __CE(ecalErrConnectGroup, 7),
	ecalErrConnectException			= __CE(ecalErrConnectGroup, 8),
	ecalErrDisconnectException		= __CE(ecalErrConnectGroup, 9),
	ecalErrInvalidClientVersion		= __CE(ecalErrConnectGroup, 10),

	//               VideoDefaults Error Group               
	ecalErrVideoDefGroup			= __CE(ecalErrConnectionSection, 0x200),
	ecalErrInvalidOption			= __CE(ecalErrVideoDefGroup, 1),
	ecalErrInvalidVideoMode			= __CE(ecalErrVideoDefGroup, 2),
	ecalErrInvalidRefSyncParam		= __CE(ecalErrVideoDefGroup, 3),
	ecalErrInvalidMixParam			= __CE(ecalErrVideoDefGroup, 4),
	ecalErrInvalidAlphaShape		= __CE(ecalErrVideoDefGroup, 5),
	ecalErrInvalidAncillaryData		= __CE(ecalErrVideoDefGroup, 6),
	ecalErrInvalidTimeCodeParam		= __CE(ecalErrVideoDefGroup, 7),
	ecalErrInvalidBP2_3Param		= __CE(ecalErrVideoDefGroup, 8),
	ecalErrInvalidBP4_5Param		= __CE(ecalErrVideoDefGroup, 9),
	ecalErrInvalidBP6_7Param		= __CE(ecalErrVideoDefGroup,10),
	ecalErrInvalidKeyInputParam		= __CE(ecalErrVideoDefGroup,11),
	ecalErrInvalidHDelayParam		= __CE(ecalErrVideoDefGroup,12),
	ecalErrInvalidVDelayParam		= __CE(ecalErrVideoDefGroup,13),

	//---------- Status/Information Error Section ---------
	ecalErrStatusInfoSection		= __CE(0x2000, 0),

	//                   Info Error Group                   
	ecalErrInfoGroup				= __CE(ecalErrStatusInfoSection, 0x100),
	ecalErrInvalidStruct			= __CE(ecalErrInfoGroup, 1),
	ecalErrInvalidInfoType			= __CE(ecalErrInfoGroup, 2),
	ecalErrGetNetInfo           	= __CE(ecalErrInfoGroup, 3),
	ecalErrCannotGetRgnInfo			= __CE(ecalErrInfoGroup, 4),

	//                 Name/ID Error Group                   
	ecalErrNameGroup				= __CE(ecalErrStatusInfoSection, 0x200),
	ecalErrNameExists				= __CE(ecalErrNameGroup, 1),
	ecalErrUnrecogName				= __CE(ecalErrNameGroup, 2),

	//---------- Primitives/Regions Error Section ----------
	ecalErrPrimitivesSection		= __CE(0x3000, 0),

	//                Background Error Group                

	//                  Object Error Group                  
	ecalErrObjectGroup				= __CE(ecalErrPrimitivesSection, 0x100),
	ecalErrInvalidRefPt				= __CE(ecalErrObjectGroup, 1),
	ecalErrInvalidXExt				= __CE(ecalErrObjectGroup, 2),
	ecalErrInvalidYExt				= __CE(ecalErrObjectGroup, 3),
	ecalErrInvalidZExt				= __CE(ecalErrObjectGroup, 4),
	ecalErrNullObject				= __CE(ecalErrObjectGroup, 5),
	ecalErrNullObjectData			= __CE(ecalErrObjectGroup, 6),
	ecalErrInvalidObject			= __CE(ecalErrObjectGroup, 7),
	ecalErrCannotSetObjectName		= __CE(ecalErrObjectGroup, 8),
	ecalErrCannotReplaceObject		= __CE(ecalErrObjectGroup, 9),
	ecalErrCannotInsertObject		= __CE(ecalErrObjectGroup, 10),



	//                   Image Error Group                   
	ecalErrImageGroup				= __CE(ecalErrPrimitivesSection, 0x200),
	ecalErrInvalidPixelDepth		= __CE(ecalErrImageGroup, 1),
	ecalErrImageDevice				= __CE(ecalErrImageGroup, 2),
	ecalErrImageDeviceEx			= __CE(ecalErrImageGroup, 3),
	ecalErrImageMemory				= __CE(ecalErrImageGroup, 4),
	ecalErrImageFlush				= __CE(ecalErrImageGroup, 5),

	//                   Group Error Group                   
	ecalErrGroupGroup				= __CE(ecalErrPrimitivesSection, 0x300),
	ecalErrInvalidGroup				= __CE(ecalErrGroupGroup, 1),
	ecalErrInvalidChildren			= __CE(ecalErrGroupGroup, 2),
	ecalErrHasParent				= __CE(ecalErrGroupGroup, 3),
	ecalErrInvalidSlot				= __CE(ecalErrGroupGroup, 4),
	ecalErrOutOfSlots				= __CE(ecalErrGroupGroup, 5),
	ecalErrSlotOutOfRange			= __CE(ecalErrGroupGroup, 6),
	ecalErrSlotInUse				= __CE(ecalErrGroupGroup, 7),
	ecalErrHasNoParent				= __CE(ecalErrGroupGroup, 8),


	//                 Attribute Error Group                   
	ecalErrAttrGroup				= __CE(ecalErrPrimitivesSection, 0x400),
	ecalErrRegionWrongType			= __CE(ecalErrAttrGroup, 1),
	ecalErrInvalidJustify			= __CE(ecalErrAttrGroup, 2),
	ecalErrInvalidHMSFormat			= __CE(ecalErrAttrGroup, 3),
	ecalErrInvalidRegionSize    	= __CE(ecalErrAttrGroup, 4),
	ecalErrInvalidScaleMode     	= __CE(ecalErrAttrGroup, 5),
	ecalErrInvalidBGDirection		= __CE(ecalErrAttrGroup, 6),
	ecalErrInvalidWrapMode  		= __CE(ecalErrAttrGroup, 7),


	//------------- Font/Color Error Section ----------------
	ecalErrFontColorSection				= __CE(0x4000, 0),

	//                   Font Error Group                   
	ecalErrFontGroup				= __CE(ecalErrFontColorSection, 0x100),
	ecalErrFontCacheFull			= __CE(ecalErrFontGroup, 1),
	ecalErrUnknownFont				= __CE(ecalErrFontGroup, 2),
	ecalErrNullFont					= __CE(ecalErrFontGroup, 3),
	ecalErrFontDeletion				= __CE(ecalErrFontGroup, 4),
	ecalErrInvalidFontInfo			= __CE(ecalErrFontGroup, 5),
	ecalErrUnrecognizedFont			= __CE(ecalErrFontGroup, 6),
	ecalErrInvalidFontHeight		= __CE(ecalErrFontGroup, 7),
	ecalErrCannotCreateFont			= __CE(ecalErrFontGroup, 8),
	ecalErrCannotCacheChar			= __CE(ecalErrFontGroup, 9),
	ecalErrUncachedChar				= __CE(ecalErrFontGroup, 10),

	//                 Font Options / Text Options Group                 
	ecalErrFontOptionsGroup			= __CE(ecalErrFontColorSection, 0x200),
	ecalErrInvalidFontIndex			= __CE(ecalErrFontOptionsGroup, 1),
	ecalErrInvalidShear				= __CE(ecalErrFontOptionsGroup, 2),
	ecalErrInvalidKerning			= __CE(ecalErrFontOptionsGroup, 3),
	ecalErrInvalidLeading			= __CE(ecalErrFontOptionsGroup, 4),
	ecalErrInvalidCharWidth			= __CE(ecalErrFontOptionsGroup, 5),
	ecalErrInvalidAngle 			= __CE(ecalErrFontOptionsGroup, 6),

	//               Font Edge Effects Group                 
	ecalErrFontEffectsGroup			= __CE(ecalErrFontColorSection, 0x300),
	ecalErrInvalidEdgeType			= __CE(ecalErrFontEffectsGroup, 1),
	ecalErrInvalidEdgeSize			= __CE(ecalErrFontEffectsGroup, 2),
	ecalErrSoftnessOutOfRange		= __CE(ecalErrFontEffectsGroup, 3),
	ecalErrCannotAddEdge			= __CE(ecalErrFontEffectsGroup, 4),

	//                   Color Error Group                   
	ecalErrColorGroup				= __CE(ecalErrFontColorSection, 0x400),
	ecalErrColorIndexInvalid		= __CE(ecalErrColorGroup, 1),
	ecalErrColorIndexWrongType		= __CE(ecalErrColorGroup, 2),
	ecalErrColorInvalidRGBA			= __CE(ecalErrColorGroup, 3),
	ecalErrNullColor				= __CE(ecalErrColorGroup, 4),


	//---------------- Update Error Section ----------------
	ecalErrUpdateSection			= __CE(0x5000, 0),

	//                  Update Error Group                  
	ecalErrUpdateGroup				= __CE(ecalErrUpdateSection, 0x100),
	ecalErrInvalidRegionType		= __CE(ecalErrUpdateGroup, 1),
	ecalErrBadClusterName			= __CE(ecalErrUpdateGroup, 2),
	ecalErrUpdatesSuspended			= __CE(ecalErrUpdateGroup, 3),
	ecalErrDataNotCached			= __CE(ecalErrUpdateGroup, 4),


	//-------------- Transition Error Section --------------
	ecalErrTransitionSection		= __CE(0x6000, 0),

	//                Transition Error Group                
	ecalErrTransitionGroup			= __CE(ecalErrTransitionSection, 0x100),
	ecalErrInvalidTransition		= __CE(ecalErrTransitionGroup, 1),
	ecalErrInvalidEaseMode			= __CE(ecalErrTransitionGroup, 2),
	ecalErrInvalidPattern			= __CE(ecalErrTransitionGroup, 3),
	ecalErrNotATransition			= __CE(ecalErrTransitionGroup, 4),
	ecalErrInvalidDirection			= __CE(ecalErrTransitionGroup, 5),
	ecalErrInvalidTransOption		= __CE(ecalErrTransitionGroup, 6),
	

	//---------------- View/Animation Error Section ----------------
	ecalErrViewingSection			= __CE(0x7000, 0),

	//--------------------- View Error Group ----------------------
	ecalErrViewGroup				= __CE(ecalErrViewingSection, 0x100),
	ecalErrInvalidCameraPos			= __CE(ecalErrViewGroup, 1),
	ecalErrInvalidCameraAngle		= __CE(ecalErrViewGroup, 2),
	ecalErrInvalidKeyframes 		= __CE(ecalErrViewGroup, 3),
	ecalErrInvalidKeyValues 		= __CE(ecalErrViewGroup, 4),
	ecalErrTooFewKeyframes			= __CE(ecalErrViewGroup, 5),
	ecalErrTooFewKeyValues			= __CE(ecalErrViewGroup, 6),
	ecalErrKeyframeOutOfRange		= __CE(ecalErrViewGroup, 7),
	ecalErrInvalidViewSpec			= __CE(ecalErrViewGroup, 8),
	
	//----------------- Attrimation Error Group ------------------
	ecalErrAttrimationGroup			= __CE(ecalErrViewingSection, 0x200),
	ecalErrInvalidAttrimation		= __CE(ecalErrAttrimationGroup, 1),
	ecalErrCannotAssignCompiler		= __CE(ecalErrAttrimationGroup, 2),
	ecalErrNullTransform			= __CE(ecalErrAttrimationGroup, 3),
	ecalErrNullAttrimation			= __CE(ecalErrAttrimationGroup, 4),
	ecalErrNullAnimationState		= __CE(ecalErrAttrimationGroup, 5),
	ecalErrPreAnimationState		= __CE(ecalErrAttrimationGroup, 6),

	//----------------- Lighting Error Group ----------------
	ecalErrLightingGroup			= __CE(ecalErrViewingSection, 0x300),
	ecalErrInvalidLightPos			= __CE(ecalErrLightingGroup, 1),
	ecalErrInvalidLightDir			= __CE(ecalErrLightingGroup, 2),
	ecalErrLightNotAdded			= __CE(ecalErrLightingGroup, 3),
	ecalErrInvalidLightIndex		= __CE(ecalErrLightingGroup, 4),


	
	//--------------- Playback Error Section ----------------
	ecalErrPlaybackSection			= __CE(0x8000, 0),

	//                  Frame Error Group                
	ecalErrFrameGroup				= __CE(ecalErrPlaybackSection, 0x100),
	ecalErrFrameInvalid				= __CE(ecalErrFrameGroup, 1),

	//                  Play Error Group                
	ecalErrPlayGroup				= __CE(ecalErrPlaybackSection, 0x200),
	ecalErrObjectPlaying			= __CE(ecalErrPlayGroup, 1),

	//--------------- Resource Error Section ----------------
	ecalErrResourceSection			= __CE(0x9000, 0),

	//                  Memory Error Group                  
	ecalErrMemoryGroup				= __CE(ecalErrResourceSection, 0x100),
	ecalErrOutOfMemory				= __CE(ecalErrMemoryGroup, 1),
	ecalErrOverflow					= __CE(ecalErrMemoryGroup, 2),

	//------------------ I/O Error Section ------------------
	ecalErrIOSection				= __CE(0xA000, 0),

	//                   File Error Group                   
	ecalErrFileGroup				= __CE(ecalErrIOSection, 0x100),
	ecalErrHostNotFound				= __CE(ecalErrFileGroup, 1),
	ecalErrDriveNotFound			= __CE(ecalErrFileGroup, 2),
	ecalErrPathNotFound				= __CE(ecalErrFileGroup, 3),
	ecalErrFileNotFound				= __CE(ecalErrFileGroup, 4),
	ecalErrAccessDenied				= __CE(ecalErrFileGroup, 5),
	ecalErrInvalidFileFormat		= __CE(ecalErrFileGroup, 6),
	ecalErrInvalidFileType			= __CE(ecalErrFileGroup, 7),
	ecalErrLangFilename				= __CE(ecalErrFileGroup, 8),
	ecalErrFilenameTooLong			= __CE(ecalErrFileGroup, 9),

	//                   Import Error Group                   
	ecalErrImportGroup				= __CE(ecalErrIOSection, 0x200),
	ecalErrUnsupportedOLEHeader		= __CE(ecalErrImportGroup, 1),

	//                   Export Error Group                   
	ecalErrExportGroup				= __CE(ecalErrIOSection, 0x300),
	ecalErrExportInit				= __CE(ecalErrExportGroup, 1),
	ecalErrExportCreate				= __CE(ecalErrExportGroup, 2),
	ecalErrExportOpen				= __CE(ecalErrExportGroup, 3),
	ecalErrExportRead				= __CE(ecalErrExportGroup, 4),
	ecalErrExportWrite				= __CE(ecalErrExportGroup, 5),
	ecalErrExportActive				= __CE(ecalErrExportGroup, 6),
	ecalErrExportClose				= __CE(ecalErrExportGroup, 7),
	ecalErrExportFileFormat			= __CE(ecalErrExportGroup, 8),
	ecalErrExportNameFormat			= __CE(ecalErrExportGroup, 9),
	ecalErrExportUnknown			= __CE(ecalErrExportGroup, 10),
	
	//                   GPIO Error Group                   
	ecalErrGPIOGroup				= __CE(ecalErrIOSection, 0x400),
	ecalErrGPIONoHardware			= __CE(ecalErrGPIOGroup, 1),
	ecalErrGPIONoLibrary			= __CE(ecalErrGPIOGroup, 2),
	ecalErrGPIOInvalidLibrary		= __CE(ecalErrGPIOGroup, 3),

	//                  Socket Error Group
	ecalErrSocketGroup				= __CE(ecalErrIOSection, 0x500),
	ecalErrSocketWrite				= __CE(ecalErrSocketGroup, 1),
	ecalErrSocketRead				= __CE(ecalErrSocketGroup, 2),
	ecalErrSocketUserTimeout		= __CE(ecalErrSocketGroup, 3),
	ecalErrSocketInvalidPort		= __CE(ecalErrSocketGroup, 4),
	ecalErrSocketEINTR				= __CE(ecalErrSocketGroup, 100),
	ecalErrSocketEBADF				= __CE(ecalErrSocketGroup, 101),
	ecalErrSocketEACCES				= __CE(ecalErrSocketGroup, 102),
	ecalErrSocketEFAULT				= __CE(ecalErrSocketGroup, 103),
	ecalErrSocketEINVAL				= __CE(ecalErrSocketGroup, 104),
	ecalErrSocketEMFILE				= __CE(ecalErrSocketGroup, 105),
	ecalErrSocketEWOULDBLOCK		= __CE(ecalErrSocketGroup, 106),
	ecalErrSocketEINPROGRESS		= __CE(ecalErrSocketGroup, 107),
	ecalErrSocketEALREADY			= __CE(ecalErrSocketGroup, 108),
	ecalErrSocketENOTSOCK			= __CE(ecalErrSocketGroup, 109),
	ecalErrSocketEDESTADDRREQ		= __CE(ecalErrSocketGroup, 110),
	ecalErrSocketEMSGSIZE			= __CE(ecalErrSocketGroup, 111),
	ecalErrSocketEPROTOTYPE			= __CE(ecalErrSocketGroup, 112),
	ecalErrSocketENOPROTOOPT		= __CE(ecalErrSocketGroup, 113),
	ecalErrSocketEPROTONOSUPPORT	= __CE(ecalErrSocketGroup, 114),
	ecalErrSocketESOCKTNOSUPPORT	= __CE(ecalErrSocketGroup, 115),
	ecalErrSocketEOPNOTSUPP			= __CE(ecalErrSocketGroup, 116),
	ecalErrSocketEPFNOSUPPORT		= __CE(ecalErrSocketGroup, 117),
	ecalErrSocketEAFNOSUPPORT		= __CE(ecalErrSocketGroup, 118),
	ecalErrSocketEADDRINUSE			= __CE(ecalErrSocketGroup, 119),
	ecalErrSocketEADDRNOTAVAIL		= __CE(ecalErrSocketGroup, 120),
	ecalErrSocketENETDOWN			= __CE(ecalErrSocketGroup, 121),
	ecalErrSocketENETUNREACH		= __CE(ecalErrSocketGroup, 122),
	ecalErrSocketENETRESET			= __CE(ecalErrSocketGroup, 123),
	ecalErrSocketECONNABORTED		= __CE(ecalErrSocketGroup, 124),
	ecalErrSocketECONNRESET			= __CE(ecalErrSocketGroup, 125),
	ecalErrSocketENOBUFS			= __CE(ecalErrSocketGroup, 126),
	ecalErrSocketEISCONN			= __CE(ecalErrSocketGroup, 127),
	ecalErrSocketENOTCONN			= __CE(ecalErrSocketGroup, 128),
	ecalErrSocketESHUTDOWN			= __CE(ecalErrSocketGroup, 129),
	ecalErrSocketETOOMANYREFS		= __CE(ecalErrSocketGroup, 130),
	ecalErrSocketETIMEDOUT			= __CE(ecalErrSocketGroup, 131),
	ecalErrSocketECONNREFUSED		= __CE(ecalErrSocketGroup, 132),
	ecalErrSocketELOOP				= __CE(ecalErrSocketGroup, 133),
	ecalErrSocketENAMETOOLONG		= __CE(ecalErrSocketGroup, 134),
	ecalErrSocketEHOSTDOWN			= __CE(ecalErrSocketGroup, 135),
	ecalErrSocketEHOSTUNREACH		= __CE(ecalErrSocketGroup, 136),
	ecalErrSocketENOTEMPTY			= __CE(ecalErrSocketGroup, 137),
	ecalErrSocketEPROCLIM			= __CE(ecalErrSocketGroup, 138),
	ecalErrSocketEUSERS				= __CE(ecalErrSocketGroup, 139),
	ecalErrSocketEDQUOT				= __CE(ecalErrSocketGroup, 140),
	ecalErrSocketESTALE				= __CE(ecalErrSocketGroup, 141),
	ecalErrSocketEREMOTE			= __CE(ecalErrSocketGroup, 142),
	ecalErrSocketSYSNOTREADY		= __CE(ecalErrSocketGroup, 143),
	ecalErrSocketVERNOTSUPPORTED	= __CE(ecalErrSocketGroup, 144),
	ecalErrSocketNOTINITIALISED		= __CE(ecalErrSocketGroup, 145),
	ecalErrSocketEDISCON			= __CE(ecalErrSocketGroup, 146),
	ecalErrSocketENOMORE			= __CE(ecalErrSocketGroup, 147),
	ecalErrSocketECANCELLED			= __CE(ecalErrSocketGroup, 148),
	ecalErrSocketEINVALIDPROCTABLE	= __CE(ecalErrSocketGroup, 149),
	ecalErrSocketEINVALIDPROVIDER	= __CE(ecalErrSocketGroup, 150),
	ecalErrSocketEPROVIDERFAILEDINIT= __CE(ecalErrSocketGroup, 151),
	ecalErrSocketSYSCALLFAILURE		= __CE(ecalErrSocketGroup, 152),
	ecalErrSocketSERVICE_NOT_FOUND	= __CE(ecalErrSocketGroup, 153),
	ecalErrSocketTYPE_NOT_FOUND		= __CE(ecalErrSocketGroup, 154),
	ecalErrSocket_E_NO_MORE			= __CE(ecalErrSocketGroup, 155),
	ecalErrSocket_E_CANCELLED		= __CE(ecalErrSocketGroup, 156),
	ecalErrSocketEREFUSED			= __CE(ecalErrSocketGroup, 157),
	ecalErrSocketHOST_NOT_FOUND		= __CE(ecalErrSocketGroup, 158),
	ecalErrSocketTRY_AGAIN			= __CE(ecalErrSocketGroup, 159),
	ecalErrSocketNO_RECOVERY		= __CE(ecalErrSocketGroup, 160),
	ecalErrSocketNO_DATA			= __CE(ecalErrSocketGroup, 161),
	ecalErrSocketNO_ADDRESS			= __CE(ecalErrSocketGroup, 162),

	
	//                  SDMixer Error Group
	ecalErrSDMixerGroup				= __CE(ecalErrIOSection, 0x600),
	ecalErrSDMixerNoLibrary			= __CE(ecalErrSDMixerGroup, 1),
	ecalErrSDMixerInvalidLibrary	= __CE(ecalErrSDMixerGroup, 2),
	ecalErrSDMixerLocked			= __CE(ecalErrSDMixerGroup, 3),
	ecalErrSDMixerNotLocked			= __CE(ecalErrSDMixerGroup, 4),
	ecalErrSDMixerEffectsData		= __CE(ecalErrSDMixerGroup, 5),
	ecalErrSDMixerRouterData		= __CE(ecalErrSDMixerGroup, 6),
	ecalErrSDMixerNoMemory			= __CE(ecalErrSDMixerGroup, 7),
	ecalErrSDMixerEffectsRange		= __CE(ecalErrSDMixerGroup, 8),
	ecalErrSDMixerInvalidParameter	= __CE(ecalErrSDMixerGroup, 9),
	ecalErrSDMixerUnknown			= __CE(ecalErrSDMixerGroup, 10),


	//                  Squeeze Board Error Group
	ecalErrSqueezeGroup				= __CE(ecalErrIOSection, 0x700),
	ecalErrSqueezeNoLibrary			= __CE(ecalErrSqueezeGroup, 1),
	ecalErrSqueezeInvalidLibrary	= __CE(ecalErrSqueezeGroup, 2),
	ecalErrSqueezeLocked			= __CE(ecalErrSqueezeGroup, 3),
	ecalErrSqueezeNotLocked			= __CE(ecalErrSqueezeGroup, 4),
	ecalErrSqueezeEffectsData		= __CE(ecalErrSqueezeGroup, 5),
	ecalErrSqueezeRouterData		= __CE(ecalErrSqueezeGroup, 6),
	ecalErrSqueezeNoMemory			= __CE(ecalErrSqueezeGroup, 7),
	ecalErrSqueezeEffectsRange		= __CE(ecalErrSqueezeGroup, 8),
	ecalErrSqueezeNoField			= __CE(ecalErrSqueezeGroup, 9),
	ecalErrSqueezePendingRfx		= __CE(ecalErrSqueezeGroup, 10),
	ecalErrSqueezeUnknown			= __CE(ecalErrSqueezeGroup, 11),

	//                  CODI Error Group
	ecalErrCodiGroup				= __CE(ecalErrIOSection, 0x800),
	ecalErrCodiNoLibrary			= __CE(ecalErrCodiGroup, 1),
	ecalErrCodiInvalidLibrary		= __CE(ecalErrCodiGroup, 2),
	ecalErrCodiInfoQuery			= __CE(ecalErrCodiGroup, 3),
	ecalErrCodiWriteMemory			= __CE(ecalErrCodiGroup, 4),

	//           Matrox Clip Player Error Group
	ecalErrMcpGroup					= __CE(ecalErrIOSection, 0x900),
	ecalErrMcpNoLibrary				= __CE(ecalErrMcpGroup, 1),
	ecalErrMcpInvalidLibrary		= __CE(ecalErrMcpGroup, 2),

	//----------------- Misc Error Section -----------------
	ecalErrMiscSection				= __CE(0xF000, 0),

	//                   Misc Error Group                   
	ecalErrMiscGroup				= __CE(ecalErrMiscSection, 0x100),
	ecalErrUnknown					= __CE(ecalErrMiscGroup, 1),
	ecalErrInvalidID				= __CE(ecalErrMiscGroup, 2),
	ecalErrInvalidArgument			= __CE(ecalErrMiscGroup, 3),
	ecalErrInvalidComponent			= __CE(ecalErrMiscGroup, 4),
	ecalErrNotImplemented			= __CE(ecalErrMiscGroup, 5),
	ecalErrNullOutputArgument		= __CE(ecalErrMiscGroup, 6),
	ecalErrContextGrab				= __CE(ecalErrMiscGroup, 7),
	ecalErrExcessiveDelays			= __CE(ecalErrMiscGroup, 8),
	ecalErrOGL						= __CE(ecalErrMiscGroup, 9),
	ecalErrNullOGL					= __CE(ecalErrMiscGroup, 10),
	ecalErrNullNode					= __CE(ecalErrMiscGroup, 11),
	ecalErrCannotCacheTexture		= __CE(ecalErrMiscGroup, 12),
	ecalErrUncachedTexture			= __CE(ecalErrMiscGroup, 13),
	ecalErrChyAnimation				= __CE(ecalErrMiscGroup, 14),
	ecalErrDevice					= __CE(ecalErrMiscGroup, 15),
	ecalErrWaitTimedOut				= __CE(ecalErrMiscGroup, 16),
	ecalErrWaitFailed				= __CE(ecalErrMiscGroup, 17),
	ecalErrInternal					= __CE(ecalErrMiscGroup, 240),

	//                   Range Error Group                   
	ecalErrRangeGroup				= __CE(ecalErrMiscSection, 0x200),
	ecalErrNotEnoughValues			= __CE(ecalErrRangeGroup, 1),
	ecalErrTooManyValues			= __CE(ecalErrRangeGroup, 2),
	ecalErrBoundsExceeded			= __CE(ecalErrRangeGroup, 3),
	ecalErrDuplicateValue			= __CE(ecalErrRangeGroup, 4),

	//                  Options Error Group                   
	ecalErrOptionsGroup				= __CE(ecalErrMiscSection, 0x300),
	ecalErrOptFormat				= __CE(ecalErrOptionsGroup, 1),
	ecalErrOptMissing				= __CE(ecalErrOptionsGroup, 2),
	ecalErrOptNoValue				= __CE(ecalErrOptionsGroup, 3),
	ecalErrOptValueConversion		= __CE(ecalErrOptionsGroup, 4),
	ecalErrOptBufferTooSmall		= __CE(ecalErrOptionsGroup, 5),


	//---------------------- Success ------------------------
	ecalErrSuccess				= 0,
	ecalErrSuccessContinue		= 1

} ecalErrCodes;

#ifdef _CALNS_
}
#endif

#endif
