////////////////////////////////////////////////////////////////
// callib.h - Chyron Abstraction Layer library declarations
//
//	Copyright (c) 1998, Chyron Corporation. All rights reserved.
//
//

#if _MSC_VER > 1000
#pragma once
#endif

#ifndef _CALLIB_H
#define _CALLIB_H

#if !defined(_WIN32)
#error ERROR: Only Win32 targets supported!
#endif

#ifndef  __cplusplus
#error Requires C++ Compiler
#endif


#include <ole2.h>


#include "CALTypes.h"


#ifndef _JNI_
#define __STR(s) LPTSTR s
#define __CSTR(s) LPCTSTR s
#define __PHCAL(s) HCAL * s
#else
#define __STR(s) char * s
#define __CSTR(s) const char * s
#define __PHCAL(s) HCAL * s
#endif

#ifndef _TCL_

#define CALPROC(s) CAL##s
#define ENUM(s) enum s

// Callback function declarations 
typedef long (__cdecl *CALUpdateFunc)(HCAL connId, HCAL id, __STR(*data), void *clientData);
CALUpdateFunc CALRegisterUpdateFunc(CALUpdateFunc func, void *clientData);

typedef void (__cdecl *EvStateChangeEx)(long connId, long id, long state, long data);
EvStateChangeEx CALRegisterStateChangeHandler(EvStateChangeEx fn, long data);

typedef void (__cdecl *EvUserEventEx)(long connId, long eventId, long data);
EvUserEventEx CALRegisterUserEventHandler(EvUserEventEx fn, long data);

typedef void (__cdecl *EvStatusEventEx)(long connId, long id, long kind, VARIANT eventData, long data);
EvStatusEventEx CALRegisterStatusEventHandler(EvStatusEventEx fn, long data);

CALCOLOR CALRGBA(int red, int green, int blue, int opacity);

#else

#define CALPROC(s) s
#define ENUM(s) ELONG

typedef long ELONG;
//#define HCAL long
CALCOLOR CALPROC(RGBA)(int red, int green, int blue, int opacity);
//wchar_t *CALRGBA(int red, int green, int blue, int opacity);
//const wchar_t *CALRGBAX(int red, int green, int blue, int opacity);

#endif




ErrorCode CALPROC(AddEdge)(long fontIndex, long priority, CALCOLOR color, ENUM(ecalEdgeType) type, 
				   Real32 angle, Real32 size, long softness);

#ifndef _TCL_
ErrorCode CALPROC(AddEdge)(long fontIndex, long priority, long color, ENUM(ecalEdgeType) type, 
				   Real32 angle, Real32 size, long softness);
#endif
ErrorCode CALPROC(AddLight)(HCAL id, long lightIndex, ENUM(ecalLightType) lightType, CALCOLOR color,
					Real32 xyzPos[3], Real32 xyzDir[3]);

#ifndef _TCL_
ErrorCode CALPROC(AddLight)(HCAL id, long lightIndex, ENUM(ecalLightType) lightType, long color,
					Real32 xyzPos[3], Real32 xyzDir[3]);
#endif
#ifndef _TCL_
ErrorCode CALPROC(Animate)(HCAL id, long attributes, int keyValuePairs, long keys[], Real32 values[],
				   ENUM(ecalInterpMode) interpolation);
#endif
ErrorCode CALPROC(Animate)(HCAL id, long attributes[], int keyValuePairs, long keys[], Real32 values[],
				   ENUM(ecalInterpMode) interpolation);

#ifndef _TCL_
ErrorCode CALPROC(AnimateCamera)(HCAL id, long attributes, int keyValuePairs, long keys[], Real32 values[],
				   ENUM(ecalInterpMode) interpolation);
#endif
ErrorCode CALPROC(AnimateCamera)(HCAL id, long attributes[], int keyValuePairs, long keys[], Real32 values[],
				   ENUM(ecalInterpMode) interpolation);

#ifndef _TCL_
ErrorCode CALPROC(AnimateLight)(HCAL id, long lightIndex, long attributes, int keyValuePairs, long keys[], Real32 values[],
				   ENUM(ecalInterpMode) interpolation);
#endif
ErrorCode CALPROC(AnimateLight)(HCAL id, long lightIndex, long attributes[], int keyValuePairs, long keys[], Real32 values[],
				   ENUM(ecalInterpMode) interpolation);

ErrorCode CALPROC(Background)(HCAL id, ENUM(ecalTransVector) direction, CALCOLOR color1, CALCOLOR color2);
#ifndef _TCL_
ErrorCode CALPROC(Background)(HCAL id, ENUM(ecalTransVector) direction, long color1, long color2);
#endif
ErrorCode CALPROC(CacheFontChars)(long fontIndex, CALCOLOR color, __CSTR(pChars));
#ifndef _TCL_
ErrorCode CALPROC(CacheFontChars)(long fontIndex, long color, __CSTR(pChars));
#endif
ErrorCode CALPROC(CacheImage)(HCAL id, __CSTR(name), __CSTR(path));
#ifndef _TCL_
ErrorCode CALPROC(CacheImageData)(HCAL id, __CSTR(name), const unsigned char *data, long width, long height, int bitsPerPixel, float aspect = 1.0);
#endif
ErrorCode CALPROC(CameraView)(HCAL id, Real32 xyzPos[3], Real32 xyzRot[3]);
ErrorCode CALPROC(CanvasRgn)(Real32 left, Real32 top, Real32 width, Real32 height, __PHCAL(id));

ErrorCode CALPROC(ChangeStartTime)(HCAL id, long startOffset);
ErrorCode CALPROC(ClearBuffer)(long BufferIndex, CALCOLOR color);
#ifndef _TCL_
ErrorCode CALPROC(ClearBuffer)(long BufferIndex, long color);
#endif
ErrorCode CALPROC(Clock)(Real32 left, Real32 top, Real32 width, Real32 height, long fontIndex, CALCOLOR color,
				   ENUM(ecalJustify) justification, __CSTR(format), ENUM(ecalClockMode) mode, __PHCAL(id));
#ifndef _TCL_
ErrorCode CALPROC(Clock)(Real32 left, Real32 top, Real32 width, Real32 height, long fontIndex, long color,
				   ENUM(ecalJustify) justification, __CSTR(format), ENUM(ecalClockMode) mode, __PHCAL(id));
#endif
ErrorCode CALPROC(Connect)(__CSTR(sysName), __CSTR(output), ENUM(ecalVideoMode) format, __PHCAL(connId));
ErrorCode CALPROC(Crawl)(HCAL id, ENUM(ecalTimingStyle) timingStyle, Real32 value, ENUM(ecalEffectOptions) easeMode);
ErrorCode CALPROC(CxColor)(HCAL id, CALCOLOR color);
ErrorCode CALPROC(CxCreate)(__PHCAL(id));
ErrorCode CALPROC(CxFree)(HCAL id);
ErrorCode CALPROC(CxImage)(HCAL id, __CSTR(name), long justification, long scale);
ErrorCode CALPROC(CxPen)(HCAL id, Real32 size, CALCOLOR color, __CSTR(name));
ErrorCode CALPROC(CxPolygon)(HCAL id, long count, Real32 x[], Real32 y[], Real32 z[]);
ErrorCode CALPROC(CxPolyline)(HCAL id, long count, Real32 x[], Real32 y[], Real32 z[], long startFrame, long endFrame);
ErrorCode CALPROC(CxScript)(HCAL id, long lang, __CSTR(script));

ErrorCode CALPROC(DeleteLight)(HCAL id, long lightIndex);
ErrorCode CALPROC(Disconnect)(HCAL connId);
ErrorCode CALPROC(EnableLight)(HCAL id, long lightIndex, ENUM(ecalLightState) state);
ErrorCode CALPROC(Export)(HCAL id, __CSTR(filename), __CSTR(options));
ErrorCode CALPROC(FieldOfView)(HCAL id, Real32 yAngle, Real32 xAspect, Real32 nearClip, Real32 farClip);
ErrorCode CALPROC(FlipBookRgn)(Real32 left, Real32 top, Real32 width, Real32 height, 
					ENUM(ecalJustify) justification, ENUM(ecalScale) scaleMode, __PHCAL(id));
ErrorCode CALPROC(FontOptions)(long fontIndex, Real32 horzShear, Real32 vertShear, Real32 extraKerning, 
					   Real32 leading, Real32 spaceWidth, Real32 charWidth, Real32 charScale) ;

ErrorCode CALPROC(Free)(HCAL id, ENUM(ecalFreeTime) when);
ErrorCode CALPROC(FreeColor)(long colorIndex);
ErrorCode CALPROC(FreeFont)(long fontIndex);
ErrorCode CALPROC(GenerateMethod)(int count, int *pOps, double *pValue);
ErrorCode CALPROC(GetID)(__CSTR(name), __PHCAL(id));
ErrorCode CALPROC(GetName)(HCAL id, BSTR *name);
#ifndef _TCL_
ErrorCode CALPROC(GetName)(HCAL id, __STR(name), int nameSize);
#endif
ErrorCode CALPROC(GetRgnIds)(HCAL groupId, VARIANT *ids, long idsSize, long *numIds);
#ifndef _TCL_
ErrorCode CALPROC(GetRgnIds)(HCAL groupId, __PHCAL(ids), long idsSize, long *numIds);
#endif
//#ifndef _TCL_
ErrorCode CALPROC(GetRgnInfo)(HCAL id, CALRGNINFO *rgnInfo);
//#endif
ErrorCode CALPROC(GetPriority)(HCAL id, long *priority);
#ifndef _TCL_
ErrorCode CALPROC(GetSystemInfo)(ENUM(ecalVideoMode) VideoMode, CALSERVERSYSINFO  *sysInfo);
#endif
ErrorCode CALPROC(Group)(int rgnCount, HCAL regions[], __PHCAL(gid));

#ifndef _TCL_
ErrorCode CALPROC(Group)(int rgnCount, HCAL region, /* region2 */... /*, __PHCAL(gid) */) ;
#endif
ErrorCode CALPROC(ImageRgn)(Real32 left, Real32 top, Real32 width, Real32 height, 
					ENUM(ecalJustify) justification, ENUM(ecalScale) scaleMode, __PHCAL(id));

ErrorCode CALPROC(Import)(__CSTR(file), __PHCAL(id));
ErrorCode CALPROC(InsertIntoGroup)(HCAL groupId, long priority, int rgnCount, HCAL regions[]);
#ifndef _TCL_
ErrorCode CALPROC(InsertIntoGroup)(HCAL groupId, long priority, int rgnCount, HCAL region, /* region2 */ ...);
#endif
#ifndef _TCL_
ErrorCode CALPROC(LoadFont)(const LOGFONT& font, long *fontIndex);
#endif
ErrorCode CALPROC(LoadFontName)(__CSTR(name), long height, long style, long *fontIndex);
#ifndef _TCL_
ErrorCode CALPROC(LoadImage)(HCAL id, ENUM(ecalUpdateMode) mode, __CSTR(name), const unsigned char *data, long width, long height, int bitsPerPixel, float aspect = 1.0);
#endif
ErrorCode CALPROC(LoadPalette)(int colorCount, CALCOLOR colors[], long *colorIndex);
#ifndef _TCL_
ErrorCode CALPROC(LoadPalette)(int colorCount, CALCOLOR color, /* color2 */... /*, long *colorIndex */ );
ErrorCode CALPROC(LoadPalette)(int colorCount, long color, /* color2 */... /*, long *colorIndex */ );
ErrorCode CALPROC(LoadPalette)(int colorCount, long colors[], long *colorIndex);
#endif
ErrorCode CALPROC(LoadTexture)(__CSTR(name), long *colorIndex);
ErrorCode CALPROC(Loop)(HCAL id, long startAt, long frameCount, ENUM(ecalLoopMode) loopMode, long duration);
ErrorCode CALPROC(Ortho2DView)(HCAL id);
ErrorCode CALPROC(Pause)(HCAL id, long offset, long duration);
ErrorCode CALPROC(Perform)(VARIANT *data);
ErrorCode CALPROC(PlayI)(HCAL id);
ErrorCode CALPROC(PlayW)(HCAL id);
ErrorCode CALPROC(PlaybackRgn)(HCAL id, Real32 left, Real32 top, Real32 width, Real32 height, ENUM(ecalScale) scaleMode);
ErrorCode CALPROC(Poly3DRgn)(Real32 left, Real32 top, Real32 width, Real32 height,
 					ENUM(ecalJustify) justification, ENUM(ecalScale) scaleMode, __PHCAL(id));
ErrorCode CALPROC(Poly3DView)(HCAL id);
ErrorCode CALPROC(RemoveFromGroup)(HCAL groupId, long priority, int rgnCount);
ErrorCode CALPROC(RenderQuality)(ENUM(ecalRenderQuality) quality);
ErrorCode CALPROC(ReserveGPIO)(long device, Ident32 *pId);
ErrorCode CALPROC(ReserveDevice)(long device, Ident32 *pId);
ErrorCode CALPROC(Resume)(HCAL id);
ErrorCode CALPROC(RgnTransition)(HCAL id, ENUM(ecalTransType) type, ENUM(ecalTransPattern) pattern,
						 ENUM(ecalTransDir) direction, long duration, ENUM(ecalEffectOptions) options);

ErrorCode CALPROC(Roll)(HCAL id, ENUM(ecalTimingStyle) timingStyle, Real32 value, ENUM(ecalEffectOptions) easeMode);
ErrorCode CALPROC(Schedule)(HCAL id, ENUM(ecalSchedule) mode, __CSTR(when), __CSTR(data)); // 11/24/2001 - data arg
ErrorCode CALPROC(SelectBuffer)(long buffer);
ErrorCode CALPROC(SelectConnection)(HCAL connId);
#ifndef _TCL_
ErrorCode CALPROC(SetCharImage)(long fontIndex, long colorIndex, long charId, Real32 xOffset, Real32 yOffset, Real32 targetWidth, Real32 targetHeight, 
						const unsigned char *data, long sourceWidth, long sourceHeight, int bitsPerPixel, float aspect = 1.0);
#endif
ErrorCode CALPROC(SetCharImage)(long fontIndex, long colorIndex, long charId, Real32 xOffset, Real32 yOffset, Real32 width, Real32 height, __CSTR(name));
ErrorCode CALPROC(SetExtents)(HCAL id, Real32 left, Real32 top, Real32 width, Real32 height);
ErrorCode CALPROC(SetJustify)(HCAL id, ENUM(ecalJustify) justification, Real32 xOffset, Real32 yOffset);
ErrorCode CALPROC(SetMapMode)(ENUM(ecalVideoMode) input, ENUM(ecalVideoMode) output);
ErrorCode CALPROC(SetName)(HCAL id, __CSTR(name));
ErrorCode CALPROC(SetObjCenter)(HCAL id, ENUM(ecalObjCenter) refPt, Real32 x, Real32 y, Real32 z);
ErrorCode CALPROC(SetOpacity)(HCAL id, unsigned char opacity);
ErrorCode CALPROC(SetOptions)(HCAL id, __CSTR(options));
ErrorCode CALPROC(SetPriority)(HCAL id, long priority);
ErrorCode CALPROC(Shutdown)();
ErrorCode CALPROC(StepFrame)(HCAL id, long frameIndex = -1);
ErrorCode CALPROC(TextOptions)(HCAL id, Real32 angle, ENUM(ecalWrapMode) wrapMode, Real32 extraKerning, Real32 extraLeading);
ErrorCode CALPROC(TextRgn)(Real32 left, Real32 top, Real32 width, Real32 height, long fontIndex, 
				   CALCOLOR color, ENUM(ecalJustify) justification, __PHCAL(id));

#ifndef _TCL_
ErrorCode CALPROC(TextRgn)(Real32 left, Real32 top, Real32 width, Real32 height, long fontIndex, 
				   long color, ENUM(ecalJustify) justification, __PHCAL(id));
#endif
ErrorCode CALPROC(Timer)(Real32 left, Real32 top, Real32 width, Real32 height, long fontIndex, CALCOLOR color,
				   ENUM(ecalJustify) justification, __CSTR(format), ENUM(ecalTimerMode) mode, __PHCAL(id));
#ifndef _TCL_
ErrorCode CALPROC(Timer)(Real32 left, Real32 top, Real32 width, Real32 height, long fontIndex, long color,
				   ENUM(ecalJustify) justification, __CSTR(format), ENUM(ecalTimerMode) mode, __PHCAL(id));
#endif
ErrorCode CALPROC(Transition)(ENUM(ecalTransType) transType, ENUM(ecalTransPattern) pattern,  ENUM(ecalTransDir) direction,
					  long duration, ENUM(ecalEffectOptions) options, ENUM(ecalOutputChannel) output, __PHCAL(id));	
ErrorCode CALPROC(TransitionOptions)(HCAL id, ENUM(ecalTransOption) option, Real32 dValue);
ErrorCode CALPROC(TypeOn)(HCAL id, ENUM(ecalTimingStyle) style, Real32 value);

#if 0 //def _TCL_
ErrorCode CALPROC(UpdateI)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, char * values[]);
ErrorCode CALPROC(UpdateW)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, char * values[]);
#endif

#ifdef _JNI_
ErrorCode CALPROC(UpdateI)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, char **values);
#else
ErrorCode CALPROC(UpdateI)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, __CSTR(values[]));
#endif

#ifndef _TCL_
ErrorCode CALPROC(UpdateI)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, __CSTR(value), /* value2 */...);
#endif
#ifdef _JNI_
ErrorCode CALPROC(UpdateW)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, char **values);
#else
ErrorCode CALPROC(UpdateW)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, __CSTR(values[]));
#endif
#ifndef _TCL_
ErrorCode CALPROC(UpdateW)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, __CSTR(value), /* value2 */...);
#endif
#ifndef _TCL_
ErrorCode CALPROC(UpdateI)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, const VARIANT values[]);
#endif
#ifndef _TCL_
ErrorCode CALPROC(UpdateI)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, const VARIANT& value, /* value2 */...);
#endif
#ifndef _TCL_
ErrorCode CALPROC(UpdateW)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, const VARIANT values[]);
#endif
#ifndef _TCL_
ErrorCode CALPROC(UpdateW)(HCAL id, ENUM(ecalUpdateMode) mode, int valueCount, const VARIANT& value, /* value2 */...);
#endif
ErrorCode CALPROC(VideoDefaults)(long opts, long values[] );
#ifndef _TCL_
ErrorCode CALPROC(VideoDefaults)(long opts, long value, /* value2 */...);
#endif
ErrorCode CALPROC(VideoRgn)(Real32 left, Real32 top, Real32 width, Real32 height, 
					ENUM(ecalJustify) justification, ENUM(ecalScale) scaleMode, __PHCAL(id));
ErrorCode CALPROC(ViewVolume)(HCAL id, Real32 xLeft, Real32 xRight, Real32 yTop, Real32 yBottom, Real32 zNear, Real32 zFar);

#endif	/* _CALLIB_H */
