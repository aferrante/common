////////////////////////////////////////////////////////////////
// caldir.h - Chyron Abstraction Layer library declarations
//
//	Copyright (c) 1998, Chyron Corporation. All rights reserved.
//
//

// header aliases
#ifndef _CALDIR_H
#define _CALDIR_H

#pragma include_alias("caltypes.h",  "C:/Source/Common/API/Chyron/CAL/LIB/caltypes.h")
#pragma include_alias("calerrors.h", "C:/Source/Common/API/Chyron/CAL/LIB/calerrors.h")
#pragma include_alias("callib.h",    "C:/Source/Common/API/Chyron/CAL/LIB/callib.h")

// libraries 

#ifdef _UNICODE 
# ifdef _DEBUG
#  ifdef _MT
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30umtd.lib")
#  else 
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30ud.lib")
#  endif // _MT
# else
#  ifdef _MT
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30umt.lib")
#  else 
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30u.lib")
#  endif // _MT
# endif // _DEBUG
#else
# ifdef _DEBUG
#  ifdef _MT
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30mtd.lib")
#  else 
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30d.lib")
#  endif // _MT
# else
#  ifdef _MT
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30mt.lib")
#  else 
#	pragma comment(lib, "C:/Source/Common/API/Chyron/CAL/LIB/cal30.lib")
#  endif // _MT
# endif // _DEBUG
#endif

#endif // _CALDIR_H

