////////////////////////////////////////////////////////////////
// CALTypes.h - Chyron Abstraction Layer library declarations
//
//	Copyright (c) 1998, Chyron Corporation. All rights reserved.
//
// 19-Apr-99 KH:	This file has been reconciled with the following VSS version:
//					54		Byrne		3/26/99		5:02p
//
////////////////////////////////////////////////////////////////
	
#ifndef _CALTYPES_H
#define _CALTYPES_H

#include "CALErrors.h"

//------------ identifiers returned by CAL --------------
#ifndef _IDENT32_
# define _IDENT32_
  typedef long  Ident32;          // identifier 
#endif

#ifndef _TCL_
const long INVALID_ID32 = (Ident32)(-1);
#endif

//------ red-green-blue-alpha color specifications ------
#ifdef RGBA 
# undef RGBA // use CALRGBA
#endif

#ifdef __MIDL__
#define __MIDL_PUBLIC__ [public]
#else
#define __MIDL_PUBLIC__
#endif


#ifndef __CALX__

typedef long HCAL;

#else // __CALX__

typedef __MIDL_PUBLIC__ VARIANT HCAL;

#endif 

typedef __MIDL_PUBLIC__ VARIANT CALCOLOR;


typedef __MIDL_PUBLIC__ float Real32;

#ifndef SWIG

typedef struct {
	long reserved;
} CALPLAYSTATUS;

typedef struct CALPOINT {
	Real32 x;
	Real32 y;
} CALPOINT;

typedef struct CALRECT {
	Real32 left;
	Real32 top;
	Real32 right;
	Real32 bottom;
#if !defined(__MIDL__) && !defined(SWIG)
	CALRECT& operator=(RECT &rhs) {
		left   = (Real32)rhs.left;
		top    = (Real32)rhs.top;
		right  = (Real32)rhs.right;
		bottom = (Real32)rhs.bottom;
		return *this;
	}
#endif
} CALRECT;

typedef __MIDL_PUBLIC__ struct CALRGNINFO {
	CALRECT bounds;
	CALRECT overrun;
	long rgnType;
	long PlayState;
	long CurrFrame;
	long ecalLastError; 
	long TextDispStatus;

} CALRGNINFO;

typedef __MIDL_PUBLIC__ struct CALCOORDSYS{
	long xmin;
	long ymin;
	long xres;
	long yres;
	long pixW;
	long pixH;
	long frW;
	long frH;
} CALCOORDSYS;

typedef struct {
	char InterfaceVersion[20];
	char HardwareRevLevel[20];
	char IPAddress[20];
	char HostName[40];
	long VideoMode;
	long Status;
	CALCOORDSYS CoordSys; 
} CALSERVERSYSINFO;

typedef struct {
	long Interpolation;
	Real32 x, y, z;
} CALPATHNODE;
#endif // SWIG

typedef enum ecalSyncModes {
	ecalAnalogSync,
	ecalSDIVideo,
	ecalFreeRun
} ecalSyncModes;



#ifndef LEGACY_CAL_TYPES // ??? Cut Legacy ASAP - FIND -> LEGACY_CAL_TYPES

//------------ ecalDblNoChange Preprocessor ------------
#ifndef ecalDblNoChange
# include <limits>
# define ecalDblNoChange std::numeric_limits<double>::infinity()
#endif


//-------------- ecalVideoMode Enumeration --------------
typedef enum ecalVideoMode
{
	ecalFormatNTSC = 1,
	ecalFormatPAL,
	ecalFormat720P,
	ecalFormat1080I,
	ecalFormatDefault,
	ecalFormatCurrent
} ecalVideoMode;


//------------ ecalVideoOptions Enumeration -------------
typedef enum ecalVideoOptions {
	ecalVIOFormat        = 0x00000001,
	ecalVIOSync          = 0x00000002,              
	ecalVIOMix           = 0x00000004,
	ecalVIOAlphaShape    = 0x00000008,
	ecalVIOAncillaryData = 0x00000010,
	ecalVIOKeyInput      = 0x00000020,
	ecalVIOHDelay        = 0x00000040,
	ecalVIOVDelay        = 0x00000080,
	ecalVIOTimeCode      = 0x00000100,
	ecalVIOBackplane2_3  = 0x00000200,
	ecalVIOBackplane4_5  = 0x00000400,
	ecalVIOBackplane6_7  = 0x00000800,
	ecalVIOAllOptions    = 0x000000ff
} ecalVideoOptions;

//-------------- ecalJustify Enumeration ----------------
typedef enum ecalJustify {
	ecalInvalidJustify = -1,
	ecalNoJustify = 0,
	ecalUpperLeft = 1,
	ecalUpperCenter,
	ecalUpperRight,
	ecalCenterLeft,
	ecalCenter,
	ecalCenterRight,
	ecalLowerLeft,
	ecalLowerCenter,
	ecalLowerRight
} ecalJustify;


//---------------- ecalStatus Enumeration ----------------
typedef enum ecalStatusEvent {
	ecalStatusTextual     = 0,
	ecalStatusPlayFinished = 1,
	ecalStatusSpoolPaused = 1000,
	ecalStatusSpoolResumed = 1001,
	ecalStatusSpoolObjectLevel  = 1010,
	ecalStatusBuildObjectBounds = 2000,
	ecalStatusBuildCelRange = 2100,
	ecalStatusError = 0xFFFFFFFF
} ecalStatusEvent;


//---------------- ecalScale Enumeration ----------------
typedef enum ecalScale {
	ecalScaleToFit = 1,
	ecalMaintainAspect,
	ecalScaleClip,
	ecalOrigSize
} ecalScale;

//---------------------- ecalColors ---------------------
typedef enum ecalColors {
	ecalBlack = 0,
	ecalWhite,
	ecalRed,
	ecalGreen,
	ecalBlue,
	ecalCyan,
	ecalMagenta,
	ecalYellow,
	ecalDarkGray,
	ecalLightGray,
	ecalPink,
	ecalLightGreen,
	ecalLightBlue,
	ecalAqua,
	ecalLightViolet,
	ecalPastelYellow
} ecalColors;

//-------------- ecalFontStyle Enumeration -------------
typedef enum ecalFontStyle {
	ecalNormal = 0,
	ecalBold = 1,	// must be 1 so it will equal to CFE_BOLD
	ecalItalic = 2	// must be 2 so it will equal to CFE_ITALIC
} ecalFontStyle;

//-------------- ecalEdgeType Enumeration --------------
typedef enum ecalEdgeType {
	ecalNone = 0,
	ecalBorder,
	ecalDropShadow,
	ecalOffset
} ecalEdgeType;


//-------------- ecalSlideDir Enumeration --------------
typedef enum ecalSlideDir {
	ecalSlideFromLeft = 0,
	ecalSlideFromRight,
	ecalSlideFromTop,
	ecalSlideFromBottom,
	ecalCrawlFromLeft,
	ecalCrawlFromRight
} ecalSlideDir;

//-------------- ecalWrapMode Enumeration --------------
typedef enum ecalWrapMode {
	ecalWordWrap = 0,
	ecalCharWrap,
	ecalClip,
	ecalNoClip,
	ecalCompWord,
	ecalCompSpace,
	ecalScaleTextToFit,
	ecalCurrentWrap,
	ecalCompLine
} ecalWrapMode;

//------------- ecalUpdateMode Enumeration -------------
typedef enum ecalUpdateMode {
	ecalUpdateOnly = 0,      // just change data - don't build
	ecalUpdateAndCache,      // don't display - store in cache
	ecalUpdateAndPlay,       // retrieve from cache
	ecalUpdateAndReplace,
	ecalUpdatePrevMode,
	ecalUpdateAndSpool,
	ecalUpdateAndSpoolEnd,
	ecalUpdateSelect,
	ecalUpdatePassiveCache,
	ecalUpdateSpoolSelect = 100,  // internal use only
	ecalUpdateTimer = 0x101, // internal use only 
} ecalUpdateMode;


//------------- ecalInterpMode Enumeration -------------
typedef enum ecalInterpMode {
	ecalJump = 0,       // discrete points
	ecalLinear,         // uniformly interpolated
	ecalSpline,          // cubic spline

	ecalCompileKeyFrames = 0x4000, // compile key frames
	ecalClearKeyFrames = 0x8000, // clear existing key frames

	ecalJumpCompile = ecalJump | ecalCompileKeyFrames,
	ecalLinearCompile = ecalLinear | ecalCompileKeyFrames,
	ecalSplineCompile = ecalSpline | ecalCompileKeyFrames,

	ecalJumpClear = ecalJump | ecalClearKeyFrames,
	ecalLinearClear = ecalLinear | ecalClearKeyFrames,
	ecalSplineClear = ecalSpline | ecalClearKeyFrames,
	
	ecalJumpClearCompile = ecalJump | ecalClearKeyFrames | ecalCompileKeyFrames,
	ecalLinearClearCompile = ecalLinear | ecalClearKeyFrames | ecalCompileKeyFrames,
	ecalSplineClearCompile = ecalSpline | ecalClearKeyFrames | ecalCompileKeyFrames
} ecalInterpMode;

//-------------- ecalPlayState Enumeration --------------
typedef enum ecalPlayState {
	ecalInit = 0,       // initial, not played yet
	ecalPaused,         // was playing, now paused
	ecalPlaying,        // currently playing
	ecalLooping,
	ecalFinished
} ecalPlayState;

//-------------- ecalAnimAttr Enumeration --------------
typedef enum ecalAnimAttr
{
	ecalXPos    = 0x1,
	ecalYPos    = 0x2,
	ecalZPos    = 0x4,
	ecalXRot    = 0x8,
	ecalYRot    = 0x10,
	ecalZRot    = 0x20,
	ecalXScale  = 0x40,
	ecalYScale  = 0x80,
	ecalZScale  = 0x100,
	ecalXCenter = 0x200,
	ecalYCenter = 0x400,
	ecalZCenter = 0x800,
	ecalRGBA    = 0x1000,
	ecalFocus   = 0x2000,
	ecalShine   = 0x4000,
	ecalLoops   = 0x8000,	
	ecalAlpha   = 0x10000,
	ecalEffectPct = 0x20000,
	ecalMethod	= 0x40000,

	ecalXOffset = ecalXCenter,
	ecalYOffset = ecalYCenter,
	ecalWidth = ecalXRot,
	ecalHeight = ecalYRot,

} ecalAnimAttr;


//------------- ecalTimingStyle Enumeration -------------
typedef enum ecalTimingStyle
{
	ecalDuration = 0,
	ecalRate,
	ecalOpenEnded
} ecalTimingStyle;


//-------------- ecalEffectOptions Enumeration --------------
typedef enum ecalEffectOptions
{
	ecalNoEffect			= 0x0,
	ecalNoEase				= 0x0,
	ecalEaseIn				= 0x1,
	ecalEaseOut				= 0x2,
	ecalEaseInOut			= 0x3, // Same as ecalEaseIn | ecalEaseOut
	ecalStartOnScreen		= 0x4,
	ecalEndOnScreen			= 0x8,
	ecalStartEndOnScreen	= 0xC, // Same as ecalStartOnScreen | ecalEndOnScreen
} ecalEffectOptions;


//-------------- ecalLoopMode Enumeration --------------
typedef enum ecalLoopMode
{
	ecalLoopForward = 0, // playback normally
	ecalLoopReverse,     // playback in reverse-frame
	ecalHold             // play once and hold for remaining frames
} ecalLoopMode;


//------------ ecalTransVector Enumeration --------------
typedef enum ecalTransVector
{
	ecalLeftToRight = 0,
	ecalRightToLeft,
	ecalTopToBottom,
	ecalBottomToTop
} ecalTransVector;

//------------ ecalTransDir Enumeration ---------------
typedef enum ecalTransDir 
{
	ecalDirDef		= 0x0,
	ecalDirRight	= 0x0,	// Horizontal Default
	ecalDirLeft		= 0x1,	// Horizontal Reverse
	ecalDirDown		= 0x2,	// Vertical Default
	ecalDirUp		= 0x3,	// Vertical Reverse
	ecalDirOut		= 0x4,	// Box Default
	ecalDirIn		= 0x5,	// Box Reverse
	ecalDirCW		= 0x6,	// Radial Default
	ecalDirCCW		= 0x7	// Radial Reverse
} ecalTransDir;

//------------ ecalTransPattern Enumeration --------------
typedef enum ecalTransPattern
{
	ecalPatternDef	= 0x0,
	ecalHorizSlide	= 0x1,	// Wipe/Push/Hide/Reveal
	ecalPatternA	= 0x1,
	ecalVertSlide	= 0x2,	// Wipe/Push/Hide/Reveal
	ecalPatternB	= 0x2,
	ecalHorizSplit	= 0x3,	// Wipe only
	ecalPatternC	= 0x3,
	ecalVertSplit	= 0x4,	// Wipe only
	ecalPatternD	= 0x4,
	ecalBox			= 0x5,	// Wipe only
	ecalPatternE	= 0x5,
	ecalRadial		= 0x6	// Wipe only
} ecalTransPattern;

//-------------- ecalTransType Enumeration --------------
typedef enum ecalTransType
{
	ecalCut = 0,
	ecalFade,
	ecalWipe,
	ecalReveal,
	ecalHide,
	ecalDissolve,
	ecalPush,
	ecalWipeDslv,
	ecalRevealDslv,
	ecalHideDslv,
	ecalPushDslv
} ecalTransType;


//------------ ecalTransOption Enumeration --------------
typedef enum ecalTransOption
{
	ecalWipeSoftness = 0,
	ecalDissolveAlpha
} ecalTransOption;


//------------- ecalChangeMode Enumeration -------------
typedef enum ecalChangeMode
{
	ecalScaleAll = 0,
	ecalHoldEnd
} ecalChangeMode;


//-------------- ecalFileType Enumeration --------------
typedef enum ecalFileType
{
	ecalInfinitRGB = 1,
	ecalFileFormatBMP, 
	ecalFileFormatTIFF,
	ecalFileFormatJPEG,
	ecalFileFormatTGA,
	ecalFileFormatRLE,
	ecalFileFormatPPM,
	ecalFileFormatGIF,
	ecalFileFormatCSF,
	ecalFileFormatMOV,
	ecalFileFormatAVI,
	ecalOtherFileFormat
} ecalFileType;

//-------------- ecalRgnType Enumeration ----------------
typedef enum ecalRgnType
{
	ecalText = 1,
	ecalImage,
	ecalVideo, 
	ecal3D,
	ecalComposite
} ecalRgnType;


//-------------- ecalDeviceType Enumeration ----------------
typedef enum ecalDeviceType {
	ecalDeviceSocket	= 0xFFFFFFFF,
	ecalDeviceCodi0		= 0xFFFFFFFE,
	ecalDeviceCodi1		= 0xFFFFFFFD, 
	ecalDeviceCodi2		= 0xFFFFFFFC, 
	ecalDeviceCodi3		= 0xFFFFFFFB,
	ecalDeviceSDMixer	= 0xFFFFFFFA,
	ecalDeviceSqueeze	= 0xFFFFFFF9
} ecalDeviceType;

//-------------- ecalMotionPath Enumeration ----------------
typedef enum ecalMotionPath {
	ecalMotionXY = 0,
	ecalMotionXZ,
	ecalMotionYZ,
	ecalMotionT3,
	ecalMotionJump,
	ecalMotionLinear,
	ecalMotionSpline
} ecalMotionPath;

//-------------- ecalNode Enumeration ----------------
typedef enum ecalNode
{
	ecalNodeInvalid = -1,
	ecalNodeConnection = 0,
	ecalNodeGPIOBoard0 = 0xFF000000,
	ecalNodeGPIOBoard1 = 0xFF010000,
	ecalNodeGPIOBoard2 = 0xFF020000,
	ecalNodeGPIOBoard3 = 0xFF030000,
	ecalNodeServer	   = 0xFF100000

} ecalNode;

//-------------- ecalRgnInfoType Enumeration --------------
typedef enum ecalRgnInfoType
{
	ecalInfoBounds  = 0x01,
	ecalInfoOverrun = 0x02,
	ecalInfoType    = 0x04
} ecalRgnInfoType;

//--------------- ecalScriptLang Enumeration ---------------
typedef enum ecalScriptLang
{
	ecalTclScript = 0x0
} ecalScriptLang;

//--------------- ecalSchedule Enumeration ---------------
typedef enum ecalSchedule {
	ecalSchedDateTimeOn = 0x01,
	ecalSchedDateTimeOff = 0x02,
	ecalSchedTimecodeSync = 0x10,
	ecalSchedScript	= 0x100, // to be OR'd with script language; e.g., ecalSchedScript | ecalTclScript
	ecalSchedGPIO = 0x1000
} ecalSchedule;

//--------------- ecalStatus Enumeration ---------------
typedef enum ecalStatus
{
	ecalStatusUnknown = 0
} ecalStatus;


//------------- ecalTextStatus Enumeration -------------
typedef enum ecalTextStatus
{
	ecalTextStatusTooWide      = -1,
	ecalTextStatusUncompressed = 0,
	ecalTextStatusCompressed   = 1
} ecalTextStatus;


//-------------- ecalObjCenter Enumeration --------------
typedef enum ecalObjCenter
{
	ecalRelativeObj = 0,
	ecalRelativeRgn
} ecalObjCenter;


//------------- ecalLightType Enumeration -------------
typedef enum ecalLightType
{
	ecalGlobal = 0,
	ecalDirectional,
	ecalPointLight,
	ecalSpotlight
} ecalLightType;


//------------- ecalLightState Enumeration -------------
typedef enum ecalLightState
{
	ecalLightOff = 0,
	ecalLightOn
} ecalLightState;


//-------------- ecalFreeTime Enumeration --------------
typedef enum ecalFreeTime
{
	ecalFreeNow = 0,
	ecalFreeAfterPlaying,
	ecalFreeUponExit
} ecalFreeTime;


//------------ ecalRenderQuality Enumeration ------------
typedef enum ecalRenderQuality
{
	ecalFast = 0, 	// aliased
	ecalQuality		// anti-aliased
} ecalRenderQuality;


//------------ ecalClockMode Enumeration ------------
typedef enum ecalClockMode
{
	ecal12Hour = 0,
	ecal24Hour
} ecalClockMode;


//------------ ecalTimerMode Enumeration ------------
typedef enum ecalTimerMode
{
	ecalCountUp = 0,
	ecalCountDown
} ecalTimerMode;

//---------------ecalCreationMode Enumeration-----------
typedef enum ecalCreationMode
{
	ecalUseExisting = 0,
	ecalForceUnique
} ecalCreationMode;

//---------------ecalFrameBuffer Enumeration------------
typedef enum ecalFrameBuffer
{
	ecalFrameBufferA = 1,
	ecalFrameBufferB = 2
} ecalFrameBuffer;

//--------------ecalOutputChannel Enumeration-----------
typedef enum ecalOutputChannel
{
	ecalOutputChannel1 = 0,
	ecalOutputChannel2 = 1
} ecalOutputChannel;



#else //****************** LEGACY_CAL_TYPES ********************
// The following is now available above and in CALInternal.h
// The intention is not to publish anything in CALInternal

#include <oaidl.h>
#include "mmsystem.h"  // for timer functions

//---------------ecalFrameBuffer Enumeration------------
typedef enum ecalFrameBuffer
{
	ecalFrameBufferA = 1,
	ecalFrameBufferB = 2
} ecalFrameBuffer;

//--------------ecalOutputChannel Enumeration-----------
typedef enum ecalOutputChannel
{
	ecalOutputChannel1 = 0,
	ecalOutputChannel2 = 1
} ecalOutputChannel;

// Output types, for CALConnect()
// Now defined in GLAbstract\CGL.h (for GL Abstraction library)
#ifndef _OUTPUT_TYPES

typedef enum _OutputType {
	eCurrent = -1,	// compute via current context(s)
	eUnchanged = 0, // used by global GL() function
	eDuetNTSC = 1,  // Duet, in NTSC mode
	eSoftwareNTSC,   // Host preview, in NTSC mode
	eDualNTSC		// Dual (Duet & Host) mode
} enOutputType;

#define _OUTPUT_TYPES
#endif


//-------------- ecalVideoOptions Enumeration --------------
// indicate which options are specified to CALVideoDefaults()
// To allow backward-compatiblity when new options are supported,
// do not change these values or the order in which they are parsed!
/*
const ULONG ecalVIOFormat        = 0x00000001;
const ULONG ecalVIOSync          = 0x00000002;              
const ULONG ecalVIOMix           = 0x00000004;
const ULONG ecalVIOAlphaShape    = 0x00000008;
const ULONG ecalVIOAncillaryData = 0x00000010;
const ULONG ecalVIOKeyInput      = 0x00000020;
const ULONG ecalVIOHDelay        = 0x00000040;
const ULONG ecalVIOVDelay        = 0x00000080;
const ULONG ecalVIOTimeCode      = 0x00000100;
const ULONG ecalVIOBackplane2_3  = 0x00000200;
const ULONG ecalVIOBackplane4_5  = 0x00000400;
const ULONG ecalVIOBackplane6_7  = 0x00000800;
// this value should only reflect options available through VideoDefaults() call
// (21-Dec-98  mdt  User cannot select backplanes or enable time-code)
const ULONG ecalVIOAllOptions    = 0x000000ff;
*/

// W Byrne - 12/29/98 - redeclaring VideoDefault bit positions as an
//                      enumeration for CALX.

typedef enum ecalVideoOptions {
	ecalVIOFormat        = 0x00000001,
	ecalVIOSync          = 0x00000002,              
	ecalVIOMix           = 0x00000004,
	ecalVIOAlphaShape    = 0x00000008,
	ecalVIOAncillaryData = 0x00000010,
	ecalVIOKeyInput      = 0x00000020,
	ecalVIOHDelay        = 0x00000040,
	ecalVIOVDelay        = 0x00000080,
	ecalVIOTimeCode      = 0x00000100,
	ecalVIOBackplane2_3  = 0x00000200,
	ecalVIOBackplane4_5  = 0x00000400,
	ecalVIOBackplane6_7  = 0x00000800,
	ecalVIOAllOptions    = 0x000000ff
} ecalVideoOptions;



// Enums for 9 basic 2D justifications: upper-left to lower-right
// These are useful for users, but must be converted to eJustifyFlags
// for the justification routine to work, especially because
// we want to handle 3D justification as well.
typedef enum _Justify2D {
	eInvalidJustify=-1,
	eNoJustify=0,
	eUpperLeft=1,
	eUpperCenter,
	eUpperRight,
	eCenterLeft,
	eCenter,
	eCenterRight,
	eLowerLeft,
	eLowerCenter,
	eLowerRight
} enJustify;


//---------------- ecalStatus Enumeration ----------------
typedef enum ecalStatusEvent {
	ecalStatusTextual     = 0,
	ecalStatusPlayFinished = 1,
	ecalStatusSpoolPaused = 1000,
	ecalStatusSpoolResumed = 1001,
	ecalStatusSpoolObjectLevel  = 1010,
	ecalStatusBuildObjectBounds = 2000,
	ecalStatusBuildCelRange = 2100,
	ecalStatusError = 0xFFFFFFFF
} ecalStatusEvent;


typedef enum _ScaleModes {
	eScaleToFit=1,
	eMaintainAspect,
	ecalScaleClip,
	ecalOrigSize
} enScale;

typedef enum _EdgeTypes {
	eNone=0,
	eBorder,
	eDropShadow,
	eOffset
} eEdgeTypes;

// color-interpolation directions for backgrounds
typedef enum _LerpDir {
	eLeftToRight = 0,
	eRightToLeft,
	eTopToBottom,
	eBottomToTop
} eLerpDir;

// directions for Slide effect
typedef enum _SlideDir {
	eSlideFromLeft,
	eSlideFromRight,
	eSlideFromTop,
	eSlideFromBottom,
	eCrawlFromLeft,
	eCrawlFromRight
} enSlideDir;

// text wrap modes
typedef enum _WrapMode {
	eWordWrap=0,
	eCharWrap,
	eClip,
	eNoClip,
	eCompWord,
	eCompSpace,
	eScaleFit,
	eCurrentWrap,
	eCompLine
} enWrapMode;

// interpolation modes for animated effects
// (e.g,. CALTransform, CALPush, etc.)
typedef enum _eUpdateMode {
	ecalUpdateOnly=0,       // just change data - don't build
	ecalUpdateAndCache,     // don't display - store in cache
	ecalUpdateAndPlay,      // retrieve from cache
	ecalUpdateAndReplace,
	ecalUpdatePrevMode,
	ecalUpdateAndSpool,
	ecalUpdateAndSpoolEnd,
	ecalUpdateSelect,
	ecalUpdatePassiveCache,
	ecalUpdateSpoolSelect = 100,  // internal use only
	ecalUpdateTimer = 0x101, // internal use only 
} enUpdateMode;

// interpolation modes for animated effects
// (e.g,. CALTransform, CALPush, etc.)
typedef enum _interpMode {
	NoChange = -1,
	eJump,           // discrete points
	eLinear,         // uniformly interpolated
	eSpline          // cubic spline
} enInterpMode;

// play-state modes
typedef enum _playState {
	eInit,           // initial, not played yet
	ePaused,         // was playing, now paused
	ePlaying,        // currently playing
	eLooping,
	eFinished
} enPlayState;

// attribute types (animatable parameters)
typedef enum tagCALAnimatableAttribute
{
	ecalXPos = 0x1,
	ecalYPos = 0x2,
	ecalZPos = 0x4,
	ecalXRot = 0x8,
	ecalYRot = 0x10,
	ecalZRot = 0x20,
	ecalXScale = 0x40,
	ecalYScale = 0x80,
	ecalZScale = 0x100,
	ecalXCenter = 0x200,
	ecalYCenter = 0x400,
	ecalZCenter = 0x800,
	ecalRGBA	= 0x1000,
	ecalFocus = 0x2000,
	ecalShine = 0x4000,
	ecalLoops = 0x8000,	
	ecalAlpha  = 0x10000,	// 19-May-98 AG: added for CAL
	ecalEffectPct = 0x2000,
	ecalMethod = 0x40000
} enCALAttribute;

// Animation timing
typedef enum _enTiming
{
	ecalDuration,
	ecalRate,
	ecalOpenEnded
} enTiming;

// Animation ease
typedef enum _enEase
{
	ecalNoEase = 0,
	ecalEaseIn				= 0x1,
	ecalEaseOut				= 0x2,
	ecalEaseInOut			= 0x3,
	ecalStartOnScreen		= 0x4,
	ecalEndOnScreen			= 0x8,
	ecalStartEndOnScreen	= 0xC
} enEase, enEffectOptions;


// Loop mode
typedef enum _enLoopMode
{
	ecalLoopForward = 0,	// playback normally
	ecalLoopReverse,	//playback in reverse-frame
	ecalHold		// play once and hold for remaining frames
} enLoopMode;

typedef enum ecalNode
{
	ecalNodeInvalid = -1,
	ecalNodeConnection = 0,
	ecalNodeGPIOBoard0 = 0xFF000000,
	ecalNodeGPIOBoard1 = 0xFF010000,
	ecalNodeGPIOBoard2 = 0xFF020000,
	ecalNodeGPIOBoard3 = 0xFF030000,
	ecalNodeServer	   = 0xFF100000

} ecalNode;

//--------------- ecalSchedule Enumeration ---------------
typedef enum ecalSchedule {
	ecalSchedDateTimeOn = 0x01,
	ecalSchedDateTimeOff = 0x02,
	ecalSchedTimecodeSync = 0x10,
	ecalSchedGPIO = 0x1000
} ecalSchedule;

typedef enum _ecalTransVector
{
	ecalLeftToRight = 0,
	ecalRightToLeft,
	ecalTopToBottom,
	ecalBottomToTop,
} ecalTransVector;

typedef enum _ecalTransDir
{
	ecalDirDef		= 0x0,
	ecalDirRight	= 0x0,	// Horizontal Default
	ecalDirLeft		= 0x1,	// Horizontal Reverse
	ecalDirDown		= 0x2,	// Vertical Default
	ecalDirUp		= 0x3,	// Vertical Reverse
	ecalDirOut		= 0x4,	// Box Default
	ecalDirIn		= 0x5,	// Box Reverse
	ecalDirCW		= 0x6,	// Radial Default
	ecalDirCCW		= 0x7	// Radial Reverse
} ecalTransDir;

typedef enum _ecalTransPattern
{
	ecalPatternDef	= 0x0,
	ecalHorizSlide	= 0x1,	// Wipe/Push/Hide/Reveal
	ecalPatternA	= 0x1,
	ecalVertSlide	= 0x2,	// Wipe/Push/Hide/Reveal
	ecalPatternB	= 0x2,
	ecalHorizSplit	= 0x3,	// Wipe only
	ecalPatternC	= 0x3,
	ecalVertSplit	= 0x4,	// Wipe only
	ecalPatternD	= 0x4,
	ecalBox			= 0x5,	// Wipe only
	ecalPatternE	= 0x5,
	ecalRadial		= 0x6	// Wipe only
} ecalTransPattern;

typedef enum _enTransType
{
	ecalCut,
	ecalFade,
	ecalWipe,
	ecalReveal,
	ecalHide,
	ecalDissolve,
	ecalPush,
	ecalWipeDslv,
	ecalRevealDslv,
	ecalHideDslv,
	ecalPushDslv
} enTransType;

typedef enum _ecalTransOption
{
	ecalWipeSoftness,
	ecalDissolveAlpha
} ecalTransOption;

typedef enum _ecalChangeMode
{
	ecalScaleAll,
	ecalHoldEnd
} ecalChangeMode;

typedef enum _ecalFileType
{
	ecalInfinitRGB = 1,
	ecalFileFormatBMP, 
	ecalFileFormatTIFF,
	ecalFileFormatJPEG,
	ecalFileFormatTGA,
	ecalFileFormatRLE,
	ecalFileFormatPPM,
	ecalFileFormatGIF,
	ecalFileFormatCSF,
	ecalFileFormatMOV,
	ecalFileFormatAVI,
	ecalOtherFileFormat
} ecalFileType;

// The region type information
typedef enum _ecalRegType
{
	ecalText = 1,
	ecalImage,
	ecalVideo, 
	ecal3D,
	ecalComposite
} ecalRegType;

// The request type of information for the region
typedef enum _ecalInfoType
{
	ecalInfoBounds = 0x01,
	ecalInfoOverrun = 0x02,
	ecalInfoType = 0x04
} ecalRegInfoType;

typedef enum _ecalVideoMode
{
	ecalFormatNTSC = 1,
	ecalFormatPAL,
	ecalFormat720P,
	ecalFormat1080I,
	ecalFormatDefault,
	ecalFormatCurrent
} ecalVideoMode;
typedef enum _ecalStatus
{
	NotImplementYet
} ecalStatus;

typedef enum _ecalTextStatus
{
	ecalTextStatusTooWide = -1,
	ecalTextStatusUncompressed = 0,
	ecalTextStatusCompressed = 1
} ecalTextStatus;


typedef enum _ecalObjCenter
{
	ecalRelativeObj = 0,
	ecalRelativeRgn
} ecalObjCenter;

typedef enum _ecalLightTypes
{
	ecalGlobal = 0,
	ecalDirectional,
	ecalPointLight,
	ecalSpotlight
} ecalLightTypes;


typedef enum _ecalLightState
{
	ecalLightOff = 0,
	ecalLightOn
} ecalLightState;

typedef enum _ecalFreeTime
{
	ecalFreeNow,
	ecalFreeAfterPlaying,
	ecalFreeUponExit
} ecalFreeTime;

typedef enum _ecalRenderQuality
{
	ecalFast = 0,	// aliased
	ecalQuality		// anti-aliased
} ecalRenderQuality;

typedef enum _ecalClockMode
{
	ecal12Hour,
	ecal24Hour
} ecalClockMode;

typedef enum _ecalTimerMode
{
	ecalCountUp,
	ecalCountDown
} ecalTimerMode;

typedef enum ecalDeviceType {
	ecalDeviceSocket	= 0xFFFFFFFF,
	ecalDeviceCodi0		= 0xFFFFFFFE,
	ecalDeviceCodi1		= 0xFFFFFFFD, 
	ecalDeviceCodi2		= 0xFFFFFFFC, 
	ecalDeviceCodi3		= 0xFFFFFFFB,
	ecalDeviceSDMixer	= 0xFFFFFFFA,
	ecalDeviceSqueeze	= 0xFFFFFFF9
} ecalDeviceType;

//-------------- ecalMotionPath Enumeration ----------------
typedef enum ecalMotionPath {
	ecalMotionXY = 0,
	ecalMotionXZ,
	ecalMotionYZ,
	ecalMotionT3,
	ecalMotionJump,
	ecalMotionLinear,
	ecalMotionSpline
} ecalMotionPath;

//---------------ecalCreationMode Enumeration-----------
typedef enum ecalCreationMode
{
	ecalUseExisting = 0,
	ecalForceUnique
} ecalCreationMode;

// Publish this define
#ifndef ecalDblNoChange
#include <limits>
#define ecalDblNoChange std::numeric_limits<double>::infinity()
#endif

#endif
#endif