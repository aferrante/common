//CAL.h

#ifndef CAL_API_INCLUDED
#define CAL_API_INCLUDED

#ifndef _CALDIR_H
#include "LIB\caldir.h"
#include "LIB\callib.h"
#endif

#include "..\..\..\..\Common\LAN\NetUtil.h"

#define VIDEO_HW_UNDEFINED  0
#define VIDEO_HW_DUET_LEX   1
#define VIDEO_HW_DUET_SD    2
#define VIDEO_HW_VGA        3

/*
// From Frank Kobelinski
typedef struct SqueezeEffect_t
{
  int iThisStructSize, iEndHeight, iEndHeight2, iEndWidth, iEndWidth2, 
    iEndingX, iEndingX2, iEndingY, iEndingY2;
  int iFields, iFields2, iStartHeight, iStartHeight2, iStartWidth, 
    iStartWidth2, iStartX, iStartX2, iStartY, iStartY2, iDisplayOn, 
    iDisplayOn2;
  int iEase, iEase2, iFadeOn, iFadeOn2, iClipTop, iClipTop2, 
    iClipBottom, iClipBottom2, iClipLeft, iClipLeft2, iClipRight, 
    iClipRight2;
  int iInterpolate, iGraphicsOnTop, iResizer2Over1;
  int iBackGroundON, iGraphicPlaneOn, iResizer1On, iResizer2On;
  int iBkg_Frame_Delay_On, iMsg, iDissolveEffect;
  int iGPI1;    // Forward GPI Number
  int iGPI2;    // Reverse GPI Number
  int bSlotActive;// Squeeze Effect Active Flag.
  char szName[10][21];  // user names for effects
  int unused[100];
} SqueezeEffect_t;

*/


#define CALCheckMD(FUNC, LNUM, FAIL) \
  { \
    CString szTemp666; HRESULT hr = FUNC; \
    if(FAILED(hr))\
    { \
      if (g_pduet != NULL) { \
      szTemp666.Format("%s: CAL Error: %s, line %d", __FILE__, (LPCTSTR)g_pduet->printCALError(hr), LNUM);\
      g_md.DM(0,0,0, "%s|CheckGDM", szTemp666); \
			if(FAIL!=NULL) *FAIL = TRUE;\
      }\
      throw(FUNC); \
    } \
		else if(FAIL!=NULL) *FAIL = FALSE;\
  }



class CCALUtil // : public CObject
{
public:
	CCALUtil();   // standard constructor
	~CCALUtil();   // standard destructor

public:
  bool m_bPreview;
	bool m_bConnected;
  int  m_nVideoFormat; // ecalFormatNTSC, ecalFormatPAL, ecalFormat720P, ecalFormat1080I
  char m_szIPAddress[64];
  HCAL m_hcalConnId;
  CNetUtil m_net;
  
  // Hardware info:
  int			m_nVideoHardwareType;
  double	m_dfCALVersion;
  DWORD		m_dwCALBuild;

  int			m_nCodiSqueezeBoardNumber;

  int     Connect(CString szDuetAddress, bool bUseDuet, int nDuetVideoFormat=ecalFormatNTSC, CString* pszInfo=NULL);
  int     Disconnect(CString* pszInfo=NULL);
  int     Shutdown(CString* pszInfo=NULL);
  int     GetVideoDimensions(int *nWidth, int *nHeight);
  int     FileExists(char *fn);
  void    RGB2HSV(double r, double g, double b, double *h, double *s, double *v);
  void    HSV2RGB(double h, double s, double v, double *r, double *g, double *b);
  void    SetBGBright(double *hout, double *sout, double *vout);
  char*   CALError(unsigned long errcode);
  int     MapFontString(CString szFontString, CString *szFont, int *PointSize, int *nStyle, COLORREF *crColor);
  int     GetHardwareInfo(int* pnHardwareType, double* pdfCALVersion, DWORD* pdwBuild);

  // CAL Utils
  char* CALAppendEvent(char* pszString, char* pszEventString, bool bDestroyInputBuffer=true);
  char* CALEncodeBrackets(char* pszString, bool bDestroyInputBuffer=true);
  bool  GetFieldHandle(HCAL rgnScene, CString szRgnName, HCAL *rgnHandle);
  bool  GetProjectedVertices(const char* pszSceneName, const char* pszRgnName,
                  double* pfdVrtX0, double* pdfVrtY0, double* pdfVrtX1, double* pdfVrtY1);


  // Main squeezeback entry points
  void SetupSqueezeback();
  void ShutdownSqueezeback();
  void DoSqueezeIn(int nSqueezeFrames,
                   int nSqueezeX, int nSqueezeY,
                   int nSqueezeW, int nSqueezeH,
                   int nXCorrection, int nYCorrection,
                   int nLayerCAL, int nLayerSqueeze);
  void DoSqueezeOut(int nSqueezeFrames,
                    int nSqueezeX, int nSqueezeY,
                    int nSqueezeW, int nSqueezeH,
                    int nXCorrection, int nYCorrection);

  // Duet Squeezeback
  void SetVideoSync(long nSync);
  void SetVideoMixing(BOOL bMix);
  void SetupSqueezebackDuet();
  void ShutdownSqueezebackDuet();
  void Mix(BOOL bEnable, int nLayerCAL, int nLayerSqueeze);
  void MixDefault(BOOL bEnable);
  void DoSqueezeInDuet(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                   int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection,
                   int nLayerCAL, int nLayerSqueeze);
  void DoSqueezeOutDuet(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                    int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection);

  // Codi Squeezeback
  void SetupSqueezebackCodi();
  void ShutdownSqueezebackCodi();
  void DoSqueezeInCodi(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                   int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection,
                   int nLayerCAL, int nLayerSqueeze);
  void DoSqueezeOutCodi(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                    int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection);
  CString BuildCodiCommand(int vEffectId, 
                           int vRegion, int StartX, int EndX,
                           int StartY, int EndY, int StartWidth, 
                           int EndWidth,
                           int StartHeight, int EndHeight, int Frames,
                           int Ease, int FadeOn, int ClipTop, 
                           int ClipBottom, int ClipLeft, int ClipRight,
                           int GraphicsPlaneOn, int BackgroundOn, 
                           int ResizerOn, int GraphicsOnTop,
                           int Resizer2Over1, 
                           int BackgroundFrameDelay, 
                           int DissolveEffect);

};


#endif //CAL_API_INCLUDED

