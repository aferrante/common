// IITestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IITest.h"
#include "IITestDlg.h"
#include <process.h>
#include "..\..\..\TXT\BufferUtil.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void LoadFieldsThread(void* pvArgs);
void SocketMonitorThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIITestDlg dialog

CIITestDlg::CIITestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIITestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIITestDlg)
	m_szCmd = _T("");
	m_szHost = _T("127.0.0.1");
	m_szPort = _T("1337");
	m_szScene = _T("");
	m_szValue = _T(" ");
	m_szField = _T("");
	m_szDelim = _T("�");
	m_bUpdateFields = FALSE;
	m_bUpdateValues = FALSE;
	m_bSendAuto = FALSE;
	m_szDSN = _T("Libretto");
	m_szPw = _T("");
	m_szUser = _T("sa");
	m_nRecvTimeout = 60000;
	m_nSendTimeout = 60000;
	m_bUTF8 = FALSE;
	m_szHex = _T("");
	m_bStar = FALSE;
	m_szAlias = _T("9900");
	m_szLayer = _T("1");
	m_nFlavor = 0;
	m_bThumbCP = FALSE;
	m_bLogTransactions = FALSE;
	//}}AFX_DATA_INIT

	m_bInCommand = FALSE;
	m_szQueueTableName =  _T("Queue");
	m_szLibrettoFileLocation =  _T("libretto.csf");

	//��#
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_s = NULL;
	m_net.Initialize();
	m_nNumScenes = 0;
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_sizeDlgMin = CSize(488,609);
	
	m_bMonitorSocket=FALSE;
	m_bSocketMonitorStarted=FALSE;
	m_bStates = FALSE;

}

void CIITestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIITestDlg)
	DDX_Control(pDX, IDC_LIST3, m_lcResponses);
	DDX_Control(pDX, IDC_LIST2, m_lcValues);
	DDX_Control(pDX, IDC_LIST1, m_lcFields);
	DDX_Text(pDX, IDC_EDIT_COMMAND, m_szCmd);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_CBString(pDX, IDC_COMBO_PORT, m_szPort);
	DDX_CBString(pDX, IDC_COMBO_SCENE, m_szScene);
	DDX_CBString(pDX, IDC_COMBO_VALUE, m_szValue);
	DDX_CBString(pDX, IDC_COMBO_FIELD, m_szField);
	DDX_CBString(pDX, IDC_COMBO_DELIM, m_szDelim);
	DDX_Check(pDX, IDC_CHECK_SENDQ, m_bUpdateFields);
	DDX_Check(pDX, IDC_CHECK_SENDQ2, m_bUpdateValues);
	DDX_Check(pDX, IDC_CHECK_SEND, m_bSendAuto);
	DDX_Text(pDX, IDC_EDIT_DSN, m_szDSN);
	DDX_Text(pDX, IDC_EDIT_PW, m_szPw);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_RECVTM, m_nRecvTimeout);
	DDX_Text(pDX, IDC_EDIT_SENDTM, m_nSendTimeout);
	DDX_Check(pDX, IDC_CHECK_UTF8, m_bUTF8);
	DDX_CBString(pDX, IDC_COMBO_HEX, m_szHex);
	DDX_Check(pDX, IDC_CHECK_STAR, m_bStar);
	DDX_CBString(pDX, IDC_COMBO_ALIAS, m_szAlias);
	DDX_CBString(pDX, IDC_COMBO_BUFFER, m_szLayer);
	DDX_Radio(pDX, IDC_RADIO_CB, m_nFlavor);
	DDX_Check(pDX, IDC_CHECK_THUMB_CP, m_bThumbCP);
	DDX_Check(pDX, IDC_CHECK_LOGXACT, m_bLogTransactions);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CIITestDlg, CDialog)
	//{{AFX_MSG_MAP(CIITestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_AN, OnButtonQuery)
	ON_BN_CLICKED(IDC_BUTTON_AV, OnButtonQueryValues)
	ON_BN_CLICKED(IDC_BUTTON_CL, OnButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_DELIM, OnButtonDelim)
	ON_BN_CLICKED(IDC_BUTTON_LO, OnButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_OP, OnButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_PL, OnButtonPlay)
	ON_BN_CLICKED(IDC_BUTTON_ST, OnButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUpdate)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, OnClickList1)
	ON_NOTIFY(NM_CLICK, IDC_LIST2, OnClickList2)
	ON_CBN_CLOSEUP(IDC_COMBO_SCENE, OnCloseupComboScene)
	ON_BN_CLICKED(IDC_CHECK_SEND, OnCheckSend)
	ON_CBN_SELCHANGE(IDC_COMBO_SCENE, OnSelchangeComboScene)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT2, OnButtonConnect2)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_HEX, OnButtonHex)
	ON_CBN_EDITCHANGE(IDC_COMBO_HEX, OnEditchangeComboHex)
	ON_CBN_CLOSEUP(IDC_COMBO_HEX, OnCloseupComboHex)
	ON_CBN_CLOSEUP(IDC_COMBO_ALIAS, OnCloseupComboAlias)
	ON_BN_CLICKED(IDC_BUTTON_RECV, OnButtonRecv)
	ON_BN_CLICKED(IDC_RADIO_CB, OnRadioCb)
	ON_BN_CLICKED(IDC_RADIO_ICON, OnRadioIcon)
	ON_BN_CLICKED(IDC_RADIO_LYR, OnRadioLyr)
	ON_BN_CLICKED(IDC_BUTTON_POLL, OnButtonPoll)
	ON_BN_CLICKED(IDC_BUTTON_QS, OnButtonQueryScene)
	ON_BN_CLICKED(IDC_BUTTON_QS2, OnButtonQueryState)
	ON_BN_CLICKED(IDC_BUTTON_TH, OnButtonTh)
	ON_BN_CLICKED(IDC_BUTTON_VIEWLOG, OnButtonViewlog)
	ON_BN_CLICKED(IDC_RADIO_G7, OnRadioG7)
	ON_CBN_SELCHANGE(IDC_COMBO_BUFFER, OnSelchangeComboBuffer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIITestDlg message handlers

BOOL CIITestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	CRect rc; 
	m_lcFields.GetWindowRect(&rc);
	m_lcFields.InsertColumn(0, "", LVCFMT_LEFT, rc.Width(), 0 );

	m_lcValues.GetWindowRect(&rc);
	m_lcValues.InsertColumn(0, "", LVCFMT_LEFT, rc.Width(), 0 );


	m_lcResponses.GetWindowRect(&rc);
	m_lcResponses.InsertColumn(0, "Command", LVCFMT_LEFT, rc.Width()/3, 0 );
	m_lcResponses.InsertColumn(1, "Response", LVCFMT_LEFT, rc.Width()-16-(rc.Width()/3), 1 );


	((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->SelectString(-1, m_szLayer);

	
	// TODO: Add extra initialization here
	FILE* fp = fopen("iihost.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;int nSelP=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				int q=0;
				if(*pch=='*')
				{
					nSel = i; q=1; 
					pch++;
				}
				if(strlen(pch))
				{
				
					char* pchDel = strchr(pch, ':');
					if(pchDel)
					{
						*pchDel = 0; pchDel++;
					}

					if(strlen(pch)) {((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch); i++;}
				
					CComboBox* ptr = ((CComboBox*)GetDlgItem(IDC_COMBO_PORT));

					if((pchDel)&&(strlen(pchDel)))
					{
						int s = ptr->FindStringExact( -1, pchDel );
						if(s==CB_ERR)
						{
							ptr->AddString(pchDel);
						}
						if(q==1)
						{
							nSelP=ptr->FindStringExact( -1, pchDel );
						}
					}
					else
					{
						int s = ptr->FindStringExact( -1, "1337" );
						if(s==CB_ERR)
						{
							ptr->AddString("1337");
						}
						if(q==1)
						{
							nSelP=ptr->FindStringExact( -1, "1337" );
						}
					}
				}

				pch = strtok(NULL, "\r\n");
				
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				if (nSelP>=0)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SetCurSel(nSelP);
				}
				else
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SetCurSel(0);
				}


				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}
	
	fp = fopen("iiscenes.cfg", "rb");

	m_nNumScenes=0;
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; 
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					m_scenes[m_nNumScenes].bSel = true;
					pch++;
				}
				else
				{
					m_scenes[m_nNumScenes].bSel = false;
				}
				if(strlen(pch))
				{
					char* pchDel = strchr(pch, '|');
					if(pchDel)
					{
						*pchDel = 0; pchDel++;
					}

					if(strlen(pch))
					{
						m_scenes[m_nNumScenes].szName.Format("%s", pch);
						m_scenes[m_nNumScenes].nNumFields = 0;
//AfxMessageBox(pch);
						while((pchDel)&&(strlen(pchDel))&&(m_scenes[m_nNumScenes].nNumFields<128))
						{
							pch = strchr(pchDel, '|');
							if(pch)
							{
								*pch = 0; pch++;
							}
//AfxMessageBox(pchDel);

							if(strlen(pchDel))
							{
								m_scenes[m_nNumScenes].szFields[m_scenes[m_nNumScenes].nNumFields].Format("%s", pchDel);
								m_scenes[m_nNumScenes].nNumFields++;
							}
							pchDel = pch;
						}
						m_nNumScenes++;

					}
				}

				pch = strtok(NULL, "\r\n");
				
			}

			i=0;
			while(i<m_nNumScenes)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_scenes[i].szName);
				if (m_scenes[i].bSel)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SetCurSel(i);
					m_szScene = m_scenes[i].szName;
					UpdateData(TRUE);
					LoadFields();
				}
				i++;

			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CIITestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIITestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIITestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CIITestDlg::OnButtonConnect() 
{
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		m_bMonitorSocket=FALSE;
		while(m_bSocketMonitorStarted) Sleep(10);

		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
		GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_CONNECT2)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
	}
	else
	{
		UpdateData(TRUE);
//		char conn[256];
//		sprintf(conn, "%s", m_szHost);  // was m_szIP
//		AfxMessageBox(m_szHost);

		if((m_szHost.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
		}
		if((m_szPort.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->FindStringExact( -1, m_szPort )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->AddString(m_szPort);
			((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SelectString(-1, m_szPort);
		}
		char szError[8096];
		
		if(m_nSendTimeout<=0)
		{
			m_nSendTimeout=60000;
			GetDlgItem(IDC_EDIT_SENDTM)->SetWindowText("60000");
		}
		if(m_nRecvTimeout<=0)
		{
			m_nRecvTimeout=60000;
			GetDlgItem(IDC_EDIT_RECVTM)->SetWindowText("60000");
		}

		if(m_net.OpenConnection(m_szHost.GetBuffer(0), (short)(atol(m_szPort)), 
			&m_s, 
			m_nSendTimeout,
			m_nRecvTimeout,
			szError)<NET_SUCCESS)
		{
			m_s = NULL;
			AfxMessageBox(szError);
		}
		else
		{
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect host");
			GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);

			m_bMonitorSocket=FALSE;
			while(m_bSocketMonitorStarted) Sleep(10);

			m_bMonitorSocket=TRUE;
			_beginthread(SocketMonitorThread, 0, (void*)this);

		}
	}
}

void CIITestDlg::OnButtonSend() 
{
	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
	{
		UpdateData(TRUE);
		char szError[8096];
		char szSQL[DB_SQLSTRING_MAXLEN];
		int nItem = clock();

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username, event_itemid) \
VALUES ('%s','', 256, '%s', 0, 'sys', %d)", //HARDCODE
			m_szQueueTableName.GetBuffer(0),
			m_szCmd.GetBuffer(0),
			m_szHost.GetBuffer(0),
			nItem
		);
//AfxMessageBox(szSQL);


		int nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
		if(nReturn<DB_SUCCESS)
		{
			CString foo;
			foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
			AfxMessageBox(foo);
		}
		else
		{
			//recv the response.
			Sleep(250);

			char szSQL[DB_SQLSTRING_MAXLEN];
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action=128 and event_itemid = %d", //HARDCODE
							m_szQueueTableName.GetBuffer(0),
							nItem);
			int nIndex = 0;
			while(nIndex<=0)
			{
				Sleep(100);
//AfxMessageBox(szSQL);
				CRecordset* prs = m_db.Retrieve(m_db.m_ppdbConn[0], szSQL, szError);
				if(prs)
				{
		//			while ((!prs->IsEOF()))
					if (!prs->IsEOF())  // just do the one record, if there is one
					{
						try
						{
							CString szMessage;
							prs->GetFieldValue("remote", szMessage);//HARDCODE
//AfxMessageBox(szMessage);
							int nCount = m_lcResponses.GetItemCount();
							m_lcResponses.InsertItem(nCount, m_szCmd);
							m_lcResponses.SetItemText(nCount, 1, szMessage.GetBuffer(0) );
							m_lcResponses.EnsureVisible(nCount, FALSE);

							nIndex++;
						}
						catch(CException* e)// CDBException *e, CMemoryException *m)  
						{
AfxMessageBox("exception");
							e->Delete();
						} 
						catch( ... )
						{
AfxMessageBox("exception2");
						}

						prs->MoveNext();
					}

					prs->Close();

					delete prs;
				}
			}
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE event_itemid = %d", //HARDCODE
				m_szQueueTableName.GetBuffer(0),
				nItem);

//AfxMessageBox(szSQL);
			nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
			if(nReturn<DB_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
		
		}
	}
	else
	if(m_s)
	{
		m_bInCommand = TRUE;
		m_nClock = clock();
		UpdateData(TRUE);
		char szError[8096];
		int nReturn;
		if(m_nSendTimeout<=0)
		{
			m_nSendTimeout=60000;
			GetDlgItem(IDC_EDIT_SENDTM)->SetWindowText("60000");
		}
		nReturn=	m_net.SendLine((unsigned char*)m_szCmd.GetBuffer(0), m_szCmd.GetLength(), m_s, EOLN_CRLF, false, m_nSendTimeout, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString foo;
			foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
			AfxMessageBox(foo);
		}
		else
		{

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Sent: ", logtmbuf, timestamp.millitm);
				fwrite(m_szCmd.GetBuffer(0), sizeof(char), m_szCmd.GetLength(), logfp);
				fprintf(logfp, "%c%c", 13, 10);
				fclose(logfp);
			}
}

			GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			m_nClock = clock();
			//recv the response.
	//		unsigned char* puc2 = NULL;
	//		unsigned long ulLen2=0
			unsigned char* puc = NULL;
			unsigned long ulLen=0;
			nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
			if(nReturn<NET_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
			else
			{
/*
				//check if more data:
				bool bMore=false;

				timeval tv;
				tv.tv_sec = 0; 
				tv.tv_usec = 500000;  // timeout value
				int nNumSockets;
				fd_set fds;
				FD_ZERO(&fds);  // Zero this out each time
				FD_SET(m_s, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);
				if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
				{
					
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(m_s, &fds)))
					) 
				{ 

				} 
				else // there is recv data.
				{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
					//recv the response.
					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, char* szError);
					if(nReturn<NET_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
						AfxMessageBox(foo);
					}
					else
					{
						if(ulLen==0) //disconnected
						{
							GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
							GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
							GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
							m_net.CloseConnection(m_s);
							m_s = NULL;
							AfxMessageBox("disconnected");
						}
						else
							bMore=true;
					}
				}
				if(bMore)
				{// use the second set to parse
				}
*/


				if(ulLen==0) //disconnected
				{
					GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
					GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
					m_net.CloseConnection(m_s);
					m_s = NULL;
					AfxMessageBox("disconnected");
				}
				else
				{
					if(puc)
					{

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();
						m_lcResponses.InsertItem(nCount, m_szCmd);
						m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						m_lcResponses.EnsureVisible(nCount, FALSE);

						if(	m_szCmd.Left(5).CompareNoCase("B\\TH\\")==0)
						{


							// here, do the thumnail thing if applicable.
						// chances are, we got the part that tells us how much data we are supposed to be getting.
						// let's make sure we have complete data, using the  length, rather than the EOLN_TOKEN method, just in case tehre is a \\CRLF sequence coincidentally in the jpg image byte sequence.
	//				AfxMessageBox((char*)puc);
	//				CString qqqqq; 

								
	//The response result will take the form:
	//*B\TH\SceneId\ByteCount\ByteSequence\\ <CR><LF>
							char bufferme[128];
							sprintf(bufferme,"*B\\TH\\%s\\", m_szScene);


							int nLen =strlen(bufferme);
							if(strnicmp((char*)puc, bufferme, nLen)==0)
							{
								//parse it!
								int nBytes = atoi((char*)(puc+nLen));
								if(nBytes>0)
								{

									sprintf(bufferme,"%d\\", nBytes);
									nLen += strlen(bufferme);

	//				qqqqq.Format("%d total bytes in response\r\n%d bytes expected for image",ulLen, nBytes); AfxMessageBox(qqqqq);

									bool bMore=false;
									unsigned char* puc2 = NULL;
									unsigned long ulLen2=0;
									if(ulLen<(unsigned int)(nBytes+nLen+4))  //+4 because of the ending CRLF
									{
										// have to receive more data.
										//check if more data:
										ulLen2 = (nBytes+nLen+4)-ulLen;

										timeval tv;
										tv.tv_sec = 2; 
										tv.tv_usec = 500000;  // timeout value
										int nNumSockets;
										fd_set fds;
										FD_ZERO(&fds);  // Zero this out each time
										FD_SET(m_s, &fds);
										nNumSockets = select(0, &fds, NULL, NULL, &tv);
										if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
										{
											
										}
										else
										if(
												(nNumSockets==0) // 0 = timed out, -1 = error
											||(!(FD_ISSET(m_s, &fds)))
											) 
										{ 

										} 
										else // there is recv data.
										{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
											//recv the response.

											nReturn=m_net.GetLine(&puc2, &ulLen2, m_s, NET_RCV_LENGTH, szError);
											if(nReturn<NET_SUCCESS)
											{
												CString foo;
												foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
												AfxMessageBox(foo);
											}
											else
											{
												if(ulLen2==0) //disconnected
												{
													GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
													GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
													GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
													m_net.CloseConnection(m_s);
													m_s = NULL;
													AfxMessageBox("disconnected");
												}
												else
													bMore=true;
											}
										}
									//	if((bMore)&&(puc2))
									//	{// use the second set to parse

	//										}




									}

									CString resp = (char*)puc ;

									if(puc2)
									{
										resp += puc2;
									}

									m_lcResponses.SetItemText(nCount, 1, resp);
									m_lcResponses.EnsureVisible(nCount, FALSE);


									_mkdir("images");

									if(m_bThumbCP)
										sprintf(bufferme,"images\\%s-cp.jpg", m_szScene);
									else
										sprintf(bufferme,"images\\%s.jpg", m_szScene);

									FILE* fp;
									fp = fopen(bufferme, "wb");
									if(fp)
									{
										fwrite((puc+nLen), sizeof(char), ulLen-nLen, fp );

										if((bMore)&&(puc2)&&(ulLen2))
										{

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				fwrite((char*)puc2, sizeof(char), ulLen2, logfp);
				fclose(logfp);
			}
}
											fwrite(puc2, sizeof(char), ulLen2, fp );
										}

										fclose(fp);

										HINSTANCE hi;
										hi=ShellExecute((HWND) NULL, NULL, bufferme, NULL, NULL, SW_HIDE);

									}
									else
									{
										CString szAlert;
										szAlert.Format("Could not write file. response was:\r\n%s",(char*)puc);
										if(puc2)
										{
											szAlert+= puc2;
										}
										AfxMessageBox(szAlert);							
									}	
									
									if(puc2)
									{
										free(puc2);
									}



								}
								else
								{
									CString szAlert;
									szAlert.Format("Byte sequence not recognized, response was:\r\n%s",(char*)puc);
									AfxMessageBox(szAlert);							
									m_lcResponses.SetItemText(nCount, 1, (char*)puc);
									m_lcResponses.EnsureVisible(nCount, FALSE);

								}

							}
							else
							{
								CString szAlert;
								szAlert.Format("Invalid response:\r\n%s",(char*)puc);
								AfxMessageBox(szAlert);			
								m_lcResponses.SetItemText(nCount, 1, (char*)puc);
								m_lcResponses.EnsureVisible(nCount, FALSE);

							}
						}

/*
						if(	m_szCmd.Left(5).CompareNoCase("B\\TH\\")==0)
						{
							// parse answer for thumbnail.
							char* pch = (char*)puc;
							char filename[MAX_PATH];

							sprintf(filename, "*B\\TH\\%s\\", m_szScene);
							if(strstr(pch, filename))
							{
								pch+= strlen(filename);
					//			AfxMessageBox(pch);
								int nLen = atoi(pch);
								if(nLen>0)
								{
									char* pchSeek = strchr(pch, '\\');
									if(pchSeek)
									{
										sprintf(filename, "%s%s.jpg", m_szScene, (m_szField.CompareNoCase("ControlPanel")?"":"-cp"));

						//				AfxMessageBox(filename);

										FILE* fp = fopen(filename, "wb");
										if(fp)
										{
											pchSeek++;
											int i=0;
											while(i<nLen)
											{
												fprintf(fp, "%c", (*pchSeek));
												pchSeek++;
												i++;
											}
											fflush(fp);
											fclose(fp);
											CString szBox;
											szBox.Format("Thumbnail retrieved and saved to %s.",filename);
											AfxMessageBox(szBox);
										}
									}

								}
							}

						}

*/
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
						free(puc);
					}
					else
					{
						AfxMessageBox("valid repsonse not received");
					}
				}
			}

		}
		m_bInCommand = FALSE;

	}
	else
	{
		AfxMessageBox("no connection");
	}	
}

void CIITestDlg::OnDestroy() 
{

	CDialog::OnDestroy();
	
}

void CIITestDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	FILE* fp = fopen("iihost.cfg", "wb");
	if(fp)
	{
		UpdateData(TRUE);
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		int nCountP=((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetCount();
		int nSelP=((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s:%s\n", m_szHost, m_szPort);
		}
		if(nCount>0)
		{
			int i=0;  int j=0;
			while(i<max(nCount, nCountP))
			{
				CString szText, szPort;
				if(i<nCount)((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				else szText="";

				if(nSel==i)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetLBText( nSelP, szPort );
					fprintf(fp, "*");
					fprintf(fp, "%s:%s\n", szText, szPort);
				}
				else
				{
					if(nSelP==j)
					{
						j++;
					}
					if(j<nCountP)
					{
						((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetLBText( j, szPort );
					}
					else szPort="";
					fprintf(fp, "%s:%s\n", szText, szPort);
				}
				j++;
				i++;
			}
		}
		fclose(fp);
	}
/* // I was thinking the following coul dbe used to compare what is in the file currently and add only new things... but just erase and rewrite
	fp = fopen("iiscenes.cfg", "rb");

	int nNumScenes=0;
	cbscene_t scenes[512];
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero
			int i=0; 
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					scenes[nNumScenes].bSel = true;
					pch++;
				}
				else
				{
					scenes[nNumScenes].bSel = false;
				}
				if(strlen(pch))
				{
					char* pchDel = strchr(pch, '|');
					if(pchDel)
					{
						*pchDel = 0; pchDel++;
					}

					if(strlen(pch))
					{
						scenes[nNumScenes].szName.Format("%s", pch);
						scenes[nNumScenes].nNumFields = 0;
//AfxMessageBox(pch);
						while((pchDel)&&(strlen(pchDel))&&(scenes[nNumScenes].nNumFields<128))
						{
							pch = strchr(pchDel, '|');
							if(pch)
							{
								*pch = 0; pch++;
							}
//AfxMessageBox(pchDel);

							if(strlen(pchDel))
							{
								scenes[nNumScenes].szFields[scenes[nNumScenes].nNumFields].Format("%s", pchDel);
								scenes[nNumScenes].nNumFields++;
							}
							pchDel = pch;
						}
						nNumScenes++;

					}
				}

				pch = strtok(NULL, "\r\n");
				
			}
		}
		fclose(fp); fp=NULL;
	}
*/
	fp = fopen("iiscenes.cfg", "wb");  // was write, but lets do append

	if(fp)
	{
		UpdateData(TRUE);
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szScene.GetLength()>0)
				fprintf(fp, "*%s\n", m_szScene);
		}
		if(nCount>0)
		{
			int q=0;
			while(q<m_nNumScenes)
			{
				if(m_scenes[q].bSel)
				{
					fprintf(fp, "*");
				}
				fprintf(fp, "%s", m_scenes[q].szName);

				int i=0;
				while(i<m_scenes[q].nNumFields)
				{
					fprintf(fp, "|%s", m_scenes[q].szFields[i]);
					i++;
				}
				fprintf(fp, "\n");

				q++;
			}
		}
		fclose(fp);
	}

	CDialog::OnCancel();
}

void CIITestDlg::OnButtonQuery() 
{
	UpdateData(TRUE);
	if(m_szScene.GetLength())
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}

		if(m_nFlavor==II_TYPE_G7)
		{

			CString szScribe;
			CString szLayout;

			int nColon = m_szScene.ReverseFind(':');
			if(nColon>=0)
			{
				szScribe=m_szScene.Left(nColon);
				szLayout = m_szScene.Mid(nColon+1);
			}
			else
			{
				szScribe = m_szScene;
				szLayout = "1"; // "safe" default
			}

/*
1 returns tag names only.
R\I\5\7\[.scribelist file]\[.scribelist event]\1\[tag 1
name]\[tag 2 name]\ ... \[tag n name]\\
2 returns tag names and their data.
R\I\5\7\[.scribelist file]\[.scribelist event]\2\[tag 1
name]\[tag 1 text]\ ... \[tag n name]\[tag n text]\\
*/
			m_szCmd.Format("I\\5\\7\\%s\\%s\\1\\\\", szScribe, szLayout );
		}
		else
			m_szCmd.Format("B\\AN\\%s\\\\", m_szScene);


		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if((m_bUpdateFields)||(m_bSendAuto))
		{
			if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
			{
				UpdateData(TRUE);
				char szError[8096];
				char szSQL[DB_SQLSTRING_MAXLEN];
				int nItem = clock();

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username, event_itemid) \
		VALUES ('%s','', 256, '%s', 0, 'sys', %d)", //HARDCODE
					m_szQueueTableName.GetBuffer(0),
					m_szCmd.GetBuffer(0),
					m_szHost.GetBuffer(0),
					nItem
				);
		//AfxMessageBox(szSQL);


				int nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
				if(nReturn<DB_SUCCESS)
				{
					CString foo;
					foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
					AfxMessageBox(foo);
				}
				else
				{
					//recv the response.
					Sleep(250);

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action=128 and event_itemid = %d", //HARDCODE
						m_szQueueTableName.GetBuffer(0),
						nItem);
					int nIndex = 0;
					while(nIndex<=0)
					{
						Sleep(100);
		//AfxMessageBox(szSQL);
						CRecordset* prs = m_db.Retrieve(m_db.m_ppdbConn[0], szSQL, szError);
						if(prs)
						{
				//			while ((!prs->IsEOF()))
							if (!prs->IsEOF())  // just do the one record, if there is one
							{
								try
								{
									CString szMessage;
									prs->GetFieldValue("remote", szMessage);//HARDCODE
		//AfxMessageBox(szMessage);

									char* puc = szMessage.GetBuffer(0);
									int nCount = m_lcResponses.GetItemCount();
									m_lcResponses.InsertItem(nCount, m_szCmd);
									m_lcResponses.SetItemText(nCount, 1, puc );
									m_lcResponses.EnsureVisible(nCount, FALSE);


									if(*puc == '*')
									{
										nCount = FindScene(m_szScene);
										if(nCount<0)
										{
											m_scenes[m_nNumScenes].szName=m_szScene;
											nCount=m_nNumScenes;
											m_nNumScenes++;
										}

										m_scenes[nCount].nNumFields = 0;
										m_lcFields.DeleteAllItems();

										char* pch = (char*)puc + m_szScene.GetLength()+ 7;
	//AfxMessageBox(pch);

										char delim[8];

										if((m_bUTF8)&&(m_szDelim.Compare("#")))
										{
											sprintf(delim, "�%c", m_szDelim.GetAt(0) );
										}
										else
										{
											sprintf(delim, "%c", m_szDelim.GetAt(0) );
										}


										while((pch)&&(strlen(pch)))
										{
											char* pchDel = strstr(pch, delim);
											if(pchDel)
											{
												*pchDel=0; pchDel+=strlen(delim);
											}
	//AfxMessageBox(pch);

											if(*pch =='\\')
											{
												pch=NULL;
											}
											if((pch)&&(strlen(pch)))
											{
												m_scenes[nCount].szFields[m_scenes[nCount].nNumFields].Format("%s", pch);
												m_scenes[nCount].nNumFields++;
												pch = strchr(pchDel, '\\');
												if(pch)
												{
													pch++;
												}
											}
										}

										int i=0;
										while(i<m_scenes[nCount].nNumFields)
										{
											m_lcFields.InsertItem(i, m_scenes[nCount].szFields[i]);
											if(i==0)
											{
												GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_scenes[nCount].szFields[i]);
											}
											i++;
										}
									}
										


									nIndex++;
								}
								catch(CException* e)// CDBException *e, CMemoryException *m)  
								{
		AfxMessageBox("exception");
									e->Delete();
								} 
								catch( ... )
								{
		AfxMessageBox("exception2");
								}

								prs->MoveNext();
							}

							prs->Close();

							delete prs;
						}
					}
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE event_itemid = %d", //HARDCODE
						m_szQueueTableName.GetBuffer(0),
						nItem);

		//AfxMessageBox(szSQL);
					nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
					if(nReturn<DB_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
						AfxMessageBox(foo);
					}
				
				}
			}
			else
			if(m_s)
			{
				m_bInCommand = TRUE;
				m_nClock = clock();
				char szError[8096];
				int nReturn;
				nReturn=	m_net.SendLine((unsigned char*)m_szCmd.GetBuffer(0), m_szCmd.GetLength(), m_s, EOLN_CRLF, false, 10000, szError);
				if(nReturn<NET_SUCCESS)
				{
					CString foo;
					foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
					AfxMessageBox(foo);
				}
				else
				{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Sent: ", logtmbuf, timestamp.millitm);
				fwrite(m_szCmd.GetBuffer(0), sizeof(char), m_szCmd.GetLength(), logfp);
				fprintf(logfp, "%c%c", 13, 10);
				fclose(logfp);
			}
}
					//recv the response.
			//		unsigned char* puc2 = NULL;
			//		unsigned long ulLen2=0
					unsigned char* puc = NULL;
					unsigned long ulLen=0;
					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
					if(nReturn<NET_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
						AfxMessageBox(foo);
					}
					else
					{
/*
						//check if more data:
						bool bMore=false;

						timeval tv;
						tv.tv_sec = 0; 
						tv.tv_usec = 500000;  // timeout value
						int nNumSockets;
						fd_set fds;
						FD_ZERO(&fds);  // Zero this out each time
						FD_SET(m_s, &fds);
						nNumSockets = select(0, &fds, NULL, NULL, &tv);
						if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
						{
							
						}
						else
						if(
								(nNumSockets==0) // 0 = timed out, -1 = error
							||(!(FD_ISSET(m_s, &fds)))
							) 
						{ 

						} 
						else // there is recv data.
						{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
							//recv the response.
							nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, char* szError);
							if(nReturn<NET_SUCCESS)
							{
								CString foo;
								foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
								AfxMessageBox(foo);
							}
							else
							{
								if(ulLen==0) //disconnected
								{
									GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
									GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
									GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
									m_net.CloseConnection(m_s);
									m_s = NULL;
									AfxMessageBox("disconnected");
								}
								else
									bMore=true;
							}
						}
						if(bMore)
						{// use the second set to parse
						}
*/


						if(ulLen==0) //disconnected
						{
							GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
							GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
							GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
							m_net.CloseConnection(m_s);
							m_s = NULL;
							AfxMessageBox("disconnected");
						}
						else
						{
							if(puc)
							{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
//				AfxMessageBox((char*)puc);
								int nCount = m_lcResponses.GetItemCount();
								m_lcResponses.InsertItem(nCount, m_szCmd);
								m_lcResponses.SetItemText(nCount, 1, (char*)puc );
								m_lcResponses.EnsureVisible(nCount, FALSE);

								if(*puc == '*')
								{
									nCount = FindScene(m_szScene);
									if(nCount<0)
									{
										m_scenes[m_nNumScenes].szName=m_szScene;
										nCount=m_nNumScenes;
										m_nNumScenes++;
									}

									m_scenes[nCount].nNumFields = 0;
									m_lcFields.DeleteAllItems();

									char* pch = (char*)puc + m_szScene.GetLength()+ 7;
//AfxMessageBox(pch);

									char delim[8];

									if((m_bUTF8)&&(m_szDelim.Compare("#")))
									{
										sprintf(delim, "�%c", m_szDelim.GetAt(0) );
									}
									else
									{
										sprintf(delim, "%c", m_szDelim.GetAt(0) );
									}


									while((pch)&&(strlen(pch)))
									{
										char* pchDel = strstr(pch, delim);
										if(pchDel)
										{
											*pchDel=0; pchDel+=strlen(delim);
										}
//AfxMessageBox(pch);

										if(*pch =='\\')
										{
											pch=NULL;
										}
										if((pch)&&(strlen(pch)))
										{
											m_scenes[nCount].szFields[m_scenes[nCount].nNumFields].Format("%s", pch);
											m_scenes[nCount].nNumFields++;
											pch = strchr(pchDel, '\\');
											if(pch)
											{
												pch++;
											}
										}
									}

									int i=0;
									while(i<m_scenes[nCount].nNumFields)
									{
										m_lcFields.InsertItem(i, m_scenes[nCount].szFields[i]);
										if(i==0)
										{
											GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_scenes[nCount].szFields[i]);
										}
										i++;
									}
								}
								free(puc);
							}


						}
					}
				}
				m_bInCommand = FALSE;

			}
			else
			{
				AfxMessageBox("no connection");
			}	

		}
	}
}

int CIITestDlg::FindScene(CString szName)
{
	int i=0;
	while(i<m_nNumScenes)
	{
		if(m_scenes[i].szName.Compare(szName)==0) return i;
		i++;
	}

	return -1;
}


void CIITestDlg::OnButtonQueryValues() 
{
	UpdateData(TRUE);
	if((m_szScene.GetLength())&&(m_szField.GetLength()))
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		if(((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->FindStringExact( -1, m_szField )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->AddString(m_szField);
			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->SelectString(-1, m_szField);
		}
		m_szCmd.Format("B\\AV\\%s\\%s\\\\", m_szScene,m_szField);
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);

		if((m_bUpdateValues)||(m_bSendAuto))
		{
			if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
			{
				UpdateData(TRUE);
				char szError[8096];
				char szSQL[DB_SQLSTRING_MAXLEN];
				int nItem = clock();

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username, event_itemid) \
		VALUES ('%s','', 256, '%s', 0, 'sys', %d)", //HARDCODE
					m_szQueueTableName.GetBuffer(0),
					m_szCmd.GetBuffer(0),
					m_szHost.GetBuffer(0),
					nItem
				);
		//AfxMessageBox(szSQL);


				int nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
				if(nReturn<DB_SUCCESS)
				{
					CString foo;
					foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
					AfxMessageBox(foo);
				}
				else
				{
					//recv the response.
					Sleep(250);

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action=128 and event_itemid = %d", //HARDCODE
						m_szQueueTableName.GetBuffer(0),
						nItem);
					int nIndex = 0;
					while(nIndex<=0)
					{
						Sleep(100);
		//AfxMessageBox(szSQL);
						CRecordset* prs = m_db.Retrieve(m_db.m_ppdbConn[0], szSQL, szError);
						if(prs)
						{
				//			while ((!prs->IsEOF()))
							if (!prs->IsEOF())  // just do the one record, if there is one
							{
								try
								{
									CString szMessage;
									prs->GetFieldValue("remote", szMessage);//HARDCODE
		//AfxMessageBox(szMessage);

									char* puc = szMessage.GetBuffer(0);
									int nCount = m_lcResponses.GetItemCount();
									m_lcResponses.InsertItem(nCount, m_szCmd);
									m_lcResponses.SetItemText(nCount, 1, puc );
									m_lcResponses.EnsureVisible(nCount, FALSE);


									if(*puc == '*')
									{
										m_lcValues.DeleteAllItems();

										char* pch = (char*)puc + m_szScene.GetLength()+m_szField.GetLength()+ 8;
										char* pchDel = strchr(pch, '\\');
										if(pchDel)
										{
											*pchDel=0; pchDel++;
										}

	//				AfxMessageBox(pch);
										if((pch)&&(strlen(pch)))
										{
											m_szValue.Format("%s", pch);
											GetDlgItem(IDC_COMBO_VALUE)->SetWindowText(m_szValue);
										}
										else
										{
											m_szValue="";
											GetDlgItem(IDC_COMBO_VALUE)->SetWindowText(m_szValue);
										}
										pch = pchDel;

										int i=0;
										bool end=false;
										char delim[8];

										if((m_bUTF8)&&(m_szDelim.Compare("#")))
										{
											sprintf(delim, "�%c", m_szDelim.GetAt(0) );
										}
										else
										{
											sprintf(delim, "%c", m_szDelim.GetAt(0) );
										}
										while((pch)&&(strlen(pch))&&(!end))
										{
											char* pchDel = strstr(pch, delim);
											if(pchDel)
											{
												*pchDel=0; pchDel+=strlen(delim);
											}
											else
											{
												pchDel = strchr(pch, '\\');
												if(pchDel)
												{
													*pchDel=0; pchDel++; end=true;
												}
											}
	//				AfxMessageBox(pch);

											m_lcValues.InsertItem(i, pch);
											pch = pchDel;
											i++;
									
										}
									
									}




									nIndex++;
								}
								catch(CException* e)// CDBException *e, CMemoryException *m)  
								{
		AfxMessageBox("exception");
									e->Delete();
								} 
								catch( ... )
								{
		AfxMessageBox("exception2");
								}

								prs->MoveNext();
							}

							prs->Close();

							delete prs;
						}
					}
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE event_itemid = %d", //HARDCODE
						m_szQueueTableName.GetBuffer(0),
						nItem);

		//AfxMessageBox(szSQL);
					nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
					if(nReturn<DB_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
						AfxMessageBox(foo);
					}
				
				}
			}
			else
			if(m_s)
			{
				m_bInCommand = TRUE;
				m_nClock = clock();
				char szError[8096];
				int nReturn;
				nReturn=	m_net.SendLine((unsigned char*)m_szCmd.GetBuffer(0), m_szCmd.GetLength(), m_s, EOLN_CRLF, false, 10000, szError);
				if(nReturn<NET_SUCCESS)
				{
					CString foo;
					foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
					AfxMessageBox(foo);
				}
				else
				{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Sent: ", logtmbuf, timestamp.millitm);
				fwrite(m_szCmd.GetBuffer(0), sizeof(char), m_szCmd.GetLength(), logfp);
				fprintf(logfp, "%c%c", 13, 10);
				fclose(logfp);
			}
}
					//recv the response.
			//		unsigned char* puc2 = NULL;
			//		unsigned long ulLen2=0
					unsigned char* puc = NULL;
					unsigned long ulLen=0;
					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
					if(nReturn<NET_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
						AfxMessageBox(foo);
					}
					else
					{
/*
						//check if more data:
						bool bMore=false;

						timeval tv;
						tv.tv_sec = 0; 
						tv.tv_usec = 500000;  // timeout value
						int nNumSockets;
						fd_set fds;
						FD_ZERO(&fds);  // Zero this out each time
						FD_SET(m_s, &fds);
						nNumSockets = select(0, &fds, NULL, NULL, &tv);
						if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
						{
							
						}
						else
						if(
								(nNumSockets==0) // 0 = timed out, -1 = error
							||(!(FD_ISSET(m_s, &fds)))
							) 
						{ 

						} 
						else // there is recv data.
						{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
							//recv the response.
							nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, char* szError);
							if(nReturn<NET_SUCCESS)
							{
								CString foo;
								foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
								AfxMessageBox(foo);
							}
							else
							{
								if(ulLen==0) //disconnected
								{
									GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
									GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
									GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
									m_net.CloseConnection(m_s);
									m_s = NULL;
									AfxMessageBox("disconnected");
								}
								else
									bMore=true;
							}
						}
						if(bMore)
						{// use the second set to parse
						}
*/


						if(ulLen==0) //disconnected
						{
							GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
							GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
							GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
							m_net.CloseConnection(m_s);
							m_s = NULL;
							AfxMessageBox("disconnected");
						}
						else
						{
							if(puc)
							{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
//				AfxMessageBox((char*)puc);
								int nCount = m_lcResponses.GetItemCount();
								m_lcResponses.InsertItem(nCount, m_szCmd);
								m_lcResponses.SetItemText(nCount, 1, (char*)puc );
								m_lcResponses.EnsureVisible(nCount, FALSE);

								if(*puc == '*')
								{
									m_lcValues.DeleteAllItems();

									char* pch = (char*)puc + m_szScene.GetLength()+m_szField.GetLength()+ 8;
									char* pchDel = strchr(pch, '\\');
									if(pchDel)
									{
										*pchDel=0; pchDel++;
									}

//				AfxMessageBox(pch);
									if((pch)&&(strlen(pch)))
									{
										m_szValue.Format("%s", pch);
										GetDlgItem(IDC_COMBO_VALUE)->SetWindowText(m_szValue);
									}
									else
									{
										m_szValue="";
										GetDlgItem(IDC_COMBO_VALUE)->SetWindowText(m_szValue);
									}
									pch = pchDel;

									int i=0;
									bool end=false;
									char delim[8];

									if((m_bUTF8)&&(m_szDelim.Compare("#")))
									{
										sprintf(delim, "�%c", m_szDelim.GetAt(0) );
									}
									else
									{
										sprintf(delim, "%c", m_szDelim.GetAt(0) );
									}

									while((pch)&&(strlen(pch))&&(!end))
									{
										char* pchDel = strstr(pch, delim);
										if(pchDel)
										{
											*pchDel=0; pchDel+=strlen(delim);
										}
										else
										{
											pchDel = strchr(pch, '\\');
											if(pchDel)
											{
												*pchDel=0; pchDel++; end=true;
											}
										}
//				AfxMessageBox(pch);

										m_lcValues.InsertItem(i, pch);
										pch = pchDel;
										i++;
								
									}
								
								}
								free(puc);
							}


						}
					}
				}
				m_bInCommand = FALSE;

			}
			else
			{
				AfxMessageBox("no connection");
			}	

		}
	}
}

void CIITestDlg::OnButtonClose() 
{
	UpdateData(TRUE);
	if(m_szScene.GetLength())
	{
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:	
			{
				m_szCmd.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, m_szLayer);  //we'll see if this works.
			} break;
		case II_TYPE_ICON:	
			{
				m_szCmd.Format("T\\14\\%s\\\\", m_szLayer);
			} break;
		case II_TYPE_G7:	
			{
				m_szCmd.Format("T\\14\\%s\\\\", m_szLayer);
			} break;
		case II_TYPE_CB:	
		default:
			{
				m_szCmd.Format("B\\CL\\%s\\\\", m_szScene);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
}

void CIITestDlg::OnButtonDelim() 
{
	UpdateData(TRUE);
	if(m_szDelim.GetLength())
	{
		if((m_bUTF8)&&(m_szDelim.Compare("#")))	{m_szCmd += "�";}
		m_szCmd += m_szDelim;
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
	}
	
}

void CIITestDlg::OnButtonLoad() 
{
	UpdateData(TRUE);
	if(m_szScene.GetLength())
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}

		switch(m_nFlavor)
		{
		case II_TYPE_LYR:
			{
				m_szCmd.Format("V\\5\\13\\1\\%s\\%s\\1\\\\", m_szLayer, m_szAlias);

				if((m_szAlias.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->FindStringExact( -1, m_szAlias )==CB_ERR))
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->AddString(m_szAlias);
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->SelectString(-1, m_szAlias);
				}

			} break;
		case II_TYPE_ICON:
			{
				if(m_szField.GetLength())
				{
					if(m_szDelim.CompareNoCase("4 digit")==0)
					{
						m_szCmd.Format("T\\7\\%s%s\\%s\\\\", m_szScene, m_szField, m_szLayer);
					}
					else
					if(m_szDelim.CompareNoCase("8 digit")==0)
					{
						m_szCmd.Format("T\\7\\%s%s\\%s\\\\", m_szScene, m_szField, m_szLayer);
					}
					else // string
					{
						m_szCmd.Format("T\\7\\%s/%s\\%s\\\\", m_szScene, m_szField, m_szLayer);
					}

				}
				else
				{
					if(m_szDelim.CompareNoCase("4 digit")==0)
					{
						m_szCmd.Format("T\\7\\%s00\\%s\\\\", m_szScene, m_szLayer);
					}
					else
					if(m_szDelim.CompareNoCase("8 digit")==0)
					{
						m_szCmd.Format("T\\7\\%s0000\\%s\\\\", m_szScene, m_szLayer);
					}
					else // string
					{
						m_szCmd.Format("T\\7\\%s/\\%s\\\\", m_szScene, m_szLayer);
					}

				}
			} break;

		case II_TYPE_G7:
			{
				CString szScribe;
				CString szLayout;

				int nColon = m_szScene.ReverseFind(':');
				if(nColon>=0)
				{
					szScribe=m_szScene.Left(nColon);
					szLayout = m_szScene.Mid(nColon+1);
				}
				else
				{
					szScribe = m_szScene;
					szLayout = "1"; // "safe" default
				}
				m_szCmd.Format("I\\20\\%s\\%s\\\\", m_szAlias, szLayout);
			} break;



		case II_TYPE_CB:
		default:
			{
				m_szCmd.Format("B\\LO\\%s\\\\", m_szScene);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
}

void CIITestDlg::OnButtonOpen() 
{
	UpdateData(TRUE);
	if(m_szScene.GetLength())
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:
			{
				m_szCmd.Format("W\\%s\\%s\\", m_szAlias, m_szScene);

				for(int maxfields=0; maxfields<99; maxfields++)
				{
					m_szCmd += " \\";
				}
				m_szCmd += "\\";  // term II

				if((m_szAlias.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->FindStringExact( -1, m_szAlias )==CB_ERR))
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->AddString(m_szAlias);
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->SelectString(-1, m_szAlias);
				}

			} break;

		case II_TYPE_G7:
			{
				CString szScribe;
				CString szLayout;

				int nColon = m_szScene.ReverseFind(':');
				if(nColon>=0)
				{
					szScribe=m_szScene.Left(nColon);
					szLayout = m_szScene.Mid(nColon+1);
				}
				else
				{
					szScribe = m_szScene;
					szLayout = "1"; // "safe" default
				}
				m_szCmd.Format("I\\25\\1\\%s\\\\", szScribe);
			} break;

		case II_TYPE_ICON:
			{
				if(m_szDelim.CompareNoCase("4 digit")==0)
				{
					m_szCmd.Format("T\\7\\%s00\\%s\\\\", m_szScene, m_szLayer);
				}
				else
				if(m_szDelim.CompareNoCase("8 digit")==0)
				{
					m_szCmd.Format("T\\7\\%s0000\\%s\\\\", m_szScene, m_szLayer);
				}
				else // string
				{
					m_szCmd.Format("T\\7\\%s/\\%s\\\\", m_szScene, m_szLayer);
				}

			} break;
		case II_TYPE_CB:
		default:
			{
				m_szCmd.Format("B\\OP\\%s\\\\", m_szScene);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
	
}

void CIITestDlg::OnButtonPlay() 
{
	UpdateData(TRUE);
	if((m_szScene.GetLength())||(m_nFlavor==II_TYPE_ICON)||(m_nFlavor==II_TYPE_LYR))
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:	
			{
				m_szCmd.Format("Y\\%c%c%s\\\\", 0xd5, 0xf3, m_szLayer);  //we'll see if this works.
			} break;
		case II_TYPE_G7:
			{
				m_szCmd.Format("T\\7\\%s\\%s\\\\", m_szAlias, m_szLayer);
			} break;


		case II_TYPE_ICON:	
			{
				if(m_szField.GetLength())
				{
					if(m_szDelim.CompareNoCase("4 digit")==0)
					{
						m_szCmd.Format("T\\7\\00%s\\%s\\\\", m_szField, m_szLayer);
					}
					else
					if(m_szDelim.CompareNoCase("8 digit")==0)
					{
						m_szCmd.Format("T\\7\\0000%s\\%s\\\\", m_szField, m_szLayer);
					}
					else // string
					{
						m_szCmd.Format("T\\7\\/%s\\%s\\\\", m_szField, m_szLayer);
					}

				}
				else
				{ 
					return;
				}
			} break;
		case II_TYPE_CB:	
		default:
			{
				m_szCmd.Format("B\\PL\\%s\\\\", m_szScene);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
}

void CIITestDlg::OnButtonStop() 
{
	UpdateData(TRUE);
	if(m_szScene.GetLength())
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:	
			{
				m_szCmd.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, m_szLayer);  //we'll see if this works.
			} break;
		case II_TYPE_ICON:	
			{
				m_szCmd.Format("T\\14\\%s\\\\", m_szLayer);
			} break;
		case II_TYPE_G7:	//is fire G3d Trigger
			{
				if(m_szField.GetLength())
				{
					m_szCmd.Format("I\\31\\%s\\\\", m_szField);
				}
				else
				{
					m_szCmd = "";
				}
			} break;
		case II_TYPE_CB:	
		default:
			{
				m_szCmd.Format("B\\ST\\%s\\\\", m_szScene);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
}

void CIITestDlg::OnButtonUpdate() 
{
	UpdateData(TRUE);
	if((m_szScene.GetLength())&&(m_szField.GetLength())/*&&(m_szValue.GetLength())*/&&(m_szDelim.GetLength()))
	{
		m_bStates = FALSE;
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		if(((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->FindStringExact( -1, m_szField )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->AddString(m_szField);
			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->SelectString(-1, m_szField);
		}
		if(m_szValue.GetLength())
		{
			if(((CComboBox*)GetDlgItem(IDC_COMBO_VALUE))->FindStringExact( -1, m_szValue )==CB_ERR)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_VALUE))->AddString(m_szValue);
				((CComboBox*)GetDlgItem(IDC_COMBO_VALUE))->SelectString(-1, m_szValue);
			}
		}
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:
		{
				CBufferUtil bu;
				char* pch = bu.EscapeChar(m_szValue.GetBuffer(0), '\\');
				if(pch)
				{
					if(m_bStar)
						m_szCmd.Format("U\\*\\%s\\%s\\\\", m_szField, pch);
					else
						m_szCmd.Format("U\\%s\\%s\\%s\\\\", m_szAlias, m_szField, pch);
					free(pch);
				}
				else
				{
					if(m_bStar)
						m_szCmd.Format("U\\*\\%s\\%s\\\\", m_szField, m_szValue);
					else
						m_szCmd.Format("U\\%s\\%s\\%s\\\\", m_szAlias, m_szField, m_szValue);

				}

				if((m_szAlias.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->FindStringExact( -1, m_szAlias )==CB_ERR))
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->AddString(m_szAlias);
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->SelectString(-1, m_szAlias);
				}

			} break;
		case II_TYPE_G7:
		{
				CBufferUtil bu;
				char* pch = bu.EscapeChar(m_szValue.GetBuffer(0), '\\');
				if(pch)
				{
					m_szCmd.Format("I\\30\\%s\\%s\\%s\\\\", m_szLayer, m_szField, pch);
					free(pch);
				}
				else
				{
					m_szCmd.Format("I\\30\\%s\\%s\\%s\\\\", m_szLayer, m_szField, m_szValue);
				}

				if((m_szAlias.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->FindStringExact( -1, m_szAlias )==CB_ERR))
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->AddString(m_szAlias);
					((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->SelectString(-1, m_szAlias);
				}

			} break;
		case II_TYPE_ICON:	
			{
				CBufferUtil bu;
				CString szRegion; 
				CString szTag;
				char* pszValue = bu.XMLEncode(m_szValue.GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

				int nColon = m_szField.ReverseFind(':');
				if(nColon>=0)
				{
					szRegion=m_szField.Left(nColon);
					szTag = m_szField.Mid(nColon+1);
					if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
				}
				else
				{
					szRegion = m_szField;
					szTag = "Field1"; // "safe" default
				}

				m_szCmd.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
					m_szScene,
					szRegion, szTag, (pszValue?pszValue:""));
				if(pszValue) free(pszValue);


			} break;
		case II_TYPE_CB:	
		default:
			{
				m_szCmd.Format("B\\UP\\%s\\%s%s%s%s\\\\", 
					m_szScene, 
					m_szField, 
					(((m_bUTF8)&&(m_szDelim.Compare("#")))?"�":""),
					m_szDelim, 
					m_szValue);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
}

void CIITestDlg::OnClickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nSel = m_lcFields.GetNextItem( -1,  LVNI_SELECTED );
	if(nSel>=0)
	{
		m_szField = m_lcFields.GetItemText(nSel, 0);
		GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_szField);
	}
	
	*pResult = 0;
}

void CIITestDlg::OnClickList2(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nSel = m_lcValues.GetNextItem( -1,  LVNI_SELECTED );
	if(nSel>=0)
	{
		m_szValue = m_lcValues.GetItemText(nSel, 0);
		GetDlgItem(IDC_COMBO_VALUE)->SetWindowText(m_szValue);
	}
	
	*pResult = 0;
}

void CIITestDlg::OnCloseupComboScene() 
{
//return;
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
	int q=0;
	while(q<m_nNumScenes)
	{
		if(m_scenes[q].szName.Compare(m_szScene)==0)
		{
			m_scenes[q].bSel=true;
			m_lcFields.DeleteAllItems();
			m_bStates = FALSE;

//AfxMessageBox(m_szScene);
			int i=0;
			while(i<m_scenes[q].nNumFields)
			{
//AfxMessageBox(m_scenes[q].szFields[i]);
				m_lcFields.InsertItem(i, m_scenes[q].szFields[i]);
				if(i==0)
				{
					GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_scenes[q].szFields[i]);
					m_szField = m_scenes[q].szFields[i];
				}
				i++;
			}

		}
		else
		{
			m_scenes[q].bSel=false;
		}
		q++;
	}

}

void CIITestDlg::OnCheckSend() 
{
	// TODO: Add your control notification handler code here
	
	
}

void CIITestDlg::OnSelchangeComboScene() 
{
	if(_beginthread(LoadFieldsThread, 0, (void*)this)==-1) LoadFields();

}

void LoadFieldsThread(void* pvArgs)
{
	Sleep(50);
	if(pvArgs)
	{
		CIITestDlg* p = (CIITestDlg*)pvArgs;
		p->LoadFields();
	}
}

int CIITestDlg::LoadFields()
{

//AfxMessageBox(m_szScene);
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
//AfxMessageBox(m_szScene);
	
	int q=0;
	while(q<m_nNumScenes)
	{
		if(m_scenes[q].szName.Compare(m_szScene)==0)
		{
			m_scenes[q].bSel=true;
			m_lcFields.DeleteAllItems();
//AfxMessageBox(m_szScene);
			int i=0;
			while(i<m_scenes[q].nNumFields)
			{
//AfxMessageBox(m_scenes[q].szFields[i]);
				m_lcFields.InsertItem(i, m_scenes[q].szFields[i]);
				if(i==0)
				{
					GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_scenes[q].szFields[i]);
					m_szField = m_scenes[q].szFields[i];
				}
				i++;
			}

		}
		else
		{
			m_scenes[q].bSel=false;
		}
		q++;
	}
//	m_lcFields.Invalidate();

	return 0;
}


void CIITestDlg::OnButtonConnect2() 
{
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		if(AfxMessageBox("The Channel Box must not be connected directly, in order to send commands to Libretto.\r\nDisconnect and continue?", MB_YESNO)!=IDYES) return;
		OnButtonConnect();
	}

	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0))
	{
		m_db.DisconnectDatabase(m_db.m_ppdbConn[0]);
		GetDlgItem(IDC_BUTTON_CONNECT2)->SetWindowText("Connect to Libretto");
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_db.RemoveConnection(m_db.m_ppdbConn[0]);
	}
	else
	{
		UpdateData(TRUE);
		char szError[8096];


		// Lets get the latest csf.
		CFileUtil file;
		// get settings.
//AfxMessageBox(m_szLibrettoFileLocation);

		int rv = file.GetSettings(m_szLibrettoFileLocation.GetBuffer(0), false);


		while(!(rv&FILEUTIL_FILE_EXISTS))
		{
			CFileDialog fdlg(TRUE,
											 "csf",
											 "Libretto.csf",
												OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_LONGNAMES|OFN_NOCHANGEDIR|OFN_PATHMUSTEXIST );

			if(fdlg.DoModal()==IDOK)
			{
				m_szLibrettoFileLocation = fdlg.GetPathName();
				AfxGetApp()->WriteProfileString("Libretto","CSFFilePath", m_szLibrettoFileLocation);
			

				rv = file.GetSettings(m_szLibrettoFileLocation.GetBuffer(0), false);

				if(!(rv&FILEUTIL_FILE_EXISTS))
				{
					if(AfxMessageBox("The setting could not be extracted.\r\nDo you wish to select another file?", MB_YESNO)!=IDYES) break;
				}
			}
			else //cancelled
				return;

		}

		if(rv&FILEUTIL_FILE_EXISTS)
		{
			m_szQueueTableName = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue");  // the CommandQueue table name
//AfxMessageBox("got from csf");
		}
		else 
		{
			return;
		}


//AfxMessageBox(m_szQueueTableName);


		CDBconn* pdb = m_db.CreateNewConnection(m_szDSN.GetBuffer(1),m_szUser.GetBuffer(1),m_szPw.GetBuffer(1));
		if((pdb)&&(m_db.ConnectDatabase(pdb,szError))>=NET_SUCCESS)
		{
//			AfxMessageBox("Connected!");

			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT2)->SetWindowText("Disconnect Libretto");
			GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);
		}
		else
		{
			AfxMessageBox(szError);
			if(pdb)
			{
				m_db.DisconnectDatabase(pdb);
				m_db.RemoveConnection(pdb);
			}
		}
	}
}

void CIITestDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<4;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST3;break;
			case 1: nCtrlID=IDC_EDIT_COMMAND;break;
			case 2: nCtrlID=IDC_BUTTON_SEND;break;
			case 3: nCtrlID=IDC_BUTTON_RECV;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void CIITestDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_EDIT_COMMAND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[1].Width()-dx,  // goes with right edge
			m_rcCtrl[1].Height(), 
			SWP_NOZORDER|SWP_NOMOVE
			);

		pWnd=GetDlgItem(IDC_BUTTON_SEND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[2].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[2].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_BUTTON_RECV);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[3].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[3].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_LIST3);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			m_rcCtrl[0].Width()-dx,  // goes with right edge
			m_rcCtrl[0].Height()-dy,  // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		int col1w= m_lcResponses.GetColumnWidth(0);
		m_lcResponses.SetColumnWidth(1, (m_rcCtrl[0].Width()-dx-16-col1w) ); 


		for (int i=0;i<4;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST3;break;
			case 1: nCtrlID=IDC_EDIT_COMMAND;break;
			case 2: nCtrlID=IDC_BUTTON_SEND;break;
			case 3: nCtrlID=IDC_BUTTON_RECV;break;
			}
			GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
}

void CIITestDlg::SetupFlavor() 
{
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);

	switch(m_nFlavor)
	{
	case II_TYPE_LYR:
		{
			GetDlgItem(IDC_STATIC_SCENE)->SetWindowText("Message");
			GetDlgItem(IDC_STATIC_FIELDS)->SetWindowText("Templates");
			
			GetDlgItem(IDC_BUTTON_AN)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_AV)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CL)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_QS)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_QS2)->EnableWindow(FALSE);

			GetDlgItem(IDC_BUTTON_OP)->SetWindowText("Load");
			GetDlgItem(IDC_BUTTON_LO)->SetWindowText("Read");
			GetDlgItem(IDC_BUTTON_PL)->SetWindowText("Play");
			GetDlgItem(IDC_BUTTON_ST)->SetWindowText("Unload");

			GetDlgItem(IDC_BUTTON_CL)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_AN)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_QS)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_QS2)->SetWindowText("");


			GetDlgItem(IDC_BUTTON_ST)->EnableWindow(TRUE);
	//		GetDlgItem(IDC_BUTTON_CL)->SetWindowText("Clear");
			GetDlgItem(IDC_CHECK_STAR)->EnableWindow(TRUE);
			GetDlgItem(IDC_CHECK_STAR)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_COMBO_ALIAS)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_ALIAS)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_ALIAS)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_ALIAS)->SetWindowText("alias");
			GetDlgItem(IDC_STATIC_ALIAS)->ShowWindow(SW_SHOW);
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->ResetContent();
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("0");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("1");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("2");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->SelectString(-1, "1");
			GetDlgItem(IDC_COMBO_BUFFER)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_BUFFER)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_FB)->SetWindowText("frame buffer");
			GetDlgItem(IDC_STATIC_FB)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_FB)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BUTTON_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BUTTON_TH)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_TH)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->ShowWindow(SW_HIDE);
			
		} break;
	case II_TYPE_G7:
		{
			GetDlgItem(IDC_STATIC_SCENE)->SetWindowText("Scribe:Layout");
			GetDlgItem(IDC_STATIC_FIELDS)->SetWindowText("Fields");
			
			GetDlgItem(IDC_BUTTON_AN)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_AV)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CL)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_QS)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_QS2)->EnableWindow(FALSE);

			GetDlgItem(IDC_BUTTON_OP)->SetWindowText("Set");
			GetDlgItem(IDC_BUTTON_LO)->SetWindowText("Load");
			GetDlgItem(IDC_BUTTON_PL)->SetWindowText("Play");
			GetDlgItem(IDC_BUTTON_ST)->SetWindowText("Fire G3D");

			GetDlgItem(IDC_BUTTON_CL)->SetWindowText("Clear");
			GetDlgItem(IDC_BUTTON_AN)->SetWindowText("Query Field");
			GetDlgItem(IDC_BUTTON_QS)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_QS2)->SetWindowText("");
//			GetDlgItem(IDC_BUTTON_QS)->SetWindowText("Query Scene");


			GetDlgItem(IDC_BUTTON_ST)->EnableWindow(TRUE);
	//		GetDlgItem(IDC_BUTTON_CL)->SetWindowText("Clear");
			GetDlgItem(IDC_CHECK_STAR)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_STAR)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_ALIAS)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_ALIAS)->SetWindowText("output");
			GetDlgItem(IDC_COMBO_ALIAS)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_ALIAS)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_ALIAS)->ShowWindow(SW_SHOW);
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->ResetContent();
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("A");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("B");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("C");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("D");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("E");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->SelectString(-1, "A");
			GetDlgItem(IDC_COMBO_BUFFER)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_BUFFER)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_FB)->SetWindowText("frame buffer");
			GetDlgItem(IDC_STATIC_FB)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_FB)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BUTTON_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BUTTON_TH)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_TH)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->ShowWindow(SW_HIDE);
			
		} break;
	case II_TYPE_ICON:
		{
			GetDlgItem(IDC_STATIC_SCENE)->SetWindowText("Layout");
			GetDlgItem(IDC_STATIC_FIELDS)->SetWindowText("Fields and Salvos");
			
			GetDlgItem(IDC_BUTTON_AN)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_AV)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CL)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_ST)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_QS)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_QS2)->EnableWindow(FALSE);

			GetDlgItem(IDC_BUTTON_OP)->SetWindowText("Load");
			GetDlgItem(IDC_BUTTON_LO)->SetWindowText("LoadFire");
			GetDlgItem(IDC_BUTTON_PL)->SetWindowText("Fire");
			GetDlgItem(IDC_BUTTON_ST)->SetWindowText("Clear");
			GetDlgItem(IDC_BUTTON_CL)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_AN)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_QS)->SetWindowText("");
			GetDlgItem(IDC_BUTTON_QS2)->SetWindowText("");


	//		GetDlgItem(IDC_BUTTON_CL)->SetWindowText("Clear");
			GetDlgItem(IDC_CHECK_STAR)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_STAR)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_ALIAS)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_ALIAS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_ALIAS)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_ALIAS)->ShowWindow(SW_HIDE);
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->ResetContent();
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("A");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("B");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("C");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("D");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->AddString("E");
			((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->SelectString(-1, "A");
			GetDlgItem(IDC_COMBO_BUFFER)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_BUFFER)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_FB)->SetWindowText("layer");
			GetDlgItem(IDC_STATIC_FB)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_FB)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DELIM)->SetWindowText("Mode");
			GetDlgItem(IDC_STATIC_DELIM)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_DELIM)->ShowWindow(SW_SHOW);

			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->ResetContent();
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->AddString("4 digit");
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->AddString("8 digit");
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->AddString("string");
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->SelectString(-1, "string");

			GetDlgItem(IDC_COMBO_DELIM)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_DELIM)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_BUTTON_DELIM)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_DELIM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BUTTON_TH)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_TH)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->ShowWindow(SW_HIDE);
		} break;

	case II_TYPE_CB:
	default:
		{
			GetDlgItem(IDC_STATIC_SCENE)->SetWindowText("Scene");
			GetDlgItem(IDC_STATIC_FIELDS)->SetWindowText("Fields");
			GetDlgItem(IDC_BUTTON_AN)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_AV)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_CL)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_ST)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_QS)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_QS2)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_OP)->SetWindowText("Open");
			GetDlgItem(IDC_BUTTON_LO)->SetWindowText("Load");
			GetDlgItem(IDC_BUTTON_PL)->SetWindowText("Play");
			GetDlgItem(IDC_BUTTON_ST)->SetWindowText("Stop");
			GetDlgItem(IDC_BUTTON_CL)->SetWindowText("Close");
			GetDlgItem(IDC_BUTTON_AN)->SetWindowText("Query Field");
			GetDlgItem(IDC_BUTTON_QS)->SetWindowText("Query Scene");
			GetDlgItem(IDC_BUTTON_QS2)->SetWindowText("Query State");

			GetDlgItem(IDC_CHECK_STAR)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_STAR)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_ALIAS)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_ALIAS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_ALIAS)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_ALIAS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_COMBO_BUFFER)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_BUFFER)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_FB)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_FB)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DELIM)->SetWindowText("Delimiter");
			GetDlgItem(IDC_STATIC_DELIM)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_DELIM)->ShowWindow(SW_SHOW);

			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->ResetContent();
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->AddString("�");
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->AddString("#");
			((CComboBox*)GetDlgItem(IDC_COMBO_DELIM))->SelectString(-1, "�");

			GetDlgItem(IDC_COMBO_DELIM)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_DELIM)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_BUTTON_DELIM)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_DELIM)->ShowWindow(SW_SHOW);

			GetDlgItem(IDC_BUTTON_TH)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_TH)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CHECK_THUMB_CP)->EnableWindow(TRUE);
			GetDlgItem(IDC_CHECK_THUMB_CP)->ShowWindow(SW_SHOW);

		} break;
	}
}

void CIITestDlg::OnButtonHex() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	int nLen = m_szHex.GetLength();
	if(nLen>1)
	{
		if((m_szHex.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->FindStringExact( -1, m_szHex )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->AddString(m_szHex);
			((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->SelectString(-1, m_szHex);
		}


		CString szHex = "";
		CBufferUtil bu;
		int i=0;
		char* pch = m_szHex.GetBuffer(nLen);
		if(pch)
		{
			while(i<nLen-1)
			{
				char ch = (char)bu.xtol(pch, 2); // returns zero on error, wont use zero in this.
				if(ch) szHex += ch; 
				pch+=2;
				i+=2;
			}

			m_szCmd += szHex;

			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		}
	}
}

void CIITestDlg::OnEditchangeComboHex() 
{
	// TODO: Add your control notification handler code here
	//UpdateData(TRUE);

	CString szHex;
	CString szHexOut="";
	DWORD dwSel = ((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->GetEditSel();
	GetDlgItem(IDC_COMBO_HEX)->GetWindowText(szHex);
	int nLen = szHex.GetLength();
	if(nLen>0)
	{
		int i=0;
		while(i<nLen)
		{
			char ch = szHex.GetAt(i);
			if(
					((ch>47)&&(ch<58))
				||((ch>64)&&(ch<71))
				||((ch>96)&&(ch<103))
				)
			{
				szHexOut += ch;
			}
			i++;
		}
	}

	GetDlgItem(IDC_COMBO_HEX)->SetWindowText(szHexOut);
	((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->SetEditSel(HIWORD(dwSel), LOWORD(dwSel));

}

void CIITestDlg::OnCloseupComboHex() 
{
		UpdateData(TRUE);
//		char conn[256];
//		sprintf(conn, "%s", m_szHost);  // was m_szIP
//		AfxMessageBox(m_szHost);

		if((m_szHex.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->FindStringExact( -1, m_szHex )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->AddString(m_szHex);
			((CComboBox*)GetDlgItem(IDC_COMBO_HEX))->SelectString(-1, m_szHex);
		}
}

void CIITestDlg::OnCloseupComboAlias() 
{
		UpdateData(TRUE);
//		char conn[256];
//		sprintf(conn, "%s", m_szHost);  // was m_szIP
//		AfxMessageBox(m_szHost);
return;
		if((m_szAlias.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->FindStringExact( -1, m_szAlias )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->AddString(m_szAlias);
			((CComboBox*)GetDlgItem(IDC_COMBO_ALIAS))->SelectString(-1, m_szAlias);
		}
}

void CIITestDlg::OnMonitorButtonRecv()
{
	if(m_bInCommand) return;
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
//		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				m_net.CloseConnection(m_s);
				m_s = NULL;
				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
					int nCount = m_lcResponses.GetItemCount();
					m_lcResponses.InsertItem(nCount, "");
					m_lcResponses.SetItemText(nCount, 1, (char*)puc );
					m_lcResponses.EnsureVisible(nCount, FALSE);

					free(puc);
				}
			}
		}

	}
}

void CIITestDlg::OnButtonRecv() 
{
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				m_net.CloseConnection(m_s);
				m_s = NULL;
				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
					int nCount = m_lcResponses.GetItemCount();
					m_lcResponses.InsertItem(nCount, "");
					m_lcResponses.SetItemText(nCount, 1, (char*)puc );
					m_lcResponses.EnsureVisible(nCount, FALSE);

					free(puc);
				}
			}
		}

	}
}

void CIITestDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();  prevent cancellation with enter.
}

void CIITestDlg::OnRadioCb() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CIITestDlg::OnRadioIcon() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CIITestDlg::OnRadioLyr() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CIITestDlg::OnRadioG7() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void SocketMonitorThread(void* pvArgs)
{
	CIITestDlg* p = (CIITestDlg*)pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{
			timeval tv;
			tv.tv_sec = 0; 
			tv.tv_usec = 500;  // timeout value
			int nNumSockets;
			fd_set fds;

			FD_ZERO(&fds);  // Zero this out each time
			FD_SET(p->m_s, &fds);

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
			if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
			{
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(p->m_s, &fds)))
				) 
			{ 
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else // there is recv data.
			{ // wait some delay.
				if((clock() > p->m_nClock+1000)&&(!p->m_bInCommand))
				{
					p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
					if(clock() > p->m_nClock+1000) // five seconds is a huge timeout.
						p->OnMonitorButtonRecv();
				}
			}
			if(p->m_bMonitorSocket) Sleep(100);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}


void CIITestDlg::OnButtonPoll() 
{
	UpdateData(TRUE);
//	if(m_szScene.GetLength())  // dont need a scene
	{
/*
		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
*/
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:
			{
				m_szCmd.Format("Q\\\\");
			} break;
		case II_TYPE_G7:
			{
				m_szCmd.Format("I\\1000\\0\\\\");
			} break;
		case II_TYPE_ICON:
			{
				m_szCmd.Format("*\\\\");
			} break;
		case II_TYPE_CB:
		default:
			{
				m_szCmd.Format("Q\\\\");// not really supported but it should work
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
	
}

void CIITestDlg::OnButtonQueryScene() 
{
	UpdateData(TRUE);
	if(m_szScene.GetLength())
	{

		if(!m_bStates)
		{
			m_bStates = TRUE;
			m_lcFields.DeleteAllItems();
			m_lcFields.InsertItem(0, "Opened");
			m_lcFields.InsertItem(1, "Loaded");
			m_lcFields.InsertItem(2, "Loading");
			m_lcFields.InsertItem(3, "Playing");
			m_lcFields.InsertItem(4, "Stopped");
			m_lcFields.InsertItem(5, "Closed");
			m_lcFields.InsertItem(6, "NonExistent");

//			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->AddString("Opened");
//			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->SelectString(-1, "Opened");

			// do following instead of adding states to the dropdown

			m_szField = "Opened";
			GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_szField);

		}

		if(((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->FindStringExact( -1, m_szScene )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->AddString(m_szScene);
			((CComboBox*)GetDlgItem(IDC_COMBO_SCENE))->SelectString(-1, m_szScene);
		}
		switch(m_nFlavor)
		{
		case II_TYPE_LYR:
			{
				m_szCmd.Format("Q\\\\");
			} break;
		case II_TYPE_ICON:
			{
				m_szCmd.Format("*\\\\");
			} break;
		case II_TYPE_CB:
		default:
			{
				m_szCmd.Format("B\\QS\\%s\\\\", m_szScene);
			} break;
		}
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
		if(m_bSendAuto) OnButtonSend();
	}
}

void CIITestDlg::OnButtonQueryState() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(!m_bStates)
	{
		m_bStates = TRUE;
		m_lcFields.DeleteAllItems();
		m_lcFields.InsertItem(0, "Opened");
		m_lcFields.InsertItem(1, "Loaded");
		m_lcFields.InsertItem(2, "Loading");
		m_lcFields.InsertItem(3, "Playing");
		m_lcFields.InsertItem(4, "Stopped");
		m_lcFields.InsertItem(5, "Closed");
		m_lcFields.InsertItem(6, "NonExistent");

//			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->AddString("Opened");
//			((CComboBox*)GetDlgItem(IDC_COMBO_FIELD))->SelectString(-1, "Opened");

		// do following instead of adding states to the dropdown

		m_szField = "Opened";
		GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_szField);

	}

	switch(m_nFlavor)
	{
	case II_TYPE_LYR:
		{
			m_szCmd.Format("Q\\\\");
		} break;
	case II_TYPE_ICON:
		{
			m_szCmd.Format("*\\\\");
		} break;
	case II_TYPE_CB:
	default:
		{
			m_szCmd.Format("B\\QL\\%s\\\\", m_szField);
		} break;
	}
	GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);
	if(m_bSendAuto) OnButtonSend();
	
}

void CIITestDlg::OnButtonTh() 
{
	UpdateData(TRUE);
/*	if((m_szField.CompareNoCase("Scene"))&&(m_szField.CompareNoCase("ControlPanel")))
	{
		m_lcFields.DeleteAllItems();
		m_lcFields.InsertItem(0, "Scene");
		m_lcFields.InsertItem(1, "ControlPanel");
		m_szField = "Scene";
		GetDlgItem(IDC_COMBO_FIELD)->SetWindowText(m_szField);
	}
*/
	switch(m_nFlavor)
	{
	case II_TYPE_LYR:
		{
			m_szCmd.Format("");
		} break;
	case II_TYPE_ICON:
		{
			m_szCmd.Format("");
		} break;
	case II_TYPE_CB:
	default:
		{
			if(m_bThumbCP)
				m_szCmd.Format("B\\TH\\%s\\ControlPanel\\\\", m_szScene);
			else
				m_szCmd.Format("B\\TH\\%s\\Scene\\\\", m_szScene);
		} break;
	}
	GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(m_szCmd);

	if(m_nFlavor != II_TYPE_CB) return; // remove this when we support thumbnail images for other boxes later.

	if((m_bUpdateValues)||(m_bSendAuto))
	{
		if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
		{
			UpdateData(TRUE);
			char szError[8096];
			char szSQL[DB_SQLSTRING_MAXLEN];
			int nItem = clock();

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username, event_itemid) \
	VALUES ('%s','', 256, '%s', 0, 'sys', %d)", //HARDCODE
				m_szQueueTableName.GetBuffer(0),
				m_szCmd.GetBuffer(0),
				m_szHost.GetBuffer(0),
				nItem
			);
	//AfxMessageBox(szSQL);


			int nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
			if(nReturn<DB_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
			else
			{
				//recv the response.
				Sleep(250);

				char szSQL[DB_SQLSTRING_MAXLEN];
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action=128 and event_itemid = %d", //HARDCODE
					m_szQueueTableName.GetBuffer(0),
					nItem);
				int nIndex = 0;
				while(nIndex<=0)
				{
					Sleep(100);
	//AfxMessageBox(szSQL);
					CRecordset* prs = m_db.Retrieve(m_db.m_ppdbConn[0], szSQL, szError);
					if(prs)
					{
			//			while ((!prs->IsEOF()))
						if (!prs->IsEOF())  // just do the one record, if there is one
						{
							try
							{
								CString szMessage;
								prs->GetFieldValue("remote", szMessage);//HARDCODE
	//AfxMessageBox(szMessage);


								if(szMessage.GetLength())
								{
									char* puc = szMessage.GetBuffer(0);
									HINSTANCE hi;
									hi=ShellExecute((HWND) NULL, NULL, puc, NULL, NULL, SW_HIDE);
								}
								else
								{
									AfxMessageBox("No valid file path was returned");
								}

								nIndex++;
							}
							catch(CException* e)// CDBException *e, CMemoryException *m)  
							{
	AfxMessageBox("exception");
								e->Delete();
							} 
							catch( ... )
							{
	AfxMessageBox("exception2");
							}

							prs->MoveNext();
						}

						prs->Close();

						delete prs;
					}
				}
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE event_itemid = %d", //HARDCODE
					m_szQueueTableName.GetBuffer(0),
					nItem);

	//AfxMessageBox(szSQL);
				nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
				if(nReturn<DB_SUCCESS)
				{
					CString foo;
					foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
					AfxMessageBox(foo);
				}
			
			}
		}
		else
		if(m_s)
		{
			m_nClock = clock();
			m_bInCommand = TRUE;
			char szError[8096];
			int nReturn;
			nReturn=	m_net.SendLine((unsigned char*)m_szCmd.GetBuffer(0), m_szCmd.GetLength(), m_s, EOLN_CRLF, false, 10000, szError);
			if(nReturn<NET_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
			else
			{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Sent: ", logtmbuf, timestamp.millitm);
				fwrite(m_szCmd.GetBuffer(0), sizeof(char), m_szCmd.GetLength(), logfp);
				fprintf(logfp, "%c%c", 13, 10);
				fclose(logfp);
			}
}
				//recv the response.
		//		unsigned char* puc2 = NULL;
		//		unsigned long ulLen2=0
				unsigned char* puc = NULL;
				unsigned long ulLen=0;
				nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);

//				sprintf(szError, "\\\\%c%c", 13,10);
//				nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_EOLN|EOLN_TOKEN, szError);

				if(nReturn<NET_SUCCESS)
				{
					CString foo;
					foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
					AfxMessageBox(foo);
				}
				else
				{


					if(ulLen==0) //disconnected
					{
						GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
						GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
						GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
						m_net.CloseConnection(m_s);
						m_s = NULL;
						AfxMessageBox("disconnected");
					}
					else
					{
						if(puc)
						{
					// chances are, we got the part that tells us how much data we are supposed to be getting.
					// let's make sure we have complete data, using the  length, rather than the EOLN_TOKEN method, just in case tehre is a \\CRLF sequence coincidentally in the jpg image byte sequence.
//				AfxMessageBox((char*)puc);
//				CString qqqqq; 
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fclose(logfp);
			}
}
							
//The response result will take the form:
//*B\TH\SceneId\ByteCount\ByteSequence\\ <CR><LF>
							char bufferme[128];
							sprintf(bufferme,"*B\\TH\\%s\\", m_szScene);

							int nCount = m_lcResponses.GetItemCount();
							m_lcResponses.InsertItem(nCount, m_szCmd);


							int nLen =strlen(bufferme);
							if(strnicmp((char*)puc, bufferme, nLen)==0)
							{
								//parse it!
								int nBytes = atoi((char*)(puc+nLen));
								if(nBytes>0)
								{

									sprintf(bufferme,"%d\\", nBytes);
									nLen += strlen(bufferme);

//				qqqqq.Format("%d total bytes in response\r\n%d bytes expected for image",ulLen, nBytes); AfxMessageBox(qqqqq);

									bool bMore=false;
									unsigned char* puc2 = NULL;
									unsigned long ulLen2=0;
									if(ulLen<(unsigned int)(nBytes+nLen+4))  //+4 because of the ending CRLF
									{
										// have to receive more data.
										//check if more data:
										ulLen2 = (nBytes+nLen+4)-ulLen;

										timeval tv;
										tv.tv_sec = 2; 
										tv.tv_usec = 500000;  // timeout value
										int nNumSockets;
										fd_set fds;
										FD_ZERO(&fds);  // Zero this out each time
										FD_SET(m_s, &fds);
										nNumSockets = select(0, &fds, NULL, NULL, &tv);
										if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
										{
											
										}
										else
										if(
												(nNumSockets==0) // 0 = timed out, -1 = error
											||(!(FD_ISSET(m_s, &fds)))
											) 
										{ 

										} 
										else // there is recv data.
										{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
											//recv the response.

											nReturn=m_net.GetLine(&puc2, &ulLen2, m_s, NET_RCV_LENGTH, szError);
											if(nReturn<NET_SUCCESS)
											{
												CString foo;
												foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
												AfxMessageBox(foo);
											}
											else
											{
												if(ulLen2==0) //disconnected
												{
													GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
													GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
													GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
													m_net.CloseConnection(m_s);
													m_s = NULL;
													AfxMessageBox("disconnected");
												}
												else
													bMore=true;
											}
										}
									//	if((bMore)&&(puc2))
									//	{// use the second set to parse

//										}




									}

									CString resp = (char*)puc ;

									if(puc2)
									{
										resp += puc2;
									}

									m_lcResponses.SetItemText(nCount, 1, resp);
									m_lcResponses.EnsureVisible(nCount, FALSE);


									_mkdir("images");

									if(m_bThumbCP)
										sprintf(bufferme,"images\\%s-cp.jpg", m_szScene);
									else
										sprintf(bufferme,"images\\%s.jpg", m_szScene);

									FILE* fp;
									fp = fopen(bufferme, "wb");
									if(fp)
									{
										fwrite((puc+nLen), sizeof(char), ulLen-nLen, fp );

										if((bMore)&&(puc2)&&(ulLen2))
										{
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				fwrite(puc2, sizeof(char), ulLen2, logfp);
				fclose(logfp);
			}
}

											fwrite(puc2, sizeof(char), ulLen2, fp );
										}

										fclose(fp);

										HINSTANCE hi;
										hi=ShellExecute((HWND) NULL, NULL, bufferme, NULL, NULL, SW_HIDE);

									}
									else
									{
										CString szAlert;
										szAlert.Format("Could not write file. response was:\r\n%s",(char*)puc);
										if(puc2)
										{
											szAlert+= puc2;
										}
										AfxMessageBox(szAlert);							
									}	
									
									if(puc2)
									{
										free(puc2);
									}



								}
								else
								{
									CString szAlert;
									szAlert.Format("Byte sequence not recognized, response was:\r\n%s",(char*)puc);
									AfxMessageBox(szAlert);							
									m_lcResponses.SetItemText(nCount, 1, (char*)puc);
									m_lcResponses.EnsureVisible(nCount, FALSE);

								}

							}
							else
							{
								CString szAlert;
								szAlert.Format("Invalid response:\r\n%s",(char*)puc);
								AfxMessageBox(szAlert);			
								m_lcResponses.SetItemText(nCount, 1, (char*)puc);
								m_lcResponses.EnsureVisible(nCount, FALSE);
	
							}
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
							free(puc);
						}
						else
						{
							AfxMessageBox("valid repsonse not received");
						}
					}
				}
			}
			m_bInCommand = FALSE;
		}
		else
		{
			AfxMessageBox("no connection");
		}	

	}

	
}

void CIITestDlg::OnButtonViewlog() 
{
	HINSTANCE hi;
	hi=ShellExecute((HWND) NULL, NULL, LOGFILENAME, NULL, NULL, SW_HIDE);
}


void CIITestDlg::OnSelchangeComboBuffer() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_COMBO_BUFFER)->GetWindowText(m_szLayer);
	((CComboBox*)GetDlgItem(IDC_COMBO_BUFFER))->SelectString(-1, m_szLayer);
	
}
