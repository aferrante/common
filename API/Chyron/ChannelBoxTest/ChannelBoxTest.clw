; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CChannelBoxTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ChannelBoxTest.h"

ClassCount=3
Class1=CChannelBoxTestApp
Class2=CChannelBoxTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_CBTEST_DIALOG

[CLS:CChannelBoxTestApp]
Type=0
HeaderFile=ChannelBoxTest.h
ImplementationFile=ChannelBoxTest.cpp
Filter=N

[CLS:CChannelBoxTestDlg]
Type=0
HeaderFile=ChannelBoxTestDlg.h
ImplementationFile=ChannelBoxTestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDOK

[CLS:CAboutDlg]
Type=0
HeaderFile=ChannelBoxTestDlg.h
ImplementationFile=ChannelBoxTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342179331
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CBTEST_DIALOG]
Type=1
Class=CChannelBoxTestDlg
ControlCount=51
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_CONNECT,button,1342242816
Control4=IDC_EDIT_COMMAND,edit,1350631552
Control5=IDC_BUTTON_SEND,button,1476460544
Control6=IDC_COMBO_HOST,combobox,1344339970
Control7=IDC_COMBO_PORT,combobox,1344339970
Control8=IDC_COMBO_SCENE,combobox,1344340034
Control9=IDC_STATIC,static,1342308352
Control10=IDC_BUTTON_OP,button,1342242816
Control11=IDC_STATIC,static,1342308352
Control12=IDC_BUTTON_DELIM,button,1342242816
Control13=IDC_STATIC,button,1342177287
Control14=IDC_BUTTON_LO,button,1342242816
Control15=IDC_BUTTON_PL,button,1342242816
Control16=IDC_BUTTON_ST,button,1342242816
Control17=IDC_BUTTON_CL,button,1342242816
Control18=IDC_BUTTON_AN,button,1342242816
Control19=IDC_COMBO_DELIM,combobox,1344339970
Control20=IDC_STATIC,static,1342308352
Control21=IDC_BUTTON_UP,button,1342242816
Control22=IDC_LIST1,SysListView32,1350680589
Control23=IDC_STATIC,static,1342308352
Control24=IDC_BUTTON_AV,button,1342242816
Control25=IDC_LIST2,SysListView32,1350680589
Control26=IDC_COMBO_FIELD,combobox,1344340034
Control27=IDC_COMBO_VALUE,combobox,1344340034
Control28=IDC_STATIC,static,1342308352
Control29=IDC_LIST3,SysListView32,1350664717
Control30=IDC_STATIC,static,1342308352
Control31=IDC_CHECK_SENDQ,button,1342242819
Control32=IDC_CHECK_SENDQ2,button,1342242819
Control33=IDC_CHECK_SEND,button,1342242819
Control34=IDC_EDIT_DSN,edit,1350631552
Control35=IDC_EDIT_USER,edit,1350631552
Control36=IDC_EDIT_PW,edit,1350631552
Control37=IDC_BUTTON_CONNECT2,button,1342242816
Control38=IDC_EDIT_RECVTM,edit,1350631552
Control39=IDC_STATIC,static,1342308354
Control40=IDC_EDIT_SENDTM,edit,1350631552
Control41=IDC_STATIC,static,1342308354
Control42=IDC_CHECK_UTF8,button,1342242819
Control43=IDC_BUTTON_HEX,button,1342242816
Control44=IDC_COMBO_HEX,combobox,1344340034
Control45=IDC_CHECK_LYRIC,button,1342242819
Control46=IDC_CHECK_STAR,button,1208025091
Control47=IDC_COMBO_ALIAS,combobox,1210122242
Control48=IDC_COMBO_BUFFER,combobox,1210122242
Control49=IDC_STATIC_FB,static,1208090624
Control50=IDC_STATIC_ALIAS,static,1208090624
Control51=IDC_BUTTON_RECV,button,1476460544

