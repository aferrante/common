// ChannelBoxTestDlg.h : header file
//

#if !defined(AFX_CBTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
#define AFX_CBTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//#include "../IS2Core.h"
#include "..\..\..\MFC\ODBC\DBUtil.h"
#include "..\..\..\LAN\NetUtil.h"
#include "..\..\..\TXT\FileUtil.h"
/////////////////////////////////////////////////////////////////////////////
// CChannelBoxTestDlg dialog

typedef struct cbscene_t
{
	CString szName;
	CString szFields[128];
	int nNumFields;
	bool bSel;
} cbscene_t;

class CChannelBoxTestDlg : public CDialog
{
// Construction
public:
	CChannelBoxTestDlg(CWnd* pParent = NULL);	// standard constructor
	int LoadFields();

	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[4];
	CSize m_sizeDlgMin;

	CString m_szQueueTableName;
	CString m_szLibrettoFileLocation;
	

// Dialog Data
	//{{AFX_DATA(CChannelBoxTestDlg)
	enum { IDD = IDD_CBTEST_DIALOG };
	CListCtrl	m_lcResponses;
	CListCtrl	m_lcValues;
	CListCtrl	m_lcFields;
	CString	m_szCmd;
	CString	m_szHost;
	CString	m_szPort;
	CString	m_szScene;
	CString	m_szValue;
	CString	m_szField;
	CString	m_szDelim;
	BOOL	m_bUpdateFields;
	BOOL	m_bUpdateValues;
	BOOL	m_bSendAuto;
	CString	m_szDSN;
	CString	m_szPw;
	CString	m_szUser;
	int		m_nRecvTimeout;
	int		m_nSendTimeout;
	BOOL	m_bUTF8;
	CString	m_szHex;
	BOOL	m_bLyric;
	BOOL	m_bStar;
	CString	m_szAlias;
	CString	m_szLayer;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChannelBoxTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL


	CDBUtil m_db;
	CNetUtil m_net;
	SOCKET m_s;
	cbscene_t m_scenes[512];
	int m_nNumScenes;

	int FindScene(CString szName);
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CChannelBoxTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonSend();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnButtonQuery();
	afx_msg void OnButtonQueryValues();
	afx_msg void OnButtonClose();
	afx_msg void OnButtonDelim();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonOpen();
	afx_msg void OnButtonPlay();
	afx_msg void OnButtonStop();
	afx_msg void OnButtonUpdate();
	afx_msg void OnClickList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickList2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCloseupComboScene();
	afx_msg void OnCheckSend();
	afx_msg void OnSelchangeComboScene();
	afx_msg void OnButtonConnect2();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCheckLyric();
	afx_msg void OnButtonHex();
	afx_msg void OnEditchangeComboHex();
	afx_msg void OnCloseupComboHex();
	afx_msg void OnCloseupComboAlias();
	afx_msg void OnButtonRecv();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CBTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
