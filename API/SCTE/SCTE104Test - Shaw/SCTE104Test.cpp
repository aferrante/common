// SCTE104Test.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "SCTE104Test.h"
#include "SCTE104TestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSCTE104TestApp

BEGIN_MESSAGE_MAP(CSCTE104TestApp, CWinApp)
	//{{AFX_MSG_MAP(CSCTE104TestApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSCTE104TestApp construction

CSCTE104TestApp::CSCTE104TestApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSCTE104TestApp object

CSCTE104TestApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSCTE104TestApp initialization

BOOL CSCTE104TestApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  SetRegistryKey("VDS"); 

	CSCTE104TestDlg dlg;
	m_pMainWnd = &dlg;

//		AfxMessageBox("A1");

dlg.m_szLibrettoFileLocation = GetProfileString(
   "Libretto",
   "CSFFilePath" );
//	AfxMessageBox("A2");

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
