// SCTE104TestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SCTE104Test.h"
#include "SCTE104TestDlg.h"
#include <process.h>
#include "..\..\..\TXT\BufferUtil.h"
#include "..\SCTE104Util.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void SocketMonitorThread(void* pvArgs);

CSCTE104Util u;  //global just so the message number is global
CBufferUtil  bu;
#define LISTBOXMAX 500

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSCTE104TestDlg dialog

CSCTE104TestDlg::CSCTE104TestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSCTE104TestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSCTE104TestDlg)
	m_szHost = _T("127.0.0.1");
	m_szPort = _T("5167");
	m_szDSN = _T("Libretto");
	m_szPw = _T("");
	m_szUser = _T("sa");
	m_nRecvTimeout = 5000;
	m_nSendTimeout = 5000;
	m_nFlavor = 0;
	m_bLogTransactions = FALSE;
	m_szCmdName = _T("");
	m_bAutoKeepAlive = TRUE;
	m_szStart = _T("0");
	m_szID = _T("CURRENTID");
	m_szDesc = _T("Description of event");
	m_szStart2 = _T("01:23:58;13");
	m_szID2 = _T("REPL01");
	m_szDur = _T("1850");
	m_szDur2 = _T("00:55:00;01");
	m_szDesc2 = _T("Description of Replacement");
	m_bRepeatCommand = FALSE;
	m_nNumRepeats = 4;
	//}}AFX_DATA_INIT

	m_nLastType = -1;


	m_ul_splice_event_id=0x00000000;  
	m_us_unique_program_id=0x0000; 


	m_bInitRequestSent = FALSE;
	m_bInCommand = FALSE;
	m_szQueueTableName =  _T("Queue");
	m_szLibrettoFileLocation =  _T("libretto.csf");

	m_nCmdClock = -1;
	m_pvMessage = NULL;

	//��#
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_s = NULL;
	m_net.Initialize();
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_sizeDlgMin = CSize(488,609);
	
	m_bMonitorSocket=FALSE;
	m_bSocketMonitorStarted=FALSE;
	m_bStates = FALSE;

	m_bSupressEditChange = FALSE;

	m_bForceKeepAlive = FALSE;

}

void CSCTE104TestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSCTE104TestDlg)
	DDX_Control(pDX, IDC_LIST3, m_lcResponses);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_CBString(pDX, IDC_COMBO_PORT, m_szPort);
	DDX_Text(pDX, IDC_EDIT_DSN, m_szDSN);
	DDX_Text(pDX, IDC_EDIT_PW, m_szPw);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_RECVTM, m_nRecvTimeout);
	DDX_Text(pDX, IDC_EDIT_SENDTM, m_nSendTimeout);
	DDX_Radio(pDX, IDC_RADIO_EVERTZ, m_nFlavor);
	DDX_Check(pDX, IDC_CHECK_LOGXACT, m_bLogTransactions);
	DDX_CBString(pDX, IDC_COMBO_COMMAND, m_szCmdName);
	DDX_Check(pDX, IDC_CHECK_KEEP_ALIVE, m_bAutoKeepAlive);
	DDX_Text(pDX, IDC_EDIT_START, m_szStart);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDX_Text(pDX, IDC_EDIT_START2, m_szStart2);
	DDX_Text(pDX, IDC_EDIT_ID2, m_szID2);
	DDX_Text(pDX, IDC_EDIT_DUR, m_szDur);
	DDX_Text(pDX, IDC_EDIT_DUR2, m_szDur2);
	DDX_Text(pDX, IDC_EDIT_DESC2, m_szDesc2);
	DDX_Check(pDX, IDC_CHECK_REPEAT, m_bRepeatCommand);
	DDX_Text(pDX, IDC_EDIT_REPEAT, m_nNumRepeats);
	DDV_MinMaxInt(pDX, m_nNumRepeats, 1, 16000);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSCTE104TestDlg, CDialog)
	//{{AFX_MSG_MAP(CSCTE104TestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT2, OnButtonConnect2)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_RECV, OnButtonRecv)
	ON_BN_CLICKED(IDC_RADIO_EVERTZ, OnRadioEvertz)
	ON_BN_CLICKED(IDC_RADIO_ICON, OnRadioIcon)
	ON_BN_CLICKED(IDC_RADIO_LYR, OnRadioLyr)
	ON_BN_CLICKED(IDC_BUTTON_VIEWLOG, OnButtonViewlog)
	ON_BN_CLICKED(IDC_RADIO_G7, OnRadioG7)
	ON_CBN_CLOSEUP(IDC_COMBO_COMMAND, OnCloseupComboCommand)
	ON_BN_CLICKED(IDC_CHECK_KEEP_ALIVE, OnCheckKeepAlive)
	ON_WM_TIMER()
	ON_EN_CHANGE(IDC_EDIT_DESC, OnChangeEditDesc)
	ON_EN_CHANGE(IDC_EDIT_DESC2, OnChangeEditDesc2)
	ON_EN_CHANGE(IDC_EDIT_DUR, OnChangeEditDur)
	ON_EN_CHANGE(IDC_EDIT_DUR2, OnChangeEditDur2)
	ON_EN_CHANGE(IDC_EDIT_ID, OnChangeEditId)
	ON_EN_CHANGE(IDC_EDIT_ID2, OnChangeEditId2)
	ON_EN_CHANGE(IDC_EDIT_START, OnChangeEditStart)
	ON_EN_CHANGE(IDC_EDIT_START2, OnChangeEditStart2)
	ON_CBN_SELCHANGE(IDC_COMBO_COMMAND, OnSelchangeComboCommand)
	ON_BN_CLICKED(IDC_CHECK_REPEAT, OnCheckRepeat)
	ON_BN_CLICKED(IDC_CHECK_LOGXACT, OnCheckLogxact)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSCTE104TestDlg message handlers

BOOL CSCTE104TestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	CRect rc; 

	m_lcResponses.GetWindowRect(&rc);
	m_lcResponses.InsertColumn(0, "Command", LVCFMT_LEFT, (rc.Width()-16)/2, 0 );
	m_lcResponses.InsertColumn(1, "Response", LVCFMT_LEFT,(rc.Width()-16)/2, 1 );
	ListView_SetExtendedListViewStyle(m_lcResponses.m_hWnd, LVS_EX_FULLROWSELECT);


	//AfxMessageBox("X1");

	// TODO: Add extra initialization here
	FILE* fp = fopen("104host.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;int nSelP=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				int q=0;
				if(*pch=='*')
				{
					nSel = i; q=1; 
					pch++;
				}
				if(strlen(pch))
				{
				
					char* pchDel = strchr(pch, ':');
					if(pchDel)
					{
						*pchDel = 0; pchDel++;
					}

					if(strlen(pch)) {((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch); i++;}
				
					CComboBox* ptr = ((CComboBox*)GetDlgItem(IDC_COMBO_PORT));

					if((pchDel)&&(strlen(pchDel)))
					{
						int s = ptr->FindStringExact( -1, pchDel );
						if(s==CB_ERR)
						{
							ptr->AddString(pchDel);
						}
						if(q==1)
						{
							nSelP=ptr->FindStringExact( -1, pchDel );
						}
					}
					else
					{
						int s = ptr->FindStringExact( -1, "5167" );
						if(s==CB_ERR)
						{
							ptr->AddString("5167");
						}
						if(q==1)
						{
							nSelP=ptr->FindStringExact( -1, "5167" );
						}
					}
				}

				pch = strtok(NULL, "\r\n");
				
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				if (nSelP>=0)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SetCurSel(nSelP);
				}
				else
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SetCurSel(0);
				}


				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}
//	AfxMessageBox("X2");
	
	SetupFlavor();
//	AfxMessageBox("X3");

	SetTimer(666, 10, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSCTE104TestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSCTE104TestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSCTE104TestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSCTE104TestDlg::OnButtonConnect() 
{
	CWaitCursor c;
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		m_bMonitorSocket=FALSE;
		while(m_bSocketMonitorStarted) Sleep(10);

		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
		GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);
/*
		GetDlgItem(IDC_BUTTON_CONNECT2)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
*/
		m_net.CloseConnection(m_s);
		m_s = NULL;
		m_nCmdClock = -1;

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Connection closed gracefully.\r\n\r\n", logtmbuf, timestamp.millitm);
				
				fclose(logfp);
			}
}


	}
	else
	{
		UpdateData(TRUE);
//		char conn[256];
//		sprintf(conn, "%s", m_szHost);  // was m_szIP
//		AfxMessageBox(m_szHost);

		if((m_szHost.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
		}
		if((m_szPort.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->FindStringExact( -1, m_szPort )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->AddString(m_szPort);
			((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SelectString(-1, m_szPort);
		}
		char szError[8096];
		
		if(m_nSendTimeout<=0)
		{
			m_nSendTimeout=5000;
			GetDlgItem(IDC_EDIT_SENDTM)->SetWindowText("5000");
		}
		if(m_nRecvTimeout<=0)
		{
			m_nRecvTimeout=5000;
			GetDlgItem(IDC_EDIT_RECVTM)->SetWindowText("5000");
		}

		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect host");
		if(m_net.OpenConnection(m_szHost.GetBuffer(0), (short)(atol(m_szPort)), 
			&m_s, 
			m_nSendTimeout,
			m_nRecvTimeout,
			szError)<NET_SUCCESS)
		{
			m_s = NULL;
			AfxMessageBox(szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
		}
		else
		{
			m_bInitRequestSent = FALSE;
			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Monitor setup");
			GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);

			// set up new connection to have init_request be the first command to be sent.
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, "init_request");
//			OnCloseupComboCommand();
			OnSelchangeComboCommand();
			GetDlgItem(IDC_COMBO_COMMAND)->EnableWindow(FALSE);  // disable until init_request is sent.


			m_bMonitorSocket=FALSE;
			while(m_bSocketMonitorStarted) Sleep(10);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Monitor start");

			m_bMonitorSocket=TRUE;
			_beginthread(SocketMonitorThread, 0, (void*)this);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Monitor started");

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Connection opened.\r\n\r\n", logtmbuf, timestamp.millitm);
				
				fclose(logfp);
			}
}
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect host");
		}
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);

	}
}

void CSCTE104TestDlg::OnButtonSend() 
{
	int nRepeats=0;
	do
	{
	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
	{
		UpdateData(TRUE);
		char szError[8096];
		char szSQL[DB_SQLSTRING_MAXLEN];
		int nItem = clock();

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username, event_itemid) \
VALUES ('%s','', 256, '%s', 0, 'sys', %d)", //HARDCODE
			m_szQueueTableName.GetBuffer(0),
			m_szCmd.GetBuffer(0),
			m_szHost.GetBuffer(0),
			nItem
		);
//AfxMessageBox(szSQL);


		int nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
		if(nReturn<DB_SUCCESS)
		{
			CString foo;
			foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
			AfxMessageBox(foo);
		}
		else
		{
			//recv the response.
			Sleep(250);

			char szSQL[DB_SQLSTRING_MAXLEN];
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action=128 and event_itemid = %d", //HARDCODE
							m_szQueueTableName.GetBuffer(0),
							nItem);
			int nIndex = 0;
			while(nIndex<=0)
			{
				Sleep(100);
//AfxMessageBox(szSQL);
				CRecordset* prs = m_db.Retrieve(m_db.m_ppdbConn[0], szSQL, szError);
				if(prs)
				{
		//			while ((!prs->IsEOF()))
					if (!prs->IsEOF())  // just do the one record, if there is one
					{
						try
						{
							CString szMessage;
							prs->GetFieldValue("remote", szMessage);//HARDCODE
//AfxMessageBox(szMessage);
							int nCount = m_lcResponses.GetItemCount();
							m_lcResponses.InsertItem(nCount, m_szCmd);
							m_lcResponses.SetItemText(nCount, 1, szMessage.GetBuffer(0) );
							m_lcResponses.EnsureVisible(nCount, FALSE);
							while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest


							m_nCmdClock = clock();
							nIndex++;
						}
						catch(CException* e)// CDBException *e, CMemoryException *m)  
						{
//AfxMessageBox("exception");
							e->Delete();
						} 
						catch( ... )
						{
//AfxMessageBox("exception2");
						}

						prs->MoveNext();
					}

					prs->Close();

					delete prs;
				}
			}
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE event_itemid = %d", //HARDCODE
				m_szQueueTableName.GetBuffer(0),
				nItem);

//AfxMessageBox(szSQL);
			nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
			if(nReturn<DB_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
		
		}
	}
	else
	if(m_s)
	{
		m_bInCommand = TRUE;
		UpdateData(TRUE);
		char szError[8096];
		int nReturn;
		if(m_nSendTimeout<=0)
		{
			m_nSendTimeout=5000;
			GetDlgItem(IDC_EDIT_SENDTM)->SetWindowText("5000");
		}


		if(m_pvMessage)
		{
			unsigned char* buffer = NULL;
			unsigned long buflen=0;
			unsigned char ucMsg;
			int nCommand = 0x0000ffff&((((CSCTE104SingleMessage*)m_pvMessage)->OpId1High<<8)|(((CSCTE104SingleMessage*)m_pvMessage)->OpId1Low));

			if(nCommand == 0xffff)
			{
				ucMsg = (nRepeats==0)?u.IncrementMessageNumber():u.m_ucLastMessageNumber;

//				CString Q;
//				Q.Format("MULTI %04x",  nCommand);
//				AfxMessageBox(Q);

				unsigned long ul_splice_event_id = (nRepeats==0)?IncrementSpliceEventID():m_ul_splice_event_id;
				unsigned short us_unique_program_id = (nRepeats==0)?IncrementUniqueProgID():m_us_unique_program_id;

/*
				if(
					  (((CSCTE104MultiMessage*)m_pvMessage)->m_ppscte104Ops)
					&&(((CSCTE104MultiMessage*)m_pvMessage)->m_ppscte104Ops[0])
					&&(((CSCTE104MultiMessage*)m_pvMessage)->m_ppscte104Ops[0]->proprietary_data_ptr)
					)
				{

					SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) ((CSCTE104MultiMessage*)m_pvMessage)->m_ppscte104Ops[0]->proprietary_data_ptr;
					
					unsigned char uch = psrd->splice_event_id[0]; //retain source
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | (uch&0xf0);    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first


				}

*/



				// multi obj
				// removing SetTimestamp for immediate messages. (time type = 0)
/*
				SCTE104HMSF_t HMSF;

				_timeb timestamp;
				_ftime(&timestamp);

				// fill the data with drop frame

				int rem = (timestamp.time- (timestamp.timezone*60)+(timestamp.dstflag?3600:0));
				while(rem<0) rem+=86400;
				while(rem>86399) rem-=86400;
				double ms = (rem%86400)*1000 + timestamp.millitm;
        int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
        int deci = (int)(fr / 17982);
        rem = fr % 17982;
        if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

        HMSF.hours = deci / 6; 
        HMSF.minutes = ((deci % 6) * 10) + (rem / 1800); 
        HMSF.seconds = (rem % 1800) / 30; 
        HMSF.frames = rem % 30;

				((CSCTE104MultiMessage*)m_pvMessage)->SetTimestamp(HMSF);
*/
				((CSCTE104MultiMessage*)m_pvMessage)->m_scte104Hdr.MessageNumber = ucMsg;
				buffer = u.ReturnMessageBuffer((CSCTE104MultiMessage*)m_pvMessage);
				if(buffer)
				{
					buflen = 0x0000ffff&((((CSCTE104MultiMessage*)m_pvMessage)->m_scte104Hdr.MsgSizeHigh<<8) + ((CSCTE104MultiMessage*)m_pvMessage)->m_scte104Hdr.MsgSizeLow);
				}
			}
			else
			{

//				if(( nCommand == 0x0001 )||( nCommand == 0x0003 )) // there are only 7 commands.  both single op commands are 
//				{
					ucMsg = u.IncrementSecondaryMessageNumber();
//				}

				// need to create a message number that starts at u.m_ucLastMessageNumber + 128 and increments from there with a secondary incrementer

//				CString Q;
//				Q.Format("SINGLE %04x", nCommand);
//				AfxMessageBox(Q);
/*
				if( nCommand == 0x0001) // init message
				{
										AfxMessageBox("init msg");
				}
				else
				if( nCommand == 0x0003) // keep_alive message
				{
									AfxMessageBox("keep_alive msg");
				}
	*/		
				// single obj
				((CSCTE104SingleMessage*)m_pvMessage)->MessageNumber = ucMsg;

				buffer = u.ReturnMessageBuffer((CSCTE104SingleMessage*)m_pvMessage);
				if(buffer)
				{					
					buflen = 0x0000ffff&((((CSCTE104SingleMessage*)m_pvMessage)->OpMessageLengthHigh<<8) + ((CSCTE104SingleMessage*)m_pvMessage)->OpMessageLengthLow);
				}
			}


			CBufferUtil bu;
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

unsigned long ulSend = buflen;
char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_FULLHEX);

		m_nClock = clock();
		
		nReturn=	m_net.SendLine(buffer, buflen, m_s, EOLN_NONE, false, m_nSendTimeout, szError);
		if(nReturn<NET_SUCCESS)
		{

			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
			m_net.CloseConnection(m_s);
			m_s = NULL;
			m_nCmdClock = -1;
			m_bMonitorSocket=FALSE;


if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Disconnected with error code %d: %s\r\n\r\n", logtmbuf, timestamp.millitm, nReturn,szError);
				
				fclose(logfp);
			}
}


			CString foo;
			foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
			AfxMessageBox(foo);
		}
		else
		{


if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);



				fprintf(logfp, "%s%03d Sent %d bytes: ", logtmbuf, timestamp.millitm, buflen);

				if(pchSnd)
				{
					fwrite(pchSnd, sizeof(char), ulSend, logfp);
//					free(pchSnd); NO!  do not free, we use later
				}
				else
				{
					fwrite(buffer, sizeof(char), buflen, logfp);
				}
				fprintf(logfp, "%c%c", 13, 10);
				fclose(logfp);
			}
}

			GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			m_nClock = clock();
			//recv the response.
	//		unsigned char* puc2 = NULL;
	//		unsigned long ulLen2=0
			unsigned char* puc = NULL;
			unsigned long ulLen=0;
	//		Sleep(500);
			nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
			if(nReturn<NET_SUCCESS)
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
				m_net.CloseConnection(m_s);
				m_s = NULL;
				m_nCmdClock = -1;
				m_bMonitorSocket=FALSE;

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Disconnected with error code %d: %s\r\n\r\n", logtmbuf, timestamp.millitm, nReturn,szError);
				
				fclose(logfp);
			}
}

				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
			else
			{

//CString Q;
//Q.Format("buflen %d, puc = %08x, %d", ulLen, puc, nReturn);
//AfxMessageBox(Q);
//AfxMessageBox(szError);


//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

/*
				//check if more data:
				bool bMore=false;

				timeval tv;
				tv.tv_sec = 0; 
				tv.tv_usec = 500000;  // timeout value
				int nNumSockets;
				fd_set fds;
				FD_ZERO(&fds);  // Zero this out each time
				FD_SET(m_s, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);
				if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
				{
					
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(m_s, &fds)))
					) 
				{ 

				} 
				else // there is recv data.
				{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
					//recv the response.
					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, char* szError);
					if(nReturn<NET_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
						AfxMessageBox(foo);
					}
					else
					{
						if(ulLen==0) //disconnected
						{
							GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
							GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
							GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
							m_net.CloseConnection(m_s);
							m_s = NULL;
							AfxMessageBox("disconnected");
						}
						else
							bMore=true;
					}
				}
				if(bMore)
				{// use the second set to parse
				}
*/


				if(ulLen==0) //disconnected
				{
					GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
					GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
					GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
					m_net.CloseConnection(m_s);
					m_s = NULL;
					m_nCmdClock = -1;

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Disconnected!\r\n\r\n", logtmbuf, timestamp.millitm);
				
				fclose(logfp);
			}
}


					m_bMonitorSocket=FALSE;

					AfxMessageBox("disconnected");
				}
				else
				{
					if(puc)
					{

unsigned long ulRecv = ulLen;
char* pchRecv = bu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, ulLen);
				
				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
				}
				else
				{
					fwrite((char*)puc, sizeof(char), ulLen, logfp);
				}

				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();

						if(pchSnd)
						{
							m_lcResponses.InsertItem(nCount, pchSnd);
						}
						else
						{
							m_lcResponses.InsertItem(nCount, (char*)buffer);
						}
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}
						m_lcResponses.EnsureVisible(nCount, FALSE);
							
						while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
						

						// let's make the command timer just react on keep_alive only. or init, since init will force the keep alive

						if(( nCommand == 0x0001 )||( nCommand == 0x0003 ))
						{
//							m_nCmdClock = clock();
							nRepeats = m_nNumRepeats+1; // defeats repeat on init and keep alive, it is just for the other commands.
						}


						if( nCommand == 0x0001 )
						{
							m_bInitRequestSent = TRUE;
							GetDlgItem(IDC_COMBO_COMMAND)->EnableWindow(TRUE);  // disable until init_request is sent.
						}

						m_nCmdClock = clock(); // put the clock reset outside so that any command send will reset it


if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}


			if( nCommand == 0x0001 )
			{
				if(m_bAutoKeepAlive) m_bForceKeepAlive=TRUE;
//				AfxMessageBox("TRUE");
			}


						free(puc);
						if(pchRecv) free(pchRecv);


					}
					else
					{
						AfxMessageBox("valid repsonse not received");
					}
				}
			}

		}
		m_bInCommand = FALSE;
		if(pchSnd) free(pchSnd);

		if(buffer) free(buffer);
		}
		else
		{
			AfxMessageBox("Error: no message object");
		}

	}
	else
	{
		AfxMessageBox("no connection");
	}
	nRepeats++;
	} while((m_bRepeatCommand)&&(nRepeats<=m_nNumRepeats)&&(m_s)); // allow only for socket connection
}

void CSCTE104TestDlg::OnDestroy() 
{

	CDialog::OnDestroy();
	
}

void CSCTE104TestDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	KillTimer(666);
	
	FILE* fp = fopen("104host.cfg", "wb");
	if(fp)
	{
		UpdateData(TRUE);
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		int nCountP=((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetCount();
		int nSelP=((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s:%s\n", m_szHost, m_szPort);
		}
		if(nCount>0)
		{
			int i=0;  int j=0;
			while(i<max(nCount, nCountP))
			{
				CString szText, szPort;
				if(i<nCount)((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				else szText="";

				if(nSel==i)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetLBText( nSelP, szPort );
					fprintf(fp, "*");
					fprintf(fp, "%s:%s\n", szText, szPort);
				}
				else
				{
					if(nSelP==j)
					{
						j++;
					}
					if(j<nCountP)
					{
						((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetLBText( j, szPort );
					}
					else szPort="";
					fprintf(fp, "%s:%s\n", szText, szPort);
				}
				j++;
				i++;
			}
		}
		fclose(fp);
	}



	CDialog::OnCancel();
}




void CSCTE104TestDlg::OnButtonConnect2() 
{
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		if(AfxMessageBox("The Channel Box must not be connected directly, in order to send commands to Libretto.\r\nDisconnect and continue?", MB_YESNO)!=IDYES) return;
		OnButtonConnect();
	}

	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0))
	{
		m_db.DisconnectDatabase(m_db.m_ppdbConn[0]);
		GetDlgItem(IDC_BUTTON_CONNECT2)->SetWindowText("Connect to Libretto");
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_db.RemoveConnection(m_db.m_ppdbConn[0]);
	}
	else
	{
		UpdateData(TRUE);
		char szError[8096];


		// Lets get the latest csf.
		CFileUtil file;
		// get settings.
//AfxMessageBox(m_szLibrettoFileLocation);

		int rv = file.GetSettings(m_szLibrettoFileLocation.GetBuffer(0), false);


		while(!(rv&FILEUTIL_FILE_EXISTS))
		{
			CFileDialog fdlg(TRUE,
											 "csf",
											 "Libretto.csf",
												OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_LONGNAMES|OFN_NOCHANGEDIR|OFN_PATHMUSTEXIST );

			if(fdlg.DoModal()==IDOK)
			{
				m_szLibrettoFileLocation = fdlg.GetPathName();
				AfxGetApp()->WriteProfileString("Libretto","CSFFilePath", m_szLibrettoFileLocation);
			

				rv = file.GetSettings(m_szLibrettoFileLocation.GetBuffer(0), false);

				if(!(rv&FILEUTIL_FILE_EXISTS))
				{
					if(AfxMessageBox("The setting could not be extracted.\r\nDo you wish to select another file?", MB_YESNO)!=IDYES) break;
				}
			}
			else //cancelled
				return;

		}

		if(rv&FILEUTIL_FILE_EXISTS)
		{
			m_szQueueTableName = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue");  // the CommandQueue table name
//AfxMessageBox("got from csf");
		}
		else 
		{
			return;
		}


//AfxMessageBox(m_szQueueTableName);


		CDBconn* pdb = m_db.CreateNewConnection(m_szDSN.GetBuffer(1),m_szUser.GetBuffer(1),m_szPw.GetBuffer(1));
		if((pdb)&&(m_db.ConnectDatabase(pdb,szError))>=NET_SUCCESS)
		{
//			AfxMessageBox("Connected!");

			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT2)->SetWindowText("Disconnect Libretto");
			GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);
		}
		else
		{
			AfxMessageBox(szError);
			if(pdb)
			{
				m_db.DisconnectDatabase(pdb);
				m_db.RemoveConnection(pdb);
			}
		}
	}
}

void CSCTE104TestDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
//sizing stuff
//	AfxMessageBox("OSW1");
	if(m_bNewSizeInit)
	{
		int nCtrlID;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<8;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST3;break;
			case 1: nCtrlID=IDC_EDIT_COMMAND;break;
			case 2: nCtrlID=IDC_BUTTON_SEND;break;
			case 3: nCtrlID=IDC_BUTTON_RECV;break;
			case 4: nCtrlID=IDC_STATIC_TIMEOUT;break;
			case 5: nCtrlID=IDC_CHECK_REPEAT;break;
			case 6: nCtrlID=IDC_EDIT_REPEAT;break;
			case 7: nCtrlID=IDC_STATIC_REPEAT;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void CSCTE104TestDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_EDIT_COMMAND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[1].Width()-dx,  // goes with right edge
			m_rcCtrl[1].Height(), 
			SWP_NOZORDER|SWP_NOMOVE
			);

		pWnd=GetDlgItem(IDC_STATIC_TIMEOUT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[4].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[4].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_BUTTON_SEND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[2].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[2].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_BUTTON_RECV);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[3].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[3].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_LIST3);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			m_rcCtrl[0].Width()-dx,  // goes with right edge
			m_rcCtrl[0].Height()-dy,  // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		pWnd=GetDlgItem(IDC_CHECK_REPEAT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[5].left-dx,  // goes with right edge
			m_rcCtrl[5].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
		pWnd=GetDlgItem(IDC_EDIT_REPEAT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[6].left-dx,  // goes with right edge
			m_rcCtrl[6].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
		pWnd=GetDlgItem(IDC_STATIC_REPEAT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[7].left-dx,  // goes with right edge
			m_rcCtrl[7].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
		


		int col1w= m_lcResponses.GetColumnWidth(0);
		m_lcResponses.SetColumnWidth(1, (m_rcCtrl[0].Width()-dx-16-col1w) ); 


		for (int i=0;i<8;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST3;break;
			case 1: nCtrlID=IDC_EDIT_COMMAND;break;
			case 2: nCtrlID=IDC_BUTTON_SEND;break;
			case 3: nCtrlID=IDC_BUTTON_RECV;break;
			case 4: nCtrlID=IDC_STATIC_TIMEOUT;break;
			case 5: nCtrlID=IDC_CHECK_REPEAT;break;
			case 6: nCtrlID=IDC_EDIT_REPEAT;break;
			case 7: nCtrlID=IDC_STATIC_REPEAT;break;
			}
			
			GetDlgItem(nCtrlID)->Invalidate();

		}

	}		
}

void CSCTE104TestDlg::SetupFlavor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	switch(m_nFlavor)
	{
	case II_TYPE_LYR:
		{
		} break;
	case II_TYPE_G7:
		{		
		} break;
	case II_TYPE_ICON:
		{
		} break;

	case II_TYPE_EVERTZ:
	default:
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->ResetContent();

/* Disney values
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Event Notification");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Replace Ad");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Replace Content");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Rejoin Main");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Current Time");
*/

// Fox News Channel values
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("National Break");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Local 60 second");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Local 120 second");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Break End");


//Shaw values:
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("out_of_network TRUE");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("out_of_network FALSE");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Roll Invidi");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Roll Thunder Bay");


			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("init_request");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("alive_request");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, "init_request");
//			OnCloseupComboCommand();
			OnSelchangeComboCommand();

//			GetDlgItem(IDC_COMBO_COMMAND)->EnableWindow(TRUE);
//			GetDlgItem(IDC_COMBO_COMMAND)->ShowWindow(SW_SHOW);

		} break;
	}
}



void CSCTE104TestDlg::OnMonitorButtonRecv()
{
	if(m_bInCommand) return;
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
		m_nCmdClock = -1;
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
//		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
			m_nCmdClock = -1;
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
				m_net.CloseConnection(m_s);
				m_nCmdClock = -1;
				m_s = NULL;

				m_bMonitorSocket=FALSE;

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Disconnected!\r\n\r\n", logtmbuf, timestamp.millitm);
				
				fclose(logfp);
			}
}

				m_bMonitorSocket=FALSE;


				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{

unsigned long ulRecv = ulLen;
char* pchRecv = bu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, ulLen);
				
				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
				}
				else
				{
					fwrite((char*)puc, sizeof(char), ulLen, logfp);
				}
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);

				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();

						m_lcResponses.InsertItem(nCount, "");
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
							free(pchRecv);
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}
						m_lcResponses.EnsureVisible(nCount, FALSE);
						
						while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest

/*
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
					int nCount = m_lcResponses.GetItemCount();
					m_lcResponses.InsertItem(nCount, "");
					m_lcResponses.SetItemText(nCount, 1, (char*)puc );
					m_lcResponses.EnsureVisible(nCount, FALSE);
*/
					free(puc);
				}
			}
		}

	}
}

void CSCTE104TestDlg::OnButtonRecv() 
{
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_nCmdClock = -1;
		m_net.CloseConnection(m_s);
		m_s = NULL;
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
			m_nCmdClock = -1;
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
				m_nCmdClock = -1;
				m_net.CloseConnection(m_s);
				m_s = NULL;

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Disconnected!\r\n\r\n", logtmbuf, timestamp.millitm);
				
				fclose(logfp);
			}
}
				m_bMonitorSocket=FALSE;


				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{



unsigned long ulRecv = ulLen;
char* pchRecv = bu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, ulLen);
				
				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
				}
				else
				{
					fwrite((char*)puc, sizeof(char), ulLen, logfp);
				}
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);

				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();

						m_lcResponses.InsertItem(nCount, "");
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
							free(pchRecv);
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}
						m_lcResponses.EnsureVisible(nCount, FALSE);
						while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest

/*
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
					int nCount = m_lcResponses.GetItemCount();
					m_lcResponses.InsertItem(nCount, "");
					m_lcResponses.SetItemText(nCount, 1, (char*)puc );
					m_lcResponses.EnsureVisible(nCount, FALSE);
*/
					free(puc);
				}
			}
		}

	}
}

void CSCTE104TestDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();  prevent cancellation with enter.
}

void CSCTE104TestDlg::OnRadioEvertz() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CSCTE104TestDlg::OnRadioIcon() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CSCTE104TestDlg::OnRadioLyr() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CSCTE104TestDlg::OnRadioG7() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void SocketMonitorThread(void* pvArgs)
{
	CSCTE104TestDlg* p = (CSCTE104TestDlg*)pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{
			timeval tv;
			tv.tv_sec = 0; 
			tv.tv_usec = 500;  // timeout value
			int nNumSockets;
			fd_set fds;

			FD_ZERO(&fds);  // Zero this out each time
			FD_SET(p->m_s, &fds);

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
			if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
			{
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(p->m_s, &fds)))
				) 
			{ 
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else // there is recv data.
			{ // wait some delay.
				if((clock() > p->m_nClock+1000)&&(!p->m_bInCommand))
				{
					p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
					if(clock() > p->m_nClock+1000) // five seconds is a huge timeout.
						p->OnMonitorButtonRecv();
				}
			}
			if(p->m_bMonitorSocket) Sleep(100);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}



void CSCTE104TestDlg::OnButtonViewlog() 
{
	HINSTANCE hi;
	hi=ShellExecute((HWND) NULL, NULL, LOGFILENAME, NULL, NULL, SW_HIDE);
}


void CSCTE104TestDlg::OnCloseupComboCommand() 
{
//	OnSelchangeComboCommand();

}


void CSCTE104TestDlg::OnCheckKeepAlive() 
{
	UpdateData(TRUE);

//	if(m_bAutoKeepAlive) AfxMessageBox("Auto Keep Alive"); else  AfxMessageBox("NOT Auto Keep Alive"); 

	if(m_bAutoKeepAlive)
	{
	}
	else
	{
	}
}

void CSCTE104TestDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent==666)
	{
		if(m_nCmdClock>0)
		{
			CString s;
			int nElapsed = clock()-m_nCmdClock;
			s.Format("%d ms", nElapsed);
			GetDlgItem(IDC_STATIC_TIMEOUT)->SetWindowText(s);

			if(!m_bInCommand)
			if(m_bAutoKeepAlive)
			if(
					(
						((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
					||(m_s)
					)
				&&(
				    (nElapsed>54000)// SCTE104 hardcoded to 60 seconds, we have a 5000 timeout, so...
					||(m_bForceKeepAlive)
					)
				) 
			{
				// get old selection so we can set it back
				CComboBox* p =((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND));

				int s = p->GetCurSel();

				CString szCmd;
				if(s>=0) 
				{
					int len = p->GetLBTextLen( s );
					p->GetLBText( s, szCmd.GetBuffer(len) );
					szCmd.ReleaseBuffer();
				}

				((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, "alive_request");
				OnSelchangeComboCommand();
//				OnCloseupComboCommand();

				// send the command when able
				while(m_bInCommand) Sleep(1);
				OnButtonSend();

				// set everything back:
				((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, szCmd);
				OnSelchangeComboCommand();
//				OnCloseupComboCommand();

				m_bForceKeepAlive = FALSE;
			}
		}
		else
		{
			if(m_nCmdClock<0)
			{
				m_nCmdClock=0;
				GetDlgItem(IDC_STATIC_TIMEOUT)->SetWindowText("time elapsed");
			}
		}
	}	
	CDialog::OnTimer(nIDEvent);
}

void CSCTE104TestDlg::OnChangeEditDesc() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_DESC);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
//	AfxMessageBox(szText);
	TruncateText(&szText, 32);
//	AfxMessageBox("2");
	m_szDesc = szText;
	p->SetWindowText(m_szDesc);
	p->SetSel(x, y);

//	AfxMessageBox("3");
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();

}

void CSCTE104TestDlg::OnChangeEditDesc2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_DESC2);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
//	AfxMessageBox(szText);
	TruncateText(&szText, 32);
//	AfxMessageBox("2");
	m_szDesc2 = szText;
	p->SetWindowText(m_szDesc2);
	p->SetSel(x, y);

//	AfxMessageBox("3");
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();
}

void CSCTE104TestDlg::OnChangeEditDur() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_DUR);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
//	FormatTime(&szText);
	FormatNum(&szText, 2);

	m_szDur = szText;
	p->SetWindowText(m_szDur);
	p->SetSel(x, y);
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();
}

void CSCTE104TestDlg::OnChangeEditDur2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_DUR2);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
	FormatTime(&szText);

	m_szDur2 = szText;
	p->SetWindowText(m_szDur2);
	p->SetSel(x, y);
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();

}

void CSCTE104TestDlg::OnChangeEditId() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_ID);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
//	AfxMessageBox(szText);
	TruncateText(&szText, 16);
//	AfxMessageBox("2");
	m_szID = szText;
	p->SetWindowText(m_szID);
	p->SetSel(x, y);

//	AfxMessageBox("3");
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();

}

void CSCTE104TestDlg::OnChangeEditId2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_ID2);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
//	AfxMessageBox(szText);
	TruncateText(&szText, 16);
//	AfxMessageBox("2");
	m_szID2 = szText;
	p->SetWindowText(m_szID2);
	p->SetSel(x, y);

//	AfxMessageBox("3");
	m_bSupressEditChange = FALSE;

	
	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();
}

void CSCTE104TestDlg::OnChangeEditStart() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) 
	{
//		AfxMessageBox("foo");
		return;
	}
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_START);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
//	FormatTime(&szText);
	FormatNum(&szText, 2);

	m_szStart = szText;
	p->SetWindowText(m_szStart);
	p->SetSel(x, y);
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();
}

void CSCTE104TestDlg::OnChangeEditStart2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	if(m_bSupressEditChange) return;
	CEdit* p = 	(CEdit*)GetDlgItem(IDC_EDIT_START2);
	m_bSupressEditChange = TRUE;
	int x,y;
	p->GetSel(x, y);
	CString szText;
	p->GetWindowText(szText);
	FormatTime(&szText);

	m_szStart2 = szText;
	p->SetWindowText(m_szStart2);
	p->SetSel(x, y);
	m_bSupressEditChange = FALSE;

	OnSelchangeComboCommand(); // have to update the command with the new value
	p->SetFocus();
}


int CSCTE104TestDlg::TruncateText(CString* pszText, int nLimit)
{
	if(nLimit<0) nLimit=0;
	if(pszText)
	{
		char* p = pszText->GetBuffer(nLimit+1);
		if(p)
		{
//			AfxMessageBox(p);
			*(p+nLimit) = 0;
//			AfxMessageBox(p);
		}
		pszText->ReleaseBuffer(nLimit);
//			AfxMessageBox(*pszText);
	}

	return 0;
}

unsigned long CSCTE104TestDlg::IncrementSpliceEventID()
{
	if(m_ul_splice_event_id<0x0fffffff) 
		m_ul_splice_event_id++;
	else
		m_ul_splice_event_id=0x00000001; // loop around.
	return m_ul_splice_event_id;

}

unsigned short CSCTE104TestDlg::IncrementUniqueProgID()
{
	if(m_us_unique_program_id<0xffff) 
		m_us_unique_program_id++;
	else
		m_us_unique_program_id=0x0001; // loop around.
	return m_us_unique_program_id;

}


int CSCTE104TestDlg::FormatNum(CString* pszText, int nBytes)
{
	if(pszText)
	{
		int n = atoi(*pszText);
		switch(nBytes)
		{
		case 1:
			{
				if(n<0) n=0;
				while(n>0xff)
				{
					n/=10;  // removes new offending digit
				//	n=0xff;
				}
			}
		case 2:
			{
				if(n<0) n=0;
				while(n>0xffff)
				{
					n/=10;  // removes new offending digit
//					n=0xffff;
				}
			}
		case 3:
			{
				if(n<0) n=0;
				while(n>0xffffff)
				{
					n/=10;  // removes new offending digit
					n=0xffffff;
				}
			}
		default:
			{
				if(n<0) n=0;
				while(n>0xffffffff)
				{
					n/=10;  // removes new offending digit
					n=0xffffffff;
				}
			}
		}
		pszText->Format("%d", n);

	}

	return 0;

}

int CSCTE104TestDlg::FormatTime(CString* pszText)
{
	if(pszText)
	{
		char* p = pszText->GetBuffer(12);
		if(p)
		{
			int n = 0;
			if((	*(p+0) < 48)||(	*(p+0) > 57)) *(p+0) = '0';
			if((	*(p+1) < 48)||(	*(p+1) > 57)) *(p+1) = '0';

			n = ((*p)-48)*10 + ((*(p+1))-48);
			if(n>23)
			{
			 *(p+0) = '2';
			 *(p+1) = '3';
			}
			
			if(	*(p+2) != ':') *(p+2) = ':';

			if((	*(p+3) < 48)||(	*(p+3) > 57)) *(p+3) = '0';
			if((	*(p+4) < 48)||(	*(p+4) > 57)) *(p+4) = '0';

			n = ((*(p+3))-48)*10 + ((*(p+4))-48);
			if(n>59)
			{
			 *(p+3) = '5';
			 *(p+4) = '9';
			}
			
			if(	*(p+5) != ':') *(p+5) = ':';

			if((	*(p+6) < 48)||(	*(p+6) > 57)) *(p+6) = '0';
			if((	*(p+7) < 48)||(	*(p+7) > 57)) *(p+7) = '0';

			n = ((*(p+6))-48)*10 + ((*(p+7))-48);
			if(n>59)
			{
			 *(p+6) = '5';
			 *(p+7) = '9';
			}

			if(	*(p+8) != ';') *(p+8) = ';';

			if((	*(p+9) < 48)||(	*(p+9) > 57)) *(p+9) = '0';
			if((	*(p+10) < 48)||(	*(p+10) > 57)) *(p+10) = '0';
		
			n = ((*(p+9))-48)*10 + ((*(p+10))-48);
			if(n>29)
			{
			 *(p+9) = '2';
			 *(p+10) = '9';
			}
			*(p+11) = 0;
		}
		pszText->ReleaseBuffer();
	}

	return 0;
}


void CSCTE104TestDlg::OnSelchangeComboCommand() 
{
	// TODO: Add your control notification handler code here
	CComboBox* p =((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND));

	int s = p->GetCurSel();

	if(s>=0) 
	{
    int len = p->GetLBTextLen( s );
    p->GetLBText( s, m_szCmdName.GetBuffer(len) );
    m_szCmdName.ReleaseBuffer();
	}

	int n = ((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->FindStringExact( -1, m_szCmdName );
	int nType = 0;

	if((m_szCmdName.GetLength()>0)&&(n!=CB_ERR))
	{

		CString szText;
		// make the appropriate command:
		void* pm = NULL;
		BOOL bSingle = FALSE;

		if((m_szCmdName.Compare("alive_request")==0)||(m_szCmdName.Compare("init_request")==0))
		{
			pm = u.CreateSingleMessageObject();
			bSingle = TRUE;

			CSCTE104SingleMessage* sm = (CSCTE104SingleMessage*)pm;

			if(m_szCmdName.Compare("alive_request")==0)
			{
				sm->OpId1High =0x00;
				sm->OpId1Low =0x03;
			}
			else
			{
				sm->OpId1High =0x00;
				sm->OpId1Low =0x01;
			}

			// we are going to override the single messages with a secondary counter expected value.
			int counter = u.m_ucLastSecondaryMessageNumberInc+1;
			if(counter>u.m_ucSecondaryMessageNumberofValues) counter=0; // loop around, smaller loop
			// now calc the return value:
			
			int rv = u.m_ucLastMessageNumber + counter + 128 - (u.m_ucSecondaryMessageNumberofValues/2); // 128 number loop averaging 180 degree phase on primary message number
			
			while(rv>255)	rv-=256; // loop around.


			sm->MessageNumber = (unsigned char) rv;


			// hide irrelevant fields.
			GetDlgItem(IDC_STATIC_ID)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_ID)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_ID)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_ID)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DESC)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DESC)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_DESC)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DESC)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_START)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_START)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_START)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DUR)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DUR)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_DUR)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DUR)->ShowWindow(SW_HIDE);

			GetDlgItem(IDC_STATIC_ID2)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_ID2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_ID2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_ID2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DESC2)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DESC2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_DESC2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DESC2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_START2)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_START2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_START2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_START2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DUR2)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DUR2)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_DUR2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DUR2)->ShowWindow(SW_HIDE);

		}
		else
		{
//AfxMessageBox("HERE1");

			GetDlgItem(IDC_STATIC_START)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_START)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_START)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_DUR)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_DUR)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_EDIT_DUR)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DUR)->ShowWindow(SW_HIDE);




			CSCTE104MultiMessage* mm = NULL;
/*
			if((m_szCmdName.Compare("Current Time")==0)||(m_szCmdName.Compare("Event Notification")==0))
			{
//AfxMessageBox("HERE2");
				pm = u.CreateMultiMessageObject(1);
//AfxMessageBox("HERE3");
				mm = ((CSCTE104MultiMessage*)pm);
				mm->m_scte104Hdr.NumberOfOps = 0x01;
			}
			else
*/
			{
//				pm = u.CreateMultiMessageObject(2, 0);  // Fox has splice request and dtmf descriptor, and time type 0
				pm = u.CreateMultiMessageObject(1, 0);  // Shaw has just splice request
				mm = ((CSCTE104MultiMessage*)pm);
				mm->m_scte104Hdr.NumberOfOps = 0x01;
			}


/*
// removed for Fox, using time type 0
			char buffer[12];
			_timeb timestamp;
			_ftime(&timestamp);
			tm* theTime = localtime( &timestamp.time	);
			strftime( buffer, 12, "%Y-%m-%d", theTime );

		
//					mm->m_scte104Hdr.TimeStamp.time_type = 0x02;
//					mm->m_scte104Hdr.TimeStamp.time_ptr = new unsigned char[4]; // NO, this was already created with CreateMultiMessageObject.
			if(mm->m_scte104Hdr.TimeStamp.time_ptr)  // removed for Fox, using time type 0
			{
				// fill the data with drop frame

				int rem = (timestamp.time- (timestamp.timezone*60)+(timestamp.dstflag?3600:0));
				while(rem<0) rem+=86400;
				while(rem>86399) rem-=86400;
				double ms = (rem%86400)*1000 + timestamp.millitm;
        int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
        int deci = (int)(fr / 17982);
        rem = fr % 17982;
        if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

        *(buffer) = deci / 6; 
        *(buffer+1) = ((deci % 6) * 10) + (rem / 1800); 
        *(buffer+2) = (rem % 1800) / 30; 
        *(buffer+3) = rem % 30;

/* // previously commented out.
				*(buffer) = (timestamp.time%86400)/3600;
				*(buffer+1) = (timestamp.time%3600)/60;
				*(buffer+2) = (timestamp.time%60);
				*(buffer+3) = (unsigned char)(((double)timestamp.millitm)/33.3333333);  //NTSC hardcoded

				if((buffer[2]%10 != 0)&&((buffer[3]==0)||(buffer[3]==1))) buffer[3]=2;  // drop frame approximation.
* /
						
				memcpy(mm->m_scte104Hdr.TimeStamp.time_ptr, buffer, 4);

			}
*/



//AfxMessageBox("HERE4");


			// show relevant fields.
//			GetDlgItem(IDC_STATIC_ID)->EnableWindow(TRUE);
//			GetDlgItem(IDC_STATIC_ID)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_EDIT_ID)->EnableWindow(TRUE);
//			GetDlgItem(IDC_EDIT_ID)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_STATIC_DESC)->EnableWindow(TRUE);
//			GetDlgItem(IDC_STATIC_DESC)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_EDIT_DESC)->EnableWindow(TRUE);
//			GetDlgItem(IDC_EDIT_DESC)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_STATIC_START)->EnableWindow(TRUE);
//			GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_EDIT_START)->EnableWindow(TRUE);
//			GetDlgItem(IDC_EDIT_START)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_STATIC_DUR)->EnableWindow(TRUE);
//			GetDlgItem(IDC_STATIC_DUR)->ShowWindow(SW_SHOW);
//			GetDlgItem(IDC_EDIT_DUR)->EnableWindow(TRUE);
//			GetDlgItem(IDC_EDIT_DUR)->ShowWindow(SW_SHOW);



//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("National Break");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Local 60 second");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Local 120 second");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Break End");



			// everything gets splice_request_data
			mm->m_ppscte104Ops[0]->OpId1High = 	0x01; //		opID [high order byte]						0x0101 splice_request_data
			mm->m_ppscte104Ops[0]->OpId1Low = 0x01;		//		opID [low order byte]
			mm->m_ppscte104Ops[0]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
			mm->m_ppscte104Ops[0]->OpDataLengthLow  = 0x0e;			// xx		data_length [low order byte]			

			mm->m_ppscte104Ops[0]->proprietary_data_ptr = new unsigned char[0x0e];
//AfxMessageBox("HERE 1");

			SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) mm->m_ppscte104Ops[0]->proprietary_data_ptr;
//AfxMessageBox("HERE 2");

			if(mm->m_ppscte104Ops[0]->proprietary_data_ptr)
			{
//AfxMessageBox("HERE 3");
				memset(mm->m_ppscte104Ops[0]->proprietary_data_ptr, 0x00, 0x0e); // nulls

//AfxMessageBox("HERE 4");
//				psrd->auto_return_flag = 1;  // everything gets this
			}

//AfxMessageBox("HERE 5");
/*
			// everything gets insert_DTMF_descriptor_request_data() with 4 DTMF chars
			mm->m_ppscte104Ops[1]->OpId1High = 	0x01; //		opID [high order byte]						0x0109 insert_DTMF_descriptor_request_data
			mm->m_ppscte104Ops[1]->OpId1Low = 0x09;		//		opID [low order byte]
			mm->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
			mm->m_ppscte104Ops[1]->OpDataLengthLow  = 0x06;			// xx		data_length [low order byte]			

			mm->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[0x06];

			if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
			{
				memset(mm->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, 0x06); // nulls

				mm->m_ppscte104Ops[1]->proprietary_data_ptr[0]=0x00;
				mm->m_ppscte104Ops[1]->proprietary_data_ptr[1]=0x04;
			}

*/
			unsigned long ul_splice_event_id=m_ul_splice_event_id;
			if(ul_splice_event_id<0xffffffff) ul_splice_event_id++;
			else ul_splice_event_id = 1; // cycle around.

			unsigned short us_unique_program_id=m_us_unique_program_id;
			if(us_unique_program_id<0xffff) us_unique_program_id++;
			else us_unique_program_id = 1; // cycle around.


/*
splice_request_data() {
splice_insert_type 1 uimsbf
splice_event_id 4 uimsbf
unique_program_id 2 uimsbf
pre_roll_time 2 uimsbf
break_duration 2 uimsbf
avail_num 1 uimsbf
avails_expected 1 uimsbf
auto_return_flag 1 uimsbf
}
*/
//AfxMessageBox((char*)mm->m_ppscte104Ops[0]->proprietary_data_ptr);
/*
					CString szText;
					char* p = NULL;
					
/*
					GetDlgItem(IDC_EDIT_START)->GetWindowText(szText);

					p = szText.GetBuffer(11);
					n = (p[0]-48)*10;
					n += p[1]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[15] = n;

					n = (p[3]-48)*10;
					n += p[4]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[16] = n;

					n = (p[6]-48)*10;
					n += p[7]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[17] = n;

					n = (p[9]-48)*10;
					n += p[10]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[18] = n;
* /
					GetDlgItem(IDC_EDIT_DUR)->GetWindowText(szText);
					p = szText.GetBuffer(11);

					n = (p[0]-48)*10;
					n += p[1]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[19] = n;

					n = (p[3]-48)*10;
					n += p[4]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[20] = n;

					n = (p[6]-48)*10;
					n += p[7]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[21] = n;

					n = (p[9]-48)*10;
					n += p[10]-48;
					mm->m_ppscte104Ops[0]->proprietary_data_ptr[22] = n;


					memset(&mm->m_ppscte104Ops[0]->proprietary_data_ptr[23], 0x20, 48); // spaces

					GetDlgItem(IDC_EDIT_ID)->GetWindowText(szText);
					int nLen = szText.GetLength();
					if(nLen>0)
					{
						if(nLen>16) nLen=16;
						p = szText.GetBuffer(nLen);
//AfxMessageBox(p);
//CString Q; Q.Format("nLen %d", nLen);AfxMessageBox(Q);
						memcpy(&mm->m_ppscte104Ops[0]->proprietary_data_ptr[23], p, nLen);
//Q.Format("char %d %d", mm->m_ppscte104Ops[0]->proprietary_data_ptr[23], *p);AfxMessageBox(Q);
//AfxMessageBox((char*)&mm->m_ppscte104Ops[0]->proprietary_data_ptr[23]);
					}

					GetDlgItem(IDC_EDIT_DESC)->GetWindowText(szText);
					nLen = szText.GetLength();
					if(nLen>0)
					{
						if(nLen>32) nLen=32;
						p = szText.GetBuffer(nLen);
//AfxMessageBox(p);
						memcpy(&mm->m_ppscte104Ops[0]->proprietary_data_ptr[39], p, nLen);
					}
				}

*/			

//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("National Break");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Local 60 second");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Local 120 second");
//			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("Break End");


			if(m_szCmdName.Compare("out_of_network TRUE")==0)
			{
				nType = 1;
				if(m_nLastType != nType)
				{
m_bSupressEditChange = TRUE;
					m_szStart = "0";
					m_szDur = "1850";
					GetDlgItem(IDC_EDIT_START)->SetWindowText(m_szStart);
					GetDlgItem(IDC_EDIT_DUR)->SetWindowText(m_szDur);
m_bSupressEditChange = FALSE;
				}
/*
				GetDlgItem(IDC_STATIC_START)->EnableWindow(TRUE);
				GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_EDIT_START)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_START)->ShowWindow(SW_SHOW);

				GetDlgItem(IDC_STATIC_DUR)->EnableWindow(TRUE);
				GetDlgItem(IDC_STATIC_DUR)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_EDIT_DUR)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_DUR)->ShowWindow(SW_SHOW);

*/

//AfxMessageBox("HERE");
				if(psrd)
				{

// spliceStart_immediate sections may come once at the splice point�s exact location. 
// The Injector shall set the splice_immediate_flag to 1 and the out_of_network_indicator to 1 in the resulting SCTE 35 [1] splice_info_section() section.

					psrd->splice_insert_type = 2; //(spliceStart_immediate)

//					psrd->auto_return_flag = 1;  // only for out_of_network TRUE  // leave this off for now.

/*
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x10;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first
*/

/*

					CString szText;
					int n;
					GetDlgItem(IDC_EDIT_START)->GetWindowText(szText);

					n = atol(szText);
					psrd->pre_roll_time[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->pre_roll_time[1] = (unsigned char)((n&0x000000ff));


					GetDlgItem(IDC_EDIT_DUR)->GetWindowText(szText);
					n = atol(szText);
					psrd->break_duration[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->break_duration[1] = (unsigned char)((n&0x000000ff));
*/
				
				}

/*
				if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
				{
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x31;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x35;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x30;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
				}
*/

			}
			else
			if(m_szCmdName.Compare("out_of_network FALSE")==0)
			{
				nType = 2;

				if(psrd)
				{

					psrd->splice_insert_type = 4; //(spliceEnd_immediate)

// spliceEnd_immediate sections come to terminate a current splice before the splice point, or a splice in process earlier than expected. 
// The Injector sets the out_of_network_indicator to 0 and the splice_immediate_flag to 1. The value of pre_roll_time is ignored.

/*
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x20;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first

					// 8000 = 0x1f40
					psrd->pre_roll_time[0] = (unsigned char)(0x1f);
					psrd->pre_roll_time[1] = (unsigned char)(0x40);

					// 0600 = 0x0258
					psrd->break_duration[0] = (unsigned char)(0x02);
					psrd->break_duration[1] = (unsigned char)(0x58);
*/
				}
/*
				if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
				{
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 0x50; // preroll of 80 tenths = 0x50

					mm->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x37;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x38;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x34;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
				}
*/
			
			}			
			else
			if(m_szCmdName.Compare("Roll Invidi")==0)
			{

			GetDlgItem(IDC_STATIC_START)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_EDIT_START)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_START)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DUR)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_DUR)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_EDIT_DUR)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DUR)->ShowWindow(SW_SHOW);

				nType = 3;
				if(m_nLastType != nType)
				{

m_bSupressEditChange = TRUE;
					m_szStart = "3000";
					m_szDur = "1850";
					GetDlgItem(IDC_EDIT_START)->SetWindowText(m_szStart);
					GetDlgItem(IDC_EDIT_DUR)->SetWindowText(m_szDur);
m_bSupressEditChange = FALSE;
				}
				if(psrd)
				{

					psrd->splice_insert_type = 1; //(spliceStart_normal)

// spliceEnd_immediate sections come to terminate a current splice before the splice point, or a splice in process earlier than expected. 
// The Injector sets the out_of_network_indicator to 0 and the splice_immediate_flag to 1. The value of pre_roll_time is ignored.

/*
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x20;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first
*/
					psrd->unique_program_id[0] = 0;//(unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = 1;//(unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first



					CString szText;
					int n;
					GetDlgItem(IDC_EDIT_START)->GetWindowText(szText);

					n = atol(szText);
					psrd->pre_roll_time[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->pre_roll_time[1] = (unsigned char)((n&0x000000ff));


					GetDlgItem(IDC_EDIT_DUR)->GetWindowText(szText);
					n = atol(szText);
					psrd->break_duration[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->break_duration[1] = (unsigned char)((n&0x000000ff));

/*

					// 8000 = 0x1f40
					psrd->pre_roll_time[0] = (unsigned char)(0x1f);
					psrd->pre_roll_time[1] = (unsigned char)(0x40);

					// 0600 = 0x0258
					psrd->break_duration[0] = (unsigned char)(0x02);
					psrd->break_duration[1] = (unsigned char)(0x58);
*/
				}
/*
				if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
				{
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 0x50; // preroll of 80 tenths = 0x50

					mm->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x37;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x38;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x34;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
				}
*/
			
			}			else
			if(m_szCmdName.Compare("Roll Thunder Bay")==0)
			{
				nType = 4;

			GetDlgItem(IDC_STATIC_START)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_START)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_EDIT_START)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_START)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STATIC_DUR)->EnableWindow(TRUE);
			GetDlgItem(IDC_STATIC_DUR)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_EDIT_DUR)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DUR)->ShowWindow(SW_SHOW);
				if(m_nLastType != nType)
				{
m_bSupressEditChange = TRUE;
					m_szStart = "3000";
					m_szDur = "1850";
					GetDlgItem(IDC_EDIT_START)->SetWindowText(m_szStart);
					GetDlgItem(IDC_EDIT_DUR)->SetWindowText(m_szDur);
m_bSupressEditChange = FALSE;
				}
				if(psrd)
				{

					psrd->splice_insert_type = 1; //(spliceStart_normal)

// spliceEnd_immediate sections come to terminate a current splice before the splice point, or a splice in process earlier than expected. 
// The Injector sets the out_of_network_indicator to 0 and the splice_immediate_flag to 1. The value of pre_roll_time is ignored.

/*
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x20;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first
*/
					psrd->unique_program_id[0] = 0;//(unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = 2;//(unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first

					CString szText;
					int n;
					GetDlgItem(IDC_EDIT_START)->GetWindowText(szText);

					n = atol(szText);
					psrd->pre_roll_time[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->pre_roll_time[1] = (unsigned char)((n&0x000000ff));


					GetDlgItem(IDC_EDIT_DUR)->GetWindowText(szText);
					n = atol(szText);
					psrd->break_duration[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->break_duration[1] = (unsigned char)((n&0x000000ff));

/*

					// 8000 = 0x1f40
					psrd->pre_roll_time[0] = (unsigned char)(0x1f);
					psrd->pre_roll_time[1] = (unsigned char)(0x40);

					// 0600 = 0x0258
					psrd->break_duration[0] = (unsigned char)(0x02);
					psrd->break_duration[1] = (unsigned char)(0x58);
*/

				}
/*
				if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
				{
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 0x50; // preroll of 80 tenths = 0x50

					mm->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x37;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x38;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x34;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
				}
*/
			
			}
/*
			else
			if(m_szCmdName.Compare("Local 120 second")==0)
			{
				nType = 3;
				if(psrd)
				{

					psrd->splice_insert_type = 1; //(spliceStart_normal)

					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x20;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first

					// 8000 = 0x1f40
					psrd->pre_roll_time[0] = (unsigned char)(0x1f);
					psrd->pre_roll_time[1] = (unsigned char)(0x40);

					// 1200 = 0x04b0
					psrd->break_duration[0] = (unsigned char)(0x04);
					psrd->break_duration[1] = (unsigned char)(0xb0);

				}
				if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
				{
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 0x50; // preroll of 80 tenths = 0x50

					mm->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x34;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x36;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x35;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
				}

			}
			else
			if(m_szCmdName.Compare("Break End")==0)
			{
				nType = 4;
				if(psrd)
				{

					psrd->splice_insert_type = 4; //(spliceEnd_immediate)

					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24);    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first

					// zero preroll and break dur

				}
				if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
				{
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x31;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x35;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x30;
					mm->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x23;
				}

			}
*/
			else nType = -1;

			m_nLastType = nType;

			mm->UpdateDataLengths();
		}

		if(pm)
		{

			if(m_pvMessage) delete m_pvMessage;
			m_pvMessage = pm;

			char* pch = NULL;
			unsigned char* pchBuf = NULL;
			if(bSingle)
			{
				pchBuf = u.ReturnMessageBuffer((CSCTE104SingleMessage*)pm);
				if(pchBuf)
				{
//CString Q;
//Q.Format("[%s]", pchBuf);
//AfxMessageBox(Q);

					unsigned long ulLen = *(pchBuf+2);
//Q.Format("buflen %d", ulLen);
//AfxMessageBox(Q);
					ulLen <<= 8; ulLen += *(pchBuf+3);
//Q.Format("buflen %d", ulLen);
//AfxMessageBox(Q);

					unsigned long ulHdr = 13;
					char* pchHdr = bu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM32BIT);
//AfxMessageBox(pchHdr);

					unsigned long ulData = ulLen-13;
					char* pchData = NULL;
					if(ulData>0)
					{
					  pchData = bu.ReadableHex((char*)(pchBuf+13), &ulData, MODE_ALLOWPRINTABLES|MODE_DELIMPRINTABLES|MODE_DELIM32BIT);
//AfxMessageBox(pchData);
					}

					pch = (char*)malloc(ulHdr+ulData+3); //crlf and term 0
					if(pch)
					{
						if(pchHdr) memcpy(pch, pchHdr, ulHdr);
						*(pch+ulHdr) = 13;
						*(pch+ulHdr+1) = 10;
						if(pchData)
						{
							memcpy(pch+ulHdr+2, pchData, ulData);
						}
						*(pch+ulHdr+ulData+2) = 0;
//AfxMessageBox(pch);

					}
					if(pchHdr) free(pchHdr);
					if(pchData) free(pchData);

					pchHdr = NULL;
					pchData = NULL;
				}
			}
			else
			{
				pchBuf = u.ReturnMessageBuffer((CSCTE104MultiMessage*)pm);
				if(pchBuf)
				{
					unsigned long ulLen = *(pchBuf+2);
//Q.Format("buflen %d", ulLen);
//AfxMessageBox(Q);
					ulLen <<= 8; ulLen += *(pchBuf+3);
//Q.Format("buflen %d", ulLen);
//AfxMessageBox(Q);

					unsigned long ulHdr = 11;  // fixed portion
					char* pchHdr = bu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM32BIT);
//AfxMessageBox(pchHdr);

					int nBytePosition = 11;
					CString szText;
					CString szAdd;

//					szText.Format("%s\r\ntimestamp: %02d:%02d:%02d;%02d\r\nnum ops: %d",pchHdr, *(pchBuf+11),*(pchBuf+12),*(pchBuf+13),*(pchBuf+14),*(pchBuf+15) ); // assumed time type = 0x02

					szText.Format("%s\r\ntimestamp: %02d\r\nnum ops: %d",pchHdr, 0,*(pchBuf+nBytePosition)); // assumed time type = 0x00

					unsigned long ulData = 2;
					char* pchData = NULL;
					nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\n\r\nsplice_request_data %s ", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 2; nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("len: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}
//AfxMessageBox(pchData);
					ulData = 1;nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\nsplice_insert_type: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 4;nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM64BIT);
					if(pchData)
					{
						szAdd.Format("\r\nsplice_event_id: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 2;nBytePosition+=4;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\nunique_program_id: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 2;nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\npre_roll_time: %s (= %d ms)", pchData, (pchBuf[nBytePosition]<<8)|pchBuf[nBytePosition+1]);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 2;nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\nbreak_duration: %s (= %d tenths)", pchData, (pchBuf[nBytePosition]<<8)|pchBuf[nBytePosition+1]);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 1;nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\navail_num: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 1;nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("  avail_expected: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 1;nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\nauto_return_flag: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

/*
					ulData = 2;nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\n\r\nDTMF_descriptor %s ", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 2;nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format(" len: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 1;nBytePosition+=2;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("\r\npre-roll: %s (= %d tenths)", pchData, pchBuf[nBytePosition]);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 1;nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM32BIT);
					if(pchData)
					{
						szAdd.Format("  dtmf_length: %s", pchData);
						szText+=szAdd;
						free(pchData);
					}

					ulData = 4;nBytePosition++;
					pchData = bu.ReadableHex((char*)(pchBuf+nBytePosition), &ulData, MODE_DELIM64BIT);
					if(pchData)
					{
						szAdd.Format("\r\nDTMF: %s  %c%c%c%c", pchData, pchBuf[nBytePosition], pchBuf[nBytePosition+1], pchBuf[nBytePosition+2], pchBuf[nBytePosition+3]);
						szText+=szAdd;
						free(pchData);
					}
*/
					
					ulData = szText.GetLength();

					pch = (char*)malloc(ulData+1); // term 0
					if(pch)
					{
						memcpy(pch, szText.GetBuffer(ulData), ulData);
						*(pch+ulData) = 0;
//AfxMessageBox(pch);

					}

					if(pchHdr) free(pchHdr);
//					if(pchData) free(pchData);
					pchHdr = NULL;
				}
			}
//AfxMessageBox("delete buf");
			if(pchBuf) delete pchBuf;
			pchBuf = NULL;
//AfxMessageBox("deleted buf");


			if(pch)
			{
				//now fill the text box
				GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(pch);
//AfxMessageBox("delete other buf");
				free(pch); // used malloc
			}
			else
			{
				GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message buffer!");
			}

		}
		else
		{
			//now fill the text box
			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message object!");
		}

	}
	else
	{
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Not Valid");
	}
	
}

void CSCTE104TestDlg::OnCheckRepeat() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_bRepeatCommand)
	{
		GetDlgItem(IDC_EDIT_REPEAT)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_REPEAT)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_EDIT_REPEAT)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_REPEAT)->EnableWindow(FALSE);
	}
}

void CSCTE104TestDlg::OnCheckLogxact() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
}
