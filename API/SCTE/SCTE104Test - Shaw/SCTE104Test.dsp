# Microsoft Developer Studio Project File - Name="SCTE104Test" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=SCTE104Test - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SCTE104Test.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SCTE104Test.mak" CFG="SCTE104Test - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SCTE104Test - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "SCTE104Test - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SCTE104Test - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "SCTE104Test - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "SCTE104Test - Win32 Release"
# Name "SCTE104Test - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\TXT\BufferUtil.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\..\..\MFC\ODBC\DBUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\TXT\FileUtil.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\..\..\TXT\LogUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\MSG\Messager.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\MSG\MessagingObject.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\LAN\NetUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\SCTE104Test.cpp
# End Source File
# Begin Source File

SOURCE=.\SCTE104Test.rc
# End Source File
# Begin Source File

SOURCE=.\SCTE104TestDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\SCTE104Util.cpp

!IF  "$(CFG)" == "SCTE104Test - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "SCTE104Test - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\TTY\Serial.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\SMTP\smtp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\SMTP\SMTPUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\TXT\BufferUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\MFC\ODBC\DBUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\TXT\FileUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\TXT\LogUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\MSG\Messager.h
# End Source File
# Begin Source File

SOURCE=..\..\..\MSG\MessagingObject.h
# End Source File
# Begin Source File

SOURCE=..\..\..\MSG\msg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\LAN\NetDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\..\LAN\NetUtil.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SCTE104Test.h
# End Source File
# Begin Source File

SOURCE=.\SCTE104TestDlg.h
# End Source File
# Begin Source File

SOURCE=..\SCTE104Util.h
# End Source File
# Begin Source File

SOURCE=..\..\..\TTY\Serial.h
# End Source File
# Begin Source File

SOURCE=..\..\..\SMTP\smtp.h
# End Source File
# Begin Source File

SOURCE=..\..\..\SMTP\SMTPUtil.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\SCTE104Test.ico
# End Source File
# Begin Source File

SOURCE=.\res\SCTE104Test.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
