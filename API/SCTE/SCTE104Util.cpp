// SCTE104Util.cpp: implementation of the CSCTE104Util class and other support classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  //temp if need AfxMessageBox

#include "SCTE104Util.h"
#include <string.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CSCTE104MultiMessage Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSCTE104MultiMessage::CSCTE104MultiMessage()
{
	m_scte104Hdr.Reserved1 = 0xff;							// 0xFF		Reserved											Beginning of multiple_operation_message()
	m_scte104Hdr.Reserved2 = 0xff;							// 0xFF		Reserved											per SCTE 104 Table 7-2
	m_scte104Hdr.MsgSizeHigh = 0xff;						// 0x00		Message Size [high order byte]			calculated byte count
	m_scte104Hdr.MsgSizeLow = 0xff;							// 0xXX		Message Size [low order byte]				calculated byte count < 254
	m_scte104Hdr.ProtocolVersion = 0x00;				// 0x00		Protocol Version
	m_scte104Hdr.AS_Index = 0x00;								// 0x00		AS_index
	m_scte104Hdr.MessageNumber = 0xff;					// 0xyy		message_number									Auto increments on each message
	m_scte104Hdr.DPI_PID_High = 0x00;						// 0xXX		DPI_PID_index [high order byte]
	m_scte104Hdr.DPI_PID_Low = 0x00;						// 0xXX		DPI_PID_index [low order byte]
	m_scte104Hdr.SCTE35_ProtocolVersion = 0x00;	// 0x00		SCTE35_protocol_version

	m_scte104Hdr.TimeStamp.time_type = 0x00;		// 0x00		
	m_scte104Hdr.TimeStamp.time_ptr = NULL;			// 0xXX	0-23
	m_scte104Hdr.NumberOfOps=0;			
	m_ppscte104Ops = NULL;
}

CSCTE104MultiMessage::~CSCTE104MultiMessage()
{
	if(m_ppscte104Ops != NULL)
	{
		unsigned char i=0;
		while(i<m_scte104Hdr.NumberOfOps)
		{
			if(m_ppscte104Ops[i])
			{
				if(m_ppscte104Ops[i]->proprietary_data_ptr)
				{
					try
					{
						delete [] m_ppscte104Ops[i]->proprietary_data_ptr;
					}
					catch(...)
					{
					}
				}
				try
				{
					delete m_ppscte104Ops[i];
				}
				catch(...)
				{
				}
			}
			i++;
		}

		try
		{
			delete [] m_ppscte104Ops[i]->proprietary_data_ptr;
		}
		catch(...)
		{
		}

	}
	if(m_scte104Hdr.TimeStamp.time_ptr) delete m_scte104Hdr.TimeStamp.time_ptr;
}

int CSCTE104MultiMessage::SetTimestamp(SCTE104HMSF_t scte104HMSF)  // does not work on GPI or UTC types.
{
	if(m_scte104Hdr.TimeStamp.time_type != 0x02) return -1;
	if(m_scte104Hdr.TimeStamp.time_ptr == NULL) return -2;

	// could check validity of time values in timestamp...  but maybe want to set it to invalid values on purpose, so leave it for now.

	memcpy(m_scte104Hdr.TimeStamp.time_ptr, &scte104HMSF, 4);
	return 0;
}

int CSCTE104MultiMessage::UpdateDataLengths()
{
	unsigned long ulTotalLen=12; // fixed bytes in header
	switch(m_scte104Hdr.TimeStamp.time_type)
	{
		case 0x01: ulTotalLen+=6; break;
		case 0x02: ulTotalLen+=4; break;
		case 0x03: ulTotalLen+=2; break;
	}
	if(m_ppscte104Ops != NULL)
	{
		unsigned char i=0;
		while(i<m_scte104Hdr.NumberOfOps)
		{
			if(m_ppscte104Ops[i])
			{
				unsigned long buflen = 0x0000ffff&(((m_ppscte104Ops[i]->OpDataLengthHigh&0x00ff)<<8)|(m_ppscte104Ops[i]->OpDataLengthLow&0x00ff));
				ulTotalLen += buflen+4; // 4 is for the 4 bytes of opid and datalen
			}

			i++;
		}
	}

	// now update the message itself:
	m_scte104Hdr.MsgSizeHigh = (unsigned char)((ulTotalLen>>8)&0x00ff);
	m_scte104Hdr.MsgSizeLow  = (unsigned char)(ulTotalLen&0x00ff);

	return ulTotalLen;
}


//////////////////////////////////////////////////////////////////////
// CSCTE104SingleMessage Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSCTE104SingleMessage::CSCTE104SingleMessage()
{
	OpId1High=0x00;							// 81		opID [high order byte]						0x8150 user defined operation
	OpId1Low=0x00;								// 50		opID [low order byte]
	OpMessageLengthHigh=0;		// xx		data_length [high order byte]			
	OpMessageLengthLow=13;			// xx		data_length [low order byte]			
	ResultHigh=0xff;									
	ResultLow=0xff;				
	ResultExtensionHigh=0xff;									
	ResultExtensionLow=0xff;				
	ProtocolVersion=0x00;			// 0x00		Protocol Version
	AS_Index=0x00;						// 0x00		AS_index
	MessageNumber=0;					// 0xyy		message_number									Auto increments on each message
	DPI_PID_High=0;						// 0xXX		DPI_PID_index [high order byte]
	DPI_PID_Low=0;						// 0xXX		DPI_PID_index [low order byte]
	data_ptr = NULL;
}

CSCTE104SingleMessage::~CSCTE104SingleMessage()
{
	if(data_ptr) 
	{
		try
		{
			delete [] data_ptr;
		}
		catch(...)
		{
		}
	}
}



//////////////////////////////////////////////////////////////////////
// CSCTE104Util Construction/Destruction
//////////////////////////////////////////////////////////////////////


CSCTE104Util::CSCTE104Util()
{
	m_ucLastMessageNumber = 0;
	m_ucLastSecondaryMessageNumberInc = 63;
	m_ucSecondaryMessageNumberofValues = 128;  // default value
}

CSCTE104Util::~CSCTE104Util()
{

}

CSCTE104MultiMessage* CSCTE104Util::CreateMultiMessageObject(int nNumOps, int nTimeType, bool bSuppressMessageNum)
{
	if(nNumOps<1) return NULL; // nothing to do here.

	CSCTE104MultiMessage* p = new CSCTE104MultiMessage;
	if(p)
	{
		p->m_scte104Hdr.NumberOfOps = 0;
		if(nNumOps>0)
		{
			p->m_ppscte104Ops = new SCTE104Op_t*[nNumOps];
			if(p->m_ppscte104Ops)
			{
				int i=0;
				while(i<nNumOps)
				{
					p->m_ppscte104Ops[i] = new SCTE104Op_t;
					if(p->m_ppscte104Ops[i]) 
					{
						p->m_ppscte104Ops[i]->proprietary_data_ptr=NULL;
						p->m_ppscte104Ops[i]->OpDataLengthHigh=0;
						p->m_ppscte104Ops[i]->OpDataLengthLow=0;
						p->m_ppscte104Ops[i]->OpId1High=0;
						p->m_ppscte104Ops[i]->OpId1Low=0;
					}
					else
					{
						i--;
						while(i>=0)
						{
							if(p->m_ppscte104Ops[i])
							{
								if(p->m_ppscte104Ops[i]->proprietary_data_ptr) delete [] p->m_ppscte104Ops[i]->proprietary_data_ptr;
								delete p->m_ppscte104Ops[i];
							}
							i--;
						}
						delete [] p->m_ppscte104Ops;
						delete p;
						return NULL;
					}
					i++;
				}
			}
			else
			{
				delete p;
				return NULL;
			}
		}

		if(!bSuppressMessageNum)
		{
//			m_ucLastMessageNumber++;
			p->m_scte104Hdr.MessageNumber = m_ucLastMessageNumber+1; // expected value
//			if(m_ucLastMessageNumber>=255) m_ucLastMessageNumber=0; // loop around.
		}

		p->m_scte104Hdr.NumberOfOps = (unsigned char) nNumOps;
		p->m_scte104Hdr.TimeStamp.time_type = (unsigned char) nTimeType;
/*
if( time_type == 1 ) 
{
  UTC_seconds 4 uimsbf
  UTC_microseconds 2 uimsbf
}
if( time_type == 2 ) 
{
  hours 1 uimsbf
  minutes 1 uimsbf
  seconds 1 uimsbf
  frames 1 uimsbf
}
if( time_type == 3 ) 
{
  GPI_number 1 uimsbf
  GPI_edge 1 uimsbf
}
*/

		switch(nTimeType)
		{
		case 0:
			{
				p->m_scte104Hdr.TimeStamp.time_ptr = NULL;  // just to make sure
			} break;
		case 1:
			{
				p->m_scte104Hdr.TimeStamp.time_ptr = new SCTE104UTC_t;
				if(p->m_scte104Hdr.TimeStamp.time_ptr)
				{
					((SCTE104UTC_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->UTC_seconds=0;
					((SCTE104UTC_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->UTC_microseconds=0;
				}

			} break;
		case 2:
			{
				p->m_scte104Hdr.TimeStamp.time_ptr = new SCTE104HMSF_t;
				if(p->m_scte104Hdr.TimeStamp.time_ptr)
				{
					((SCTE104HMSF_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->hours=0;
					((SCTE104HMSF_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->minutes=0;
					((SCTE104HMSF_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->seconds=0;
					((SCTE104HMSF_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->frames=0;
				}

			} break;
		case 3:
			{
				p->m_scte104Hdr.TimeStamp.time_ptr = new SCTE104GPI_t;
				if(p->m_scte104Hdr.TimeStamp.time_ptr)
				{
					((SCTE104GPI_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->GPI_number=0;
					((SCTE104GPI_t*)(p->m_scte104Hdr.TimeStamp.time_ptr))->GPI_edge=0;
				}

			} break;
		default:
			{
				delete p; p=NULL;  // deconstructor deallocates the ops
			}
			break;
		}
		return p;
	}
	return NULL;
}

CSCTE104MultiMessage* CSCTE104Util::CreateMultiMessageObjectFromBuffer(unsigned char*  pMessageBuffer)
{
	if(pMessageBuffer == NULL) return NULL; // nothing to do here.

	int nTimeType = (int) pMessageBuffer[10];
	int nNumOps = 0;

	if(nTimeType==1) nNumOps = (int) pMessageBuffer[17];
	else if(nTimeType==2) nNumOps = (int) pMessageBuffer[15];
	else if(nTimeType==3) nNumOps = (int) pMessageBuffer[13];

	CSCTE104MultiMessage* p = CreateMultiMessageObject(nNumOps, nTimeType, true);
	if(p)
	{
		//parse:
		memcpy(p, pMessageBuffer, 11);  // first 11 go direct.
		unsigned char* ptr = ((unsigned char*)p)+11+sizeof(void*);

		if(p->m_scte104Hdr.TimeStamp.time_ptr)
		{
			switch(nTimeType)
			{
			case 1: 
				{
					memcpy(p->m_scte104Hdr.TimeStamp.time_ptr, pMessageBuffer+11, 6);  // utc type
				} break;
			case 2: 
				{
					memcpy(p->m_scte104Hdr.TimeStamp.time_ptr, pMessageBuffer+11, 4);  // hmsf type
				} break;
			case 3: 
				{
					memcpy(p->m_scte104Hdr.TimeStamp.time_ptr, pMessageBuffer+11, 2);  // gpi type
				} break;
			}


			int n=0;
			while(n<nNumOps)
			{
				if(p->m_ppscte104Ops[n])
				{
					memcpy(p->m_ppscte104Ops[n], ptr, 4);
					ptr += 4;
					int buflen = 0x0000ffff&(((p->m_ppscte104Ops[n]->OpDataLengthHigh&0x00ff)<<8)|(p->m_ppscte104Ops[n]->OpDataLengthLow&0x00ff));
					p->m_ppscte104Ops[n]->proprietary_data_ptr = new unsigned char [buflen];
					if(p->m_ppscte104Ops[n]->proprietary_data_ptr)
					{
						memcpy(p->m_ppscte104Ops[n]->proprietary_data_ptr, ptr, buflen);
						ptr+=buflen;
					}
				}
				n++;
			}


		}
		else 
		{
			delete p; 
			return NULL;
		}

		return p;
	}
	return NULL;
}


unsigned char CSCTE104Util::IncrementMessageNumber()
{
	if(m_ucLastMessageNumber<255) 
		m_ucLastMessageNumber++;
	else
		m_ucLastMessageNumber=0; // loop around.
	return m_ucLastMessageNumber;
}

unsigned char CSCTE104Util::IncrementSecondaryMessageNumber()
{
	m_ucLastSecondaryMessageNumberInc++;
	if(m_ucLastSecondaryMessageNumberInc>m_ucSecondaryMessageNumberofValues) m_ucLastSecondaryMessageNumberInc=0; // loop around, smaller loop
	// now calc the return value:

	int rv = m_ucLastMessageNumber + m_ucLastSecondaryMessageNumberInc + 128 - (m_ucSecondaryMessageNumberofValues/2); // small loop averaging 180 degree phase on primary message number
	
	while(rv>255)	rv-=256; // loop around.

	return rv;
}

void CSCTE104Util::InitializeMessageNumbers()
{
	m_ucLastMessageNumber = 0;
//	m_ucSecondaryMessageNumberofValues = 128;  // do not reset to default value here
	m_ucLastSecondaryMessageNumberInc = m_ucSecondaryMessageNumberofValues/2 - 1;  // allows non default value to recalc
}



CSCTE104SingleMessage*	CSCTE104Util::CreateSingleMessageObject(bool bSuppressMessageNum)
{
	CSCTE104SingleMessage* p = new CSCTE104SingleMessage;
	if(p)
	{
		if(!bSuppressMessageNum)
		{
//			m_ucLastMessageNumber++;
			p->MessageNumber = m_ucLastMessageNumber+1; // expected value
//			if(m_ucLastMessageNumber>=255) m_ucLastMessageNumber=0; // loop around.
		}
		return p;
	}

	return NULL;
}

CSCTE104SingleMessage*	CSCTE104Util::CreateSingleMessageObjectFromBuffer(unsigned char*  pMessageBuffer)
{
	return NULL;

}


unsigned char* CSCTE104Util::ReturnMessageBuffer(CSCTE104MultiMessage* pMessage)
{
	if(pMessage)
	{
		unsigned long buflen = 0x0000ffff&(((pMessage->m_scte104Hdr.MsgSizeHigh&0x00ff)<<8)|(pMessage->m_scte104Hdr.MsgSizeLow&0x00ff));

//CString Q;
//Q.Format("multi buflen %d from %d %d", buflen, pMessage->m_scte104Hdr.MsgSizeHigh, pMessage->m_scte104Hdr.MsgSizeLow);
//AfxMessageBox(Q);
		unsigned char* buffer = new unsigned char[buflen+1]; // zero terminate for safety
		int n=0;
		if(buffer)
		{
			memcpy(buffer, &pMessage->m_scte104Hdr, 10); // fixed length.
			memcpy(buffer+10, &pMessage->m_scte104Hdr.TimeStamp.time_type, 1); // fixed length.
			n=11;
			switch(pMessage->m_scte104Hdr.TimeStamp.time_type)
			{
			case 0x01:
				{
					if(pMessage->m_scte104Hdr.TimeStamp.time_ptr)
					{
						memcpy(buffer+n, pMessage->m_scte104Hdr.TimeStamp.time_ptr, 6); // 6 bytes UTC
					}
					else
					{// have to put in NULL values
						memset(buffer+n, 0, 6); // 6 bytes UTC
					}
					n+=6;
				} break;
			case 0x02:
				{
					if(pMessage->m_scte104Hdr.TimeStamp.time_ptr)
					{
						memcpy(buffer+n, pMessage->m_scte104Hdr.TimeStamp.time_ptr, 4); // 4 bytes HMSF
					}
					else
					{// have to put in NULL values
						memset(buffer+n, 0, 4); // 4 bytes HMSF
					}
					n+=4;
				} break;
			case 0x03:
				{
					if(pMessage->m_scte104Hdr.TimeStamp.time_ptr)
					{
						memcpy(buffer+n, pMessage->m_scte104Hdr.TimeStamp.time_ptr, 2); // 2 bytes GPI
					}
					else
					{// have to put in NULL values
						memset(buffer+n, 0, 2); // 2 bytes GPI
					}
					n+=2;
				} break;
			}

			buffer[n] = pMessage->m_scte104Hdr.NumberOfOps;
			n++;

			if(pMessage->m_ppscte104Ops)
			{
				unsigned char i=0;
				while (i<pMessage->m_scte104Hdr.NumberOfOps)
				{
					if(pMessage->m_ppscte104Ops[i])
					{
						memcpy(buffer+n, pMessage->m_ppscte104Ops[i], 4);
						buflen = 0x0000ffff&(((pMessage->m_ppscte104Ops[i]->OpDataLengthHigh&0x00ff)<<8)|(pMessage->m_ppscte104Ops[i]->OpDataLengthLow&0x00ff));
//Q.Format("multi op %d buflen %d from %d %d", i, buflen, pMessage->m_ppscte104Ops[i]->OpDataLengthHigh, pMessage->m_ppscte104Ops[i]->OpDataLengthLow);
//AfxMessageBox(Q);
						if(pMessage->m_ppscte104Ops[i]->proprietary_data_ptr) 
						{

//AfxMessageBox((char*)(pMessage->m_ppscte104Ops[i]->proprietary_data_ptr+23));
							memcpy(buffer+n+4, pMessage->m_ppscte104Ops[i]->proprietary_data_ptr, buflen);
						}
						else  // must pad with NULL
						{
							memset(buffer+n, 0, buflen);
						}
						n += buflen+4;
					}
					i++;
				}
			}
			buffer[n]=0;
			return buffer;
		}
	}
	return NULL;
}



unsigned char* CSCTE104Util::ReturnMessageBuffer(CSCTE104SingleMessage* pMessage)
{
	if(pMessage)
	{
//AfxMessageBox("s");
		unsigned long buflen = 0x0000ffff&(((pMessage->OpMessageLengthHigh&0x00ff)<<8)|(pMessage->OpMessageLengthLow&0x00ff));
//CString Q;
//Q.Format("buflen %d from %d %d", buflen, pMessage->OpMessageLengthHigh, pMessage->OpMessageLengthLow);
//AfxMessageBox(Q);

		if(buflen<13) return NULL; //invalid

		unsigned char* buffer = new unsigned char[buflen+1]; // zero terminate for safety
		int n=buflen-13;
		if(buffer)
		{
			memcpy(buffer, &pMessage->OpId1High, 13); // fixed portion
			if(n>0)
			{
				if(pMessage->data_ptr)
				{
					memcpy(buffer+13, pMessage->data_ptr, n); // variable portion
				}
				else // must pad with null
				{
					memset(buffer+n, 0, n);
				}
			}
			buffer[buflen]=0;
//AfxMessageBox((char*)buffer);

			return buffer;
		}
	}
	return NULL;
}



char*		CSCTE104Util::ReturnReadableBuffer(CSCTE104MultiMessage* pMessage)
{
	return NULL;
}


char*		CSCTE104Util::ReturnReadableBuffer(CSCTE104SingleMessage* pMessage)
{
	return NULL;
}
