// SCTE104TestDlg.h : header file
//

#if !defined(AFX_SCTE104TESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
#define AFX_SCTE104TESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//#include "../IS2Core.h"
#include "..\..\..\MFC\ODBC\DBUtil.h"
#include "..\..\..\LAN\NetUtil.h"
#include "..\..\..\TXT\FileUtil.h"
/////////////////////////////////////////////////////////////////////////////
// CSCTE104TestDlg dialog

#define LOGFILENAME "scte104-tx.log"






class CSCTE104TestDlg : public CDialog
{
// Construction
public:
	CSCTE104TestDlg(CWnd* pParent = NULL);	// standard constructor
//	int LoadFields();
	void OnMonitorButtonRecv();

	BOOL m_bMonitorSocket;
	BOOL m_bSocketMonitorStarted;

	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[8];
	CSize m_sizeDlgMin;

	CString m_szQueueTableName;
	CString m_szLibrettoFileLocation;

	CString m_szCmd;
	void* m_pvMessage;

	int m_nClock;
	int m_nLastType;
	SOCKET m_s;
	BOOL m_bInCommand;

	BOOL m_bStates;
	BOOL m_bSupressEditChange;

	int m_nCmdClock;

	unsigned long m_ul_splice_event_id;    // most significant bit first
	unsigned short m_us_unique_program_id;  // most significant bit first


	BOOL m_bForceKeepAlive;
	BOOL m_bInitRequestSent;

// Dialog Data
	//{{AFX_DATA(CSCTE104TestDlg)
	enum { IDD = IDD_SCTE104TEST_DIALOG };
	CListCtrl	m_lcResponses;
	CString	m_szHost;
	CString	m_szPort;
	CString	m_szDSN;
	CString	m_szPw;
	CString	m_szUser;
	int		m_nRecvTimeout;
	int		m_nSendTimeout;
	int		m_nFlavor;
	BOOL	m_bLogTransactions;
	CString	m_szCmdName;
	BOOL	m_bAutoKeepAlive;
	CString	m_szStart;
	CString	m_szID;
	CString	m_szDesc;
	CString	m_szStart2;
	CString	m_szID2;
	CString	m_szDur;
	CString	m_szDur2;
	CString	m_szDesc2;
	BOOL	m_bRepeatCommand;
	int		m_nNumRepeats;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSCTE104TestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL


	CDBUtil m_db;
	CNetUtil m_net;

	void SetupFlavor();

	int TruncateText(CString* pszText, int nLimit=32);
	int FormatTime(CString* pszText);

	int FormatNum(CString* pszText, int nBytes);

	unsigned long IncrementSpliceEventID();
	unsigned short IncrementUniqueProgID();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSCTE104TestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonSend();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnButtonConnect2();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonRecv();
	virtual void OnOK();
	afx_msg void OnRadioEvertz();
	afx_msg void OnRadioIcon();
	afx_msg void OnRadioLyr();
	afx_msg void OnButtonViewlog();
	afx_msg void OnRadioG7();
	afx_msg void OnCloseupComboCommand();
	afx_msg void OnCheckKeepAlive();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnChangeEditDesc();
	afx_msg void OnChangeEditDesc2();
	afx_msg void OnChangeEditDur();
	afx_msg void OnChangeEditDur2();
	afx_msg void OnChangeEditId();
	afx_msg void OnChangeEditId2();
	afx_msg void OnChangeEditStart();
	afx_msg void OnChangeEditStart2();
	afx_msg void OnSelchangeComboCommand();
	afx_msg void OnCheckRepeat();
	afx_msg void OnCheckLogxact();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCTE104TESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
