// SCTE104Util.h: interface for the CSCTE104Util class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCTE104UTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_)
#define AFX_SCTE104UTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef NULL
#define NULL 0
#endif


// single message defs - assigned values as per spec, ANSI SCTE 104 2004, page 24, table 7.3.
#define INIT_REQUEST		0x0001  // init_request_data() Initial Message to Injector on predefined port
#define INIT_RESPONSE		0x0002  // init_response_data() Injector Automation Y Initial Response to Automation on the established connection
#define ALIVE_REQUEST		0x0003  // alive_request_data() Automation Injector Y Sends an alive message to acquire current status.
#define ALIVE_RESPONSE	0x0004  // alive_response_data() Injector Automation Y Response to the alive message indicating current status.

// multiple message defs - assigned values as per spec, ANSI SCTE 104 2004, page 28, table 7.4.
#define USER_REQUEST		0x8150  // Range Reserved (see Table 7-3).

/*
if( time_type == 1 ) 
{
  UTC_seconds 4 uimsbf
  UTC_microseconds 2 uimsbf
}
if( time_type == 2 ) 
{
  hours 1 uimsbf
  minutes 1 uimsbf
  seconds 1 uimsbf
  frames 1 uimsbf
}
if( time_type == 3 ) 
{
  GPI_number 1 uimsbf
  GPI_edge 1 uimsbf
}
*/

typedef struct SCTE104TimeStamp_t
{
	unsigned char  time_type;
	void* time_ptr;
} SCTE104TimeStamp_t;

typedef struct SCTE104UTC_t
{
  unsigned long UTC_seconds;
  unsigned short UTC_microseconds;
} SCTE104UTC_t;

typedef struct SCTE104HMSF_t
{
	unsigned char hours;			// 0xXX	0-23
	unsigned char minutes;		// 0xXX	0-59
	unsigned char seconds;		// 0xXX	0-59
	unsigned char frames;			// 0xXX	0-29
} SCTE104HMSF_t;

typedef struct SCTE104GPI_t
{
	unsigned char GPI_number;
  unsigned char GPI_edge;
} SCTE104GPI_t;


typedef struct SCTE104_splice_request_data_t
{
	unsigned char splice_insert_type;
	unsigned char splice_event_id[4];    // most significant bit first
	unsigned char unique_program_id[2];  // most significant bit first
	unsigned char pre_roll_time[2];			 // most significant bit first
	unsigned char break_duration[2];		 // most significant bit first
	unsigned char avail_num;
	unsigned char avails_expected;
	unsigned char auto_return_flag;
} SCTE104_splice_request_data_t;

/*
typedef struct SCTE104_insert_DTMF_descriptor_request_data_t
{
	unsigned char pre-roll;
	unsigned char dtmf_length;
	unsigned chars ...
} SCTE104_insert_DTMF_descriptor_request_data_t;
*/

typedef struct SCTE104_time_signal_request_data_t
{
	unsigned char pre_roll_time[2];			 // most significant bit first
} SCTE104_time_signal_request_data_t;


// in the following struct, the pointer was removed because it was adding padding bytes, making for incorrect memcppy.
// the ID is variable bytes, so we leave the commented line to remind us of the insertion point.
typedef struct SCTE104_insert_segmentation_descriptor_request_data_t
{
	unsigned char segmentation_event_id[4]; //4 uimsbf // most significant bit first
	unsigned char segmentation_event_cancel_indicator; //1 uimsbf
	unsigned char duration[2]; //2 uimsbf // most significant bit first
	unsigned char segmentation_upid_type; //1 uimsbf
	unsigned char segmentation_upid_length; //1 uimsbf
//	unsigned char* p_segmentation_upid; //varies uimsbf // pointed to a uchar array holding the string
	unsigned char segmentation_type_id; //1 uimsbf
	unsigned char segment_num; //1 uimsbf
	unsigned char segments_expected; //1 uimsbf
	unsigned char duration_extension_frames; //1 uimsbf
	unsigned char delivery_not_restricted_flag; //1 uimsbf
	unsigned char web_delivery_allowed_flag; //1 uimsbf
	unsigned char no_regional_blackout_flag; //1 uimsbf
	unsigned char archive_allowed_flag; //1 uimsbf
	unsigned char device_restrictions; //1 uimsbf
} SCTE104_insert_segmentation_descriptor_request_data_t;



typedef struct SCTE104Header_t
{
	unsigned char Reserved1;							// 0xFF		Reserved											Beginning of multiple_operation_message()
	unsigned char Reserved2;							// 0xFF		Reserved											per SCTE 104 Table 7-2
	unsigned char MsgSizeHigh;						// 0x00		Message Size [high order byte]			calculated byte count
	unsigned char MsgSizeLow;							// 0xXX		Message Size [low order byte]				calculated byte count < 254
	unsigned char ProtocolVersion;				// 0x00		Protocol Version
	unsigned char AS_Index;								// 0x00		AS_index
	unsigned char MessageNumber;					// 0xyy		message_number									Auto increments on each message
	unsigned char DPI_PID_High;						// 0xXX		DPI_PID_index [high order byte]
	unsigned char DPI_PID_Low;						// 0xXX		DPI_PID_index [low order byte]
	unsigned char SCTE35_ProtocolVersion;	// 0x00		SCTE35_protocol_version

	SCTE104TimeStamp_t TimeStamp;

	unsigned char NumberOfOps;						

} SCTE104Header_t;


typedef struct SCTE104Op_t
{
	unsigned char OpId1High;							// 81		opID [high order byte]						0x8150 user defined operation
	unsigned char OpId1Low;								// 50		opID [low order byte]
	unsigned char OpDataLengthHigh;				// xx		data_length [high order byte]			
	unsigned char OpDataLengthLow;				// xx		data_length [low order byte]			

	// the following may be specific to Disney ABC
//	unsigned char proprietary_id[4];			// ....  SMPTE registered ID	
//	unsigned char proprietary_command;		// 0x01		event notification command

	// yes, those were.  so, we are going to just make them part of the buffer below
	unsigned char* proprietary_data_ptr;		
} SCTE104Op_t;


class CSCTE104MultiMessage  
{
public:

	SCTE104Header_t	m_scte104Hdr;  // num ops is in the header
	SCTE104Op_t**		m_ppscte104Ops;

	CSCTE104MultiMessage();
	virtual ~CSCTE104MultiMessage();
	int SetTimestamp(SCTE104HMSF_t scte104HMSF);  // does not work on GPI or UTC types.
	int UpdateDataLengths();
};


/*
single_operation_message() 
{
	opID 2 uimsbf
	messageSize 2 uimsbf
	result 2 uimsbf
	result_extension 2 uimsbf
	protocol_version 1 uimsbf
	AS_index 1 uimsbf
	message_number 1 uimsbf
	DPI_PID_index 2 uimsbf
	data() * Varies
}
*/

class CSCTE104SingleMessage  
{
public:
	unsigned char OpId1High;							// 81		opID [high order byte]						0x8150 user defined operation
	unsigned char OpId1Low;								// 50		opID [low order byte]
	unsigned char OpMessageLengthHigh;		// xx		data_length [high order byte]			
	unsigned char OpMessageLengthLow;			// xx		data_length [low order byte]			
	unsigned char ResultHigh;									
	unsigned char ResultLow;				
	unsigned char ResultExtensionHigh;									
	unsigned char ResultExtensionLow;				
	unsigned char ProtocolVersion;				// 0x00		Protocol Version
	unsigned char AS_Index;								// 0x00		AS_index
	unsigned char MessageNumber;					// 0xyy		message_number									Auto increments on each message
	unsigned char DPI_PID_High;						// 0xXX		DPI_PID_index [high order byte]
	unsigned char DPI_PID_Low;						// 0xXX		DPI_PID_index [low order byte]
	unsigned char* data_ptr;

	CSCTE104SingleMessage();
	virtual ~CSCTE104SingleMessage();

};



class CSCTE104Util  
{
public:
	CSCTE104Util();
	virtual ~CSCTE104Util();

	unsigned char m_ucLastMessageNumber;
	unsigned char m_ucLastSecondaryMessageNumberInc;
	unsigned char m_ucSecondaryMessageNumberofValues;

	CSCTE104MultiMessage*	CreateMultiMessageObject(int nNumOps, int nTimeType = 0x02, bool bSuppressMessageNum=false);
	CSCTE104MultiMessage*	CreateMultiMessageObjectFromBuffer(unsigned char*  pMessageBuffer);
	CSCTE104SingleMessage*	CreateSingleMessageObject(bool bSuppressMessageNum=false);
	CSCTE104SingleMessage*	CreateSingleMessageObjectFromBuffer(unsigned char*  pMessageBuffer);
	unsigned char*		ReturnMessageBuffer(CSCTE104MultiMessage* pMessage);
	unsigned char*		ReturnMessageBuffer(CSCTE104SingleMessage* pMessage);
	char*		ReturnReadableBuffer(CSCTE104MultiMessage* pMessage);
	char*		ReturnReadableBuffer(CSCTE104SingleMessage* pMessage);

	unsigned char IncrementMessageNumber();
	unsigned char IncrementSecondaryMessageNumber();
	void InitializeMessageNumbers();

};

#endif // !defined(AFX_SCTE104UTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_)
