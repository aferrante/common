#if !defined(AFX_SETUPDIALOG_H__4F7E10B4_5334_4CA2_83FA_BEA443F82C1B__INCLUDED_)
#define AFX_SETUPDIALOG_H__4F7E10B4_5334_4CA2_83FA_BEA443F82C1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetupDialog.h : header file
//
#include "VF200TestDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CSetupDialog dialog

class CSetupDialog : public CDialog
{
// Construction
public:
	CSetupDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetupDialog)
	enum { IDD = IDD_SETUP_DIALOG };
	CListCtrl	m_lc;
	int		m_nButtonPressDelay;
	//}}AFX_DATA

	ButtonEvent_t m_buttonevent[16];


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetupDialog)
	afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUPDIALOG_H__4F7E10B4_5334_4CA2_83FA_BEA443F82C1B__INCLUDED_)
