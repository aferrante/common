#if !defined(AFX_BUTTONDIALOG_H__EA9EB43D_097F_42A8_894C_ECAAD520193A__INCLUDED_)
#define AFX_BUTTONDIALOG_H__EA9EB43D_097F_42A8_894C_ECAAD520193A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ButtonDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CButtonDialog dialog

class CButtonDialog : public CDialog
{
// Construction
public:
	CButtonDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CButtonDialog)
	enum { IDD = IDD_BUTTON_DIALOG };
	CString	m_szEventLabel;
	CString	m_szValue;
	int		m_nInvertState;
	int		m_nOverrideIndexValue;
	BOOL	m_bOverrideIndex;
	//}}AFX_DATA

	CString	m_szTitle;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CButtonDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CButtonDialog)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheck1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUTTONDIALOG_H__EA9EB43D_097F_42A8_894C_ECAAD520193A__INCLUDED_)
