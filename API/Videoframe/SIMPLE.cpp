// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.


//#include <sys/timeb.h>
//#include <time.h>
// #include "stdafx.h"// just for Afx


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SIMPLE.h"

//#include "..\..\TXT\BufferUtil.cpp"


CVFSimpleThreadSync::CVFSimpleThreadSync()   // standard constructor
{
	m_nThreadID=-1;
	m_bEnableTransmitGPI=false;
	m_bTransmitGPI=false;
	m_bTransmitSGTX=false;
	m_bTransmitDTMF=false;
	m_nSequence=0; // optional, can do global instead...

	InitializeCriticalSection(&m_critSequence);

}

CVFSimpleThreadSync::~CVFSimpleThreadSync()   // standard destructor
{
	DeleteCriticalSection(&m_critSequence);
}

int CVFSimpleThreadSync::IncrementSequence()
{
	EnterCriticalSection(&m_critSequence);
	m_nSequence++; 
	if(m_nSequence>9999) m_nSequence=1;

	int n = m_nSequence;
	LeaveCriticalSection(&m_critSequence);
	return n;
}



CVFSimpleCmd::CVFSimpleCmd()   // standard constructor
{
	m_pchUnitNumber = NULL;
	m_pchCommand = NULL;
	m_pchProtocol = NULL;
	m_pchProtocolVersion = NULL;
	m_pchMessage = NULL;
	m_uchChecksum[0] = 0;
	m_uchChecksum[1] = 0;
}

CVFSimpleCmd::~CVFSimpleCmd()   // standard destructor
{
	if(m_pchUnitNumber)      free(m_pchUnitNumber); // made char because dont know if there is alpha in there...
	if(m_pchCommand)         free(m_pchCommand); // made char because dont know if there is alpha in there...
	if(m_pchProtocol)        free(m_pchProtocol); // made char because dont know if there is alpha in there...
	if(m_pchProtocolVersion) free(m_pchProtocolVersion); // made char because dont know if there is alpha in there...
	if(m_pchMessage)         free(m_pchMessage); // made char because dont know if there is alpha in there...
}


CVFSimpleUtil::CVFSimpleUtil()   // standard constructor
{
	m_ppThreadSync=NULL;
	m_nNumThreadSync=0;

	m_nSequence = 0;
	strcpy((char*)m_chHexASCII, "0123456789abcdef");

	InitializeCriticalSection(&m_critGlobalSequence);
	InitializeCriticalSection(&m_critThreadSync);
}

CVFSimpleUtil::~CVFSimpleUtil()   // standard destructor
{
	DeleteCriticalSection(&m_critGlobalSequence);
	DeleteCriticalSection(&m_critThreadSync);
}

int CVFSimpleUtil::IncrementSequence()
{
	EnterCriticalSection(&m_critGlobalSequence);
	m_nSequence++; 
	if(m_nSequence>9999) m_nSequence=1;

	int n = m_nSequence;
	LeaveCriticalSection(&m_critGlobalSequence);
	return n;
}

CVFSimpleThreadSync* CVFSimpleUtil::AddThreadSync(int m_nThreadID)
{
	if(m_ppThreadSync)
	{
		int i=0;
		while(i<m_nNumThreadSync)
		{
			if((m_ppThreadSync[i])&&(m_ppThreadSync[i]->m_nThreadID == m_nThreadID)) return 0;// already there.
			i++;
		}
	}

	CVFSimpleThreadSync** ppThreadSync = new CVFSimpleThreadSync*[m_nNumThreadSync+1];
	if(ppThreadSync)
	{
		if(m_ppThreadSync)
		{
			memcpy(ppThreadSync, m_ppThreadSync, m_nNumThreadSync*sizeof(CVFSimpleThreadSync*));
			delete [] m_ppThreadSync;
		}

		m_ppThreadSync = ppThreadSync;

		CVFSimpleThreadSync* p = new CVFSimpleThreadSync;
		if(p)
		{
			p->m_nThreadID = m_nThreadID;
			p->m_psi = NULL;
			m_ppThreadSync[m_nNumThreadSync] = p;
			m_nNumThreadSync++;
			return p;
		}
	}
	return NULL;
}

int CVFSimpleUtil::RemoveThreadSync(int nThreadID)
{
	if(m_ppThreadSync)
	{
		int i=0;
		while(i<m_nNumThreadSync)
		{
			if((m_ppThreadSync[i])&&(m_ppThreadSync[i]->m_nThreadID == nThreadID))
			{
				m_ppThreadSync[i]->m_psi = NULL; // just null it out before deleting
				delete m_ppThreadSync[i];
				m_nNumThreadSync--;

				if(m_nNumThreadSync <= 0)
				{
					delete [] m_ppThreadSync;
					m_ppThreadSync = NULL;
				}
				else
				{
					while(i<m_nNumThreadSync)
					{
						m_ppThreadSync[i] = m_ppThreadSync[i+1];
						i++;
					}
					m_ppThreadSync[i] = NULL;
				}
				return m_nNumThreadSync;
			}
			i++;
		}
	}
	return -1;
}

int CVFSimpleUtil::SetThreadSync(int nCmdType, unsigned char* pucIP)// must pass in exactly 4 u chars if pucIP is not NULL.  Contained is IP address, that can be memcmp'd with sockaddr_in's in_addr.
{
	EnterCriticalSection(&m_critThreadSync);

	if(m_ppThreadSync)
	{
		int i=0;
		while(i<m_nNumThreadSync)
		{
			if(m_ppThreadSync[i])
			{
				bool bSet=true;

/*
if(pucIP)
{ 
	char ch[MAX_PATH];
	sprintf(ch, "Checking IP match DTMF sequence %d.%d.%d.%d on %d connections", pucIP[0],pucIP[1],pucIP[2],pucIP[3], m_nNumThreadSync);// Sleep(500); //(Dispatch message)
	MessageBox(NULL, ch, NULL, MB_OK);
}
*/

				if((pucIP)&&(m_ppThreadSync[i]->m_psi))
				{

					if(memcmp(m_ppThreadSync[i]->m_psi, pucIP, 4)!=0)
					{
						bSet=false;
					}
				}

				if(bSet)
				{
					switch(nCmdType)
					{
					case VF_CMD_DTMF:// 1
						{
							m_ppThreadSync[i]->m_bTransmitDTMF = true;
						} break;
					case VF_CMD_SGTX:// s
						{
							if(m_ppThreadSync[i]->m_bEnableTransmitGPI) m_ppThreadSync[i]->m_bTransmitSGTX = true; // enforce async messages enabled for this as well as GTR
						} break;
					case VF_CMD_GTR://   3
						{
							if(m_ppThreadSync[i]->m_bEnableTransmitGPI) m_ppThreadSync[i]->m_bTransmitGPI = true;
						} break;
					}
				}
			}
			i++;
		}
		LeaveCriticalSection(&m_critThreadSync);
		return m_nNumThreadSync;
	}
	LeaveCriticalSection(&m_critThreadSync);
	return -1;

}


  
int CVFSimpleUtil::ParseCommand(char* pchCommand, CVFSimpleCmd* pCmd)
{
	if(pchCommand==NULL) return VF_BADBUFFER;
	if(pCmd==NULL) return VF_BADCMDPTR;
	int checkSum=0;// = CalcChecksum(pchCommand, nBytes);
	char buffer[1024];
	buffer[0]=0;
	int b=0;
	int x=0;
	// Scan the send buffer up to the EOT; adding to the checksum.
	// First skip over the SOH.
	pchCommand++;
	while( *pchCommand != 0 )
	{
		if ( *( pchCommand + 2 ) == 0x04 ) 
		{

			// We've reached the end of the scan. We first check
			// the incoming checksum for "00".
			if ( ( *pchCommand == '0' ) && ( *( pchCommand + 1 ) == '0' ) )
			{
				pCmd->m_uchChecksum[0]=0;
				pCmd->m_uchChecksum[1]=0;
				return 0;
			}
			else
			{
				// We now have the 8 bit total. Next we convert it to
				// mod 256 and negate it.
				checkSum = ( 0x00FF - (	checkSum & 0x00FF ) ) + 1;
				// Now compare the two hex digits to the newly calculated ones.
				if ( *pchCommand != m_chHexASCII[ ( checkSum >> 4 ) & 0x0F ] )	return VF_BADCHKSUM;
				if ( *( pchCommand + 1 ) != m_chHexASCII[ checkSum & 0x0F ] )		return VF_BADCHKSUM;
			}
			pCmd->m_uchChecksum[0] = ( checkSum >> 4 ) & 0x0F;
			pCmd->m_uchChecksum[1] = checkSum & 0x0F;

			return 0;
		}
		else
		{

//The general form of a message is:
// <SOH><unit number>,<command code>[,[protocol ID],[protocol version number]]
// <STX>[message body]<ETX><checksum char 1><checksum char 2><EOT>
			switch(*pchCommand)
			{
			case 0x03:
				{
					pCmd->m_pchMessage = (char*)malloc(b+1);
					if(pCmd->m_pchMessage) 
					{
						buffer[b] = 0;
						strcpy(pCmd->m_pchMessage, buffer);
						b=0;		
						buffer[b] = 0;
					}
				} break;
			case 0x02: 
			case ',': 
				{
					if(x<4)
					{
						char** ppch=NULL;
						switch(x)
						{
						case 0: ppch = &pCmd->m_pchUnitNumber; break;
						case 1: ppch = &pCmd->m_pchCommand; break;
						case 2: ppch = &pCmd->m_pchProtocol; break;
						case 3: ppch = &pCmd->m_pchProtocolVersion; break;
						}
						x++;

						if(*pchCommand == 0x02) x=4;

						if(ppch)
						{
							*ppch = (char*)malloc(b+1);
							if(*ppch) 
							{
								buffer[b] = 0;
								strcpy(*ppch, buffer);
								b=0;		
								buffer[b] = 0;
							}
						}
						break;
					}
				} 
			default:
				buffer[b] = *pchCommand; b++; break;
			}

			checkSum = checkSum + (unsigned char)*pchCommand++;
		}
	}

	
	return -1;
}

int CVFSimpleUtil::CreateCommand(CVFSimpleCmd* pCmd, char** ppchCommand)
{
	if((ppchCommand)&&(pCmd))
	{
		int b = 6;
		if(pCmd->m_pchUnitNumber) b+= strlen(pCmd->m_pchUnitNumber);
		if(pCmd->m_pchCommand) b+= strlen(pCmd->m_pchCommand)+1;
		if(pCmd->m_pchProtocol) b+= strlen(pCmd->m_pchProtocol)+1;
		if(pCmd->m_pchProtocolVersion) b+= strlen(pCmd->m_pchProtocolVersion)+1;
		if(pCmd->m_pchMessage) b+= strlen(pCmd->m_pchMessage);


		char* pch = (char*)malloc(b+1);
		if(pch) 
		{
			int q=0;
			sprintf(pch, "%c%s,%s%s%s%s%s%c%s%c00%c",
				0x01,
			  (pCmd->m_pchUnitNumber)?pCmd->m_pchUnitNumber:"0",  // default to 0.
			  (pCmd->m_pchCommand)?pCmd->m_pchCommand:"",
				(pCmd->m_pchProtocol)?",":"",
				(pCmd->m_pchProtocol)?pCmd->m_pchProtocol:"",
				(pCmd->m_pchProtocolVersion)?",":"",
				(pCmd->m_pchProtocolVersion)?pCmd->m_pchProtocolVersion:"",
				0x02,
				(pCmd->m_pchMessage)?pCmd->m_pchMessage:"",
				0x03, 
				0x04);

			InsertChecksum(pch, b);

			*ppchCommand = pch;

			return 0;
		}
	}
	return -1;
}

char* CVFSimpleUtil::PrintCommand(CVFSimpleCmd* pCmd)
{
	return NULL;
}

char* CVFSimpleUtil::PrintCommand(char* pchCommand)
{
	if(pchCommand)
	{
		char* pch = (char*)malloc(1024);
		if(pch)
		{
			int i=0;
			int r=0;
			int n=1024;

			while( *(pchCommand+i) != 0 )
			{
				if( r+4 >= n )  
				{
					char* pch0 = (char*)malloc(n+1024);
					if(pch0)
					{
						memcpy(pch0, pch, n);
						n+=1024;
						free(pch);
						pch = pch0;
					}
					else
					{
						free(pch); return NULL;
					}
				}

				if ((*(pchCommand+i) > 31)&&(*(pchCommand+i) <127))
				{
					// just add directly
					pch[r] = (*(pchCommand+i)); r++;
				}
				else
				{
					// encode hex.
					pch[r] = '['; r++;
					pch[r] = (((*(pchCommand+i))>>4)&(0x0f)) + 0x30; if(pch[r]>0x39) pch[r]+= 39; r++;
					pch[r] = ((*(pchCommand+i))&(0x0f)) + 0x30; if(pch[r]>0x39) pch[r]+= 39; r++;
					pch[r] = ']'; r++;
				}
				i++;

			}
			pch[r] = 0;

			return pch;
		}
	}
	return NULL;
}



int CVFSimpleUtil::VerifyChecksum(char* pchCommand, int nBytes)
{
	int checkSum=0;// = CalcChecksum(pchCommand, nBytes);
	int i; // General counter.
	// Scan the send buffer up to the EOT; adding to the checksum.
	// First skip over the SOH.
	pchCommand++;
	for ( i = 0; i < nBytes; i++ ) 
	{
		if ( *( pchCommand + 2 ) == 0x04 ) 
		{
			// We've reached the end of the scan. We first check
			// the incoming checksum for "00".
			if ( ( *pchCommand == '0' ) && ( *( pchCommand + 1 ) == '0' ) ) return 0;

			// We now have the 8 bit total. Next we convert it to
			// mod 256 and negate it.
			checkSum = ( 0x00FF - (	checkSum & 0x00FF ) ) + 1;
			// Now compare the two hex digits to the newly calculated ones.
			if ( *pchCommand != m_chHexASCII[ ( checkSum >> 4 ) & 0x0F ] )	return 1;
			if ( *( pchCommand + 1 ) != m_chHexASCII[ checkSum & 0x0F ] )		return 1;
			return 0 ;
		}
		else checkSum = checkSum + (unsigned char)*pchCommand++;
	}
	return -1;
}

int CVFSimpleUtil::InsertChecksum(char* pchCommand, int nBytes)
{
	int i; // General counter.
	int checkSum=0; // Checksum running total.
	// Scan the send buffer up to the EOT; adding to the checksum.
	// First skip over the SOH.
	pchCommand++;
	for ( i = 0; i < nBytes; i++ ) 
	{
		if ( *( pchCommand + 2 ) == 0x04 ) 
		{
			// We now have the 8 bit total. Next we convert it to
			// mod 256 and negate it.
			checkSum = ( 0x00FF - (	checkSum & 0x00FF ) ) + 1;
			// ... and convert this to two hex digits and insert them.
			*pchCommand++ = m_chHexASCII[ ( checkSum >> 4 ) & 0x0F ];
			*pchCommand = m_chHexASCII[ checkSum & 0x0F ];
			return 0;
		}
		else checkSum = checkSum + (unsigned char)*pchCommand++;
	}
	return -1;
}

int CVFSimpleUtil::CalcChecksum(char* pchCommand, int nBytes)
{
	int i; // General counter.
	int checkSum; // Checksum running total.
	checkSum = 0;
	// Scan the send buffer up to the EOT; adding to the checksum.
	// First skip over the SOH.
	pchCommand++;
	for ( i = 0; i < nBytes; i++ ) 
	{
		if ( *( pchCommand + 2 ) == 0x04 ) 
		{
			// We now have the 8 bit total. Next we convert it to
			// mod 256 and negate it.
			checkSum = ( 0x00FF - (	checkSum & 0x00FF ) ) + 1;
			// ... and convert this to two hex digits and insert them.
//			*pchCommand++ = csHexRecodeTable[ ( checkSum >> 4 ) & 0x0F ];
//			*pchCommand = csHexRecodeTable[ checkSum & 0x0F ];
			return checkSum;
		}
		else checkSum = checkSum + (unsigned char)*pchCommand++;
	}
	return -1;
}


