// (c) 2012-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.

// SIMPLE.h

// Videoframe SIMPLE protocol

#ifndef VF_SIMPLE_INCLUDED
#define VF_SIMPLE_INCLUDED

#include <windows.h>


/*

#ifndef NULL
#define NULL 0
#endif
*/


#define VF_BADCHKSUM -2
#define VF_BADCMDPTR -3
#define VF_BADBUFFER -4


#define VF_CMD_DTMF	 1  // really GSDA, but, GSDA might be used for other reasons as well, so make it specific to DTMF.
#define VF_CMD_SGTX  2
#define VF_CMD_GTR   3

class CVFSimpleThreadSync
{
public:
  CVFSimpleThreadSync();   // standard constructor
  ~CVFSimpleThreadSync();   // standard destructor

	int  m_nThreadID;
	bool m_bEnableTransmitGPI;
	bool m_bTransmitGPI;
	bool m_bTransmitSGTX;
	bool m_bTransmitDTMF;
	int m_nSequence; // optional, can do global instead...

	void* m_psi;							// used for internet sockets - store the IP address and socket info.

	CRITICAL_SECTION m_critSequence;

	int IncrementSequence();

};



// <SOH><unit number>,<command code>[,[protocol ID],[protocol version number]]<STX>[message body]<ETX><checksum char 1><checksum char 2><EOT>
// Note that two ASCII zeroes are always considered a correct checksum. If a nonzero checksum is present then it should be verified and if incorrect the message should be discarded.
class CVFSimpleCmd
{
public:
  CVFSimpleCmd();   // standard constructor
  ~CVFSimpleCmd();   // standard destructor

	char* m_pchUnitNumber; // made char because dont know if there is alpha in there...
	char* m_pchCommand;
	char* m_pchProtocol;
	char* m_pchProtocolVersion;
	char* m_pchMessage;
	unsigned char m_uchChecksum[2];
};


class CVFSimpleUtil 
{
public:
  CVFSimpleUtil();   // standard constructor
  ~CVFSimpleUtil();   // standard destructor
  
	char m_chHexASCII[17]; //= { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

	int m_nSequence; // just a convenient place to store this; optional, can keep track outside.
	CRITICAL_SECTION m_critGlobalSequence;

	CVFSimpleThreadSync** m_ppThreadSync;
	int m_nNumThreadSync;
	CRITICAL_SECTION m_critThreadSync;

public:
	int ParseCommand(char* pchCommand, CVFSimpleCmd* pCmd);
	int CreateCommand(CVFSimpleCmd* pCmd, char** ppchCommand);
	char* PrintCommand(CVFSimpleCmd* pCmd);
	char* PrintCommand(char* pchCommand);

	int VerifyChecksum(char* pchCommand, int nBytes);
	int InsertChecksum(char* pchCommand, int nBytes);
	int CalcChecksum(char* pchCommand, int nBytes);
	int IncrementSequence();

	CVFSimpleThreadSync* AddThreadSync(int m_nThreadID);
	int RemoveThreadSync(int nThreadID);
	int SetThreadSync(int nCmdType, unsigned char* pucIP=NULL); // must pass in exactly 4 chars (actual buffer can be longer) if pchIP is not NULL.  Contained is hex IP address, e.g. 192.168.0.5 = "C0A80005"
};

#endif //VF_SIMPLE_INCLUDED