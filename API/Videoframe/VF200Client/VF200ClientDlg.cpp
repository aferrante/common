// VF200ClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VF200Client.h"
#include "VF200ClientDlg.h"
#include <process.h>
#include "..\SIMPLE.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



void SocketMonitorThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVF200ClientDlg dialog

CVF200ClientDlg::CVF200ClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVF200ClientDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVF200ClientDlg)
	m_szCmd = _T("0,SGTX");
	m_szMsg = _T("1");
	m_szHost = _T("127.0.0.1");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_s=NULL;
	m_net.Initialize();
	m_bMonitorSocket=FALSE;
	m_bSocketMonitorStarted=FALSE;
	m_bInCommand = FALSE;
}

void CVF200ClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVF200ClientDlg)
	DDX_Control(pDX, IDC_LIST1, m_lcResponses);
	DDX_Text(pDX, IDC_EDIT1, m_szCmd);
	DDX_Text(pDX, IDC_EDIT2, m_szMsg);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szHost);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CVF200ClientDlg, CDialog)
	//{{AFX_MSG_MAP(CVF200ClientDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVF200ClientDlg message handlers

BOOL CVF200ClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	CRect rc;
	m_lcResponses.GetWindowRect(&rc);
	m_lcResponses.InsertColumn(0, "-", LVCFMT_LEFT, 16, 0 );
	m_lcResponses.InsertColumn(1, "To Tester", LVCFMT_LEFT, (rc.Width()-40)/2, 1 );
	m_lcResponses.InsertColumn(2, "From Tester", LVCFMT_LEFT,(rc.Width()-40)/2, 2 );

	ListView_SetExtendedListViewStyle(m_lcResponses.m_hWnd, LVS_EX_FULLROWSELECT);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CVF200ClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CVF200ClientDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CVF200ClientDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CVF200ClientDlg::OnButtonConnect() 
{
	CWaitCursor c;
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		m_bMonitorSocket=FALSE;
		while(m_bSocketMonitorStarted) Sleep(10);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
//		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect 5050");
	}
	else
	{
		char szError[1024];
		UpdateData(TRUE);
		if(m_net.OpenConnection(m_szHost.GetBuffer(0), (short)(5050), 
			&m_s, 
			5000,
			5000,
			szError)<NET_SUCCESS)
		{
			m_s = NULL;
			AfxMessageBox(szError);
		}
		else
		{
			hostent* h=NULL;
//			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");


			h = gethostbyname(m_szHost);
			if(h!=NULL)
			{
				memcpy(&m_saiGTPhost.sin_addr, h->h_addr, 4);
/*
				CString foo; foo.Format("Got %d.%d.%d.%d for %s", 
					m_saiGTPhost.sin_addr.s_net, 
					m_saiGTPhost.sin_addr.s_host, 
					m_saiGTPhost.sin_addr.s_lh, 
					m_saiGTPhost.sin_addr.s_impno,
					m_szHost);
				AfxMessageBox(foo);
*/
//			}

				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);

				m_bMonitorSocket=FALSE;
				while(m_bSocketMonitorStarted) Sleep(10);

				m_bMonitorSocket=TRUE;
				_beginthread(SocketMonitorThread, 0, (void*)this);
			}

		}

	}
	
}

void CVF200ClientDlg::OnButtonSend() 
{
	// TODO: Add your control notification handler code here
	CVFSimpleUtil util;

	UpdateData(TRUE);
	char pszStatus[1024];
	char pchResponse[1024];
	unsigned long ulBufLen =0;

	char* pchTemp = (char*)malloc(m_szCmd.GetLength()+ m_szMsg.GetLength()+32);
	if(pchTemp)
	{
		sprintf(pchTemp, "%c%s%c%s%c00%c",
			0x01,m_szCmd,
			0x02,m_szMsg,
			0x03,
			0x04
			);
		util.InsertChecksum(pchTemp, strlen(pchTemp));
		strcpy(pchResponse,pchTemp);
		free(pchTemp);
		ulBufLen = strlen(pchResponse);

//		CString s; s.Format("[%s] %d", pchResponse, ulBufLen);AfxMessageBox(s);
	}


	
	if((ulBufLen>0)&&(m_s))
	{

		m_nClock = clock();

m_bInCommand = TRUE;
		int nReturn = m_net.SendLine((unsigned char*)pchResponse, ulBufLen, m_s, EOLN_NONE, false, 5000, pszStatus);
		if(nReturn>=NET_SUCCESS)
		{
			int nCount = m_lcResponses.GetItemCount();
			m_lcResponses.InsertItem(nCount, "->");
			char* b=util.PrintCommand(pchResponse);
//		CString s; s.Format("[%s] %d", b, strlen(b));AfxMessageBox(s);


//			m_lcResponses.SetItemText(nCount, 1, pchResponse );
			m_lcResponses.SetItemText(nCount, 1, b?b:"(null)" );
			m_lcResponses.EnsureVisible(nCount, FALSE);
			while(nCount>100){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
			if(b) free(b);
		}
		else
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,pszStatus);
/*
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
			m_nCmdClock = -1;
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}

m_bInCommand = FALSE;


	}
}


void SocketMonitorThread(void* pvArgs)
{
	CVF200ClientDlg* p = (CVF200ClientDlg*)pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{
			timeval tv;
			tv.tv_sec = 0; 
			tv.tv_usec = 500;  // timeout value
			int nNumSockets;
			fd_set fds;

			FD_ZERO(&fds);  // Zero this out each time
			FD_SET(p->m_s, &fds);

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
			if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
			{
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
//				AfxMessageBox("f1");
			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(p->m_s, &fds)))
				) 
			{ 
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
//				AfxMessageBox("f2");
			}
			else // there is recv data.
			{ // wait some delay.
//				AfxMessageBox("f3");
				if((clock() > p->m_nClock+200)&&(!p->m_bInCommand))
				{
					p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
					if(clock() > p->m_nClock+200) // five seconds is a huge timeout.
						p->OnMonitorButtonRecv();
				}
			}
			if(p->m_bMonitorSocket) Sleep(100);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}

void CVF200ClientDlg::OnMonitorButtonRecv()
{
	if(m_bInCommand) return;
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
/*
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
		m_net.CloseConnection(m_s);
		m_s = NULL;
		m_nCmdClock = -1;
		AfxMessageBox(attaturk);

		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect 5050");

	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
//		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
m_bInCommand = TRUE;
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);

m_bInCommand = FALSE;

		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d receiving command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
/*
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
			m_nCmdClock = -1;
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect 5050");
		}
		else
		{

			if(ulLen==0) //disconnected
			{
/*
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
				
				m_net.CloseConnection(m_s);
				m_nCmdClock = -1;
				m_s = NULL;
				AfxMessageBox("disconnected");
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect 5050");
			}
			else
			{
				if(puc)
				{

					int nCount = m_lcResponses.GetItemCount();

					CVFSimpleUtil util;
					char* b=util.PrintCommand((char*)puc);

					m_lcResponses.InsertItem(nCount, "<-");
					m_lcResponses.SetItemText(nCount, 2, b?b:"(null)" );
					m_lcResponses.EnsureVisible(nCount, FALSE);

					if(b) free(b);

					while(nCount>100){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
						
					free(puc);
				}
			}
		}

	}
}

