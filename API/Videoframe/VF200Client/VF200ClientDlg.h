// VF200ClientDlg.h : header file
//

#if !defined(AFX_VF200CLIENTDLG_H__4D440AEF_41CE_4275_80BD_5CD3FE9D8175__INCLUDED_)
#define AFX_VF200CLIENTDLG_H__4D440AEF_41CE_4275_80BD_5CD3FE9D8175__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\..\..\LAN\NetUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CVF200ClientDlg dialog

class CVF200ClientDlg : public CDialog
{
// Construction
public:
	CVF200ClientDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CVF200ClientDlg)
	enum { IDD = IDD_VF200CLIENT_DIALOG };
	CListCtrl	m_lcResponses;
	CString	m_szCmd;
	CString	m_szMsg;
	CString	m_szHost;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVF200ClientDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:
	CNetUtil m_net;
	int m_nClock;
	int m_nCmdClock;
	SOCKET m_s;
	BOOL m_bInCommand;

	BOOL m_bMonitorSocket;
	BOOL m_bSocketMonitorStarted;
	sockaddr_in m_saiGTPhost;
void OnMonitorButtonRecv();


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CVF200ClientDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonSend();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VF200CLIENTDLG_H__4D440AEF_41CE_4275_80BD_5CD3FE9D8175__INCLUDED_)
