; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CVF200ClientDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "VF200Client.h"

ClassCount=3
Class1=CVF200ClientApp
Class2=CVF200ClientDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_VF200CLIENT_DIALOG

[CLS:CVF200ClientApp]
Type=0
HeaderFile=VF200Client.h
ImplementationFile=VF200Client.cpp
Filter=N

[CLS:CVF200ClientDlg]
Type=0
HeaderFile=VF200ClientDlg.h
ImplementationFile=VF200ClientDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_EDIT_HOST

[CLS:CAboutDlg]
Type=0
HeaderFile=VF200ClientDlg.h
ImplementationFile=VF200ClientDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_VF200CLIENT_DIALOG]
Type=1
Class=CVF200ClientDlg
ControlCount=11
Control1=IDOK,button,1208025089
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT1,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EDIT2,edit,1350631552
Control6=IDC_STATIC,static,1342308352
Control7=IDC_BUTTON_SEND,button,1476460544
Control8=IDC_LIST1,SysListView32,1350664205
Control9=IDC_BUTTON_CONNECT,button,1342242816
Control10=IDC_BUTTON_RECV,button,1476460544
Control11=IDC_EDIT_HOST,edit,1350631552

