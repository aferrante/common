// VF200Client.h : main header file for the VF200CLIENT application
//

#if !defined(AFX_VF200CLIENT_H__EEB15F62_1A85_4607_8275_0B0E940CCCFB__INCLUDED_)
#define AFX_VF200CLIENT_H__EEB15F62_1A85_4607_8275_0B0E940CCCFB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CVF200ClientApp:
// See VF200Client.cpp for the implementation of this class
//

class CVF200ClientApp : public CWinApp
{
public:
	CVF200ClientApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVF200ClientApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CVF200ClientApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VF200CLIENT_H__EEB15F62_1A85_4607_8275_0B0E940CCCFB__INCLUDED_)
