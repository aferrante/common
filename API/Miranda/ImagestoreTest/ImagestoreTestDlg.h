// ImagestoreTestDlg.h : header file
//

#if !defined(AFX_IMAGESTORETESTDLG_H__A990FBBE_BB25_4E73_8599_9EA5BA49FCDE__INCLUDED_)
#define AFX_IMAGESTORETESTDLG_H__A990FBBE_BB25_4E73_8599_9EA5BA49FCDE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../IS2Comm.h"

/////////////////////////////////////////////////////////////////////////////
// CImagestoreTestDlg dialog

class CImagestoreTestDlg : public CDialog
{
// Construction
public:
	CImagestoreTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CImagestoreTestDlg)
	enum { IDD = IDD_IMAGESTORETEST_DIALOG };
	CListCtrl	m_lc;
	CString	m_szHost;
	CString	m_szGetFileDest;
	CString	m_szGetFileSrc;
	CString	m_szPutFileDest;
	CString	m_szPutFileSrc;
	CString	m_szDelFile;
	CString	m_szDir;
	BOOL	m_bVer2;
	CString	m_szDirectory;
	BOOL	m_bUseIs2Object;
	BOOL	m_b0x01;
	BOOL	m_b0x02;
	BOOL	m_b0x04;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImagestoreTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:
	CIS2Comm m_is2;
	bool m_bGetFile;
	bool m_bPutFile;
	char m_file[512];
	bool m_bValidFilenames;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CImagestoreTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButtonLoad();
	afx_msg void OnButtonHost();
	afx_msg void OnButtonInitwinsock();
	afx_msg void OnButtonGetfile();
	afx_msg void OnButtonPutfile();
	afx_msg void OnButtonAborttransfer();
	afx_msg void OnButtonDelfile();
	afx_msg void OnButtonPing();
	afx_msg void OnButtonGetnameddir();
	afx_msg void OnButtonGetfulldir();
	afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonGetisinfo();
	afx_msg void OnButtonGetdriveinfo();
	afx_msg void OnButtonGetversion();
	afx_msg void OnButtonParsetem();
	afx_msg void OnButtonGetfileinfo();
	afx_msg void OnButtonGetfilestat2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGESTORETESTDLG_H__A990FBBE_BB25_4E73_8599_9EA5BA49FCDE__INCLUDED_)
