//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ImagestoreTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IMAGESTORETEST_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_STATIC_STATUS               1001
#define IDC_BUTTON_LOAD                 1002
#define IDC_EDIT_HOST                   1003
#define IDC_BUTTON_HOST                 1004
#define IDC_BUTTON_INITWINSOCK          1005
#define IDC_BUTTON_GETFILE              1006
#define IDC_EDIT_GETFILE_S              1007
#define IDC_EDIT_GETFILE_D              1008
#define IDC_BUTTON_PUTFILE              1009
#define IDC_EDIT_PUTFILE_D              1010
#define IDC_EDIT_PUTFILE_S              1011
#define IDC_STATIC_GETFILE              1012
#define IDC_STATIC_PUTFILE              1013
#define IDC_BUTTON_ABORTTRANSFER        1014
#define IDC_BUTTON_DELFILE              1015
#define IDC_EDIT_DELFILE                1016
#define IDC_BUTTON_PING                 1017
#define IDC_LIST1                       1018
#define IDC_BUTTON_GETNAMEDDIR          1019
#define IDC_BUTTON_GETFULLDIR           1020
#define IDC_EDIT_DIR                    1021
#define IDC_CHECK_VER                   1022
#define IDC_BUTTON_GETISINFO            1023
#define IDC_BUTTON_GETDRIVEINFO         1024
#define IDC_COMBO_DIR                   1025
#define IDC_BUTTON_GETVERSION           1026
#define IDC_CHECK_USEOBJ                1027
#define IDC_CHECK01                     1028
#define IDC_CHECK02                     1030
#define IDC_CHECK04                     1031
#define IDC_BUTTON_PARSETEM             1032
#define IDC_BUTTON_GETFILEINFO          1033
#define IDC_BUTTON_GETFILESTAT2         1034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1034
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
