; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CImagestoreTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ImagestoreTest.h"

ClassCount=3
Class1=CImagestoreTestApp
Class2=CImagestoreTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_IMAGESTORETEST_DIALOG

[CLS:CImagestoreTestApp]
Type=0
HeaderFile=ImagestoreTest.h
ImplementationFile=ImagestoreTest.cpp
Filter=N

[CLS:CImagestoreTestDlg]
Type=0
HeaderFile=ImagestoreTestDlg.h
ImplementationFile=ImagestoreTestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_BUTTON_GETFILESTAT2

[CLS:CAboutDlg]
Type=0
HeaderFile=ImagestoreTestDlg.h
ImplementationFile=ImagestoreTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_IMAGESTORETEST_DIALOG]
Type=1
Class=CImagestoreTestDlg
ControlCount=43
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC_STATUS,static,1342308352
Control4=IDC_BUTTON_LOAD,button,1342242816
Control5=IDC_EDIT_HOST,edit,1350631552
Control6=IDC_BUTTON_HOST,button,1342242816
Control7=IDC_BUTTON_INITWINSOCK,button,1342242816
Control8=IDC_BUTTON_GETFILE,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT_GETFILE_S,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_EDIT_GETFILE_D,edit,1350631552
Control13=IDC_BUTTON_PUTFILE,button,1342242816
Control14=IDC_STATIC,static,1342308352
Control15=IDC_EDIT_PUTFILE_D,edit,1350631552
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT_PUTFILE_S,edit,1350631552
Control18=IDC_STATIC_GETFILE,static,1342308352
Control19=IDC_STATIC_PUTFILE,static,1342308352
Control20=IDC_BUTTON_ABORTTRANSFER,button,1342242816
Control21=IDC_BUTTON_DELFILE,button,1342242816
Control22=IDC_STATIC,static,1342308352
Control23=IDC_EDIT_DELFILE,edit,1350631552
Control24=IDC_BUTTON_PING,button,1342242816
Control25=IDC_LIST1,SysListView32,1350664205
Control26=IDC_BUTTON_GETNAMEDDIR,button,1342242816
Control27=IDC_BUTTON_GETFULLDIR,button,1342242816
Control28=IDC_EDIT_DIR,edit,1216413824
Control29=IDC_STATIC,static,1342308352
Control30=IDC_CHECK_VER,button,1342242819
Control31=IDC_BUTTON_GETISINFO,button,1342242816
Control32=IDC_BUTTON_GETDRIVEINFO,button,1342242816
Control33=IDC_COMBO_DIR,combobox,1344340034
Control34=IDC_STATIC,static,1342308352
Control35=IDC_BUTTON_GETVERSION,button,1342242816
Control36=IDC_CHECK_USEOBJ,button,1342242819
Control37=IDC_STATIC,static,1342308352
Control38=IDC_CHECK01,button,1342242819
Control39=IDC_CHECK02,button,1342242819
Control40=IDC_CHECK04,button,1342242819
Control41=IDC_BUTTON_PARSETEM,button,1342242816
Control42=IDC_BUTTON_GETFILEINFO,button,1342242816
Control43=IDC_BUTTON_GETFILESTAT2,button,1342242816

