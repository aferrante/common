// ImagestoreTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImagestoreTest.h"
#include "ImagestoreTestDlg.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void FileProgress(void* pArg);
void GetFile(void* pArg);
void PutFile(void* pArg);
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImagestoreTestDlg dialog

CImagestoreTestDlg::CImagestoreTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImagestoreTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImagestoreTestDlg)
	m_szHost = _T("10.3.210.23");
	m_szGetFileDest = _T("c:/v100.oxt");
	m_szGetFileSrc = _T("$VIDEO/V100.OXT");
	m_szPutFileDest = _T("$VIDEO/V100.OXT");
	m_szPutFileSrc = _T("c:/v100.oxt");
	m_szDelFile = _T("$VIDEO/V100.OXT");
	m_szDir = _T("$VIDEO");
	m_bVer2 = FALSE;
	m_szDirectory = _T("$VIDEO");
	m_bUseIs2Object = TRUE;
	m_b0x01 = FALSE;
	m_b0x02 = FALSE;
	m_b0x04 = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bGetFile=false;
	m_bPutFile=false;
	strcpy(m_file, "");
	m_bValidFilenames = false;

}

void CImagestoreTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImagestoreTestDlg)
	DDX_Control(pDX, IDC_LIST1, m_lc);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_GETFILE_D, m_szGetFileDest);
	DDX_Text(pDX, IDC_EDIT_GETFILE_S, m_szGetFileSrc);
	DDX_Text(pDX, IDC_EDIT_PUTFILE_D, m_szPutFileDest);
	DDX_Text(pDX, IDC_EDIT_PUTFILE_S, m_szPutFileSrc);
	DDX_Text(pDX, IDC_EDIT_DELFILE, m_szDelFile);
	DDX_CBString(pDX, IDC_COMBO_DIR, m_szDir);
	DDX_Check(pDX, IDC_CHECK_VER, m_bVer2);
	DDX_Text(pDX, IDC_EDIT_DIR, m_szDirectory);
	DDX_Check(pDX, IDC_CHECK_USEOBJ, m_bUseIs2Object);
	DDX_Check(pDX, IDC_CHECK01, m_b0x01);
	DDX_Check(pDX, IDC_CHECK02, m_b0x02);
	DDX_Check(pDX, IDC_CHECK04, m_b0x04);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CImagestoreTestDlg, CDialog)
	//{{AFX_MSG_MAP(CImagestoreTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_HOST, OnButtonHost)
	ON_BN_CLICKED(IDC_BUTTON_INITWINSOCK, OnButtonInitwinsock)
	ON_BN_CLICKED(IDC_BUTTON_GETFILE, OnButtonGetfile)
	ON_BN_CLICKED(IDC_BUTTON_PUTFILE, OnButtonPutfile)
	ON_BN_CLICKED(IDC_BUTTON_ABORTTRANSFER, OnButtonAborttransfer)
	ON_BN_CLICKED(IDC_BUTTON_DELFILE, OnButtonDelfile)
	ON_BN_CLICKED(IDC_BUTTON_PING, OnButtonPing)
	ON_BN_CLICKED(IDC_BUTTON_GETNAMEDDIR, OnButtonGetnameddir)
	ON_BN_CLICKED(IDC_BUTTON_GETFULLDIR, OnButtonGetfulldir)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
	ON_BN_CLICKED(IDC_BUTTON_GETISINFO, OnButtonGetisinfo)
	ON_BN_CLICKED(IDC_BUTTON_GETDRIVEINFO, OnButtonGetdriveinfo)
	ON_BN_CLICKED(IDC_BUTTON_GETVERSION, OnButtonGetversion)
	ON_BN_CLICKED(IDC_BUTTON_PARSETEM, OnButtonParsetem)
	ON_BN_CLICKED(IDC_BUTTON_GETFILEINFO, OnButtonGetfileinfo)
	ON_BN_CLICKED(IDC_BUTTON_GETFILESTAT2, OnButtonGetfilestat2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImagestoreTestDlg message handlers

BOOL CImagestoreTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	CRect rc;
	m_lc.GetClientRect(&rc);

	m_lc.InsertColumn(0, "filename", LVCFMT_LEFT, rc.Width()/2, 0);
	m_lc.InsertColumn(1, "other info", LVCFMT_LEFT, rc.Width()/2, 1);

	//GetDlgItem(IDC_STATIC_STATUS)->SetWindowText("Loading OXSOX2.DLL...");
	
//	GetDlgItem(IDC_COMBO_DIR)->SetWindowText("$VIDEO");
//	OpenClipboard();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CImagestoreTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CImagestoreTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CImagestoreTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CImagestoreTestDlg::OnOK() 
{
//CDialog::OnOK();
}

void CImagestoreTestDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CImagestoreTestDlg::OnButtonLoad() 
{
	CString szText;
	if(m_is2.m_hinstDLL)
	{
		// unload
		int nReturn = m_is2.OxSoxFree();
		if(m_is2.m_hinstDLL)
			szText.Format("OxSoxFree returned %d", nReturn);
		else
			szText.Format("OxSoxFree returned 0x%07x", nReturn);
	}
	else
	{
		//load
		int nReturn = m_is2.OxSoxLoad();
		if(nReturn<0)
			szText.Format("OxSoxLoad returned %d", nReturn);
		else
			szText.Format("OxSoxLoad returned 0x%07x", nReturn);
	}

	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);

	if(m_is2.m_hinstDLL)
		GetDlgItem(IDC_BUTTON_LOAD)->SetWindowText("Unload DLL");
	else
		GetDlgItem(IDC_BUTTON_LOAD)->SetWindowText("Load DLL");

}

void CImagestoreTestDlg::OnButtonHost() 
{
	UpdateData(TRUE);
	
	CString szText;
	char buffer[500]; sprintf(buffer, "%s", m_szHost);

	int nReturn = m_is2.SetHost(buffer);
	if(nReturn<0)
		szText.Format("SetHost returned %d", nReturn);
	else
		szText.Format("SetHost returned 0x%07x", nReturn);
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
}

void CImagestoreTestDlg::OnButtonInitwinsock() 
{
	CString szText;

	int nReturn;
	
	if(m_is2.m_lpfnInitWinsock)
	{ 
		nReturn = m_is2.m_lpfnInitWinsock();
		if(nReturn<0)
			szText.Format("InitWinsock returned %d", nReturn);
		else
			szText.Format("InitWinsock returned 0x%07x", nReturn);
		m_is2.m_bDLLWinsockInit = true; // InitWinsock always returns true
	}
	else
		szText.Format("NULL function address for InitWinsock");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
}

void CImagestoreTestDlg::OnButtonGetfile() 
{
	CString szText;
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	if((m_bGetFile)||(m_bPutFile))
	{
		szText.Format("File transfer already in progress");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	UpdateData(TRUE);
	
	char gfd[500]; sprintf(gfd, "%s", m_szGetFileDest);
	char gfs[500]; sprintf(gfs, "%s", m_szGetFileSrc);

	if(strlen(gfd)<=0)
	{
		szText.Format("invalid destination path");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if(strlen(gfs)<=0)
	{
		szText.Format("invalid source path");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	if(m_bVer2)
	{
		if(m_is2.m_lpfnGetFile2)  //1 not working
		{ 
			m_bGetFile = true;
			_beginthread(GetFile, NULL, (void*)this);
			szText.Format("Getting file %s -> %s", gfs, gfd);

		}
		else
			szText.Format("NULL function address for GetFile2");
	}
	else
	{
		if(m_is2.m_lpfnGetFile)  
		{ 
			m_bGetFile = true;
			_beginthread(GetFile, NULL, (void*)this);
			szText.Format("Getting file %s -> %s", gfs, gfd);

		}
		else
			szText.Format("NULL function address for GetFile");
	}
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);

	
}

void CImagestoreTestDlg::OnButtonPutfile() 
{
	CString szText;
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	if((m_bGetFile)||(m_bPutFile))
	{
		szText.Format("File transfer already in progress");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	UpdateData(TRUE);
	
	char pfd[500]; sprintf(pfd, "%s", m_szPutFileDest);
	char pfs[500]; sprintf(pfs, "%s", m_szPutFileSrc);

	if(strlen(pfd)<=0)
	{
		szText.Format("invalid destination path");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if(strlen(pfs)<=0)
	{
		szText.Format("invalid source path");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	if(m_bVer2)
	{
		if(m_is2.m_lpfnPutFile2)  //1 not working
		{ 
			m_bPutFile = true;
			_beginthread(PutFile, NULL, (void*)this);
			szText.Format("Putting file %s -> %s", pfs, pfd);

		}
		else
			szText.Format("NULL function address for PutFile2");
	}
	else
	{
		if(m_is2.m_lpfnPutFile) 
		{ 
			m_bPutFile = true;
			_beginthread(PutFile, NULL, (void*)this);
			szText.Format("Putting file %s -> %s", pfs, pfd);

		}
		else
			szText.Format("NULL function address for PutFile");
	}

	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
}

void FileProgress(void* pArg)
{
	CImagestoreTestDlg* pdlg = (CImagestoreTestDlg*) pArg;
	if(pdlg)
	{
		CString szText;
		while(pdlg->m_bGetFile)
		{
			if(pdlg->m_is2.m_lpfnTransferProgress)
			{
				float fReturn = pdlg->m_is2.m_lpfnTransferProgress( );
				if((fReturn>=0.0)&&(fReturn<=100.0))
				{
					szText.Format("%.02f%%", fReturn);
					pdlg->GetDlgItem(IDC_STATIC_GETFILE)->SetWindowText(szText);
				}
				Sleep(50);
			}
			else
			{
				pdlg->GetDlgItem(IDC_STATIC_GETFILE)->SetWindowText("NA");
				break;
			}
		}
		while(pdlg->m_bPutFile)
		{
			if(pdlg->m_is2.m_lpfnTransferProgress)
			{
				float fReturn = pdlg->m_is2.m_lpfnTransferProgress( );
				if((fReturn>=0.0)&&(fReturn<=100.0))
				{
					szText.Format("%.02f%%", fReturn);
					pdlg->GetDlgItem(IDC_STATIC_PUTFILE)->SetWindowText(szText);
				}
				Sleep(50);
			}
			else
			{
				pdlg->GetDlgItem(IDC_STATIC_PUTFILE)->SetWindowText("NA");
				break;
			}
		}
	}
}

void GetFile(void* pArg)
{
	CImagestoreTestDlg* pdlg = (CImagestoreTestDlg*) pArg;
	if(pdlg)
	{
	 	char gfd[500]; sprintf(gfd, "%s", pdlg->m_szGetFileDest);
		char gfs[500]; sprintf(gfs, "%s", pdlg->m_szGetFileSrc);
		_beginthread(FileProgress, NULL, (void*)pdlg);

		BOOL bReturn;
		if(pdlg->m_bVer2)
			bReturn = pdlg->m_is2.m_lpfnGetFile2(pdlg->m_is2.m_pszHost, gfs, gfd );  //1 not working
		else
			bReturn = pdlg->m_is2.m_lpfnGetFile(pdlg->m_is2.m_pszHost, gfs, gfd );

//AfxMessageBox("Got file");
		pdlg->m_bGetFile = false;

		CString szText;
		if(bReturn==FALSE)
		{
			szText.Format("%.02f%%", 0.0);
			pdlg->GetDlgItem(IDC_STATIC_GETFILE)->SetWindowText(szText);
			szText.Format("GetFile%s returned FALSE", pdlg->m_bVer2?"2":"");
		}
		else
		{
			szText.Format("%.02f%%", 100.0);
			pdlg->GetDlgItem(IDC_STATIC_GETFILE)->SetWindowText(szText);
			szText.Format("GetFile%s returned TRUE", pdlg->m_bVer2?"2":"");
		}
		pdlg->GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	}

}

void PutFile(void* pArg)
{
	CImagestoreTestDlg* pdlg = (CImagestoreTestDlg*) pArg;
	if(pdlg)
	{
	 	char pfd[500]; sprintf(pfd, "%s", pdlg->m_szPutFileDest);
		char pfs[500]; sprintf(pfs, "%s", pdlg->m_szPutFileSrc);
		_beginthread(FileProgress, NULL, (void*)pdlg);
		BOOL bReturn;
		if(pdlg->m_bVer2)
			bReturn = pdlg->m_is2.m_lpfnPutFile2(pdlg->m_is2.m_pszHost, pfd, pfs );   //1 not working
		else
			bReturn = pdlg->m_is2.m_lpfnPutFile(pdlg->m_is2.m_pszHost, pfd, pfs );   //1 not working

		pdlg->m_bPutFile = false;

		CString szText;
		if(bReturn==FALSE)
		{
			szText.Format("%.02f%%", 0.0);
			pdlg->GetDlgItem(IDC_STATIC_PUTFILE)->SetWindowText(szText);
			szText.Format("PutFile%s returned FALSE", pdlg->m_bVer2?"2":"");
		}
		else
		{
			szText.Format("%.02f%%", 100.0);
			pdlg->GetDlgItem(IDC_STATIC_PUTFILE)->SetWindowText(szText);
			szText.Format("PutFile%s returned TRUE", pdlg->m_bVer2?"2":"");
		}
		pdlg->GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	}

}


void CImagestoreTestDlg::OnButtonAborttransfer() 
{
	CString szText;

	if((m_bGetFile)||(m_bPutFile))
	{
		if(m_is2.m_lpfnAbortTransfer)
		{ 
			m_is2.m_lpfnAbortTransfer();
			szText.Format("AbortTransfer called");
			m_bPutFile = false;
			m_bGetFile = false;

		}
		else
			szText.Format("NULL function address for AbortTransfer");
	}
	else
		szText.Format("No file transfer in progress.");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
	
}

void CImagestoreTestDlg::OnButtonDelfile() 
{
	CString szText;
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}


	UpdateData(TRUE);
	char df[500]; sprintf(df, "%s", m_szDelFile);

	BOOL bReturn;
	
	if(m_bVer2)
	{
		if(m_is2.m_lpfnDelFile2)
		{ 
			bReturn = m_is2.m_lpfnDelFile2(m_is2.m_pszHost, df);
			if(bReturn==FALSE)
				szText.Format("DelFile2 returned FALSE deleting %s",df);
			else
				szText.Format("DelFile2 returned TRUE deleting %s",df);
		}
		else
			szText.Format("NULL function address for DelFile2");
	}
	else
	{
		if(m_is2.m_lpfnDelFile)
		{ 
			bReturn = m_is2.m_lpfnDelFile(m_is2.m_pszHost, df);
			if(bReturn==FALSE)
				szText.Format("DelFile returned FALSE deleting %s",df);
			else
				szText.Format("DelFile returned TRUE deleting %s",df);
		}
		else
			szText.Format("NULL function address for DelFile");
	}
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
}

void CImagestoreTestDlg::OnButtonPing() 
{
	CString szText;
	szText.Format("Calling Ping...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}


	BOOL bReturn;
	
	if(m_is2.m_lpfnPing)
	{ 
//		AfxMessageBox("X");
		bReturn = m_is2.m_lpfnPing(m_is2.m_pszHost);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("Ping returned FALSE - host NOT found on %s", m_is2.m_pszHost);
		else
			szText.Format("Ping returned TRUE - host found on %s", m_is2.m_pszHost);
	}
	else
		szText.Format("NULL function address for Ping");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
	
}

void CImagestoreTestDlg::OnButtonGetnameddir() 
{
	CString szText;
	szText.Format("Calling GetNamedDir%s...", m_bVer2?"2":"");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);

	
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}


	BOOL bReturn;
	if(m_bVer2)
	{
		if(m_is2.m_lpfnGetNamedDir2)
		{ 
	//		AfxMessageBox("X");
			FILEDIR FileDir;
			int num;
			UpdateData(TRUE);
			char dir[500]; sprintf(dir, "%s", m_szDir);

			bReturn = m_is2.m_lpfnGetNamedDir2(m_is2.m_pszHost, dir, &FileDir, &num);
	//		AfxMessageBox("Y");
			if(bReturn==FALSE)
				szText.Format("GetNamedDir2 returned FALSE");
			else
			{
				m_lc.DeleteAllItems();
				for(int i=0; i<num; i++)
				{
					m_lc.InsertItem(i, FileDir.FileEntry[i].FileName);
				}
				szText.Format("GetNamedDir2 returned TRUE");
				m_bValidFilenames = true;
			}
		}
		else
			szText.Format("NULL function address for GetNamedDir2");
	}
	else
	{
		if(m_is2.m_lpfnGetNamedDir)
		{ 
	//		AfxMessageBox("X");
			FILEDIR FileDir;
			int num;
			UpdateData(TRUE);
			char dir[500]; sprintf(dir, "%s", m_szDir);

			bReturn = m_is2.m_lpfnGetNamedDir(m_is2.m_pszHost, dir, &FileDir, &num);
	//		AfxMessageBox("Y");
			if(bReturn==FALSE)
				szText.Format("GetNamedDir returned FALSE");
			else
			{
				m_lc.DeleteAllItems();
				for(int i=0; i<num; i++)
				{
					m_lc.InsertItem(i, FileDir.FileEntry[i].FileName);
				}
				szText.Format("GetNamedDir returned TRUE");
				m_bValidFilenames = true;
			}
		}
		else
			szText.Format("NULL function address for GetNamedDir");
	}
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
}

void CImagestoreTestDlg::OnButtonGetfulldir() 
{
	CString szText;
	szText.Format("Calling GetFullDir...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	BOOL bReturn;
	
	if(m_is2.m_lpfnGetFullDir)
	{ 
//		AfxMessageBox("X");
		PV3DirEntry_ pFileDir;
		int num;


		UpdateData(TRUE);
		char dir[500]; sprintf(dir, "%s", m_szDir);

		
		bReturn = m_is2.m_lpfnGetFullDir(m_is2.m_pszHost, dir, &pFileDir, &num);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("GetFullDir returned FALSE");
		else
		{
			if(pFileDir!=NULL)
			{
				m_lc.DeleteAllItems();
				for(int i=0; i<num; i++)
				{
					CString szEntry;
					szEntry.Format("%s",
						pFileDir[i].FileName
						);


					m_lc.InsertItem(i, szEntry);
					szEntry.Format("size:%d     attrib:0x%02x    time:%d",
						pFileDir[i].FileSize,
						pFileDir[i].FileAttribs,
						pFileDir[i].FileTimeStamp
						);
					m_lc.SetItemText(i, 1, szEntry);
				}

				m_is2.m_lpfnFreeOxSoxPtr(pFileDir);
				//free(pFileDir);
			}
			szText.Format("GetFullDir returned TRUE");
			m_bValidFilenames = false;

		}
	}
	else
		szText.Format("NULL function address for GetFullDir");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
}

void CImagestoreTestDlg::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{

	UpdateData(TRUE);

//	if(!m_bValidFilenames)
	{
//		AfxMessageBox("Get a list of filenames with GetNamedDir first."); return;
	}

	int nItem = m_lc.GetNextItem(-1, LVNI_SELECTED);
	if(nItem>=0)
	{
		
		if(m_szDir.GetLength())
			sprintf(m_file, "%s/%s", m_szDir, m_lc.GetItemText(nItem, 0));
		else
			sprintf(m_file, "%s", m_lc.GetItemText(nItem, 0));

		HANDLE hData = GlobalAlloc(GPTR, strlen(m_file)+1);
		if(hData)
		{
			void* ptr = GlobalLock(hData);

			memcpy(ptr, m_file, strlen(m_file)+1);
			OpenClipboard();
			EmptyClipboard();
			if(::SetClipboardData( CF_TEXT, hData)==NULL) AfxMessageBox("couldn't copy");
			CloseClipboard();

			GlobalUnlock(hData);
			GetDlgItem(IDC_STATIC_STATUS)->SetWindowText((char*) hData);

			CString szText;
			szText.Format("Calling GetFileInfo...");
			GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
			if(m_is2.m_bDLLWinsockInit == false)
			{
				szText.Format("Winsock not initialized");
				GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
				return;
			}
			if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
			{
				szText.Format("No host specified");
				GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
				return;
			}

			BOOL bReturn;
			
			if(m_is2.m_lpfnGetFileInfo)
			{ 
		//		AfxMessageBox("X");
				FileInfo_ file_info;
				bReturn = m_is2.m_lpfnGetFileInfo(m_is2.m_pszHost,  m_file, &file_info);
		//		AfxMessageBox("Y");
				if(bReturn==FALSE)
					szText.Format("GetFileInfo returned FALSE");
				else
				{
					szText.Format("title: %s\ntype: 0x%02x\nassoc: 0x%02x\nuid: 0x%02x\next_size: %d\next_info: %d\n",
						file_info.Title,
						file_info.File_Type,
						file_info.Associations,
						file_info.UID,
						file_info.Ext_Size,
						file_info.Ext_Info
						);
					


					AfxMessageBox(szText);
					szText.Format("GetFileInfo returned TRUE");
				}
				
				GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
			}
			else
			{
				szText.Format("NULL function address for GetFileInfo");
				GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
			}
		}
	}

	*pResult = 0;
}

void CImagestoreTestDlg::OnButtonGetisinfo() 
{
	CString szText;
	szText.Format("Calling GetISInfo...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	BOOL bReturn;
	
	if(m_is2.m_lpfnGetISInfo)
	{ 
//		AfxMessageBox("X");
		SystemInfo_ sysInfo;
		
		bReturn = m_is2.m_lpfnGetISInfo(m_is2.m_pszHost, &sysInfo);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("GetISInfo returned FALSE");
		else
		{
			szText.Format("GetISInfo returned TRUE, system name: %s, serial#: %d",
				sysInfo.SystemName,
				sysInfo.SerialNumber
				);
		}
	}
	else
		szText.Format("NULL function address for GetISInfo");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
}

void CImagestoreTestDlg::OnButtonGetdriveinfo() 
{
	CString szText;
	szText.Format("Calling GetDriveInfo...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}

	BOOL bReturn;
	
	if(m_is2.m_lpfnGetDriveInfo)
	{ 
//		AfxMessageBox("X");
		UpdateData(TRUE);
		char dir[500]; sprintf(dir, "%s", m_szDir);

		DiskInfo_ diskinfo;

		bReturn = m_is2.m_lpfnGetDriveInfo(m_is2.m_pszHost, dir, &diskinfo);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("GetDriveInfo returned FALSE");
		else
		{
			szText.Format("GetDriveInfo (%s) returned TRUE, %d free, %d total.", dir,
				diskinfo.KBytes_Free,
				diskinfo.KBytes_Total
				);
		}
	}
	else
		szText.Format("NULL function address for GetDriveInfo");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
}

void CImagestoreTestDlg::OnButtonGetversion() 
{
	CString szText;
	szText.Format("Calling GetOxSox2DLLVer...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);

	BOOL bReturn;
	
	if(m_is2.m_lpfnGetOxSox2DLLVer)
	{ 
//		AfxMessageBox("X");
		int nVersion;

		bReturn = m_is2.m_lpfnGetOxSox2DLLVer(&nVersion);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("GetOxSox2DLLVer returned FALSE");
		else
		{
			szText.Format("GetOxSox2DLLVer returned TRUE, version = %d",
				nVersion
				);
		}
	}
	else
		szText.Format("NULL function address for GetOxSox2DLLVer");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
	
}

void CImagestoreTestDlg::OnButtonParsetem() 
{
	// TODO: Add your control notification handler code here
	
}

void CImagestoreTestDlg::OnButtonGetfileinfo() 
{
	CString szText;
	szText.Format("Calling GetFileInfo...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);

	
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}


	BOOL bReturn;
	if(m_is2.m_lpfnGetFileInfo)
	{ 
//		AfxMessageBox("X");
		FileInfo_ file_info;
		UpdateData(TRUE);
		char file[256]; sprintf(file, "%s", m_szDelFile);

		bReturn = m_is2.m_lpfnGetFileInfo(m_is2.m_pszHost, file, &file_info);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("GetFileInfo returned FALSE");
		else
		{
			m_lc.DeleteAllItems();

			m_lc.InsertItem(0, file_info.Title);

					szText.Format("title: %s\ntype: 0x%02x\nassoc: 0x%02x\nuid: 0x%02x\next_size: %d\next_info: %d\n",
						file_info.Title,
						file_info.File_Type,
						file_info.Associations,
						file_info.UID,
						file_info.Ext_Size,
						file_info.Ext_Info
						);
					


					AfxMessageBox(szText);

			szText.Format("GetFileInfo returned TRUE");
			m_bValidFilenames = true;
		}
	}
	else
		szText.Format("NULL function address for GetFileInfo");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
}

void CImagestoreTestDlg::OnButtonGetfilestat2() 
{
	CString szText;
	szText.Format("Calling GetFileStat2...");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);

	
	if(m_is2.m_bDLLWinsockInit == false)
	{
		szText.Format("Winsock not initialized");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}
	if((m_is2.m_pszHost == NULL)||(strlen(m_is2.m_pszHost)<=0))
	{
		szText.Format("No host specified");
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
		return;
	}


	BOOL bReturn;
	if(m_is2.m_lpfnGetFileStat2)
	{ 
//		AfxMessageBox("X");
		UpdateData(TRUE);
		char file[256]; sprintf(file, "%s", m_szDelFile);

		unsigned nSize; unsigned nTimeStamp;
		bReturn = m_is2.m_lpfnGetFileStat2(m_is2.m_pszHost, file, &nSize, &nTimeStamp);
//		AfxMessageBox("Y");
		if(bReturn==FALSE)
			szText.Format("GetFileStat2 returned FALSE");
		else
		{
			m_lc.DeleteAllItems();

			m_lc.InsertItem(0, file);

					szText.Format("file: %s\nsize: %d\ntimestamp: %d\n",
						file,nSize, nTimeStamp
						);
					


					AfxMessageBox(szText);

			szText.Format("GetFileStat2 returned TRUE");
			m_bValidFilenames = true;
		}
	}
	else
		szText.Format("NULL function address for GetFileStat2");
	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);}
