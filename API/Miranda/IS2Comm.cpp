// IS2Comm.cpp: implementation of the CIS2Comm class.
//
//////////////////////////////////////////////////////////////////////


//#include <stdafx.h>
#include "IS2Comm.h"
#include <process.h>
#include <sys/timeb.h>
/*
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/
void TransferFile(void* pArg); // file transfer thread;
void HiveMaintenanceThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIS2Comm::CIS2Comm()
{
	m_pszOxSoxFilename = NULL;
	m_pszHost = NULL;
	m_ulLoadTime = 0;
	
	m_hinstDLL = NULL;
	m_bDLLWinsockInit = false;
	m_bTransferring = false;
	m_nDLLVersion = -1;

	m_nTransferIDCurrent = OX_KILL;
	m_nTransferIDLast = OX_KILL;
	m_bTransferSuppress = false;
	m_bTransferError = false;
	m_pTransferError = NULL;
	m_bTransferAborted = false;
	m_bScheduledForRestart = false;

	m_lpfnOxSox2Entry = NULL;
	m_lpfnGetUnitNames = NULL;
	m_lpfnGetDir = NULL;
	m_lpfnGetNamedDir = NULL;
	m_lpfnGetFile = NULL;
//	m_lpfnCancelGetFile = NULL;
	m_lpfnPutFile = NULL;
	m_lpfnDelFile = NULL;
	m_lpfnPing = NULL;
	m_lpfnChangeDir = NULL;
	m_lpfnPWDir = NULL;
	m_lpfnInitWinsock = NULL;
	m_lpfnReturnPortNumber = NULL;
	m_lpfnIsMemberOfGroup = NULL;
	m_lpfnGetTCPServerVer = NULL;
	m_lpfnGetNamedDir2 = NULL;
	m_lpfnGetFile2 = NULL;
	m_lpfnPutFile2 = NULL;
	m_lpfnDelFile2 = NULL;
	m_lpfnGetOxSox2DLLVer = NULL;
	m_lpfnAbortTransfer = NULL;
	m_lpfnTransferProgress = NULL;
	m_lpfnGetFullDir = NULL;
	m_lpfnGetFileInfo = NULL;
	m_lpfnGetThumbnail = NULL;
	m_lpfnGetISInfo = NULL;
	m_lpfnGetDriveInfo = NULL;
	m_lpfnGetFileStat2 = NULL;
	m_lpfnFreeOxSoxPtr = NULL;

	
	InitializeCriticalSection(&m_critIS2);  // critsec access to hive

}

CIS2Comm::~CIS2Comm()
{
	EnterCriticalSection(&m_critIS2);  // critsec access to hive
	if(m_pszHost) free(m_pszHost);
	m_pszHost=NULL;
	if(m_hinstDLL) FreeLibrary(m_hinstDLL);
	m_hinstDLL=NULL;
	if(m_pszOxSoxFilename) free(m_pszOxSoxFilename);
	m_pszOxSoxFilename=NULL;	
	LeaveCriticalSection(&m_critIS2);  // critsec access to hive

	// wait while threads get killed
	DeleteCriticalSection(&m_critIS2);  // critsec access to hive

}

int CIS2Comm::SetHost(char* pszHost)
{
	if(m_bTransferring) return OX_INPROGRESS;
	if((pszHost)&&(strlen(pszHost)>0))
	{
		if(m_pszHost)
		{
			if(strlen(m_pszHost)>=strlen(pszHost))
			{
				strcpy(m_pszHost, pszHost);
				return OX_SUCCESS;
			}
		}
		if(m_pszHost)free(m_pszHost);
		m_pszHost = (char*) malloc(strlen(pszHost)+1);
		if(m_pszHost)
		{
			strcpy(m_pszHost, pszHost);
			return OX_SUCCESS;
		}
	}
	char pstrError[256];
	_snprintf(pstrError, 255, "Could not set host [%s]", pszHost?pszHost:"(null)");
	Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Comm:SetHost");
	return OX_ERROR;
}

int CIS2Comm::OxSoxLoad(char* pszOxSoxFilename)
{
	if(m_hinstDLL) return OX_ALREADYLOADED;

	if(m_pszOxSoxFilename) free(m_pszOxSoxFilename);
	m_pszOxSoxFilename = NULL;
	if((pszOxSoxFilename)&&(strlen(pszOxSoxFilename)>0))
	{
		m_hinstDLL = LoadLibrary(pszOxSoxFilename);
		m_pszOxSoxFilename = (char*)malloc(strlen(pszOxSoxFilename)+1);
		if(m_pszOxSoxFilename) strcpy(m_pszOxSoxFilename, pszOxSoxFilename);
	}
	else
	{
		m_hinstDLL = LoadLibrary("oxsox2.dll");
		m_pszOxSoxFilename = (char*)malloc(strlen("oxsox2.dll")+1);
		if(m_pszOxSoxFilename) strcpy(m_pszOxSoxFilename, "oxsox2.dll");
	}

	if(m_hinstDLL)
	{// load succeeded, lets get the functions!

		m_bScheduledForRestart = false;
		int nFunctionsMissed = 0;

		m_lpfnOxSox2Entry = (LPFNOxSox2Entry)GetProcAddress(m_hinstDLL, "OxSox2Entry");
		if(m_lpfnOxSox2Entry==NULL) nFunctionsMissed |= OX_FUNC_OxSox2Entry;

		m_lpfnGetUnitNames = (LPFNGetUnitNames)GetProcAddress(m_hinstDLL, "GetUnitNames");
		if(m_lpfnGetUnitNames==NULL) nFunctionsMissed |= OX_FUNC_GetUnitNames;
		
		m_lpfnGetDir = (LPFNGetDir)GetProcAddress(m_hinstDLL, "GetDir");
		if(m_lpfnGetDir==NULL) nFunctionsMissed |= OX_FUNC_GetDir;
		
		m_lpfnGetNamedDir = (LPFNGetNamedDir)GetProcAddress(m_hinstDLL, "GetNamedDir");
		if(m_lpfnGetNamedDir==NULL) nFunctionsMissed |= OX_FUNC_GetNamedDir;
		
		m_lpfnGetFile = (LPFNGetFile)GetProcAddress(m_hinstDLL, "GetFile");
		if(m_lpfnGetFile==NULL) nFunctionsMissed |= OX_FUNC_GetFile;
		
//		m_lpfnCancelGetFile = (LPFNCancelGetFile)GetProcAddress(m_hinstDLL, "CancelGetFile");
//		if(m_lpfnCancelGetFile==NULL) nFunctionsMissed |= 0x0000020;
		
		m_lpfnPutFile = (LPFNPutFile)GetProcAddress(m_hinstDLL, "PutFile");
		if(m_lpfnPutFile==NULL) nFunctionsMissed |= OX_FUNC_PutFile;
		
		m_lpfnDelFile = (LPFNDelFile)GetProcAddress(m_hinstDLL, "DelFile"); 
		if(m_lpfnDelFile==NULL) nFunctionsMissed |= OX_FUNC_DelFile;
		
		m_lpfnPing = (LPFNPing)GetProcAddress(m_hinstDLL, "Ping");
		if(m_lpfnPing==NULL) nFunctionsMissed |= OX_FUNC_Ping;
		
		m_lpfnChangeDir = (LPFNChangeDir)GetProcAddress(m_hinstDLL, "ChangeDir");
		if(m_lpfnChangeDir==NULL) nFunctionsMissed |= OX_FUNC_ChangeDir;
		
		m_lpfnPWDir = (LPFNPWDir)GetProcAddress(m_hinstDLL, "PWDir");
		if(m_lpfnPWDir==NULL) nFunctionsMissed |= OX_FUNC_PWDir;
		
		m_lpfnInitWinsock = (LPFNInitWinsock)GetProcAddress(m_hinstDLL, "InitWinsock");
		if(m_lpfnInitWinsock==NULL) nFunctionsMissed |= OX_FUNC_InitWinsock;
		
		m_lpfnReturnPortNumber = (LPFNReturnPortNumber)GetProcAddress(m_hinstDLL, "ReturnPortNumber");
		if(m_lpfnReturnPortNumber==NULL) nFunctionsMissed |= OX_FUNC_ReturnPortNumber;
		
		m_lpfnIsMemberOfGroup = (LPFNIsMemberOfGroup)GetProcAddress(m_hinstDLL, "IsMemberOfGroup");
		if(m_lpfnIsMemberOfGroup==NULL) nFunctionsMissed |= OX_FUNC_IsMemberOfGroup;
		
		m_lpfnGetTCPServerVer = (LPFNGetTCPServerVer)GetProcAddress(m_hinstDLL, "_GetTCPServerVer");
		if(m_lpfnGetTCPServerVer==NULL) nFunctionsMissed |= OX_FUNC_GetTCPServerVer;
		
		m_lpfnGetNamedDir2 = (LPFNGetNamedDir2)GetProcAddress(m_hinstDLL, "GetNamedDir2");
		if(m_lpfnGetNamedDir2==NULL) nFunctionsMissed |= OX_FUNC_GetNamedDir2;
		
		m_lpfnGetFile2 = (LPFNGetFile2)GetProcAddress(m_hinstDLL, "GetFile2");
		if(m_lpfnGetFile2==NULL) nFunctionsMissed |= OX_FUNC_GetFile2;
		
		m_lpfnPutFile2 = (LPFNPutFile2)GetProcAddress(m_hinstDLL, "PutFile2");
		if(m_lpfnPutFile2==NULL) nFunctionsMissed |= OX_FUNC_PutFile2;
		
		m_lpfnDelFile2 = (LPFNDelFile2)GetProcAddress(m_hinstDLL, "DelFile2");
		if(m_lpfnDelFile2==NULL) nFunctionsMissed |= OX_FUNC_DelFile2;
		
		m_lpfnGetOxSox2DLLVer = (LPFNGetOxSox2DLLVer)GetProcAddress(m_hinstDLL, "GetOxSox2DLLVer");
		if(m_lpfnGetOxSox2DLLVer==NULL) nFunctionsMissed |= OX_FUNC_GetOxSox2DLLVer;
		
		m_lpfnAbortTransfer = (LPFNAbortTransfer)GetProcAddress(m_hinstDLL, "AbortTransfer");
		if(m_lpfnAbortTransfer==NULL) nFunctionsMissed |= OX_FUNC_AbortTransfer;
		
		m_lpfnTransferProgress = (LPFNTransferProgress)GetProcAddress(m_hinstDLL, "TransferProgress");
		if(m_lpfnTransferProgress==NULL) nFunctionsMissed |= OX_FUNC_TransferProgress;
		
		m_lpfnGetFullDir = (LPFNGetFullDir)GetProcAddress(m_hinstDLL, "GetFullDir");
		if(m_lpfnGetFullDir==NULL) nFunctionsMissed |= OX_FUNC_GetFullDir;
		
		m_lpfnGetFileInfo = (LPFNGetFileInfo)GetProcAddress(m_hinstDLL, "GetFileInfo");
		if(m_lpfnGetFileInfo==NULL) nFunctionsMissed |= OX_FUNC_GetFileInfo;
		
		m_lpfnGetThumbnail = (LPFNGetThumbnail)GetProcAddress(m_hinstDLL, "GetThumbnail");
		if(m_lpfnGetThumbnail==NULL) nFunctionsMissed |= OX_FUNC_GetThumbnail;

		m_lpfnGetISInfo = (LPFNGetISInfo)GetProcAddress(m_hinstDLL, "GetISInfo");
		if(m_lpfnGetISInfo==NULL) nFunctionsMissed |= OX_FUNC_GetISInfo;
		
		m_lpfnGetDriveInfo = (LPFNGetDriveInfo)GetProcAddress(m_hinstDLL, "GetDriveInfo");
		if(m_lpfnGetDriveInfo==NULL) nFunctionsMissed |= OX_FUNC_GetDriveInfo;

		m_lpfnGetFileStat2 = (LPFNGetFileStat2)GetProcAddress(m_hinstDLL, "GetFileStat2");
		if(m_lpfnGetFileStat2==NULL) nFunctionsMissed |= OX_FUNC_GetFileStat2;

		m_lpfnFreeOxSoxPtr = (LPFNFreeOxSoxPtr)GetProcAddress(m_hinstDLL, "FreeOxSoxPtr");
		if(m_lpfnFreeOxSoxPtr==NULL) nFunctionsMissed |= OX_FUNC_FreeOxSoxPtr;


		if(nFunctionsMissed!=0)
		{
			int nReport=OX_FUNC_OxSox2Entry;
			while(nReport<=OX_FUNC_MAX)
			{
				switch(nReport&nFunctionsMissed) // only do the ones we missed!
				{
				case OX_FUNC_OxSox2Entry://					0x0000001
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for OxSox2Entry.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetUnitNames://					0x0000002
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetUnitNames.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetDir://								0x0000004
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetDir.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetNamedDir://						0x0000008
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetNamedDir.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetFile://								0x0000010
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetFile.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_PutFile://								0x0000020
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for PutFile.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_DelFile://								0x0000040
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for DelFile.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_Ping://									0x0000080
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for Ping.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_ChangeDir://							0x0000100
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for ChangeDir.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_PWDir://									0x0000200
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for PWDir.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_InitWinsock://						0x0000400
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for InitWinsock.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_ReturnPortNumber://			0x0000800
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for ReturnPortNumber.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_IsMemberOfGroup://				0x0001000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for IsMemberOfGroup.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetTCPServerVer://				0x0002000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for _GetTCPServerVer.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetNamedDir2://					0x0004000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetNamedDir2.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetFile2://							0x0008000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetFile2.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_PutFile2://							0x0010000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for PutFile2.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_DelFile2://							0x0020000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for DelFile2.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetOxSox2DLLVer://				0x0040000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetOxSox2DLLVer.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_AbortTransfer://					0x0080000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for AbortTransfer.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_TransferProgress://			0x0100000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for TransferProgress.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetFullDir://						0x0200000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetFullDir.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetFileInfo://						0x0400000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetFileInfo.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetThumbnail://					0x0800000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetThumbnail.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetISInfo://							0x1000000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetISInfo.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetDriveInfo://					0x2000000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetDriveInfo.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_GetFileStat2://				0x4000000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for GetFileStat2.", "CIS2Comm:OxSoxLoad"); break;
				case OX_FUNC_FreeOxSoxPtr://				0x8000000
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Null process address for FreeOxSoxPtr.", "CIS2Comm:OxSoxLoad"); break;
				}

				nReport<<=1;
			}
		}
		else
		{
			_timeb now;
			_ftime(&now);
			
			m_ulLoadTime = now.time;
		}

		char pstrError[256];
		_snprintf(pstrError, 255, "Loaded library [%s].", pszOxSoxFilename?pszOxSoxFilename:"oxsox2.dll");
		Message(MSG_ICONINFO, pstrError, "CIS2Comm:OxSoxLoad");

		return nFunctionsMissed;
	}
	else
	{ // load failed!
		char pstrError[256];
		_snprintf(pstrError, 255, "Could not load library [%s].", pszOxSoxFilename?pszOxSoxFilename:"oxsox2.dll");
		Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxLoad");

		return OX_ERROR;
	}
}


int CIS2Comm::OxSoxFree()
{
	if(m_bTransferring) return OX_INPROGRESS;
	if(m_hinstDLL) FreeLibrary(m_hinstDLL);

	m_hinstDLL = NULL;
	m_bDLLWinsockInit = false;
	m_nDLLVersion = -1;

	// don't do the following here, we need the filename to reload and to delete the file
//	if(m_pszOxSoxFilename) free(m_pszOxSoxFilename);
//	m_pszOxSoxFilename=NULL;	

	m_nTransferIDCurrent = OX_KILL;
	m_nTransferIDLast = OX_KILL;
	m_bTransferSuppress = false;

	m_lpfnOxSox2Entry = NULL;
	m_lpfnGetUnitNames = NULL;
	m_lpfnGetDir = NULL;
	m_lpfnGetNamedDir = NULL;
	m_lpfnGetFile = NULL;
//	m_lpfnCancelGetFile = NULL;
	m_lpfnPutFile = NULL;
	m_lpfnDelFile = NULL;
	m_lpfnPing = NULL;
	m_lpfnChangeDir = NULL;
	m_lpfnPWDir = NULL;
	m_lpfnInitWinsock = NULL;
	m_lpfnReturnPortNumber = NULL;
	m_lpfnIsMemberOfGroup = NULL;
	m_lpfnGetTCPServerVer = NULL;
	m_lpfnGetNamedDir2 = NULL;
	m_lpfnGetFile2 = NULL;
	m_lpfnPutFile2 = NULL;
	m_lpfnDelFile2 = NULL;
	m_lpfnGetOxSox2DLLVer = NULL;
	m_lpfnAbortTransfer = NULL;
	m_lpfnTransferProgress = NULL;
	m_lpfnGetFullDir = NULL;
	m_lpfnGetFileInfo = NULL;
	m_lpfnGetThumbnail = NULL;
	m_lpfnGetISInfo = NULL;
	m_lpfnGetDriveInfo = NULL;
	m_lpfnGetFileStat2 = NULL;
	m_lpfnFreeOxSoxPtr = NULL;
	
	Message(MSG_ICONINFO, "Freed oxsox library.", "CIS2Comm:OxSoxFree");

	return OX_SUCCESS;

}


// global oxsox
int CIS2Comm::OxSoxInit()
{
	int nReturn = OX_SUCCESS;

	if(m_hinstDLL == NULL)
	{
		return OX_NODLL;
	}

	if(m_nDLLVersion<0)
	{
		if(m_lpfnGetOxSox2DLLVer)
		{ 
			try
			{
				if(m_lpfnGetOxSox2DLLVer(&m_nDLLVersion))
				{
					char pstrError[256];
					_snprintf(pstrError, 255, "GetOxSox2DLLVer returned %d.", m_nDLLVersion);
					Message(MSG_ICONINFO, pstrError, "CIS2Comm:OxSoxInit");
				}
				else  // should never get here
				{
					Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetOxSox2DLLVer returned FALSE.", "CIS2Comm:OxSoxInit");
					nReturn |= OX_NOVER;
					m_nDLLVersion = -1;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetOxSox2DLLVer returned FALSE.", "CIS2Comm:OxSoxInit");
					nReturn |= OX_NOVER;
					m_nDLLVersion = -1;
			}
		}
		else
		{
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetOxSox2DLLVer has a NULL function address, cannot complete.", "CIS2Comm:OxSoxInit");
			nReturn |= OX_NOVER;
			m_nDLLVersion = -1;
		}
	}

	if(m_bDLLWinsockInit)
	{
		nReturn |= OX_ALREADYINIT;
	}
	else
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxInit");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxInit");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxInit");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxInit");
			return OX_ERROR;
		}
	}
	
	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxInit");
		return OX_NULLHOST;
	}

	if(m_lpfnPing)
	{ 
		try
		{
			if(m_lpfnPing(m_pszHost))
			{
				Message(MSG_ICONINFO, "Ping returned TRUE.", "CIS2Comm:OxSoxInit");
			}
			else  // should never get here
			{
				char pstrError[256];
				_snprintf(pstrError, 255, "Ping returned FALSE on host [%s].", m_pszHost);  //m_pszHost cant be null here
				Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxInit");
				nReturn |= OX_NOPING;
			}
		}
		catch(...)
		{
				Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "Ping exception.", "CIS2Comm:OxSoxInit");
				nReturn |= OX_NOPING;
		}
	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "Ping has a NULL function address, cannot complete.", "CIS2Comm:OxSoxInit");
		nReturn |= OX_NOPING;
	}
	
	return nReturn;
}

int CIS2Comm::OxSoxPing()
{

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxPing");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxPing");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxPing");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxPing");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxPing");
		return OX_NULLHOST;
	}

	if(m_lpfnPing)
	{ 
		char pstrError[256];
		try
		{
			if(m_lpfnPing(m_pszHost))
			{
				_snprintf(pstrError, 255, "Ping returned TRUE for host %s.", m_pszHost);
				Message(MSG_ICONINFO, pstrError, "CIS2Comm:OxSoxPing");
				return OX_SUCCESS;
			}
			else
			{
				_snprintf(pstrError, 255, "Ping returned FALSE on host [%s].", m_pszHost);  //m_pszHost cant be null here
				Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxPing");
				return OX_NOPING;
			}
		}
		catch(...)
		{
			_snprintf(pstrError, 255, "Ping exception for host %s.", m_pszHost);
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxPing");
			return OX_NOPING|OX_EXCEPTION;
		}
	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "Ping has a NULL function address, cannot complete.", "CIS2Comm:OxSoxPing");
		return OX_NOPING;
	}

}

//file level oxsox
int CIS2Comm::OxSoxPutFile(char* pszSourcePath, char* pszDestFile, char* pszDirAlias, unsigned long ulFlags)
{
	if((pszSourcePath==NULL)||(strlen(pszSourcePath)<=0))	return OX_BADPARAM;
	if((pszDestFile==NULL)||(strlen(pszDestFile)<=0))	return OX_BADPARAM;
	if((m_bTransferring)&&(!(ulFlags&OX_QUEUE))) return OX_INPROGRESS;  // dont put if something is in progress already

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxPutFile");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxPutFile");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxPutFile");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxPutFile");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxPutFile");
		return OX_NULLHOST;
	}

	// assemble filenames and other info

	char psrc[_MAX_PATH];
//	char* psrc  = (char*)malloc(_MAX_PATH);
//	if(psrc == NULL) return OX_NOALLOC;
	char pdest[IS2_MAX_PATHNAME];
//	char* pdest = (char*)malloc(IS2_MAX_PATHNAME);
//	if(pdest == NULL)
//	{ 
//		free(psrc);
//		return OX_NOALLOC;
//	}

	// source
	strncpy(psrc, pszSourcePath, _MAX_PATH);

	// destination
	//if theres a dir alias, then use it
	if((pszDirAlias)&&(strlen(pszDirAlias)>0)&&(*pszDirAlias == '$')) //must be valid
	{
		if(strcmp(pszDirAlias, "$VIDEO")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$AUDIO")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$THUMBS")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$FONTS")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PLAYLIST")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$ROOT")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$CODE")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PARAM")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$LAS")==0) strcpy(pdest, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PANEL")==0) strcpy(pdest, pszDirAlias);
		else strcpy(pdest,"");

		// now, dest is either blank or has an alias
		if(strlen(pdest)==0)  //blank
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszDestFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszDestFile+nLen-3, "tem")==0) strcpy(pdest, "$VIDEO");  // intuition template
				else
				if(_stricmp(pszDestFile+nLen-3, "oxt")==0) strcpy(pdest, "$VIDEO");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxa")==0) strcpy(pdest, "$VIDEO");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxi")==0) strcpy(pdest, "$VIDEO");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxe")==0) strcpy(pdest, "$AUDIO");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxw")==0) strcpy(pdest, "$AUDIO");
				else
				if(_stricmp(pszDestFile+nLen-3, "wav")==0) strcpy(pdest, "$AUDIO");
				else
				if(_stricmp(pszDestFile+nLen-3, "ttf")==0) strcpy(pdest, "$FONTS");
				else
				{
//					free(psrc);
//					free(pdest);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
//				free(pdest);
				return OX_BADPARAM;
			}
		}

		strcat(pdest, "/");
		
		// override alias if already in filename
		if(
			  (strncmp(pszDestFile, "$VIDEO", 6)==0)
			||(strncmp(pszDestFile, "$AUDIO", 6)==0)
			||(strncmp(pszDestFile, "$THUMBS", 7)==0)
			||(strncmp(pszDestFile, "$FONTS", 6)==0)
			||(strncmp(pszDestFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszDestFile, "$ROOT", 5)==0)
			||(strncmp(pszDestFile, "$CODE", 5)==0)
			||(strncmp(pszDestFile, "$PARAM", 6)==0)
			||(strncmp(pszDestFile, "$LAS", 4)==0)
			||(strncmp(pszDestFile, "$PANEL", 6)==0)
			)
		{
			strncpy(pdest, pszDestFile, IS2_MAX_PATHNAME);
		}
		else
		{
			strcat(pdest, pszDestFile);
		}
	}
	else  // must discard use of alias
	{
		// override alias if already in filename
		if(
			  (strncmp(pszDestFile, "$VIDEO", 6)==0)
			||(strncmp(pszDestFile, "$AUDIO", 6)==0)
			||(strncmp(pszDestFile, "$THUMBS", 7)==0)
			||(strncmp(pszDestFile, "$FONTS", 6)==0)
			||(strncmp(pszDestFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszDestFile, "$ROOT", 5)==0)
			||(strncmp(pszDestFile, "$CODE", 5)==0)
			||(strncmp(pszDestFile, "$PARAM", 6)==0)
			||(strncmp(pszDestFile, "$LAS", 4)==0)
			||(strncmp(pszDestFile, "$PANEL", 6)==0)
			)
		{
			strncpy(pdest, pszDestFile, IS2_MAX_PATHNAME);
		}
		else
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszDestFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszDestFile+nLen-3, "tem")==0) strcpy(pdest, "$VIDEO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxt")==0) strcpy(pdest, "$VIDEO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxa")==0) strcpy(pdest, "$VIDEO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxi")==0) strcpy(pdest, "$VIDEO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxe")==0) strcpy(pdest, "$AUDIO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "oxw")==0) strcpy(pdest, "$AUDIO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "wav")==0) strcpy(pdest, "$AUDIO/");
				else
				if(_stricmp(pszDestFile+nLen-3, "ttf")==0) strcpy(pdest, "$FONTS/");
				else
				{
//					free(psrc);
//					free(pdest);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
//				free(pdest);
				return OX_BADPARAM;
			}
			strcat(pdest, pszDestFile);
		}
	}


	if(m_lpfnPutFile2) 
	{ 
		while(m_bTransferSuppress) Sleep(5); // dont modify queue while we are mod'ing it elsewhere
		if(m_nTransferIDCurrent<0) m_nTransferIDCurrent=0; //start from reset
		m_nTransferIDLast++;

		ThreadInfo_t* pInfo = new ThreadInfo_t;
		if(pInfo == NULL) return OX_NOALLOC;
		pInfo->pObj = this;
		pInfo->pszTransferSource = (char*)malloc(strlen(psrc)+1);
		if(pInfo->pszTransferSource) strcpy(pInfo->pszTransferSource, psrc);
		pInfo->pszTransferDest = (char*)malloc(strlen(pdest)+1);
		if(pInfo->pszTransferDest) strcpy(pInfo->pszTransferDest, pdest);
		pInfo->ulFlags = ulFlags;
		pInfo->nTransferDir = OX_PUT;
		pInfo->nTransferID = m_nTransferIDLast;
		char pstrError[512];
		_snprintf(pstrError, 511, "Queueing PutFile transfer [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, m_pszHost);
		Message(MSG_ICONINFO, pstrError, "CIS2Comm:OxSoxPutFile");

		if(_beginthread(TransferFile, NULL, (void*)pInfo)<0)
		{
			// error
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "Could not start transfer thread.", "CIS2Comm:OxSoxPutFile");
//			free(psrc);
//			free(pdest);
			if(pInfo->pszTransferSource) free(pInfo->pszTransferSource);
			if(pInfo->pszTransferDest) free(pInfo->pszTransferDest);
			delete pInfo;
			return OX_NOTHREAD;
		}

	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "PutFile2 has a NULL function address, cannot complete.", "CIS2Comm:OxSoxPutFile");
//		free(psrc);
//		free(pdest);
//		delete pInfo;
		return OX_ERROR;
	}
	
	return OX_SUCCESS;
}

int CIS2Comm::OxSoxGetFile(char* pszLocalPath, char* pszSourceFile, char* pszDirAlias, unsigned long ulFlags)
{
	if((pszLocalPath==NULL)||(strlen(pszLocalPath)<=0))	return OX_BADPARAM;
	if((pszSourceFile==NULL)||(strlen(pszSourceFile)<=0))	return OX_BADPARAM;
	if((m_bTransferring)&&(!(ulFlags&OX_QUEUE))) return OX_INPROGRESS;  // dont put if something is in progress already

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxGetFile");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxGetFile");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxGetFile");
					return OX_ERROR;
			}
			
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFile");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxGetFile");
		return OX_NULLHOST;
	}

	// assemble filenames and other info

	char psrc[IS2_MAX_PATHNAME];
//	char* psrc  = (char*)malloc(IS2_MAX_PATHNAME);
//	if(psrc == NULL) return OX_NOALLOC;
	char pdest[_MAX_PATH];
//	char* pdest = (char*)malloc(_MAX_PATH);
//	if(pdest == NULL)
//	{ 
//		free(psrc);
//		return OX_NOALLOC;
//	}

	// destination
	strncpy(pdest, pszLocalPath, _MAX_PATH);

	// source
	//if theres a dir alias, then use it
	if((pszDirAlias)&&(strlen(pszDirAlias)>0)&&(*pszDirAlias == '$')) //must be valid
	{
		if(strcmp(pszDirAlias, "$VIDEO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$AUDIO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$THUMBS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$FONTS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PLAYLIST")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$ROOT")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$CODE")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PARAM")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$LAS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PANEL")==0) strcpy(psrc, pszDirAlias);
		else strcpy(psrc,"");

		// now, dest is either blank or has an alias
		if(strlen(psrc)==0)  //blank
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS");
				else
				{
//					free(psrc);
//					free(pdest);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
//				free(pdest);
				return OX_BADPARAM;
			}
		}

		strcat(psrc, "/");
		
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			strcat(psrc, pszSourceFile);
		}
	}
	else  // must discard use of alias
	{
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS/");
				else
				{
//					free(psrc);
//					free(pdest);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
//				free(pdest);
				return OX_BADPARAM;
			}
			strcat(psrc, pszSourceFile);
		}
	}


	if(m_lpfnGetFile2) 
	{ 
		while(m_bTransferSuppress) Sleep(5); // dont modify queue while we are mod'ing it elsewhere
		if(m_nTransferIDCurrent<0) m_nTransferIDCurrent=0; //start from reset
		m_nTransferIDLast++;

		
		ThreadInfo_t* pInfo = new ThreadInfo_t;
		if(pInfo == NULL) return OX_NOALLOC;
		pInfo->pObj = this;
		pInfo->pszTransferSource = (char*)malloc(strlen(psrc)+1);
		if(pInfo->pszTransferSource) strcpy(pInfo->pszTransferSource, psrc);
		pInfo->pszTransferDest = (char*)malloc(strlen(pdest)+1);
		if(pInfo->pszTransferDest) strcpy(pInfo->pszTransferDest, pdest);
		pInfo->ulFlags = ulFlags;
		pInfo->nTransferDir = OX_GET;
		pInfo->nTransferID = m_nTransferIDLast;
		char pstrError[512];
		_snprintf(pstrError, 511, "Queueing GetFile transfer [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, m_pszHost);
		Message(MSG_ICONINFO, pstrError, "CIS2Comm:OxSoxGetFile");

		if(_beginthread(TransferFile, NULL, (void*)pInfo)<0)
		{
			// error
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "Could not start transfer thread.", "CIS2Comm:OxSoxGetFile");
//			free(psrc);
//			free(pdest);
			if(pInfo->pszTransferSource) free(pInfo->pszTransferSource);
			if(pInfo->pszTransferDest) free(pInfo->pszTransferDest);
			delete pInfo;
			return OX_NOTHREAD;
		}
	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetFile2 has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFile");
		return OX_ERROR;
	}
	
	return OX_SUCCESS;
}

int CIS2Comm::OxSoxDelFile(char* pszSourceFile, char* pszDirAlias, unsigned long ulFlags)
{
	if((pszSourceFile==NULL)||(strlen(pszSourceFile)<=0))	return OX_BADPARAM;
	if(m_bTransferring) return OX_INPROGRESS;  // dont put if something is in progress already

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxDelFile");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxDelFile");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxDelFile");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxDelFile");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxDelFile");
		return OX_NULLHOST;
	}

	// assemble filename

	char psrc[IS2_MAX_PATHNAME];
//	char* psrc  = (char*)malloc(IS2_MAX_PATHNAME);
//	if(psrc == NULL) return OX_NOALLOC;

	// source to delete
	//if theres a dir alias, then use it
	if((pszDirAlias)&&(strlen(pszDirAlias)>0)&&(*pszDirAlias == '$')) //must be valid
	{
		if(strcmp(pszDirAlias, "$VIDEO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$AUDIO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$THUMBS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$FONTS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PLAYLIST")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$ROOT")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$CODE")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PARAM")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$LAS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PANEL")==0) strcpy(psrc, pszDirAlias);
		else strcpy(psrc,"");

		// now, dest is either blank or has an alias
		if(strlen(psrc)==0)  //blank
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
		}

		strcat(psrc, "/");
		
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			strcat(psrc, pszSourceFile);
		}
	}
	else  // must discard use of alias
	{
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS/");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
			strcat(psrc, pszSourceFile);
		}
	}

	if(m_lpfnDelFile2) 
	{ 
		int nReturn = OX_SUCCESS;
		BOOL bReturn = FALSE;
		try
		{
			bReturn = m_lpfnDelFile2(m_pszHost, psrc); 
		}
		catch(...)
		{
			bReturn = FALSE;
			char pstrError[512];
			_snprintf(pstrError, 511, "Exception in DelFile2: code %d", GetLastError());
			Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxDelFile");
			nReturn |= OX_EXCEPTION;
		}
		char pstrError[512];
		if(bReturn)
		{
			_snprintf(pstrError, 511, "Deleted [%s] on host [%s].", psrc, m_pszHost);
			Message(MSG_ICONINFO, pstrError, "CIS2Comm:OxSoxDelFile");
		}
		else
		{
			_snprintf(pstrError, 511, "Error deleting [%s] on host [%s].", psrc, m_pszHost);
			Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxDelFile");
			if(nReturn) 
				return nReturn;
			else 
				return OX_ERROR;
		}

//		free(psrc);
		return nReturn;
	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "DelFile2 has a NULL function address, cannot complete.", "CIS2Comm:OxSoxDelFile");
//		free(psrc);
		return OX_ERROR;
	}
	
}

// this returns true on Imagestore (but not intuition) even if the file is not there.  so only use on a file you know is there
// can use GetFileStats for check existence on both.
int CIS2Comm::OxSoxGetFileInfo(char* pszSourceFile, char* pszDirAlias, FileInfo_* pfile_info, V3DirEntry_* pv3DirEntry, unsigned long ulFlags)
{
	if((pszSourceFile==NULL)||(strlen(pszSourceFile)<=0))	return OX_BADPARAM;
	if((ulFlags&OX_FILEINFO)&&(pfile_info==NULL)) 	return OX_BADPARAM;
	if((ulFlags&OX_DIRENTRYINFO)&&(pv3DirEntry==NULL)) 	return OX_BADPARAM;
//	if(m_bTransferring) return OX_INPROGRESS;  // dont do if something is in progress already

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxGetFileInfo");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxGetFileInfo");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxGetFileInfo");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFileInfo");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxGetFileInfo");
		return OX_NULLHOST;
	}

	// assemble filename
	char psrc[IS2_MAX_PATHNAME];
//	char* psrc  = (char*)malloc(IS2_MAX_PATHNAME);
//	if(psrc == NULL) return OX_NOALLOC;

	// source to delete
	//if theres a dir alias, then use it
	if((pszDirAlias)&&(strlen(pszDirAlias)>0)&&(*pszDirAlias == '$')) //must be valid
	{
		if(strcmp(pszDirAlias, "$VIDEO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$AUDIO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$THUMBS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$FONTS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PLAYLIST")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$ROOT")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$CODE")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PARAM")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$LAS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PANEL")==0) strcpy(psrc, pszDirAlias);
		else strcpy(psrc,"");

		// now, dest is either blank or has an alias
		if(strlen(psrc)==0)  //blank
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
		}

		strcat(psrc, "/");
		
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			strcat(psrc, pszSourceFile);
		}
	}
	else  // must discard use of alias
	{
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS/");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
			strcat(psrc, pszSourceFile);
		}
	}

	int nReturn = OX_SUCCESS;
	if(ulFlags&OX_FILEINFO)
	{
		strcpy(pfile_info->Title, "not found");
		pfile_info->File_Type = -1;
		pfile_info->UID = -1;
		if(m_lpfnGetFileInfo) 
		{ 
			BOOL bReturn = FALSE;
			try
			{
				bReturn = m_lpfnGetFileInfo(m_pszHost, psrc, pfile_info); 
			}
			catch (...)
			{
				bReturn = FALSE;
				char pstrError[512];
				_snprintf(pstrError, 511, "Exception in GetFileInfo: code %d", GetLastError());
				Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileInfo");
				nReturn |= OX_EXCEPTION;
			}

			if(!bReturn)
			{
				strcpy(pfile_info->Title, "error");
				char pstrError[512];
				_snprintf(pstrError, 511, "Error obtaining file info for [%s] on host [%s].", psrc, m_pszHost);
				Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileInfo");
				nReturn |= OX_NOFILE;
			}
		}
		else
		{
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetFileInfo has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFileInfo");
			nReturn |= OX_NOFILE;
		}
	}

	if(ulFlags&OX_DIRENTRYINFO)
	{
		strcpy(pv3DirEntry->FileName, "not found");
		pv3DirEntry->FileSize = 0;
		pv3DirEntry->FileAttribs = 0;
		pv3DirEntry->FileTimeStamp = 0;

		char* pdir = NULL;
		char* pfile = NULL;
		char* pch = strchr(psrc, '/');
		if(pch!=NULL)
		{
			int nLen = pch-psrc;
			pdir = (char*)malloc(nLen+1);
			if(pdir)
			{
				pfile = pch+1; //filename, no dir alias
				strncpy(pdir, psrc, nLen);
				memset(pdir+nLen, 0, 1); //term 0;
			}
			else
			{
//				free(psrc);
				return OX_NOALLOC;
			}
		}
		if(m_lpfnGetFullDir) 
		{ 
			PV3DirEntry_ pv3Dir=NULL;
			int nEntries=0;
			BOOL bReturn = FALSE;
			try
			{
				bReturn = m_lpfnGetFullDir(m_pszHost, pdir, &pv3Dir, &nEntries); 
			}
			catch(...)
			{
				bReturn = FALSE;
				char pstrError[512];
				_snprintf(pstrError, 511, "Exception in GetFullDir: code %d", GetLastError());
				Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileInfo");
				nReturn |= OX_EXCEPTION;
			}

			if(!bReturn)
			{
				strcpy(pv3DirEntry->FileName, "error");
				char pstrError[512];
				_snprintf(pstrError, 511, "Error obtaining dir info for [%s] on host [%s].", pdir, m_pszHost);
				Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileInfo");
				nReturn |= OX_NODIR;

			}
			else // search for the thingie
			{
				bool bFound = false;
				if(pv3Dir!=NULL)
				{
					for(int i=0; i<nEntries; i++)
					{
						if(stricmp(pfile, pv3Dir[i].FileName))
						{
							strcpy(pv3DirEntry->FileName, pv3Dir[i].FileName);
							pv3DirEntry->FileSize = pv3Dir[i].FileSize;
							pv3DirEntry->FileAttribs = pv3Dir[i].FileAttribs;
							pv3DirEntry->FileTimeStamp = pv3Dir[i].FileTimeStamp;

							i=nEntries;
							bFound = true;
						}
					}
					if(m_lpfnFreeOxSoxPtr)
					{
						try
						{
							m_lpfnFreeOxSoxPtr(pv3Dir);
						}
						catch(...)
						{
						}
					}
					else free(pv3Dir); // try this one
				}
				if(!bFound)
				{
					char pstrError[512];
					_snprintf(pstrError, 511, "File [%s] not found on [%s] on host [%s].", pfile, pdir, m_pszHost);
					Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileInfo");
					nReturn |= OX_NODIR;
				}
			}
		}
		else
		{
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetFullDir has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFileInfo");
			nReturn |= OX_NODIR;
		}
		if(pdir) free(pdir);
	}
//	free(psrc);
	return nReturn;

}

int CIS2Comm::OxSoxGetFileStats(char* pszSourceFile, char* pszDirAlias, unsigned* puiSize, unsigned* puiTimestamp )
{
	if((pszSourceFile==NULL)||(strlen(pszSourceFile)<=0))	return OX_BADPARAM;
	if(puiSize==NULL) 	return OX_BADPARAM;
	if(puiTimestamp==NULL) 	return OX_BADPARAM;
//	if(m_bTransferring) return OX_INPROGRESS;  // dont do if something is in progress already

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxGetFileInfo");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxGetFileInfo");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxGetFileInfo");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFileInfo");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxGetFileInfo");
		return OX_NULLHOST;
	}

	// assemble filename
	char psrc[IS2_MAX_PATHNAME];
//	char* psrc  = (char*)malloc(IS2_MAX_PATHNAME);
//	if(psrc == NULL) return OX_NOALLOC;

	// source to delete
	//if theres a dir alias, then use it
	if((pszDirAlias)&&(strlen(pszDirAlias)>0)&&(*pszDirAlias == '$')) //must be valid
	{
		if(strcmp(pszDirAlias, "$VIDEO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$AUDIO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$THUMBS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$FONTS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PLAYLIST")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$ROOT")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$CODE")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PARAM")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$LAS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PANEL")==0) strcpy(psrc, pszDirAlias);
		else strcpy(psrc,"");

		// now, dest is either blank or has an alias
		if(strlen(psrc)==0)  //blank
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
		}

		strcat(psrc, "/");
		
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			strcat(psrc, pszSourceFile);
		}
	}
	else  // must discard use of alias
	{
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS/");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
			strcat(psrc, pszSourceFile);
		}
	}

	int nReturn = OX_SUCCESS;
	if(m_lpfnGetFileStat2) 
	{ 
		BOOL bReturn = FALSE;
		try
		{
			bReturn = m_lpfnGetFileStat2(m_pszHost, psrc, puiSize, puiTimestamp); 
		}
		catch(...)
		{
			bReturn = FALSE;
			char pstrError[512];
			_snprintf(pstrError, 511, "Exception in GetFileStat2: code %d", GetLastError());
			Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileStats");
			nReturn |= OX_EXCEPTION;
		}
		if(!bReturn)
		{
			char pstrError[512];
			_snprintf(pstrError, 511, "Error obtaining file statistics for [%s] on host [%s].", psrc, m_pszHost);
			Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetFileStats");
			nReturn |= OX_NOFILE;
		}
	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetFileStats has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetFileStats");
		nReturn |= OX_NOFILE;
	}

//	free(psrc);
	return nReturn;
}

//system oxsox
int CIS2Comm::OxSoxGetDirInfo(char* pszDirAlias, PV3DirEntry_* ppv3DirEntry, int* pnNumEntries, unsigned long ulFlags)
{
	if((pszDirAlias==NULL)||(strlen(pszDirAlias)<=0)||(*pszDirAlias != '$'))	return OX_BADPARAM;
	if(ppv3DirEntry==NULL) 	return OX_BADPARAM;
	if(pnNumEntries==NULL) 	return OX_BADPARAM;
//	if(m_bTransferring) return OX_INPROGRESS;  // dont do if something is in progress already

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxGetDirInfo");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxGetDirInfo");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxGetDirInfo");
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetDirInfo");
			return OX_ERROR;
		}
	}

	if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxGetDirInfo");
		return OX_NULLHOST;
	}

	if(m_lpfnGetFullDir) 
	{ 
		int nReturn = OX_SUCCESS;
		BOOL bReturn = FALSE;
		try
		{
			bReturn = m_lpfnGetFullDir(m_pszHost, pszDirAlias, ppv3DirEntry, pnNumEntries); 
		}
		catch(...)
		{
			bReturn = FALSE;
			char pstrError[512];
			_snprintf(pstrError, 511, "Exception in GetFullDir: code %d", GetLastError());
			Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetDirInfo");
			nReturn |= OX_EXCEPTION;
		}
		if(!bReturn)
		{
			char pstrError[512];
			_snprintf(pstrError, 511, "Error obtaining dir info for [%s] on host [%s].", pszDirAlias, m_pszHost);
			Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetDirInfo");
			if(nReturn) 
				return nReturn;
			else
				return OX_ERROR;
		}
	}
	else
	{
		Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetFullDir has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetDirInfo");
		return OX_ERROR;
	}
	return OX_SUCCESS;
}

int CIS2Comm::OxSoxGetDriveInfo(char* pszDirAlias, PDiskInfo_ pdisk_info, unsigned long ulFlags)
{
	if(pdisk_info==NULL) return OX_BADPARAM;
	pdisk_info->KBytes_Free = 0;
	pdisk_info->KBytes_Total = 0;

	if(ulFlags&OX_ALL)
	{
		DiskInfo_ disk_info[4];
		int nReturn[4];

		for(int i=0; i<4; i++)
		{
			disk_info[i].KBytes_Free = 0;
			disk_info[i].KBytes_Total = 0;

			switch(i)
			{
			case 0:	nReturn[i] = OxSoxGetDriveInfo("$CODE", &(disk_info[i])); break;
			case 1:	nReturn[i] = OxSoxGetDriveInfo("$THUMBS", &(disk_info[i])); break;
			case 2:	nReturn[i] = OxSoxGetDriveInfo("$VIDEO", &(disk_info[i])); break;
			case 3:	nReturn[i] = OxSoxGetDriveInfo("$AUDIO", &(disk_info[i])); break;
			}
		}
		// OK did all the calls.

		if(nReturn[0]+nReturn[1]+nReturn[2]+nReturn[3] == OX_SUCCESS)
		{
			// all calls successful.
			if(
					(disk_info[0].KBytes_Free == disk_info[1].KBytes_Free)
				&&(disk_info[1].KBytes_Free == disk_info[2].KBytes_Free)
				&&(disk_info[2].KBytes_Free == disk_info[3].KBytes_Free)
				&&(disk_info[0].KBytes_Total == disk_info[1].KBytes_Total)
				&&(disk_info[1].KBytes_Total == disk_info[2].KBytes_Total)
				&&(disk_info[2].KBytes_Total == disk_info[3].KBytes_Total)
				)
			{
				// this must be an intuition
				pdisk_info->KBytes_Free = disk_info[0].KBytes_Free;
				pdisk_info->KBytes_Total = disk_info[0].KBytes_Total;
			}
			else
			{
				// this must be an imagestore2/3
				pdisk_info->KBytes_Free = disk_info[0].KBytes_Free;
				pdisk_info->KBytes_Total = disk_info[0].KBytes_Total;
				pdisk_info->KBytes_Free += disk_info[1].KBytes_Free;
				pdisk_info->KBytes_Total += disk_info[1].KBytes_Total;
				pdisk_info->KBytes_Free += disk_info[2].KBytes_Free;
				pdisk_info->KBytes_Total += disk_info[2].KBytes_Total;
				pdisk_info->KBytes_Free += disk_info[3].KBytes_Free;
				pdisk_info->KBytes_Total += disk_info[3].KBytes_Total;
			}

			return OX_SUCCESS;
		}
		return OX_ERROR;
	}
	else
	{
		if((pszDirAlias==NULL)||(strlen(pszDirAlias)<=0)||(*pszDirAlias != '$'))	return OX_BADPARAM;

		if(m_bDLLWinsockInit == false)
		{
			if(m_lpfnInitWinsock)
			{ 
				try
				{
					if(m_lpfnInitWinsock())
					{
						Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxGetDriveInfo");
						m_bDLLWinsockInit = true; // InitWinsock always returns true
					}
					else  // should never get here
					{
						Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxGetDriveInfo");
						return OX_ERROR;
					}
				}
				catch(...)
				{
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxGetDriveInfo");
					return OX_ERROR;
				}
			}
			else
			{
				Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetDriveInfo");
				return OX_ERROR;
			}
		}

		if((m_pszHost == NULL)||(strlen(m_pszHost)<=0))
		{
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "Host is invalid.", "CIS2Comm:OxSoxGetDriveInfo");
			return OX_NULLHOST;
		}

		if(m_lpfnGetDriveInfo) 
		{ 
			int nReturn = OX_SUCCESS;
			BOOL bReturn = FALSE;
			try
			{
				bReturn = m_lpfnGetDriveInfo(m_pszHost, pszDirAlias, pdisk_info);
			}
			catch(...)
			{
				bReturn = FALSE;
				char pstrError[512];
				_snprintf(pstrError, 511, "Exception in GetDriveInfo: code %d", GetLastError());
				Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetDriveInfo");
				nReturn |= OX_EXCEPTION;
			}
			if(!bReturn)
			{
				char pstrError[512];
				_snprintf(pstrError, 511, "Error obtaining drive info for [%s] on host [%s].", pszDirAlias, m_pszHost);
				Message(MSG_ICONERROR, pstrError, "CIS2Comm:OxSoxGetDriveInfo");
				if(nReturn)
					return nReturn;
				else
					return OX_ERROR;
			}
		}
		else
		{
			Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "GetDriveInfo has a NULL function address, cannot complete.", "CIS2Comm:OxSoxGetDriveInfo");
			return OX_ERROR;
		}
		return OX_SUCCESS;
	}
}

int CIS2Comm::OxSoxAbortTransfer(unsigned long ulFlags)
{
	if(!m_bTransferring) return OX_SUCCESS;  // ok...

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					if(ulFlags&OX_REPORT) Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxAbortTransfer");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					if(ulFlags&OX_REPORT) Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxAbortTransfer");
					return OX_ERROR;
				}
			}
			catch(...)
			{
				if(ulFlags&OX_REPORT) Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxAbortTransfer");
				return OX_ERROR;
			}
		}
		else
		{
			if(ulFlags&OX_REPORT) Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxAbortTransfer");
			return OX_ERROR;
		}
	}

	if(m_lpfnAbortTransfer) 
	{ 
		m_bTransferAborted = true;
		try
		{
			m_lpfnAbortTransfer();  // aborts the current transfer.
		}
		catch(...)
		{
		}

		if(ulFlags&OX_KILLALL)
		{
			// have to unqueue everything.
			m_bTransferError = false;
			m_nTransferIDCurrent = OX_KILL;
			m_nTransferIDLast = OX_KILL;
		}
	}
	else
	{
		if(ulFlags&OX_REPORT) Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "AbortTransfer has a NULL function address, cannot complete.", "CIS2Comm:OxSoxAbortTransfer");
		return OX_ERROR;
	}
	return OX_SUCCESS;
}

int CIS2Comm::OxSoxTransferProgress(float* pfProgress, unsigned long ulFlags)
{
	if(pfProgress == NULL) return OX_BADPARAM;
	if(!m_bTransferring) return OX_ERROR;  // nothing in progress

	if(m_bDLLWinsockInit == false)
	{
		if(m_lpfnInitWinsock)
		{ 
			try
			{
				if(m_lpfnInitWinsock())
				{
					if(ulFlags&OX_REPORT) Message(MSG_ICONINFO, "InitWinsock returned TRUE.", "CIS2Comm:OxSoxTransferProgress");
					m_bDLLWinsockInit = true; // InitWinsock always returns true
				}
				else  // should never get here
				{
					if(ulFlags&OX_REPORT) Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock returned FALSE.", "CIS2Comm:OxSoxTransferProgress");
					return OX_ERROR;
				}
			}
			catch(...)
			{
					if(ulFlags&OX_REPORT) Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock exception.", "CIS2Comm:OxSoxTransferProgress");
					return OX_ERROR;
			}
		}
		else
		{
			if(ulFlags&OX_REPORT) Message(MSG_PRI_HIGH|MSG_ICONERROR, "InitWinsock has a NULL function address, cannot complete.", "CIS2Comm:OxSoxTransferProgress");
			return OX_ERROR;
		}
	}

	if(m_lpfnTransferProgress) 
	{ 
		try
		{
			*pfProgress = m_lpfnTransferProgress();  // get the current transfer progress.
		}
		catch(...)
		{
			return OX_ERROR; // bad return value
		}
		if((*pfProgress>100.0)||(*pfProgress<0.0)) return OX_ERROR; // bad return value
	}
	else
	{
		if(ulFlags&OX_REPORT) Message(MSG_PRI_MEDIUM|MSG_ICONERROR, "TransferProgress has a NULL function address, cannot complete.", "CIS2Comm:OxSoxTransferProgress");
		return OX_ERROR;
	}
	return OX_SUCCESS;
}

void TransferFile(void* pArg)
{
	ThreadInfo_t* pInfo = (ThreadInfo_t*) pArg;
	if(pInfo)
	{
		CIS2Comm* pis2 = (CIS2Comm*)pInfo->pObj;
		while(
						(pInfo->nTransferDir!=OX_KILL)
					&&(
							(pis2->m_bTransferring)
						||(pis2->m_bTransferSuppress)
						||(pis2->m_bTransferError)  // if an error, wait until outside thread deals with it;
						||(pInfo->nTransferID!=pis2->m_nTransferIDCurrent)
						)
					)		
		{
			Sleep(5); // just wait
		}
		BOOL bReturn;
		if(pInfo->nTransferDir == OX_KILL)
		{
			// just kill
			if(pInfo->ulFlags&OX_REPORT)
			{
				char pstrError[512];
				_snprintf(pstrError, 511, "Killing transfer [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, pis2->m_pszHost);
				pis2->Message(MSG_ICONINFO, pstrError, "CIS2Comm:TransferFile");
			}
		}
		else
		if(pInfo->nTransferDir == OX_GET)
		{
			pis2->m_bTransferring = true;
			pis2->m_bTransferAborted = false;
			try
			{
				bReturn = pis2->m_lpfnGetFile2(pis2->m_pszHost, pInfo->pszTransferSource, pInfo->pszTransferDest ); 
			}
			catch(...)
			{
				bReturn = FALSE;
				char pstrError[512];
				_snprintf(pstrError, 511, "Exception calling m_lpfnGetFile2. code %d", GetLastError());
				pis2->Message(MSG_ICONERROR, pstrError, "CIS2Comm:TransferFile");
			}
			pis2->m_bTransferSuppress = true;
			pis2->m_bTransferring = false;
			if(bReturn)
			{
				pis2->m_bTransferError = false;
				pis2->m_pTransferError = NULL;
				pis2->m_bTransferring = false;

				if(pInfo->ulFlags&OX_REPORT)
				{
					char pstrError[512];
					_snprintf(pstrError, 511, "Successful transfer [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, pis2->m_pszHost);
					pis2->Message(MSG_ICONINFO, pstrError, "CIS2Comm:TransferFile");
				}
				if(pInfo->ulFlags&OX_DEL)  // delete the file after get
				{
					bReturn = pis2->OxSoxDelFile(pInfo->pszTransferSource);  // alias dir already in there
					if(pInfo->ulFlags&OX_REPORT)
					{
						char pstrError[512];
						if(bReturn)
						{
							_snprintf(pstrError, 511, "Deleted [%s] on host [%s].", pInfo->pszTransferSource, pis2->m_pszHost);
							pis2->Message(MSG_ICONINFO, pstrError, "CIS2Comm:TransferFile");
						}
						else
						{
							_snprintf(pstrError, 511, "Error deleting [%s] on host [%s].", pInfo->pszTransferSource, pis2->m_pszHost);
							pis2->Message(MSG_ICONERROR, pstrError, "CIS2Comm:TransferFile");
						}
					}
				}
			}
			else
			{
				pis2->m_bTransferError = pis2->m_bTransferAborted?false:true;  // no error if intentionally aborted
				pis2->m_pTransferError = pInfo;
				pis2->m_bTransferring = false;
				if((pInfo->ulFlags&OX_REPORT)&&(pis2->m_bTransferError))
				{
					char pstrError[512];
					_snprintf(pstrError, 511, "Error transferring [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, pis2->m_pszHost);
					pis2->Message(MSG_ICONERROR, pstrError, "CIS2Comm:TransferFile");
				}
			}
		}
		else
		if(pInfo->nTransferDir == OX_PUT)
		{
			pis2->m_bTransferring = true;
			pis2->m_bTransferAborted = false;
			try
			{
				bReturn = pis2->m_lpfnPutFile2(pis2->m_pszHost,  pInfo->pszTransferDest, pInfo->pszTransferSource );
			}
			catch(...)
			{
				bReturn = FALSE;
				char pstrError[512];
				_snprintf(pstrError, 511, "Exception calling m_lpfnPutFile2. code %d", GetLastError());
				pis2->Message(MSG_ICONERROR, pstrError, "CIS2Comm:TransferFile");
			}
			pis2->m_bTransferSuppress = true;
			pis2->m_bTransferring = false;
			if(bReturn)
			{
				pis2->m_bTransferError = false;
				pis2->m_pTransferError = NULL;
				pis2->m_bTransferring = false;
				if(pInfo->ulFlags&OX_REPORT)
				{
					char pstrError[512];
					_snprintf(pstrError, 511, "Successful transfer [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, pis2->m_pszHost);
					pis2->Message(MSG_ICONINFO, pstrError, "CIS2Comm:TransferFile");
				}
				if(pInfo->ulFlags&OX_DEL)  // delete the file after put
				{
					bReturn = (BOOL)_unlink(pInfo->pszTransferSource);  // alias dir already in there
					if(pInfo->ulFlags&OX_REPORT)
					{
						char pstrError[512];
						if(bReturn==0)  //unlink returns 0 on success, -1 on fail
						{
							_snprintf(pstrError, 511, "Deleted [%s] on localhost.", pInfo->pszTransferSource);
							pis2->Message(MSG_ICONINFO, pstrError, "CIS2Comm:TransferFile");
						}
						else
						{
							_snprintf(pstrError, 511, "Error deleting [%s] on localhost.", pInfo->pszTransferSource);
							pis2->Message(MSG_ICONERROR, pstrError, "CIS2Comm:TransferFile");
						}
					}
				}
			}
			else
			{
				pis2->m_bTransferError = pis2->m_bTransferAborted?false:true;  // no error if intentionally aborted
				pis2->m_pTransferError = pInfo;
				pis2->m_bTransferring = false;
				if((pInfo->ulFlags&OX_REPORT)&&(pis2->m_bTransferError))
				{
					char pstrError[512];
					_snprintf(pstrError, 511, "Error transferring [%s]->[%s] on host [%s].", pInfo->pszTransferSource, pInfo->pszTransferDest, pis2->m_pszHost);
					pis2->Message(MSG_ICONERROR, pstrError, "CIS2Comm:TransferFile");
				}
			}
		}

		// deal with the queue, if any
		pis2->m_nTransferIDCurrent++;  // advance to next
		if(pis2->m_nTransferIDCurrent>pis2->m_nTransferIDLast) // oo, nothing, so can reset
		{
			pis2->m_nTransferIDCurrent = OX_KILL;
			pis2->m_nTransferIDLast = OX_KILL;
		}
		pis2->m_bTransferSuppress = false;

		if(!pis2->m_bTransferError)  // if there is an error, we deal with it and delete this stuff outside
		{
			if(pInfo->pszTransferSource) free(pInfo->pszTransferSource);
			if(pInfo->pszTransferDest) free(pInfo->pszTransferDest);
			delete pInfo;
		}
	}
}


int CIS2Comm::UtilParseTem(char* pszSourceFile, char*** pppszChildren, int* pnNumChildren)
{
	if((pszSourceFile==NULL)||(strlen(pszSourceFile)<=0))	return OX_BADPARAM;
	if(pppszChildren==NULL) 	return OX_BADPARAM;
	if(pnNumChildren==NULL) 	return OX_BADPARAM;

	FILE* fp = fopen(pszSourceFile, "rb");
	if(fp)
	{
		int nRV = OX_SUCCESS;
		// determine file size
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);

		char* pch = (char*) malloc(ulFileLen+1); // term zero
		if(pch)
		{
			fseek(fp, 0, SEEK_SET);
			fread(pch, sizeof(char), ulFileLen, fp);
			*(pch+ulFileLen) = 0; // term zero

			// now we have a buffer
			*pnNumChildren = 0;
			*pppszChildren = NULL;

			char** ppszReturnChildren=NULL;
			int nChildren=0;

			char pszFilename[MAX_PATH];

			char* pchElement = NULL;
			pchElement = strstr(pch, "<Box");
			while(pchElement)
			{
//Message(MSG_ICONHAND, pchElement, "CIS2Comm:UtilParseTem");
				char* pchFinish = NULL;
				pchFinish = strstr(pchElement, "</Box>");  // end of the box tag
				if(pchFinish)
				{
					// well formed tag, all the info is in there
					// let's parse it.
					char* pchField = NULL;
					pchField = strstr(pchElement, "FileName=\"");  // start of the filename.  Gets font files too.
					if((pchField)&&(pchField<pchFinish))
					{
						// we have an element
						pchField += strlen("FileName=\"");  // start of the filename.  Gets font files too.
						pchElement = strstr(pchField, "\"");  // end of the filename 

						if((pchElement)&&(pchElement<pchFinish))
						{
//Message(MSG_ICONHAND, "Found end of name of element", "CIS2Comm:UtilParseTem");
//Message(MSG_ICONHAND, pchField, "CIS2Comm:UtilParseTem");
							int i=pchElement-pchField;
							if(i>0)
							{
								memset(pszFilename, 0, MAX_PATH);
								memcpy(pszFilename, pchField, i);
								char** ppszChildren = new char*[(nChildren+1)];
//char bx[64]; sprintf(bx, "ppszChildren %08x",  ppszChildren);
//Message(MSG_ICONHAND,  bx, "CIS2Comm:UtilParseTem"); Sleep(50);
								if(ppszChildren)
								{
									char* pchChild = (char*)malloc(strlen(pszFilename)+1);
									if(pchChild)
									{
								//		memset(pchChild, 0, pchElement-pchField+1);
								//		strncpy(pchChild, pchField, pchElement-pchField);
								//		memcpy(pchChild, pchField, i);
								//		*(pchChild+i) = 0; //null term
										strcpy(pchChild, pszFilename);

//char bx[64]; sprintf(bx, "%08x %08x %s", pchChild,  ppszChildren, pchChild);
//Message(MSG_ICONHAND,  bx, "CIS2Comm:UtilParseTem1a"); Sleep(50);
//Message(MSG_ICONHAND,  pchChild, "CIS2Comm:UtilParseTem1"); Sleep(50);
//char bx[64]; sprintf(bx, "%08x %08x %s", pchChild,  ppszChildren, pchChild);
//Message(MSG_ICONHAND,  bx, "CIS2Comm:UtilParseTem1b"); Sleep(50);
										i=0;
										if(ppszReturnChildren)
										{
//											memcpy(ppszChildren, ppszReturnChildren, nChildren*sizeof(char*));
											
											while(i<nChildren)
											{
												ppszChildren[i] = ppszReturnChildren[i];
												i++;
											}
											
											delete [] ppszReturnChildren;
											ppszReturnChildren = NULL;
										}
										ppszChildren[nChildren] = pchChild;
										ppszReturnChildren = ppszChildren;
										nChildren++;
									}
								}
							}
						}
					}
					pchElement = strstr(pchFinish, "<Box");  // next box tag
				}
				else pchElement = NULL ; //breaks
			}

			*pppszChildren  = ppszReturnChildren;
			*pnNumChildren = nChildren;

/*
			int i=0;
			while(i<*pnNumChildren)
			{
//AfxMessageBox(ppszReturnChildren[i]);
//Message(MSG_ICONHAND, ppszReturnChildren[i], "CIS2Comm:UtilParseTem");Sleep(50);
				i++;
			}
*/
			free(pch);
		}
		else
		{
			nRV = OX_ERROR;
		}
		fclose(fp);
	
		return nRV;
	}

	return OX_FILEERROR;
}

int CIS2Comm::UtilParseRemoteTem(char* pszSourceFile, char* pszDirAlias, char*** pppszChildren, int* pnNumChildren, char* pszTempFileName, bool bDeleteFile)
{
	if((pszSourceFile==NULL)||(strlen(pszSourceFile)<=0))	return OX_BADPARAM;
	if(pppszChildren==NULL) 	return OX_BADPARAM;
	if(pnNumChildren==NULL) 	return OX_BADPARAM;

	char tempfile[MAX_PATH];

	if((pszTempFileName)&&(strlen(pszTempFileName)))
	{
		_snprintf(tempfile, MAX_PATH, "%s", pszTempFileName);
	}
	else
	{
		strcpy(tempfile, "C:\\tmp.tem");
	}

	_unlink(tempfile);
//AfxMessageBox("retrieving...");

/*
	int nRV = OxSoxGetFile("C:\\tmp.tem", pszSourceFile, pszDirAlias);
	if(nRV == OX_SUCCESS)
	{
//AfxMessageBox("retrieve success");
		while( (m_bTransferring)||(m_bTransferSuppress) )
		{
			Sleep(50); // wait for tem file to transfer, should be fairly quick...
		}
		Sleep(1500); // added measure, have to wait for disk access

		// ok try to open it.
		FILE* fp = fopen("C:\\tmp.tem", "rb");
		nRV = 0;
		while((fp==NULL)&&(nRV<20))  // 20 tries is ten seconds
		{
			nRV++;
			fp = fopen("C:\\tmp.tem", "rb");
			Sleep(500); // added measure, have to wait for disk access
		}
		fclose(fp);
		Sleep(500); // added measure, have to wait for disk access
*/

	int nRV = OX_ERROR;

	// add the dir alias stuff

	char psrc[IS2_MAX_PATHNAME];
//	char* psrc  = (char*)malloc(IS2_MAX_PATHNAME);
//	if(psrc == NULL) return OX_NOALLOC;

	// source
	//if theres a dir alias, then use it
	if((pszDirAlias)&&(strlen(pszDirAlias)>0)&&(*pszDirAlias == '$')) //must be valid
	{
		if(strcmp(pszDirAlias, "$VIDEO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$AUDIO")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$THUMBS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$FONTS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PLAYLIST")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$ROOT")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$CODE")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PARAM")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$LAS")==0) strcpy(psrc, pszDirAlias);
		else
		if(strcmp(pszDirAlias, "$PANEL")==0) strcpy(psrc, pszDirAlias);
		else strcpy(psrc,"");

		// now, dest is either blank or has an alias
		if(strlen(psrc)==0)  //blank
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
		}

		strcat(psrc, "/");
		
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			strcat(psrc, pszSourceFile);
		}
	}
	else  // must discard use of alias
	{
		// override alias if already in filename
		if(
			  (strncmp(pszSourceFile, "$VIDEO", 6)==0)
			||(strncmp(pszSourceFile, "$AUDIO", 6)==0)
			||(strncmp(pszSourceFile, "$THUMBS", 7)==0)
			||(strncmp(pszSourceFile, "$FONTS", 6)==0)
			||(strncmp(pszSourceFile, "$PLAYLIST", 9)==0)
			||(strncmp(pszSourceFile, "$ROOT", 5)==0)
			||(strncmp(pszSourceFile, "$CODE", 5)==0)
			||(strncmp(pszSourceFile, "$PARAM", 6)==0)
			||(strncmp(pszSourceFile, "$LAS", 4)==0)
			||(strncmp(pszSourceFile, "$PANEL", 6)==0)
			)
		{
			strncpy(psrc, pszSourceFile, IS2_MAX_PATHNAME);
		}
		else
		{
			//determine dir alias by file ext.
			int nLen = strlen(pszSourceFile);
			if(nLen>4)  // min "x.oxt"
			{
				if(_stricmp(pszSourceFile+nLen-3, "tem")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxt")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxa")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxi")==0) strcpy(psrc, "$VIDEO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxe")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "oxw")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "wav")==0) strcpy(psrc, "$AUDIO/");
				else
				if(_stricmp(pszSourceFile+nLen-3, "ttf")==0) strcpy(psrc, "$FONTS/");
				else
				{
//					free(psrc);
					return OX_BADPARAM;
				}
			}
			else
			{
//				free(psrc);
				return OX_BADPARAM;
			}
			strcat(psrc, pszSourceFile);
		}
	}

	try
	{
		if((m_lpfnGetFile2)&&(m_lpfnGetFile2(m_pszHost, psrc, tempfile )))
		{
	//		free(psrc);
			nRV = UtilParseTem(tempfile, pppszChildren, pnNumChildren);
		}
	}
	catch(...)
	{
		nRV = OX_ERROR;
	}
	if(bDeleteFile) 	_unlink(tempfile);

//	free(psrc);
	return nRV;
}



CIS2Hive::CIS2Hive()
{
	m_bHiveInitialized = false;
	m_ppIs2Comm = NULL;
	m_nNumObjects=0;
	InitializeCriticalSection(&m_critHive);  // critsec access to hive

	// some settings:
	m_bReloadLibraries = true; // if true (default), will reload the DLLs at staggered intervals (based on a fraction of the reload interval)
	m_nReloadInterval = 86400; // in seconds, defaults to 1 day.  If there are 7 objects in the hive, each gets reloaded once per day, each 1/7 of a day after another in sequence, until they are all reset within 1 day.
	m_nReloadCheckInterval = 30; // check every 30 seconds (default) to see if we need to reload

	m_pszBaseDLL = NULL;

	m_bHiveMaintenanceKill = true; 
	m_bHiveMaintenanceStarted = false; 

	m_nIndex = -1;
	m_nCheckIndex = -1;

	m_ulLoadTime = 0;
}

CIS2Hive::~CIS2Hive()
{
	m_bHiveMaintenanceKill = true; 

	while(m_bHiveMaintenanceStarted) Sleep(10);

	DeleteCriticalSection(&m_critHive);  // critsec access to hive
}

int CIS2Hive::InitializeHive(char* pszBaseDLL, int nNumHiveObjects) // copies files and loads them in.
{
	if((pszBaseDLL==NULL)||(strlen(pszBaseDLL)<=0)||(nNumHiveObjects<=0)) return OX_BADPARAM;

	DeInitializeHive(); // unloads all libraries, deletes objects

	m_pszBaseDLL = (char*)malloc(strlen(pszBaseDLL)+1);
	if(m_pszBaseDLL == NULL) return OX_NOALLOC;

	strcpy(m_pszBaseDLL, pszBaseDLL);

	m_ppIs2Comm = new CIS2Comm*[nNumHiveObjects];
	if(m_ppIs2Comm == NULL) return OX_NOALLOC;

	char pszHiveFile[MAX_PATH];

	int i=0;

	EnterCriticalSection(&m_critHive);  // critsec access to hive

	_timeb timeNow;
	_ftime(&timeNow);

	while(i<nNumHiveObjects)
	{
		sprintf(pszHiveFile, "H%d_%s", m_nNumObjects, m_pszBaseDLL);

		if(CopyFile(m_pszBaseDLL, pszHiveFile, FALSE)==0)
		{
			//failed!
			int e = GetLastError();
			if(e == ERROR_FILE_NOT_FOUND) // source file not there, need to just exit out since all will fail
			{
				char pstrError[512];
				_snprintf(pstrError, 511, "Source DLL file %s does not exist.", m_pszBaseDLL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
				return OX_FILEERROR;
			}
			else
			{
				char pstrError[1024];
				_snprintf(pstrError, 511, "Error %d copying file %s to %s.", e, m_pszBaseDLL, pszHiveFile);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
				return OX_FILEERROR;
			}
		}
		else
		{
			// successful copy.
			// create new object and load it into the array

			CIS2Comm* pObj = new CIS2Comm;
			if(pObj)
			{
				pObj->InitializeMessaging(m_pmsgr);

				// dont do this, it happens inside oxsoxload.
//				pObj->m_pszOxSoxFilename = (char*)malloc(strlen(pszHiveFile)+1);
//				if(pObj->m_pszOxSoxFilename)
//				{
//					strcpy(pObj->m_pszOxSoxFilename,pszHiveFile);
//				}

				int nReturn = pObj->OxSoxLoad(pszHiveFile);
				char pstrError[1024];
				if(nReturn<0)
				{
					_snprintf(pstrError, 511, "Error: OxSoxLoad returned %d", nReturn);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
				}	
				else
				{
					_snprintf(pstrError, 511, "Success: OxSoxLoad returned %07x", nReturn);
					Message(MSG_ICONNONE, pstrError, "CIS2Hive:InitializeHive");


					if(pObj->m_lpfnInitWinsock)
					{ 
						try
						{
							nReturn = pObj->m_lpfnInitWinsock();
							pObj->m_bDLLWinsockInit = true; // InitWinsock always returns true
						}
						catch(...)
						{
							_snprintf(pstrError, 511, "Error: exception calling InitWinsock");
							Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
						}
					}
					else
					{
						_snprintf(pstrError, 511, "Error: NULL function address for InitWinsock");  //(Dispatch message)
						Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
					}


				}

				m_ppIs2Comm[i] = pObj;

				m_ppIs2Comm[i]->m_ulLoadTime = timeNow.time;

				m_nNumObjects++;
			}
			else
			{
				while (i>0)
				{
					i--;
					try{ delete m_ppIs2Comm[i];} 
					catch(...){}
				}
				try{ delete [] m_ppIs2Comm;} catch(...){}
				m_ppIs2Comm = NULL;
				m_nNumObjects=0;
				return OX_NOALLOC;
			}
		}

		i++;
	}


	m_ulLoadTime = timeNow.time;

	LeaveCriticalSection(&m_critHive);  // critsec access to hive

	if(m_bReloadLibraries)
	{
		m_bHiveMaintenanceKill = false;
		if(_beginthread(HiveMaintenanceThread, 0, (void*)this)==-1)
		{
			char pstrError[512];
			_snprintf(pstrError, 511, "Error: could not start hive maintenance thread.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
		}	
		else
		{
			char pstrError[512];
			_snprintf(pstrError, 511, "Success: started hive maintenance thread.");
			Message(MSG_ICONNONE, pstrError, "CIS2Hive:InitializeHive");
		}
	}

	return OX_SUCCESS;
}

int CIS2Hive::DeInitializeHive() // unloads all libraries
{
	m_nIndex = -1;
	m_nCheckIndex = -1;

	m_bHiveMaintenanceKill = true;
	while(m_bHiveMaintenanceStarted) Sleep(10);

	int i=0;
	EnterCriticalSection(&m_critHive);  // critsec access to hive
	if(m_ppIs2Comm)
	{
		while (i<m_nNumObjects)
		{
			if(m_ppIs2Comm[i])
			{ 
				EnterCriticalSection(&m_ppIs2Comm[i]->m_critIS2);  // critsec access
				while( m_ppIs2Comm[i]->OxSoxFree() != OX_SUCCESS)
				{
					Sleep(100);
				}

				if((m_ppIs2Comm[i]->m_pszOxSoxFilename)&&(m_pszBaseDLL)&&(strcmp(m_ppIs2Comm[i]->m_pszOxSoxFilename, m_pszBaseDLL )))
					_unlink(m_ppIs2Comm[i]->m_pszOxSoxFilename); //delete actual DLL;
				LeaveCriticalSection(&m_ppIs2Comm[i]->m_critIS2);  // critsec access
				try{ delete m_ppIs2Comm[i];} 
				catch(...){}
			}
			i++;
		}
		try{ delete [] m_ppIs2Comm;} catch(...){}
	}
	m_ppIs2Comm = NULL;
	m_nNumObjects=0;

	if(m_pszBaseDLL){try{ delete m_pszBaseDLL;} catch(...){}}
	m_pszBaseDLL =  NULL;

	LeaveCriticalSection(&m_critHive);  // critsec access to hive
	return OX_ERROR;
}


int CIS2Hive::CheckHostInHive(char* pszServer)// checks if a particular host is transferring, so we can disallow new transfers at the same time, from a different hive member.
{
	if((pszServer)&&(strlen(pszServer)))
	{
		if(m_ppIs2Comm)
		{
			int i=0;

			while (i<m_nNumObjects)
			{
				if(m_ppIs2Comm[i])
				{
	EnterCriticalSection(&m_ppIs2Comm[i]->m_critIS2);  // critsec access to hive

					if(m_ppIs2Comm[i]->m_bTransferring)
					{
						if((m_ppIs2Comm[i]->m_pszHost)&&(stricmp(pszServer, m_ppIs2Comm[i]->m_pszHost)==0))
						{
	LeaveCriticalSection(&m_ppIs2Comm[i]->m_critIS2);  // critsec access to hive

							return i;
						}
					}
	LeaveCriticalSection(&m_ppIs2Comm[i]->m_critIS2);  // critsec access to hive

				}

				i++;
			}
		}
	}

	return OX_ERROR;
}


// the following call "should" be called while within a crit sec, so it stays valid.
// but practically, no one should be messing with the hive count...
int CIS2Hive::AccessHive(int nRequiredIndex)
{
	if(m_ppIs2Comm)
	{
		if((nRequiredIndex>=0)&&(nRequiredIndex<m_nNumObjects))
		{
			if(m_ppIs2Comm[nRequiredIndex])
			{
				if(
					  (!m_ppIs2Comm[nRequiredIndex]->m_bScheduledForRestart)  // if its scheduled for restart, we dont want to return it even though we want to check it out. we'll let it restart, then return OK so that we can check the file delivery
//					&&(!m_ppIs2Comm[nRequiredIndex]->m_bTransferring)  // commented out to allow us to request a particular instance to see transferring status.
					&&(m_ppIs2Comm[nRequiredIndex]->m_hinstDLL)
					)
				{
/*
					if(!m_ppIs2Comm[nRequiredIndex]->m_bTransferring) // if we arent transferring, we can actually set the last index to here.
					{
						m_nIndex = nRequiredIndex;
					}
*/
					return nRequiredIndex; // return this but do not update the round robin as if this were a thing, we are just doing a check.
				}
			}
		}
	//	m_nIndex;  // last accessed index.
		m_nCheckIndex = m_nIndex+1;
		if(m_nCheckIndex >= m_nNumObjects) m_nCheckIndex = 0;

		int i=0;

		while ((i<m_nNumObjects)&&(m_nCheckIndex!=m_nIndex))
		{
			if(m_ppIs2Comm[m_nCheckIndex])
			{
				if(
					  (!m_ppIs2Comm[m_nCheckIndex]->m_bScheduledForRestart)
					&&(!m_ppIs2Comm[m_nCheckIndex]->m_bTransferring)
					&&(m_ppIs2Comm[m_nCheckIndex]->m_hinstDLL)
					)
				{
					m_nIndex = m_nCheckIndex;


					return m_nCheckIndex;
				}
			}
			m_nCheckIndex++;
			if(m_nCheckIndex >= m_nNumObjects) m_nCheckIndex = 0;

			i++;
		}
	}

	return OX_ERROR;
}


void HiveMaintenanceThread(void* pvArgs)
{
	CIS2Hive* pHive = (CIS2Hive*)pvArgs;
	if(pHive == NULL) return;
	pHive->m_bHiveMaintenanceStarted = true; 
	char pstrError[1024];
	
	_snprintf(pstrError, 511, "Thread started");
	pHive->Message(MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");

	bool bReportThreshold =true;
	bool bReportAvail =true;

	while(!pHive->m_bHiveMaintenanceKill)
	{
//		We want to reload based on a formula.
// each DLL should be reloaded at ((m_nReloadInterval/m_nNumObjects)* index) + m_ppIs2Comm[index]->m_ulLoadTime on the first init, then after that it's just after m_nReloadInterval.

		int i=0;

		_timeb timeNow;
		_ftime(&timeNow);
		unsigned long ulThreshold;

		bool bInProgWhenReady = false;

		EnterCriticalSection(&pHive->m_critHive);  // critsec access to hive
		if(pHive->m_ppIs2Comm)
		{
//	_snprintf(pstrError, 511, "array exists");
//	pHive->Message(MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");
			while (i<pHive->m_nNumObjects)
			{
//				_snprintf(pstrError, 511, "array iten %d is %sNULL", i, pHive->m_ppIs2Comm[i]?"not ":"");
//	pHive->Message(MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");

				if(pHive->m_ppIs2Comm[i])
				{ 
					ulThreshold = pHive->m_ppIs2Comm[i]->m_ulLoadTime + pHive->m_nReloadInterval;

					if((pHive->m_ppIs2Comm[i]->m_ulLoadTime == pHive->m_ulLoadTime)&&(i != 0)) // init go around.
					{
						ulThreshold -= ((pHive->m_nReloadInterval * (pHive->m_nNumObjects - i))/pHive->m_nNumObjects);
					}

//					_snprintf(pstrError, 511, "timecheck: threshold %d, now %d", ulThreshold, timeNow.time);
//					pHive->Message(MSG_ICONHAND, pstrError, "CIS2Hive:HiveMaintenance");

					if((unsigned long)timeNow.time > ulThreshold)
					{
						if(bReportThreshold)
						{
							_snprintf(pstrError, 511, "Reload: %s index %d (%d > %d)", pHive->m_ppIs2Comm[i]->m_pszOxSoxFilename?pHive->m_ppIs2Comm[i]->m_pszOxSoxFilename:"(null)", i, timeNow.time, ulThreshold);
							pHive->Message(MSG_PRI_NORMAL|MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");
							bReportThreshold = false;
						}

						if(i == pHive->m_nIndex)  // let's not reload the most recently used one until something else has been used.
						{
							pHive->m_ppIs2Comm[i]->m_bScheduledForRestart = true;
							bInProgWhenReady = true;

							if(bReportAvail)
							{

								_snprintf(pstrError, 511, "Reload: %s (waiting for availability on index %d at %d)",
									pHive->m_ppIs2Comm[i]->m_pszOxSoxFilename?pHive->m_ppIs2Comm[i]->m_pszOxSoxFilename:"(null)", i, timeNow.time);
								pHive->Message(MSG_PRI_NORMAL|MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");
								bReportAvail = false;
							}
						}
						else
						{
							EnterCriticalSection(&pHive->m_ppIs2Comm[i]->m_critIS2);  // critsec access

							if( pHive->m_ppIs2Comm[i]->OxSoxFree() == OX_SUCCESS)
							{
								Sleep(100);

								char* pchFilename = pHive->m_ppIs2Comm[i]->m_pszOxSoxFilename; // have to do this step to overwrite the thing again...
								pHive->m_ppIs2Comm[i]->m_pszOxSoxFilename = NULL;

								int nReturn = pHive->m_ppIs2Comm[i]->OxSoxLoad(pchFilename);

								free(pchFilename);

								if(nReturn<0)
								{
									_snprintf(pstrError, 511, "Reload Error: OxSoxLoad returned %d", nReturn);
									pHive->Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:HiveMaintenance");
								}	
								else
								{
									pHive->m_ppIs2Comm[i]->m_ulLoadTime = ulThreshold;
									_snprintf(pstrError, 511, "Reload Success: OxSoxLoad returned %07x", nReturn);
									pHive->Message(MSG_ICONNONE, pstrError, "CIS2Hive:HiveMaintenance");

									pHive->m_ppIs2Comm[i]->m_bScheduledForRestart = false; // let's just reset this here again to make sure.


									if(pHive->m_ppIs2Comm[i]->m_lpfnInitWinsock)
									{ 
										try
										{
											nReturn = pHive->m_ppIs2Comm[i]->m_lpfnInitWinsock();
											pHive->m_ppIs2Comm[i]->m_bDLLWinsockInit = true; // InitWinsock always returns true
										}
										catch(...)
										{
											_snprintf(pstrError, 511, "Error: exception calling InitWinsock");
											pHive->Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
										}
									}
									else
									{
										_snprintf(pstrError, 511, "Error: NULL function address for InitWinsock");  //(Dispatch message)
										pHive->Message(MSG_PRI_HIGH|MSG_ICONERROR, pstrError, "CIS2Hive:InitializeHive");
									}

								}

								bReportThreshold = true;
								bReportAvail = true;
							}
							else
							{
								pHive->m_ppIs2Comm[i]->m_bScheduledForRestart = true;
								bInProgWhenReady = true;
							}
							LeaveCriticalSection(&pHive->m_ppIs2Comm[i]->m_critIS2);  // critsec access
						}
					}
				}
				i++;
			}
		}
		LeaveCriticalSection(&pHive->m_critHive);  // critsec access to hive
		
		ulThreshold = timeNow.time + (bInProgWhenReady?1:pHive->m_nReloadCheckInterval);  // 1 sec only, if there was an in prog, have to retry it.

//	_snprintf(pstrError, 511, "Sleeping until %d, now %d", ulThreshold, timeNow.time);
//	pHive->Message(MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");

		while(!pHive->m_bHiveMaintenanceKill)
		{
			_ftime(&timeNow);

			if((unsigned long)timeNow.time > ulThreshold) break;			

			Sleep(10);  // really, don't need much on the refresh...
		}
//	_snprintf(pstrError, 511, "Slept");
//	pHive->Message(MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");
	}

	_snprintf(pstrError, 511, "Thread ended");
	pHive->Message(MSG_ICONINFO, pstrError, "CIS2Hive:HiveMaintenance");
	pHive->m_bHiveMaintenanceStarted = false; 
}
