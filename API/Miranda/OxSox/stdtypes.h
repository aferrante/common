#ifndef __INC_STDTYPES
#define __INC_STDTYPES
typedef             short     INT16;
typedef unsigned    short    UINT16;
typedef             int         INT;
typedef unsigned    int        UINT;
typedef     		char       INT8;
typedef unsigned    char      UINT8;
typedef unsigned    char      uchar;

typedef UINT16 u_short;
typedef int BOOL;

#endif
