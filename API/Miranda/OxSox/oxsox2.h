// OxSox2.h
// V2.0  MGS  02/06/1998
// The exposed interface to OxSox2.dll

#ifndef OXSOX2HDR
#define OXSOX2HDR

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __BUILDING_OXSOX2
#define __EXPORT_TYPE  __declspec(dllexport)
#else
#define __EXPORT_TYPE  __declspec(dllimport)
#endif

#include "stdtypes.h"

#define MAXUNITS         100    /* max no of entries in a units list struct */
#define MAXDIRENTRIES  1024  /* max no of entries in a dir storage struct */

#define OX_MAXPATH         100    /* max length (in chars) of a full file path */

// Unit Types
// Format is: High Byte denotes category, low byte gives finer detail (e.g. model, OS )
#define UNIT_TYPE_NOT_KNOWN        0x0000

#define UNIT_TYPE_OXTEL            0x1000
#define UNIT_TYPE_IMAGESTORE       0x1000
#define UNIT_TYPE_IMAGESTORE_DS    0x1100
#define UNIT_TYPE_IMAGESTORE_US    0x1200

#define UNIT_TYPE_PC               0x2000
#define UNIT_TYPE_OXTEL_PC         0x2100

#define HARD_DISK0_ROOT_NAME   "$ROOT"

#define VIDEO_DIRECTORY        "$VIDEO"
#define AUDIO_DIRECTORY        "$AUDIO"
#define CODE_DIRECTORY         "$CODE"
#define PARAM_DIRECTORY        "$PARAM"
#define LAS_DIRECTORY          "$LAS"
#define PAN_DIRECTORY          "$PANEL"
#define BMP_DIRECTORY          "$THUMBS"
#define PLAYLIST_DIRECTORY     "$PLAYLIST"
#define FONTS_DIRECTORY        "$FONTS"

#pragma pack(push,4)

// typedefs

// UNITS
typedef struct
   {
   char UnitName[OX_MAXPATH];
   int  UnitType;                // see #defines above
   int  UnitCaps;                // unit capabilities
   int  UnitServerVer;           // version level of server software
   char UnitDefaultCatalogPath[OX_MAXPATH];
   } UNITSENTRY, *PUNITSENTRY;

typedef struct
   {
   UNITSENTRY UnitEntry[MAXUNITS];
   } UNITSLIST, *PUNITSLIST;

// DIRECTORY
typedef struct
   {
   char FileName[OX_MAXPATH];
   } FILEDIRENTRY, *PFILEDIRENTRY;


typedef struct
   {
   FILEDIRENTRY FileEntry[MAXDIRENTRIES];
   } FILEDIR, *PFILEDIR;

#pragma pack(pop)

// Exported functions

extern __EXPORT_TYPE BOOL WINAPI OxSox2Entry( HINSTANCE  hinstDLL,	// handle to DLL module
                           DWORD  fdwReason,	// reason for calling function
                           LPVOID  lpvReserved 	);// reserved

// returns TRUE for success, FALSE for error
extern  __EXPORT_TYPE BOOL WINAPI GetUnitNames(PUNITSLIST pUnitsList,     // address of a units list
                  int * pNumUnits);                 // number of units in the list

// returns TRUE for success, FALSE for error
extern  __EXPORT_TYPE BOOL WINAPI GetDir( const char * szHostName, PFILEDIR pReturnedDirectory, int * nEntries );
extern  __EXPORT_TYPE BOOL WINAPI GetNamedDir( const char * szHostName, const char * szPath, PFILEDIR pReturnedDirectory, int * nEntries );
extern  __EXPORT_TYPE BOOL WINAPI GetFile( char * szHostName, char * szHostFileToGet, char * szLocalFileName );
//extern  __EXPORT_TYPE void WINAPI CancelGetFile(void);     use AbortTransfer
extern  __EXPORT_TYPE BOOL WINAPI PutFile( char * szHostName, char * szHostFileName, char * szLocalFileToPut );
extern  __EXPORT_TYPE BOOL WINAPI DelFile( char * serverName, char * szFileName );
extern  __EXPORT_TYPE BOOL WINAPI Ping( char * serverName );
extern  __EXPORT_TYPE int WINAPI ChangeDir( char * serverName, char * szNewDirectory );
extern  __EXPORT_TYPE int WINAPI PWDir( char * serverName, char * szReturnedPath );

extern  __EXPORT_TYPE BOOL WINAPI InitWinsock(void);
extern  __EXPORT_TYPE u_short WINAPI ReturnPortNumber(void);
extern  __EXPORT_TYPE BOOL WINAPI IsMemberOfGroup (char *grp);
BOOL IsNT(void);

// new stuff for OxSox2 05/06/1998
extern  __EXPORT_TYPE int GetTCPServerVer( const char * serverName, char * szServerVersionString );

extern  __EXPORT_TYPE BOOL WINAPI GetNamedDir2( const char * szHostName, const char * szDirectoryName, PFILEDIR pReturnedDirectory, int * nEntries );
extern  __EXPORT_TYPE BOOL WINAPI GetFile2( char * szHostName, char * szHostFileToGet, char * szLocalFileName );

extern  __EXPORT_TYPE BOOL WINAPI PutFile2( char * szHostName, char * szHostFileName, char * szLocalFileToPut );
extern  __EXPORT_TYPE BOOL WINAPI DelFile2( char * serverName, char * szFileName );
extern  __EXPORT_TYPE BOOL WINAPI GetFileStat2( char * szHostName, char * szFileName, unsigned* pnSize, unsigned* pnTimeStamp );

extern  __EXPORT_TYPE BOOL WINAPI GetOxSox2DLLVer (int * DLLVersion );

extern  __EXPORT_TYPE void WINAPI AbortTransfer(void);
extern  __EXPORT_TYPE float WINAPI TransferProgress(void);

// Here we have the new additions for IS Explorer - Added by Roddy - Sept 2001

// First of all, a new updated directory function, returning file size, file date and attributes as well as filename
// This allows handling of cache files locally on the pc.
#define IS2_MAX_FILENAME 64
#define IS2_MAX_PATHNAME 128

typedef struct
   {
   char FileName[IS2_MAX_FILENAME];   // Actually, IS2 filenames are limited to 28 chars, but..
   UINT32 FileSize;
   UINT32 FileAttribs;
   UINT32 FileTimeStamp;  // Used for Unique IDs
   } V3DirEntry_, *PV3DirEntry_;

#define STILL	0x1
#define ANIM	0x2
#define ETEXT	0x4
#define CLOCK	0x8
#define AUDIO	0x10
#define FONT    0x20
#define PLAYLIST 0x40

typedef struct
{
  INT32 actual_width;
  INT32 actual_height;
  INT32 x_pos;
  INT32 y_pos;
  INT32 clip;
  INT32 gain;
  INT32 trans;
  INT32 key_src;
  INT32 key_mode;
  INT32 key_inv;
} Still_;

typedef struct
{
  Still_ still_info;
  INT32 num_frames;
  INT32 play_mode;	// 0 = Cycle, 1 = Single Shot
} Anim_;

typedef struct
{
  Still_ still_info;
  INT32 format;		// 0 = PAL, 1 = NTSC, 2 = HDLow, 3 = HDHigh, 4 = Userdef (shouldn't be there!)
  INT32 type;		// 0 = Normal, 1 = Crawl, 2 = Roll
} EasyText_;

typedef struct
{
  Still_ still_info;
  INT32 type;		// 0 = Analogue, 1 = Digital
} Clock_;

typedef struct
{
  INT32 bits_per_sample;
  INT32 samp_freq;
  INT32 size;		// In bytes
  INT32 format;		// 0 = Mono, 1 = Stereo
  INT32 looped;		// 0 = False, 1 = True
} Audio_;

typedef struct
{
  INT32 images;
} Playlist_;

typedef struct
{
  char Title[128];
  INT32 File_Type;	// As defined at top
  INT32 Associations;	// Bit mask using values defined at top...?
  INT32 UID;
  union
  {
    Still_ Still;
    Anim_ Anim;
    EasyText_ ET;
    Clock_ Clock;
    Audio_ Audio;
    Playlist_ Playlist;
  } File_Info;
  INT32 Ext_Size;
  INT32 Ext_Info;
} FileInfo_, *PFileInfo_;



#define THUMB_I565 1
#define THUMB_BMP  2

typedef struct
    {
    int w,h;
    int thumb_code;
    float aspect_required;
    char filename[IS2_MAX_FILENAME];
    } ThumbnailSpec_, *PThumbnailSpec_;

#pragma pack(push, 2)
typedef struct {
INT16 ImageFileType ;
INT32 FileSize ;
INT16 Reserved1 ;
INT16 Reserved2 ;
INT32 ImagedataOffset ;
} WIN3XHEAD;

typedef struct
{
WIN3XHEAD BmpHead;
BITMAPINFOHEADER InfoHead;
UINT16 data[1];
} bmp_file_, *Pbmp_file_;

#pragma pack(pop)
typedef struct
    {
    bmp_file_ bmp_file;
    } Thumbnail_, *PThumbnail_;

typedef struct
    {
    UINT32 SerialNumber;
    char SystemName[32];
    } SystemInfo_, *PSystemInfo_;

typedef struct
{
	unsigned long KBytes_Free;	// size of free space in KiB
	unsigned long KBytes_Total;	//  size of volume in KiB
} DiskInfo_, *PDiskInfo_;

// Unlike the other dir calls, this returns a pointer to an array of dire entries in PReturnedDirectory.
// This is malloc'd internally to the call.
// It's the applications job to FREE this when it's done with it.
// If the call fails, this param will be set to null.

// There is no 'current directory' version - a path must be passed in.
// Unlike the old calls, directories
// Like the other directory calls, it supports the $MACROS for specific directories.

extern __EXPORT_TYPE BOOL WINAPI GetFullDir(const char * szHostName,  const char * szPath,
        PV3DirEntry_ *pReturnedDirectory, int * nEntries );

extern __EXPORT_TYPE void WINAPI FreeOxSoxPtr(void* pMemory);

extern __EXPORT_TYPE BOOL WINAPI GetFileInfo(const char *szHostName,  const char * szPath,
        PFileInfo_ file_info);

extern __EXPORT_TYPE BOOL WINAPI GetThumbnail(const char *szHostName, const PThumbnailSpec_ thumb_spec,
        PThumbnail_ *thumb);

extern __EXPORT_TYPE BOOL WINAPI GetISInfo(const char * szHostName,  PSystemInfo_ sys_info);

extern __EXPORT_TYPE BOOL WINAPI GetDriveInfo(const char * szHostName, const char * szPath, PDiskInfo_ disk_info);

#ifdef __cplusplus
}
#endif

#endif




