// IS2Comm.h: interface for the CIS2Comm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IS2COMM_H__1C44F41F_A066_41DB_9A5D_5F9A6B85C716__INCLUDED_)
#define AFX_IS2COMM_H__1C44F41F_A066_41DB_9A5D_5F9A6B85C716__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../MSG/MessagingObject.h"


// defines here, no separate defs file

typedef             int         INT32;  //added by Kenji to support types in oxsox2.h (why omitted - unknown)
typedef unsigned    int        UINT32;  //added by Kenji to support types in oxsox2.h (why omitted - unknown)


#include <windows.h>
//#pragma comment(lib, "../OxSox/oxsox2.lib")  // lib not valid.
#include "OxSox/oxsoxtypes.h"


// destination type codes
#define OX_DESTTYPE_UNK				0x00  // undefined
#define OX_DESTTYPE_IS2				0x01  // Miranda imagestore 2
#define OX_DESTTYPE_INT				0x02  // Miranda intuition
#define OX_DESTTYPE_IS300			0x03  // Miranda imagestore 300.
#define OX_DESTTYPE_ISHD			0x04  // Miranda imagestore HD.


typedef BOOL (CALLBACK* LPFNOxSox2Entry)(HINSTANCE  hinstDLL,	// handle to DLL module
                           DWORD  fdwReason,	// reason for calling function
                           LPVOID  lpvReserved 	);// reserved

// returns TRUE for success, FALSE for error
typedef BOOL (CALLBACK* LPFNGetUnitNames)(PUNITSLIST pUnitsList,     // address of a units list
                  int * pNumUnits);                 // number of units in the list

// returns TRUE for success, FALSE for error
typedef BOOL (CALLBACK* LPFNGetDir)( const char * szHostName, PFILEDIR pReturnedDirectory, int * nEntries );
typedef BOOL (CALLBACK* LPFNGetNamedDir)( const char * szHostName, const char * szPath, PFILEDIR pReturnedDirectory, int * nEntries );
typedef BOOL (CALLBACK* LPFNGetFile)( char * szHostName, char * szHostFileToGet, char * szLocalFileName );
//typedef void (CALLBACK* LPFNCancelGetFile)(void);
typedef BOOL (CALLBACK* LPFNPutFile)( char * szHostName, char * szHostFileName, char * szLocalFileToPut );
typedef BOOL (CALLBACK* LPFNDelFile)( char * serverName, char * szFileName );
typedef BOOL (CALLBACK* LPFNPing)( char * serverName );
typedef int  (CALLBACK* LPFNChangeDir)( char * serverName, char * szNewDirectory );
typedef int  (CALLBACK* LPFNPWDir)( char * serverName, char * szReturnedPath );

typedef BOOL (CALLBACK* LPFNInitWinsock)(void);
typedef u_short (CALLBACK* LPFNReturnPortNumber)(void);
typedef BOOL (CALLBACK* LPFNIsMemberOfGroup) (char *grp);

// new stuff for OxSox2 05/06/1998
typedef int  (CALLBACK* LPFNGetTCPServerVer)( const char * serverName, char * szServerVersionString );

typedef BOOL (CALLBACK* LPFNGetNamedDir2)( const char * szHostName, const char * szDirectoryName, PFILEDIR pReturnedDirectory, int * nEntries );
typedef BOOL (CALLBACK* LPFNGetFile2)( char * szHostName, char * szHostFileToGet, char * szLocalFileName );
typedef BOOL (CALLBACK* LPFNPutFile2)( char * szHostName, char * szHostFileName, char * szLocalFileToPut );
typedef BOOL (CALLBACK* LPFNDelFile2)( char * serverName, char * szFileName );
typedef BOOL (CALLBACK* LPFNGetFileStat2)( char * szHostName, char * szFileName, unsigned* pnSize, unsigned* pnTimeStamp );
typedef BOOL (CALLBACK* LPFNGetOxSox2DLLVer) (int * DLLVersion );
typedef void (CALLBACK* LPFNAbortTransfer)(void);
typedef float (CALLBACK* LPFNTransferProgress)(void);

typedef BOOL (CALLBACK* LPFNGetFullDir)(const char * szHostName,  const char * szPath, PV3DirEntry_ *pReturnedDirectory, int * nEntries );
typedef BOOL (CALLBACK* LPFNGetFileInfo)(const char *szHostName,  const char * szPath, PFileInfo_ file_info);
typedef BOOL (CALLBACK* LPFNGetThumbnail)(const char *szHostName, const PThumbnailSpec_ thumb_spec, PThumbnail_ *thumb);
typedef BOOL (CALLBACK* LPFNGetISInfo)(const char * szHostName,  PSystemInfo_ sys_info);
typedef BOOL (CALLBACK* LPFNGetDriveInfo)(const char * szHostName, const char * szPath, PDiskInfo_ disk_info);
typedef void (CALLBACK* LPFNFreeOxSoxPtr)(void* pMemory);


#define OX_SUCCESS				0
#define OX_ERROR					-1
#define OX_NULLHOST				-2
#define OX_NODLL					-3
#define OX_INPROGRESS			-4
#define OX_BADPARAM				-5
#define OX_NOALLOC				-6
#define OX_NOTHREAD				-7
#define OX_FILEERROR			-8

#define OX_ALREADYLOADED	0x01
#define OX_ALREADYINIT		0x02
#define OX_NOPING					0x04
#define OX_NOVER					0x08
#define OX_NOFILE					0x10
#define OX_NODIR					0x20
#define OX_EXCEPTION			0x40

#define OX_GET				0
#define OX_PUT				1
#define OX_KILL				-1


//flags
#define OX_NONE				0x00  // used in every
#define OX_ALL				0x01  // OxSoxGetDriveInfo
#define OX_DEL				0x01  // TransferFile
#define OX_KILLALL		0x01  // AbortTransfer
#define OX_QUEUE			0x02  // TransferFile
#define OX_REPORT			0x04  // used in many
#define OX_FILEINFO				0x01  // OxSoxGetFileInfo
#define OX_DIRENTRYINFO		0x02  // OxSoxGetFileInfo

//function enum
#define OX_FUNC_OxSox2Entry					0x0000001
#define OX_FUNC_GetUnitNames				0x0000002
#define OX_FUNC_GetDir							0x0000004
#define OX_FUNC_GetNamedDir					0x0000008
#define OX_FUNC_GetFile							0x0000010
#define OX_FUNC_PutFile							0x0000020
#define OX_FUNC_DelFile							0x0000040
#define OX_FUNC_Ping								0x0000080
#define OX_FUNC_ChangeDir						0x0000100
#define OX_FUNC_PWDir								0x0000200
#define OX_FUNC_InitWinsock					0x0000400
#define OX_FUNC_ReturnPortNumber		0x0000800
#define OX_FUNC_IsMemberOfGroup			0x0001000
#define OX_FUNC_GetTCPServerVer			0x0002000
#define OX_FUNC_GetNamedDir2				0x0004000
#define OX_FUNC_GetFile2						0x0008000
#define OX_FUNC_PutFile2						0x0010000
#define OX_FUNC_DelFile2						0x0020000
#define OX_FUNC_GetOxSox2DLLVer			0x0040000
#define OX_FUNC_AbortTransfer				0x0080000
#define OX_FUNC_TransferProgress		0x0100000
#define OX_FUNC_GetFullDir					0x0200000
#define OX_FUNC_GetFileInfo					0x0400000
#define OX_FUNC_GetThumbnail				0x0800000
#define OX_FUNC_GetISInfo						0x1000000
#define OX_FUNC_GetDriveInfo				0x2000000
#define OX_FUNC_GetFileStat2				0x4000000
#define OX_FUNC_FreeOxSoxPtr				0x8000000
#define OX_FUNC_MAX									0x8000000

typedef struct 
{
	void*		pObj;
	char*		pszTransferSource;
	char*		pszTransferDest;
	int			nTransferID;
	int			nTransferDir;
	unsigned long	ulFlags;
} ThreadInfo_t;



class CIS2Comm : public CMessagingObject
{
public:
	CIS2Comm();
	virtual ~CIS2Comm();

// main
	char* m_pszHost;
	int SetHost(char* pszHost);

	char* m_pszOxSoxFilename;
	
// oxsox, notes in comments at end of file
	HINSTANCE m_hinstDLL;
	bool	m_bDLLWinsockInit;
	bool	m_bTransferring;
	int		m_nDLLVersion;
	unsigned long m_ulLoadTime;

	int			m_nTransferIDCurrent;
	int			m_nTransferIDLast;
	bool		m_bTransferSuppress;
	bool		m_bTransferError;
	bool		m_bTransferAborted;
	bool		m_bScheduledForRestart;
	ThreadInfo_t* m_pTransferError;

	LPFNOxSox2Entry				m_lpfnOxSox2Entry;
	LPFNGetUnitNames			m_lpfnGetUnitNames;
	LPFNGetDir						m_lpfnGetDir;
	LPFNGetNamedDir				m_lpfnGetNamedDir;
	LPFNGetFile						m_lpfnGetFile;
//	LPFNCancelGetFile			m_lpfnCancelGetFile;
	LPFNPutFile						m_lpfnPutFile;
	LPFNDelFile						m_lpfnDelFile;
	LPFNPing							m_lpfnPing;
	LPFNChangeDir					m_lpfnChangeDir;
	LPFNPWDir							m_lpfnPWDir;
	LPFNInitWinsock				m_lpfnInitWinsock;
	LPFNReturnPortNumber	m_lpfnReturnPortNumber;
	LPFNIsMemberOfGroup		m_lpfnIsMemberOfGroup;
	LPFNGetTCPServerVer		m_lpfnGetTCPServerVer;
	LPFNGetNamedDir2			m_lpfnGetNamedDir2;
	LPFNGetFile2					m_lpfnGetFile2;
	LPFNPutFile2					m_lpfnPutFile2;
	LPFNDelFile2					m_lpfnDelFile2;
	LPFNGetOxSox2DLLVer		m_lpfnGetOxSox2DLLVer;
	LPFNAbortTransfer			m_lpfnAbortTransfer;
	LPFNTransferProgress	m_lpfnTransferProgress;
	LPFNGetFullDir				m_lpfnGetFullDir;
	LPFNGetFileInfo				m_lpfnGetFileInfo;
	LPFNGetThumbnail			m_lpfnGetThumbnail;
	LPFNGetISInfo					m_lpfnGetISInfo;
	LPFNGetDriveInfo			m_lpfnGetDriveInfo;
	LPFNGetFileStat2			m_lpfnGetFileStat2;
	LPFNFreeOxSoxPtr			m_lpfnFreeOxSoxPtr;
	

// oxsox
	int OxSoxLoad(char* pszOxSoxFilename=NULL);
	int OxSoxFree();
	int OxSoxInit();
	int OxSoxPing();
	int OxSoxPutFile(char* pszSourcePath, char* pszDestFile, char* pszDirAlias=NULL, unsigned long ulFlags=OX_NONE);
	int OxSoxGetFile(char* pszLocalPath, char* pszSourceFile, char* pszDirAlias=NULL, unsigned long ulFlags=OX_NONE);
	int OxSoxDelFile(char* pszSourceFile, char* pszDirAlias=NULL, unsigned long ulFlags=OX_NONE);
	int OxSoxGetFileInfo(char* pszSourceFile, char* pszDirAlias=NULL, FileInfo_* pfile_info=NULL, V3DirEntry_* pv3DirEntry=NULL, unsigned long ulFlags=OX_DIRENTRYINFO|OX_FILEINFO);
	int OxSoxGetFileStats(char* pszSourceFile, char* pszDirAlias=NULL, unsigned* puiSize=NULL, unsigned* puiTimestamp=NULL );
	int OxSoxGetDirInfo(char* pszDirAlias, PV3DirEntry_* ppv3DirEntry, int* pnNumEntries, unsigned long ulFlags=OX_NONE);
	int OxSoxGetDriveInfo(char* pszDirAlias, PDiskInfo_ pdisk_info, unsigned long ulFlags=OX_NONE);

	int OxSoxAbortTransfer(unsigned long ulFlags=OX_NONE);
	int OxSoxTransferProgress(float* pfProgress, unsigned long ulFlags=OX_NONE);

	// Miranda utilities
	int UtilParseTem(char* pszSourceFile, char*** pppszChildren, int* pnNumChildren);
	int UtilParseRemoteTem(char* pszSourceFile, char* pszDirAlias, char*** pppszChildren, int* pnNumChildren, char* pszTempFileName=NULL, bool bDeleteFile=false);

	CRITICAL_SECTION m_critIS2;  // critsec access to object

// automation protocol
};


// A Hive is needed because Oxsox DLL has problems with multithreaded operations.
// the idea is to create a hive of DLL instances based on the base DLL.
// The base DLL is copied N number of times, and each of those DLLs are loaded individually.
// Then access to the hive is requested, and the first free DLL in a round-robin seaech will
// be returned to the requester, or none if the whole hive is busy.
// This means we can run many operations simultaneously, while treating each DLL as a single-threaded processor.
class CIS2Hive : public CMessagingObject
{
public:
	CIS2Hive();
	virtual ~CIS2Hive();

	// the objects in the hive.
	CIS2Comm** m_ppIs2Comm;
	int m_nNumObjects;
	CRITICAL_SECTION m_critHive;  // critsec access to hive

	// hive construction:
	int InitializeHive(char* pszBaseDLL, int nNumHiveObjects); // copies files and loads them in.
	int DeInitializeHive(); // unloads all libraries
	int AccessHive(int nRequiredIndex=-1);  // returns an index to the next free object, or an error code (neg value)
	int CheckHostInHive(char* pszServer);  // checks if a particular host is transferring, so we can disallow new transfers at the same time, from a different hive member.

	// some settings:
	bool m_bReloadLibraries; // if true (default), will reload the DLLs at staggered intervals (based on a fraction of the reload interval)
	int m_nReloadInterval; // in seconds, defaults to 1 day.  If there are 7 objects in the hive, each gets reloaded once per day, each 1/7 of a day after another in sequence, until they are all reset within 1 day.
	int m_nReloadCheckInterval; // in seconds, interval on which to check if the reload interval has elapsed.

	// some internal variables
	bool m_bHiveInitialized; 
	char* m_pszBaseDLL;

	unsigned long m_ulLoadTime;

	bool m_bHiveMaintenanceKill; 
	bool m_bHiveMaintenanceStarted; 

	int m_nIndex;  // last accessed index.
	int m_nCheckIndex;
};

#endif // !defined(AFX_IS2COMM_H__1C44F41F_A066_41DB_9A5D_5F9A6B85C716__INCLUDED_)


/*

	from Nick Holland, nick.holland@oxtel.com

I've created a new version (2.30) of the OxSox2.dll which is available from
www.imagestore2.com (just click on the OxSox button). 

1) I've commented out CancelGetFile from the OxSox header.
2) a) b) Here's a bit of history...

A long time ago we made a product called Imagestore 1, which you can easily
tell by a white front. It ran something called OxSox server 1. Our newer
products such as Imagestore 2/3/300/HD and Intuition run OxSox server 2
(slightly modified protocol). 

The OxSox.dll as you know has commands called GetFile,PutFile,DelFile.
These functions auto detect what type of server you're connected to, then
call the associated command. For example:
PutFile calls PutFile1 if you're connected to an Imagestore 1, or PutFile2
if you're connected to an Imagestore 2.

The Imagestore 1 did not use file paths such as $VIDEO, so for backwards
compatibility, if you call PutFile it does something like the following:
PutFile (filename)
If (server==1)
	PutFile1(filename)
If (server==2)
	AddFilePathToName... So filename becomes $VIDEO/filename (if it's a
graphic file) or $AUDIO (if it's a audio file).
	PutFile2(filename)

What it means in English is if you called PutFile("$VIDEO/airwolf.oxt") and
you're connected to an Imagestore2 it will try to do
PutFile2("$VIDEO/$VIDEO/airwolf.oxt") which will fail. 

In summary, just use the commands ending in 2 directly, e.g. PutFile2,
GetFile2 and you'll be fine. We need to correct this in the OxSox
documentation. 

2 c) I've exposed DelFile2 now so you can use it. 

3) I would recommend using GetNamedDir2.

4, a) GetFileInfo. Ext_Size and Ext_Info are unused. The best way to get the
file size is indeed to use GetFullDir. For some of the formats you can
calculate a rough guess of the file size, e.g. for a logo which has 4 bytes
per pixel you can work out its size using the width and height fields.

5) The Imagestore 2/3 has 4 partitions which are: software, thumbnails,
video files (including fonts), audio files.  They are
$CODE,$THUMBS,$VIDEO,$AUDIO respectively. Any other alias will be contained
within one of those four.  The Intuition has just one partition, so yes it
is a global value. 

6) Undocumented export functions. 
There's a lot of legacy code in the dll so I would try an ignore most of it.
If you scan through the Oxsox source code you can get a rough idea of what
certain functions do. Here's the few you pointed out:
IsMemberOfGroup. Does nothing.
GetUnitNames, I think this gets the names of units held in units.txt on your
PC.
GetTCPServerVer, gets the version of the OxSox server.

7) The thumbnail transferred is just an uncompressed bitmap. There is some
example code in the OxSox manual. What I might try and do is knock up a
small test application which gets a thumbnail from an Imagestore as a
working example. 
Thumbnails are generated dynamically, either from the file itself, or an
embedded thumbnail within the file.


*/