//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EasyText.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_EASYTEXT_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_CONNECT              1001
#define IDC_EDIT_COMMAND                1002
#define IDC_BUTTON_SEND                 1003
#define IDC_EDIT_ESCIN                  1004
#define IDC_EDIT_ESCOUT                 1005
#define IDC_COMBO_HOST                  1006
#define IDC_BUTTON_ESC                  1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
