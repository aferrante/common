// EasyTextDlg.h : header file
//

#if !defined(AFX_EASYTEXTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
#define AFX_EASYTEXTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../IS2Core.h"

/////////////////////////////////////////////////////////////////////////////
// CEasyTextDlg dialog

class CEasyTextDlg : public CDialog
{
// Construction
public:
	CEasyTextDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CEasyTextDlg)
	enum { IDD = IDD_EASYTEXT_DIALOG };
	CString	m_szCmd;
	CString	m_szHost;
	CString	m_szEscIn;
	CString	m_szEscOut;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEasyTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL


	CIS2Core m_is2;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CEasyTextDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonSend();
	afx_msg void OnDestroy();
	afx_msg void OnButtonEsc();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EASYTEXTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
