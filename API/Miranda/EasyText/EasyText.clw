; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CEasyTextDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "EasyText.h"

ClassCount=3
Class1=CEasyTextApp
Class2=CEasyTextDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_EASYTEXT_DIALOG

[CLS:CEasyTextApp]
Type=0
HeaderFile=EasyText.h
ImplementationFile=EasyText.cpp
Filter=N

[CLS:CEasyTextDlg]
Type=0
HeaderFile=EasyTextDlg.h
ImplementationFile=EasyTextDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDCANCEL

[CLS:CAboutDlg]
Type=0
HeaderFile=EasyTextDlg.h
ImplementationFile=EasyTextDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_EASYTEXT_DIALOG]
Type=1
Class=CEasyTextDlg
ControlCount=12
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_CONNECT,button,1342242816
Control4=IDC_EDIT_COMMAND,edit,1350631552
Control5=IDC_BUTTON_SEND,button,1342242816
Control6=IDC_COMBO_HOST,combobox,1344339970
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT_ESCIN,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT_ESCOUT,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_BUTTON_ESC,button,1342242816

