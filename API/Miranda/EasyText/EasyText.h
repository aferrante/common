// EasyText.h : main header file for the EASYTEXT application
//

#if !defined(AFX_EASYTEXT_H__EBDE3D6F_9965_44C3_BC3E_19CE3BCF2D79__INCLUDED_)
#define AFX_EASYTEXT_H__EBDE3D6F_9965_44C3_BC3E_19CE3BCF2D79__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CEasyTextApp:
// See EasyText.cpp for the implementation of this class
//

class CEasyTextApp : public CWinApp
{
public:
	CEasyTextApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEasyTextApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CEasyTextApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EASYTEXT_H__EBDE3D6F_9965_44C3_BC3E_19CE3BCF2D79__INCLUDED_)
