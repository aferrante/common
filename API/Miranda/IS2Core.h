// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.

// IS2Core.h

#ifndef IMAGESTORE2_CORE_INCLUDED
#define IMAGESTORE2_CORE_INCLUDED

#include "../../MSG/MessagingObject.h"
#include "../../TTY/Serial.h"
#include "../../LAN/NetUtil.h"

#define IS2_ERRORSTRING_LEN 512

#define IS2_CONN_EXISTING 0  // use existing connection, create new if none
#define IS2_CONN_SEPARATE	1  // close and make new each time

// triggerOxtel commands
#define IMAGESTORE2_SETSQUEEZE 0
#define IMAGESTORE2_DOSQUEEZEIN 1
#define IMAGESTORE2_DOSQUEEZEOUT 2
#define IMAGESTORE2_FADEIN 3
#define IMAGESTORE2_FADEOUT 4
#define IMAGESTORE2_SETFADE 5

// OX_Ack replies
#define SEND_ERROR -1
#define REPLY_OK 0
#define REPLY_TIMEOUT 1
#define REPLY_MISMATCHED 2
#define REPLY_NAK 3

// for logging
#ifndef LAST_STATUS_SIZE
#define LAST_STATUS_SIZE 25
#endif

// layers
#define IMAGESTORE2_LAYER_PREVIEW 0 //0 represents preview/midground, 
#define IMAGESTORE2_LAYER_PROGRAM 1 //1 represents programme/foreground. 


// for easytext
#define IMAGESTORE2_MAX_ET_LENGTH 250
#define IMAGESTORE2_MAX_MSGBUFFER 4096

#define IMAGESTORE2_ET_RENDER 1
#define IMAGESTORE2_ET_APPEND 2

#define PRESET_FULL     0  // this is the default squeeze: per 
                           // the manual: Up to 255 presets are available
                           // (0x1..0xFF). Preset 0 cannot be overwritten, 
                           // and is always the nominal ‘unsqueezed’ image.
#define PRESET_SQUEEZED 1  // This is the preset we use for squeezes

#define IS2CORE_ERRORCONN -2
#define IS2CORE_ERROR -1
#define IS2CORE_SUCCESS 0
#define IS2CORE_CORRECTED 1


class CIS2Core : public CMessagingObject
{
public:
  CIS2Core();   // standard constructor
  ~CIS2Core();   // standard destructor
  
	int m_nTxRxDwellMS;
	int m_nTxTxPauseMS;
	int m_nTxRetries;
	int m_nMode;

private:
  float m_OxtelX, m_OxtelY, m_OxtelW, m_OxtelH;

  bool m_bUseEthernet;

	CNetUtil  m_net;
	CSerial		m_serial;	


public:

  int   m_nCOMport;
  DWORD m_dwBaudRate;
  
  char   m_szEthernetAddress[256];
  int    m_nEthernetPort;
  SOCKET m_socket;

  char   m_szType[256];


	int PublicSendCommand(char* pszError, int nMode, int* pnReturn, char* pszCommand, ... );
	char* EscapeSpecialChars(char* buffer);


  // comms
  int SetupEthernetConnection(char* pszIP, int nPort=5006);
  int SetupSerialConnection(int nCOMport, DWORD nBaud);
  int OpenConnection(char* pszError=NULL);
  int CloseConnection();
  int GetComPort();
  int SetComPort(int nPort);
  int RelinquishCOMPort();
  int SetBaudRate(DWORD nBaud);
  DWORD GetBaudRate();


  // squeeze
  int GetSqueezebackParams(float* px, float* py, float* pw, float* ph);
  int SetupSqueezebackParams(float x, float y, float w, float h);
  int SqueezySetPreset(int preset, float xpos, float ypos, float xsize, float ysize, char* pszError=NULL);
  int SqueezySetPreset(int preset, char* pszError=NULL);
  int SqueezySetSqueezeConfig(int setting, char* pszError=NULL);
  int SqueezyRunSqueeze(int presetNumber, int nDurationInFields, char* pszError=NULL);
  int SqueezySetMoveProfile(int param, char* pszError=NULL);
  int SqueezySelectBorders(int preset, float left, float right, float top, float bottom, char* pszError=NULL);

  // faders
  int SetFader(int nLayer, int nDurationInFields, char* pszError=NULL);
  int TriggerFader(int nLayer, bool bFadeIn, char* pszError=NULL);

/*
  // EasyText Support
  int EasyText_UpdateTextBox(         int nLayer, int nTextBoxNum, char* pszText, char* pszError=NULL);
  int EasyText_ChangeDims(            int nLayer, int nTextBoxNum, int nX, int nY, int nWidth, int nHeight, char* pszError=NULL);
  int EasyText_RenderBox(             int nLayer, int nTextBoxNum, char* pszError=NULL);
  int EasyText_UpdateBoxImage(        int nLayer, int nTextBoxNum, char* pszImageFilename, char* pszError=NULL);
  int EasyText_ChangeFont(            int nLayer, int nTextBoxNum, int nSize, int nColorR, int nColorG, int nColorB, char* pszFontFilename, char* pszError=NULL);
  int EasyText_ChangeTextTransparency(int nLayer, int nTextBoxNum, int nTransparency, char* pszError=NULL);
  int EasyText_SetDropShadow(         int nLayer, int nTextBoxNum, int Xoffset, int Yoffset, int nTransparency, int nColorR,int nColorG,int nColorB, char* pszError=NULL);
  int EasyText_SetTextTracking(       int nLayer, int nTextBoxNum, int nTracking, char* pszError=NULL);
  int EasyText_SetTextAlignment(      int nLayer, int nTextBoxNum, int nHAlign, int nVAlign, char* pszError=NULL);
  int EasyText_SetTextWrapping(       int nLayer, int nTextBoxNum, int nWrapMode, char* pszError=NULL);
  int EasyText_TextBgnd_Clear(        int nLayer, int nTextBoxNum, char* pszError=NULL);
  int EasyText_TextBgnd_Matte(        int nLayer, int nTextBoxNum, int nColorR, int nColorG, int nColorB, int nTransparency, int nHBorder, int nVBorder, char* pszError=NULL);
  int EasyText_TextBgnd_Gradient(     int nLayer, int nTextBoxNum, int nSourceColR, int nSourceColG, int nSourceColB, int nDestColR, int nDestColG, int nDestColB, int nBorderColR, int nBorderColG, int nBorderColB, int nTransparency, int nBorderWidth, int nDirection, int nHBorder, int nVBorder, char* pszError=NULL);
  int EasyText_ChangeTextBgnd(        int nLayer, int nColorR, int nColorG, int nColorB, int nTransparency, int nMode, int nEdgeSize, char* pszError=NULL);
  int EasyText_StartStrap(            int nLayer, int nStart, char* pszError=NULL);
  int EasyText_SetStrapSpeed(         int nLayer, int nSpeed, char* pszError=NULL);
*/

private:
  // sending commands
  // int TriggerSqueezeback(int duration, int command, char* pszError=NULL);
  int SendCommand(char* pszError, int nMode, char* pszCommand, ... );
  int ReceiveAck(int* pnReturnCode);
  int rem_send_char(char ch);
  int remote_send(char* pszFormat, ...);
  long oxfix(float f);
  void do_crc( unsigned char ch, unsigned short* crcptr );
};


#endif
