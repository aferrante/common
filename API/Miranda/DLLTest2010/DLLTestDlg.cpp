// DLLTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DLLTest.h"
#include "DLLTestDlg.h"
#include <Psapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/////////////////////////////////////////////////////////////////////////////
// CDLLTestDlg dialog

CDLLTestDlg::CDLLTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLLTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDLLTestDlg)
	m_szDLL = _T("Oxsox2.dll");
	m_szMem = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hinstDLL = NULL;
}

void CDLLTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDLLTestDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szDLL);
	DDX_Text(pDX, IDC_STATIC_MEM, m_szMem);
}	//}}AFX_DATA_MAP


BEGIN_MESSAGE_MAP(CDLLTestDlg, CDialog)
	//{{AFX_MSG_MAP(CDLLTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDLLTestDlg message handlers

BOOL CDLLTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

/*
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
*/
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	GetMem();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDLLTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
//		CAboutDlg dlgAbout;
//		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDLLTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDLLTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDLLTestDlg::OnButtonLoad() 
{
	// TODO: Add your control notification handler code here

	if(m_hinstDLL)
	{
		FreeLibrary(m_hinstDLL);
		m_hinstDLL = NULL;
		GetDlgItem(IDC_BUTTON_LOAD)->SetWindowText("Load");

	}
	else
	{
		if(UpdateData(TRUE))
		{
			if(m_szDLL.GetLength()<5)
			{
				AfxMessageBox("Enter a filename of at least 5 characters in length, ending in \".dll\"");
			}
			else
			{
				m_hinstDLL = LoadLibrary(m_szDLL);
				if(m_hinstDLL)
				{
					GetDlgItem(IDC_BUTTON_LOAD)->SetWindowText("Unload");
				}
				else
				{
					CString x; x.Format("Failed to load %s", m_szDLL);
					AfxMessageBox(x);
				}
			}
		}
	}

	GetMem();

}


void CDLLTestDlg::GetMem()
{
	PROCESS_MEMORY_COUNTERS_EX psmemCounters;
 	GetProcessMemoryInfo(GetCurrentProcess(), (PPROCESS_MEMORY_COUNTERS)&psmemCounters, sizeof(psmemCounters));

	m_szMem.Format("mem usage: %d bytes (%d private)", psmemCounters.WorkingSetSize, psmemCounters.PrivateUsage);
	GetDlgItem(IDC_STATIC_MEM)->SetWindowText(m_szMem);
}