// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.


#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <direct.h>  // directory routines
#include <sys/timeb.h>
#include <time.h>

#include "IS2Core.h"

CIS2Core::CIS2Core()
{
  m_dwBaudRate = CBR_19200;
  m_nCOMport = 1;
  m_bUseEthernet = true;
  m_nEthernetPort = 5006;
  strcpy(m_szEthernetAddress,"");
  strcpy(m_szType,"Imagestore");
	m_socket = NULL;
	
	m_nTxRxDwellMS=150;
	m_nTxTxPauseMS=150;
	m_nTxRetries=4;
	m_nMode = IS2_CONN_EXISTING;
	m_net.Initialize();
}

CIS2Core::~CIS2Core()
{
}


// Note these codes bear no relation to the ASCII defined codes with similar names.
#define STX0 0x002
#define STX1 0x003
#define ACK0 0x004
#define ACK1 0x005
#define NAK  0x007
char g_chStx = STX0;


const unsigned int lstab[] =
{
  0x0000, 0xc0c1, 0xc181, 0x0140,
    0xc301, 0x03c0, 0x0280, 0xc241,
    0xc601, 0x06c0, 0x0780, 0xc741,
    0x0500, 0xc5c1, 0xc481, 0x0440
};

const unsigned int mstab[] =
{
  0x0000, 0xcc01, 0xd801, 0x1400,
    0xf001, 0x3c00, 0x2800, 0xe401,
    0xa001, 0x6c00, 0x7800, 0xb401,
    0x5000, 0x9c01, 0x8801, 0x4400
};




/////////////////////////////////////////////////////////////////////////////
// Public Methods Implementation



/////////////////////////////////////////////////////////////////////////////
// Connection settings

int CIS2Core::SetupEthernetConnection(char* pszIP, int nPort)
{
	if((pszIP)&&(strlen(pszIP)))
	{
		if(m_serial.m_bConnected) m_serial.CloseConnection();
		m_nEthernetPort = nPort;
		strcpy(m_szEthernetAddress, pszIP);
	  m_bUseEthernet = true;
		return IS2CORE_SUCCESS;
	}
  return IS2CORE_ERROR;
}

int CIS2Core::SetupSerialConnection(int nCOMport, DWORD nBaud)
{
	if(SetBaudRate(nBaud)>=IS2CORE_SUCCESS)
	{
		if(m_socket) { m_net.CloseConnection(m_socket); m_socket=NULL; }
		m_bUseEthernet = false;
		return SetComPort(nCOMport);
	}
  return IS2CORE_ERROR;
}

int CIS2Core::SetBaudRate(DWORD nBaud)
{
  // verify baud rate
  if(
			(nBaud == CBR_9600)
		||(nBaud == CBR_19200)
		||(nBaud == CBR_38400)
		||(nBaud == CBR_57600)
		||(nBaud == CBR_115200)
		)
  {
		m_dwBaudRate = nBaud;
		return IS2CORE_SUCCESS;
  }
  return IS2CORE_ERROR;
}

DWORD CIS2Core::GetBaudRate()
{
  return m_dwBaudRate;
}

int CIS2Core::SetComPort(int nPort)
{
	int nRV = IS2CORE_SUCCESS;
  if (nPort < 1) { nPort = 1; nRV = IS2CORE_CORRECTED; }
  if (nPort > 4) {nPort = 4; nRV = IS2CORE_CORRECTED; }
  m_nCOMport = nPort;
	return nRV;
}

int CIS2Core::GetComPort()
{
  return m_nCOMport;
}

int CIS2Core::RelinquishCOMPort()
{
  m_serial.CloseConnection();
	return IS2CORE_SUCCESS;
}


int CIS2Core::OpenConnection(char* pszError)
{
  if(pszError) strcpy(pszError, "");
  if (m_bUseEthernet)
  {
    if(m_net.OpenConnection(m_szEthernetAddress, m_nEthernetPort, &m_socket, 5000, 5000, pszError)<NET_SUCCESS)
		{
			return IS2CORE_ERROR;
		}
  }
  else
  {
    if(m_serial.OpenConnection(m_nCOMport, m_dwBaudRate, 8, ONESTOPBIT, NOPARITY)<SERIAL_SUCCESS)
    {
      if(pszError) _snprintf(pszError, IS2_ERRORSTRING_LEN-1, "Error connecting to %s on COM %d at %d.", m_szType, m_nCOMport, m_dwBaudRate);
      return IS2CORE_ERROR;
    }
  }
	return IS2CORE_SUCCESS;
}

int CIS2Core::CloseConnection()
{
  if(m_bUseEthernet)
  {
    m_net.CloseConnection(m_socket);  // never fails
		m_socket=NULL; 
		return IS2CORE_SUCCESS;
  }
  else
  {
    return RelinquishCOMPort();
  }
}



////////////////////////////////////////////////////////////////////
//  Squeeze driver

int CIS2Core::GetSqueezebackParams(float* px, float* py, float* pw, float* ph)
{
	if((px)&&(py)&&(pw)&&(ph))
	{
		*px=m_OxtelX;
		*py=m_OxtelY;
		*pw=m_OxtelW;
		*ph=m_OxtelH;
		return IS2CORE_SUCCESS;
	}
	return IS2CORE_ERROR;
}

int CIS2Core::SetupSqueezebackParams(float x, float y, float w, float h)
{
  if ((x < 0.0) || (x > 1.0)) { /*AfxMessageBox("Oxtel X position out of bounds!");*/ return IS2CORE_ERROR; }
  if ((y < 0.0) || (y > 1.0)) { /*AfxMessageBox("Oxtel Y position out of bounds!");*/ return IS2CORE_ERROR; }
  if ((w < 0.0) || (w > 1.0)) { /*AfxMessageBox("Oxtel W position out of bounds!");*/ return IS2CORE_ERROR; }
  if ((h < 0.0) || (h > 1.0)) { /*AfxMessageBox("Oxtel H position out of bounds!");*/ return IS2CORE_ERROR; }

  m_OxtelX = x;
  m_OxtelY = y;
  m_OxtelW = w;
  m_OxtelH = h;
  return IS2CORE_SUCCESS;
}



/////////////////////////////////////////////////////////////////////////////
//
//  Generic command retry  
//
//   SendImageStore2Command(rem_send(FormatCommand());

int CIS2Core::PublicSendCommand(char* pszError, int nMode, int* pnReturn, char* pszCommand, ... )
{
  int nSucceeded=IS2CORE_ERROR; 
  int nCount, nReturnCode=-256;
  
	if(nMode == IS2_CONN_EXISTING)
	{
		if(m_bUseEthernet)
		{
//			AfxMessageBox("using ethernet on existing");
			if(m_socket==NULL)
			{
//			AfxMessageBox("socket null");
				if (OpenConnection(pszError)<IS2CORE_SUCCESS)
				{
//			AfxMessageBox("socket not open");

					return IS2CORE_ERRORCONN;
				}
			}
//			else 			AfxMessageBox("socket not null");

		}
		else
		{
			if(!m_serial.m_bConnected)
			{
				if (OpenConnection(pszError)<IS2CORE_SUCCESS) return IS2CORE_ERRORCONN;
			}
		}
	}
	else
	if(nMode == IS2_CONN_SEPARATE)
	{
		CloseConnection();
		if (OpenConnection(pszError)<IS2CORE_SUCCESS) return IS2CORE_ERRORCONN;
	}
	else
	{
//			AfxMessageBox("unknown mode");
		return IS2CORE_ERROR;
	}

  nCount=0;
  //AfxMessageBox((g_chStx == STX0)?"Trying STX0":"Trying STX1");
//			AfxMessageBox("about to try stuff...");

  while ((nSucceeded<IS2CORE_SUCCESS) && (nCount<m_nTxRetries))
  {
    if(remote_send(pszCommand) != SOCKET_ERROR)
		{
 			// Wait a moment...
			Sleep(m_nTxRxDwellMS);

	//			AfxMessageBox("ReceiveAck");

			nSucceeded = ReceiveAck(&nReturnCode);
			if(pnReturn) *pnReturn = nReturnCode;

			if (nSucceeded<IS2CORE_SUCCESS) // keep trying with same STX
			{
	 //			AfxMessageBox("ReceiveAck error");
			 if (g_chStx == STX0) g_chStx = STX1;
				else g_chStx = STX0;

				Sleep(m_nTxTxPauseMS); //wait again, unit might just be busy, 
				// dont pepper it with rapid commands
			}
		}
		else
		{
			nReturnCode = SEND_ERROR;
			if(pnReturn) *pnReturn = nReturnCode;
			if (pszError)
			{
				_snprintf(pszError, IS2_ERRORSTRING_LEN-1, "Socket error in SendCommand after %d retries.\nCommand: %s\nReturn code: %d.", nCount, pszCommand, nReturnCode);
			}
			// for the retry
			CloseConnection();
			if (OpenConnection(pszError)<IS2CORE_SUCCESS)
			{
				if (pszError)
				{
					_snprintf(pszError, IS2_ERRORSTRING_LEN-1, "SendCommand failed due to socket error after %d retries.\nCommand: %s\nReturn code: %d.", nCount, pszCommand, nReturnCode);

			//		AfxMessageBox(pszError);
				}
				return IS2CORE_ERRORCONN;
			}

		}
//		else AfxMessageBox("ReceiveAck success");
    nCount++;
  }

  if ((nSucceeded<IS2CORE_SUCCESS)&&(pszError))
	{
		_snprintf(pszError, IS2_ERRORSTRING_LEN-1, "SendCommand failed after %d retries.\nCommand: %s\nReturn code: %d.", nCount, pszCommand, nReturnCode);

//		AfxMessageBox(pszError);
	}
  
	if(nMode == IS2_CONN_SEPARATE)
	{
		CloseConnection();
	}

  return nSucceeded;

}


int CIS2Core::SendCommand(char* pszError, int nMode, char* pszCommand, ... )
{
  int nSucceeded=IS2CORE_ERROR; 
  int nCount, nReturnCode=-256;
  
	if(nMode == IS2_CONN_EXISTING)
	{
		if(m_bUseEthernet)
		{
			if(m_socket==NULL)
			{
				if (OpenConnection(pszError)<IS2CORE_ERROR) return IS2CORE_ERRORCONN;
			}
		}
		else
		{
			if(!m_serial.m_bConnected)
			{
				if (OpenConnection(pszError)<IS2CORE_ERROR) return IS2CORE_ERRORCONN;
			}
		}
	}
	else
	if(nMode == IS2_CONN_SEPARATE)
	{
		CloseConnection();
		if (OpenConnection(pszError)<IS2CORE_ERROR) return IS2CORE_ERRORCONN;
	}
	else return IS2CORE_ERROR;

  nCount=0;
  //AfxMessageBox((g_chStx == STX0)?"Trying STX0":"Trying STX1");

  while ((nSucceeded<IS2CORE_SUCCESS) && (nCount<m_nTxRetries))
  {
    if(remote_send(pszCommand) != SOCKET_ERROR)
		{
 			// Wait a moment...
			Sleep(m_nTxRxDwellMS);

	//			AfxMessageBox("ReceiveAck");

			nSucceeded = ReceiveAck(&nReturnCode);

			if (nSucceeded<IS2CORE_SUCCESS) // keep trying with same STX
			{
	 //			AfxMessageBox("ReceiveAck error");
			 if (g_chStx == STX0) g_chStx = STX1;
				else g_chStx = STX0;

				Sleep(m_nTxTxPauseMS); //wait again, unit might just be busy, 
				// dont pepper it with rapid commands
			}
		}
		else
		{
			nReturnCode = SEND_ERROR;
			if (pszError)
			{
				_snprintf(pszError, IS2_ERRORSTRING_LEN-1, "Socket error in SendCommand after %d retries.\nCommand: %s\nReturn code: %d.", nCount, pszCommand, nReturnCode);
			}
			// for the retry
			CloseConnection();
			if (OpenConnection(pszError)<IS2CORE_SUCCESS)
			{
				if (pszError)
				{
					_snprintf(pszError, IS2_ERRORSTRING_LEN-1, "SendCommand failed due to socket error after %d retries.\nCommand: %s\nReturn code: %d.", nCount, pszCommand, nReturnCode);

			//		AfxMessageBox(pszError);
				}
				return IS2CORE_ERRORCONN;
			}

		}
    nCount++;
  }

  if ((nSucceeded<IS2CORE_SUCCESS)&&(pszError)) _snprintf(pszError, IS2_ERRORSTRING_LEN-1, "SendCommand failed after %d retries.\nCommand: %s\nReturn code: %d.", nCount, pszCommand, nReturnCode);
  
	if(nMode == IS2_CONN_SEPARATE)
	{
		CloseConnection();
	}

  return nSucceeded;
}


/////////////////////////////////////////////////////////////////////////////
// Private Methods Implementation

void CIS2Core::do_crc( unsigned char ch, unsigned short * crcptr )
{
  unsigned int tmp;
  tmp = *crcptr ^ ch;
  *crcptr = mstab[ (tmp>>4) & 0xf ] ^ lstab[ tmp&0xf ] ^ ( (*crcptr) >> 8 );
}

/*
int CIS2Core::rem_send_char(unsigned char ch)
{
  char temp[5];
  temp[0]=ch;
  temp[1]=0;
// sprintf(temp, "%c", ch);

  if (m_bUseEthernet)
  {
    send(m_Socket, temp, 1, 0);
  }
  else
  {
    m_tty.WriteCommBlock(temp, 1);
  }
}
*/


// receive response
int  CIS2Core::ReceiveAck(int* pnReturnCode)
{ 
	unsigned char* pucBuffer=NULL;
	unsigned long ulBufferLen=0;

  if (m_bUseEthernet)
  {
		if(m_net.GetLine(&pucBuffer, &ulBufferLen, m_socket)<NET_SUCCESS) ulBufferLen = 0;
  }
  else
  {
		if(m_serial.ReceiveData((char**)(&pucBuffer), &ulBufferLen)<SERIAL_SUCCESS) ulBufferLen = 0;
  }

  *pnReturnCode = REPLY_OK;

  if (ulBufferLen == 0)  { *pnReturnCode = REPLY_TIMEOUT; return IS2CORE_ERROR; }

  char ch = *pucBuffer;
  if ((ch == ACK0) && (g_chStx == STX1))
  {
    return IS2CORE_SUCCESS;
  }
  else if ((ch == ACK1) && (g_chStx == STX0))
  {
    return IS2CORE_SUCCESS;
  }
  else if (ch == NAK)
  { 
//    AfxMessageBox("nak");
    *pnReturnCode = NAK;
    return IS2CORE_ERROR;
  }
  *pnReturnCode = REPLY_MISMATCHED;
  return IS2CORE_ERROR;
}

char* CIS2Core::EscapeSpecialChars(char* buffer)
{
	char* pch = NULL;
	if(buffer)
	{
		int n=0;
		char ch, pszMessage[(IMAGESTORE2_MAX_MSGBUFFER*3)+1], pszEscape[8]; // if every single caracter is escaped, it is 3X the size of max
		for (int i=0; i<(int)strlen(buffer); i++)
		{
			ch = buffer[i];

			if ((ch == ';') || (ch == ':') || (ch == '|') ||  (ch == '\\'))
			{
				sprintf(pszEscape,"\\%02x",ch);
				for (int e=0; e<(int)strlen(pszEscape); e++)
				{
					pszMessage[n] = pszEscape[e]; n++;
				}
			}
			else
			{
				pszMessage[n] = ch; n++;
			}
		}
		pszMessage[n] = 0;// null term

		n = strlen(pszMessage);
		if(n>0)
		{
			pch = (char*)malloc(n+1);
			if(pch)
			{
				strcpy(pch, pszMessage);
			}
		}
	}
	return pch;
}

// Send a single command to an Imagestore, using printf style formatting.
int CIS2Core::remote_send(char* pszFormat,...)
{
  unsigned short rem_crc = 0;
  int i;
  int n=0, r=0;;
  char ch, pszMessage[IMAGESTORE2_MAX_MSGBUFFER+1];//, pszEscape[8];
  
	va_list marker;
	// create the formatted output string
	va_start(marker, pszFormat); // Initialize variable arguments.
	_vsnprintf(pszMessage, IMAGESTORE2_MAX_MSGBUFFER, pszFormat, (va_list) marker);
	va_end( marker );             // Reset variable arguments.

  r= rem_send_char(g_chStx);  // byte 0 is STX0/STX1

	if(r == SOCKET_ERROR)
	{
		n=r;
	}
	else
	{
		n+=r;

		for (i=0; i<(int)strlen(pszMessage); i++)
		{
			ch = pszMessage[i];
/*  // dont escape stuff here.  messes with delimiters that should not be escaped
			if ((ch == ';') || (ch == ':') || (ch == '|') ||  (ch == '\\'))
			{
				sprintf(pszEscape,"\\%02x",ch);
				for (int e=0; e<(int)strlen(pszEscape); e++)
				{
					ch = pszEscape[e];
//AfxMessageBox(ch);
					r= rem_send_char(ch);
					if(r == SOCKET_ERROR)
					{
						n=r; break;
					}
					else
					{
						n+=r;
						do_crc( ch, &rem_crc );
					}
				}
			}
			else
*/
			{
//AfxMessageBox(ch);
				r= rem_send_char(ch);
				if(r == SOCKET_ERROR)
				{
					n=r; break;
				}
				else
				{
					n+=r;
					do_crc( ch, &rem_crc );
				}
			}
		}

		if(	r != SOCKET_ERROR)
		{
			r = rem_send_char(':');
  
			if(r == SOCKET_ERROR)
			{
				n=r; 
			}
			else
			{
				n+=r;
				do_crc( ':', &rem_crc );
  
				r = rem_send_char(rem_crc & 0xff );
				if(r == SOCKET_ERROR)
				{
					n=r; 
				}
				else
				{
					n+=r;
					r= rem_send_char(rem_crc >> 8 );
					if(r == SOCKET_ERROR)
					{
						n=r; 
					}
					else
					{
						n+=r;
					}

				}
			}

			if (g_chStx == STX0) g_chStx = STX1;
			else g_chStx = STX0;
		}
	}
	return n;
}

int CIS2Core::rem_send_char(char ch)
{
  if (m_bUseEthernet)
  {
    return send(m_socket, &ch, 1, 0);
  }
  else
  {
    return m_serial.SendData(&ch, 1);
  }
}

/*
Video Command Set:

Description                   Type ID Param list

Fade to/from black            ACT 0 p1=Layer p2=(0=to, 1=from)
Fade A/B or key up/down       ACT 1 p1=Layer p2=0=down, 1=up
Cut to/from black             ACT 2 p1=Layer p2=0=to, 1=from
Cut A/B or key up/down        ACT 3 p1=Layer p2=0=down, 1=up
Set mask                      ACT 4 p1=Layer p2=Left p3=right p4=top p5=bottom
Mask enable                   ACT 5 p1=Layer p2=0=disable,1=enable
Swap preview w/main&cut up    ACT 6
Swap preview w/main&fadeup    ACT 7
Load display from file #<n>   ACT 8 p1=Layer p2=n
Store display in file #<n>    ACT 9 p1=Layer p2=n
Set transition type           ACT < p1=Layer p2=0=Mix,1=Vertical wipe,2=Horiz wipe
Erase store                   ACT A p1=Layer
Set transition rate           ACT B p1=Layer p2=type:0=FTB,1=fade   p3=rate
Set Key Source                ACT C p1=Layer p2=0=separate, 1=self, 2=none (IS2)
Set Key Mode                  ACT D p1=Layer p2=0=full, 1=linear
Set key normal/invert         ACT E p1=Layer p2=0=norm, 1=inv
Set clip, gain & trans        ACT F p1=Layer p2=clip p3=gain p4=transp
Set image position            ACT G p1=Layer p2=horiz p3=vert
Load / unload live video      ACT H p1=Layer p2=0=unload,1=load
File save masked              ACT I p1=Layer p2=n
Grab Fill/B+KEY as video+key  ACT J p1=Layer
Pass SDI                      ACT K p1=Input
Force colour field            ACT L p1=Input p2=r p3=g p4=b
Enquire System Status         ENQ M
    Reply                         M system status block
Enquire layer status          ENQ N p1=Layer
    Reply                         N Layer status block
Enquire image status          ENQ O p1=Layer
    Reply                         O Image block
Enquire file status           ENQ P p1=N
    Reply                         P file status block
File erase                    ACT Q p1=n

*/


/*  Squeezy commands

Set Squeeze preset W0
This command modifies the basic size & position parameters for a squeeze preset. 
XY position and XY size are floating point values (converted to 16.16), where 
the active video area is defined as 1.0 high by 1.0 wide. The position is the 
position of the centre of the squeezed image.
Example:
To place the image in its nominal size and position, X position = Y Position = 
0.5, X Size = Y Size = 1.0.

Example: to shrink the image to occupy the upper-right quarter of the screen:
X position = 0.75, Y Position = 0.25, X Size = 0.5, Y Size = 0.5

  CMD Param_1 Param_2 Param_3 Param_4 Param_5
  W0 %02x : Preset No. %08x : X Position %08x: Y Position %08x: X Size %08x:Y Size
*/

long CIS2Core::oxfix(float f)
{
  return (long)(f * 0x10000);
}

int CIS2Core::SqueezySetPreset(int preset, float xpos, float ypos, 
                         float xsize, float ysize, char* pszError)
{
  //remote_send("W0%02x%08x%08x%08x%08x", preset, oxfix(xpos), oxfix(ypos), oxfix(xsize), oxfix(ysize));

//  return SendImageStore2Command(FormatCommand("W0%02x%08x%08x%08x%08x", preset, oxfix(xpos), oxfix(ypos), oxfix(xsize), oxfix(ysize)), pszError);
	return SendCommand(pszError, m_nMode, "W0%02x%08x%08x%08x%08x", preset, oxfix(xpos), oxfix(ypos), oxfix(xsize), oxfix(ysize) );
}

int CIS2Core::SqueezySetPreset(int preset, char* pszError)
{
  //remote_send("W0%02x%08x%08x%08x%08x", preset, oxfix(xpos), oxfix(ypos), oxfix(xsize), oxfix(ysize));

//  return SendImageStore2Command(FormatCommand("W0%02x%08x%08x%08x%08x", preset, oxfix(m_OxtelX), oxfix(m_OxtelY), oxfix(m_OxtelW), oxfix(m_OxtelH)), pszError);
	return SendCommand(pszError, m_nMode, "W0%02x%08x%08x%08x%08x", preset, oxfix(m_OxtelX), oxfix(m_OxtelY), oxfix(m_OxtelW), oxfix(m_OxtelH));
}

/*
  Select Squeeze Configuration W1
  Select the squeeze configuration mode.
  This determines where in the signal flow the Squeezy hardware is configured. 
  Note : Changing the squeeze configuration can cause instant changes in program 
  output as it also affects keyer priorities.
    0: No Squeeze
    1: Picture-in-Picture (only when AB Mix option fitted)
    2: Squeeze & Reveal (swap-preview only)
    3: Squeeze Stored(swap-preview only)
    4: Squeeze & Reveal Midground (cascade only)
    5: Squeeze & Reveal Foreground (cascade only)
    6: Squeeze Midground Store (cascade only)
    7: Squeeze Foreground Store (cascade only)
*/
int CIS2Core::SqueezySetSqueezeConfig(int setting, char* pszError)
{
//  return SendImageStore2Command(FormatCommand("W1 %02x", setting), pszError);
	return SendCommand(pszError, m_nMode, "W1 %02x", setting);
}

/*
  Run Squeeze move W2
  Runs a squeeze move to a particular preset position over a specified 
  number of fields.
  W2 %02x : Preset %03x : Fields

*/
int CIS2Core::SqueezyRunSqueeze(int presetNumber, int nDurationInFields, char* pszError)
{
//  return SendImageStore2Command(FormatCommand("W2 %02x %03x", presetNumber, nDurationInFields), pszError);
	return SendCommand(pszError, m_nMode, "W2 %02x %03x", presetNumber, nDurationInFields);
}


/*
  Select move profile W3
  Select Linear or smooth velocity profile for squeeze moves.
  W3 %01x:  Profile : 0 = linear, 1 = smoothed
*/
int CIS2Core::SqueezySetMoveProfile(int param, char* pszError)
{
//  return SendImageStore2Command(FormatCommand("W2 %01x", param), pszError);
	return SendCommand(pszError, m_nMode, "W2 %01x", param);
}


/*
  Select Preset Borders  W4
  Setup the border positions associated with a squeeze preset. Borders 
  can be used to simulate wipes. For example, squeezing from full screen 
  to a preset with a Left Border of 1.0 will � wipe� the video off to the
  right.
  W4 %02x : Preset No. %08x : Left Border %08x: Right Border %08x: Top Border %08x:Bottom Border
*/
int CIS2Core::SqueezySelectBorders(int preset, float left, float right, float top, float bottom, char* pszError)
{
//  return SendImageStore2Command(FormatCommand("W2 %02x %08x %08x %08x %08x", preset, oxfix(left), oxfix(right), oxfix(top), oxfix(bottom)), pszError);
	return SendCommand(pszError, m_nMode, "W2 %02x %08x %08x %08x %08x", preset, oxfix(left), oxfix(right), oxfix(top), oxfix(bottom));
}

/////////////////////////////////////////////////////////////////////////////////
//
//  Direct fader control

int CIS2Core::SetFader(int nLayer, int nDurationInFields, char* pszError)
{
  // remote_send("B%d %d %03x", nLayer, 1, duration*2); //layer, fade
//  return SendImageStore2Command(FormatCommand("B%d %d %03x", nLayer, 1, nDurationInFields), pszError);
	return SendCommand(pszError, m_nMode, "B%d %d %03x", nLayer, 1, nDurationInFields);
}


int CIS2Core::TriggerFader(int nLayer, bool bFadeIn, char* pszError)
{
  if (bFadeIn)
  {
    //return SendImageStore2Command(FormatCommand("1%d 1", nLayer), pszError); //layer, fade
		return SendCommand(pszError, m_nMode, "1%d 1", nLayer);
  }
  else // fade out
  {
    //return SendImageStore2Command(FormatCommand("1%d 0", nLayer), pszError); //layer, fade
		return SendCommand(pszError, m_nMode, "1%d 0", nLayer);
  }
}


/////////////////////////////////////////////////////////////////////////////////
// 
//  EasyText CG Support
//
//  REQUIRES v2.11 SOFTWARE!!!!!!!!!!!!!!
//
/*

  Miranda Technologies Ltd Page 123

  Easytext Automation Commands
  
  Introduction

  The Oxtel Series automation protocol now supports control of the Easytext
  character generation software. Commands have been added to the
  Imagestore software to allow specification and formatting of on-screen text.

  Summary

  Easytext �templates� are built using the Textbuilder software application from
  the Media Conversion Software (MCS) suite. Templates consist of a number
  of �boxes� each of which can either contain a piece of text in a single style, or
  a particular graphic.

  The number, positions, parameters and styles of these boxes are defined in
  the template, along with some optional �static text� - the text shown when the
  template is first loaded.

  For most simple automated CG (Character Generation) applications, the
  automation must perform three separate tasks: It must load the template, and
  then subsequently download new text to be displayed in certain boxes. On
  completion of this, it can take the template to air using the keyer controls.
  Various problems may be found as this is attempted:
    1. The loading of a template happens as a �background� operation: The
       �ACK� from Imagestore simply means the load request has been
       received correctly, not that it has completed.
    2. Because of this, new text should not be sent until the template has
       been loaded. It�s possible to determine this by using the �disk busy�
       bit returned as part of the "Enquire loaded image status" (O)
       command - Alternatively, a simple delay of a couple of seconds will
       normally suffice.
    3. Once the template is loaded, new text can then be sent. The box
       numbers specified when the template was created must be known, to
       ensure that the new text is inserted in the correct places. Again, the
       rendering of the text into the template is comparatively slow, and it
       may be important to wait for the text to be rendered before taking
       the template to air. This must currently be handled with a delay,
       which will depend on the complexity of the template and the
       number of characters to be drawn.

  The �Z0� command is the basis for all text updates.

  All Easytext commands take a �layer� parameter, defining whether the
  midground (preview) or foreground (program) image is being modified.

  In addition, many commands refer to a specific box on the template. Box
  numbers can run up to 254. Box 255 is treated as a special case by some
  commands.

 */

///////////////////////////////////////////////////////////////////////////////
// This command updates the text in a specified text box. Formatting and style 
// of the text is unchanged.
//
// The layer value specifies which Imagestore keying layer is addressed. 0
//  represents preview/midground, and 1 represents programme/foreground. 
//
// The Box number ranges from 0x00 to 0xFF and relates to which Text Box is
// updated.
//
// The String is the new text to be written into text box specified. To enable
// international character support, the string should be UTF-8 encoded before
// transmission, see below.
// 
// Two bits are defined in flags: 
// ET_RENDER (=1) If this bit is set to 1 then
// the new text will appear on screen with all changes to font, size, colour and
// position specified since the last call to �Update Box� or update text field. If
// set to FALSE then changes will not appear on screen until a call to �Update
// Box� or Update Text Field with the Render bit set to 1 is sent.
//
// ET_APPEND (=2) If this bit is set, then the text is appended to the current
// text. This allows arbitrarily long text data to be sent in several packets.
// Typically, the first packet should have flags = 0 (don�t append, don�t render),
// remaining packets should have ET_APPEND flag set, and the final packet
// should have ET_APPEND + ET_RENDER, to make the text get displayed.

/*

bool CIS2Core::EasyText_UpdateTextBox(int nLayer, int nTextBoxNo, 
                                    CString szString, CString* pszError)
{
  int nFlags = 0;
  int i, nLength = strlen(szString);
  bool bOK = TRUE;
  
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;
  
  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;
  

  // All text is expanded for datasource references, so attempting to display
  // vertical bar characters now needs them to be transmitted doubled up thus: ||
  CString szTempString = "";
  for (i=0; i<szString.GetLength(); i++)
  {
    if (szString.GetAt(i) == '|')
      szTempString += "||";
    else 
      szTempString += szString.GetAt(i);
  }
  szString = szTempString;

  // convert to UTF8
  // .....

  if (nLength < IMAGESTORE2_MAX_ET_LENGTH)
  {
    nFlags = 0;
    return SendImageStore2Command(FormatCommand("Z0%01x%02x%01x%s", nLayer, nTextBoxNo, nFlags, szString), pszError);
  }
  else
  {
    CString szSend;
    szSend.Format("%s", szString);
    // chunk into packets, 
    for (i=0; i<=(nLength / IMAGESTORE2_MAX_ET_LENGTH); i+=IMAGESTORE2_MAX_ET_LENGTH)
    {
      if (i == 0)
        nFlags = 0;  // first packet
      else if (i == (nLength / IMAGESTORE2_MAX_ET_LENGTH)) 
        nFlags = IMAGESTORE2_ET_APPEND + IMAGESTORE2_ET_RENDER; // last packet
      else 
        nFlags = IMAGESTORE2_ET_APPEND; // middle packets
      
      bOK = bOK & SendImageStore2Command(FormatCommand("Z0%01x%02x%01x%s", nLayer, nTextBoxNo, nFlags, 
        szSend.Mid(i*IMAGESTORE2_MAX_ET_LENGTH, IMAGESTORE2_MAX_ET_LENGTH)), pszError);
    }
    return bOK;
  }
}


//Change Box Size and Position Z1
//This command alters the position and dimensions of a text or image box.
//The X and Y position indicate the top left position of the text box. All values
//are in pixels. This command can be used to alter the position of an image
//box; however its size cannot be changed remotely.
//This command will not update the text box on screen - the settings take effect
//when the �Render Box� command is issued.
bool CIS2Core::EasyText_ChangeDims(int nLayer, int nTextBoxNo, int X, int Y, 
                                 int Width, int Height, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  return SendImageStore2Command(FormatCommand("Z1%01x%02x%03x%03x%03x%03x",
    nLayer, nTextBoxNo, X, Y, Width, Height), pszError);
}


//Render Box Z3
//This command will update the specified text or image box on screen using all
//previous settings.
//If a box number of 255 (0xFF) is sent, then ALL boxes are re-rendered with
//their latest values. This is useful to guarantee that all boxes update
//simultaneously.
//This command must be sent to cause boxes to be re-rendered after adjusting
//their parameters via automation.

bool CIS2Core::EasyText_RenderBox(int nLayer, int nTextBoxNo, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  return SendImageStore2Command(FormatCommand("Z3%01x%02x", nLayer, nTextBoxNo), pszError);
}



//Change Image Z4
//This command allows the image associated with an image box to be replaced
//with another image held on disk.
//Only .OXT image files are supported for this command.

bool CIS2Core::EasyText_UpdateBoxImage(int nLayer, int nTextBoxNo, 
                                     CString szImage, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  return SendImageStore2Command(FormatCommand("Z4%01x%02x%s", nLayer, nTextBoxNo, szImage), pszError);
}



// Set Text Font and Colour Z2
// The font and colour of the text contained in a text box can be changed with
// this command.
// The font name relates to the font filename to be used in the graphic, For
// example �Times New Roman� must be addressed as �times.ttf�.
// The size is the �point size� of the font. Note that points do not directly equate
// to pixels. The font�s design determines this.
// This command will not update the text box on screen - the settings take effect
// when the �Render Box� command is issued.
bool CIS2Core::EasyText_ChangeFont(int nLayer, int nTextBoxNo, int nSize, 
                                 int nColorR, int nColorG, int nColorB, 
                                 CString szFontFileName, CString* pszError)
{
  DWORD dwColor = ((nColorR & 0xff) << 16 ) + 
                  ((nColorG & 0xff) << 8) + 
                   (nColorB & 0xff);


  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  return SendImageStore2Command(FormatCommand("Z2%01x%02x%02x%06x%s", nLayer, nTextBoxNo,
              nSize, dwColor, szFontFileName), pszError);
}



// Set Text Transparency ZA
// The transparency of text in a box can be set with this command.
// A transparency of 255 (0xFF) is fully opaque, and 0 is transparent.

bool CIS2Core::EasyText_ChangeTextTransparency(int nLayer, int nTextBoxNo, 
                                             int nTransparency, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  if (nTransparency < 0) nTransparency = 0;
  if (nTransparency > 255) nTransparency = 255;

  //remote_send ("ZA%01x%02x%02x", nLayer, nTextBoxNo, nTransparency);
  return SendImageStore2Command(FormatCommand("ZA%01x%02x%02x", nLayer, nTextBoxNo, nTransparency), pszError); 
}



// Set Text Drop Shadow Z9
// The text�s drop shadow can be adjusted with this command.
//
//The X and Y offsets are the distance in pixels from the text which the drop
//shadow falls. Positive numbers are down and right. Don�t forget to limit these
//numbers to two digits. Setting these both to zero turns off the drop shadow.
//The transparency of the drop shadow can be set with �trans.� where FF is
//solid colour and 00 is transparent.

bool CIS2Core::EasyText_SetDropShadow(int nLayer, int nTextBoxNo, int Xoffset, int Yoffset, 
                                    int nTransparency, 
                                    int nColorR,int nColorG,int nColorB, 
                                    CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  if (nTransparency < 0) nTransparency = 0;
  if (nTransparency > 255) nTransparency = 255;

  // I think this is reasonable:
  if (Xoffset < -255) Xoffset = -255;
  if (Xoffset > 255) Xoffset = 255;
  if (Yoffset < -255) Yoffset = -255;
  if (Yoffset > 255) Yoffset = 255;

  DWORD dwColor = ((nColorR & 0xff) << 16 ) + 
                  ((nColorG & 0xff) << 8) + 
                   (nColorB & 0xff);

  return SendImageStore2Command(FormatCommand("Z9%01x%02x%02x%02x%02x%06x", nLayer, nTextBoxNo, 
    Xoffset & 0xFF, Yoffset & 0xFF, nTransparency &0xFF, dwColor), pszError);
}



// Set Text Tracking ZB
// The tracking (internal inter-character spacing) of text can be modified with
// this command.
// Positive numbers move the letters further apart, while negative tracking
// closes up the gaps between letters. The unit of tracking is 1/64th of a pixel.
// The signed value should be �ANDED� with 0xFFFF to ensure it is only four
// digits long.
// The �Text Tracking� command (ZB) now takes a two�s complement
// parameter, instead of offset binary.

bool CIS2Core::EasyText_SetTextTracking(int nLayer, int nTextBoxNo, 
                                      int nTracking, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  return SendImageStore2Command(FormatCommand("ZB%01x%02x%04x", nLayer, nTextBoxNo, nTracking & 0xffff), pszError);
}


// Set Text Alignment ZE
// The alignment of text within a box can be adjusted with this command
// H Align : 0 = Align Left, 1 = Align Centre, 2 = Align Right.
// V Align : 0 = Align Top, 1 = Align Centre, 2 = Align Bottom.

bool CIS2Core::EasyText_SetTextAlignment(int nLayer, int nTextBoxNo, int nHAlign, int nVAlign, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  if (nHAlign < 0) nHAlign = 0;
  if (nHAlign > 2) nHAlign = 2;

  if (nVAlign < 0) nVAlign = 0;
  if (nVAlign > 2) nVAlign = 2;

  return SendImageStore2Command(FormatCommand("ZE%01x%02x%01x%01x", nLayer, nTextBoxNo, nHAlign, nVAlign), pszError);
}

// Set Text Wrapping ZF
// The alignment of text within a box can be adjusted with this command
// A Wrap mode of 0 means no automatic wrapping occurs. If the text is too
// wide for the box, the pointsize is automatically reduced. Existing line breaks
// in the text are maintained.
// If a wrap mode of 1 allows Easytext to insert additional line breaks between
// words to use the size of the text box without excessive shrinking of the text.

bool CIS2Core::EasyText_SetTextWrapping(int nLayer, int nTextBoxNo, int nWrapMode, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  if (nWrapMode < 0) nWrapMode = 0;
  if (nWrapMode > 1) nWrapMode = 1;

  return SendImageStore2Command(FormatCommand("ZF%01x%02x%01x", nLayer, nTextBoxNo, nWrapMode), pszError);
}


// Set Text Background to Clear Z8
// A coloured or gradient background may be removed with this command.
bool CIS2Core::EasyText_TextBgnd_Clear(int nLayer, int nTextBoxNo, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  return SendImageStore2Command(FormatCommand("Z8%01x%02x", nLayer, nTextBoxNo), pszError);
}


// Set Text Background to Matte Z6
// This command allows a solid background behind a box to be automatically
// drawn (and resized) when the box is drawn
// The transparency of the colour can be set with �trans� where FF is solid
// colour and 00 is transparent.
// The hborder and vborder describe the horizontal and vertical border placed
// around the colour block. Border values of 0 will produce a rectangle which
// fits behind the rendered text exactly. Values of 10 would produce a 10 pixel
// border outside the text rectangle.
// This command will take effect only when the text box is re-rendered.
bool CIS2Core::EasyText_TextBgnd_Matte(int nLayer, int nTextBoxNo, 
                                     int nColorR, int nColorG, int nColorB, 
                                     int nTransparency, int nHBorder, 
                                     int nVBorder, CString* pszError)
{

  DWORD dwColor = ((nColorR & 0xff) << 16 ) + 
                  ((nColorG & 0xff) << 8) + 
                   (nColorB & 0xff);

  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  if (nTransparency < 0) nTransparency = 0;
  if (nTransparency > 255) nTransparency = 255;

  if (nHBorder < 0) nHBorder = 0;
  if (nHBorder > 255) nHBorder = 255;

  if (nVBorder < 0) nVBorder = 0;
  if (nVBorder > 255) nVBorder = 255;

  return SendImageStore2Command(FormatCommand("Z6%01x%02x%06x%02x%02x%02x", nLayer, nTextBoxNo, dwColor, 
    nTransparency, nHBorder, nVBorder), pszError);
}


// Set Text Background to Gradient Z7
// This command allows a gradient background behind a box to be
// automatically drawn (and resized) when the box is drawn
// The gradient starts from the source colour, and finishes at the destination colour.
// The border colour is drawn around the gradient-filled text box.
// The transparency of the box background can be set with �trans� where FF is
// solid colour and 00 is transparent.
// If the direction variable is 0, the gradient will be vertical, otherwise the
// gradient will be horizontal.
// The hborder and vborder define the width of the horizontal and vertical
// borders placed around the gradient colour block. Border values of 0 will
// produce a rectangle which fits behind the rendered text exactly, values of 10
// will produce a 10 pixel border around the text rectangle. This command will
// take effect only when the text box is re-rendered.

bool CIS2Core::EasyText_TextBgnd_Gradient(int nLayer, int nTextBoxNo, 
                                        int nSourceColR, int nSourceColG, int nSourceColB, 
                                        int nDestColR, int nDestColG, int nDestColB, 
                                        int nBorderColR, int nBorderColG, int nBorderColB, 
                                        int nTransparency, int nBorderWidth, int nDirection, 
                                        int nHBorder, int nVBorder, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nTextBoxNo < 0) nTextBoxNo = 0;
  if (nTextBoxNo > 255) nTextBoxNo = 255;

  DWORD dwSrcColor = ((nSourceColR & 0xff) << 16 ) + 
                  ((nSourceColG & 0xff) << 8) + 
                   (nSourceColB & 0xff);

  DWORD dwDestColor = ((nDestColR & 0xff) << 16 ) + 
                  ((nDestColG & 0xff) << 8) + 
                   (nDestColB & 0xff);

  DWORD dwBorderColor = ((nBorderColR & 0xff) << 16 ) + 
                  ((nBorderColG & 0xff) << 8) + 
                   (nBorderColB & 0xff);

  if (nHBorder < 0) nHBorder = 0;
  if (nHBorder > 255) nHBorder = 255;

  if (nVBorder < 0) nVBorder = 0;
  if (nVBorder > 255) nVBorder = 255;

  if (nTransparency < 0) nTransparency = 0;
  if (nTransparency > 255) nTransparency = 255;

  return SendImageStore2Command(FormatCommand("Z7%01x%02x%06x%06x%06x%02x%02x%03x%02x%02x", nLayer, nTextBoxNo, 
    dwSrcColor, dwDestColor, dwBorderColor, nTransparency, 
    nBorderWidth, nDirection, nHBorder, nVBorder), pszError);
}



// Setting Template background options ZD
// As well as specifying backgrounds for individual boxes, it is possible to
//specify a background for the entire template. The background size will be the
//size of the template created in Text Builder.
//Template backgrounds can be a solid colour, with controllable transparency,
//or can have transparency that fades in and/or out.
//Mode values:
//0 = Solid, 1 = Fade Down, 2 = Fade Up, 3 = Fade Both, 4 = Fade Edge
// For mode 4, the Edge Size parameter determines the width of the faded area.
bool CIS2Core::EasyText_ChangeTextBgnd(int nLayer,int nColorR, int nColorG, 
                                     int nColorB, int nTransparency,  
                                     int nMode, int nEdgeSize, CString* pszError)
{

  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nMode < 0) nMode = 0;
  if (nMode > 4) nMode = 4;

  DWORD dwColor = ((nColorR & 0xff) << 16 ) + 
                  ((nColorG & 0xff) << 8) + 
                   (nColorB & 0xff);

  if (nTransparency < 0)   nTransparency = 0;
  if (nTransparency > 255) nTransparency = 255;

  if (nEdgeSize < 0)   nEdgeSize = 0;
  if (nEdgeSize > 255) nEdgeSize = 255;

  return SendImageStore2Command(FormatCommand("ZD%01x%06x%02x%01x%02x", nLayer, dwColor, nTransparency, nMode, nEdgeSize), pszError);
}



// Text Straps

// Easytext supports text rolls and crawls, known together as straps. Additional
// commands give extra control of the playout of these.
// It�s important to note that because the transition between �old� and �new� text
// happens at the wrapping point of the strap, there may be a significant delay
// between sending the text message, and having it displayed. There is no
// �queue� for messages: If new text is sent before previously transmitted text
// has been displayed, the previously transmitted text may never be seen.


// Run Strap Z5
// This command allows an Easytext Strap to be stopped and started
// 0=Stop, 1=Run
bool CIS2Core::EasyText_StartStrap(int nLayer, int nStart, CString* pszError)
{
  if (nStart <= 0) nStart = 0;
  else nStart = 1;

  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  return SendImageStore2Command(FormatCommand("Z5%01x%01x", nLayer, nStart), pszError);
}

// Set Strap Speed ZC
// This command allows the speed of an Easytext Strap to be controlled
// Speed is in pixels/lines per field
// Only positive �even� speeds (0,2,4,6,8, etc) are supported.
bool CIS2Core::EasyText_SetStrapSpeed(int nLayer, int nSpeed, CString* pszError)
{
  if (nLayer < 0) nLayer = 0;
  if (nLayer > 1) nLayer = 1;

  if (nSpeed <= 0) nSpeed = 0;
  else 
  {
    // round odd speeds
    nSpeed = (nSpeed / 2) * 2;
  }
 
  return SendImageStore2Command(FormatCommand("ZC%01x%02x", nLayer, nSpeed), pszError);
}


// Datasource automation commands
// beyond the scope of what we need, see Oxtel Series Automation Protocol.pdf,
// page 142 for more info.
*/

#ifdef OLD_METHOD

// duration is in frames
bool CIS2Core::TriggerSqueezeback(int duration, int command, CString *err)
{
  bool ok; 
  int count, i, retries=0, reason;
  CString label;
  int nLayer = 1; // I think 0=preview, 1=program
  
  if (m_bStub) return TRUE;

  // make connection
  /*
  m_tty.CloseConnection();

  if(!m_tty.OpenConnection(
    m_dwBaudRate, 8,ONESTOPBIT,NOPARITY,m_nCOMport))
  {
    err->Format("Error making serial connection to Oxtel! Check settings and connections.");
    return FALSE;
  }

  */
  CloseConnection();
  if (!OpenConnection(err)) return FALSE;

  for (i=0; i<2; i++)
  {
    ok = FALSE; 
    count=0;
    //AfxMessageBox((g_chStx == STX0)?"Trying STX0":"Trying STX1");

    while ((!ok) && (count < IMAGESTORE2_RETRY))
    {
      retries++; 
      switch(command)
      {
      case IMAGESTORE2_SETSQUEEZE:
        label="Set Squeezy Presets";
        // changed from this: on 1/5/01
        //SqueezySetPreset(1, (float)0.357, (float)0.581, (float)/*0.678 */0.717, (float)0.750);//0.732);
        
        // changed from this: on 4/6/01 for new look - change in Settings!
        //SqueezySetPreset(1, (float)0.326, (float)0.499, 
        //  (float)/*0.678 */0.658, (float)0.678);//0.732);

        SqueezySetPreset(PRESET_SQUEEZED, m_OxtelX, m_OxtelY,
          m_OxtelW, m_OxtelH, err);
        break;
      case IMAGESTORE2_SETFADE:
        label="Set Fader Presets";
        remote_send("B%d %d %03x", nLayer, 1, duration*2); //layer, fade
        break;  
      case IMAGESTORE2_DOSQUEEZEIN:
        label="Squeeze In";
        SqueezyRunSqueeze(PRESET_SQUEEZED, duration*2, err);
        break;
      case IMAGESTORE2_DOSQUEEZEOUT:
        label="Squeeze Out";
        SqueezyRunSqueeze(PRESET_FULL, duration*2, err);  // fast squeeze out
        break;
      case IMAGESTORE2_FADEOUT:
        label = "Fade Out";
        remote_send("1%d 0", nLayer); //layer, fade
        break;
      case IMAGESTORE2_FADEIN:
        label = "Fade In";
        remote_send("1%d 1", nLayer); //layer, fade
        break; 
      }
    
      // Wait a moment...
      Sleep(IMAGESTORE2_TIMEOUT_SLEEP);

      ok = getAck(label, &reason);

      if (!ok) // keep trying with same STX
      {
        /*
        switch(reason){
        case MISMATCHED: AMB("%s mismatch",label); break;
        case TIMEOUT: AMB("%s Timeout",label); break;
        case NAK: AMB("%s NAK",label); break;
        } 
        */
        if (g_chStx == STX0) g_chStx = STX1;
        else g_chStx = STX0;
      }
      count++;
    }
    if (ok) break;

    // on second try, try other stx
    if (g_chStx == STX0) g_chStx = STX1;
    else g_chStx = STX0;
  }
  if (!ok) err->Format("TriggerSqueezeback:%s failed despite %d retries.", label, count*2);
  
  // m_tty.CloseConnection();
  CloseConnection();

  return ok;
}

#endif