// OmnibusTesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OmnibusTester.h"
#include "OmnibusTesterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int CALLBACK CompareItems(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

typedef struct listsortinfo_t
{
	CListCtrlEx* pList;
	int nItem;
	bool bColSortDesc;
} listsortinfo_t;

typedef struct channelinfo_t
{
	int nID;
	bool bFound;
} channelinfo_t;
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COmnibusTesterDlg dialog

COmnibusTesterDlg::COmnibusTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COmnibusTesterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COmnibusTesterDlg)
	m_szIP = _T("172.29.132.45");
	m_nPort = 10540;
	m_szHost = _T("127.0.0.1");
	m_bWriteXML = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pConn = NULL;
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_nSelChan =-1;
	m_bUpdating = false;
	m_bChangeSel = false;
	m_pdbConn = NULL;
	m_nColSort = 0;
	m_bColSortDesc = false;
	m_nChColSort = 0;
	m_bChColSortDesc = false;
	strcpy(m_pszDB, "(none)");
	m_nSelEventID = -1;

	InitializeCriticalSection(&m_critEvents);
	InitializeCriticalSection(&m_critChannels);


}

void COmnibusTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COmnibusTesterDlg)
	DDX_Control(pDX, IDC_LIST_EVENTS, m_lcEvents);
	DDX_Control(pDX, IDC_LIST_CHANNELS, m_lcChannels);
	DDX_Text(pDX, IDC_EDIT_IP, m_szIP);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Check(pDX, IDC_CHECK_XML, m_bWriteXML);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(COmnibusTesterDlg, CDialog)
	//{{AFX_MSG_MAP(COmnibusTesterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_LIST_CHANNELS, OnClickListChannels)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_EVENTS, OnColumnclickListEvents)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_CHANNELS, OnColumnclickListChannels)
	ON_NOTIFY(NM_CLICK, IDC_LIST_EVENTS, OnClickListEvents)
	ON_WM_GETMINMAXINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COmnibusTesterDlg message handlers

BOOL COmnibusTesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here


	FILE* fp = fopen("netlast.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					nSel = i;
					pch++;
				}
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch);

				pch = strtok(NULL, "\r\n");
				i++;
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}


  CRect rcList;
  m_lcChannels.GetWindowRect(&rcList);
	m_nChColPercents[0] = 80;
	m_nChColPercents[1] = 20;
	
  if(m_lcChannels.InsertColumn(
    0, 
    "Channels", 
    LVCFMT_LEFT, 
    ((rcList.Width()-20)*m_nChColPercents[0])/100,
    -1
    ) <0) AfxMessageBox("Could not add Channels column");
  m_lcChannels.InsertColumn(
    1, 
    "events", 
    LVCFMT_LEFT, 
    ((rcList.Width()-20)*m_nChColPercents[1])/100,
    1
    );

  m_lcEvents.GetWindowRect(&rcList);


	m_nColPercents[1] = 128; // not a %
	m_nColPercents[2] = 72;  // not a %
	m_nColPercents[6] = 50;   // not a %
	m_nColPercents[7] = 50;   // not a %
	m_nColPercents[8] = 20;   // not a %

	int nSum = m_nColPercents[1]+m_nColPercents[2]+m_nColPercents[6]+m_nColPercents[7]+m_nColPercents[8]+20; // 20 is scrollbar
	m_nColPercents[0] = 15;
	m_nColPercents[3] = 20;
	m_nColPercents[4] = 35;
	m_nColPercents[5] = 30;


  m_lcEvents.InsertColumn(
    0, 
    "id", 
    LVCFMT_LEFT, 
    ((rcList.Width()-nSum)*m_nColPercents[0])/100,
    -1
    );
  m_lcEvents.InsertColumn(
    1, 
    "On Air", 
    LVCFMT_LEFT, 
    m_nColPercents[1],  // 128
    1
    );
  m_lcEvents.InsertColumn(
    2, 
    "Dur", 
    LVCFMT_LEFT, 
    m_nColPercents[2],  //72
    2
    );
  m_lcEvents.InsertColumn(
    3, 
    "Clip ID", 
    LVCFMT_LEFT, 
    ((rcList.Width()-nSum)*m_nColPercents[3])/100,
    3
    );
  m_lcEvents.InsertColumn(
    4, 
    "Title", 
    LVCFMT_LEFT, 
    ((rcList.Width()-nSum)*m_nColPercents[4])/100,
    4
    );
  m_lcEvents.InsertColumn(
    5, 
    "Data", 
    LVCFMT_LEFT, 
    ((rcList.Width()-nSum)*m_nColPercents[5])/100,
    5
    );
  m_lcEvents.InsertColumn(
    6, 
    "type", 
    LVCFMT_LEFT, 
    m_nColPercents[6],
    6
    );
  m_lcEvents.InsertColumn(
    7, 
    "status", 
    LVCFMT_LEFT, 
    m_nColPercents[7],
    7
    );
  m_lcEvents.InsertColumn(
    8, 
    "time mode", 
    LVCFMT_LEFT, 
    m_nColPercents[8],
    8
    );



	return TRUE;  // return TRUE  unless you set the focus to a control
}

void COmnibusTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void COmnibusTesterDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR COmnibusTesterDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void COmnibusTesterDlg::OnCancel() 
{
	DeleteCriticalSection(&m_critEvents);
	DeleteCriticalSection(&m_critChannels);

	CDialog::OnCancel();

}

void COmnibusTesterDlg::OnOK() 
{
//	CDialog::OnOK();
}

void COmnibusTesterDlg::OnButtonConnect() 
{
	CWaitCursor cw;
	UpdateData(TRUE);



	if(m_pConn)
	{
		m_omni.DisconnectServer(m_pConn);
		m_pConn = NULL;
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_CHECK_XML)->EnableWindow(TRUE);
		if(m_pdbConn) m_db.DisconnectDatabase(m_pdbConn); // disconnects. 

	}
	else
	{
		if(m_lcEvents.GetItemCount()) m_lcEvents.DeleteAllItems();

		m_lcChannels.DeleteAllItems();
		m_nSelChan = -1;


		char conn[256];
		sprintf(conn, "%s", m_szHost);  // was m_szIP
//		AfxMessageBox(m_szHost);

		if(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
		}

		char pszValue[256];  strcpy(pszValue, "");
		char pszDSN[256];
		CDBconn* pConn = NULL;

		FILE* fp = fopen("settings.cfg", "rb");

		if(fp)
		{
			fseek(fp, 0, SEEK_END);
			unsigned long ulFileLen = ftell(fp);
			char* pchFile = (char*) malloc(ulFileLen+1); // term zero
			if(pchFile)
			{
				fseek(fp, 0, SEEK_SET);

				fread(pchFile, sizeof(char), ulFileLen, fp);
				*(pchFile+ulFileLen) = 0; // term zero

				char* pch = NULL;
				char pszUser[256];
				char pszPW[256];

				pch = strstr(pchFile, "vidstd=");
				if(pch)
				{
					_snprintf(pszValue, 255, "%s",pch+strlen("vidstd="));
					pch = strchr(pszValue, 13);
					if(pch) *pch=0;
					pch = strchr(pszValue, 10);
					if(pch) *pch=0;

				}

				pch = strstr(pchFile, "DSN=");
				if(pch)
				{
					_snprintf(pszDSN, 255, "%s",pch+strlen("DSN="));
					pch = strchr(pszDSN, 13);
					if(pch) *pch=0;
					pch = strchr(pszDSN, 10);
					if(pch) *pch=0;

					pch = strstr(pchFile, "Username=");
					if(pch)
					{
						_snprintf(pszUser, 255, "%s",pch+strlen("Username="));
						pch = strchr(pszUser, 13);
						if(pch) *pch=0;
						pch = strchr(pszUser, 10);
						if(pch) *pch=0;

						pch = strstr(pchFile, "Password=");
						if(pch)
						{
							_snprintf(pszPW, 255, "%s",pch+strlen("Password="));
							pch = strchr(pszPW, 13);
							if(pch) *pch=0;
							pch = strchr(pszPW, 10);
							if(pch) *pch=0;

							pConn = m_db.CreateNewConnection( pszDSN,  pszUser,  pszPW);  // won't create new if conn already exists in m_db object
							if(pConn)
							{
								if(m_db.ConnectDatabase(pConn, (char*)(&m_pszTable))<DB_SUCCESS) // connects. 
								{
									sprintf(m_pszTable, "Could not connect to %s with %s:%s\n", pszDSN,  pszUser,  pszPW);
									AfxMessageBox(m_pszTable);
								}
								else
								{
								//	sprintf(m_pszTable, "Connected to %s with %s:%s", pszDSN,  pszUser,  pszPW);
								//	AfxMessageBox(m_pszTable);
								}
								strcpy(m_pszTable, "Events");
								//m_omni.SetConnDB(m_pConn->m_pszServerAddress, &m_db, pConn, m_pszTable);
								m_pdbConn = pConn;
							}
							else AfxMessageBox("Could not create new DB connection");


						}

					}

				}


				free(pchFile);

			}
			fclose(fp);
		}
/*
		if((&m_db)==NULL) AfxMessageBox("DB null");
		if(pConn==NULL) AfxMessageBox("pConn null");
		if(m_pszTable==NULL) AfxMessageBox("m_pszTable null"); 
*/
		if(((&m_db)==NULL)||(pConn==NULL)||(m_pszTable==NULL))
		{
			strcpy(m_pszDB, "(none)");
//AfxMessageBox("Database parameters unavailable.\nEvents will not be databased."); 
		}
		else
		{
			strcpy(m_pszDB, pszDSN);
		}

		
		m_pConn = m_omni.ConnectServer(conn, (unsigned short) m_nPort, NULL, &m_db, pConn, m_pszTable);
		if(m_pConn)
		{
			if(stricmp(pszValue,"PAL")==0)
			{
				m_pConn->SetFrameBasis(OMNI_PAL);
//				AfxMessageBox(pszValue);
			}
			else
			if(stricmp(pszValue,"NTSCNDF")==0)
			{
				m_pConn->SetFrameBasis(OMNI_NTSCNDF);
			}
			else
			{
				m_pConn->SetFrameBasis(OMNI_NTSC);
			}


			m_pConn->m_bWriteXMLtoFile = m_bWriteXML?true:false;

			if(m_pConn->m_pszDebugFile) free(m_pConn->m_pszDebugFile);
			m_pConn->m_pszDebugFile =NULL;
			if(m_bWriteXML) 
			{
				m_pConn->SetDebugName("debug.log");
			}

			if(m_pConn->m_pszCommFile) free(m_pConn->m_pszCommFile);
			m_pConn->m_pszCommFile =NULL;
			if(m_bWriteXML) 
			{
				m_pConn->SetCommLogName("comm.log");
			}

			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PORT)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_XML)->EnableWindow(FALSE);
			//AfxMessageBox("Connected");


		}
		else
			AfxMessageBox("Error connecting to Colossus Adaptor");
	}

	if(m_pConn)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
//		SetTimer(2, 1000, NULL);
		SetTimer(3, 100, NULL);
	}
	else
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
//		KillTimer(2);
		KillTimer(3);
	}
}

void COmnibusTesterDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==2)
	{
		//update event display
		CString szText;
		CString szRecord;

		szText.Format("%d connections\r\n", 	m_omni.m_usNumConn);
		unsigned short i=0;
		unsigned short k=0;
		while(i<m_omni.m_usNumConn)
		{
			szRecord.Format("conn %d: %d lists\r\n", i, m_omni.m_ppConn[i]->GetListCount());
			szText+=szRecord;

			unsigned short j=0;
			while(j<m_omni.m_ppConn[i]->GetListCount())
			{
				szRecord.Format("conn %d: list %02d [%s]: %d events\r\n", i, j, m_omni.m_ppConn[i]->GetList(j)->m_pszListName, 
					m_omni.m_ppConn[i]->GetList(j)->GetEventCount());
				szText+=szRecord;


//				EnterCriticalSection(&m_omni.m_ppConn[i]->GetList(j)->m_crit);
				unsigned short k=0;
				unsigned char h,m,s,f, dh, dm, ds, df;


				if(m_omni.m_ppConn[i]->GetList(j))
				while(k < m_omni.m_ppConn[i]->GetList(j)->GetEventCount())
				{
					if((m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)))
					{

						m_omni.m_ppConn[i]->ConvertMillisecondsToHMSF(m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_ulOnAirTimeMS, 
							&h,&m,&s,&f);
						m_omni.m_ppConn[i]->ConvertMillisecondsToHMSF(m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_ulDurationMS, 
							&dh, &dm, &ds, &df);

						szRecord.Format(" event %03d: [%s] [%s] [%s] [%s] t%d, tm%d, s%d, o%02d:%02d:%02d.%02d, d%02d:%02d:%02d.%02d\r\n",
							k, 
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_pszReconcileKey,
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_pszID,
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_pszTitle,
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_pszData,
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_usType,
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_usControl,
							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_usStatus,
							h,m,s,f, dh, dm, ds, df
//							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_ulOnAirTimeMS,
//							m_omni.m_ppConn[i]->GetList(j)->GetEvent(k)->m_ulDurationMS

							);
						szText+=szRecord;
					}

					k++;
				}
//				LeaveCriticalSection(&m_omni.m_ppConn[i]->GetList(j)->m_crit);

				j++;
			}
			i++;
		}
		
		CString szNow;
		GetDlgItem(IDC_EDIT_RETURN)->GetWindowText(szNow);

		if( szNow.Compare(szText))
			GetDlgItem(IDC_EDIT_RETURN)->SetWindowText(szText);
		
	}
	else
	if(nIDEvent==3)
	{
		if(m_bUpdating) return;
		if(m_omni.m_usNumConn<1) return;
		m_bUpdating = true;
		int i=0;
		int nChannelItems = m_lcChannels.GetItemCount();
		int nChItemSoFar = 0;
		int nSelConn=-1;
		int nSelChan=-1;

//		first, get any selected list.

		bool bChanges = false;


		while(i<m_omni.m_usNumConn)
		{
			if(m_omni.m_ppConn[i]->m_ulCounter!=m_omni.m_ppConn[i]->m_ulRefTick)
			{
				bChanges = true;
				break;
			}
			i++;
		}

		int nEventsTotal=0;
		int nChannelsTotal=0;
		if(bChanges)
		{
			i=0;
			bool bInserted = false;
			while(i<m_omni.m_usNumConn)
			{
				m_omni.m_ppConn[i]->m_ulRefTick = m_omni.m_ppConn[i]->m_ulCounter;
				int j=0;
				nChannelsTotal += m_omni.m_ppConn[i]->GetListCount();
				while(j<m_omni.m_ppConn[i]->GetListCount())
				{

					char buffer[1024];
					sprintf(buffer, "[%03d] %s", 
						m_omni.m_ppConn[i]->GetList(j)->m_usListID,
						m_omni.m_ppConn[i]->GetList(j)->m_pszListName
						);

					// dont understand why FindItem did not work for this.

//					static LV_FINDINFO fi;
//					CString szKey = buffer;

//					fi.flags = LVFI_PARTIAL;
//					fi.psz = szKey;

					
	EnterCriticalSection (&m_critChannels);
					int nFoundItem = -1;//m_lcEvents.FindItem( &fi, -1 );
					

					nChannelItems = m_lcChannels.GetItemCount();
					int find=0;
					bool bFound = false;
					while((!bFound)&&(find<nChannelItems))
					{
						channelinfo_t* pchan = (channelinfo_t*) m_lcChannels.GetItemData(find);
						if(pchan)
						{
							if(pchan->nID == m_omni.m_ppConn[i]->GetList(j)->m_usListID)
							{ 
								bFound = true;
								nFoundItem = find;
							}
						}
						find++;
					}


					if(nFoundItem>=0)
					{
						if(m_lcChannels.GetItemText(nFoundItem, 0).Compare(buffer))
							m_lcChannels.SetItemText(nFoundItem, 0, buffer);
					}
					else
					{
//						AfxMessageBox(szKey);
						bInserted = true;
//						AfxMessageBox("ins");
						nFoundItem =  m_lcChannels.GetItemCount();
						m_lcChannels.InsertItem(nFoundItem, buffer);
						channelinfo_t* pchan = new channelinfo_t;
						m_lcChannels.SetItemData(nFoundItem, (unsigned long)pchan);
					}
	LeaveCriticalSection (&m_critChannels);

					channelinfo_t* pchan = (channelinfo_t*) m_lcChannels.GetItemData(nFoundItem);
					if(pchan)
					{
						pchan->nID = m_omni.m_ppConn[i]->GetList(j)->m_usListID;
						pchan->bFound = true;
					}

					sprintf(buffer, "%d events", m_omni.m_ppConn[i]->GetList(j)->GetEventCount());
					if(m_lcChannels.GetItemText(nFoundItem, 1).Compare(buffer))
						m_lcChannels.SetItemText(nFoundItem, 1, buffer);

/*
					if(m_nSelChan == m_omni.m_ppConn[i]->GetList(j)->m_usListID)
					{
						nSelConn=i;
						nSelChan=j;
//	EnterCriticalSection (&m_critChannels);
						m_lcChannels.SetItem(nFoundItem, 0, LVIF_STATE, NULL, NULL, LVNI_SELECTED, LVNI_SELECTED, NULL);
					}
	LeaveCriticalSection (&m_critChannels);
*/
					nEventsTotal +=m_omni.m_ppConn[i]->GetList(j)->GetEventCount();

					nChItemSoFar++;
					j++;
				}
				i++;
			}

/*
			while( nChannelItems > nChItemSoFar )
			{
				m_lcChannels.DeleteItem(nChItemSoFar);
				nChannelItems--;
			}
*/
			nChannelItems = m_lcChannels.GetItemCount();
			for( int del=0; del<nChannelItems; del++)
			{

				channelinfo_t* pchan = (channelinfo_t*) m_lcChannels.GetItemData(del);
				if(pchan)
				{
					if(!pchan->bFound)
					{
						m_lcChannels.DeleteItem(del);
						nChannelItems--;
						delete (pchan);
					}
					else
						pchan->bFound = false;
				}

			}

			if(bInserted)
			{

				listsortinfo_t info;
				info.pList =  &m_lcChannels;
				info.nItem = m_nChColSort;
				info.bColSortDesc = m_bChColSortDesc;

			//	char d[256];
			//	sprintf(d, "%d %d", pNMListView->iItem, pNMListView->iSubItem);
			//	AfxMessageBox(d);
	EnterCriticalSection (&m_critChannels);
				m_lcChannels.SortItems(CompareItems, (LPARAM) &info);
	LeaveCriticalSection (&m_critChannels);
			}
		
		}

		if(m_pConn==NULL) GetDlgItem(IDC_STATIC_COUNT)->SetWindowText("not connected");
		else if(bChanges)
		{
			char buffer[1024];
			sprintf(buffer, "[%s]  %d Total Events on %d Total Channels  [DB: %s]", 
				((m_pConn->m_ulFlags&OMNI_FRAMEBASISMASK) == OMNI_PAL)?"PAL - 25 fps":(((m_pConn->m_ulFlags&OMNI_FRAMEBASISMASK) == OMNI_NTSCNDF)?"NTSC NDF - 30 fps":"NTSC DF - 29.97 fps"),
				nEventsTotal, nChannelsTotal, m_pszDB);
			GetDlgItem(IDC_STATIC_COUNT)->SetWindowText(buffer);
		}
		// OK now here, we have to figure out what channel is selected. and deal.
		if(m_nSelChan>-1)
		{
			// there is a selection so need to figuh out da ting

			//find which channel index it is;
			i=0;
			while(i<m_omni.m_usNumConn)
			{
				int j=0;
				while(j<m_omni.m_ppConn[i]->GetListCount())
				{
					if(m_nSelChan == m_omni.m_ppConn[i]->GetList(j)->m_usListID)
					{
						nSelConn=i;
						nSelChan=j;
					}	
					j++;
				}
				i++;
			}

			if((nSelConn>-1)&&(nSelChan>-1))
			{
				if((m_bChangeSel)||(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->m_ulRefTick!=m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->m_ulCounter))
				{
					m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->m_ulRefTick=m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->m_ulCounter;

					// load up the event items.

					int nEventItems = m_lcEvents.GetItemCount();
					int nEvItemSoFar = 0;
					bool bInserted = false;

					unsigned short k=0;
					unsigned char h,m,s,f, dh, dm, ds, df;
					if(m_omni.m_ppConn[nSelConn]->GetList(nSelChan))
					{
						if(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEventCount()==0)
						{
							m_lcEvents.DeleteAllItems();
						}
						else
						{
							while(k < m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEventCount())
							{
								if((m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)))
								{

									m_omni.m_ppConn[nSelConn]->ConvertMillisecondsToHMSF(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_ulOnAirTimeMS, 
										&h,&m,&s,&f);
									m_omni.m_ppConn[nSelConn]->ConvertMillisecondsToHMSF(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_ulDurationMS, 
										&dh, &dm, &ds, &df);

									char buffer[1024];

									sprintf(buffer, "%f", m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_dblOnAirTimeInternal);
									unsigned long ulRefUnixTime;
									m_omni.m_ppConn[nSelConn]->ConvertTime(buffer, &ulRefUnixTime);

									char tmbuffer[256];
									const time_t* ctp = (time_t*)&ulRefUnixTime;
									tm* theTime = gmtime( ctp	);
									strftime(tmbuffer, 30, "%Y-%m-%d ", theTime );

									static LV_FINDINFO fi;
									CString szKey = m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszReconcileKey;

									fi.flags = LVFI_STRING;
									fi.psz = szKey;

									
									int nFoundItem = m_lcEvents.FindItem( &fi, -1 );


									if(nFoundItem>=0)
									{
										if(m_lcEvents.GetItemText(nFoundItem, 0).Compare(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszReconcileKey))
											m_lcEvents.SetItemText(nFoundItem, 0, m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszReconcileKey);
									}
									else
									{
										bInserted = true;
										nFoundItem =  m_lcEvents.GetItemCount();
										m_lcEvents.InsertItem(nFoundItem, m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszReconcileKey);
									}

									m_lcEvents.SetItemData(nFoundItem, 1);

									sprintf(buffer, "%s%02d:%02d:%02d.%02d", tmbuffer, h,m,s,f);
									if(m_lcEvents.GetItemText(nFoundItem, 1).Compare(buffer))
										m_lcEvents.SetItemText(nFoundItem, 1, buffer);

									sprintf(buffer, "%02d:%02d:%02d.%02d", dh,dm,ds,df);
									if(m_lcEvents.GetItemText(nFoundItem, 2).Compare(buffer))
										m_lcEvents.SetItemText(nFoundItem, 2, buffer);

									if(m_lcEvents.GetItemText(nFoundItem, 3).Compare(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszID))
										m_lcEvents.SetItemText(nFoundItem, 3, m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszID);

									if(m_lcEvents.GetItemText(nFoundItem, 4).Compare(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszTitle))
										m_lcEvents.SetItemText(nFoundItem, 4, m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszTitle);

									if(m_lcEvents.GetItemText(nFoundItem, 5).Compare(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszData))
										m_lcEvents.SetItemText(nFoundItem, 5, m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_pszData);


									switch(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_usType)
									{
									case 0: strcpy(buffer, "internal"); break;
									case 1: strcpy(buffer, "item"); break;
									case 2: strcpy(buffer, "segment"); break;
									case 3: strcpy(buffer, "live"); break;
									case 4: strcpy(buffer, "break start"); break;
									case 5: strcpy(buffer, "break end"); break;
									case 6: strcpy(buffer, "split break"); break;
									case 7: strcpy(buffer, "macro"); break;
									case 8: strcpy(buffer, "schedule"); break;
									default: strcpy(buffer, "unknown"); break;
									}

									if(m_lcEvents.GetItemText(nFoundItem, 6).Compare(buffer))
										m_lcEvents.SetItemText(nFoundItem, 6, buffer);

									switch(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_usStatus)
									{
									case 0: strcpy(buffer, "error"); break;
									case 1: strcpy(buffer, "init"); break;
									case 2: strcpy(buffer, "unchk"); break;
									case 3: strcpy(buffer, "unalloc"); break;
									case 4: strcpy(buffer, "unavail"); break;
									case 5: strcpy(buffer, "alloc"); break;
									case 6: strcpy(buffer, "cueing"); break;
									case 7: strcpy(buffer, "ready"); break;
									case 8: strcpy(buffer, "commit"); break;
									case 9: strcpy(buffer, "on-air"); break;
									case 10: strcpy(buffer, "hold"); break;
									case 11: strcpy(buffer, "done"); break;
									default: strcpy(buffer, " "); break;
									}
									if(m_lcEvents.GetItemText(nFoundItem, 7).Compare(buffer))
										m_lcEvents.SetItemText(nFoundItem, 7, buffer);


									switch(m_omni.m_ppConn[nSelConn]->GetList(nSelChan)->GetEvent(k)->m_usControl)
									{
									case 0: strcpy(buffer, "M"); break;
									case 1: strcpy(buffer, "A"); break;
									case 2: strcpy(buffer, "="); break;
									case 3: strcpy(buffer, "+"); break;
									case 4: strcpy(buffer, "-"); break;
									case 5: strcpy(buffer, "F"); break;
									default: strcpy(buffer, " "); break;
									}
									if(m_lcEvents.GetItemText(nFoundItem, 8).Compare(buffer))
										m_lcEvents.SetItemText(nFoundItem, 8, buffer);


									nEvItemSoFar++;
								}


								k++;
							}

/*
							while( nEventItems > nEvItemSoFar )
							{
								m_lcEvents.DeleteItem(nEvItemSoFar);
								nEventItems--;
							}
*/

							nEventItems = m_lcEvents.GetItemCount();
							for( int del=0; del<nEventItems; del++)
							{
								if(m_lcEvents.GetItemData(del)!=1) 
								{
									m_lcEvents.DeleteItem(del);
									nEventItems--;
								}
								m_lcEvents.SetItemData(del, 0); // reset
							}

							if(bInserted)
							{

								listsortinfo_t info;
								info.pList =  &m_lcEvents;
								info.nItem = m_nColSort;
								info.bColSortDesc = m_bColSortDesc;

							//	char d[256];
							//	sprintf(d, "%d %d", pNMListView->iItem, pNMListView->iSubItem);
							//	AfxMessageBox(d);
	EnterCriticalSection (&m_critEvents);
								m_lcEvents.SortItems(CompareItems, (LPARAM) &info);
	LeaveCriticalSection (&m_critEvents);
							}

/*
							if(m_nSelEventID>0)
							{
								static LV_FINDINFO fi;
								CString szKey;
								szKey.Format("%d", m_nSelEventID);

								fi.flags = LVFI_STRING;
								fi.psz = szKey;
								int nFoundItem = m_lcEvents.FindItem( &fi, -1 );

	EnterCriticalSection (&m_critEvents);
								if(nFoundItem>=0) m_lcEvents.SetItem(nFoundItem, 0, LVIF_STATE, NULL, NULL, LVNI_SELECTED, LVNI_SELECTED, NULL);
	LeaveCriticalSection (&m_critEvents);

							}
*/
						}
					}

				}
			}
			else
			{
				if(m_lcEvents.GetItemCount()) m_lcEvents.DeleteAllItems();
			}
		}
		else
		{
			if(m_lcEvents.GetItemCount()) m_lcEvents.DeleteAllItems();
		}

		m_bUpdating = false;
	} else
	CDialog::OnTimer(nIDEvent);
}

void COmnibusTesterDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	FILE* fp = fopen("netlast.cfg", "wb");

	if(fp)
	{
		UpdateData(TRUE);
		CString szText;
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s\n", m_szHost);
		}
		if(nCount>0)
		{
			int i=0;
			while(i<nCount)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				if(nSel==i)
				{
					fprintf(fp, "*");
				}
				fprintf(fp, "%s\n", szText);
				i++;
			}
		}
		fclose(fp);
	}
	
}

void COmnibusTesterDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID = 0;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LC_CH:				nCtrlID=IDC_LIST_CHANNELS; break;
			case ID_LC_EV:				nCtrlID=IDC_LIST_EVENTS; break;
			case ID_BN_CN:				nCtrlID=IDC_BUTTON_CONNECT; break;
			case ID_ST_EV:				nCtrlID=IDC_STATIC_COUNT; break;
			default: nCtrlID=0; break;
			}
			if(nCtrlID)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
				ScreenToClient(&m_rcCtrl[i]);
			}
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
	
}

void COmnibusTesterDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	int nCtrlID = 0;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{

		m_lcChannels.SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[ID_LC_CH].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_LC_CH].Height(),
			SWP_NOZORDER|SWP_NOMOVE
			);
		m_lcEvents.SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[ID_LC_EV].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_LC_EV].Height()-dy,
			SWP_NOZORDER|SWP_NOMOVE
			);
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BN_CN].left-dx,  // goes with right edge
			m_rcCtrl[ID_BN_CN].top,
			0,0,
			SWP_NOZORDER|SWP_NOSIZE
			);
		GetDlgItem(IDC_STATIC_COUNT)->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_ST_EV].left,  // goes with right edge
			m_rcCtrl[ID_ST_EV].top-dy,
			m_rcCtrl[ID_ST_EV].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_ST_EV].Height(),
			SWP_NOZORDER
			);


  m_lcChannels.SetColumnWidth(  0, 
    ((m_rcCtrl[ID_LC_CH].Width()-dx-20)*m_nChColPercents[0])/100 );

  m_lcChannels.SetColumnWidth(  1, 
    ((m_rcCtrl[ID_LC_CH].Width()-dx-20)*m_nChColPercents[1])/100 );


	int nSum = m_nColPercents[1]+m_nColPercents[2]+m_nColPercents[6]+m_nColPercents[7]+m_nColPercents[8]+20; // 20 is scrollbar


  m_lcEvents.SetColumnWidth(
    0, 
//    "id", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-nSum)*m_nColPercents[0])/100    );
/*
  m_lcEvents.SetColumnWidth(
    1, 
//    "On Air", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-20)*m_nColPercents[1])/100    );
  m_lcEvents.SetColumnWidth(
    2, 
//    "Dur", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-20)*m_nColPercents[2])/100    );
	*/
  m_lcEvents.SetColumnWidth(
    3, 
//    "Clip ID", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-nSum)*m_nColPercents[3])/100    );
  m_lcEvents.SetColumnWidth(
    4, 
//    "Title", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-nSum)*m_nColPercents[4])/100    );
  m_lcEvents.SetColumnWidth(
    5, 
//    "Data", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-nSum)*m_nColPercents[5])/100  );
/*  m_lcEvents.SetColumnWidth(
    6, 
//    "status, ctrl, type", 
    ((m_rcCtrl[ID_LC_EV].Width()-dx-20)*m_nColPercents[6])/100    );
*/


		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LC_CH:				nCtrlID=IDC_LIST_CHANNELS; break;
			case ID_LC_EV:				nCtrlID=IDC_LIST_EVENTS; break;
			case ID_BN_CN:				nCtrlID=IDC_BUTTON_CONNECT; break;
			case ID_ST_EV:				nCtrlID=IDC_STATIC_COUNT; break;
			default: nCtrlID=0; break;
			}
			if(nCtrlID) GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
	
}

void COmnibusTesterDlg::OnClickListChannels(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	EnterCriticalSection (&m_critChannels);
	if(m_lcChannels.GetItemCount()>0)
	{
		int nSel = m_lcChannels.GetNextItem(-1, LVNI_SELECTED|LVNI_FOCUSED|LVNI_ALL);
		if(nSel>-1)
		{
			// there is a selection
			char buffer[256];
			m_lcChannels.GetItemText(nSel, 0, buffer, 256);
			m_nSelChan = atoi(buffer+1);
		}
		else m_nSelChan=-1;

	}
	else m_nSelChan=-1;
	LeaveCriticalSection (&m_critChannels);

	m_bChangeSel = true;

	*pResult = 0;
}


static int CALLBACK CompareItems(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	// lParamSort contains a pointer to the list view control.
	// The lParam of an item is just its index.
	listsortinfo_t* pInfo = (listsortinfo_t*) lParamSort;


	static LV_FINDINFO fi;
	static int		 nItem1, nItem2;
 
	fi.flags = LVFI_PARAM;
	fi.lParam = lParam1;
	nItem1 = pInfo->pList->FindItem( &fi, -1 );
 
	fi.lParam = lParam2;
	nItem2 = pInfo->pList->FindItem( &fi, -1 );
 
	 CString    strItem1 = pInfo->pList->GetItemText(nItem1, pInfo->nItem);
	 CString    strItem2 = pInfo->pList->GetItemText(nItem2, pInfo->nItem);
//AfxMessageBox("sort");
//	char d[2256];
//	sprintf(d, "col %d: %d[%s] vs %d[%s]", pInfo->nItem, nItem1, strItem1, nItem2, strItem2);
//	AfxMessageBox(d);
	if (pInfo->bColSortDesc)
		return strcmp(strItem2, strItem1);
	else
		return strcmp(strItem1, strItem2);
}

void COmnibusTesterDlg::OnColumnclickListEvents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
//AfxMessageBox("column click");
	listsortinfo_t info;
	info.pList =  &m_lcEvents;
	info.nItem = pNMListView->iSubItem;

	if(m_nColSort != pNMListView->iSubItem)
	{
		m_bColSortDesc = false;
	}
	else
		m_bColSortDesc = !m_bColSortDesc;

	info.bColSortDesc = m_bColSortDesc;

	m_nColSort = pNMListView->iSubItem;
//	char d[256];
//	sprintf(d, "%d %d", pNMListView->iItem, pNMListView->iSubItem);
//	AfxMessageBox(d);
	EnterCriticalSection (&m_critEvents);
	m_lcEvents.SortItems(CompareItems, (LPARAM) &info);
	LeaveCriticalSection (&m_critEvents);

	*pResult = 0;
}


void COmnibusTesterDlg::OnColumnclickListChannels(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	listsortinfo_t info;
	info.pList =  &m_lcChannels;
	info.nItem = pNMListView->iSubItem;

	if(m_nChColSort != pNMListView->iSubItem)
	{
		m_bChColSortDesc = false;
	}
	else
		m_bChColSortDesc = !m_bChColSortDesc;

	info.bColSortDesc = m_bChColSortDesc;

	m_nChColSort = pNMListView->iSubItem;
//	char d[256];
//	sprintf(d, "%d %d", pNMListView->iItem, pNMListView->iSubItem);
//	AfxMessageBox(d);
	EnterCriticalSection (&m_critChannels);
	m_lcChannels.SortItems(CompareItems, (LPARAM) &info);
	LeaveCriticalSection (&m_critChannels);
	
	
	*pResult = 0;
}

void COmnibusTesterDlg::OnClickListEvents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	EnterCriticalSection (&m_critEvents);
	if(m_lcEvents.GetItemCount()>0)
	{
		int nSel = m_lcEvents.GetNextItem(-1, LVNI_SELECTED|LVNI_FOCUSED|LVNI_ALL);
		if(nSel>-1)
		{
			// there is a selection
			char buffer[256];
			m_lcEvents.GetItemText(nSel, 0, buffer, 256);
			m_nSelEventID = atoi(buffer);
		}
		else m_nSelEventID=-1;

	}
	else m_nSelEventID=-1;
	LeaveCriticalSection (&m_critEvents);
	
	*pResult = 0;
}


void COmnibusTesterDlg::OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ) 
{
	lpMMI->ptMinTrackSize.x = m_nColPercents[1]+m_nColPercents[2]+m_nColPercents[6]+m_nColPercents[7]+m_nColPercents[8]+50;
	lpMMI->ptMinTrackSize.y = 350;
}
