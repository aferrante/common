// OmnibusDefs.cpp: implementation of the COmniComm and support classes.
//
//////////////////////////////////////////////////////////////////////

#include "OmnibusDefs.h"
#include "..\..\LAN\NetUtil.h"
#include <process.h>
#include <sys/timeb.h>
#include <time.h>
#include <direct.h>


// comm thread proto
void CommThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// CCAEvent Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAEvent::CCAEvent()
{
// supplied from automation system
	m_usType = TYPE_NOT_DEFINED;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszData = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = SEGMENT_NOT_DEFINED;
	m_ulOnAirTimeMS = TIME_NOT_DEFINED;
	m_usOnAirJulianDate =0;
	m_ulDurationMS = TIME_NOT_DEFINED;
	m_usStatus = STATUS_NOT_DEFINED;
	m_usControl = STATUS_NOT_DEFINED;

// a pointer to anything we want, to contextualize this event - must use new operator to allocate
	m_pContextData = NULL;

}

CCAEvent::~CCAEvent()
{
	if(m_pszID) free(m_pszID); // must use malloc to allocate buffer;
	if(m_pszTitle) free(m_pszTitle); // must use malloc to allocate buffer;
	if(m_pszData) free(m_pszData); // must use malloc to allocate buffer;
	if(m_pszReconcileKey) free(m_pszReconcileKey); // must use malloc to allocate buffer;
	if(m_pContextData) delete(m_pContextData); // must use new to allocate object;
}

//////////////////////////////////////////////////////////////////////
// CCAList Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAList::CCAList(char* pszListName, unsigned short usListID )
{
//	InitializeCriticalSection (&m_critGEI);
//	InitializeCriticalSection (&m_critUE);
	m_pszListName = NULL;			  // basically, channel name

	if((pszListName)&&(strlen(pszListName)))
	{
		char* pch = (char*)malloc(strlen(pszListName)+1);
		if(pch)
		{
			sprintf(pch, "%s", pszListName);
//			if(m_pszListName) free(m_pszListName);  // cant be, this is the constructor
			m_pszListName = pch;

//			return OMNI_SUCCESS;
		} 
//		return OMNI_ERROR;
	}
//	else
//		return OMNI_BADARGS;

	m_usListID = usListID;
	m_ulFlags = OMNI_LIST_DISCONNECTED;
	m_ppEvents = NULL; // a pointer to an array of event pointers.
	m_usEventCount = 0; 

	m_ulRefTick=0; // last status get.
	m_ulCounter=1;		// changes counter incrementer
}

CCAList::~CCAList()
{
//	EnterCriticalSection (&m_critGEI);
//	EnterCriticalSection (&m_critUE);
	if(m_pszListName) free(m_pszListName);
	if(m_ppEvents)
	{
		for(unsigned short i=0;i<m_usEventCount;i++)
		{
			if(m_ppEvents[i]) delete(m_ppEvents[i]);
			m_ppEvents[i] = NULL;
		}
		delete [] m_ppEvents;
		m_ppEvents = NULL;
	}
//	LeaveCriticalSection (&m_critUE);
//	LeaveCriticalSection (&m_critGEI);

//	DeleteCriticalSection (&m_critUE);
//	DeleteCriticalSection (&m_critGEI);

}

	
// be sure to critical section this outside, before accessing
int CCAList::GetEventIndex(char* pszKey, unsigned long ulFlags)  // this is the unique list ID, the "story ID"
{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "GetEventIndex, checking %s\r\n", pszKey);
	fflush(fp);
	fclose(fp);
}
*/
	if((pszKey)&&(strlen(pszKey)))
	{
		if((m_ppEvents)&&(m_usEventCount>0))
		{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "GetEventIndex, checking %s, found event count %d\r\n", pszKey, m_usEventCount);
	fflush(fp);
	fclose(fp);
}
*/
			unsigned short i=0;
			while(i<m_usEventCount)
			{
				if(m_ppEvents[i])
				{
					switch(ulFlags)
					{
					case OMNI_KEY_RECKEY://								0x00000000  // key is Reconcile Key
						{
							if(m_ppEvents[i]->m_pszReconcileKey)
							{
								if(strcmp(m_ppEvents[i]->m_pszReconcileKey, pszKey)==0) return i;
							}
						} break;
					case OMNI_KEY_CLIPID://								0x00000001  // key is Clip ID
						{
							if(m_ppEvents[i]->m_pszID)
							{
								if(strcmp(m_ppEvents[i]->m_pszID, pszKey)==0) return i;
							}
						} break;
					case OMNI_KEY_TITLE://								0x00000002  // key is Title
						{
							if(m_ppEvents[i]->m_pszTitle)
							{
								if(strcmp(m_ppEvents[i]->m_pszTitle, pszKey)==0) return i;
							}
						} break;
					case OMNI_KEY_DATA://									0x00000003  // key is "data" field
						{
							if(m_ppEvents[i]->m_pszData)
							{
								if(strcmp(m_ppEvents[i]->m_pszData, pszKey)==0) return i;
							}
						} break;
					}
				}
				i++;
			}
		}
		return -1;
	}
	else
		return OMNI_BADARGS;

}

unsigned short CCAList::GetEventCount()
{
	if(m_ppEvents)
	{
		return m_usEventCount;
	}
	return 0;
}

CCAEvent* CCAList::GetEvent(unsigned short usEventIndex)
{
	if(usEventIndex<m_usEventCount)
	{
		if((m_ppEvents)&&(m_ppEvents[usEventIndex]))
		{
			return m_ppEvents[usEventIndex];
		}
	}
	return NULL;
}


// be sure to critical section this outside, before accessing
int CCAList::UpdateEvent(CCAEvent* pEvent, CDBUtil*	pdb, CDBconn* pdbConn,	char*	pszTable, char*	pszConnServer, unsigned short usConnPort)
{

/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "updating event\r\n");
	fflush(fp);
	fclose(fp);
}
*/
	if((pEvent==NULL)||(pEvent->m_pszReconcileKey==NULL)||(strlen(pEvent->m_pszReconcileKey)<=0)) return OMNI_BADARGS;

	if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
	{
	// we are using DB, so check it out and update if exists, insert if not

		//we need to encode all the values:

		char* pszEncoded[OMNI_DB_APPDATA_ID]; // only need a few of these
		for (int i=0; i<OMNI_DB_APPDATA_ID; i++)
		{
			pszEncoded[i] = NULL;
		}


		pszEncoded[OMNI_DB_CONNIP_ID] =  pdb->EncodeQuotes(pszConnServer);   // of colossus connection, varchar 16
//OMNI_DB_CONNPORT_ID				1  // of colossus connection, int
//OMNI_DB_LISTID_ID					2			// omnibus stream number, int
		pszEncoded[OMNI_DB_EVENTID_ID] =  pdb->EncodeQuotes(pEvent->m_pszReconcileKey);    // unique ID (story ID), varchar 32, recKey on harris
		pszEncoded[OMNI_DB_EVENTCLIP_ID] =  pdb->EncodeQuotes(pEvent->m_pszID);   // clip ID, varchar 32
		pszEncoded[OMNI_DB_EVENTTITLE_ID] =  pdb->EncodeQuotes(pEvent->m_pszTitle);  // title, varchar 64
		pszEncoded[OMNI_DB_EVENTDATA_ID] =  pdb->EncodeQuotes(pEvent->m_pszData);   // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
//OMNI_DB_EVENTTYPE_ID			7  // internal colossus type, int
//OMNI_DB_EVENTSTATUS_ID		8  // internal colossus status, int
//OMNI_DB_EVENTTMODE_ID			9  // time mode, int
//OMNI_DB_EVENTFSJ70_ID			10 // frames since Jan 1970, SQL float (C++ double)
//OMNI_DB_EVENTDURMS_ID			11 // duration in milliseconds, int
//OMNI_DB_EVENTUPDATED_ID		12 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500

		

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"SELECT * FROM %s \
WHERE %s = %d \
AND %s = '%s'", 
							pszTable,
							OMNI_DB_LISTID_NAME, m_usListID,
							OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
							);


//AfxMessageBox(szSQL);
		int nNumRecords=0;
		// run the query

		CRecordset* prs = pdb->Retrieve(pdbConn, szSQL);
		if(prs == NULL) 
		{
			; // send some sort of message
//							AfxMessageBox("NULL");

		}
		else
		{
			while ((!prs->IsEOF()))//&&(nIndex<nCount))
			{
				nNumRecords++;
//				CString szTemp;
//				prs->GetFieldValue(OMNI_DB_EVENTID_ID, szTemp);
//				AfxMessageBox(szTemp);
				prs->MoveNext();
			}
			prs->Close();
			delete prs;   // no longer needed.
			prs = NULL; // reset it.

			// check the items.
		}


		if(nNumRecords>0)
		{
			// update
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"UPDATE %s SET \
%s = '%s', \
%s = %d, \
%s = '%s', \
%s = '%s', \
%s = '%s', \
%s = %d, \
%s = %d, \
%s = %d, \
%s = %.0f, \
%s = %d, \
%s = %.03f \
WHERE %s = %d \
AND %s = '%s'", 
						pszTable,
						OMNI_DB_CONNIP_NAME, pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
						OMNI_DB_CONNPORT_NAME, usConnPort,  // of colossus connection, int
						OMNI_DB_EVENTCLIP_NAME,	pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
						OMNI_DB_EVENTTITLE_NAME, pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
						OMNI_DB_EVENTDATA_NAME, pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
						OMNI_DB_EVENTTYPE_NAME, pEvent->m_usType,  // internal colossus type, int
						OMNI_DB_EVENTSTATUS_NAME, pEvent->m_usStatus,  // internal colossus status, int
						OMNI_DB_EVENTTMODE_NAME, pEvent->m_usControl,  // time mode, int
						OMNI_DB_EVENTFSJ70_NAME, pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double)
						OMNI_DB_EVENTDURMS_NAME,pEvent->m_ulDurationMS,  // duration in milliseconds, int
						OMNI_DB_EVENTUPDATED_NAME, pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
						OMNI_DB_LISTID_NAME, m_usListID,
						OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
						);
//		AfxMessageBox(szSQL);
		}
		else
		{ // must insert

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"INSERT INTO %s (\
%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (\
'%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %.0f, %d, %.03f);", 
								pszTable,
								OMNI_DB_CONNIP_NAME,  // of colossus connection, varchar 16
								OMNI_DB_CONNPORT_NAME,  // of colossus connection, int
								OMNI_DB_LISTID_NAME,			// omnibus stream number, int
								OMNI_DB_EVENTID_NAME,   // unique ID (story ID), varchar 32, recKey on harris
								OMNI_DB_EVENTCLIP_NAME,  // clip ID, varchar 32
								OMNI_DB_EVENTTITLE_NAME, // title, varchar 64
								OMNI_DB_EVENTDATA_NAME,  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
								OMNI_DB_EVENTTYPE_NAME,  // internal colossus type, int
								OMNI_DB_EVENTSTATUS_NAME,  // internal colossus status, int
								OMNI_DB_EVENTTMODE_NAME,  // time mode, int
								OMNI_DB_EVENTFSJ70_NAME, // frames since Jan 1970, SQL float (C++ double)
								OMNI_DB_EVENTDURMS_NAME,// duration in milliseconds, int
								OMNI_DB_EVENTUPDATED_NAME, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
								pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
								usConnPort,  // of colossus connection, int
								m_usListID,
								pszEncoded[OMNI_DB_EVENTID_ID]?pszEncoded[OMNI_DB_EVENTID_ID]:"",
								pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
								pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
								pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
								pEvent->m_usType,  // internal colossus type, int
								pEvent->m_usStatus,  // internal colossus status, int
								pEvent->m_usControl,  // time mode, int
								pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double)
								pEvent->m_ulDurationMS,  // duration in milliseconds, int
								pEvent->m_dblUpdated // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
								);
//AfxMessageBox(szSQL);

		}

		char errorstring[DB_ERRORSTRING_LEN];

		if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//AfxMessageBox(errorstring);
		}

		for (i=0; i<OMNI_DB_APPDATA_ID; i++)
		{
			if(pszEncoded[i]) free(pszEncoded[i]);
		}

	}



//	EnterCriticalSection (&m_critGEI);
	int nIndex = GetEventIndex(pEvent->m_pszReconcileKey); // must check if it's there
	if(nIndex>=0)
	{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "event found at %d\r\n", nIndex);
	fflush(fp);
	fclose(fp);
}
*/
		//first check if the pointer is not the same
		if(pEvent != m_ppEvents[nIndex])
		{
			// if it's different, switch the thing
			delete m_ppEvents[nIndex];
			m_ppEvents[nIndex] = pEvent;
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
			m_ulCounter++;

		}
//	LeaveCriticalSection (&m_critGEI);
		return OMNI_SUCCESS;  // it's already there, we dont overwrite the list name tho.
	}
	else
	{

/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "adding new event, %s, %s, %s, %s\r\n",
	pEvent->m_pszReconcileKey,       // the "story ID"
	pEvent->m_pszID,    // the clip ID
	pEvent->m_pszTitle,
	pEvent->m_pszData  //must encode zero.
);
	fflush(fp);
	fclose(fp);
}

*/
		// we have to add it.
		CCAEvent** ppEvents = NULL;
		ppEvents = new CCAEvent*[m_usEventCount+1];

		if(ppEvents)
		{
			unsigned short i=0;
			if((ppEvents)&&(m_usEventCount))
			{
				while(i<m_usEventCount)
				{
					ppEvents[i] = m_ppEvents[i];
					i++;
				}
				delete [] m_ppEvents;
			}
			ppEvents[m_usEventCount] = pEvent; // assign new one

			m_ppEvents = ppEvents;
			m_usEventCount++;
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
			m_ulCounter++;
//	LeaveCriticalSection (&m_critGEI);
			return OMNI_SUCCESS;  
		}
		else
		{
			// couldnt allocate array!
//	LeaveCriticalSection (&m_critGEI);
			return OMNI_MEMEX;
		}
	}
}

int CCAList::DeleteAllEvents(CDBUtil*	pdb, CDBconn* pdbConn,	char*	pszTable)
{
	if((m_ppEvents)&&(m_usEventCount>0))
	{
//	EnterCriticalSection (&m_critGEI);

		if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
		{
		// we are using DB, so remove!

			char szSQL[DB_SQLSTRING_MAXLEN];
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
								"DELETE FROM %s WHERE %s = %d", 
								pszTable,
								OMNI_DB_LISTID_NAME, m_usListID
								);

			if(pdb->ExecuteSQL(pdbConn, szSQL)<DB_SUCCESS)
			{
				;  // could do a message alert at some point.
			}
		}

		// remove all
		unsigned short usEventCount = m_usEventCount;
		m_usEventCount = 0; // clear the events before deleting so no outside access.

		if(m_ppEvents)
		{
			for(unsigned short i=0;i<usEventCount;i++)
			{
				if(m_ppEvents[i]) delete(m_ppEvents[i]);
				m_ppEvents[i] = NULL;
			}
			delete [] m_ppEvents;
			m_ppEvents = NULL;
		}
		if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
		m_ulCounter++;

	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_SUCCESS;
}


// be sure to critical section this outside, before accessing
int CCAList::DeleteEvent(unsigned short usIndex, CDBUtil*	pdb, CDBconn* pdbConn,	char*	pszTable)
{
	if((m_ppEvents)&&(m_usEventCount>0)&&(usIndex<m_usEventCount)&&(m_ppEvents[usIndex]))
	{
//	EnterCriticalSection (&m_critGEI);

		if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
		{
		// we are using DB, so remove!
			char* pszEncoded = NULL;

			pszEncoded =  pdb->EncodeQuotes(m_ppEvents[usIndex]->m_pszReconcileKey);    // unique ID (story ID), varchar 32, recKey on harris

			if(pszEncoded)
			{

				char szSQL[DB_SQLSTRING_MAXLEN];
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
									"DELETE FROM %s \
WHERE %s = %d \
AND %s LIKE '%s'", 
									pszTable,
									OMNI_DB_LISTID_NAME, m_usListID,
									OMNI_DB_EVENTID_NAME, pszEncoded
									);

				if(pdb->ExecuteSQL(pdbConn, szSQL)<DB_SUCCESS)
				{
					;  // could do a message alert at some point.
				}
				free(pszEncoded);
			}
		}



		// remove it
		delete m_ppEvents[usIndex];
		m_ppEvents[usIndex] = NULL;
		m_usEventCount--;

		CCAEvent** ppEvents = NULL;
		if(m_usEventCount>0)
		{
			ppEvents = new CCAEvent*[m_usEventCount];
		}
		else
		{
			delete [] m_ppEvents;
			m_ppEvents = NULL;
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
			m_ulCounter++;
//	LeaveCriticalSection (&m_critGEI);
			return OMNI_SUCCESS;
		}

		if(ppEvents)
		{
			if(usIndex>0) memcpy(ppEvents, m_ppEvents, sizeof(CCAEvent*)*usIndex);
			while(usIndex<m_usEventCount)
			{
				ppEvents[usIndex] = m_ppEvents[usIndex+1];
				usIndex++;
			}
			delete [] m_ppEvents;
			m_ppEvents = ppEvents;
		}
		else  // could not alloc, so just shuffle
		{
			while(usIndex<m_usEventCount)
			{
				m_ppEvents[usIndex] = m_ppEvents[usIndex+1];
				usIndex++;
			}
			m_ppEvents[usIndex] = NULL;
		}
		if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
		m_ulCounter++;
//	LeaveCriticalSection (&m_critGEI);
		return OMNI_SUCCESS;
	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_ERROR;
}

int CCAList::SetFrameBasis(unsigned long ulFrameBasis)
{
	if(ulFrameBasis&OMNI_FRAMEBASISMASK)
	{
		m_ulFlags |= (ulFrameBasis&OMNI_FRAMEBASISMASK);
		m_ulFlags |= OMNI_FRAMEBASISSET;

		return OMNI_SUCCESS;
	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_ERROR;
}
//////////////////////////////////////////////////////////////////////
// CCAConn Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAConn::CCAConn()
{
	InitializeCriticalSection (&m_crit);
//	InitializeCriticalSection (&m_critRLI);
//	InitializeCriticalSection (&m_critUL);

	// used to connect:
	m_pszServerName = NULL;				// friendly name for the connection to the server.  not used programmatically
	m_pszServerAddress = NULL;		// can be host name or IP address
	m_usPort = 10540;							// port to connect
	m_ulFlags = OMNI_NTSC;				// connection options
	m_bKillThread = true;  // kill the comm thread
	m_bThreadStarted = false;  // state of comm thread
	m_bUseRepsonse = false;
	m_bWriteXMLtoFile = false;

// supplied from automation system
	m_usRefJulianDate=0;	// compiled from "current time" offset from January 1, 1900
	m_ulRefTimeMS=0;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
	m_ulRefUnixTime=0;		// compiled from "current time".

// assigned internally
	m_ppList = NULL;			// need to dynamically allocate.
	m_usListCount = 0;
	m_pTempList = NULL;		// a place to store events temporarily.  volatile.
	m_ulRefTick=0;					// last status get.

	m_ulCounter = 1;
	m_ulConnectionRetries = ULONG_MAX-1;		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
	m_ulConnectionInterval = 30000;	// how long to wait in milliseconds before retrying

	m_pszDebugFile=NULL;   // debug file
	m_pszCommFile=NULL;	   // comm log file

	m_pdb = NULL;  // pointer to a db util object to use for operations
	m_pdbConn = NULL;  // pointer to a db connection
	m_pszTable = NULL; // name of the table to update

	m_bUseUTC = true;
	m_bUseDST = false;

}

CCAConn::~CCAConn()
{
	m_bKillThread = true;  // kill the comm thread
//	DisconnectServer();  // if already disconnected, no harm
	// wait!
/*
	while(m_bThreadStarted)
	{
		Sleep(1);
	}
*/
	if(m_pszServerName) free(m_pszServerName);
	if(m_pszServerAddress) free(m_pszServerAddress);
	EnterCriticalSection (&m_crit);
//	EnterCriticalSection (&m_critRLI);
//	EnterCriticalSection (&m_critUL);
	if(m_ppList)
	{
		for(int i=0;i<m_usListCount;i++)
		{
			if(m_ppList[i]) delete(m_ppList[i]);
			m_ppList[i] = NULL;
		}
		delete [] m_ppList;
		m_ppList = NULL;
	}
	if(m_pTempList)
	{
		delete m_pTempList;
		m_pTempList = NULL;
	}
	LeaveCriticalSection (&m_crit);
//	LeaveCriticalSection (&m_critUL);
//	LeaveCriticalSection (&m_critRLI);

	DeleteCriticalSection (&m_crit);
//	DeleteCriticalSection (&m_critUL);
//	DeleteCriticalSection (&m_critRLI);
	if(m_pszDebugFile) free(m_pszDebugFile);
	if(m_pszCommFile) free(m_pszCommFile);

}

int CCAConn::SetCommLogName(char* pszFileName)
{
	if((pszFileName == NULL)||(strlen(pszFileName)<=0))	return OMNI_ERROR;
	if((m_pszServerAddress == NULL)||(strlen(m_pszServerAddress)<=0))	return OMNI_ERROR;

	char filename[MAX_PATH];
	strcpy(filename, "Logs");
	_mkdir(filename);  // if exists already np
	strcat(filename, "\\");
	strcat(filename, ((m_pszServerName!=NULL)&&(strlen(m_pszServerName)>0))? m_pszServerName : m_pszServerAddress);
	_mkdir(filename);  // if exists already np

	char* pch = NULL;
	pch = (char*)malloc(strlen(filename)+strlen(pszFileName)+2);
	if(pch)
	{
		sprintf(pch, "%s\\%s", filename, pszFileName);
		if(m_pszCommFile) free(m_pszCommFile);
		m_pszCommFile = pch;
		return OMNI_SUCCESS;
	}
	return OMNI_ERROR;
}

int CCAConn::SetDebugName(char* pszFileName)
{
	if((pszFileName == NULL)||(strlen(pszFileName)<=0))	return OMNI_ERROR;
	if((m_pszServerAddress == NULL)||(strlen(m_pszServerAddress)<=0))	return OMNI_ERROR;

	char filename[MAX_PATH];
	strcpy(filename, "Logs");
	_mkdir(filename);  // if exists already np
	strcat(filename, "\\");
	strcat(filename, ((m_pszServerName!=NULL)&&(strlen(m_pszServerName)>0))? m_pszServerName : m_pszServerAddress);
	_mkdir(filename);  // if exists already np

	char* pch = NULL;
	pch = (char*)malloc(strlen(filename)+strlen(pszFileName)+2);
	if(pch)
	{
		sprintf(pch, "%s\\%s", filename, pszFileName);
		if(m_pszDebugFile) free(m_pszDebugFile);
		m_pszDebugFile = pch;
		return OMNI_SUCCESS;
	}
	return OMNI_ERROR;
}

int CCAConn::SetFrameBasis(unsigned long ulFrameBasis)
{
	if(ulFrameBasis&OMNI_FRAMEBASISMASK)
	{
	//	char x[356]; sprintf(x,"flags before = %08x", m_ulFlags); AfxMessageBox(x);
		m_ulFlags &= (~OMNI_FRAMEBASISMASK);

		m_ulFlags |= (ulFrameBasis&OMNI_FRAMEBASISMASK);
		m_ulFlags |= OMNI_FRAMEBASISSET;

		//char x[356];
	//	sprintf(x,"flags after = %08x", m_ulFlags); AfxMessageBox(x);

		return OMNI_SUCCESS;
	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_ERROR;
}


CCAList* CCAConn::GetList(unsigned short usListIndex)
{
	if(usListIndex<m_usListCount)
	{
		if((m_ppList)&&(m_ppList[usListIndex]))
		{
			return m_ppList[usListIndex];
		}
	}
	return NULL;

}

unsigned short CCAConn::GetListCount()
{
	if(m_ppList)
	{
		return m_usListCount;
	}
	return 0;
}

int CCAConn::ConnectServer()
{
	if((m_pszServerAddress)&&(strlen(m_pszServerAddress)))
	{
		m_bKillThread = false;  // dont kill the comm thread
		
		if(_beginthread(CommThread, 0, this)>=0)
		{
			return OMNI_SUCCESS;
		}
		else m_bKillThread = true;  // kill the comm thread
	}
	return OMNI_ERROR;
}

int CCAConn::DisconnectServer()
{
	m_bKillThread = true;  // kill the comm thread

	return OMNI_SUCCESS;
}

int CCAConn::ReturnListIndex(unsigned short usListID)
{

/// debug
/*
FILE* fp;
fp = fopen("omnibus.log", "at");


if(fp)
{
	fprintf(fp, "before lookup: listing assignment\r\n");
	fflush(fp);
}

				unsigned short i=0;
				if((m_ppList)&&(m_usListCount))
				{
					while(i<m_usListCount)
					{
if(fp)
{
	fprintf(fp, "looking: %d has 0x%08x\r\n",  i, m_ppList[i]);
	fflush(fp);
}

						i++;
					}
				}
if(fp)
{
	fprintf(fp, "done with pre-lookup listing\r\n");
	fflush(fp);
	fclose(fp);
}
////// end debug
*/

	if((m_ppList)&&(m_usListCount>0))
	{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "checking %d lists for %d\r\n", m_usListCount, usListID);
	fflush(fp);
	fclose(fp);
}
*/
		unsigned short i=0;
		while(i<m_usListCount)
		{
			if(m_ppList[i])
			{

/*fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "checking [%d]:  %d ?= %d\r\n", i, m_ppList[i]->m_usListID, usListID);
	fflush(fp);
	fclose(fp);
}
*/
				if(usListID == m_ppList[i]->m_usListID) return i;
			}
			i++;
		}
	}
	return -1;
}

int CCAConn::UpdateList(unsigned short usListID, char* pszListName, unsigned long ulFlags, int* pnListID)
{
	if(pszListName==NULL) return OMNI_BADARGS;
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "update list called: %s, %d\r\n", pszListName, usListID);
	fflush(fp);
	fclose(fp);
}
*/
//	EnterCriticalSection (&m_critRLI);

	int nIndex=-1;

	if((pnListID)&&(*pnListID>=0))
		nIndex = *pnListID;
	else
		nIndex = ReturnListIndex(usListID);
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "looked up list %s, %d: %d\r\n", pszListName, usListID, nIndex);
	fflush(fp);
	fclose(fp);
}
*/
	if(nIndex>=0)
	{
		if(m_ppList[nIndex]->m_ulFlags != ulFlags)
		{
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
			m_ulCounter++;
			if(m_ppList[nIndex]->m_ulCounter >= ULONG_MAX) m_ppList[nIndex]->m_ulCounter=0;
			m_ppList[nIndex]->m_ulCounter++;
		}

		m_ppList[nIndex]->m_ulFlags = ulFlags;
//		LeaveCriticalSection (&m_critRLI);

		return OMNI_SUCCESS;  // it's already there, we dont overwrite the list name tho.
	}
	else
	{
		// we have to add it.
		CCAList* pList = new CCAList(pszListName, usListID);
		if(pList)
		{
			pList->m_ulFlags = ulFlags;
			CCAList** ppList = NULL;
			ppList = new CCAList*[m_usListCount+1];
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "adding list %s, %d\r\n", pszListName, usListID);
	fflush(fp);
	fclose(fp);
}
*/
			if(ppList)
			{
				unsigned short i=0;
				if((m_ppList)&&(m_usListCount))
				{
					while(i<m_usListCount)
					{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "assigning list 0x%08x to %d\r\n", m_ppList[i], i);
	fflush(fp);
	fclose(fp);
}
*/
						ppList[i] = m_ppList[i];
						i++;
					}
					delete [] m_ppList;
				}
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "assigning list 0x%08x to %d\r\n", pList, m_usListCount);
	fflush(fp);
	fclose(fp);
}
*/
				ppList[m_usListCount] = pList; // assign new one
				m_ppList = ppList;

				if(pnListID) *pnListID = m_usListCount;
				m_usListCount++;  // increment after all is set up
				
/// debug
/*
FILE* fp;
fp = fopen("omnibus.log", "at");

if(fp)
{
	fprintf(fp, "listing assignment\r\n");
	fflush(fp);
}
				i=0;
				if((m_ppList)&&(m_usListCount))
				{
					while(i<m_usListCount)
					{
if(fp)
{
	fprintf(fp, "assigned: %d has 0x%08x\r\n",  i, m_ppList[i]);
	fflush(fp);
}

						i++;
					}
				}
if(fp)
{
	fprintf(fp, "done with assignment\r\n");
	fflush(fp);
	fclose(fp);
}
////// end debug
*/

				// signal change
				if(m_ulCounter >= ULONG_MAX) m_ulCounter=0;
				m_ulCounter++;
//	LeaveCriticalSection (&m_critRLI);

				return OMNI_SUCCESS;  
			}
			else
			{
				// couldnt allocate array!
//	LeaveCriticalSection (&m_critRLI);
				return OMNI_MEMEX;
			}
		}
		else
		{
			// couldnt allocate object!
//	LeaveCriticalSection (&m_critRLI);
			return OMNI_MEMEX;
		}
	}
}


// core - event search
int CCAConn::GetRelativeIndex(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags)
{

	return -1;
}

int CCAConn::GetNumSecondaries(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags)
{

	return -1;
}

// utility
 // returns milliseconds in current day, -1 if error.
int CCAConn::ConvertTime(char* pszTimeString, unsigned long* pulRefUnixTime, unsigned short* pusJulianDate)  // returns milliseconds in current day, -1 if error.
{
	if((pszTimeString)&&(strlen(pszTimeString)))
	{
		int nMillisecondsInDay=-1;

		double dblFrames = atof(pszTimeString);  // frames since January 1970.  (I think it means jan 1, 12:00 am (local), but this is how it is in the omnibus documentation)
		double dblFrameBasis = 29.97;
		switch(m_ulFlags&OMNI_FRAMEBASISMASK)
		{
		case OMNI_PAL://											0x00000010  // PAL	(25 fps)
			{
				dblFrameBasis = 25.0;
			} break;
		case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
			{
				dblFrameBasis = 30.0;
			} break;
		default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
			break;
		}

		double dblMilliseconds = (dblFrames*1000.0/dblFrameBasis);
		unsigned short usJulianDate=25567;  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
		while (dblMilliseconds>86400000.0)
		{
			dblMilliseconds-=86400000.0; // need to get the remainder.
			usJulianDate++;  // add up days
		}

		nMillisecondsInDay = (int)(dblMilliseconds);

		//let's get the unixtime (though, not GMT offset, local)
		if(pulRefUnixTime)
		{
			unsigned long  ulRefUnixTime=0;
			double dblSeconds = (dblFrames/dblFrameBasis);
			if(dblSeconds<0.0) 
				ulRefUnixTime = 0;
			else
			{
				while (dblSeconds>(double)0xffffffff) dblSeconds-=(double)0xffffffff; // epoch maxed by size of unsigned long
				ulRefUnixTime = (unsigned long) dblSeconds;
			}
			*pulRefUnixTime = ulRefUnixTime;
		}

		//let's get the julian date
		if(pusJulianDate)
		{
			*pusJulianDate = usJulianDate;
		}
		return nMillisecondsInDay;
	}
	return OMNI_ERROR;
}

int	CCAConn::ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned long ulFlags) 
{
	if(ulFlags&OMNI_HEX)  // means, if BCD
	{
/*
		char temp[32];
		sprintf(temp, "%02x", ucHours);		ucHours = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucMinutes);	ucMinutes = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucSeconds);	ucSeconds = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucFrames);	ucFrames = (unsigned char)(atoi(temp));
*/		
		unsigned char ucTemp = 	(ucHours&0x0f)+(((ucHours&0xf0)>>4)*10); ucHours = ucTemp;
		ucTemp = 	(ucMinutes&0x0f)+(((ucMinutes&0xf0)>>4)*10); ucMinutes = ucTemp;
		ucTemp = 	(ucSeconds&0x0f)+(((ucSeconds&0xf0)>>4)*10); ucSeconds = ucTemp;
		ucTemp = 	(ucFrames&0x0f)+(((ucFrames&0xf0)>>4)*10); ucFrames = ucTemp;
	}
	if((ucHours>23)||(ucHours<0)) return TIME_NOT_DEFINED;
	if((ucMinutes>59)||(ucMinutes<0)) return TIME_NOT_DEFINED;
	if((ucSeconds>59)||(ucSeconds<0)) return TIME_NOT_DEFINED;

	switch(m_ulFlags&OMNI_FRAMEBASISMASK)
	{
	case OMNI_PAL://											0x00000010  // PAL	(25 fps)
		{
			if((ucFrames>24)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/25); //parens for rounding error
		} break;
	case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
		{
			if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/30); //parens for rounding error
		} break;
	default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
		{
			if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(int)(((double)(ucFrames)*1000.0)/29.97)); //parens for rounding error
		} break;

	}
}

int	CCAConn::ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned long ulFlags)
{
	*pucHours		= ((unsigned char)(ulMilliseconds/3600000L))&0xff;
	*pucMinutes = ((unsigned char)((ulMilliseconds/60000L)%60))&0xff;	
	*pucSeconds = ((unsigned char)(((ulMilliseconds/1000L)%60)%60))&0xff;
	switch(m_ulFlags&OMNI_FRAMEBASISMASK)
	{
	case OMNI_PAL://											0x00000010  // PAL	(25 fps)
		{
			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/40)))&0xff;  //(int)(1000.0/25.0) = 40
		} break;
	case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
		{
			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/33)))&0xff;  //(int)(1000.0/30.0)  = (int)(33.333333) = 33
		} break;
	default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
		{
			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/33)))&0xff;  //(int)(1000.0/29.97) = (int)(33.366700) = 33
		} break;

	}

	if(ulFlags&OMNI_HEX)  // means, if BCD
	{
		unsigned char ucTemp = *pucHours;
		*pucHours = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucMinutes;
		*pucMinutes = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucSeconds;
		*pucSeconds = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucFrames;
		*pucFrames = ((ucTemp/10)*16) + (ucTemp%10);
	}
	return OMNI_SUCCESS;  // meaningless
}

void CommThread(void* pvArgs)
{
	CCAConn* pConn = (CCAConn*) pvArgs;
	FILE* fp;
	if(pConn)
	{
		if(pConn->m_bThreadStarted) return; // only one per conn.
		pConn->m_bThreadStarted = true;  // state of comm thread

		CNetUtil	net;  // the connection exclusive networking obj
		SOCKET s;
		unsigned long ulRetry =0 ;

						// create tables if they dont exist.
		if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected)&&(pConn->m_pszTable)&&(strlen(pConn->m_pszTable)))
		{
			char szSQL[DB_SQLSTRING_MAXLEN];
			pConn->m_pdb->AddTable(pConn->m_pdbConn, pConn->m_pszTable); // add errors table to schema
			if(pConn->m_pdb->TableExists(pConn->m_pdbConn, pConn->m_pszTable)==DB_EXISTS)
			{
				// get the schema
				pConn->m_pdb->GetTableInfo(pConn->m_pdbConn, pConn->m_pszTable);

			}
			else
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 

"CREATE TABLE [%s] (\"%s\" varchar(16) NOT NULL, \"%s\" int NOT NULL, \
\"%s\" int NOT NULL, \"%s\" varchar(32) NOT NULL, \"%s\" varchar(32) NOT NULL, \
\"%s\" varchar(64) NOT NULL, \"%s\" varchar(4096) NULL, \"%s\" int NOT NULL, \
\"%s\" int NOT NULL, \"%s\" int NOT NULL, \"%s\" float NOT NULL, \
\"%s\" int NOT NULL, \"%s\" float NOT NULL, \"%s\" varchar(512) NULL, \
\"%s\" int NULL);",
										pConn->m_pszTable,
										OMNI_DB_CONNIP_NAME, //				"conn_ip"    // of colossus connection, varchar 16
										OMNI_DB_CONNPORT_NAME, //			"conn_port"  // of colossus connection, int
										OMNI_DB_LISTID_NAME, //				"list_id"			// omnibus stream number, int
										OMNI_DB_EVENTID_NAME, //			"event_id"    // unique ID (story ID), varchar 32, recKey on harris
										OMNI_DB_EVENTCLIP_NAME, //		"event_clip"  // clip ID, varchar 32
										OMNI_DB_EVENTTITLE_NAME, //		"event_title" // title, varchar 64
										OMNI_DB_EVENTDATA_NAME, //		"event_data"  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
										OMNI_DB_EVENTTYPE_NAME, //		"event_type"  // internal colossus type, int
										OMNI_DB_EVENTSTATUS_NAME, //	"event_status"  // internal colossus status, int
										OMNI_DB_EVENTTMODE_NAME, //		"event_time_mode"  // time mode, int
										OMNI_DB_EVENTFSJ70_NAME, //		"event_start" // frames since Jan 1970, SQL float (C++ double)
										OMNI_DB_EVENTDURMS_NAME, //		"event_duration" // duration in milliseconds, int
										OMNI_DB_EVENTUPDATED_NAME, //	"event_last_update" // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
											// fields from other apps
										OMNI_DB_APPDATA_NAME, //			"app_data" // application data, varchar 512, allow null
										OMNI_DB_APPDATAAUX_NAME	//	"app_data_aux" // auxiliary application data, int, allow null
									);

					//					AfxMessageBox(szSQL);


				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)>=DB_SUCCESS)
				{
					pConn->m_pdb->GetTableInfo(pConn->m_pdbConn, pConn->m_pszTable);
				}
				// else error notice...

			}

			// now that the table existence has been ensured, delete things more than a day old.
			timeb timestamp;
			ftime(&timestamp);

			double dblTime;
			if(pConn->m_bUseUTC)
			{
				dblTime = (double)(timestamp.time - 86400); // UTC
			}
			else
			{
				dblTime = (double)(timestamp.time - 86400 - (timestamp.timezone*60));  // local time
			}

			if(pConn->m_bUseDST)
			{
				dblTime += (double)(timestamp.dstflag?3600:0);  // DST
			}



			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
									"DELETE FROM %s WHERE %s < %.03f;", 
									pConn->m_pszTable,
									OMNI_DB_EVENTUPDATED_NAME, dblTime
									);
										//AfxMessageBox(szSQL);

			if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
			{
				;  // could do a message alert at some point.
				//AfxMessageBox("error deleting old");
			}

		}

		while((ulRetry<pConn->m_ulConnectionRetries)&&(!pConn->m_bKillThread))
		{
			if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "Trying connection to %s:%d...\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
					fflush(fp);
					fclose(fp);

				}
			}

			if(net.OpenConnection(
				pConn->m_pszServerAddress, 
				pConn->m_usPort, 
				&s,
				1000, 1000  // 1000 millisecond com timeouts to not freeze thread
				)>=NET_SUCCESS)
			{	
				pConn->m_ulFlags &= ~OMNI_SOCKETERROR;
				ulRetry = 0;
				char* pchBuffer = NULL;
				unsigned long ulBufferLen = 0;

				char buffer[32];
				strcpy(buffer, "<mos><roReq></roReq></mos>" );
				ulBufferLen = strlen(buffer)+1; //send the term 0

				if (pConn->m_bKillThread)
				{
					net.CloseConnection(s);
					pConn->m_bThreadStarted = false;  // state of comm thread
					_endthread();
				}
				// first, set up the connection by sending <mos><roReq></roReq></mos>
				int n = net.SendLine((unsigned char*)buffer, ulBufferLen, s);//, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
				
	/*
				fp = fopen("omnibus.log", "at");
				if(fp)
				{
					fprintf(fp, "SendLine returned %d\r\n", n);
					fflush(fp);
					fclose(fp);
				}
	*/
				char* pch = NULL;
				char* pchXML = NULL;
				char* pchXMLStream = NULL;
				unsigned long ulAccumulatedBufferLen = 0;
				char filename[MAX_PATH];
				char lastfilename[MAX_PATH];
				int nDupes=0;

				// set up the buffer for Ack
				strcpy(buffer, "<mos><roAck/></mos>" );

				while(!pConn->m_bKillThread)  // kill the comm thread
				{
					_timeb timestamp;
					pchBuffer = NULL;
					ulBufferLen = 0;

/*
_ftime( &timestamp );

char buffer[256];
tm* theTime = localtime( &timestamp.time	);
strftime(buffer, 30, "calling getline %d-%H:%M:%S.", theTime );
int nOffset = strlen(buffer);
sprintf(buffer+nOffset,"%03d\r\n",timestamp.millitm);

fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite(buffer, 1, strlen(buffer),fp);
	fflush(fp);
	fclose(fp);
}
*/

					char chError[NET_ERRORSTRING_LEN];
					if (pConn->m_bKillThread)
					{
						net.CloseConnection(s);
						pConn->m_bThreadStarted = false;  // state of comm thread
						_endthread();
					}
					int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, s, NET_RCV_ONCE, chError);
	
					
/*_ftime( &timestamp );

theTime = localtime( &timestamp.time	);
strftime(buffer, 30, "called getline  %d-%H:%M:%S.", theTime );
nOffset = strlen(buffer);
sprintf(buffer+nOffset,"%03d returned\r\n",timestamp.millitm);

fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite(buffer, 1, strlen(buffer),fp);
	fflush(fp);
	fclose(fp);
}
*/

						
					if(nReturnCode == NET_SUCCESS)
					{
						//process any received XML.


if((pConn->m_pszCommFile)&&(strlen(pConn->m_pszCommFile)))
{
	fp = fopen(pConn->m_pszCommFile, "at");
	if(fp)
	{
		fwrite(pchBuffer, 1, ulBufferLen,fp);
		fflush(fp);
		fclose(fp);
	}
}

						// have to keep accumulating until we find a </mos> tag.
						if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))
						{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("recd\r\n", 1, strlen("recd\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/


/*
	// debug the receipt
						_ftime( &timestamp );

						tm* theTime = localtime( &timestamp.time	);
						strftime(filename, 30, "logs\\received%d-%H%M%S", theTime );
						int nOffset = strlen(filename);
						sprintf(filename+nOffset,"%03d.txt",timestamp.millitm);

						fp = fopen(filename, "wb");
	//					fp = fopen("omnibus.log", "at");
						if(fp)
						{
							fwrite(pchBuffer, 1, ulBufferLen,fp);
							fflush(fp);
							fclose(fp);
						}
	// end debug
*/
						
							int nLen = 0;
							if(pchXMLStream) nLen = ulAccumulatedBufferLen;
							pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
							if(pch)
							{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("pch\r\n", 1, strlen("pch\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/								
								char* pchEnd = NULL;
								char* pchNext = NULL;
								if(pchXMLStream)  // we have an old buffer.
								{
									memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
									memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
									free(pchXMLStream); 
									pchXMLStream = pch;  // reassign!
									ulAccumulatedBufferLen += ulBufferLen;
									*(pch+ulAccumulatedBufferLen) = 0;  // null term
								}
								else
								{
									// this is new.
									// first we have to skip all chars that are not a '<'
									// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
									// used to use strchr but if there are leading zeros in the buffer, we never get past them

									pchEnd = pchBuffer;
									while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

									if(pchEnd<pchBuffer+ulBufferLen)
									{
										strcpy(pch, pchEnd);
										ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
										pchXMLStream = pch;  // reassign!
										*(pch+ulAccumulatedBufferLen) = 0;  // null term
									}
									else  // not found!
									{
										free(pch);
										pchXMLStream = NULL;
									}
								}

								if(pchBuffer) free(pchBuffer);
								pchBuffer = NULL;

/*fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("pchBuffer\r\n", 1, strlen("pchBuffer\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

								// now, pchXMLStream has everything so far.
/*
	if(pchXMLStream)
	{
		strftime(filename, 30, "logs\\debugaccumbuf%d-%H%M%S", theTime );
		int nOffset = strlen(filename);
		sprintf(filename+nOffset,"%03d.txt",timestamp.millitm);

		fp = fopen(filename, "wb");
//					fp = fopen("omnibus.log", "at");
		if(fp)
		{
			fwrite(pchXMLStream, 1, ulAccumulatedBufferLen,fp);
			fflush(fp);
			fclose(fp);
		}
	}
*/
								if(pchXMLStream)
								{
									pchEnd = strstr(pchXMLStream, "</mos>");
									while((pchEnd)&&(!pConn->m_bKillThread))
									{

/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("mos\r\n", 1, strlen("mos\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
									// found a token.
										pchEnd+=strlen("</mos>");

										pchNext = pchEnd;
										while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

										if(pchNext<pchEnd+strlen(pchEnd))
										{
											// we found a remainder.
											nLen = strlen(pchNext);
											pch = (char*) malloc(nLen+1);  //term 0
											if(pch)
											{
												memcpy(pch, pchNext, nLen);
												*(pch+nLen) = 0;
											}
										}
										else pch = NULL;


										pchXML = pchXMLStream; // just use it.
										*pchEnd = 0; //null terminate it

										pchXMLStream = pch;  // take the rest of the stream.
										if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
										else ulAccumulatedBufferLen=0;

										// now have to deal with XML found in pchXML.


										if(pConn->m_bUseRepsonse)
										{
											ulBufferLen = strlen(buffer)+1; //send the term 0
											n = net.SendLine((unsigned char*)buffer, ulBufferLen, s);//, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
										}

										//debug file write
										if(pConn->m_bWriteXMLtoFile)
										{
											strcpy(filename, "Logs");
											_mkdir(filename);  // if exists already np
											strcat(filename, "\\");
											strcat(filename, ((pConn->m_pszServerName!=NULL)&&(strlen(pConn->m_pszServerName)>0))? pConn->m_pszServerName : pConn->m_pszServerAddress);

											_mkdir(filename);  // if exists already np

											_ftime( &timestamp );

											tm* theTime = localtime( &timestamp.time	);

											char filenametemp[MAX_PATH];

											if(strstr(pchXML, "<heartbeat>"))
												strftime(filenametemp, MAX_PATH-1, "\\Heartbeat_Message_Day_%d-%H.%M.%S.", theTime );
											else
												strftime(filenametemp, MAX_PATH-1, "\\Output_Message_Day_%d-%H.%M.%S.", theTime );

											strcat(filename, filenametemp);

											int nOffset = strlen(filename);
											sprintf(filename+nOffset,"%03d",timestamp.millitm);

											if (strcmp(lastfilename, filename)==0)
											{
												nDupes++;
											}
											else
											{
												nDupes=0;
												strcpy(lastfilename, filename);
											}
											nOffset = strlen(filename);
											sprintf(filename+nOffset,"%02d.xml",nDupes);

											fp = fopen(filename, "wb");
											if(fp)
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												fwrite(pchXML, 1, strlen(pchXML),fp);
												fflush(fp);
												fclose(fp);
											}
		/*									else
											{
												fp = fopen("logs\\omnibus.log", "at");
											
												if(fp)
												{
													fwrite(pchXML, 1, strlen(pchXML),fp);
													fflush(fp);
													fclose(fp);
												}
											}
	*/
										}
									//// end debug file write

										// ok so, here is the XML, in pchXML
										// let's populate the channels and the event information,

										if(strstr(pchXML, "<heartbeat>"))
										{
											// heartbeat msg
											// find the list name:

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;
											bool bConnected = false;


											// TODO: may want to change this later to not bother allocating unconnected lists.

											pchName = strstr(pchXML, "<channel>");
											while((pchName)&&(!pConn->m_bKillThread))
											{
												usListID = LIST_NOT_DEFINED;
												bConnected = false;
												pchEnd = strstr(pchName, "</channel>");
												if(pchEnd)  // we found a correctly encapsulted record.
												{
													pchNext = strstr(pchName, "<id>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<id>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<connected>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<connected>");
															bConnected = atoi(pchNext)==0?false:true;
														}
														pchNext = strstr(pchName, "<name>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<name>");
															pch = strstr(pchName, "</name>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else  strcpy(pchNext, ""); // blank name
														}
														else strcpy(pchNext, ""); // blank name

														// add the list!
if(!pConn->m_bKillThread) 	EnterCriticalSection (&pConn->m_crit);
														pConn->UpdateList(usListID, pchNext, bConnected?OMNI_LIST_CONNECTED:OMNI_LIST_DISCONNECTED);
	LeaveCriticalSection (&pConn->m_crit);
													}

													pchName = strstr(pchEnd, "<channel>");  // find the next one.
												} // if(pchEnd)
											}//while ((pchName)&&(!pConn->m_bKillThread))
										}
										else
										{
											// output message.
											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;

											pchName = strstr(pchXML, "<roReplace>");
											if(pchName)
											{
												pchEnd = strstr(pchName, "</roReplace>");
												if(pchEnd)  // we found a correctly encapsulated record.
												{
													pchNext = strstr(pchName, "<roID>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<roID>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<roSlug>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<roSlug>");
															pch = strstr(pchName, "</roSlug>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else
															{
																pchNext = NULL; // blank name
																pch = pchName;
															}
														}
														else
														{
															pchNext = NULL; // blank name
															pch = pchName;
														}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "found %s in roReplace, about to update\r\n",pchNext);
		fflush(fp);
		fclose(fp);
	}
}

														// add or update the list!
if(!pConn->m_bKillThread)  EnterCriticalSection (&pConn->m_crit);
														pConn->UpdateList(usListID, pchNext?pchNext:"", OMNI_LIST_CONNECTED);
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "updated, checking for events\r\n");
		fflush(fp);
		fclose(fp);
	}
}

														int nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);

														// then, deal with any events we might have.
														pchNext = strstr((pch+1), "<story>");

////////////////////////////////////////////////////////////////////////////////////
// deletion of all events
//
// we arent able to just delete on a blank roReplaceMessage,
// as it turns out, the Adaptor will once in a while send one of these
// for whatever reason, when nothing has changed and nothing needs to be replaced												
/*
														if(pchNext == NULL) // we don't have any events.  
														// This seems to be only sent if the list is initially blank on connect, 
														// or if the live schedule is deleted.  Interestingly, no deletion events are sent in this case.
														{
if(!pConn->m_bKillThread) EnterCriticalSection(&pConn->m_crit);
															CCAList* pList = pConn->GetList(nListIndex);
															if(pList)
															{
																if(!pConn->m_bKillThread)
																{
																	pList->DeleteAllEvents( pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszTable);
																}
																if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=0;
																pConn->m_ulCounter++;
															}
LeaveCriticalSection(&pConn->m_crit);


														}
*/
														while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
														{
															char* pchEndRec = pchNext+strlen("<story>");
															pchName = strstr(pchNext, "</story>");
															if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
															{
																pchEndRec = pchName;
																// ok, now pchNext is the beginning, 
																// and pchEnd is the end of the list,
																// pchEndRec is the end of the record, and we can use pchName for the fields
																// we are interested in getting:
																
																//unsigned short m_usType;              <type>      // internal colossus type
																//char* m_pszID;												<clipId>		// the clip ID
																//char* m_pszTitle;											<title>		  // the title
																//char* m_pszData;  										<data> 			//must encode zero.
																//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																//unsigned char  m_ucSegment;														// not used

																// following 2 compiled from <startTime>
																//unsigned long  m_ulOnAirTimeMS;
																//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																//unsigned long  m_ulDurationMS;        <dur>        // duration
																//unsigned short m_usStatus;						<status>     // status (omnibus code)
																//unsigned short m_usControl;           <timeMode>   // time mode

																// additionally, up at the list level, we can compile from
																//  <currentTime>
																//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																// let's create a new event object;
																CCAEvent* pEvent = new CCAEvent;
																char* pchField;

																pchName = strstr(pchNext, "<storyID>");
																if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																{
																	pchName += strlen("<storyID>");
																	pch = strstr(pchName, "</storyID>");
																	if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																	{
																		*pch = 0;  //null term the ID!
																		pchField = (char*) malloc(strlen(pchName)+1);
																		if(pchField)  // have to at least have this field
																		{
																			strcpy(pchField, pchName);
																			pEvent->m_pszReconcileKey = pchField;
																			*pch = '<'; // put it back so we can search whole string again.

																			pchName = strstr(pchNext, "<clipId>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<clipId>");
																				pch = strstr(pchName, "</clipId>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					*pch = 0;  //null term the ID!
																					pchField = (char*) malloc(strlen(pchName)+1);
																					if(pchField)  // have to at least have this field
																					{
																						strcpy(pchField, pchName);
																						pEvent->m_pszID = pchField;
																						*pch = '<'; // put it back so we can search whole string again.
																					}
																				}
																			}


																			pchName = strstr(pchNext, "<title>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<title>");
																				pch = strstr(pchName, "</title>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					*pch = 0;  //null term the ID!
																					pchField = (char*) malloc(strlen(pchName)+1);
																					if(pchField)  // have to at least have this field
																					{
																						strcpy(pchField, pchName);
																						pEvent->m_pszTitle = pchField;
																						*pch = '<'; // put it back so we can search whole string again.
																					}
																				}
																			}


																			pchName = strstr(pchNext, "<data>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<data>");
																				pch = strstr(pchName, "</data>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					*pch = 0;  //null term the ID!
																					pchField = (char*) malloc(strlen(pchName)+1);
																					if(pchField)  // have to at least have this field
																					{
																						strcpy(pchField, pchName);
																						pEvent->m_pszData = pchField;
																						*pch = '<'; // put it back so we can search whole string again.
																					}
																				}
																			}

																			pchName = strstr(pchNext, "<startTime>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<startTime>");
																				pch = strstr(pchName, "</startTime>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					pEvent->m_dblOnAirTimeInternal = atof(pchName);
																					int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate));  // returns milliseconds in current day, -1 if error.
																					if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																					else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																				}
																			}

																			pchName = strstr(pchNext, "<currentTime>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<currentTime>");
																				pch = strstr(pchName, "</currentTime>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																					if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																					//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																				}
																			}

																			pchName = strstr(pchNext, "<type>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<type>");
																				pch = strstr(pchName, "</type>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					pEvent->m_usType = (unsigned short)atoi(pchName);
																				}
																			}

																			pchName = strstr(pchNext, "<dur>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<dur>");
																				pch = strstr(pchName, "</dur>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																					if(nMS>=0) pEvent->m_ulDurationMS = (unsigned long) nMS;
																					else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																				}
																			}

																			pchName = strstr(pchNext, "<status>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<status>");
																				pch = strstr(pchName, "</status>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					pEvent->m_usStatus = (unsigned short)atoi(pchName);
																				}
																			}

																			pchName = strstr(pchNext, "<timeMode>");
																			if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																			{
																				pchName += strlen("<timeMode>");
																				pch = strstr(pchName, "</timeMode>");
																				if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																				{
																					pEvent->m_usControl = (unsigned short)atoi(pchName);
																				}
																			}

//																			Ok, lets update the event!
																			if(nListIndex>=0)
																			{
if(!pConn->m_bKillThread) EnterCriticalSection(&pConn->m_crit);
																				CCAList* pList = pConn->GetList(nListIndex);
																				if(pList)
																				{
																					if(!pConn->m_bKillThread) 
																					{

																						double dblTime = ((double)timestamp.millitm/1000.0);
																						if(pConn->m_bUseUTC)
																						{
																							dblTime += (double)(timestamp.time); // UTC
																						}
																						else
																						{
																							dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																						}

																						if(pConn->m_bUseDST)
																						{
																							dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																						}

																						pEvent->m_dblUpdated = dblTime;  // UTC 
																						 
																						pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszTable, pConn->m_pszServerAddress, pConn->m_usPort);
																					}
																				}
																				if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=0;
																				pConn->m_ulCounter++;
LeaveCriticalSection(&pConn->m_crit);
																			}
																		}
																	}
																}
																//else // cant do anything with this event, so just have to skip.
															}

															pchNext = strstr(pchEndRec, "<story>");
														}
													}
												}
											}
											else  // not roReplace
											{
												pchName = strstr(pchXML, "<roStoryReplace>");
												if(pchName)
												{
													// it's a replace single item record
													pchEnd = strstr(pchName, "</roStoryReplace>");
													if(pchEnd)  // we found a correctly encapsulated record.
													{
														pchNext = strstr(pchName, "<roID>");
														if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
														{
															pchNext += strlen("<roID>");
															usListID = atoi(pchNext);
if(!pConn->m_bKillThread) EnterCriticalSection (&pConn->m_crit);
															int nListIndex = pConn->ReturnListIndex(usListID);
LeaveCriticalSection (&pConn->m_crit);


															// then, deal with the events we have.
															pchNext = strstr(pchName, "<story>");
															while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))  // this while should only get hit once.
															{
																pchName = strstr(pchNext, "</story>");
																if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																{
																	char* pchEndRec = pchName;
																	// ok, now pchNext is the beginning, 
																	// and pchEnd is the end of the list,
																	// pchEndRec is the end of the record, and we can use pchName for the fields
																	// we are interested in getting:
																	
																	//unsigned short m_usType;              <type>      // internal colossus type
																	//char* m_pszID;												<clipId>		// the clip ID
																	//char* m_pszTitle;											<title>		  // the title
																	//char* m_pszData;  										<data> 			//must encode zero.
																	//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																	//unsigned char  m_ucSegment;														// not used

																	// following 2 compiled from <startTime>
																	//unsigned long  m_ulOnAirTimeMS;
																	//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																	//unsigned long  m_ulDurationMS;        <dur>        // duration
																	//unsigned short m_usStatus;						<status>     // status (omnibus code)
																	//unsigned short m_usControl;           <timeMode>   // time mode

																	// additionally, up at the list level, we can compile from
																	//  <currentTime>
																	//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																	//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																	//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																	// let's create a new event object;
																	CCAEvent* pEvent = new CCAEvent;
																	char* pchField;

																	pchName = strstr(pchNext, "<storyID>");
																	if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																	{
																		pchName += strlen("<storyID>");
																		pch = strstr(pchName, "</storyID>");
																		if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																		{
																			*pch = 0;  //null term the ID!
																			pchField = (char*) malloc(strlen(pchName)+1);
																			if(pchField)  // have to at least have this field
																			{
																				strcpy(pchField, pchName);
																				pEvent->m_pszReconcileKey = pchField;
																				*pch = '<'; // put it back so we can search whole string again.

																				pchName = strstr(pchNext, "<clipId>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<clipId>");
																					pch = strstr(pchName, "</clipId>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszID = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}


																				pchName = strstr(pchNext, "<title>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<title>");
																					pch = strstr(pchName, "</title>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszTitle = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}


																				pchName = strstr(pchNext, "<data>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<data>");
																					pch = strstr(pchName, "</data>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszData = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}

																				pchName = strstr(pchNext, "<startTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<startTime>");
																					pch = strstr(pchName, "</startTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_dblOnAirTimeInternal = atof(pchName);
																						int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																						else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																					}
																				}

																				pchName = strstr(pchNext, "<currentTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<currentTime>");
																					pch = strstr(pchName, "</currentTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																						//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																					}
																				}

																				pchName = strstr(pchNext, "<type>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<type>");
																					pch = strstr(pchName, "</type>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usType = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<dur>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<dur>");
																					pch = strstr(pchName, "</dur>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulDurationMS = (unsigned long) nMS;
																						else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																					}
																				}

																				pchName = strstr(pchNext, "<status>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<status>");
																					pch = strstr(pchName, "</status>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usStatus = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<timeMode>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<timeMode>");
																					pch = strstr(pchName, "</timeMode>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usControl = (unsigned short)atoi(pchName);
																					}
																				}

	//																			Ok, lets update the event!
																				if(nListIndex>=0)
																				{
if(!pConn->m_bKillThread) EnterCriticalSection(&pConn->m_crit);
																					CCAList* pList = pConn->GetList(nListIndex);
																					if(pList)
																					{
																						if(!pConn->m_bKillThread) 
																						{
																							double dblTime = ((double)timestamp.millitm/1000.0);
																							if(pConn->m_bUseUTC)
																							{
																								dblTime += (double)(timestamp.time); // UTC
																							}
																							else
																							{
																								dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																							}

																							if(pConn->m_bUseDST)
																							{
																								dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																							}
																							pEvent->m_dblUpdated = dblTime;
																							pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszTable, pConn->m_pszServerAddress, pConn->m_usPort);
																						}
																					}
																					if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=0;
																					pConn->m_ulCounter++;
LeaveCriticalSection(&pConn->m_crit);
																				}

																			}
																		}
																	}
																	//else // cant do anything with this event, so just have to skip.
																}

																pchNext = strstr(pchEnd, "<story>");
															}
														} //if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
													}
												} 
												else
												if(!pConn->m_bKillThread)
												{
													pchName = strstr(pchXML, "<roStoryDelete>");
													if(pchName)
													{
															// it's a delete record
														pchEnd = strstr(pchName, "</roStoryDelete>");
														if(pchEnd)  // we found a correctly encapsulated record.
														{
															pchNext = strstr(pchName, "<roID>");
															if((pchNext)&&(pchNext<pchEnd))
															{
																pchNext += strlen("<roID>");
																usListID = atoi(pchNext);

if(!pConn->m_bKillThread) EnterCriticalSection(&pConn->m_crit);
																int i = pConn->ReturnListIndex(usListID);
LeaveCriticalSection(&pConn->m_crit);
																if(i>=0)
																{
																	//pConn->m_ppList[i]->UpdateEvent(pEvent);

																	// then, find the story ID to delete
																	pchNext = strstr(pchName, "<storyID>");
																	if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
																	{
																		pchNext += strlen("<storyID>");
																		pch = strstr(pchNext, "</storyID>");
																		if((pch)&&(pch<pchEnd))
																		{
																			*pch = 0;  //null term the ID!
if(!pConn->m_bKillThread) EnterCriticalSection(&pConn->m_crit);
																			CCAList* pList = pConn->GetList(i);
																			if(pList)
																			{
																				int j = pList->GetEventIndex(pchNext);
																				if(j>=0)
																				{
																					if(!pConn->m_bKillThread)
																					{
																						pList->DeleteEvent((unsigned short) j, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszTable);
																					}
																					if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=0;
																					pConn->m_ulCounter++;
																				}
																			}
LeaveCriticalSection(&pConn->m_crit);
																		}
																	}
																}
															}
														}//if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))


													}  // else its an unrecognized mos command, or time to die
												}
											}
										}


										free(pchXML);
										pchXML = NULL;

										if (pchXMLStream) 
											pchEnd = strstr(pchXMLStream, "</mos>");
										else pchEnd = NULL;
									}  // while </mos> exists.
								}//if(pchXMLStream)
								// dont do a free(pch);  
							}  // else out of mem, so just skip
							else
							{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "**** Couldn't allocate buffer!\r\n");
		fflush(fp);
		fclose(fp);
	}
}
							}
						} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("out\r\n", 1, strlen("out\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
					} // if getline succeeds.
					else
					{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "**** GetLineError %d: %s\r\n", nReturnCode, chError);
		fflush(fp);
		fclose(fp);

	}
}

						// here, we either have timed out because theres no data, or, the connection has been lost.
						// if we lost the conn, need ot re-establish.
						if(nReturnCode == NET_ERROR_CONN)		// connection lost
						{
							pConn->m_ulFlags |= OMNI_SOCKETERROR;
							ulRetry++;
							break;  // break out of while loop
						}
					}
					if(pchBuffer) free(pchBuffer);

					if(!pConn->m_bKillThread) Sleep(1);  // dont peg processor
	/*
					fp = fopen("omnibus.log", "at");
					if(fp)
					{
						fprintf(fp, "here %d\r\n", timestamp.time);
						fflush(fp);
						fclose(fp);
					}
	*/
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("while\r\n", 1, strlen("while\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

				} //while(!pConn->m_bKillThread)  // kill the comm thread

				net.CloseConnection(s);
			}  // else couldnt open connection
			else
			{
				pConn->m_ulFlags |= OMNI_SOCKETERROR;
				ulRetry++;
			}

			if(!pConn->m_bKillThread) Sleep(pConn->m_ulConnectionInterval); 

		}  // while retrying
		pConn->m_bThreadStarted = false;  // state of comm thread
	}
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("bye\r\n", 1, strlen("bye\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
	_endthread();
}

