// OmniParse.h: interface for the COmniParse class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OMNIPARSE_H__A0953E24_B5AD_4D97_9F7E_5C7A1DFF024D__INCLUDED_)
#define AFX_OMNIPARSE_H__A0953E24_B5AD_4D97_9F7E_5C7A1DFF024D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


//#include "../../MSG/MessagingObject.h"
#include "OmnibusDefs.h"  // includes DB and messaging obj

class COmniParse : public CMessagingObject
{
public:

	CCAList*				m_pList;
	CCAEventData**	m_ppfi;  // dynamic array of field info.
	int							m_nNumFields;
	char*						m_szChannelID;
	unsigned long   m_ulFlags;		// options

public:
	COmniParse();
	virtual ~COmniParse();

// override
//	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);

// core - main
	int ClearList();  // does not clear datafields
	int SetDataFields(char* pszDataFields);  // clear with valid, zero length buffer
	int LoadFile(char* pszFilename);
	char* GetEventField(char* pchEventStart, char* pchEventEnd, char* szKey);

// utility
	int ConvertTime(char* pszTimeString, unsigned long* pulRefUnixTime=NULL, unsigned short* pusJulianDate=NULL);  // returns milliseconds in current day, -1 if error.
	int	ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned long ulFlags=OMNI_NORMAL);  
	int	ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned long ulFlags=OMNI_NORMAL);

//	int SetConnDB(CCAConn* pConn, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL, char*	pszTable=NULL); // searches for conn idx
};

#endif // !defined(AFX_OMNIPARSE_H__A0953E24_B5AD_4D97_9F7E_5C7A1DFF024D__INCLUDED_)
