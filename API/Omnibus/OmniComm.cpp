// OmniComm.cpp: implementation of the COmniComm class.
//
//////////////////////////////////////////////////////////////////////

#include "OmniComm.h"
#include <process.h>
#include <sys/timeb.h>
#include <time.h>
#include <direct.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//send message type
#define OMNI_SENDMSG_ERROR   0
#define OMNI_SENDMSG_INFO	   1

#define HELIOS_NEW_DB_SCHEMA
// comm thread proto
//#ifdef HELIOS_NEW_DB_SCHEMA
void HeliosAdaptorCommThread(void* pvArgs);
//#else // ! HELIOS_NEW_DB_SCHEMA
//void CommThread(void* pvArgs);
//#endif// ! HELIOS_NEW_DB_SCHEMA

void HeliosAdaptorQueueThread(void* pvArgs);
void HeliosAdaptorChannelThread(void* pvArgs);

// if not multi threading channel processing, just use this:
void ProcessXML(CCAConn* pConn, char* pchXML);


// if multi threading channel processing, use these two:
void PreProcessXML(CCAConn* pConn, char* pchXML);
void ProcessChannelXML(CCAList* pList, char* pchXML);

extern CCAActives g_actives;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COmniComm::COmniComm()
{
	InitializeCriticalSection (&m_crit);
	InitializeCriticalSection (&m_critMem);
	

	m_ppConn = NULL;
	m_usNumConn = 0;
}

COmniComm::~COmniComm()
{
	EnterCriticalSection (&m_crit);
	if(m_ppConn)
	{
		for(unsigned short i=0;i<m_usNumConn;i++)
		{
			if(m_ppConn[i]) delete(m_ppConn[i]);
			m_ppConn[i] = NULL;
		}
		delete [] m_ppConn;
		m_ppConn = NULL;
	}
	LeaveCriticalSection (&m_crit);
	DeleteCriticalSection (&m_crit);
	DeleteCriticalSection (&m_critMem);

}

bool COmniComm::CheckConnectionValid(unsigned short usConnIndex)
{
	EnterCriticalSection (&m_crit);
	if(usConnIndex<m_usNumConn)
	{
		if((m_ppConn)&&(m_ppConn[usConnIndex]))
		{
	LeaveCriticalSection (&m_crit);
			return true;
		}
	}
	LeaveCriticalSection (&m_crit);
	return false;
}



CCAConn* COmniComm::ConnectServer(char* pszServerAddress, unsigned short usPort, char* pszServerName, char* pszDataFields, 
																	CDBUtil* pdb, CDBconn* pdbConn, char* pszEventsTable, char* pszExchangeTable, char* pszConnectionsTable, char* pszChannelsTable, 
																	CRITICAL_SECTION* pRetrieveCrit,
																	void* lpfnMsg, CRITICAL_SECTION* pMsgCrit) // returns pointer to connection
{
	CCAConn* pConn = new CCAConn;

	if(pConn)
	{
		pConn->m_pcritMem = &m_critMem;

		if(pszServerAddress)
		{
			pConn->m_pszServerAddress = (char*)malloc(strlen(pszServerAddress)+1);		//	hostname
			if(pConn->m_pszServerAddress) 
			{
				strcpy(pConn->m_pszServerAddress, pszServerAddress);
			} else return NULL;
		}else return NULL;    // need a server address!


		if(pszServerName)
		{
			pConn->m_pszServerName = (char*)malloc(strlen(pszServerName)+1);				// friendly name for the connection to the server.  not used programmatically
			if(pConn->m_pszServerName) strcpy(pConn->m_pszServerName, pszServerName);
		}

		if((pszDataFields)&&(strlen(pszDataFields)))  // a comma separated list of tags inside the mosPayLoad that will be assembled and put into the m_pszData member of the event items
		{

//			AfxMessageBox(pszDataFields);
			pConn->m_nNumFields = 0;
			pConn->m_ppfi = NULL;  // dynamic array of field info.
																			
			// first split up the pConn->m_pszDataFields into the tag names
			CSafeBufferUtil sbu;

			char* pchField = sbu.Token(pszDataFields, strlen(pszDataFields), "><, ");
			while(pchField)
			{
				if(strlen(pchField))
				{
					CCAEventData* pfi = new CCAEventData;
					if(pfi)
					{
						CCAEventData** ppnewfi = new CCAEventData*[pConn->m_nNumFields+1];
						if(ppnewfi)
						{
							if(pConn->m_ppfi)
							{
								for(int i=0; i<pConn->m_nNumFields; i++)
								{
									ppnewfi[i] = pConn->m_ppfi[i];
								}
								try{ delete [] pConn->m_ppfi;} catch(...){}
							}

							pConn->m_ppfi = ppnewfi;

	//			AfxMessageBox(pch);
							//pfi->m_pszFieldName = pchField;  
							pfi->m_pszFieldName = (char*)malloc(strlen(pchField)+1);
							if(pfi->m_pszFieldName) strcpy(pfi->m_pszFieldName, pchField);  
//				AfxMessageBox(pConn->m_ppfi[pConn->m_nNumFields]->m_pszFieldName);

							pConn->m_ppfi[pConn->m_nNumFields] = pfi;
							pConn->m_nNumFields++;

						}
					}
				}

				//get the next one.
//				AfxMessageBox(pConn->m_ppfi[pConn->m_nNumFields-1]->m_pszFieldName);
				pchField = sbu.Token(NULL, NULL, "><, ");
//				AfxMessageBox(pConn->m_ppfi[pConn->m_nNumFields-1]->m_pszFieldName);

/*
				for(int i=0; i<pConn->m_nNumFields; i++)
				{
					char fn[256];
					sprintf(fn, "%d: 0x%08x: %s", i, pConn->m_ppfi[i]->m_pszFieldName, pConn->m_ppfi[i]->m_pszFieldName);
					AfxMessageBox(fn);
				}
*/			
			}

		}


/*
		for(int i=0; i<pConn->m_nNumFields; i++)
		{
			char fn[256];
			sprintf(fn, "final %d: 0x%08x: %s", i, pConn->m_ppfi[i]->m_pszFieldName, pConn->m_ppfi[i]->m_pszFieldName);
			AfxMessageBox(fn);
		}
*/
		pConn->m_usPort = usPort;							// port to connect

		SetConnDB(pConn, pdb, pdbConn, pszEventsTable, pszExchangeTable, pszConnectionsTable, pszChannelsTable, pRetrieveCrit);
		SetMessager(pConn, lpfnMsg, pMsgCrit); // sets the messager



		if( pConn->ConnectServer() == OMNI_SUCCESS)  // we began the thread, but did it connect?
		{
			_timeb timestamp;
			_timeb checktime;
			_ftime( &timestamp );
			checktime.time = timestamp.time ; 
			checktime.millitm = timestamp.millitm + 500; // many timeouts
			if(checktime.millitm>999)
			{
				checktime.time++;
				checktime.millitm%=1000;
			}

			while(!pConn->m_bThreadStarted)
			{   
				_ftime( &timestamp );
				if(
						(timestamp.time > checktime.time)
					||((timestamp.time == checktime.time)&&(timestamp.millitm >= checktime.millitm))
					)
				{
					break;
				}
				Sleep(1);
			}  // just do something


	EnterCriticalSection (&m_crit);

			if((!((pConn->m_ulFlags)&OMNI_SOCKETERROR))&&(pConn->m_bThreadStarted))
			{
				// add this connection to the matrix and return the pointer.
				CCAConn** ppConn = NULL;

				int nNumConn = ((int)(m_usNumConn));

				ppConn = new CCAConn*[nNumConn+1];

				if(ppConn)
				{
					CCAConn** ppConnTemp = m_ppConn;
					int i=0;
					if((m_ppConn)&&(nNumConn>0))
					{

						memcpy(ppConn, m_ppConn, sizeof(CCAConn*)*nNumConn);

/*
						while(i<nNumConn)
						{
							ppConn[i] = m_ppConn[i];
//							ppConn[i]->m_pszServerName = m_ppConn[i]->m_pszServerName;
//							ppConn[i]->m_pszServerAddress = m_ppConn[i]->m_pszServerAddress;
//							m_ppConn[i]->m_pszServerName = NULL;
//							m_ppConn[i]->m_pszServerAddress = NULL;

							i++;
						}
*/						
					}
					ppConn[nNumConn] = pConn; // assign new one

					m_ppConn = ppConn;
					m_usNumConn++;

					if(ppConnTemp) 
					{
						try{ delete [] ppConnTemp;} catch(...){}
					}

	LeaveCriticalSection (&m_crit);
					return pConn;
				}
				else
				{
					// couldnt allocate array so must disconnect!
	LeaveCriticalSection (&m_crit);

					pConn->DisconnectServer();

					return NULL;
				}
			}
			else
			{
	LeaveCriticalSection (&m_crit);

				pConn->DisconnectServer();
			}
		}

	}
	return NULL;
}

CCAConn* COmniComm::ConnectionExists(char* pszServerAddress) // returns pointer to connection
{
	if((m_ppConn)&&(m_usNumConn>0))
	{
		unsigned short i=0;
	EnterCriticalSection (&m_crit);
		while(i<m_usNumConn)
		{
			if(m_ppConn[i])
			{
				if(stricmp(pszServerAddress, m_ppConn[i]->m_pszServerAddress)==0) 
				{
	LeaveCriticalSection (&m_crit);
					return m_ppConn[i];
				}
			}
			i++;
		}
	LeaveCriticalSection (&m_crit);
	}
	return NULL;
}

int COmniComm::DisconnectServer(CCAConn* pConn, int nTimeoutMS)
{
	if(pConn)
	{
		// have to disconnect and remove from connection list
		pConn->DisconnectServer();  // always returns success

		// have to wait here for the conn to close and thread to die.

		_timeb timestamp;
		_timeb endtime;
		_timeb checktime;
		_ftime( &timestamp );
		endtime.time = timestamp.time + nTimeoutMS/1000; 
		endtime.millitm = timestamp.millitm + nTimeoutMS%1000;
		if(endtime.millitm>999)
		{
			endtime.time++;
			endtime.millitm%=1000;
		}

		_ftime( &checktime );
		while(pConn->m_bThreadStarted)
		{   
			_ftime( &checktime );
			if(
					(checktime.time > endtime.time)
				||((checktime.time == endtime.time)&&(checktime.millitm >= endtime.millitm))
				)
			{
				break;  // if the timeout exipires
			}
			Sleep(10);
		}  // just do something while the thread is still there;


	EnterCriticalSection (&m_crit);
		if((m_ppConn)&&(m_usNumConn>0))
		{
			unsigned short i=0;
			while(i<m_usNumConn)
			{
				if(m_ppConn[i] == pConn)
				{
					// remove it
					if(pConn)
					{
						try
						{
							// something is deleting or corrupting this somewhere before we get here!
							delete pConn;
						}
						catch(...)
						{
						}
					}
					pConn = NULL;
					m_usNumConn--;

					CCAConn** ppConn = NULL;
					if(m_usNumConn>0)
					{
						ppConn = new CCAConn*[m_usNumConn];
					}
					else
					{
						delete [] m_ppConn;
						m_ppConn = NULL;
	LeaveCriticalSection (&m_crit);
						return OMNI_SUCCESS;
					}


					if(ppConn)
					{
		//copy over all the beginning ones
						int q=0;
						while(q<i)
						{
							ppConn[q] = m_ppConn[q];
							q++;
						}

						while(i<m_usNumConn)
						{
							ppConn[i] = m_ppConn[i+1];
							i++;
						}
						delete [] m_ppConn;
						m_ppConn = ppConn;
					}
					else  // could not alloc, so just shuffle
					{
						if(m_ppConn)
						{
							while(i<m_usNumConn)
							{
								m_ppConn[i] = m_ppConn[i+1];
								i++;
							}
							m_ppConn[i] = NULL;
						}
					}
	LeaveCriticalSection (&m_crit);
					return ((checktime.time - timestamp.time)*1000 + (checktime.millitm - timestamp.millitm));
	//				return OMNI_SUCCESS;
				}
				i++;
			}
		}
	LeaveCriticalSection (&m_crit);

	}
	return OMNI_ERROR;
}

int COmniComm::DisconnectServer(char* pszServerAddress, int nTimeoutMS) // searches for conn idx
{
	CCAConn* pConn = ConnectionExists(pszServerAddress);
	return DisconnectServer(pConn, nTimeoutMS);
}

int COmniComm::SetMessager(CCAConn* pConn, void* lpfnMsg, CRITICAL_SECTION* pMsgCrit) // sets the messager
{
	if(pConn==NULL)
	{
//		AfxMessageBox("Could not set DB");
		return OMNI_ERROR;
	}

	pConn->m_lpfnSendMsg = (LPFNHMSG)lpfnMsg;
	pConn->m_pcritSendMsg = pMsgCrit;

	return OMNI_SUCCESS;
}


int COmniComm::SetConnDB(CCAConn* pConn, CDBUtil* pdb, CDBconn* pdbConn, char* pszEventsTable, char* pszExchangeTable, char* pszConnectionsTable, char* pszChannelsTable, CRITICAL_SECTION* pRetrieveCrit) 
{
	if(pConn==NULL)
	{
//		AfxMessageBox("Could not set DB");
		return OMNI_ERROR;
	}

	if((pdb==NULL)||(pdbConn==NULL))
	{
		//clear out the old....
		pConn->m_pdb = NULL;  // pointer to a db util object to use for operations
		pConn->m_pdbConn = NULL;  // pointer to a db connection
		pConn->m_pszEventsTable = NULL; // name of the table to update
		pConn->m_pszExchangeTable = NULL; // name of the table to update
		pConn->m_pszConnectionsTable = NULL; // name of the table to update
		pConn->m_pszChannelsTable = NULL; // name of the table to update
		pConn->m_pcritSQL = NULL;

		// just pointers, we arent freeing the connection objects etc.
//		AfxMessageBox("set to NULL");
	}
	else
	{
		pConn->m_pdb = pdb;  // pointer to a db util object to use for operations
		pConn->m_pdbConn = pdbConn;  // pointer to a db connection
		if((pszEventsTable==NULL)||(strlen(pszEventsTable)<=0))
			pConn->m_pszEventsTable = NULL;
		else
			pConn->m_pszEventsTable = pszEventsTable; // name of the table to update

		if((pszExchangeTable==NULL)||(strlen(pszExchangeTable)<=0))
			pConn->m_pszExchangeTable = NULL;
		else
			pConn->m_pszExchangeTable = pszExchangeTable; // name of the table to update

		if((pszConnectionsTable==NULL)||(strlen(pszConnectionsTable)<=0))
			pConn->m_pszConnectionsTable = NULL;
		else
			pConn->m_pszConnectionsTable = pszConnectionsTable; // name of the table to update

		if((pszChannelsTable==NULL)||(strlen(pszChannelsTable)<=0))
			pConn->m_pszChannelsTable = NULL;
		else
			pConn->m_pszChannelsTable = pszChannelsTable; // name of the table to update

		pConn->m_pcritSQL = pRetrieveCrit;
//		AfxMessageBox(pszTable);
	}
	return OMNI_SUCCESS;
}


#ifdef HELIOS_NEW_DB_SCHEMA
void HeliosAdaptorCommThread(void* pvArgs)
{
	CCAConn* pConn = (CCAConn*) pvArgs;
	FILE* fp;
	if(pConn)
	{
		if(pConn->m_bThreadStarted) return; // only one per conn.
		pConn->m_bThreadStarted = true;  // state of comm thread

		_timeb timestamp;

		Sleep(500);
		_ftime(&timestamp);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d.%03d (%d) Beginning Comm thread.\r\n", timestamp.time, timestamp.millitm, clock());
		fflush(fp);
		fclose(fp);

	}
}
		CNetUtil	net;  // the connection exclusive networking obj
		char szMsg[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

//return;
						// create tables if they dont exist.
		if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
		{
//			char szSQL[DB_SQLSTRING_MAXLEN];
			if((pConn->m_pszExchangeTable)&&(strlen(pConn->m_pszExchangeTable)))
			{
				_ftime( &timestamp );
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('Server_Time', '%s:%05d|%d%03d', -1)",
					pConn->m_pszExchangeTable,
					pConn->m_pszServerAddress,
					pConn->m_usPort,
					(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
					timestamp.millitm
					);
if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
				{
					// error.
				}
if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
			}

			if((pConn->m_pszEventsTable)&&(strlen(pConn->m_pszEventsTable)))
			{
				pConn->m_pdb->AddTable(pConn->m_pdbConn, pConn->m_pszEventsTable); // add errors table to schema
				if(pConn->m_pdb->TableExists(pConn->m_pdbConn, pConn->m_pszEventsTable)==DB_EXISTS)
				{
					// get the schema
					pConn->m_pdb->GetTableInfo(pConn->m_pdbConn, pConn->m_pszEventsTable);

				}
				else
				{
/*
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 

"CREATE TABLE [%s] (\"%s\" int identity(1,1) NOT NULL, \"%s\" varchar(32) NOT NULL, \"%s\" int NOT NULL, \
\"%s\" int NOT NULL, \"%s\" varchar(32) NOT NULL, \"%s\" varchar(64) NOT NULL, \
\"%s\" varchar(64) NOT NULL, \"%s\" varchar(4096) NULL, \"%s\" int NOT NULL, \
\"%s\" int NOT NULL, \"%s\" int NOT NULL, \"%s\" float NOT NULL, \"%s\" float NOT NULL, \
\"%s\" int NOT NULL, \"%s\" float NOT NULL, \"%s\" varchar(512) NULL, \
\"%s\" int NULL);",
										pConn->m_pszEventsTable,
										OMNI_DB_ITEMID_NAME, //				"itemid"    // of the DB event.
										OMNI_DB_CONNIP_NAME, //				"conn_ip"    // of colossus connection, varchar 32
										OMNI_DB_CONNPORT_NAME, //			"conn_port"  // of colossus connection, int
										OMNI_DB_LISTID_NAME, //				"list_id"			// omnibus stream number, int
										OMNI_DB_EVENTID_NAME, //			"event_id"    // unique ID (story ID), varchar 32, recKey on harris
										OMNI_DB_EVENTCLIP_NAME, //		"event_clip"  // clip ID, varchar 32
										OMNI_DB_EVENTTITLE_NAME, //		"event_title" // title, varchar 64
										OMNI_DB_EVENTDATA_NAME, //		"event_data"  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
										OMNI_DB_EVENTTYPE_NAME, //		"event_type"  // internal colossus type, int
										OMNI_DB_EVENTSTATUS_NAME, //	"event_status"  // internal colossus status, int
										OMNI_DB_EVENTTMODE_NAME, //		"event_time_mode"  // time mode, int
										OMNI_DB_EVENTFSJ70_NAME, //		"event_start_internal" // frames since Jan 1970, SQL float (C++ double) 
										OMNI_DB_EVENTSTART_NAME, //		"event_start" // start time of event, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
										OMNI_DB_EVENTDURMS_NAME, //		"event_duration" // duration in milliseconds, int
										OMNI_DB_EVENTUPDATED_NAME, //	"event_last_update" // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
											// fields from other apps
										OMNI_DB_APPDATA_NAME, //			"app_data" // application data, varchar 512, allow null
										OMNI_DB_APPDATAAUX_NAME	//	"app_data_aux" // auxiliary application data, int, allow null
									);

					//					AfxMessageBox(szSQL);
*/
					// just hard code the new schema for now.
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
"CREATE TABLE [%s] (itemid int identity(1,1) NOT NULL, conn_ip varchar(32) NOT NULL, \
conn_port int NOT NULL, list_id int NOT NULL, event_id varchar(32) NOT NULL, event_clip varchar(64) NOT NULL, \
event_title varchar(64) NOT NULL, event_data varchar(4096) NULL, event_type int NOT NULL, event_status int NOT NULL, \
event_time_mode int NOT NULL, event_start decimal(20,3) NOT NULL, event_duration int NOT NULL, \
event_last_update decimal(20,3) NOT NULL, event_calc_start decimal(20,3), event_position int NOT NULL, \
parent_id varchar(32) NULL, parent_position int NULL, parent_start decimal(20,3) NULL, parent_calc_start decimal(20,3) NULL, \
parent_duration int, parent_calc_end decimal(20,3), event_calc_end decimal(20,3), app_data varchar(512) NULL, \
app_data_aux int NULL, event_start_internal decimal(20,3));", 
						pConn->m_pszEventsTable
						);


					if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
					if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)>=DB_SUCCESS)
					{
						pConn->m_pdb->GetTableInfo(pConn->m_pdbConn, pConn->m_pszEventsTable);
					}
					if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
				// else error notice...

				}

				// now that the table existence has been ensured, delete things more than a day old.
				_ftime(&timestamp);

				double dblTime;
				if(pConn->m_bUseUTC)
				{
					dblTime = (double)(timestamp.time - 86400); // UTC
				}
				else
				{
					dblTime = (double)(timestamp.time - 86400 - (timestamp.timezone*60));  // local time
				}

				if(pConn->m_bUseDST)
				{
					dblTime += (double)(timestamp.dstflag?3600:0);  // DST
				}



				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"DELETE FROM %s WHERE %s < %.03f AND %s = '%s';", 
										pConn->m_pszEventsTable?pConn->m_pszEventsTable:"Events",
										OMNI_DB_EVENTUPDATED_NAME, dblTime,
										OMNI_DB_CONNIP_NAME,  pConn->m_pszServerAddress
										);
											//AfxMessageBox(szSQL);

				if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d Executing SQL: %s\r\n", clock(), szSQL);
						fflush(fp);
						fclose(fp);

					}
				}

				if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);

				int nSQLreturn = pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL, pConn->m_errorstring);
				if(nSQLreturn<DB_SUCCESS)
				{
					if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
					{
						fp = fopen(pConn->m_pszDebugFile, "at");
						if(fp)
						{
							fprintf(fp, "%d SQL Error: %s\r\n", clock(), pConn->m_errorstring);
							fflush(fp);
							fclose(fp);

						}
					}

					  // could do a message alert at some point.
					//AfxMessageBox("error deleting old");
				}
				else
				{
					if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
					{
						fp = fopen(pConn->m_pszDebugFile, "at");
						if(fp)
						{
							fprintf(fp, "%d SQL returned %d\r\n", clock(), nSQLreturn);
							fflush(fp);
							fclose(fp);

						}
					}
				}
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);



			}//if((pConn->m_pszEventsTable)&&(strlen(pConn->m_pszEventsTable)))
		}//if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
		

		int  nRetry = 0 ;
		while((nRetry<(int)pConn->m_ulConnectionRetries)&&(!pConn->m_bKillThread))
		{


			if(!pConn->m_bXMLQueueStarted)
			{
				EnterCriticalSection(&pConn->m_critXMLQueue);
				pConn->m_bXMLQueueKill=false;

				if(_beginthread(HeliosAdaptorQueueThread, 0, (void*)pConn) == -1)
				{
					if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg)/*&&(nRetry<1)*/) // always report
					{
						EnterCriticalSection(pConn->m_pcritSendMsg);
						_snprintf(szMsg, DB_ERRORSTRING_LEN, "Failed to start queue for connection to %s:%d.\r\n",
							pConn->m_pszServerAddress?pConn->m_pszServerAddress:"null", pConn->m_usPort);
						pConn->m_lpfnSendMsg(OMNI_SENDMSG_ERROR, "AdaptorComm:start_queue", szMsg);
						LeaveCriticalSection(pConn->m_pcritSendMsg);
					}
				}
				LeaveCriticalSection(&pConn->m_critXMLQueue);
			}


			if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "%d Trying connection to %s:%d...\r\n", clock(), pConn->m_pszServerAddress, pConn->m_usPort);
					fflush(fp);
					fclose(fp);

				}
			}

			if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg)&&(nRetry>0))
			{
				EnterCriticalSection(pConn->m_pcritSendMsg);
				_snprintf(szMsg, DB_ERRORSTRING_LEN, "Attempting to re-establish connection to %s:%d.  (Retry %d.)\r\n",
					pConn->m_pszServerAddress?pConn->m_pszServerAddress:"null", pConn->m_usPort, nRetry);
				pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:reconnect", szMsg);
				LeaveCriticalSection(pConn->m_pcritSendMsg);
			}

			// let's set the time so that the initial connection doesn't immediately time out.
			_ftime(&timestamp);
			pConn->m_ulConnLastMessage = timestamp.time;


			bool bInitialConnection = true;
			if(net.OpenConnection(
				pConn->m_pszServerAddress, 
				pConn->m_usPort, 
				&pConn->m_socket,
				1000, 1000  // 1000 millisecond com timeouts to not freeze thread
				)>=NET_SUCCESS)
			{	
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket is %d after OpenConnection.\r\n", clock(), pConn->m_socket);
		fflush(fp);

		fclose(fp);
	}
}

				pConn->m_ulFlags &= ~OMNI_SOCKETERROR;
				pConn->m_ulFlags |= OMNI_SOCKETCONNECTED;
				nRetry = 0;
				char* pchBuffer = NULL;
				unsigned long ulBufferLen = 0;

				char buffer[32];
				strcpy(buffer, "<mos><roReq></roReq></mos>" );
				ulBufferLen = strlen(buffer)+1; //send the term 0
/*
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "Socket is %d before kill=%d.\r\n", pConn->m_socket, pConn->m_bKillThread);
		fflush(fp);
		fclose(fp);

	}
}

*/
				if (pConn->m_bKillThread)
				{
					net.CloseConnection(pConn->m_socket);
					pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
					pConn->m_bThreadStarted = false;  // state of comm thread
					if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
					{
						EnterCriticalSection(pConn->m_pcritSendMsg);
						_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Disconnecting %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
						pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:disconnect", szMsg);
						LeaveCriticalSection(pConn->m_pcritSendMsg);
					}

					_endthread();
					return;
				}
/*
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "Socket is %d after kill=%d.\r\n", pConn->m_socket, pConn->m_bKillThread);
		fflush(fp);
		fclose(fp);

	}
}
*/
				if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
				{

					EnterCriticalSection(pConn->m_pcritSendMsg);
					_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Connection established to %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket is %d before callback.\r\n", clock(), pConn->m_socket);
		fflush(fp);
		fclose(fp);

	}
}

					pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:connect", szMsg);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
 		fprintf(fp, "%d Socket is %d after callback.\r\n", clock(), pConn->m_socket);
		fflush(fp);
		fclose(fp);

	}
}


					LeaveCriticalSection(pConn->m_pcritSendMsg);
				}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket is %d before SendLine.\r\n", clock(), pConn->m_socket);
		fflush(fp);
		fclose(fp);

	}
}



///////////////xxxxxxxxxxxxx  implement clear on connect, because 
// need to clear database in case stragglers exist.
// but want to delay until first connect message is parsed, delete everything but this.
// database and memory.


///////////////

//xxxxxxxxxxxxx


//	bool m_bClearEventsConnect;  // if true, clears channel based event table on connection


				if((pConn->m_ulFlags&OMNI_CLEARONCONNECT)&&(pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"DELETE FROM %s WHERE %s = '%s'", 
										pConn->m_pszEventsTable?pConn->m_pszEventsTable:"Events",
										OMNI_DB_CONNIP_NAME, pConn->m_pszServerAddress
										);

	EnterCriticalSection(pConn->m_pcritSQL);
					if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL, pConn->m_errorstring)<DB_SUCCESS)
					{
						// msg?
							if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
							{
								EnterCriticalSection(pConn->m_pcritSendMsg);
								_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "SQL error %s\r\n", pConn->m_errorstring);
								pConn->m_lpfnSendMsg(OMNI_SENDMSG_ERROR, "AdaptorComm:delete_on_connect:error", szMsg);
								LeaveCriticalSection(pConn->m_pcritSendMsg);
							}
					}
	LeaveCriticalSection(pConn->m_pcritSQL);
				}



				Sleep(pConn->m_ulThreadDelayMS);

				// first, set up the connection by sending <mos><roReq></roReq></mos>
				int n = net.SendLine((unsigned char*)buffer, ulBufferLen, pConn->m_socket);//, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
				
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket is %d after SendLine.\r\n", clock(), pConn->m_socket);
		fflush(fp);
		fclose(fp);

	}
}

	/*
				fp = fopen("omnibus.log", "at");
				if(fp)
				{
					fprintf(fp, "SendLine returned %d\r\n", n);
					fflush(fp);
					fclose(fp);
				}
	*/
				char* pch = NULL;
				char* pchXML = NULL;
				char* pchXMLStream = NULL;
				unsigned long ulAccumulatedBufferLen = 0;
				char filename[MAX_PATH];
				char lastfilename[MAX_PATH];
				int nDupes=0;

				// set up the buffer for Ack
				strcpy(buffer, "<mos><roAck/></mos>" );

				while((!pConn->m_bKillThread)&&(pConn->m_socket!=NULL))// kill the comm thread or socket null
				{
					pchBuffer = NULL;
					ulBufferLen = 0;

/*
_ftime( &timestamp );

char buffer[256];
tm* theTime = localtime( &timestamp.time	);
strftime(buffer, 30, "calling getline %d-%H:%M:%S.", theTime );
int nOffset = strlen(buffer);
sprintf(buffer+nOffset,"%03d\r\n",timestamp.millitm);

fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite(buffer, 1, strlen(buffer),fp);
	fflush(fp);
	fclose(fp);
}
*/

					char chError[NET_ERRORSTRING_LEN];
					if (pConn->m_bKillThread)
					{
						net.CloseConnection(pConn->m_socket);
						pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
						pConn->m_bThreadStarted = false;  // state of comm thread
						if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
						{
							EnterCriticalSection(pConn->m_pcritSendMsg);
							_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Disconnecting %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
							pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:disconnect", szMsg);
							LeaveCriticalSection(pConn->m_pcritSendMsg);
						}
						_endthread();
						return;
					}
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket is %d before GetLine.\r\n", clock(), pConn->m_socket);
		fflush(fp);
		fclose(fp);

	}
}

					int nReturnCode = -1;
					pchBuffer = NULL;
					try
					{
					  nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pConn->m_socket, NET_RCV_ONCE, chError);
					}
					catch(...)
					{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket %d exception after GetLine. %s\r\n", clock(), pConn->m_socket, chError);
		fflush(fp);
		fclose(fp);

	}
}
					}
	
					
/*_ftime( &timestamp );

theTime = localtime( &timestamp.time	);
strftime(buffer, 30, "called getline  %d-%H:%M:%S.", theTime );
nOffset = strlen(buffer);
sprintf(buffer+nOffset,"%03d returned\r\n",timestamp.millitm);

fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite(buffer, 1, strlen(buffer),fp);
	fflush(fp);
	fclose(fp);
}
*/
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d Socket is %d after GetLine returned %d bytes at %08x with %d.\r\n", clock(), pConn->m_socket, ulBufferLen, pchBuffer, nReturnCode);
		fflush(fp);
		fclose(fp);
	}
}
						
					if(nReturnCode == NET_SUCCESS)
					{
						//process any received XML.


if((pConn->m_pszCommFile)&&(strlen(pConn->m_pszCommFile)))
{
	fp = fopen(pConn->m_pszCommFile, "at");
	if(fp)
	{
		fwrite(pchBuffer, 1, ulBufferLen,fp);
		fflush(fp);
		fclose(fp);
	}
}


//testing
//if(pchBuffer) free(pchBuffer);
//pchBuffer = NULL;
//testing


						// have to keep accumulating until we find a </mos> tag.
						if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))
						{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("recd\r\n", 1, strlen("recd\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/


/*
	// debug the receipt
						_ftime( &timestamp );

						tm* theTime = localtime( &timestamp.time	);
						strftime(filename, 30, "logs\\received%d-%H%M%S", theTime );
						int nOffset = strlen(filename);
						sprintf(filename+nOffset,"%03d.txt",timestamp.millitm);

						fp = fopen(filename, "wb");
	//					fp = fopen("omnibus.log", "at");
						if(fp)
						{
							fwrite(pchBuffer, 1, ulBufferLen,fp);
							fflush(fp);
							fclose(fp);
						}
	// end debug
*/
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d buf %d @ %d.\r\n", clock(), ulAccumulatedBufferLen, pchBuffer);
		fflush(fp);
		fclose(fp);
	}
}
						
							int nLen = 0;
							if(pchXMLStream) nLen = ulAccumulatedBufferLen;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc %d\r\n", clock(), nLen+ulBufferLen+1);
		fflush(fp);
		fclose(fp);

	}
}

							try
							{
								pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
							}
							catch(...)
							{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc exception %d\r\n", clock(), nLen+ulBufferLen+1);
		fflush(fp);
		fclose(fp);

	}
}
								pch=NULL;
							}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc %d @ %d\r\n", clock(), nLen+ulBufferLen+1, pch);
		fflush(fp);
		fclose(fp);

	}
}
							if(pch)
							{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("pch\r\n", 1, strlen("pch\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
								
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc\r\n", clock());
		fflush(fp);
		fclose(fp);

	}
}

								char* pchEnd = NULL;
								char* pchNext = NULL;
								if(pchXMLStream)  // we have an old buffer.
								{

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus\r\n", clock());
		fflush(fp);
		fclose(fp);

	}
}

									memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus c1 %d <- %d %d\r\n", clock(), pch, pchXMLStream, ulAccumulatedBufferLen);
		fflush(fp);
		fclose(fp);

	}
}

									memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus c2 %d <- %d %d free %d\r\n", clock(), pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen, pchXMLStream);
		fflush(fp);
		fclose(fp);

	}
}

									try
									{
										free(pchXMLStream); 
									}
									catch(...)
									{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus free exception\r\n", clock());
		fflush(fp);
		fclose(fp);

	}
}

									}
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus f\r\n", clock());
		fflush(fp);
		fclose(fp);

	}
}

									pchXMLStream = pch;  // reassign!
									ulAccumulatedBufferLen += ulBufferLen;
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus zero @ %d %d\r\n", clock(), pchXMLStream, ulAccumulatedBufferLen);
		fflush(fp);
		fclose(fp);

	}
}

									*(pch+ulAccumulatedBufferLen) = 0;  // null term

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc plus has %d bytes\r\n", clock(),ulAccumulatedBufferLen);
		fflush(fp);
		fclose(fp);

	}
}
									pch = NULL;  // be explicit, everything is in pchXMLStream now.
								}
								else
								{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d alloc new %d\r\n", clock(), ulAccumulatedBufferLen);
		fflush(fp);
		fclose(fp);

	}
}

									// this is new. (there is no old accumulated buffer to append to)
									// first we have to skip all chars that are not a '<'
									// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
									// used to use strchr but if there are leading zeros in the buffer, we never get past them

									pchEnd = pchBuffer;
									while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

									if(pchEnd<pchBuffer+ulBufferLen)
									{
										strcpy(pch, pchEnd);
										ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
										pchXMLStream = pch;  // reassign!
										*(pch+ulAccumulatedBufferLen) = 0;  // null term

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d reassign %d\r\n", clock(), pchXMLStream);
		fflush(fp);
		fclose(fp);

	}
}


									}
									else  // not found!
									{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d free %d\r\n", clock(), pch);
		fflush(fp);
		fclose(fp);

	}
}

										free(pch);  
										pchXMLStream = NULL;
									}
									pch=NULL;
								}

								if(pchBuffer) free(pchBuffer);
								pchBuffer = NULL;

/*fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("pchBuffer\r\n", 1, strlen("pchBuffer\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

								// now, pchXMLStream has everything so far.
/*
	if(pchXMLStream)
	{
		strftime(filename, 30, "logs\\debugaccumbuf%d-%H%M%S", theTime );
		int nOffset = strlen(filename);
		sprintf(filename+nOffset,"%03d.txt",timestamp.millitm);

		fp = fopen(filename, "wb");
//					fp = fopen("omnibus.log", "at");
		if(fp)
		{
			fwrite(pchXMLStream, 1, ulAccumulatedBufferLen,fp);
			fflush(fp);
			fclose(fp);
		}
	}
*/

								if(pchXMLStream)
								{

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d chkmos %d in %d\r\n", clock(), pchXMLStream, ulAccumulatedBufferLen);
		fflush(fp);
		fclose(fp);

	}
}

									pchEnd = strstr(pchXMLStream, "</mos>");

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d chkmos %d -> %d (%d)\r\n", clock(), pchXMLStream, pchEnd, pchEnd-pchXMLStream );
		fflush(fp);
		fclose(fp);

	}
}


/*
//testing
if(pchXMLStream)
{
free(pchXMLStream);  
pchXMLStream = NULL;
ulAccumulatedBufferLen=0;
pchEnd=NULL;
}
//testing
*/


									while((pchEnd)&&(!pConn->m_bKillThread))
									{

/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("mos\r\n", 1, strlen("mos\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos e%d\r\n", clock(), pchEnd);
		fflush(fp);
		fclose(fp);

	}
}

									// found a token.
										pchEnd+=strlen("</mos>");

										pchNext = pchEnd;
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos e%d n%d\r\n", clock(), pchEnd, pchNext);
		fflush(fp);
		fclose(fp);

	}
}
										char* pchEndBuf = pchEnd+strlen(pchEnd);
										while(((*pchNext) != '<')&&(pchNext<pchEndBuf))
										{
											pchNext++;
										}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos n%d (%d)  e+%d\r\n", clock(), pchNext, strlen(pchNext), pchEndBuf);
		fflush(fp);
		fclose(fp);

	}
}
										if(pchNext<pchEndBuf)
										{
											// we found a remainder.
											nLen = strlen(pchNext);
											try
											{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos alloc %d\r\n", clock(), nLen+1);
		fflush(fp);
		fclose(fp);

	}
}
												pch = (char*) malloc(nLen+1);  //term 0

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos alloc %d @ %d\r\n", clock(), nLen+1, pch);
		fflush(fp);
		fclose(fp);

	}
}
											}
											catch(...)
											{

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos exception alloc %d", clock(), nLen+1);
		fflush(fp);
		fclose(fp);

	}
}

												pch=NULL;
											}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d mos alloc %d @ %d\r\n", clock(), nLen+1, pch);
		fflush(fp);
		fclose(fp);

	}
}


											if(pch)
											{
												memcpy(pch, pchNext, nLen);
												*(pch+nLen) = 0;
											}
										}
										else pch = NULL;


										pchXML = pchXMLStream; // just use it.
										*pchEnd = 0; //null terminate it

										pchXMLStream = pch;  // take the rest of the stream.
										if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
										else ulAccumulatedBufferLen=0;

										pch=NULL;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d rem %d %d - [%d] s%d->%d\r\n", clock(), pchXMLStream, ulAccumulatedBufferLen, pchXML, pConn, pConn->m_bUseResponse);
		fflush(fp);
		fclose(fp);

	}
}


										// now have to deal with XML found in pchXML.


										if(pConn->m_bUseResponse)
										{
											ulBufferLen = strlen(buffer)+1; //send the term 0
											n = net.SendLine((unsigned char*)buffer, ulBufferLen, pConn->m_socket);//, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
										}

										unsigned long ulCounter = 0;  //local counter, we're going to parse the whole XML first, THEN update the counter.
										// otherwise, we are hammering the database with requests, if it is a large XML packet, best to do it all at once.
										//debug file write
										if(pConn->m_bWriteXMLtoFile)
										{
											strcpy(filename, "Logs");
											_mkdir(filename);  // if exists already np
											strcat(filename, "\\");
											strcat(filename, ((pConn->m_pszServerName!=NULL)&&(strlen(pConn->m_pszServerName)>0))? pConn->m_pszServerName : pConn->m_pszServerAddress);

											_mkdir(filename);  // if exists already np

											_ftime( &timestamp );

											tm* theTime = localtime( &timestamp.time	);

											char filenametemp[MAX_PATH];

											if(strstr(pchXML, "<heartbeat>"))
												strftime(filenametemp, MAX_PATH-1, "\\Heartbeat_Message_Day_%d-%H.%M.%S.", theTime );
											else
												strftime(filenametemp, MAX_PATH-1, "\\Output_Message_Day_%d-%H.%M.%S.", theTime );

											strcat(filename, filenametemp);

											int nOffset = strlen(filename);
											sprintf(filename+nOffset,"%03d",timestamp.millitm);

											if (strcmp(lastfilename, filename)==0)
											{
												nDupes++;
											}
											else
											{
												nDupes=0;
												strcpy(lastfilename, filename);
											}
											nOffset = strlen(filename);
											sprintf(filename+nOffset,"%02d.xml",nDupes);

											fp = fopen(filename, "wb");
											if(fp)
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												fwrite(pchXML, 1, strlen(pchXML),fp);
												fflush(fp);
												fclose(fp);
											}
		/*									else
											{
												fp = fopen("logs\\omnibus.log", "at");
											
												if(fp)
												{
													fwrite(pchXML, 1, strlen(pchXML),fp);
													fflush(fp);
													fclose(fp);
												}
											}
	*/
										}
									//// end debug file write

										// ok so, here is the XML, in pchXML

// 2.1.1.16 

//	AfxMessageBox(pchXML);

										char* pchQueue = new char[strlen(pchXML)+1];
										if(pchQueue)
										{
											strcpy(pchQueue,pchXML);
		EnterCriticalSection(&pConn->m_critXMLQueue);
											pConn->AddMessage(pchQueue, bInitialConnection);
											bInitialConnection = false;
		LeaveCriticalSection(&pConn->m_critXMLQueue);
										}



// 2.1.1.16 

//moved to:				ProcessXML(pConn, pchXML);

										if(0)  // 2.1.1.16  // whoa! ooo-wee!
										{


										// let's set the time.
										_ftime( &timestamp );

										pConn->m_ulConnLastMessage = timestamp.time;

										// let's populate the channels and the event information,

	////////////////////////////////////////////////////////////////////////////////////
	// iTX does not send server time, except in heartbeat.  But it is not guaranteed to ever get a hearbeat message.
	// we need servertime, so if allowed, grab the system time here, on receipt of any message.
  // we leave this as a global setting so that you can use it with Colossus too.

										if(pConn->m_ulFlags&OMNI_SYSTIME)
										{
											pConn->UpdateServerTime(true);  // sets vars.
										}


										if(strstr(pchXML, "<heartbeat>"))
										{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d A heartbeat message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}
											// heartbeat msg
											pchEnd = strstr(pchXML, "</heartbeat>");
											// find the list name:

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;
											bool bConnected = false;

											if(!(pConn->m_ulFlags&OMNI_SYSTIME))
											{
												pchName = strstr(pchXML, "<currentTime>");
												if((pchName)&&(pchEnd)&&(pchName<pchEnd))
												{
													pchName += strlen("<currentTime>");
													pch = strstr(pchName, "</currentTime>");
													if((pch)&&(pch<pchEnd))
													{
														
														int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
														if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
														//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
														pConn->UpdateServerTime(false);

													}
												}
											}
											// TODO: may want to change this later to not bother allocating unconnected lists.

											pchName = strstr(pchXML, "<channel>");
											while((pchName)&&(!pConn->m_bKillThread))
											{
												usListID = LIST_NOT_DEFINED;
												bConnected = false;
												pchEnd = strstr(pchName, "</channel>");
												if(pchEnd)  // we found a correctly encapsulted record.
												{
													pchNext = strstr(pchName, "<id>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<id>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<connected>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<connected>");
															bConnected = atoi(pchNext)==0?false:true;
														}
														pchNext = strstr(pchName, "<name>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<name>");
															pch = strstr(pchName, "</name>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else  strcpy(pchNext, ""); // blank name
														}
														else strcpy(pchNext, ""); // blank name

														// add the list!
if(!pConn->m_bKillThread)
{
 	EnterCriticalSection (&pConn->m_crit);
														pConn->UpdateList(usListID, pchNext, bConnected?OMNI_LIST_CONNECTED:OMNI_LIST_DISCONNECTED);
														int nListIndex = pConn->ReturnListIndex(usListID);
														if(nListIndex>=0)
														{
															CCAList* pList = pConn->GetList(nListIndex);
															if(pList)
															{
																pList->UpdateListStatus();
															}
														}
	LeaveCriticalSection (&pConn->m_crit);
}
													}

													pchName = strstr(pchEnd, "<channel>");  // find the next one.
												} // if(pchEnd)
											}//while ((pchName)&&(!pConn->m_bKillThread))
										}
										else
										{

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d An output message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}

											// output message.

											bool bProcessedTime = false;
	
											// note: iTX only sends roReplace (and heatbeat).  other commands are not supported.
											// iTX sends snapshots beginning with the currently playing event.

											unsigned short usRunningEventCount = 0;

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;

											pchName = strstr(pchXML, "<roReplace>");
											if(pchName)
											{
												pchEnd = strstr(pchName, "</roReplace>");
												if(pchEnd)  // we found a correctly encapsulated record.
												{
													pchNext = strstr(pchName, "<roID>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<roID>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<roSlug>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<roSlug>");
															pch = strstr(pchName, "</roSlug>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else
															{
																pchNext = NULL; // blank name
																pch = pchName;
															}
														}
														else
														{
															pchNext = NULL; // blank name
															pch = pchName;
														}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roReplace, about to update\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}


														int nListIndex = OMNI_SUCCESS;

/*
														if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
														{

															EnterCriticalSection(&g_actives.m_critActive);
												//			AfxMessageBox("roReplace, about to enter channel active check");
															nListIndex = g_actives.IsChannelActive(usListID);
												//			AfxMessageBox("roReplace, left channel active check");
															LeaveCriticalSection(&g_actives.m_critActive);
														}
*/
														
														if(!pConn->m_bKillThread)
														{
															int nUL = 0;
															if(pchNext)
															{
	EnterCriticalSection (&pConn->m_crit);
																nUL = pConn->UpdateList(usListID, pchNext, OMNI_LIST_CONNECTED);
	LeaveCriticalSection (&pConn->m_crit);
															}
															else
															{
	EnterCriticalSection (&pConn->m_crit);
																nUL = pConn->UpdateList(usListID, "", OMNI_LIST_CONNECTED);
	LeaveCriticalSection (&pConn->m_crit);
															}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d UpdateList returned %d\r\n",clock(), nUL);
		fflush(fp);
		fclose(fp);
	}
}

														}

														if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
														{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d updated, checking for events\r\n", clock());
		fflush(fp);
		fclose(fp);
	}
}

	EnterCriticalSection (&pConn->m_crit);
															nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
														}

														if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
														{

															// then, deal with any events we might have.
															pchNext = strstr((pch+1), "<story>");

	////////////////////////////////////////////////////////////////////////////////////
	// deletion of all events
	//
	// we weren't able to just delete on a blank roReplaceMessage,
	// as it turns out, the 2.8.16 Adaptor would send these messages
	// when something on a track we were filtering changed.
	// update: v2.8.18 fixes the blank message on filtered track change.

															

															if(pchNext == NULL) // we don't have any events.  
															// This seems to be only sent if the list is initially blank on connect, 
															// or if the live schedule is deleted.  Interestingly, no deletion events are sent in this case.
															{
	EnterCriticalSection(&pConn->m_crit);
																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(!pConn->m_bKillThread)
																	{
																		nReturnCode = pList->DeleteAllEvents( pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

																		if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																		{
																			fp = fopen(pConn->m_pszDebugFile, "at");
																			if(fp)
																			{
																				fprintf(fp, "DeleteAllEvents for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																				fflush(fp);
																				fclose(fp);
																			}
																		}

																		pList->UpdateListStatus();

																	}
/*
																	EnterCriticalSection(&pConn->m_critCounter);

																	if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																	pConn->m_ulCounter++;

																	LeaveCriticalSection(&pConn->m_critCounter);

*/
																	ulCounter++;
																}
	LeaveCriticalSection(&pConn->m_crit);


															}

															while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
															{


	if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
	{
		fp = fopen(pConn->m_pszDebugFile, "at");
		if(fp)
		{
			fprintf(fp, "%d pch n%d e%d\r\n", clock(),pchNext,pchEnd);
			fflush(fp);
			fclose(fp);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// iTX.  only sends this command, never deletes.
	// so we have to delete off the top, based on the first ID that is obtained.
	// we alslo have to delete any items in the list that are removed, since we are getting snapshots, if someone deletes from the middle of the list...



																char* pchEndRec = pchNext+strlen("<story>");
																pchName = strstr(pchNext, "</story>");
																if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																{
																	pchEndRec = pchName;
																	// ok, now pchNext is the beginning, 
																	// and pchEnd is the end of the list,
																	// pchEndRec is the end of the record, and we can use pchName for the fields
																	// we are interested in getting:
																	
																	//unsigned short m_usType;              <type>      // internal colossus type
																	//char* m_pszID;												<clipId>		// the clip ID
																	//char* m_pszTitle;											<title>		  // the title
																	//char* m_pszData;  										<data> 			//must encode zero.
																	//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																	//unsigned char  m_ucSegment;														// not used

																	// following 2 compiled from <startTime>
																	//unsigned long  m_ulOnAirTimeMS;
																	//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																	//unsigned long  m_ulDurationMS;        <dur>        // duration
																	//unsigned short m_usStatus;						<status>     // status (omnibus code)
																	//unsigned short m_usControl;           <timeMode>   // time mode

																	// additionally, up at the list level, we can compile from
																	//  <currentTime>
																	//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																	//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																	//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																	// let's create a new event object;
																	CCAEvent* pEvent = new CCAEvent;


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d pEvent %d\r\n", clock(),pEvent);
		fflush(fp);
		fclose(fp);
	}
}


																	char* pchField = NULL;

																	pchName = strstr(pchNext, "<storyID>");
																	if((pEvent) && (pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																	{

//20151106 2.1.1.15 - added parent pointer ssignment to self.  make default behavior, no option.  never referenced before so ignored elsewhere.  needed for Nucleus parameter queries
																		pEvent->m_pParent = pEvent;


																		pchName += strlen("<storyID>");
																		pch = strstr(pchName, "</storyID>");
																		if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																		{
																			*pch = 0;  //null term the ID!
																			pchField = (char*) malloc(strlen(pchName)+1);
																			if(pchField)  // have to at least have this field
																			{
																				strcpy(pchField, pchName);
																				pEvent->m_pszReconcileKey = pchField;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d story ID: %s\r\n", clock(),pEvent->m_pszReconcileKey?pEvent->m_pszReconcileKey:"NULL");
		fflush(fp);
		fclose(fp);
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
// deal with iTX on the first event of the batch

																				if(pConn->m_ulFlags&OMNI_ITX)
																				{
																					if(usRunningEventCount<1)
																					{
																						// delete any preceding things left over, but include done count number of items.

	EnterCriticalSection(&pConn->m_crit);
																						CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread)
																							{
																								int itxidx=0;
																								//int itxcount = pList->GetEventCount();

																								while(itxidx < pList->GetEventCount())
																								{
																									CCAEvent* pitxEvent = pList->GetEvent(itxidx);
																									if(pitxEvent)
																									{
																										// every event gets this to start:
																										pitxEvent->m_ulFlags &= ~OMNI_FLAGS_FOUND; // make sure this is cleared

																										if(strcmp(pitxEvent->m_pszReconcileKey, pEvent->m_pszReconcileKey)==0)
																										{
																											pitxEvent->m_ulFlags |= OMNI_FLAGS_FOUND;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d iTX search: initial found %d: %s\r\n", clock(), itxidx, pitxEvent->m_pszReconcileKey?pitxEvent->m_pszReconcileKey:"NULL");
		fflush(fp);
		fclose(fp);
	}
}

																											// found it
																											int delidx=-1;
																											if(itxidx>pList->m_usDoneCount)
																											{
																												// items to delete
																												delidx = itxidx - pList->m_usDoneCount - 1; // 0,1,2,3,4, curr at 4, done = 3, delete only index 0 delidx = 4-3-1 = 0
																											}
																											// now count back and set the next pList->m_usDoneCount events to done, then delete the rest off the top.
																											int itxpastidx = itxidx-1;

																											while(itxpastidx>=0)																											
																											{
																												if(itxpastidx <= delidx)
																												{
																													if(!pConn->m_bKillThread)
																													{
																														nReturnCode = pList->DeleteEvent(itxpastidx, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																														if(nReturnCode == OMNI_SUCCESS) itxidx--;// if the delete is successful, need to back up the index after deletion to continue from the right place.

																														if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																														{
																															fp = fopen(pConn->m_pszDebugFile, "at");
																															if(fp)
																															{
																																// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																																//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																																fprintf(fp, "%d DeleteEvent %d (<%d) (iTX Delete) for channel %d returned with %d\r\n", clock(), itxpastidx, delidx, pList->m_usListID, nReturnCode);
																																fflush(fp);
																																fclose(fp);
																															}
																														}
																													}
																												}
																												else  // change status to done
																												{
																													pitxEvent = pList->GetEvent(itxpastidx);
																													if(pitxEvent)
																													{
																														pitxEvent->m_usStatus = C_DONE; //done.

																														nReturnCode = pList->UpdateEvent(pitxEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);
																													
																														pitxEvent->m_ulFlags |= OMNI_FLAGS_FOUND;

																														if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																														{
																															fp = fopen(pConn->m_pszDebugFile, "at");
																															if(fp)
																															{
																																fprintf(fp, "%d UpdateEvent %d %s iTX done\r\n", clock(), itxpastidx, pitxEvent->m_pszReconcileKey);
																																fflush(fp);
																																fclose(fp);
																															}
																														}
																													}
																												}

																												itxpastidx--;
																											}

																											// done with the past events search 
																										}// was the first event matching the received first event.


																									}
																									itxidx++;
																								}
																							}
																						}
	LeaveCriticalSection(&pConn->m_crit);
																					}
																				}

// deal with iTX on the first event of the batch
///////////////////////////////////////////////////////////////////////////////////////////////////////

																				*pch = '<'; // put it back so we can search whole string again.

																				pchName = strstr(pchNext, "<clipId>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<clipId>");
																					pch = strstr(pchName, "</clipId>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszID = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}


																				pchName = strstr(pchNext, "<title>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<title>");
																					pch = strstr(pchName, "</title>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszTitle = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extract %d %d\r\n", clock(),pConn->m_ppfi,pConn->m_nNumFields );
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																				bool bFieldsFound = false;

																				if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																				{
																					// we found some fields!
																					bFieldsFound = true;	

																					for(int i=0; i<pConn->m_nNumFields; i++)
																					{

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extract %d %d %d\r\n", clock(), i, pConn->m_ppfi[i], pConn );
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						if((pConn->m_ppfi[i])&&(pConn->m_ppfi[i]->m_pszFieldName))
																						{
																							char pszTag[256];
																							sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
					//AfxMessageBox(pszTag);
																							pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																							if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																							{
																								sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																								pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																								if(pConn->m_ppfi[i]->m_pchEndTag)
																								{
																									pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																								}
																								if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																								{
																									int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																									pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																									{
																										memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																										memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																									}
																								}
																							}
																						}
																					}

																				//	Sleep(50);

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extracted\r\n", clock());
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																					//we have to check validity of fields (top-level-ness)
																					bool bAllTopLevel = false;
																					
																					while(!bAllTopLevel)
																					{
																						bool bOverlapFound = false;
																						i=0; 
																						while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																						{
																							for(int j=0; j<pConn->m_nNumFields-1; j++)
																							{
																								if(j!=i)
																								{
																									if(
																											(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																										&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																										)
																									{
																										// i is inside j, need to look for a different one and start over.
																										bOverlapFound = true;

																										char pszTag[256];
																										sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																										pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																										if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																										{
																											sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																											if(pConn->m_ppfi[i]->m_pchEndTag)
																											{
																												pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																											}
																											if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																											{
																												int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																												pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																												if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																												{
																													memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																													memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																												}
																											}
																										}
																										else
																										{
																											if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																											pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																										}
																										break;
																									}

																								}
																							}
																							
																							i++;
																						}

																						if(!bOverlapFound) bAllTopLevel = true;
																					}


																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data validated\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}
																					//assemble the data string
																					int nDataLen = 0;
																					for(i=0; i<pConn->m_nNumFields; i++)
																					{
																						if(pConn->m_ppfi[i]->m_pszFieldInfo)
																						{
																							nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
	//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																						}
																					}

																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data assembled\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																					
																					if(nDataLen)
																					{
																						pchField = (char*) malloc(nDataLen+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, "");
																							for(i=0; i<pConn->m_nNumFields; i++)
																							{
																								if(pConn->m_ppfi[i]->m_pszFieldInfo)
																								{
																									strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																									free(pConn->m_ppfi[i]->m_pszFieldInfo);
																									pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																								}
																							}
																							pEvent->m_pszData = pchField;
	//																						AfxMessageBox(pEvent->m_pszData );
																						}
																					}

																				}

																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data tagged\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																				if(!bFieldsFound)
																				{
																					// use default, minus tags
																					pchName = strstr(pchNext, "<data>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<data>");
																						pch = strstr(pchName, "</data>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszData = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}
																				}
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data end\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																				pchName = strstr(pchNext, "<startTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<startTime>");
																					pch = strstr(pchName, "</startTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						if(pConn->m_ulFlags&OMNI_ITX)
																						{
//	An important value is 621355968000000000.  this is the number of dateTime ticks from 0001 to 1970.  this offset gets you back to unix time

//																							pEvent->m_dblOnAirTimeInternal = (atof(pchName) - 621355968000000000.0)/10000.0;  // now unixtime in seconds.  // this was wrong!!!!  it's milliseconds! 
																							pEvent->m_dblOnAirTimeInternal = (atof(pchName) - 621355968000000000.0)/10000000.0;  // now unixtime in seconds.  // corrected here 2.1.1.18

																							// then multiply by number of frames per second.  Leave it as a non-integer, that is fine.
																							switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																							{
																							case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 25.0;
																								} break;
																							case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 30.0;
																								} break;
																							default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 29.97;
																								}
																								break;
																							}
																							
																						}
																						else
																						{
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);
																						}
																						int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																						else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																					}
																				}

																				if((!(pConn->m_ulFlags&OMNI_SYSTIME))&&((!pConn->m_bPreventServerTimeFromEvents)||(!bProcessedTime))) //2.1.1.18
									//											if(!(pConn->m_ulFlags&OMNI_SYSTIME))
																				{
																					pchName = strstr(pchNext, "<currentTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<currentTime>");
																						pch = strstr(pchName, "</currentTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																							//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																							pConn->UpdateServerTime(false);
																							bProcessedTime = true;
																						}
																					}
																				}

																				pchName = strstr(pchNext, "<type>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<type>");
																					pch = strstr(pchName, "</type>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usType = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<dur>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<dur>");
																					pch = strstr(pchName, "</dur>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulDurationMS = (unsigned long) nMS;
																						else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																					}
																				}

																				pchName = strstr(pchNext, "<status>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<status>");
																					pch = strstr(pchName, "</status>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usStatus = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<timeMode>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<timeMode>");
																					pch = strstr(pchName, "</timeMode>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usControl = (unsigned short)atoi(pchName);
																					}
																				}


																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d updating\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}


	//																			Ok, lets update the event!
																				if(nListIndex>=0)
																				{
																					if(!pConn->m_bKillThread) 
																					{
EnterCriticalSection(&pConn->m_crit);
																						CCAList* pList = pConn->GetList(nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread) 
																							{

																								double dblTime = ((double)timestamp.millitm/1000.0);
																								if(pConn->m_bUseUTC)
																								{
																									dblTime += (double)(timestamp.time); // UTC
																								}
																								else
																								{
																									dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																								}

																								if(pConn->m_bUseDST)
																								{
																									dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																								}

																								pEvent->m_dblUpdated = dblTime;  // UTC 
																								 
																								nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);

																								pEvent->m_ulFlags |= OMNI_FLAGS_FOUND;
																							
																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent Sleep %d\r\n", clock(), pConn->m_ulInterEventIntervalMS);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																								
																								Sleep(pConn->m_ulInterEventIntervalMS);   // was zero, but pegged processor

																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent (roReplace) %s for channel %d returned %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																								nReturnCode = pList->UpdateListStatus();

																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateListStatus %d returned %d\r\n", clock(), pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																							}
																						}

																						ulCounter++;


																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d counter now %d\r\n", clock(),ulCounter);
																									fflush(fp);
																									fclose(fp);
																								}
																							}
/*
																	EnterCriticalSection(&pConn->m_critCounter);
																					if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																					pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
*/
LeaveCriticalSection(&pConn->m_crit);
																					}
																				}
																			}
																		}
																	}
																	//else // cant do anything with this event, so just have to skip.
																}

																pchNext = strstr(pchEndRec, "<story>");
																usRunningEventCount++;
															} // while records exist

///////////////////////////////////////////////////////////////////////////////////////////////////////
// deal with iTX after the last event of the batch

															if(pConn->m_ulFlags&OMNI_ITX)
															{
																					// delete any preceding things left over, but include done count number of items.

	EnterCriticalSection(&pConn->m_crit);
																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(!pConn->m_bKillThread)
																	{
																		int itxcount = pList->GetEventCount()-1;

																		while( itxcount >= 0 )
																		{
																			CCAEvent* pitxEvent = pList->GetEvent(itxcount);
																			if(pitxEvent)
																			{
																				// every event gets this to start:
																				if(pitxEvent->m_ulFlags&OMNI_FLAGS_FOUND)
																				{
																					// was in the received data, so just clear the flag.
																					pitxEvent->m_ulFlags &= ~OMNI_FLAGS_FOUND;
																				}
																				else
																				{
																					// wasn't in the received data, so must be deleted.
																					if(!pConn->m_bKillThread)
																					{
																						nReturnCode = pList->DeleteEvent(itxcount, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

																						if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																						{
																							fp = fopen(pConn->m_pszDebugFile, "at");
																							if(fp)
																							{
																								// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																								//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																								fprintf(fp, "%d DeleteEvent %d (iTX Delete) for channel %d returned with %d\r\n", clock(), itxcount, pList->m_usListID, nReturnCode);
																								fflush(fp);
																								fclose(fp);
																							}
																						}
																					}
																				}
																			}
																			itxcount--;
																		}
																	}
																}

	LeaveCriticalSection(&pConn->m_crit);
															}


// deal with iTX after the last event of the batch
///////////////////////////////////////////////////////////////////////////////////////////////////////
														}
													}
												}
											}
											else  // not roReplace
											{
												pchName = strstr(pchXML, "<roStoryReplace>");
												if(pchName)
												{
													// it's a replace single item record
													pchEnd = strstr(pchName, "</roStoryReplace>");
													if(pchEnd)  // we found a correctly encapsulated record.
													{
														pchNext = strstr(pchName, "<roID>");
														if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
														{
															pchNext += strlen("<roID>");
															usListID = atoi(pchNext);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roStoryReplace\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}


															int nListIndex = OMNI_SUCCESS;
															// add or update the list!
/*
															if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
															{
																EnterCriticalSection(&g_actives.m_critActive);
												//				AfxMessageBox("roStoryReplace, about to enter channel active check");
																nListIndex = g_actives.IsChannelActive(usListID);
												//				AfxMessageBox("roStoryReplace, left channel active check");
																LeaveCriticalSection(&g_actives.m_critActive);
															}
*/
															if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
															{ 
	EnterCriticalSection (&pConn->m_crit);
																nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
															}

															if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
															{


																// then, deal with the events we have.
																pchNext = strstr(pchName, "<story>");
																while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))  // this while should only get hit once.
																{
																	pchName = strstr(pchNext, "</story>");
																	if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																	{
																		char* pchEndRec = pchName;
																		// ok, now pchNext is the beginning, 
																		// and pchEnd is the end of the list,
																		// pchEndRec is the end of the record, and we can use pchName for the fields
																		// we are interested in getting:
																		
																		//unsigned short m_usType;              <type>      // internal colossus type
																		//char* m_pszID;												<clipId>		// the clip ID
																		//char* m_pszTitle;											<title>		  // the title
																		//char* m_pszData;  										<data> 			//must encode zero.
																		//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																		//unsigned char  m_ucSegment;														// not used

																		// following 2 compiled from <startTime>
																		//unsigned long  m_ulOnAirTimeMS;
																		//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																		//unsigned long  m_ulDurationMS;        <dur>        // duration
																		//unsigned short m_usStatus;						<status>     // status (omnibus code)
																		//unsigned short m_usControl;           <timeMode>   // time mode

																		// additionally, up at the list level, we can compile from
																		//  <currentTime>
																		//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																		//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																		//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																		// let's create a new event object;
																		CCAEvent* pEvent = new CCAEvent;
																		char* pchField;

																		pchName = strstr(pchNext, "<storyID>");
																		if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																		{

//20151106 2.1.1.15 - added parent pointer ssignment to self.  make default behavior, no option.  never referenced before so ignored elsewhere.  needed for Nucleus parameter queries
																			pEvent->m_pParent = pEvent;

																			pchName += strlen("<storyID>");
																			pch = strstr(pchName, "</storyID>");
																			if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																			{
																				*pch = 0;  //null term the ID!
																				pchField = (char*) malloc(strlen(pchName)+1);
																				if(pchField)  // have to at least have this field
																				{
																					strcpy(pchField, pchName);
																					pEvent->m_pszReconcileKey = pchField;
																					*pch = '<'; // put it back so we can search whole string again.

																					pchName = strstr(pchNext, "<clipId>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<clipId>");
																						pch = strstr(pchName, "</clipId>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszID = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}


																					pchName = strstr(pchNext, "<title>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<title>");
																						pch = strstr(pchName, "</title>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszTitle = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


																					bool bFieldsFound = false;

																					if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																					{
																						// we found some fields!
																						bFieldsFound = true;	

																						for(int i=0; i<pConn->m_nNumFields; i++)
																						{

																							if(pConn->m_ppfi[i]->m_pszFieldName)
																							{
																								char pszTag[256];
																								sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
						//AfxMessageBox(pszTag);
																								pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																								if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																								{
																									sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																									pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																									if(pConn->m_ppfi[i]->m_pchEndTag)
																									{
																										pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																									}
																									if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																									{
																										int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																										pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																										if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																										{
																											memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																											memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																										}
																									}
																								}
																							}
																						}


																						//we have to check validity of fields (top-level-ness)
																						bool bAllTopLevel = false;
																						
																						while(!bAllTopLevel)
																						{
																							bool bOverlapFound = false;
																							i=0; 
																							while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																							{
																								for(int j=0; j<pConn->m_nNumFields-1; j++)
																								{
																									if(j!=i)
																									{
																										if(
																												(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																											&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																											)
																										{
																											// i is inside j, need to look for a different one and start over.
																											bOverlapFound = true;

																											char pszTag[256];
																											sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																											if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																											{
																												sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																												pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																												if(pConn->m_ppfi[i]->m_pchEndTag)
																												{
																													pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																												}
																												if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																												{
																													int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																													if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																													pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																													if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																													{
																														memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																														memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																													}
																												}
																											}
																											else
																											{
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																												pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																											}
																											break;
																										}

																									}
																								}
																								
																								i++;
																							}

																							if(!bOverlapFound) bAllTopLevel = true;
																						}

																						//assemble the data string
																						int nDataLen = 0;
																						for(i=0; i<pConn->m_nNumFields; i++)
																						{
																							if(pConn->m_ppfi[i]->m_pszFieldInfo)
																							{
																								nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
		//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																							}
																						}
																						
																						if(nDataLen)
																						{
																							pchField = (char*) malloc(nDataLen+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, "");
																								for(i=0; i<pConn->m_nNumFields; i++)
																								{
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)
																									{
																										strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																										free(pConn->m_ppfi[i]->m_pszFieldInfo);
																										pConn->m_ppfi[i]->m_pszFieldInfo = NULL;

																									}
																								}
																								pEvent->m_pszData = pchField;
		//																						AfxMessageBox(pEvent->m_pszData );
																							}
																						}

																					}


																					if(!bFieldsFound)
																					{
																						// use default, minus tags
																						pchName = strstr(pchNext, "<data>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<data>");
																							pch = strstr(pchName, "</data>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
																								*pch = 0;  //null term the ID!
																								pchField = (char*) malloc(strlen(pchName)+1);
																								if(pchField)  // have to at least have this field
																								{
																									strcpy(pchField, pchName);
																									pEvent->m_pszData = pchField;
																									*pch = '<'; // put it back so we can search whole string again.
																								}
																							}
																						}
																					}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																					pchName = strstr(pchNext, "<startTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<startTime>");
																						pch = strstr(pchName, "</startTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							// no need to deal with iTX here, since iTX does not send this command.
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);
																							int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																							else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																						}
																					}

																					if((!(pConn->m_ulFlags&OMNI_SYSTIME))&&((!pConn->m_bPreventServerTimeFromEvents)||(!bProcessedTime))) //2.1.1.18
	//																				if(!(pConn->m_ulFlags&OMNI_SYSTIME))
																					{
																						pchName = strstr(pchNext, "<currentTime>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<currentTime>");
																							pch = strstr(pchName, "</currentTime>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
																								int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																								if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																								//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																								pConn->UpdateServerTime(false);
																								bProcessedTime=true;
																							}
																						}
																					}

																					pchName = strstr(pchNext, "<type>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<type>");
																						pch = strstr(pchName, "</type>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usType = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<dur>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<dur>");
																						pch = strstr(pchName, "</dur>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pEvent->m_ulDurationMS = (unsigned long) nMS;
																							else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																						}
																					}

																					pchName = strstr(pchNext, "<status>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<status>");
																						pch = strstr(pchName, "</status>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usStatus = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<timeMode>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<timeMode>");
																						pch = strstr(pchName, "</timeMode>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usControl = (unsigned short)atoi(pchName);
																						}
																					}

		//																			Ok, lets update the event!
																					if(nListIndex>=0)
																					{
																						if(!pConn->m_bKillThread) 
																						{
EnterCriticalSection(&pConn->m_crit);
																							CCAList* pList = pConn->GetList(nListIndex);
																							if(pList)
																							{
																								if(!pConn->m_bKillThread) 
																								{
																									double dblTime = ((double)timestamp.millitm/1000.0);
																									if(pConn->m_bUseUTC)
																									{
																										dblTime += (double)(timestamp.time); // UTC
																									}
																									else
																									{
																										dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																									}

																									if(pConn->m_bUseDST)
																									{
																										dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																									}
																									pEvent->m_dblUpdated = dblTime;


																									nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);

																									Sleep(pConn->m_ulInterEventIntervalMS);  // was zero, but pegged processor
																									
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d UpdateEvent (roStoryReplace) %s for channel %d returned %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																									nReturnCode = pList->UpdateListStatus();

																								}
																							}

																							ulCounter++;

/*
																	EnterCriticalSection(&pConn->m_critCounter);
																						if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																						pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
*/
LeaveCriticalSection(&pConn->m_crit);
																						}
																					}
																				}
																			}
																		}
																		//else // cant do anything with this event, so just have to skip.
																	}

																	pchNext = strstr(pchEnd, "<story>");
																}
															}
														} //if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
													}
												} 
												else
												if(!pConn->m_bKillThread)
												{
													pchName = strstr(pchXML, "<roStoryDelete>");
													if(pchName)
													{
															// it's a delete record
														pchEnd = strstr(pchName, "</roStoryDelete>");
														if(pchEnd)  // we found a correctly encapsulated record.
														{
															pchNext = strstr(pchName, "<roID>");
															if((pchNext)&&(pchNext<pchEnd))
															{
																pchNext += strlen("<roID>");
																usListID = atoi(pchNext);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roStoryDelete\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}

																int nListIndex = OMNI_SUCCESS;
/*
																if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
																{
																	EnterCriticalSection(&g_actives.m_critActive);
													//				AfxMessageBox("roStoryDelete, about to enter channel active check");
																	nListIndex = g_actives.IsChannelActive(usListID);
												//				//	AfxMessageBox("roStoryDelete, left channel active check");
																	LeaveCriticalSection(&g_actives.m_critActive);
																}
*/
																if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
																{ 
	EnterCriticalSection (&pConn->m_crit);
																	nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
																}

																if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
																{
																	//pConn->m_ppList[i]->UpdateEvent(pEvent);

																	// then, find the story ID to delete
																	pchNext = strstr(pchName, "<storyID>");
																	if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
																	{
																		pchNext += strlen("<storyID>");
																		pch = strstr(pchNext, "</storyID>");
																		if((pch)&&(pch<pchEnd))
																		{
																			*pch = 0;  //null term the ID!
																			if(!pConn->m_bKillThread) 
																			{
EnterCriticalSection(&pConn->m_crit);
																				CCAList* pList = pConn->GetList(nListIndex);
																				if(pList)
																				{
				EnterCriticalSection(&pList->m_critEvents);
																					int j = pList->GetEventIndexByRecKey(pchNext);
				LeaveCriticalSection(&pList->m_critEvents);
																					if(j>=0)
																					{
																						if(!pConn->m_bKillThread)
																						{
																							nReturnCode = pList->DeleteEvent(j, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																									//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																									fprintf(fp, "%d DeleteEvent (roStoryDelete) for channel %d returned with %d\r\n", clock(), pList->m_usListID, nReturnCode);
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						}

																						ulCounter++;
	/*
																		EnterCriticalSection(&pConn->m_critCounter);
																						if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																						pConn->m_ulCounter++;
																		LeaveCriticalSection(&pConn->m_critCounter);
	*/
																					}
																				}
LeaveCriticalSection(&pConn->m_crit);
																			}
																		}
																	}
																}
															}
														}//if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))


													}  // else it's an unrecognized mos command, or time to die
												}
											}


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d An output message has been processed. %d\r\n", clock(), ulCounter);
		fflush(fp);
		fclose(fp);

	}
}


										}

										EnterCriticalSection(&pConn->m_critCounter);
														if(ULONG_MAX - pConn->m_ulCounter < ulCounter )
														{
															ulCounter -= (ULONG_MAX - pConn->m_ulCounter);
															pConn->m_ulCounter = ulCounter;
														}
														else	pConn->m_ulCounter += ulCounter;
										LeaveCriticalSection(&pConn->m_critCounter);


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d free [%d]\r\n", clock(), pchXML);
		fflush(fp);
		fclose(fp);

	}
}


										}

										free(pchXML);
										pchXML = NULL;

										if (pchXMLStream) 
										{
											pchEnd = strstr(pchXMLStream, "</mos>");

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d chkmos rem %d -> %d (%d)\r\n", clock(), pchXMLStream, pchEnd, pchEnd-pchXMLStream);
		fflush(fp);
		fclose(fp);

	}
}

										}
										else pchEnd = NULL;
									}  // while </mos> exists.
								}//if(pchXMLStream)
								// dont do a free(pch);  
							}  // else out of mem, so just skip
							else
							{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "**** Couldn't allocate buffer!\r\n");
		fflush(fp);
		fclose(fp);
	}
}
							}
						} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("out\r\n", 1, strlen("out\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
					} // if getline succeeds.
					else
					{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "**** GetLineError %d: %s\r\n", nReturnCode, chError);
		fflush(fp);
		fclose(fp);

	}
}

						// here, we either have timed out because theres no data, or, the connection has been lost.
						// if we lost the conn, need to re-establish.
						if(nReturnCode == NET_ERROR_CONN)		// connection lost
						{
							pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
							pConn->m_ulFlags |= OMNI_SOCKETERROR;
							nRetry++;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d **** retry %d, lost connection\r\n", clock(), nRetry);
		fflush(fp);
		fclose(fp);

	}
}

							if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
							{
								EnterCriticalSection(pConn->m_pcritSendMsg);
								_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Lost connection to %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
								pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:error", szMsg);
								LeaveCriticalSection(pConn->m_pcritSendMsg);
							}
							break;  // break out of while loop, closes connection, starts again
						}
						else
						{
							// we prob just havent gotten a message because no data.  
							// so, use the time to purge old events if it is time to do so.
							if(pConn->m_ulCheckInterval>0)	// number of seconds between checks (0 turns off)
							{
								_ftime(&timestamp);
								if((pConn->m_ulLastPurge+pConn->m_ulCheckInterval)<(unsigned long)timestamp.time)
								{
									// it's time!
									if((pConn->m_ulRefUnixTime>0)&&(pConn->m_ulRefUnixTime!=TIME_NOT_DEFINED)&&(pConn->m_ulRefUnixTime>pConn->m_ulExpiryPeriod)) // need a valid expiry time
									{
										pConn->m_ulLastPurge = (unsigned long)timestamp.time;
										int l=0;
										while((l<pConn->GetListCount())&&(!pConn->m_bKillThread))
										{
											CCAList* pList = pConn->GetList(l);
											int nCount = pList->GetEventCount();
											if((pList)&&(nCount>0))
											{
												int e=0;
												unsigned long ulExpiry = pConn->m_ulRefUnixTime-pConn->m_ulExpiryPeriod;
												while((e<nCount)&&(!pConn->m_bKillThread))
												{
													CCAEvent* pEvent = pList->GetEvent(e);
													if(
															(pEvent)
														&&(((unsigned long)pEvent->m_dblOnAirTime+(pEvent->m_ulDurationMS/1000))<ulExpiry)
														)
													{
														// purge!
														if(!pConn->m_bKillThread)
														{
															nReturnCode = pList->DeleteEvent(e, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
															if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
															{
																fp = fopen(pConn->m_pszDebugFile, "at");
																if(fp)
																{
																	// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																	//fprintf(fp, "DeleteEvent (Expiry Purge) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																	fprintf(fp, "DeleteEvent (Expiry Purge) for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																	fflush(fp);
																	fclose(fp);
																}
															}
															if(nReturnCode<OMNI_SUCCESS)
															{
																e++;  // just increment
															}
															else
															{
																// update the list status.
																pList->UpdateListStatus(); // but don't force it, just do if it's time. 2.1.1.18
															}
														}
													}
													else
													{
														e++;
													}
												}
											}
											l++;
										}
									}
								}
							}
						}
					} // end of else from if(nReturnCode == NET_SUCCESS)

					// and lets check if we've timed out on the connection, no matter what the success was
					_ftime( &timestamp );
					if((!pConn->m_bKillThread)&&((pConn->m_ulConnLastMessage + pConn->m_ulConnTimeout) < (unsigned long)timestamp.time ))
					{
						// we timed out, need to disconnect and reconnect.
						pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
						nRetry++;


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "***** retry %d    %d\r\n", nRetry, clock());
		fflush(fp);
		fclose(fp);

	}
}
						
						if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
						{
							EnterCriticalSection(pConn->m_pcritSendMsg);
							_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Connection to %s:%d timed out.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
							pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:timeout", szMsg);
							LeaveCriticalSection(pConn->m_pcritSendMsg);
						}
						if(pchBuffer) { try{free(pchBuffer); pchBuffer=NULL; } catch(...){} } //free this memory before breaking out of the while.

						break;  // break out of while loop, closes connection, starts again
					}

					if(pchBuffer) { try{free(pchBuffer); pchBuffer=NULL; } catch(...){} } 

					if(!pConn->m_bKillThread) Sleep(1);  // dont peg processor
	/*
					fp = fopen("omnibus.log", "at");
					if(fp)
					{
						fprintf(fp, "here %d\r\n", timestamp.time);
						fflush(fp);
						fclose(fp);
					}
	*/
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("while\r\n", 1, strlen("while\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

				} //while(!pConn->m_bKillThread)  // kill the comm thread
				// if we are here we may have broken out because we had an error.

				net.CloseConnection(pConn->m_socket);
			}  // else couldnt open connection
			else
			{
				pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
				pConn->m_ulFlags |= OMNI_SOCKETERROR;
				nRetry++;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "****** retry %d    %d\r\n", nRetry, clock());
		fflush(fp);
		fclose(fp);

	}
}

				if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
				{
					EnterCriticalSection(pConn->m_pcritSendMsg);
					_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Connection to %s:%d failed.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
					pConn->m_lpfnSendMsg(OMNI_SENDMSG_ERROR, "AdaptorComm:connect", szMsg);
					LeaveCriticalSection(pConn->m_pcritSendMsg);
				}
			}

			_ftime(&timestamp);
			unsigned long ulExpiry = (unsigned long)timestamp.time + pConn->m_ulConnectionInterval;
			while((!pConn->m_bKillThread)&&((unsigned long)timestamp.time<ulExpiry)) // make this interruptible sleep
			{
				_ftime(&timestamp);

				Sleep(10); 
			}

		}  // while retrying


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "Ending queue thread\r\n", nRetry, clock());
		fflush(fp);
		fclose(fp);

	}
}

		pConn->m_bXMLQueueKill = true;
		while( pConn->m_bXMLQueueStarted) Sleep(1);  // must want for the queue threads to die.
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "Queue thread ended\r\n", nRetry, clock());
		fflush(fp);
		fclose(fp);

	}
}



		if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
		{

//			char szSQL[DB_SQLSTRING_MAXLEN];
			if((pConn->m_pszExchangeTable)&&(strlen(pConn->m_pszExchangeTable)))
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = 'Server_Time' AND flag LIKE '%s:%05d%%'",
					pConn->m_pszExchangeTable,
					pConn->m_pszServerAddress,
					pConn->m_usPort
					);
if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
				{
					// error.
				}
if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
			}


			if((pConn->m_pszConnectionsTable)&&(strlen(pConn->m_pszConnectionsTable)))
			{

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_time = -1.0, \
server_status = 0, \
server_lists = -1, \
server_changed = -1, \
server_last_update = %d%03d WHERE \
conn_ip = '%s' AND conn_port = %d",
					pConn->m_pszConnectionsTable,
					(unsigned long)(timestamp.time), //unixtime// - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
					timestamp.millitm,

					pConn->m_pszServerAddress, pConn->m_usPort
					);

				
if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
				{
					// error.
				}
if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
			}

		}

		pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		_ftime(&timestamp);

		fprintf(fp, "%d.%03d (%d) Ending Comm thread.\r\n", timestamp.time, timestamp.millitm, clock());
		fflush(fp);
		fclose(fp);

	}
}

		pConn->m_bThreadStarted = false;  // state of comm thread

	}

	pConn = NULL;
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("bye\r\n", 1, strlen("bye\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
	Sleep(100);
	_endthread();

	Sleep(200);

}

#else // ! HELIOS_NEW_DB_SCHEMA
void CommThread(void* pvArgs)
{
	CCAConn* pConn = (CCAConn*) pvArgs;
	FILE* fp;
	if(pConn)
	{
		if(pConn->m_bThreadStarted) return; // only one per conn.
		pConn->m_bThreadStarted = true;  // state of comm thread

		CNetUtil	net;  // the connection exclusive networking obj
		SOCKET s;
		unsigned long ulRetry =0 ;
		char szMsg[DB_ERRORSTRING_LEN];

						// create tables if they dont exist.
		if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
		{
			char szSQL[DB_SQLSTRING_MAXLEN];
			if((pConn->m_pszExchangeTable)&&(strlen(pConn->m_pszExchangeTable)))
			{
				_timeb timestamp;
				_ftime( &timestamp );
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('Server_Time', '%s:%05d|%d%03d', -1)",
					pConn->m_pszExchangeTable,
					pConn->m_pszServerAddress,
					pConn->m_usPort,
					(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
					timestamp.millitm
					);
if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
				{
					// error.
				}
if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
			}

			if((pConn->m_pszEventsTable)&&(strlen(pConn->m_pszEventsTable)))
			{
				pConn->m_pdb->AddTable(pConn->m_pdbConn, pConn->m_pszEventsTable); // add errors table to schema
				if(pConn->m_pdb->TableExists(pConn->m_pdbConn, pConn->m_pszEventsTable)==DB_EXISTS)
				{
					// get the schema
					pConn->m_pdb->GetTableInfo(pConn->m_pdbConn, pConn->m_pszEventsTable);

				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 

"CREATE TABLE [%s] (\"%s\" int identity(1,1) NOT NULL, \"%s\" varchar(32) NOT NULL, \"%s\" int NOT NULL, \
\"%s\" int NOT NULL, \"%s\" varchar(32) NOT NULL, \"%s\" varchar(64) NOT NULL, \
\"%s\" varchar(64) NOT NULL, \"%s\" varchar(4096) NULL, \"%s\" int NOT NULL, \
\"%s\" int NOT NULL, \"%s\" int NOT NULL, \"%s\" float NOT NULL, \"%s\" float NOT NULL, \
\"%s\" int NOT NULL, \"%s\" float NOT NULL, \"%s\" varchar(512) NULL, \
\"%s\" int NULL);",
										pConn->m_pszEventsTable,
										OMNI_DB_ITEMID_NAME, //				"itemid"    // of the DB event.
										OMNI_DB_CONNIP_NAME, //				"conn_ip"    // of colossus connection, varchar 32
										OMNI_DB_CONNPORT_NAME, //			"conn_port"  // of colossus connection, int
										OMNI_DB_LISTID_NAME, //				"list_id"			// omnibus stream number, int
										OMNI_DB_EVENTID_NAME, //			"event_id"    // unique ID (story ID), varchar 32, recKey on harris
										OMNI_DB_EVENTCLIP_NAME, //		"event_clip"  // clip ID, varchar 32
										OMNI_DB_EVENTTITLE_NAME, //		"event_title" // title, varchar 64
										OMNI_DB_EVENTDATA_NAME, //		"event_data"  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
										OMNI_DB_EVENTTYPE_NAME, //		"event_type"  // internal colossus type, int
										OMNI_DB_EVENTSTATUS_NAME, //	"event_status"  // internal colossus status, int
										OMNI_DB_EVENTTMODE_NAME, //		"event_time_mode"  // time mode, int
										OMNI_DB_EVENTFSJ70_NAME, //		"event_start_internal" // frames since Jan 1970, SQL float (C++ double) 
										OMNI_DB_EVENTSTART_NAME, //		"event_start" // start time of event, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
										OMNI_DB_EVENTDURMS_NAME, //		"event_duration" // duration in milliseconds, int
										OMNI_DB_EVENTUPDATED_NAME, //	"event_last_update" // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
											// fields from other apps
										OMNI_DB_APPDATA_NAME, //			"app_data" // application data, varchar 512, allow null
										OMNI_DB_APPDATAAUX_NAME	//	"app_data_aux" // auxiliary application data, int, allow null
									);

					//					AfxMessageBox(szSQL);


					if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
					if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)>=DB_SUCCESS)
					{
						pConn->m_pdb->GetTableInfo(pConn->m_pdbConn, pConn->m_pszEventsTable);
					}
					if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
				// else error notice...

				}

				// now that the table existence has been ensured, delete things more than a day old.
				_timeb timestamp;
				_ftime(&timestamp);

				double dblTime;
				if(pConn->m_bUseUTC)
				{
					dblTime = (double)(timestamp.time - 86400); // UTC
				}
				else
				{
					dblTime = (double)(timestamp.time - 86400 - (timestamp.timezone*60));  // local time
				}

				if(pConn->m_bUseDST)
				{
					dblTime += (double)(timestamp.dstflag?3600:0);  // DST
				}



				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"DELETE FROM %s WHERE %s < %.03f;", 
										pConn->m_pszEventsTable,
										OMNI_DB_EVENTUPDATED_NAME, dblTime
										);
											//AfxMessageBox(szSQL);

				if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Sent SQL: %s\r\n", szSQL);
						fflush(fp);
						fclose(fp);

					}
				}

				if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
				{
					;  // could do a message alert at some point.
					//AfxMessageBox("error deleting old");
				}
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);

			}//if((pConn->m_pszEventsTable)&&(strlen(pConn->m_pszEventsTable)))
		}//if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
		
		_timeb timestamp;

		while((ulRetry<pConn->m_ulConnectionRetries)&&(!pConn->m_bKillThread))
		{
			if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "Trying connection to %s:%d...\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
					fflush(fp);
					fclose(fp);

				}
			}

			if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg)&&(ulRetry>0))
			{
				EnterCriticalSection(pConn->m_pcritSendMsg);
				_snprintf(szMsg, DB_ERRORSTRING_LEN, "Attempting to re-establish connection to %s:%d.  (Retry %d.)\r\n",
					pConn->m_pszServerAddress, pConn->m_usPort, ulRetry);
				pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:reconnect", szMsg);
				LeaveCriticalSection(pConn->m_pcritSendMsg);
			}

			// lets set the time so that the initial connection doesnt immediately time out.
			_ftime(&timestamp);
			pConn->m_ulConnLastMessage = timestamp.time;


			if(net.OpenConnection(
				pConn->m_pszServerAddress, 
				pConn->m_usPort, 
				&s,
				1000, 1000  // 1000 millisecond com timeouts to not freeze thread
				)>=NET_SUCCESS)
			{	
				pConn->m_ulFlags &= ~OMNI_SOCKETERROR;
				pConn->m_ulFlags |= OMNI_SOCKETCONNECTED;
				ulRetry = 0;
				char* pchBuffer = NULL;
				unsigned long ulBufferLen = 0;

				char buffer[32];
				strcpy(buffer, "<mos><roReq></roReq></mos>" );
				ulBufferLen = strlen(buffer)+1; //send the term 0

				if (pConn->m_bKillThread)
				{
					net.CloseConnection(s);
					pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
					pConn->m_bThreadStarted = false;  // state of comm thread
					if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
					{
						EnterCriticalSection(pConn->m_pcritSendMsg);
						_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Disconnecting %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
						pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:disconnect", szMsg);
						LeaveCriticalSection(pConn->m_pcritSendMsg);
					}

					_endthread();
					return;
				}

				if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
				{
					EnterCriticalSection(pConn->m_pcritSendMsg);
					_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Connection established to %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
					pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:connect", szMsg);
					LeaveCriticalSection(pConn->m_pcritSendMsg);
				}

				// first, set up the connection by sending <mos><roReq></roReq></mos>
				int n = net.SendLine((unsigned char*)buffer, ulBufferLen, s);//, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
				
	/*
				fp = fopen("omnibus.log", "at");
				if(fp)
				{
					fprintf(fp, "SendLine returned %d\r\n", n);
					fflush(fp);
					fclose(fp);
				}
	*/
				char* pch = NULL;
				char* pchXML = NULL;
				char* pchXMLStream = NULL;
				unsigned long ulAccumulatedBufferLen = 0;
				char filename[MAX_PATH];
				char lastfilename[MAX_PATH];
				int nDupes=0;

				// set up the buffer for Ack
				strcpy(buffer, "<mos><roAck/></mos>" );

				while(!pConn->m_bKillThread)  // kill the comm thread
				{
					pchBuffer = NULL;
					ulBufferLen = 0;

/*
_ftime( &timestamp );

char buffer[256];
tm* theTime = localtime( &timestamp.time	);
strftime(buffer, 30, "calling getline %d-%H:%M:%S.", theTime );
int nOffset = strlen(buffer);
sprintf(buffer+nOffset,"%03d\r\n",timestamp.millitm);

fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite(buffer, 1, strlen(buffer),fp);
	fflush(fp);
	fclose(fp);
}
*/

					char chError[NET_ERRORSTRING_LEN];
					if (pConn->m_bKillThread)
					{
						net.CloseConnection(s);
						pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
						pConn->m_bThreadStarted = false;  // state of comm thread
						if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
						{
							EnterCriticalSection(pConn->m_pcritSendMsg);
							_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Disconnecting %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
							pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:disconnect", szMsg);
							LeaveCriticalSection(pConn->m_pcritSendMsg);
						}
						_endthread();
						return;
					}
					int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, s, NET_RCV_ONCE, chError);
	
					
/*_ftime( &timestamp );

theTime = localtime( &timestamp.time	);
strftime(buffer, 30, "called getline  %d-%H:%M:%S.", theTime );
nOffset = strlen(buffer);
sprintf(buffer+nOffset,"%03d returned\r\n",timestamp.millitm);

fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite(buffer, 1, strlen(buffer),fp);
	fflush(fp);
	fclose(fp);
}
*/

						
					if(nReturnCode == NET_SUCCESS)
					{
						//process any received XML.


if((pConn->m_pszCommFile)&&(strlen(pConn->m_pszCommFile)))
{
	fp = fopen(pConn->m_pszCommFile, "at");
	if(fp)
	{
		fwrite(pchBuffer, 1, ulBufferLen,fp);
		fflush(fp);
		fclose(fp);
	}
}

						// have to keep accumulating until we find a </mos> tag.
						if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))
						{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("recd\r\n", 1, strlen("recd\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/


/*
	// debug the receipt
						_ftime( &timestamp );

						tm* theTime = localtime( &timestamp.time	);
						strftime(filename, 30, "logs\\received%d-%H%M%S", theTime );
						int nOffset = strlen(filename);
						sprintf(filename+nOffset,"%03d.txt",timestamp.millitm);

						fp = fopen(filename, "wb");
	//					fp = fopen("omnibus.log", "at");
						if(fp)
						{
							fwrite(pchBuffer, 1, ulBufferLen,fp);
							fflush(fp);
							fclose(fp);
						}
	// end debug
*/
						
							int nLen = 0;
							if(pchXMLStream) nLen = ulAccumulatedBufferLen;
							pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
							if(pch)
							{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("pch\r\n", 1, strlen("pch\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/								
								char* pchEnd = NULL;
								char* pchNext = NULL;
								if(pchXMLStream)  // we have an old buffer.
								{
									memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
									memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
									free(pchXMLStream); 
									pchXMLStream = pch;  // reassign!
									ulAccumulatedBufferLen += ulBufferLen;
									*(pch+ulAccumulatedBufferLen) = 0;  // null term
								}
								else
								{
									// this is new.
									// first we have to skip all chars that are not a '<'
									// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
									// used to use strchr but if there are leading zeros in the buffer, we never get past them

									pchEnd = pchBuffer;
									while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

									if(pchEnd<pchBuffer+ulBufferLen)
									{
										strcpy(pch, pchEnd);
										ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
										pchXMLStream = pch;  // reassign!
										*(pch+ulAccumulatedBufferLen) = 0;  // null term
									}
									else  // not found!
									{
										free(pch);
										pchXMLStream = NULL;
									}
								}

								if(pchBuffer) free(pchBuffer);
								pchBuffer = NULL;

/*fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("pchBuffer\r\n", 1, strlen("pchBuffer\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

								// now, pchXMLStream has everything so far.
/*
	if(pchXMLStream)
	{
		strftime(filename, 30, "logs\\debugaccumbuf%d-%H%M%S", theTime );
		int nOffset = strlen(filename);
		sprintf(filename+nOffset,"%03d.txt",timestamp.millitm);

		fp = fopen(filename, "wb");
//					fp = fopen("omnibus.log", "at");
		if(fp)
		{
			fwrite(pchXMLStream, 1, ulAccumulatedBufferLen,fp);
			fflush(fp);
			fclose(fp);
		}
	}
*/
								if(pchXMLStream)
								{

									pchEnd = strstr(pchXMLStream, "</mos>");
									while((pchEnd)&&(!pConn->m_bKillThread))
									{

/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("mos\r\n", 1, strlen("mos\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
									// found a token.
										pchEnd+=strlen("</mos>");

										pchNext = pchEnd;
										while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

										if(pchNext<pchEnd+strlen(pchEnd))
										{
											// we found a remainder.
											nLen = strlen(pchNext);
											pch = (char*) malloc(nLen+1);  //term 0
											if(pch)
											{
												memcpy(pch, pchNext, nLen);
												*(pch+nLen) = 0;
											}
										}
										else pch = NULL;


										pchXML = pchXMLStream; // just use it.
										*pchEnd = 0; //null terminate it

										pchXMLStream = pch;  // take the rest of the stream.
										if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
										else ulAccumulatedBufferLen=0;

										// now have to deal with XML found in pchXML.


										if(pConn->m_bUseRepsonse)
										{
											ulBufferLen = strlen(buffer)+1; //send the term 0
											n = net.SendLine((unsigned char*)buffer, ulBufferLen, s);//, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
										}

										//debug file write
										if(pConn->m_bWriteXMLtoFile)
										{
											strcpy(filename, "Logs");
											_mkdir(filename);  // if exists already np
											strcat(filename, "\\");
											strcat(filename, ((pConn->m_pszServerName!=NULL)&&(strlen(pConn->m_pszServerName)>0))? pConn->m_pszServerName : pConn->m_pszServerAddress);

											_mkdir(filename);  // if exists already np

											_ftime( &timestamp );

											tm* theTime = localtime( &timestamp.time	);

											char filenametemp[MAX_PATH];

											if(strstr(pchXML, "<heartbeat>"))
												strftime(filenametemp, MAX_PATH-1, "\\Heartbeat_Message_Day_%d-%H.%M.%S.", theTime );
											else
												strftime(filenametemp, MAX_PATH-1, "\\Output_Message_Day_%d-%H.%M.%S.", theTime );

											strcat(filename, filenametemp);

											int nOffset = strlen(filename);
											sprintf(filename+nOffset,"%03d",timestamp.millitm);

											if (strcmp(lastfilename, filename)==0)
											{
												nDupes++;
											}
											else
											{
												nDupes=0;
												strcpy(lastfilename, filename);
											}
											nOffset = strlen(filename);
											sprintf(filename+nOffset,"%02d.xml",nDupes);

											fp = fopen(filename, "wb");
											if(fp)
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												fwrite(pchXML, 1, strlen(pchXML),fp);
												fflush(fp);
												fclose(fp);
											}
		/*									else
											{
												fp = fopen("logs\\omnibus.log", "at");
											
												if(fp)
												{
													fwrite(pchXML, 1, strlen(pchXML),fp);
													fflush(fp);
													fclose(fp);
												}
											}
	*/
										}
									//// end debug file write

										// ok so, here is the XML, in pchXML

										// lets set the time.
										pConn->m_ulConnLastMessage = timestamp.time;

										// let's populate the channels and the event information,

										if(strstr(pchXML, "<heartbeat>"))
										{
											// heartbeat msg
											pchEnd = strstr(pchXML, "</heartbeat>");
											// find the list name:

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;
											bool bConnected = false;

											pchName = strstr(pchXML, "<currentTime>");
											if((pchName)&&(pchEnd)&&(pchName<pchEnd))
											{
												pchName += strlen("<currentTime>");
												pch = strstr(pchName, "</currentTime>");
												if((pch)&&(pch<pchEnd))
												{
													int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
													if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
													//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
													pConn->UpdateServerTime();
												}
											}
											// TODO: may want to change this later to not bother allocating unconnected lists.

											pchName = strstr(pchXML, "<channel>");
											while((pchName)&&(!pConn->m_bKillThread))
											{
												usListID = LIST_NOT_DEFINED;
												bConnected = false;
												pchEnd = strstr(pchName, "</channel>");
												if(pchEnd)  // we found a correctly encapsulted record.
												{
													pchNext = strstr(pchName, "<id>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<id>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<connected>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<connected>");
															bConnected = atoi(pchNext)==0?false:true;
														}
														pchNext = strstr(pchName, "<name>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<name>");
															pch = strstr(pchName, "</name>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else  strcpy(pchNext, ""); // blank name
														}
														else strcpy(pchNext, ""); // blank name

														// add the list!
														if(!pConn->m_bKillThread) 	
														{
EnterCriticalSection (&pConn->m_crit);
															pConn->UpdateList(usListID, pchNext, bConnected?OMNI_LIST_CONNECTED:OMNI_LIST_DISCONNECTED);
LeaveCriticalSection (&pConn->m_crit);
														}
													}

													pchName = strstr(pchEnd, "<channel>");  // find the next one.
												} // if(pchEnd)
											}//while ((pchName)&&(!pConn->m_bKillThread))
										}
										else
										{
											// output message.
											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;

											pchName = strstr(pchXML, "<roReplace>");
											if(pchName)
											{
												pchEnd = strstr(pchName, "</roReplace>");
												if(pchEnd)  // we found a correctly encapsulated record.
												{
													pchNext = strstr(pchName, "<roID>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<roID>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<roSlug>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<roSlug>");
															pch = strstr(pchName, "</roSlug>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else
															{
																pchNext = NULL; // blank name
																pch = pchName;
															}
														}
														else
														{
															pchNext = NULL; // blank name
															pch = pchName;
														}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "found %s in roReplace, about to update\r\n",pchNext);
		fflush(fp);
		fclose(fp);
	}
}

														// add or update the list!

														int nListIndex = HELIOS_ERROR;
														if(!pConn->m_bKillThread)
														{
//	EnterCriticalSection(&g_phelios->m_data.m_critChannels);
//															nListIndex = g_phelios->m_data.IsChannelActive(usListID);
//	LeaveCriticalSection(&g_phelios->m_data.m_critChannels);
														}

														if((nListIndex>HELIOS_ERROR)&&(!pConn->m_bKillThread))
														{
	EnterCriticalSection (&pConn->m_crit);
															pConn->UpdateList(usListID, pchNext?pchNext:"", OMNI_LIST_CONNECTED);
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "updated, checking for events\r\n");
		fflush(fp);
		fclose(fp);
	}
}

															if(!pConn->m_bKillThread)	nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
														}

														if((nListIndex>HELIOS_ERROR)&&(!pConn->m_bKillThread))
														{
															// then, deal with any events we might have.
															pchNext = strstr((pch+1), "<story>");

////////////////////////////////////////////////////////////////////////////////////
// deletion of all events
//
// we weren't able to just delete on a blank roReplaceMessage,
// as it turns out, the 2.8.16 Adaptor would send these messages
// when something on a track we were filtering changed.
// update: v2.8.18 fixes the blank message on filtered track change.

														

															if(pchNext == NULL) // we don't have any events.  
															// This seems to be only sent if the list is initially blank on connect, 
															// or if the live schedule is deleted.  Interestingly, no deletion events are sent in this case.
															{
																if(!pConn->m_bKillThread) 
																{
		EnterCriticalSection(&pConn->m_crit);
																	CCAList* pList = pConn->GetList(nListIndex);
																	if(pList)
																	{
																		if(!pConn->m_bKillThread)
																		{
																			nReturnCode = pList->DeleteAllEvents( pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																			if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																			{
																				fp = fopen(pConn->m_pszDebugFile, "at");
																				if(fp)
																				{
																					fprintf(fp, "DeleteAllEvents for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																					fflush(fp);
																					fclose(fp);
																				}
																			}
																		}
																		EnterCriticalSection(&pConn->m_critCounter);
																		if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																		pConn->m_ulCounter++;
																		LeaveCriticalSection(&pConn->m_critCounter);
																	}
	LeaveCriticalSection(&pConn->m_crit);
																}


															}

															while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
															{
																char* pchEndRec = pchNext+strlen("<story>");
																pchName = strstr(pchNext, "</story>");
																if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																{
																	pchEndRec = pchName;
																	// ok, now pchNext is the beginning, 
																	// and pchEnd is the end of the list,
																	// pchEndRec is the end of the record, and we can use pchName for the fields
																	// we are interested in getting:
																	
																	//unsigned short m_usType;              <type>      // internal colossus type
																	//char* m_pszID;												<clipId>		// the clip ID
																	//char* m_pszTitle;											<title>		  // the title
																	//char* m_pszData;  										<data> 			//must encode zero.
																	//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																	//unsigned char  m_ucSegment;														// not used

																	// following 2 compiled from <startTime>
																	//unsigned long  m_ulOnAirTimeMS;
																	//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																	//unsigned long  m_ulDurationMS;        <dur>        // duration
																	//unsigned short m_usStatus;						<status>     // status (omnibus code)
																	//unsigned short m_usControl;           <timeMode>   // time mode

																	// additionally, up at the list level, we can compile from
																	//  <currentTime>
																	//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																	//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																	//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																	// let's create a new event object;
																	CCAEvent* pEvent = new CCAEvent;
																	char* pchField;

																	pchName = strstr(pchNext, "<storyID>");
																	if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																	{
																		pchName += strlen("<storyID>");
																		pch = strstr(pchName, "</storyID>");
																		if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																		{
																			*pch = 0;  //null term the ID!
																			pchField = (char*) malloc(strlen(pchName)+1);
																			if(pchField)  // have to at least have this field
																			{
																				strcpy(pchField, pchName);
																				pEvent->m_pszReconcileKey = pchField;
																				*pch = '<'; // put it back so we can search whole string again.

																				pchName = strstr(pchNext, "<clipId>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<clipId>");
																					pch = strstr(pchName, "</clipId>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszID = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}


																				pchName = strstr(pchNext, "<title>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<title>");
																					pch = strstr(pchName, "</title>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszTitle = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


																				bool bFieldsFound = false;

																				if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																				{
																					// we found some fields!
																					bFieldsFound = true;	

																					for(int i=0; i<pConn->m_nNumFields; i++)
																					{

																						if(pConn->m_ppfi[i]->m_pszFieldName)
																						{
																							char pszTag[256];
																							sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
					//AfxMessageBox(pszTag);
																							pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																							if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																							{
																								sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																								pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																								if(pConn->m_ppfi[i]->m_pchEndTag)
																								{
																									pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																								}
																								if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																								{
																									int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																									pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																									{
																										memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																										memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																									}
																								}
																							}
																						}
																					}


																					//we have to check validity of fields (top-level-ness)
																					bool bAllTopLevel = false;
																					
																					while(!bAllTopLevel)
																					{
																						bool bOverlapFound = false;
																						i=0; 
																						while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																						{
																							for(int j=0; j<pConn->m_nNumFields-1; j++)
																							{
																								if(j!=i)
																								{
																									if(
																											(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																										&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																										)
																									{
																										// i is inside j, need to look for a different one and start over.
																										bOverlapFound = true;

																										char pszTag[256];
																										sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																										pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																										if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																										{
																											sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																											if(pConn->m_ppfi[i]->m_pchEndTag)
																											{
																												pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																											}
																											if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																											{
																												int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																												pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																												if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																												{
																													memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																													memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																												}
																											}
																										}
																										else
																										{
																											if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																											pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																										}
																										break;
																									}

																								}
																							}
																							
																							i++;
																						}

																						if(!bOverlapFound) bAllTopLevel = true;
																					}

																					//assemble the data string
																					int nDataLen = 0;
																					for(i=0; i<pConn->m_nNumFields; i++)
																					{
																						if(pConn->m_ppfi[i]->m_pszFieldInfo)
																						{
																							nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
	//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																						}
																					}
																					
																					if(nDataLen)
																					{
																						pchField = (char*) malloc(nDataLen+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, "");
																							for(i=0; i<pConn->m_nNumFields; i++)
																							{
																								if(pConn->m_ppfi[i]->m_pszFieldInfo)
																								{
																									strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																									free(pConn->m_ppfi[i]->m_pszFieldInfo);
																									pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																								}
																							}
																							pEvent->m_pszData = pchField;
	//																						AfxMessageBox(pEvent->m_pszData );
																						}
																					}

																				}


																				if(!bFieldsFound)
																				{
																					// use default, minus tags
																					pchName = strstr(pchNext, "<data>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<data>");
																						pch = strstr(pchName, "</data>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszData = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}
																				}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																				pchName = strstr(pchNext, "<startTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<startTime>");
																					pch = strstr(pchName, "</startTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_dblOnAirTimeInternal = atof(pchName);
																						int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																						else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																					}
																				}

																				pchName = strstr(pchNext, "<currentTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<currentTime>");
																					pch = strstr(pchName, "</currentTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																						//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																						pConn->UpdateServerTime();
																					}
																				}

																				pchName = strstr(pchNext, "<type>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<type>");
																					pch = strstr(pchName, "</type>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usType = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<dur>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<dur>");
																					pch = strstr(pchName, "</dur>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulDurationMS = (unsigned long) nMS;
																						else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																					}
																				}

																				pchName = strstr(pchNext, "<status>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<status>");
																					pch = strstr(pchName, "</status>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usStatus = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<timeMode>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<timeMode>");
																					pch = strstr(pchName, "</timeMode>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usControl = (unsigned short)atoi(pchName);
																					}
																				}

	//																			Ok, lets update the event!
																				if(nListIndex>=0)
																				{
																					if(!pConn->m_bKillThread) 
																					{
		EnterCriticalSection(&pConn->m_crit);
																						CCAList* pList = pConn->GetList(nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread) 
																							{

																								double dblTime = ((double)timestamp.millitm/1000.0);
																								if(pConn->m_bUseUTC)
																								{
																									dblTime += (double)(timestamp.time); // UTC
																								}
																								else
																								{
																									dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																								}

																								if(pConn->m_bUseDST)
																								{
																									dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																								}

																								pEvent->m_dblUpdated = dblTime;  // UTC 
																								 
																								nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);
																								
																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent (roReplace) %s for channel %d returned with %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}
																							}
																						}
																	EnterCriticalSection(&pConn->m_critCounter);
																					if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																					pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
	LeaveCriticalSection(&pConn->m_crit);
																					}
																				}
																			}
																		}
																	}
																	//else // cant do anything with this event, so just have to skip.
																}

																pchNext = strstr(pchEndRec, "<story>");
															}
														}
													}
												}
											}
											else  // not roReplace
											{
												pchName = strstr(pchXML, "<roStoryReplace>");
												if(pchName)
												{
													// it's a replace single item record
													pchEnd = strstr(pchName, "</roStoryReplace>");
													if(pchEnd)  // we found a correctly encapsulated record.
													{
														pchNext = strstr(pchName, "<roID>");
														if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
														{
															pchNext += strlen("<roID>");
															usListID = atoi(pchNext);

															
															int nListIndex = HELIOS_ERROR;
															if(!pConn->m_bKillThread)
															{
//	EnterCriticalSection(&g_phelios->m_data.m_critChannels);
																nListIndex = g_phelios->m_data.IsChannelActive(usListID);
//	LeaveCriticalSection(&g_phelios->m_data.m_critChannels);
															}
															if((nListIndex>HELIOS_ERROR)&&(!pConn->m_bKillThread))
															{ 
	EnterCriticalSection (&pConn->m_crit);
																nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
															}

															if((nListIndex>HELIOS_ERROR)&&(!pConn->m_bKillThread))
															{

																// then, deal with the events we have.
																pchNext = strstr(pchName, "<story>");
																while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))  // this while should only get hit once.
																{
																	pchName = strstr(pchNext, "</story>");
																	if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																	{
																		char* pchEndRec = pchName;
																		// ok, now pchNext is the beginning, 
																		// and pchEnd is the end of the list,
																		// pchEndRec is the end of the record, and we can use pchName for the fields
																		// we are interested in getting:
																		
																		//unsigned short m_usType;              <type>      // internal colossus type
																		//char* m_pszID;												<clipId>		// the clip ID
																		//char* m_pszTitle;											<title>		  // the title
																		//char* m_pszData;  										<data> 			//must encode zero.
																		//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																		//unsigned char  m_ucSegment;														// not used

																		// following 2 compiled from <startTime>
																		//unsigned long  m_ulOnAirTimeMS;
																		//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																		//unsigned long  m_ulDurationMS;        <dur>        // duration
																		//unsigned short m_usStatus;						<status>     // status (omnibus code)
																		//unsigned short m_usControl;           <timeMode>   // time mode

																		// additionally, up at the list level, we can compile from
																		//  <currentTime>
																		//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																		//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																		//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																		// let's create a new event object;
																		CCAEvent* pEvent = new CCAEvent;
																		char* pchField;

																		pchName = strstr(pchNext, "<storyID>");
																		if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																		{
																			pchName += strlen("<storyID>");
																			pch = strstr(pchName, "</storyID>");
																			if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																			{
																				*pch = 0;  //null term the ID!
																				pchField = (char*) malloc(strlen(pchName)+1);
																				if(pchField)  // have to at least have this field
																				{
																					strcpy(pchField, pchName);
																					pEvent->m_pszReconcileKey = pchField;
																					*pch = '<'; // put it back so we can search whole string again.

																					pchName = strstr(pchNext, "<clipId>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<clipId>");
																						pch = strstr(pchName, "</clipId>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszID = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}


																					pchName = strstr(pchNext, "<title>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<title>");
																						pch = strstr(pchName, "</title>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszTitle = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


																					bool bFieldsFound = false;

																					if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																					{
																						// we found some fields!
																						bFieldsFound = true;	

																						for(int i=0; i<pConn->m_nNumFields; i++)
																						{

																							if(pConn->m_ppfi[i]->m_pszFieldName)
																							{
																								char pszTag[256];
																								sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
						//AfxMessageBox(pszTag);
																								pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																								if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																								{
																									sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																									pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																									if(pConn->m_ppfi[i]->m_pchEndTag)
																									{
																										pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																									}
																									if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																									{
																										int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																										pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																										if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																										{
																											memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																											memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																										}
																									}
																								}
																							}
																						}


																						//we have to check validity of fields (top-level-ness)
																						bool bAllTopLevel = false;
																						
																						while(!bAllTopLevel)
																						{
																							bool bOverlapFound = false;
																							i=0; 
																							while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																							{
																								for(int j=0; j<pConn->m_nNumFields-1; j++)
																								{
																									if(j!=i)
																									{
																										if(
																												(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																											&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																											)
																										{
																											// i is inside j, need to look for a different one and start over.
																											bOverlapFound = true;

																											char pszTag[256];
																											sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																											if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																											{
																												sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																												pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																												if(pConn->m_ppfi[i]->m_pchEndTag)
																												{
																													pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																												}
																												if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																												{
																													int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																													if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																													pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																													if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																													{
																														memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																														memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																													}
																												}
																											}
																											else
																											{
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																												pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																											}
																											break;
																										}

																									}
																								}
																								
																								i++;
																							}

																							if(!bOverlapFound) bAllTopLevel = true;
																						}

																						//assemble the data string
																						int nDataLen = 0;
																						for(i=0; i<pConn->m_nNumFields; i++)
																						{
																							if(pConn->m_ppfi[i]->m_pszFieldInfo)
																							{
																								nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
		//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																							}
																						}
																						
																						if(nDataLen)
																						{
																							pchField = (char*) malloc(nDataLen+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, "");
																								for(i=0; i<pConn->m_nNumFields; i++)
																								{
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)
																									{
																										strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																										free(pConn->m_ppfi[i]->m_pszFieldInfo);
																										pConn->m_ppfi[i]->m_pszFieldInfo = NULL;

																									}
																								}
																								pEvent->m_pszData = pchField;
		//																						AfxMessageBox(pEvent->m_pszData );
																							}
																						}

																					}


																					if(!bFieldsFound)
																					{
																						// use default, minus tags
																						pchName = strstr(pchNext, "<data>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<data>");
																							pch = strstr(pchName, "</data>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
																								*pch = 0;  //null term the ID!
																								pchField = (char*) malloc(strlen(pchName)+1);
																								if(pchField)  // have to at least have this field
																								{
																									strcpy(pchField, pchName);
																									pEvent->m_pszData = pchField;
																									*pch = '<'; // put it back so we can search whole string again.
																								}
																							}
																						}
																					}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																					pchName = strstr(pchNext, "<startTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<startTime>");
																						pch = strstr(pchName, "</startTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);
																							int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																							else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																						}
																					}

																					pchName = strstr(pchNext, "<currentTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<currentTime>");
																						pch = strstr(pchName, "</currentTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																							//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																							pConn->UpdateServerTime();
																						}
																					}

																					pchName = strstr(pchNext, "<type>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<type>");
																						pch = strstr(pchName, "</type>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usType = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<dur>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<dur>");
																						pch = strstr(pchName, "</dur>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pEvent->m_ulDurationMS = (unsigned long) nMS;
																							else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																						}
																					}

																					pchName = strstr(pchNext, "<status>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<status>");
																						pch = strstr(pchName, "</status>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usStatus = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<timeMode>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<timeMode>");
																						pch = strstr(pchName, "</timeMode>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usControl = (unsigned short)atoi(pchName);
																						}
																					}

		//																			Ok, lets update the event!
																					if(nListIndex>=0)
																					{
																						if(!pConn->m_bKillThread) 
																						{
		EnterCriticalSection(&pConn->m_crit);
																							CCAList* pList = pConn->GetList(nListIndex);
																							if(pList)
																							{
																								if(!pConn->m_bKillThread) 
																								{
																									double dblTime = ((double)timestamp.millitm/1000.0);
																									if(pConn->m_bUseUTC)
																									{
																										dblTime += (double)(timestamp.time); // UTC
																									}
																									else
																									{
																										dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																									}

																									if(pConn->m_bUseDST)
																									{
																										dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																									}
																									pEvent->m_dblUpdated = dblTime;
																									nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d UpdateEvent (roStoryReplace) %s for channel %d returned with %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																											fflush(fp);
																											fclose(fp);
																										}
																									}
																								}
																							}
																	EnterCriticalSection(&pConn->m_critCounter);
																							if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																							pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
	LeaveCriticalSection(&pConn->m_crit);
																						}
																					}
																				}
																			}
																		}
																		//else // cant do anything with this event, so just have to skip.
																	}

																	pchNext = strstr(pchEnd, "<story>");
																}
															}
														} //if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
													}
												} 
												else
												if(!pConn->m_bKillThread)
												{
													pchName = strstr(pchXML, "<roStoryDelete>");
													if(pchName)
													{
															// it's a delete record
														pchEnd = strstr(pchName, "</roStoryDelete>");
														if(pchEnd)  // we found a correctly encapsulated record.
														{
															pchNext = strstr(pchName, "<roID>");
															if((pchNext)&&(pchNext<pchEnd))
															{
																pchNext += strlen("<roID>");
																usListID = atoi(pchNext);

																int nListIndex = HELIOS_ERROR;
																if(!pConn->m_bKillThread)
																{
//	EnterCriticalSection(&g_phelios->m_data.m_critChannels);
																	nListIndex = g_phelios->m_data.IsChannelActive(usListID);
//	LeaveCriticalSection(&g_phelios->m_data.m_critChannels);
																}
																if((nListIndex>HELIOS_ERROR)&&(!pConn->m_bKillThread))
																{ 
	EnterCriticalSection (&pConn->m_crit);
																	nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
																}

																if((nListIndex>HELIOS_ERROR)&&(!pConn->m_bKillThread))
																{
																	//pConn->m_ppList[i]->UpdateEvent(pEvent);

																	// then, find the story ID to delete
																	pchNext = strstr(pchName, "<storyID>");
																	if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
																	{
																		pchNext += strlen("<storyID>");
																		pch = strstr(pchNext, "</storyID>");
																		if((pch)&&(pch<pchEnd))
																		{
																			*pch = 0;  //null term the ID!
																			if(!pConn->m_bKillThread) 
																			{
EnterCriticalSection(&pConn->m_crit);
																				CCAList* pList = pConn->GetList(nListIndex);
																				if(pList)
																				{
																					int j = pList->GetEventIndex(pchNext);
																					if(j>=0)
																					{
																						if(!pConn->m_bKillThread)
																						{
																							nReturnCode = pList->DeleteEvent(j, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																									//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																									fprintf(fp, "DeleteEvent (roStoryDelete) for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						}
																		EnterCriticalSection(&pConn->m_critCounter);
																						if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																						pConn->m_ulCounter++;
																		LeaveCriticalSection(&pConn->m_critCounter);
																					}
																				}
LeaveCriticalSection(&pConn->m_crit);
																			}
																		}
																	}
																}
															}
														}//if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))


													}  // else its an unrecognized mos command, or time to die
												}
											}
										}


										free(pchXML);
										pchXML = NULL;

										if (pchXMLStream) 
											pchEnd = strstr(pchXMLStream, "</mos>");
										else pchEnd = NULL;
									}  // while </mos> exists.
								}//if(pchXMLStream)
								// dont do a free(pch);  
							}  // else out of mem, so just skip
							else
							{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "**** Couldn't allocate buffer!\r\n");
		fflush(fp);
		fclose(fp);
	}
}
							}
						} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("out\r\n", 1, strlen("out\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
					} // if getline succeeds.
					else
					{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "**** GetLineError %d: %s\r\n", nReturnCode, chError);
		fflush(fp);
		fclose(fp);

	}
}

						// here, we either have timed out because theres no data, or, the connection has been lost.
						// if we lost the conn, need to re-establish.
						if(nReturnCode == NET_ERROR_CONN)		// connection lost
						{
							pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
							pConn->m_ulFlags |= OMNI_SOCKETERROR;
							ulRetry++;
							if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
							{
								EnterCriticalSection(pConn->m_pcritSendMsg);
								_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Lost connection to %s:%d.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
								pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:error", szMsg);
								LeaveCriticalSection(pConn->m_pcritSendMsg);
							}
							break;  // break out of while loop, closes connection, starts again
						}
						else
						{
							// we prob just havent gotten a message because no data.  
							// so, use the time to purge old events if it is time to do so.
							if(pConn->m_ulCheckInterval>0)	// number of seconds between checks (0 turns off)
							{
								_timeb timestamp;
								_ftime(&timestamp);
								if((pConn->m_ulLastPurge+pConn->m_ulCheckInterval)<(unsigned long)timestamp.time)
								{
									// it's time!
									if((pConn->m_ulRefUnixTime>0)&&(pConn->m_ulRefUnixTime!=TIME_NOT_DEFINED)&&(pConn->m_ulRefUnixTime>pConn->m_ulExpiryPeriod)) // need a valid expiry time
									{
										pConn->m_ulLastPurge = (unsigned long)timestamp.time;
										int l=0;
										while((l<pConn->GetListCount())&&(!pConn->m_bKillThread))
										{
											CCAList* pList = pConn->GetList(l);
											int nCount = pList->GetEventCount();
											if((pList)&&(nCount>0))
											{
												int e=0;
												unsigned long ulExpiry = pConn->m_ulRefUnixTime-pConn->m_ulExpiryPeriod;
												while((e<nCount)&&(!pConn->m_bKillThread))
												{
													CCAEvent* pEvent = pList->GetEvent(e);
													if(
															(pEvent)
														&&(((unsigned long)pEvent->m_dblOnAirTime+(pEvent->m_ulDurationMS/1000))<ulExpiry)
														)
													{
														// purge!
														if(!pConn->m_bKillThread)
														{
															nReturnCode = pList->DeleteEvent(e, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
															if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
															{
																fp = fopen(pConn->m_pszDebugFile, "at");
																if(fp)
																{
																	// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																	//fprintf(fp, "DeleteEvent (Expiry Purge) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																	fprintf(fp, "DeleteEvent (Expiry Purge) for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																	fflush(fp);
																	fclose(fp);
																}
															}
															if(nReturnCode<OMNI_SUCCESS)
															{
																e++;  // just increment
															}
														}
													}
													else
													{
														e++;
													}
												}
											}
											l++;
										}
									}
								}
							}
						}
					} // end of else from if(nReturnCode == NET_SUCCESS)

					// and lets check if we've timed out on the connection, no matter what the success was
					_ftime( &timestamp );
					if((pConn->m_ulConnLastMessage + pConn->m_ulConnTimeout) < (unsigned long)timestamp.time )
					{
						// we timed out, need to disconnect and reconnect.
						pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
						ulRetry++;
						if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
						{
							EnterCriticalSection(pConn->m_pcritSendMsg);
							_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Connection to %s:%d timed out.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
							pConn->m_lpfnSendMsg(OMNI_SENDMSG_INFO, "AdaptorComm:timeout", szMsg);
							LeaveCriticalSection(pConn->m_pcritSendMsg);
						}
						break;  // break out of while loop, closes connection, starts again
					}

					if(pchBuffer) free(pchBuffer);

					if(!pConn->m_bKillThread) Sleep(1);  // dont peg processor
	/*
					fp = fopen("omnibus.log", "at");
					if(fp)
					{
						fprintf(fp, "here %d\r\n", timestamp.time);
						fflush(fp);
						fclose(fp);
					}
	*/
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("while\r\n", 1, strlen("while\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/

				} //while(!pConn->m_bKillThread)  // kill the comm thread
				// if we are here we may have broken out because we had an error.

				net.CloseConnection(s);
			}  // else couldnt open connection
			else
			{
				pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
				pConn->m_ulFlags |= OMNI_SOCKETERROR;
				ulRetry++;
				if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg))
				{
					EnterCriticalSection(pConn->m_pcritSendMsg);
					_snprintf(szMsg, DB_SQLSTRING_MAXLEN, "Connection to %s:%d failed.\r\n", pConn->m_pszServerAddress, pConn->m_usPort);
					pConn->m_lpfnSendMsg(OMNI_SENDMSG_ERROR, "AdaptorComm:connect", szMsg);
					LeaveCriticalSection(pConn->m_pcritSendMsg);
				}
			}

			_ftime(&timestamp);
			unsigned long ulExpiry = (unsigned long)timestamp.time + pConn->m_ulConnectionInterval;
			while((!pConn->m_bKillThread)&&((unsigned long)timestamp.time<ulExpiry)) // make this interruptible sleep
			{
				_ftime(&timestamp);

				Sleep(10); 
			}

		}  // while retrying



		if((pConn->m_pdb)&&(pConn->m_pdbConn)&&(pConn->m_pdbConn->m_bConnected))
		{
			char szSQL[DB_SQLSTRING_MAXLEN];
			if((pConn->m_pszExchangeTable)&&(strlen(pConn->m_pszExchangeTable)))
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = 'Server_Time' AND flag LIKE '%s:%05d%%'",
					pConn->m_pszExchangeTable,
					pConn->m_pszServerAddress,
					pConn->m_usPort
					);
if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL)<DB_SUCCESS)
				{
					// error.
				}
if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
			}
		}

		pConn->m_ulFlags &= ~OMNI_SOCKETCONNECTED;
		pConn->m_bThreadStarted = false;  // state of comm thread
	}
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fwrite("bye\r\n", 1, strlen("bye\r\n"),fp);
	fflush(fp);
	fclose(fp);
}
*/
	_endthread();
}

#endif // ! HELIOS_NEW_DB_SCHEMA


void HeliosAdaptorQueueThread(void* pvArgs)
{
	if(pvArgs==NULL) return;
	CCAConn* pConn = (CCAConn*) pvArgs;
	// 2.1.1.16 added asynchronous database access, to take it out of critical path of the TCP communication.  Omnibus times out on large lists, have to receive as fast as possible.

	pConn->m_bXMLQueueStarted=true;

	char* pchXML; 

	while(!pConn->m_bXMLQueueKill)
	{

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	if(pConn->pAux==NULL)
	{
		_timeb timestamp;
		_ftime(&timestamp);
		char filename[256];
		sprintf(filename, "Logs\\omni_process-%s.txt", pConn->m_pszServerAddress);

		EnterCriticalSection(&pConn->m_critAux);
		FILE* fp = fopen(filename, "ab");
		if(fp)
		{
			fprintf(fp, "[%s> %d.%03d PROCESS START\r\n", pConn->m_pszServerAddress, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
		pConn->pAux = (void*)fp;
		LeaveCriticalSection(&pConn->m_critAux);
	}
}
else
{
	if(pConn->pAux!=NULL)
	{
		EnterCriticalSection(&pConn->m_critAux);
		fclose((FILE*) pConn->pAux);
		pConn->pAux = NULL;
		LeaveCriticalSection(&pConn->m_critAux);
	}
}

//	char**			m_ppchXMLQueue; // queue up XML items.
//	int				  m_nXMLQueue;
		EnterCriticalSection(&pConn->m_critXMLQueue);

		if(pConn->m_nXMLQueue>0)
		{
			pchXML = pConn->ReturnMessage();
			LeaveCriticalSection(&pConn->m_critXMLQueue);

			if(pchXML)
			{
				// process this message.

//				AfxMessageBox(pchXML);

				if(pConn->m_ulFlags&OMNI_MULTITHREAD) // 2.1.1.17 added multi thread
				{
					// multi threaded process, blocking, DO NOT delete object at end, other thread will do.
					PreProcessXML(pConn, pchXML);// 2.1.1.17  dispatches to channel threads
				}
				else
				{
					// single threaded process, blocking, delete object at end
					ProcessXML(pConn, pchXML); // 2.1.1.16
					delete [] pchXML;
				}
			}
		}
		else
		{
			LeaveCriticalSection(&pConn->m_critXMLQueue);
		}

		Sleep(1);

////////////// async database check 2.1.1.18
// here we want to interleave a check of the list, to see if there are any events that need to be committed to the database.
// if so, we only do one, we will interleave them with the items in roReplace.
// we only do this in roReplace since we are expecting many events, could be in the thousands, that take up processing time

		if(!(pConn->m_ulFlags&OMNI_MULTITHREAD)) // 2.1.1.18 if multi, we do this right in the channel queue thread
		{
			pConn->CheckAllListsForUncommitted(&pConn->m_bXMLQueueKill);

		}
////////////// async database check
	}


	// kill all the channel queues.
	pConn->KillChannelThreads();

	if(pConn->pAux!=NULL)
	{
		fclose((FILE*) pConn->pAux);
		pConn->pAux = NULL;
	}

	pConn->m_bXMLQueueStarted=false;

}

void HeliosAdaptorChannelThread(void* pvArgs)
{
	if(pvArgs==NULL) return;
	CCAList* pList = (CCAList*) pvArgs;
	CCAConn* pConn = (CCAConn*)(pList->m_pConn);

	_timeb timestamp;


	pList->m_bXMLQueueStarted=true;

	char* pchXML; 

	while(!pList->m_bXMLQueueKill)
	{
//	char**			m_ppchXMLQueue; // queue up XML items.
//	int				  m_nXMLQueue;
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:X>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

		EnterCriticalSection(&pList->m_critXMLQueue);
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:X-\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

		if(pList->m_nXMLQueue>0)
		{
			pchXML = pList->ReturnMessage();
			LeaveCriticalSection(&pList->m_critXMLQueue);

			if(pchXML)
			{
				// process this message.

//				AfxMessageBox(pchXML);
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:P>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

				if((!pList->m_bXMLQueueKill)&&(pList->m_ulFlags&OMNI_FLAGS_ACTIVE))				
				{
					ProcessChannelXML(pList, pchXML);
//				delete [] pchXML;  // done inside call.
				}
				else
				{
					delete [] pchXML; 
				}
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:P<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

			}
		}
		else
		{
			LeaveCriticalSection(&pList->m_critXMLQueue);
		}
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:X< %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, pList->m_nXMLQueue);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

		Sleep(1);

//		if(pConn->m_ulFlags&OMNI_MULTITHREAD) // 2.1.1.18 if multi, we do this right in the channel queue thread, but no need to check value, only gets here if multi-thread
		if(pList->m_ulFlags&OMNI_LIST_UNCOMMITTED_EXISTS)
		{
//			_timeb timestamp;
			_ftime(&timestamp);


if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:DBT> %d.%d + %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, pList->m_tmbLastDatabaseCheck.time, pList->m_tmbLastDatabaseCheck.millitm, pConn->m_nAsyncDatabaseCheckIntervalMS);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

			if(
					(pList->m_bXMLQueueKill == false)
					&&
					(!(
						(pList->m_tmbLastDatabaseCheck.time + (pConn->m_nAsyncDatabaseCheckIntervalMS/1000) > timestamp.time )
					||(
							( (((double)(pList->m_tmbLastDatabaseCheck.time))*1000.0) + ((double)(pConn->m_nAsyncDatabaseCheckIntervalMS + pList->m_tmbLastDatabaseCheck.millitm)) ) >
							( (((double)(timestamp.time))*1000.0) + ((double)(timestamp.millitm)) ) 
						)
					) )
				)
			{
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:DBX>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	
				int nDB = pList->CheckDatabaseEvents();// start at last known index, do whole list
if((pConn)&&(pConn->m_ulFlags&OMNI_DEBUG_PROCESS))
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d HACT:DBX< %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, nDB);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}				}	
		}

	}

	pList->m_bXMLQueueStarted=false;

}

void PreProcessXML(CCAConn* pConn, char* pchXML)
{
	if((pConn==NULL)||(pchXML==NULL)) return;

	_timeb timestamp;
	FILE* fp;
	CNetUtil	net;  // the connection exclusive networking obj
	char szMsg[DB_ERRORSTRING_LEN];
//	char szSQL[DB_SQLSTRING_MAXLEN];
	char* pchEnd = NULL;
	char* pchNext = NULL;
	char* pch = NULL;
//	int nReturnCode;
	unsigned long ulCounter = 0;  //local counter, we're going to parse the whole XML first, THEN update the counter.
	// otherwise, we are hammering the database with requests, if it is a large XML packet, best to do it all at once.


										// let's set the time.
										_ftime( &timestamp );

										pConn->m_ulConnLastMessage = timestamp.time;

										// let's populate the channels and the event information,

	////////////////////////////////////////////////////////////////////////////////////
	// iTX does not send server time, except in heartbeat.  But it is not guaranteed to ever get a hearbeat message.
	// we need servertime, so if allowed, grab the system time here, on receipt of any message.
  // we leave this as a global setting so that you can use it with Colossus too.

										if(pConn->m_ulFlags&OMNI_SYSTIME)
										{
	EnterCriticalSection(&pConn->m_critOperations);
											pConn->UpdateServerTime(true);  // sets vars.
	LeaveCriticalSection(&pConn->m_critOperations);
										}

										if(strstr(pchXML, "<heartbeat>"))
										{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d A heartbeat message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}
											// heartbeat msg
											pchEnd = strstr(pchXML, "</heartbeat>");
											// find the list name:

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;
											bool bConnected = false;

											if(!(pConn->m_ulFlags&OMNI_SYSTIME))
											{
												pchName = strstr(pchXML, "<currentTime>");
												if((pchName)&&(pchEnd)&&(pchName<pchEnd))
												{
													pchName += strlen("<currentTime>");
													pch = strstr(pchName, "</currentTime>");
													if((pch)&&(pch<pchEnd))
													{
														
	EnterCriticalSection(&pConn->m_critOperations);
														int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
														if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
														//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
														pConn->UpdateServerTime(false);
	LeaveCriticalSection(&pConn->m_critOperations);

													}
												}
											}
											// TODO: may want to change this later to not bother allocating unconnected lists.

											pchName = strstr(pchXML, "<channel>");
											while((pchName)&&(!pConn->m_bKillThread))
											{
												usListID = LIST_NOT_DEFINED;
												bConnected = false;
												pchEnd = strstr(pchName, "</channel>");
												if(pchEnd)  // we found a correctly encapsulated record.
												{
													pchNext = strstr(pchName, "<id>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<id>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<connected>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<connected>");
															bConnected = atoi(pchNext)==0?false:true;
														}
														pchNext = strstr(pchName, "<name>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<name>");
															pch = strstr(pchName, "</name>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else  strcpy(pchNext, ""); // blank name
														}
														else strcpy(pchNext, ""); // blank name

														// add the list!
if(!pConn->m_bKillThread)
{
 	EnterCriticalSection (&pConn->m_crit);
														pConn->UpdateList(usListID, pchNext, bConnected?OMNI_LIST_CONNECTED:OMNI_LIST_DISCONNECTED);
														int nListIndex = pConn->ReturnListIndex(usListID);
														if(nListIndex>=0)
														{
															CCAList* pList = pConn->GetList(nListIndex);
															if(pList)
															{
																pList->UpdateListStatus();
															}
														}
	LeaveCriticalSection (&pConn->m_crit);
}
													}

													pchName = strstr(pchEnd, "<channel>");  // find the next one.
												} // if(pchEnd)
											}//while ((pchName)&&(!pConn->m_bKillThread))

// this was a heartbeat, just free it here.
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d free [%d]\r\n", clock(), pchXML);
		fflush(fp);
		fclose(fp);

	}
}

					delete [] pchXML;
					pchXML = NULL;

										

										EnterCriticalSection(&pConn->m_critCounter);
														if(ULONG_MAX - pConn->m_ulCounter < ulCounter )
														{
															ulCounter -= (ULONG_MAX - pConn->m_ulCounter);
															pConn->m_ulCounter = ulCounter;
														}
														else	pConn->m_ulCounter += ulCounter;
										LeaveCriticalSection(&pConn->m_critCounter);


										}
										else  // this was an output message
										{
											bool bProcessedTime = false;
											bool bAdded = false;

											// let's identify which channel it might be for.
											// look for <roID>
											unsigned short usListID = LIST_NOT_DEFINED;
//											char* pchName = NULL;

											pchEnd = pchXML+ strlen(pchXML);
											pchNext = strstr(pchXML, "<roID>");
											if((pchNext)&&(pchNext<pchEnd))
											{
												pchNext += strlen("<roID>");
												usListID = atoi(pchNext);
												// have it now.  add the XML to the channel-specific message queue.

												pch = NULL;
												pchNext = strstr(pchXML, "<roSlug>");
												if((pchNext)&&(pchNext<pchEnd))
												{
													pchNext += strlen("<roSlug>");
													pch = strstr(pchNext, "</roSlug>");
													if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
													else
													{
														pchNext = NULL; // blank name
														pch = NULL;
													}
												}
												else
												{
													pchNext = NULL; // blank name
													pch = NULL;
												}

												// Get the list index
 	EnterCriticalSection (&pConn->m_crit);
												pConn->UpdateList(usListID, pchNext, OMNI_LIST_CONNECTED);
												if(pch) *pch = '<'; // replace this so it's there for the channel to parse
												int nListIndex = pConn->ReturnListIndex(usListID);
												if(nListIndex>=0)
												{
													CCAList* pList = pConn->GetList(nListIndex);

													if(pList)
													{
														bool bActive = true;

														if(g_actives.m_bUseActive)
														{
															EnterCriticalSection(&g_actives.m_critActive);
															bActive = (g_actives.IsChannelActive(pList->m_usListID)>=OMNI_SUCCESS);
															LeaveCriticalSection(&g_actives.m_critActive);
														}
														if((bActive)&&(!pList->m_bXMLQueueStarted)&&(!pConn->m_bKillThread)&&(!pConn->m_bXMLQueueKill))
														{
															EnterCriticalSection(&pList->m_critXMLQueue);
															pList->m_bXMLQueueKill=false;
															pList->m_ulFlags |= OMNI_FLAGS_ACTIVE;

															if(_beginthread(HeliosAdaptorChannelThread, 0, (void*)pList) == -1)
															{
																if((pConn)&&(pConn->m_lpfnSendMsg)&&(pConn->m_pcritSendMsg)/*&&(nRetry<1)*/) // always report
																{
																	EnterCriticalSection(pConn->m_pcritSendMsg);
																	_snprintf(szMsg, DB_ERRORSTRING_LEN, "Failed to start queue for stream %d.\r\n", usListID);
																	pConn->m_lpfnSendMsg(OMNI_SENDMSG_ERROR, "AdaptorComm:start_channel_queue", szMsg);
																	LeaveCriticalSection(pConn->m_pcritSendMsg);
																}
																LeaveCriticalSection(&pList->m_critXMLQueue);
															}
															else
															{

																LeaveCriticalSection(&pList->m_critXMLQueue);

																while((!pList->m_bXMLQueueStarted)&&(!pConn->m_bKillThread)&&(!pConn->m_bXMLQueueKill))
																{
																	Sleep(1);
																}
															}
														}

					EnterCriticalSection(&pList->m_critXMLQueue);
														pList->AddMessage(pchXML);
					LeaveCriticalSection(&pList->m_critXMLQueue);
														bAdded=true;

													}
												}
	LeaveCriticalSection (&pConn->m_crit);
												
											}

											if(!bAdded)
											{
												// have to delete it.

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d free [%d]\r\n", clock(), pchXML);
		fflush(fp);
		fclose(fp);

	}
}

					delete [] pchXML;
					pchXML = NULL;

											}
										}


}

void ProcessChannelXML(CCAList* pList, char* pchXML)
{
	if((pList==NULL)||(pchXML==NULL)) return;

	CCAConn* pConn = (CCAConn*)(pList->m_pConn);
	while((pConn==NULL)&&(!pConn->m_bKillThread))
	{
		Sleep(1);
	}

	CCAConn connUtil;
	connUtil.m_ulFlags = pConn->m_ulFlags;

	_timeb timestamp;
	FILE* fp;
	CNetUtil	net;  // the connection exclusive networking obj
//	char szMsg[DB_ERRORSTRING_LEN];
//	char szSQL[DB_SQLSTRING_MAXLEN];
	char* pchEnd = NULL;
	char* pchNext = NULL;
	char* pch = NULL;
	int nReturnCode;
	unsigned long ulCounter = 0;  //local counter, we're going to parse the whole XML first, THEN update the counter.
	// otherwise, we are hammering the database with requests, if it is a large XML packet, best to do it all at once.

	bool bInitConnection = false;

	if(pConn->m_ulFlags&OMNI_COMPAREONCONNECT)
	{

		if(*pchXML == 0x01)
		{
			*pchXML = '<';
			bInitConnection = true;
		}

		if(*pchXML == 0x02)
		{
			*pchXML = '%';
			bInitConnection = true;
		}
	}

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d XML %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, bInitConnection);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}



										// let's set the time.
										_ftime( &timestamp );

/*
										pConn->m_ulConnLastMessage = timestamp.time;

										// let's populate the channels and the event information,

	////////////////////////////////////////////////////////////////////////////////////
	// iTX does not send server time, except in heartbeat.  But it is not guaranteed to ever get a hearbeat message.
	// we need servertime, so if allowed, grab the system time here, on receipt of any message.
  // we leave this as a global setting so that you can use it with Colossus too.

										if(pConn->m_ulFlags&OMNI_SYSTIME)
										{
											pConn->UpdateServerTime(true);  // sets vars.
										}

*/
										int nType = 0;
										if(strstr(pchXML, "<heartbeat>"))  // this should never be, but let's handle it just in case.

										{

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d H\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, bInitConnection);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d A heartbeat message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}
											// heartbeat msg
											pchEnd = strstr(pchXML, "</heartbeat>");
											// find the list name:

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;
											bool bConnected = false;

											if(!(pConn->m_ulFlags&OMNI_SYSTIME))
											{
												pchName = strstr(pchXML, "<currentTime>");
												if((pchName)&&(pchEnd)&&(pchName<pchEnd))
												{
													pchName += strlen("<currentTime>");
													pch = strstr(pchName, "</currentTime>");
													if((pch)&&(pch<pchEnd))
													{
														
/*
	EnterCriticalSection(&pConn->m_critOperations);
														int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
														if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
														//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
														pConn->UpdateServerTime(false);
	LeaveCriticalSection(&pConn->m_critOperations);
*/
														int nMS = connUtil.ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
	EnterCriticalSection(&pConn->m_critOperations);
														if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
														//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
														pConn->UpdateServerTime(false);
	LeaveCriticalSection(&pConn->m_critOperations);
													}
												}
											}
											// TODO: may want to change this later to not bother allocating unconnected lists.

											pchName = strstr(pchXML, "<channel>");
											while((pchName)&&(!pConn->m_bKillThread))
											{
												usListID = LIST_NOT_DEFINED;
												bConnected = false;
												pchEnd = strstr(pchName, "</channel>");
												if(pchEnd)  // we found a correctly encapsulted record.
												{
													pchNext = strstr(pchName, "<id>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<id>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<connected>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<connected>");
															bConnected = atoi(pchNext)==0?false:true;
														}
														pchNext = strstr(pchName, "<name>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<name>");
															pch = strstr(pchName, "</name>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else  strcpy(pchNext, ""); // blank name
														}
														else strcpy(pchNext, ""); // blank name

														// add the list!
if(!pConn->m_bKillThread)
{
 	EnterCriticalSection (&pConn->m_crit);
														pConn->UpdateList(usListID, pchNext, bConnected?OMNI_LIST_CONNECTED:OMNI_LIST_DISCONNECTED);
														int nListIndex = pConn->ReturnListIndex(usListID);
														if(nListIndex>=0)
														{
//															CCAList* pList = pConn->GetList(nListIndex);
															if(pList)
															{
																pList->UpdateListStatus();
															}
														}
	LeaveCriticalSection (&pConn->m_crit);
}
													}

													pchName = strstr(pchEnd, "<channel>");  // find the next one.
												} // if(pchEnd)
											}//while ((pchName)&&(!pConn->m_bKillThread))
										}
										else
										{

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d An output message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}

											nType = -1;
											// output message.
											bool bProcessedTime = false;


if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d O\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, bInitConnection);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}

	
											// note: iTX only sends roReplace (and heartbeat).  other commands are not supported.
											// iTX sends snapshots beginning with the currently playing event.

											unsigned short usRunningEventCount = 0;

											unsigned short usListID = pList->m_usListID;
											int nListIndex = pConn->ReturnListIndex(usListID);

											char* pchName = NULL;

											pchName = strstr(pchXML, "<roReplace>");
											if(pchName)
											{
												nType = 1;
												pchEnd = strstr(pchName, "</roReplace>");
												if(pchEnd)  // we found a correctly encapsulated record.
												{
													pchNext = strstr(pchName, "<roID>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<roID>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<roSlug>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<roSlug>");
															pch = strstr(pchName, "</roSlug>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else
															{
																pchNext = NULL; // blank name
																pch = pchName;
															}
														}
														else
														{
															pchNext = NULL; // blank name
															pch = pchName;
														}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roReplace, about to update\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}


				//										int nListIndex = OMNI_SUCCESS;

/*
														if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
														{

															EnterCriticalSection(&g_actives.m_critActive);
												//			AfxMessageBox("roReplace, about to enter channel active check");
															nListIndex = g_actives.IsChannelActive(usListID);
												//			AfxMessageBox("roReplace, left channel active check");
															LeaveCriticalSection(&g_actives.m_critActive);
														}
*/
														
														if(!pConn->m_bKillThread)
														{
															int nUL = 0;
															if(pchNext)
															{

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRU1\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}

	EnterCriticalSection (&pConn->m_crit);
																nUL = pConn->UpdateList(usListID, pchNext, OMNI_LIST_CONNECTED, &nListIndex);
	LeaveCriticalSection (&pConn->m_crit);

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRU2 %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, nUL);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}
														
															}
															else
															{
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRU3\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}
	EnterCriticalSection (&pConn->m_crit);
																nUL = pConn->UpdateList(usListID, "", OMNI_LIST_CONNECTED, &nListIndex);
	LeaveCriticalSection (&pConn->m_crit);
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRU4 %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, nUL);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}
															}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d UpdateList returned %d\r\n",clock(), nUL);
		fflush(fp);
		fclose(fp);
	}
}

														}

/*
														if(!pConn->m_bKillThread)
//														if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
														{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d updated, checking for events\r\n", clock());
		fflush(fp);
		fclose(fp);
	}
}

	EnterCriticalSection (&pConn->m_crit);
															nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
														}
*/
														if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
														{

															// then, deal with any events we might have.
															pchNext = strstr((pch+1), "<story>");

	////////////////////////////////////////////////////////////////////////////////////
	// deletion of all events
	//
	// we weren't able to just delete on a blank roReplaceMessage,
	// as it turns out, the 2.8.16 Adaptor would send these messages
	// when something on a track we were filtering changed.
	// update: v2.8.18 fixes the blank message on filtered track change.

															

															if(pchNext == NULL) // we don't have any events.  
															// This seems to be only sent if the list is initially blank on connect, 
															// or if the live schedule is deleted.  Interestingly, no deletion events are sent in this case.
															{

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRDAE>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	


EnterCriticalSection(&pConn->m_crit);
//																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(!pConn->m_bKillThread)
																	{
																		nReturnCode = pList->DeleteAllEvents( pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

																		if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																		{
																			fp = fopen(pConn->m_pszDebugFile, "at");
																			if(fp)
																			{
																				fprintf(fp, "DeleteAllEvents for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																				fflush(fp);
																				fclose(fp);
																			}
																		}

																		pList->UpdateListStatus();

																	}
/*
																	EnterCriticalSection(&pConn->m_critCounter);

																	if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																	pConn->m_ulCounter++;

																	LeaveCriticalSection(&pConn->m_critCounter);

*/
																	ulCounter++;
																}
LeaveCriticalSection(&pConn->m_crit);


if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRDAE<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	


															}

															while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
															{
																if((pConn->m_nMessageEventCountLimit > 0)&& ((int)usRunningEventCount >= pConn->m_nMessageEventCountLimit)) break;  // enforce limit


	if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
	{
		fp = fopen(pConn->m_pszDebugFile, "at");
		if(fp)
		{
			fprintf(fp, "%d pch n%d e%d\r\n", clock(),pchNext,pchEnd);
			fflush(fp);
			fclose(fp);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// iTX.  only sends this command, never deletes.
	// so we have to delete off the top, based on the first ID that is obtained.
	// we alslo have to delete any items in the list that are removed, since we are getting snapshots, if someone deletes from the middle of the list...



																char* pchEndRec = pchNext+strlen("<story>");
																pchName = strstr(pchNext, "</story>");
																if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																{
																	pchEndRec = pchName;
																	// ok, now pchNext is the beginning, 
																	// and pchEnd is the end of the list,
																	// pchEndRec is the end of the record, and we can use pchName for the fields
																	// we are interested in getting:
																	
																	//unsigned short m_usType;              <type>      // internal colossus type
																	//char* m_pszID;												<clipId>		// the clip ID
																	//char* m_pszTitle;											<title>		  // the title
																	//char* m_pszData;  										<data> 			//must encode zero.
																	//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																	//unsigned char  m_ucSegment;														// not used

																	// following 2 compiled from <startTime>
																	//unsigned long  m_ulOnAirTimeMS;
																	//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																	//unsigned long  m_ulDurationMS;        <dur>        // duration
																	//unsigned short m_usStatus;						<status>     // status (omnibus code)
																	//unsigned short m_usControl;           <timeMode>   // time mode

																	// additionally, up at the list level, we can compile from
																	//  <currentTime>
																	//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																	//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																	//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																	// let's create a new event object;
																	CCAEvent* pEvent = new CCAEvent;


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d pEvent %d\r\n", clock(),pEvent);
		fflush(fp);
		fclose(fp);
	}
}


																	char* pchField = NULL;

																	pchName = strstr(pchNext, "<storyID>");
																	if((pEvent) && (pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																	{

//20151106 2.1.1.15 - added parent pointer ssignment to self.  make default behavior, no option.  never referenced before so ignored elsewhere.  needed for Nucleus parameter queries
																		pEvent->m_pParent = pEvent;


																		pchName += strlen("<storyID>");
																		pch = strstr(pchName, "</storyID>");
																		if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																		{
																			*pch = 0;  //null term the ID!
																			pchField = (char*) malloc(strlen(pchName)+1);
																			if(pchField)  // have to at least have this field
																			{
																				strcpy(pchField, pchName);
																				pEvent->m_pszReconcileKey = pchField;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d story ID: %s\r\n", clock(),pEvent->m_pszReconcileKey?pEvent->m_pszReconcileKey:"NULL");
		fflush(fp);
		fclose(fp);
	}
}

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRSID %s<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, pchField);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

///////////////////////////////////////////////////////////////////////////////////////////////////////
// deal with iTX on the first event of the batch

																				if((pConn->m_ulFlags&OMNI_ITX)||(bInitConnection))
//																				if(pConn->m_ulFlags&OMNI_ITX)
																				{
																					if(usRunningEventCount<1)
																					{
																						// delete any preceding things left over, but include done count number of items.

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRINIT>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		


EnterCriticalSection(&pConn->m_crit);
//																						CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread)
																							{
																								int itxidx=0;
																								//int itxcount = pList->GetEventCount();

																								while(itxidx < pList->GetEventCount())
																								{
																									CCAEvent* pitxEvent = pList->GetEvent(itxidx);
																									if(pitxEvent)
																									{
																										// every event gets this to start:
																										pitxEvent->m_ulFlags &= ~OMNI_FLAGS_FOUND; // make sure this is cleared

																										if(strcmp(pitxEvent->m_pszReconcileKey, pEvent->m_pszReconcileKey)==0)
																										{
																											pitxEvent->m_ulFlags |= OMNI_FLAGS_FOUND;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d iTX search: initial found %d: %s\r\n", clock(), itxidx, pitxEvent->m_pszReconcileKey?pitxEvent->m_pszReconcileKey:"NULL");
		fflush(fp);
		fclose(fp);
	}
}

																											// found it
																											int delidx=-1;
																											if(itxidx>pList->m_usDoneCount)
																											{
																												// items to delete
																												delidx = itxidx - pList->m_usDoneCount - 1; // 0,1,2,3,4, curr at 4, done = 3, delete only index 0 delidx = 4-3-1 = 0
																											}
																											// now count back and set the next pList->m_usDoneCount events to done, then delete the rest off the top.
																											int itxpastidx = itxidx-1;

																											while(itxpastidx>=0)																											
																											{
																												if(itxpastidx <= delidx)
																												{
																													if(!pConn->m_bKillThread)
																													{
																														nReturnCode = pList->DeleteEvent(itxpastidx, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																														if(nReturnCode == OMNI_SUCCESS) itxidx--;// if the delete is successful, need to back up the index after deletion to continue from the right place.

																														if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																														{
																															fp = fopen(pConn->m_pszDebugFile, "at");
																															if(fp)
																															{
																																// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																																//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																																fprintf(fp, "%d DeleteEvent %d (<%d) (iTX Delete) for channel %d returned with %d\r\n", clock(), itxpastidx, delidx, pList->m_usListID, nReturnCode);
																																fflush(fp);
																																fclose(fp);
																															}
																														}
																													}
																												}
																												else  // change status to done
																												{
																													pitxEvent = pList->GetEvent(itxpastidx);
																													if(pitxEvent)
																													{
																														pitxEvent->m_usStatus = C_DONE; //done.

																														nReturnCode = pList->UpdateEvent(pitxEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);
																													
																														pitxEvent->m_ulFlags |= OMNI_FLAGS_FOUND;

																														if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																														{
																															fp = fopen(pConn->m_pszDebugFile, "at");
																															if(fp)
																															{
																																fprintf(fp, "%d UpdateEvent %d %s iTX done\r\n", clock(), itxpastidx, pitxEvent->m_pszReconcileKey);
																																fflush(fp);
																																fclose(fp);
																															}
																														}
																													}
																												}

																												itxpastidx--;
																											}

																											// done with the past events search 
																										}// was the first event matching the received first event.


																									}
																									itxidx++;
																								}
																							}
																						}
	LeaveCriticalSection(&pConn->m_crit);

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRINIT<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	
																					}
																				}

// deal with iTX on the first event of the batch
///////////////////////////////////////////////////////////////////////////////////////////////////////

																				*pch = '<'; // put it back so we can search whole string again.

																				pchName = strstr(pchNext, "<clipId>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<clipId>");
																					pch = strstr(pchName, "</clipId>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszID = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}


																				pchName = strstr(pchNext, "<title>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<title>");
																					pch = strstr(pchName, "</title>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszTitle = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRF>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		

EnterCriticalSection(&pConn->m_critOperations); //field parsing is a connection level util operation

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extract %d %d\r\n", clock(),pConn->m_ppfi,pConn->m_nNumFields );
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																				bool bFieldsFound = false;



																				if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																				{
																					// we found some fields!
																					bFieldsFound = true;	

																					for(int i=0; i<pConn->m_nNumFields; i++)
																					{

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extract %d %d %d\r\n", clock(), i, pConn->m_ppfi[i], pConn );
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						if((pConn->m_ppfi[i])&&(pConn->m_ppfi[i]->m_pszFieldName))
																						{
																							char pszTag[256];
																							sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
					//AfxMessageBox(pszTag);
																							pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																							if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																							{
																								sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																								pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																								if(pConn->m_ppfi[i]->m_pchEndTag)
																								{
																									pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																								}
																								if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																								{
																									int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																									pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																									{
																										memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																										memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																									}
																								}
																							}
																						}
																					}

																				//	Sleep(50);

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extracted\r\n", clock());
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																					//we have to check validity of fields (top-level-ness)
																					bool bAllTopLevel = false;
																					
																
																					while(!bAllTopLevel)
																					{
																						bool bOverlapFound = false;
																						i=0; 
																						while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																						{
																							for(int j=0; j<pConn->m_nNumFields-1; j++)
																							{
																								if(j!=i)
																								{
																									if(
																											(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																										&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																										)
																									{
																										// i is inside j, need to look for a different one and start over.
																										bOverlapFound = true;

																										char pszTag[256];
																										sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																										pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																										if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																										{
																											sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																											if(pConn->m_ppfi[i]->m_pchEndTag)
																											{
																												pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																											}
																											if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																											{
																												int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																												pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																												if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																												{
																													memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																													memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																												}
																											}
																										}
																										else
																										{
																											if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																											pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																										}
																										break;
																									}

																								}
																							}
																							
																							i++;
																						}

																						if(!bOverlapFound) bAllTopLevel = true;
																					}


																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data validated\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}
																					//assemble the data string
																					int nDataLen = 0;
																					for(i=0; i<pConn->m_nNumFields; i++)
																					{
																						if(pConn->m_ppfi[i]->m_pszFieldInfo)
																						{
																							nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
	//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																						}
																					}

																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data assembled\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																					
																					if(nDataLen)
																					{
																						pchField = (char*) malloc(nDataLen+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, "");
																							for(i=0; i<pConn->m_nNumFields; i++)
																							{
																								if(pConn->m_ppfi[i]->m_pszFieldInfo)
																								{
																									strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																									free(pConn->m_ppfi[i]->m_pszFieldInfo);
																									pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																								}
																							}
																							pEvent->m_pszData = pchField;
	//																						AfxMessageBox(pEvent->m_pszData );
																						}
																					}

																				}

	LeaveCriticalSection(&pConn->m_critOperations); //field parsing is a connection level util operation


if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRF<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data tagged\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																				if(!bFieldsFound)
																				{
																					// use default, minus tags
																					pchName = strstr(pchNext, "<data>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<data>");
																						pch = strstr(pchName, "</data>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszData = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}
																				}
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data end\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																				pchName = strstr(pchNext, "<startTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<startTime>");
																					pch = strstr(pchName, "</startTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						if(pConn->m_ulFlags&OMNI_ITX)
																						{
//	An important value is 621355968000000000.  this is the number of dateTime ticks from 0001 to 1970.  this offset gets you back to unix time

//																							pEvent->m_dblOnAirTimeInternal = (atof(pchName) - 621355968000000000.0)/10000.0;  // now unixtime in seconds.  // this was wrong!!!!  it's milliseconds! 
																							pEvent->m_dblOnAirTimeInternal = (atof(pchName) - 621355968000000000.0)/10000000.0;  // now unixtime in seconds.  // corrected here 2.1.1.18

																							// then multiply by number of frames per second.  Leave it as a non-integer, that is fine.
																							switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																							{
																							case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 25.0;
																								} break;
																							case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 30.0;
																								} break;
																							default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 29.97;
																								}
																								break;
																							}
																							
																						}
																						else
																						{
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);
																						}

																						if(pConn->m_nLookaheadTimeLimitSec > 0)
																						{
																							double dblLimit = (double)(pConn->m_ulConnLastMessage +  pConn->m_nLookaheadTimeLimitSec);
																							switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																							{
																							case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																								{
																									dblLimit *= 25.0;
																								} break;
																							case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																								{
																									dblLimit *= 30.0;
																								} break;
																							default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																								{
																									dblLimit *= 29.97;
																								}
																								break;
																							}

																							if(pEvent->m_dblOnAirTimeInternal > dblLimit)	break;  //past the lookahead, discard the rest.
																						}

//	EnterCriticalSection(&pConn->m_critOperations); 
																						int nMS = connUtil.ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
//																						int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																						else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
//	LeaveCriticalSection(&pConn->m_critOperations); 
																					}
																				}

																				if((!(pConn->m_ulFlags&OMNI_SYSTIME))&&((!pConn->m_bPreventServerTimeFromEvents)||(!bProcessedTime))) //2.1.1.18
//																				if(!(pConn->m_ulFlags&OMNI_SYSTIME))
																				{
																					pchName = strstr(pchNext, "<currentTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<currentTime>");
																						pch = strstr(pchName, "</currentTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
//	EnterCriticalSection(&pConn->m_critOperations); 
																							int nMS = connUtil.ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
//																							int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																							//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
//	LeaveCriticalSection(&pConn->m_critOperations); 

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRST>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

		EnterCriticalSection(&pConn->m_crit);
																						pConn->UpdateServerTime(false);
		LeaveCriticalSection(&pConn->m_crit); 
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRST<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

																							bProcessedTime=true;
																						}
																					}
																				}

																				pchName = strstr(pchNext, "<type>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<type>");
																					pch = strstr(pchName, "</type>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usType = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<dur>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<dur>");
																					pch = strstr(pchName, "</dur>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
//	EnterCriticalSection(&pConn->m_critOperations); 
																						int nMS = connUtil.ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
//																						int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0)
																						{
																							pEvent->m_ulDurationMS = (unsigned long) nMS;

																							if(pConn->m_nLookbehindTimeLimitSec > 0)
																							{
																								double dblLimit = (double)(pConn->m_ulConnLastMessage -  pConn->m_nLookbehindTimeLimitSec);
																								switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																								{
																								case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																									{
																										dblLimit *= 25.0;
																									} break;
																								case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																									{
																										dblLimit *= 30.0;
																									} break;
																								default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																									{
																										dblLimit *= 29.97;
																									}
																									break;
																								}

																								double dblTest = (((double)(pEvent->m_ulDurationMS))/1000.0);
																								switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																								{
																								case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																									{
																										dblTest *= 25.0;
																									} break;
																								case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																									{
																										dblTest *= 30.0;
																									} break;
																								default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																									{
																										dblTest *= 29.97;
																									}
																									break;
																								}


																								if(pEvent->m_dblOnAirTimeInternal + dblTest < dblLimit)
																								{
																									pchNext = strstr(pchEndRec, "<story>");
																								//	usRunningEventCount++;
																									continue;  //in the past , skip and go to next.
																								}
																							}
																						}
																						else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
//	LeaveCriticalSection(&pConn->m_critOperations); 
																					}
																				}

																				pchName = strstr(pchNext, "<status>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<status>");
																					pch = strstr(pchName, "</status>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usStatus = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<timeMode>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<timeMode>");
																					pch = strstr(pchName, "</timeMode>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usControl = (unsigned short)atoi(pchName);
																					}
																				}


																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d updating\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}


	//																			Ok, lets update the event!
																				if(nListIndex>=0)
																				{
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RREU>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	
																					if(!pConn->m_bKillThread)
																					{
		EnterCriticalSection(&pConn->m_crit);
//																					CCAList* pList = pConn->GetList(nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread) 
																							{

																								double dblTime = ((double)timestamp.millitm/1000.0);
																								if(pConn->m_bUseUTC)
																								{
																									dblTime += (double)(timestamp.time); // UTC
																								}
																								else
																								{
																									dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																								}

																								if(pConn->m_bUseDST)
																								{
																									dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																								}

																								pEvent->m_dblUpdated = dblTime;  // UTC 
																								 
																								nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);

																								pEvent->m_ulFlags |= OMNI_FLAGS_FOUND;
																							
																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent Sleep %d\r\n", clock(), pConn->m_ulInterEventIntervalMS);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																								
																								Sleep(pConn->m_ulInterEventIntervalMS);   // was zero, but pegged processor

																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent (roReplace) %s for channel %d returned %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																								nReturnCode = pList->UpdateListStatus();

																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateListStatus %d returned %d\r\n", clock(), pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																							}
																						}


																						ulCounter++;


																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d counter now %d\r\n", clock(),ulCounter);
																									fflush(fp);
																									fclose(fp);
																								}
																							}
/*
																	EnterCriticalSection(&pConn->m_critCounter);
																					if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																					pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
*/
LeaveCriticalSection(&pConn->m_crit);
																					}
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RREU<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}	

																				}
																			}
																		}
																	}
																	//else // cant do anything with this event, so just have to skip.
																}

																pchNext = strstr(pchEndRec, "<story>");
																usRunningEventCount++;

////////////// async database check 2.1.1.18
// here we want to interleave a check of the list, to see if there are any events that need to be committed to the database.
// if so, we only do one, we will interleave them with the items in roReplace.
// we only do this in roReplace since we are expecting many events, coul dbe in the thousands, that take up processing time


																if(pList->m_ulFlags&OMNI_LIST_UNCOMMITTED_EXISTS)  // this ony gets set if pConn->m_nAsyncDatabaseLookaheadSec > 0
																{
																	pList->CheckDatabaseEvents(-1, 1);// start at last known index, limit items to one.
																}

////////////// async database check

															} // while records exist

///////////////////////////////////////////////////////////////////////////////////////////////////////
// deal with iTX after the last event of the batch
															if((pConn->m_ulFlags&OMNI_ITX)||(bInitConnection))
//															if(pConn->m_ulFlags&OMNI_ITX)
															{
																					// delete any preceding things left over, but include done count number of items.

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRINDEL>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		


EnterCriticalSection(&pConn->m_crit);
//																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(!pConn->m_bKillThread)
																	{
																		int itxcount = pList->GetEventCount()-1;

																		while( itxcount >= 0 )
																		{
																			CCAEvent* pitxEvent = pList->GetEvent(itxcount);
																			if(pitxEvent)
																			{
																				// every event gets this to start:
																				if(pitxEvent->m_ulFlags&OMNI_FLAGS_FOUND)
																				{
																					// was in the received data, so just clear the flag.
																					pitxEvent->m_ulFlags &= ~OMNI_FLAGS_FOUND;
																				}
																				else
																				{
																					// wasn't in the received data, so must be deleted.
																					if(!pConn->m_bKillThread)
																					{
																						nReturnCode = pList->DeleteEvent(itxcount, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

																						if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																						{
																							fp = fopen(pConn->m_pszDebugFile, "at");
																							if(fp)
																							{
																								// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																								//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																								fprintf(fp, "%d DeleteEvent %d (iTX Delete) for channel %d returned with %d\r\n", clock(), itxcount, pList->m_usListID, nReturnCode);
																								fflush(fp);
																								fclose(fp);
																							}
																						}
																					}
																				}
																			}
																			itxcount--;
																		}
																	}
																}

	LeaveCriticalSection(&pConn->m_crit);


if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d RRINDEL<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		


															}


// deal with iTX after the last event of the batch
///////////////////////////////////////////////////////////////////////////////////////////////////////
														}
													}
												}
											}
											else  // not roReplace
											{
												pchName = strstr(pchXML, "<roStoryReplace>");
												if(pchName)
												{
													nType = 2;
													// it's a replace single item record
													pchEnd = strstr(pchName, "</roStoryReplace>");
													if(pchEnd)  // we found a correctly encapsulated record.
													{
														pchNext = strstr(pchName, "<roID>");
														if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
														{
															pchNext += strlen("<roID>");
															usListID = atoi(pchNext);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roStoryReplace\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}


															//int nListIndex = OMNI_SUCCESS;
															// add or update the list!
/*
															if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
															{
																EnterCriticalSection(&g_actives.m_critActive);
												//				AfxMessageBox("roStoryReplace, about to enter channel active check");
																nListIndex = g_actives.IsChannelActive(usListID);
												//				AfxMessageBox("roStoryReplace, left channel active check");
																LeaveCriticalSection(&g_actives.m_critActive);
															}
*/
/*
															if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
															{ 
	EnterCriticalSection (&pConn->m_crit);
																nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
															}
*/
															if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
															{


																// then, deal with the events we have.
																pchNext = strstr(pchName, "<story>");
																while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))  // this while should only get hit once.
																{
																	pchName = strstr(pchNext, "</story>");
																	if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																	{
																		char* pchEndRec = pchName;
																		// ok, now pchNext is the beginning, 
																		// and pchEnd is the end of the list,
																		// pchEndRec is the end of the record, and we can use pchName for the fields
																		// we are interested in getting:
																		
																		//unsigned short m_usType;              <type>      // internal colossus type
																		//char* m_pszID;												<clipId>		// the clip ID
																		//char* m_pszTitle;											<title>		  // the title
																		//char* m_pszData;  										<data> 			//must encode zero.
																		//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																		//unsigned char  m_ucSegment;														// not used

																		// following 2 compiled from <startTime>
																		//unsigned long  m_ulOnAirTimeMS;
																		//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																		//unsigned long  m_ulDurationMS;        <dur>        // duration
																		//unsigned short m_usStatus;						<status>     // status (omnibus code)
																		//unsigned short m_usControl;           <timeMode>   // time mode

																		// additionally, up at the list level, we can compile from
																		//  <currentTime>
																		//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																		//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																		//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																		// let's create a new event object;
																		CCAEvent* pEvent = new CCAEvent;
																		char* pchField;

																		pchName = strstr(pchNext, "<storyID>");
																		if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																		{

//20151106 2.1.1.15 - added parent pointer ssignment to self.  make default behavior, no option.  never referenced before so ignored elsewhere.  needed for Nucleus parameter queries
																			pEvent->m_pParent = pEvent;

																			pchName += strlen("<storyID>");
																			pch = strstr(pchName, "</storyID>");
																			if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																			{
																				*pch = 0;  //null term the ID!
																				pchField = (char*) malloc(strlen(pchName)+1);
																				if(pchField)  // have to at least have this field
																				{
																					strcpy(pchField, pchName);
																					pEvent->m_pszReconcileKey = pchField;
																					*pch = '<'; // put it back so we can search whole string again.

																					pchName = strstr(pchNext, "<clipId>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<clipId>");
																						pch = strstr(pchName, "</clipId>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszID = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}


																					pchName = strstr(pchNext, "<title>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<title>");
																						pch = strstr(pchName, "</title>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszTitle = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags

	EnterCriticalSection(&pConn->m_critOperations); //field parsing is a connection level util operation

																					bool bFieldsFound = false;

																					if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																					{
																						// we found some fields!
																						bFieldsFound = true;	

																						for(int i=0; i<pConn->m_nNumFields; i++)
																						{

																							if(pConn->m_ppfi[i]->m_pszFieldName)
																							{
																								char pszTag[256];
																								sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
						//AfxMessageBox(pszTag);
																								pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																								if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																								{
																									sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																									pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																									if(pConn->m_ppfi[i]->m_pchEndTag)
																									{
																										pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																									}
																									if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																									{
																										int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																										pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																										if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																										{
																											memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																											memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																										}
																									}
																								}
																							}
																						}


																						//we have to check validity of fields (top-level-ness)
																						bool bAllTopLevel = false;
																						
																						while(!bAllTopLevel)
																						{
																							bool bOverlapFound = false;
																							i=0; 
																							while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																							{
																								for(int j=0; j<pConn->m_nNumFields-1; j++)
																								{
																									if(j!=i)
																									{
																										if(
																												(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																											&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																											)
																										{
																											// i is inside j, need to look for a different one and start over.
																											bOverlapFound = true;

																											char pszTag[256];
																											sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																											if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																											{
																												sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																												pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																												if(pConn->m_ppfi[i]->m_pchEndTag)
																												{
																													pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																												}
																												if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																												{
																													int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																													if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																													pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																													if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																													{
																														memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																														memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																													}
																												}
																											}
																											else
																											{
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																												pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																											}
																											break;
																										}

																									}
																								}
																								
																								i++;
																							}

																							if(!bOverlapFound) bAllTopLevel = true;
																						}

																						//assemble the data string
																						int nDataLen = 0;
																						for(i=0; i<pConn->m_nNumFields; i++)
																						{
																							if(pConn->m_ppfi[i]->m_pszFieldInfo)
																							{
																								nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
		//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																							}
																						}
																						
																						if(nDataLen)
																						{
																							pchField = (char*) malloc(nDataLen+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, "");
																								for(i=0; i<pConn->m_nNumFields; i++)
																								{
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)
																									{
																										strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																										free(pConn->m_ppfi[i]->m_pszFieldInfo);
																										pConn->m_ppfi[i]->m_pszFieldInfo = NULL;

																									}
																								}
																								pEvent->m_pszData = pchField;
		//																						AfxMessageBox(pEvent->m_pszData );
																							}
																						}

																					}

	LeaveCriticalSection(&pConn->m_critOperations); //field parsing is a connection level util operation

																					if(!bFieldsFound)
																					{
																						// use default, minus tags
																						pchName = strstr(pchNext, "<data>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<data>");
																							pch = strstr(pchName, "</data>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
																								*pch = 0;  //null term the ID!
																								pchField = (char*) malloc(strlen(pchName)+1);
																								if(pchField)  // have to at least have this field
																								{
																									strcpy(pchField, pchName);
																									pEvent->m_pszData = pchField;
																									*pch = '<'; // put it back so we can search whole string again.
																								}
																							}
																						}
																					}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																					pchName = strstr(pchNext, "<startTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<startTime>");
																						pch = strstr(pchName, "</startTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							// no need to deal with iTX here, since iTX does not send this command.
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);

																							if(pConn->m_nLookaheadTimeLimitSec > 0)
																							{
																								double dblLimit = (double)(pConn->m_ulConnLastMessage +  pConn->m_nLookaheadTimeLimitSec);
																								switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																								{
																								case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																									{
																										dblLimit *= 25.0;
																									} break;
																								case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																									{
																										dblLimit *= 30.0;
																									} break;
																								default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																									{
																										dblLimit *= 29.97;
																									}
																									break;
																								}

																								if(pEvent->m_dblOnAirTimeInternal > dblLimit)	break;  //past the lookahead, discard the rest.
																							}


//	EnterCriticalSection(&pConn->m_critOperations); 
																							int nMS = connUtil.ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
//																							int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																							else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
//	LeaveCriticalSection(&pConn->m_critOperations); 
																						}
																					}

																					if((!(pConn->m_ulFlags&OMNI_SYSTIME))&&((!pConn->m_bPreventServerTimeFromEvents)||(!bProcessedTime))) //2.1.1.18
//																					if(!(pConn->m_ulFlags&OMNI_SYSTIME))
																					{
																						pchName = strstr(pchNext, "<currentTime>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<currentTime>");
																							pch = strstr(pchName, "</currentTime>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
	EnterCriticalSection(&pConn->m_critOperations); // leave this for reftime
																								int nMS = connUtil.ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																								if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																								//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
	LeaveCriticalSection(&pConn->m_critOperations); 
		EnterCriticalSection(&pConn->m_crit);
																								pConn->UpdateServerTime(false);
		LeaveCriticalSection(&pConn->m_crit);
																								bProcessedTime=true;
																							}
																						}
																					}

																					pchName = strstr(pchNext, "<type>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<type>");
																						pch = strstr(pchName, "</type>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usType = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<dur>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<dur>");
																						pch = strstr(pchName, "</dur>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
//	EnterCriticalSection(&pConn->m_critOperations); 
																							int nMS = connUtil.ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
//																							int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) 
																							{
																								pEvent->m_ulDurationMS = (unsigned long) nMS;

																								if(pConn->m_nLookbehindTimeLimitSec > 0)
																								{
																									double dblLimit = (double)(pConn->m_ulConnLastMessage -  pConn->m_nLookbehindTimeLimitSec);
																									switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																									{
																									case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																										{
																											dblLimit *= 25.0;
																										} break;
																									case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																										{
																											dblLimit *= 30.0;
																										} break;
																									default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																										{
																											dblLimit *= 29.97;
																										}	break;
																									}

																									double dblTest = (((double)(pEvent->m_ulDurationMS))/1000.0);
																									switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																									{
																									case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																										{
																											dblTest *= 25.0;
																										} break;
																									case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																										{
																											dblTest *= 30.0;
																										} break;
																									default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																										{
																											dblTest *= 29.97;
																										}	break;
																									}


																									if(pEvent->m_dblOnAirTimeInternal + dblTest < dblLimit)
																									{
																										pchNext = strstr(pchEndRec, "<story>");
																									//	usRunningEventCount++;
																			//							continue;  //in the past , skip and go to next.
																								
//	LeaveCriticalSection(&pConn->m_critOperations); 

																										break;  //in the past, skip and go to next. // maybe ther eis no next, but just go around 
																									}
																								}

																							}
																							else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
//	LeaveCriticalSection(&pConn->m_critOperations); 
																						}
																					}

																					pchName = strstr(pchNext, "<status>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<status>");
																						pch = strstr(pchName, "</status>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usStatus = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<timeMode>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<timeMode>");
																						pch = strstr(pchName, "</timeMode>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usControl = (unsigned short)atoi(pchName);
																						}
																					}

		//																			Ok, lets update the event!
																					if(nListIndex>=0)
																					{
																						if(!pConn->m_bKillThread) 
																						{
EnterCriticalSection(&pConn->m_crit);
	//																					CCAList* pList = pConn->GetList(nListIndex);
																							if(pList)
																							{
																								if(!pConn->m_bKillThread) 
																								{
																									double dblTime = ((double)timestamp.millitm/1000.0);
																									if(pConn->m_bUseUTC)
																									{
																										dblTime += (double)(timestamp.time); // UTC
																									}
																									else
																									{
																										dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																									}

																									if(pConn->m_bUseDST)
																									{
																										dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																									}
																									pEvent->m_dblUpdated = dblTime;


																									nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);

																									Sleep(pConn->m_ulInterEventIntervalMS);  // was zero, but pegged processor
																									
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d UpdateEvent (roStoryReplace) %s for channel %d returned %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																									nReturnCode = pList->UpdateListStatus();

																								}
																							}

																							ulCounter++;

	/*
																		EnterCriticalSection(&pConn->m_critCounter);
																							if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																							pConn->m_ulCounter++;
																		LeaveCriticalSection(&pConn->m_critCounter);
	*/
LeaveCriticalSection(&pConn->m_crit);
																						}
																					}
																				}
																			}
																		}
																		//else // cant do anything with this event, so just have to skip.
																	}

																	pchNext = strstr(pchEnd, "<story>");
																}
															}
														} //if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
													}
												} 
												else
												if(!pConn->m_bKillThread)
												{
													pchName = strstr(pchXML, "<roStoryDelete>");
													if(pchName)
													{
														nType = 3;
															// it's a delete record
														pchEnd = strstr(pchName, "</roStoryDelete>");
														if(pchEnd)  // we found a correctly encapsulated record.
														{
															pchNext = strstr(pchName, "<roID>");
															if((pchNext)&&(pchNext<pchEnd))
															{
																pchNext += strlen("<roID>");
																usListID = atoi(pchNext);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roStoryDelete\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}

//																int nListIndex = OMNI_SUCCESS;
/*
																if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
																{
																	EnterCriticalSection(&g_actives.m_critActive);
													//				AfxMessageBox("roStoryDelete, about to enter channel active check");
																	nListIndex = g_actives.IsChannelActive(usListID);
												//				//	AfxMessageBox("roStoryDelete, left channel active check");
																	LeaveCriticalSection(&g_actives.m_critActive);
																}
*/
/*
																if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
																{ 
	EnterCriticalSection (&pConn->m_crit);
																	nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
																}
*/
																if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
																{
																	//pConn->m_ppList[i]->UpdateEvent(pEvent);

																	// then, find the story ID to delete
																	pchNext = strstr(pchName, "<storyID>");
																	if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
																	{
																		pchNext += strlen("<storyID>");
																		pch = strstr(pchNext, "</storyID>");
																		if((pch)&&(pch<pchEnd))
																		{
																			*pch = 0;  //null term the ID!
																			if(!pConn->m_bKillThread) 
																			{
EnterCriticalSection(&pConn->m_crit);
												//							CCAList* pList = pConn->GetList(nListIndex);
																				if(pList)
																				{
				EnterCriticalSection(&pList->m_critEvents);
																					int j = pList->GetEventIndexByRecKey(pchNext);
				LeaveCriticalSection(&pList->m_critEvents);
																					if(j>=0)
																					{
																						if(!pConn->m_bKillThread)
																						{
																							int pre=pList->GetEventCount();
																							nReturnCode = pList->DeleteEvent(j, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d DEL@%d %s r%d num %d->%d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, j, pchNext, nReturnCode, pre,  pList->GetEventCount() );
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}
																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																									//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																									fprintf(fp, "%d DeleteEvent (roStoryDelete) for channel %d returned with %d\r\n", clock(), pList->m_usListID, nReturnCode);
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						}

																						ulCounter++;
	/*
																		EnterCriticalSection(&pConn->m_critCounter);
																						if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																						pConn->m_ulCounter++;
																		LeaveCriticalSection(&pConn->m_critCounter);
	*/
																					}
																					else
																					{
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d DEL %s not found; num %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, pchNext,  pList->GetEventCount() );
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}

																					}
																				}
LeaveCriticalSection(&pConn->m_crit);
																			}
																		}
																	}
																}
															}
														}//if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))


													}  // else it's an unrecognized mos command, or time to die
												}
											}


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d An output message has been processed. %d %n\r\n", clock(), ulCounter, nType);
		fflush(fp);
		fclose(fp);

	}
}
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d O %d<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, nType);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		


										}

						EnterCriticalSection(&pConn->m_critCounter);
										if(ULONG_MAX - pConn->m_ulCounter < ulCounter )
										{
											ulCounter -= (ULONG_MAX - pConn->m_ulCounter);
											pConn->m_ulCounter = ulCounter;
										}
										else	pConn->m_ulCounter += ulCounter;
						LeaveCriticalSection(&pConn->m_critCounter);

										nReturnCode = pList->UpdateListStatus(true); // 2.1.1.18 - force a list update once for each message received.


if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d LU %d.%03d + %d: %d type %d num %d\r\n", pList->m_usListID, timestamp.time, timestamp.millitm, pList->m_tmbLastUpdate.time, pList->m_tmbLastUpdate.millitm, pConn->m_nMinimumListUpdateIntervalMS, nReturnCode, nType, pList->GetEventCount() );
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d free [%d] %d\r\n", clock(), pchXML, nReturnCode);
		fflush(fp);
		fclose(fp);

	}
}
if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d ODEL>\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		

					delete [] pchXML;
					pchXML = NULL;

if(pConn->m_ulFlags&OMNI_DEBUG_PROCESS)
{
	EnterCriticalSection(&pConn->m_critAux);
	if(pConn->pAux)
	{
		_ftime(&timestamp);
		FILE* fp = (FILE*)pConn->pAux;
		if(fp)
		{
			fprintf(fp, "[%d> %d.%03d ODEL<\r\n", pList->m_usListID, timestamp.time, timestamp.millitm);
			fflush(fp);
		}
	}
	LeaveCriticalSection(&pConn->m_critAux);
}		

}


void ProcessXML(CCAConn* pConn, char* pchXML)
{
	if((pConn==NULL)||(pchXML==NULL)) return;

	_timeb timestamp;
	FILE* fp;
	CNetUtil	net;  // the connection exclusive networking obj
//	char szMsg[DB_ERRORSTRING_LEN];
//	char szSQL[DB_SQLSTRING_MAXLEN];
	char* pchEnd = NULL;
	char* pchNext = NULL;
	char* pch = NULL;
	int nReturnCode;
	unsigned long ulCounter = 0;  //local counter, we're going to parse the whole XML first, THEN update the counter.
	// otherwise, we are hammering the database with requests, if it is a large XML packet, best to do it all at once.

	bool bInitConnection = false;

	if(pConn->m_ulFlags&OMNI_COMPAREONCONNECT)
	{

		if(*pchXML == 0x01)
		{
			*pchXML = '<';
			bInitConnection = true;
		}

		if(*pchXML == 0x02)
		{
			*pchXML = '%';
			bInitConnection = true;
		}
	}	
										// let's set the time.
										_ftime( &timestamp );

										pConn->m_ulConnLastMessage = timestamp.time;

										// let's populate the channels and the event information,

	////////////////////////////////////////////////////////////////////////////////////
	// iTX does not send server time, except in heartbeat.  But it is not guaranteed to ever get a hearbeat message.
	// we need servertime, so if allowed, grab the system time here, on receipt of any message.
  // we leave this as a global setting so that you can use it with Colossus too.

										if(pConn->m_ulFlags&OMNI_SYSTIME)
										{
											pConn->UpdateServerTime(true);  // sets vars.
										}

										if(strstr(pchXML, "<heartbeat>"))
										{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d A heartbeat message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}
											// heartbeat msg
											pchEnd = strstr(pchXML, "</heartbeat>");
											// find the list name:

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;
											bool bConnected = false;

											if(!(pConn->m_ulFlags&OMNI_SYSTIME))
											{
												pchName = strstr(pchXML, "<currentTime>");
												if((pchName)&&(pchEnd)&&(pchName<pchEnd))
												{
													pchName += strlen("<currentTime>");
													pch = strstr(pchName, "</currentTime>");
													if((pch)&&(pch<pchEnd))
													{
														
														int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
														if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
														//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
														pConn->UpdateServerTime(false);

													}
												}
											}
											// TODO: may want to change this later to not bother allocating unconnected lists.

											pchName = strstr(pchXML, "<channel>");
											while((pchName)&&(!pConn->m_bKillThread))
											{
												usListID = LIST_NOT_DEFINED;
												bConnected = false;
												pchEnd = strstr(pchName, "</channel>");
												if(pchEnd)  // we found a correctly encapsulted record.
												{
													pchNext = strstr(pchName, "<id>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<id>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<connected>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<connected>");
															bConnected = atoi(pchNext)==0?false:true;
														}
														pchNext = strstr(pchName, "<name>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<name>");
															pch = strstr(pchName, "</name>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else  strcpy(pchNext, ""); // blank name
														}
														else strcpy(pchNext, ""); // blank name

														// add the list!
if(!pConn->m_bKillThread)
{
 	EnterCriticalSection (&pConn->m_crit);
														pConn->UpdateList(usListID, pchNext, bConnected?OMNI_LIST_CONNECTED:OMNI_LIST_DISCONNECTED);
														int nListIndex = pConn->ReturnListIndex(usListID);
														if(nListIndex>=0)
														{
															CCAList* pList = pConn->GetList(nListIndex);
															if(pList)
															{
																pList->UpdateListStatus();
															}
														}
	LeaveCriticalSection (&pConn->m_crit);
}
													}

													pchName = strstr(pchEnd, "<channel>");  // find the next one.
												} // if(pchEnd)
											}//while ((pchName)&&(!pConn->m_bKillThread))
										}
										else
										{

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d %d An output message has been received.\r\n", clock(), pConn->m_ulConnLastMessage);
		fflush(fp);
		fclose(fp);

	}
}

											// output message.
											bool bProcessedTime = false;

	
											// note: iTX only sends roReplace (and heartbeat).  other commands are not supported.
											// iTX sends snapshots beginning with the currently playing event.

											unsigned short usRunningEventCount = 0;

											unsigned short usListID = LIST_NOT_DEFINED;
											char* pchName = NULL;

											pchName = strstr(pchXML, "<roReplace>");
											if(pchName)
											{
												pchEnd = strstr(pchName, "</roReplace>");
												if(pchEnd)  // we found a correctly encapsulated record.
												{
													pchNext = strstr(pchName, "<roID>");
													if((pchNext)&&(pchNext<pchEnd))
													{
														pchNext += strlen("<roID>");
														usListID = atoi(pchNext);

														pchNext = strstr(pchName, "<roSlug>");
														if((pchNext)&&(pchNext<pchEnd))
														{
															pchNext += strlen("<roSlug>");
															pch = strstr(pchName, "</roSlug>");
															if((pch)&&(pch<pchEnd)) *pch = 0;  //null term
															else
															{
																pchNext = NULL; // blank name
																pch = pchName;
															}
														}
														else
														{
															pchNext = NULL; // blank name
															pch = pchName;
														}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roReplace, about to update\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}


														int nListIndex = OMNI_SUCCESS;

/*
														if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
														{

															EnterCriticalSection(&g_actives.m_critActive);
												//			AfxMessageBox("roReplace, about to enter channel active check");
															nListIndex = g_actives.IsChannelActive(usListID);
												//			AfxMessageBox("roReplace, left channel active check");
															LeaveCriticalSection(&g_actives.m_critActive);
														}
*/
														
														if(!pConn->m_bKillThread)
														{
															int nUL = 0;
															if(pchNext)
															{
	EnterCriticalSection (&pConn->m_crit);
																nUL = pConn->UpdateList(usListID, pchNext, OMNI_LIST_CONNECTED);
	LeaveCriticalSection (&pConn->m_crit);
															}
															else
															{
	EnterCriticalSection (&pConn->m_crit);
																nUL = pConn->UpdateList(usListID, "", OMNI_LIST_CONNECTED);
	LeaveCriticalSection (&pConn->m_crit);
															}

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d UpdateList returned %d\r\n",clock(), nUL);
		fflush(fp);
		fclose(fp);
	}
}

														}

														if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
														{
if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d updated, checking for events\r\n", clock());
		fflush(fp);
		fclose(fp);
	}
}

	EnterCriticalSection (&pConn->m_crit);
															nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
														}

														if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
														{

															// then, deal with any events we might have.
															pchNext = strstr((pch+1), "<story>");

	////////////////////////////////////////////////////////////////////////////////////
	// deletion of all events
	//
	// we weren't able to just delete on a blank roReplaceMessage,
	// as it turns out, the 2.8.16 Adaptor would send these messages
	// when something on a track we were filtering changed.
	// update: v2.8.18 fixes the blank message on filtered track change.

															

															if(pchNext == NULL) // we don't have any events.  
															// This seems to be only sent if the list is initially blank on connect, 
															// or if the live schedule is deleted.  Interestingly, no deletion events are sent in this case.
															{
	EnterCriticalSection(&pConn->m_crit);
																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(!pConn->m_bKillThread)
																	{
																		nReturnCode = pList->DeleteAllEvents( pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

																		if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																		{
																			fp = fopen(pConn->m_pszDebugFile, "at");
																			if(fp)
																			{
																				fprintf(fp, "DeleteAllEvents for channel %d returned with %d\r\n", pList->m_usListID, nReturnCode);
																				fflush(fp);
																				fclose(fp);
																			}
																		}

																		pList->UpdateListStatus();

																	}
/*
																	EnterCriticalSection(&pConn->m_critCounter);

																	if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																	pConn->m_ulCounter++;

																	LeaveCriticalSection(&pConn->m_critCounter);

*/
																	ulCounter++;
																}
	LeaveCriticalSection(&pConn->m_crit);


															}

															 
															while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
															{
																if((pConn->m_nMessageEventCountLimit > 0)&&((int)usRunningEventCount >= pConn->m_nMessageEventCountLimit)) break;  // enforce limit


	if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
	{
		fp = fopen(pConn->m_pszDebugFile, "at");
		if(fp)
		{
			fprintf(fp, "%d pch n%d e%d\r\n", clock(),pchNext,pchEnd);
			fflush(fp);
			fclose(fp);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// iTX.  only sends this command, never deletes.
	// so we have to delete off the top, based on the first ID that is obtained.
	// we alslo have to delete any items in the list that are removed, since we are getting snapshots, if someone deletes from the middle of the list...



																char* pchEndRec = pchNext+strlen("<story>");
																pchName = strstr(pchNext, "</story>");
																if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																{
																	pchEndRec = pchName;
																	// ok, now pchNext is the beginning, 
																	// and pchEnd is the end of the list,
																	// pchEndRec is the end of the record, and we can use pchName for the fields
																	// we are interested in getting:
																	
																	//unsigned short m_usType;              <type>      // internal colossus type
																	//char* m_pszID;												<clipId>		// the clip ID
																	//char* m_pszTitle;											<title>		  // the title
																	//char* m_pszData;  										<data> 			//must encode zero.
																	//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																	//unsigned char  m_ucSegment;														// not used

																	// following 2 compiled from <startTime>
																	//unsigned long  m_ulOnAirTimeMS;
																	//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																	//unsigned long  m_ulDurationMS;        <dur>        // duration
																	//unsigned short m_usStatus;						<status>     // status (omnibus code)
																	//unsigned short m_usControl;           <timeMode>   // time mode

																	// additionally, up at the list level, we can compile from
																	//  <currentTime>
																	//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																	//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																	//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																	// let's create a new event object;
																	CCAEvent* pEvent = new CCAEvent;


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d pEvent %d\r\n", clock(),pEvent);
		fflush(fp);
		fclose(fp);
	}
}


																	char* pchField = NULL;

																	pchName = strstr(pchNext, "<storyID>");
																	if((pEvent) && (pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																	{

//20151106 2.1.1.15 - added parent pointer assignment to self.  make default behavior, no option.  never referenced before so ignored elsewhere.  needed for Nucleus parameter queries
																		pEvent->m_pParent = pEvent;


																		pchName += strlen("<storyID>");
																		pch = strstr(pchName, "</storyID>");
																		if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																		{
																			*pch = 0;  //null term the ID!
																			pchField = (char*) malloc(strlen(pchName)+1);
																			if(pchField)  // have to at least have this field
																			{
																				strcpy(pchField, pchName);
																				pEvent->m_pszReconcileKey = pchField;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d story ID: %s\r\n", clock(),pEvent->m_pszReconcileKey?pEvent->m_pszReconcileKey:"NULL");
		fflush(fp);
		fclose(fp);
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
// deal with iTX on the first event of the batch

																				if((pConn->m_ulFlags&OMNI_ITX)||(bInitConnection))
//																				if(pConn->m_ulFlags&OMNI_ITX)
																				{
																					if(usRunningEventCount<1)
																					{
																						// delete any preceding things left over, but include done count number of items.

	EnterCriticalSection(&pConn->m_crit);
																						CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread)
																							{
																								int itxidx=0;
																								//int itxcount = pList->GetEventCount();

																								while(itxidx < pList->GetEventCount())
																								{
																									CCAEvent* pitxEvent = pList->GetEvent(itxidx);
																									if(pitxEvent)
																									{
																										// every event gets this to start:
																										pitxEvent->m_ulFlags &= ~OMNI_FLAGS_FOUND; // make sure this is cleared

																										if(strcmp(pitxEvent->m_pszReconcileKey, pEvent->m_pszReconcileKey)==0)
																										{
																											pitxEvent->m_ulFlags |= OMNI_FLAGS_FOUND;

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d iTX search: initial found %d: %s\r\n", clock(), itxidx, pitxEvent->m_pszReconcileKey?pitxEvent->m_pszReconcileKey:"NULL");
		fflush(fp);
		fclose(fp);
	}
}

																											// found it
																											int delidx=-1;
																											if(itxidx>pList->m_usDoneCount)
																											{
																												// items to delete
																												delidx = itxidx - pList->m_usDoneCount - 1; // 0,1,2,3,4, curr at 4, done = 3, delete only index 0 delidx = 4-3-1 = 0
																											}
																											// now count back and set the next pList->m_usDoneCount events to done, then delete the rest off the top.
																											int itxpastidx = itxidx-1;

																											while(itxpastidx>=0)																											
																											{
																												if(itxpastidx <= delidx)
																												{
																													if(!pConn->m_bKillThread)
																													{
																														nReturnCode = pList->DeleteEvent(itxpastidx, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																														if(nReturnCode == OMNI_SUCCESS) itxidx--;// if the delete is successful, need to back up the index after deletion to continue from the right place.

																														if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																														{
																															fp = fopen(pConn->m_pszDebugFile, "at");
																															if(fp)
																															{
																																// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																																//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																																fprintf(fp, "%d DeleteEvent %d (<%d) (iTX Delete) for channel %d returned with %d\r\n", clock(), itxpastidx, delidx, pList->m_usListID, nReturnCode);
																																fflush(fp);
																																fclose(fp);
																															}
																														}
																													}
																												}
																												else  // change status to done
																												{
																													pitxEvent = pList->GetEvent(itxpastidx);
																													if(pitxEvent)
																													{
																														pitxEvent->m_usStatus = C_DONE; //done.

																														nReturnCode = pList->UpdateEvent(pitxEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);
																													
																														pitxEvent->m_ulFlags |= OMNI_FLAGS_FOUND;

																														if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																														{
																															fp = fopen(pConn->m_pszDebugFile, "at");
																															if(fp)
																															{
																																fprintf(fp, "%d UpdateEvent %d %s iTX done\r\n", clock(), itxpastidx, pitxEvent->m_pszReconcileKey);
																																fflush(fp);
																																fclose(fp);
																															}
																														}
																													}
																												}

																												itxpastidx--;
																											}

																											// done with the past events search 
																										}// was the first event matching the received first event.


																									}
																									itxidx++;
																								}
																							}
																						}
	LeaveCriticalSection(&pConn->m_crit);
																					}
																				}

// deal with iTX on the first event of the batch
///////////////////////////////////////////////////////////////////////////////////////////////////////

																				*pch = '<'; // put it back so we can search whole string again.

																				pchName = strstr(pchNext, "<clipId>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<clipId>");
																					pch = strstr(pchName, "</clipId>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszID = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}


																				pchName = strstr(pchNext, "<title>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<title>");
																					pch = strstr(pchName, "</title>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						*pch = 0;  //null term the ID!
																						pchField = (char*) malloc(strlen(pchName)+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, pchName);
																							pEvent->m_pszTitle = pchField;
																							*pch = '<'; // put it back so we can search whole string again.
																						}
																					}
																				}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extract %d %d\r\n", clock(),pConn->m_ppfi,pConn->m_nNumFields );
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																				bool bFieldsFound = false;

																				if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																				{
																					// we found some fields!
																					bFieldsFound = true;	

																					for(int i=0; i<pConn->m_nNumFields; i++)
																					{

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extract %d %d %d\r\n", clock(), i, pConn->m_ppfi[i], pConn );
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						if((pConn->m_ppfi[i])&&(pConn->m_ppfi[i]->m_pszFieldName))
																						{
																							char pszTag[256];
																							sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
					//AfxMessageBox(pszTag);
																							pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																							if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																							{
																								sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																								pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																								if(pConn->m_ppfi[i]->m_pchEndTag)
																								{
																									pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																								}
																								if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																								{
																									int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																									pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																									{
																										memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																										memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																									}
																								}
																							}
																						}
																					}

																				//	Sleep(50);

																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d data extracted\r\n", clock());
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																					//we have to check validity of fields (top-level-ness)
																					bool bAllTopLevel = false;
																					
																					while(!bAllTopLevel)
																					{
																						bool bOverlapFound = false;
																						i=0; 
																						while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																						{
																							for(int j=0; j<pConn->m_nNumFields-1; j++)
																							{
																								if(j!=i)
																								{
																									if(
																											(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																										&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																										)
																									{
																										// i is inside j, need to look for a different one and start over.
																										bOverlapFound = true;

																										char pszTag[256];
																										sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																										pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																										if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																										{
																											sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																											if(pConn->m_ppfi[i]->m_pchEndTag)
																											{
																												pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																											}
																											if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																											{
																												int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																												pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																												if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																												{
																													memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																													memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																												}
																											}
																										}
																										else
																										{
																											if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																											pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																										}
																										break;
																									}

																								}
																							}
																							
																							i++;
																						}

																						if(!bOverlapFound) bAllTopLevel = true;
																					}


																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data validated\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}
																					//assemble the data string
																					int nDataLen = 0;
																					for(i=0; i<pConn->m_nNumFields; i++)
																					{
																						if(pConn->m_ppfi[i]->m_pszFieldInfo)
																						{
																							nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
	//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																						}
																					}

																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data assembled\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																					
																					if(nDataLen)
																					{
																						pchField = (char*) malloc(nDataLen+1);
																						if(pchField)  // have to at least have this field
																						{
																							strcpy(pchField, "");
																							for(i=0; i<pConn->m_nNumFields; i++)
																							{
																								if(pConn->m_ppfi[i]->m_pszFieldInfo)
																								{
																									strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																									free(pConn->m_ppfi[i]->m_pszFieldInfo);
																									pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																								}
																							}
																							pEvent->m_pszData = pchField;
	//																						AfxMessageBox(pEvent->m_pszData );
																						}
																					}

																				}

																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data tagged\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																				if(!bFieldsFound)
																				{
																					// use default, minus tags
																					pchName = strstr(pchNext, "<data>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<data>");
																						pch = strstr(pchName, "</data>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszData = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}
																				}
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d data end\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																				pchName = strstr(pchNext, "<startTime>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<startTime>");
																					pch = strstr(pchName, "</startTime>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						if(pConn->m_ulFlags&OMNI_ITX)
																						{
//	An important value is 621355968000000000.  this is the number of dateTime ticks from 0001 to 1970.  this offset gets you back to unix time

//																							pEvent->m_dblOnAirTimeInternal = (atof(pchName) - 621355968000000000.0)/10000.0;  // now unixtime in seconds.  // this was wrong!!!!  it's milliseconds! 
																							pEvent->m_dblOnAirTimeInternal = (atof(pchName) - 621355968000000000.0)/10000000.0;  // now unixtime in seconds.  // corrected here 2.1.1.18

																							// then multiply by number of frames per second.  Leave it as a non-integer, that is fine.
																							switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																							{
																							case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 25.0;
																								} break;
																							case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 30.0;
																								} break;
																							default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																								{
																									pEvent->m_dblOnAirTimeInternal *= 29.97;
																								}
																								break;
																							}
																							
																						}
																						else
																						{
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);
																						}

																						if(pConn->m_nLookaheadTimeLimitSec > 0)
																						{
																							double dblLimit = (double)(pConn->m_ulConnLastMessage +  pConn->m_nLookaheadTimeLimitSec);
																							switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																							{
																							case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																								{
																									dblLimit *= 25.0;
																								} break;
																							case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																								{
																									dblLimit *= 30.0;
																								} break;
																							default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																								{
																									dblLimit *= 29.97;
																								}
																								break;
																							}

																							if(pEvent->m_dblOnAirTimeInternal > dblLimit)	break;  //past the lookahead, discard the rest.
																						}


																						int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0) pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																						else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																					}
																				}

																				if((!(pConn->m_ulFlags&OMNI_SYSTIME))&&((!pConn->m_bPreventServerTimeFromEvents)||(!bProcessedTime))) //2.1.1.18
//																				if(!(pConn->m_ulFlags&OMNI_SYSTIME))
																				{
																					pchName = strstr(pchNext, "<currentTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<currentTime>");
																						pch = strstr(pchName, "</currentTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																							//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																							pConn->UpdateServerTime(false);
																							bProcessedTime=true;
																						}
																					}
																				}

																				pchName = strstr(pchNext, "<type>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<type>");
																					pch = strstr(pchName, "</type>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usType = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<dur>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<dur>");
																					pch = strstr(pchName, "</dur>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																						if(nMS>=0)
																						{
																							pEvent->m_ulDurationMS = (unsigned long) nMS;

																							if(pConn->m_nLookbehindTimeLimitSec > 0)
																							{
																								double dblLimit = (double)(pConn->m_ulConnLastMessage -  pConn->m_nLookbehindTimeLimitSec);
																								switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																								{
																								case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																									{
																										dblLimit *= 25.0;
																									} break;
																								case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																									{
																										dblLimit *= 30.0;
																									} break;
																								default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																									{
																										dblLimit *= 29.97;
																									}
																									break;
																								}

																								double dblTest = (((double)(pEvent->m_ulDurationMS))/1000.0);
																								switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																								{
																								case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																									{
																										dblTest *= 25.0;
																									} break;
																								case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																									{
																										dblTest *= 30.0;
																									} break;
																								default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																									{
																										dblTest *= 29.97;
																									}
																									break;
																								}

																								if(pEvent->m_dblOnAirTimeInternal + dblTest < dblLimit)
																								{
																									pchNext = strstr(pchEndRec, "<story>");
																								//	usRunningEventCount++;
																									continue;  //in the past , skip and go to next.
																								}
																							}

																						}
																						else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																					}
																				}

																				pchName = strstr(pchNext, "<status>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<status>");
																					pch = strstr(pchName, "</status>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usStatus = (unsigned short)atoi(pchName);
																					}
																				}

																				pchName = strstr(pchNext, "<timeMode>");
																				if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																				{
																					pchName += strlen("<timeMode>");
																					pch = strstr(pchName, "</timeMode>");
																					if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																					{
																						pEvent->m_usControl = (unsigned short)atoi(pchName);
																					}
																				}


																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d updating\r\n", clock());
																											fflush(fp);
																											fclose(fp);
																										}
																									}


	//																			Ok, lets update the event!
																				if(nListIndex>=0)
																				{
																					if(!pConn->m_bKillThread) 
																					{
EnterCriticalSection(&pConn->m_crit);
																						CCAList* pList = pConn->GetList(nListIndex);
																						if(pList)
																						{
																							if(!pConn->m_bKillThread) 
																							{

																								double dblTime = ((double)timestamp.millitm/1000.0);
																								if(pConn->m_bUseUTC)
																								{
																									dblTime += (double)(timestamp.time); // UTC
																								}
																								else
																								{
																									dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																								}

																								if(pConn->m_bUseDST)
																								{
																									dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																								}

																								pEvent->m_dblUpdated = dblTime;  // UTC 
																								 
																								nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);

																								pEvent->m_ulFlags |= OMNI_FLAGS_FOUND;
																							
																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent Sleep %d\r\n", clock(), pConn->m_ulInterEventIntervalMS);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																								
																								Sleep(pConn->m_ulInterEventIntervalMS);   // was zero, but pegged processor

																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateEvent (roReplace) %s for channel %d returned %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																								nReturnCode = pList->UpdateListStatus();

																								if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																								{
																									fp = fopen(pConn->m_pszDebugFile, "at");
																									if(fp)
																									{
																										fprintf(fp, "%d UpdateListStatus %d returned %d\r\n", clock(), pList->m_usListID, nReturnCode);
																										fflush(fp);
																										fclose(fp);
																									}
																								}

																							}
																						}

																						ulCounter++;


																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									fprintf(fp, "%d counter now %d\r\n", clock(),ulCounter);
																									fflush(fp);
																									fclose(fp);
																								}
																							}
/*
																	EnterCriticalSection(&pConn->m_critCounter);
																					if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																					pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
*/
LeaveCriticalSection(&pConn->m_crit);
																					}
																				}
																			}
																		}
																	}
																	//else // cant do anything with this event, so just have to skip.
																}

																pchNext = strstr(pchEndRec, "<story>");
																usRunningEventCount++;


////////////// async database check 2.1.1.18
// here we want to interleave a check of the list, to see if there are any events that need to be committed to the database.
// if so, we only do one, we will interleave them with the items in roReplace.
// we only do this in roReplace since we are expecting many events, could be in the thousands, that take up processing time
 

	EnterCriticalSection(&pConn->m_crit);
																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(pList->m_ulFlags&OMNI_LIST_UNCOMMITTED_EXISTS)  // this ony gets set if pConn->m_nAsyncDatabaseLookaheadSec > 0
																	{
																		pList->CheckDatabaseEvents(-1, 1);// start at last known index, limit items to one.
																	}
																}
	LeaveCriticalSection(&pConn->m_crit);

////////////// async database check

															} // while records exist

///////////////////////////////////////////////////////////////////////////////////////////////////////
// deal with iTX after the last event of the batch

															if((pConn->m_ulFlags&OMNI_ITX)||(bInitConnection))
															//if(pConn->m_ulFlags&OMNI_ITX)
															{
																					// delete any preceding things left over, but include done count number of items.

	EnterCriticalSection(&pConn->m_crit);
																CCAList* pList = pConn->GetList((unsigned short)nListIndex);
																if(pList)
																{
																	if(!pConn->m_bKillThread)
																	{
																		int itxcount = pList->GetEventCount()-1;

																		while( itxcount >= 0 )
																		{
																			CCAEvent* pitxEvent = pList->GetEvent(itxcount);
																			if(pitxEvent)
																			{
																				// every event gets this to start:
																				if(pitxEvent->m_ulFlags&OMNI_FLAGS_FOUND)
																				{
																					// was in the received data, so just clear the flag.
																					pitxEvent->m_ulFlags &= ~OMNI_FLAGS_FOUND;
																				}
																				else
																				{
																					// wasn't in the received data, so must be deleted.
																					if(!pConn->m_bKillThread)
																					{
																						nReturnCode = pList->DeleteEvent(itxcount, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);

																						if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																						{
																							fp = fopen(pConn->m_pszDebugFile, "at");
																							if(fp)
																							{
																								// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																								//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																								fprintf(fp, "%d DeleteEvent %d (iTX Delete) for channel %d returned with %d\r\n", clock(), itxcount, pList->m_usListID, nReturnCode);
																								fflush(fp);
																								fclose(fp);
																							}
																						}
																					}
																				}
																			}
																			itxcount--;
																		}
																	}
																}

	LeaveCriticalSection(&pConn->m_crit);
															}


// deal with iTX after the last event of the batch
///////////////////////////////////////////////////////////////////////////////////////////////////////
														}
													}
												}
											}
											else  // not roReplace
											{
												pchName = strstr(pchXML, "<roStoryReplace>");
												if(pchName)
												{
													// it's a replace single item record
													pchEnd = strstr(pchName, "</roStoryReplace>");
													if(pchEnd)  // we found a correctly encapsulated record.
													{
														pchNext = strstr(pchName, "<roID>");
														if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
														{
															pchNext += strlen("<roID>");
															usListID = atoi(pchNext);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roStoryReplace\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}


															int nListIndex = OMNI_SUCCESS;
															// add or update the list!
/*
															if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
															{
																EnterCriticalSection(&g_actives.m_critActive);
												//				AfxMessageBox("roStoryReplace, about to enter channel active check");
																nListIndex = g_actives.IsChannelActive(usListID);
												//				AfxMessageBox("roStoryReplace, left channel active check");
																LeaveCriticalSection(&g_actives.m_critActive);
															}
*/
															if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
															{ 
	EnterCriticalSection (&pConn->m_crit);
																nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
															}

															if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
															{


																// then, deal with the events we have.
																pchNext = strstr(pchName, "<story>");
																while((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))  // this while should only get hit once.
																{
																	pchName = strstr(pchNext, "</story>");
																	if((pchName)&&(pchName<pchEnd)&&(!pConn->m_bKillThread))  // we found a correctly encapsulated record.
																	{
																		char* pchEndRec = pchName;
																		// ok, now pchNext is the beginning, 
																		// and pchEnd is the end of the list,
																		// pchEndRec is the end of the record, and we can use pchName for the fields
																		// we are interested in getting:
																		
																		//unsigned short m_usType;              <type>      // internal colossus type
																		//char* m_pszID;												<clipId>		// the clip ID
																		//char* m_pszTitle;											<title>		  // the title
																		//char* m_pszData;  										<data> 			//must encode zero.
																		//char* m_pszReconcileKey;							<storyID>				// the "story ID"
																		//unsigned char  m_ucSegment;														// not used

																		// following 2 compiled from <startTime>
																		//unsigned long  m_ulOnAirTimeMS;
																		//unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900

																		//unsigned long  m_ulDurationMS;        <dur>        // duration
																		//unsigned short m_usStatus;						<status>     // status (omnibus code)
																		//unsigned short m_usControl;           <timeMode>   // time mode

																		// additionally, up at the list level, we can compile from
																		//  <currentTime>
																		//unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
																		//unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
																		//unsigned long  m_ulRefUnixTime;		// compiled from "current time".


																		// let's create a new event object;
																		CCAEvent* pEvent = new CCAEvent;
																		char* pchField;

																		pchName = strstr(pchNext, "<storyID>");
																		if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																		{

//20151106 2.1.1.15 - added parent pointer ssignment to self.  make default behavior, no option.  never referenced before so ignored elsewhere.  needed for Nucleus parameter queries
																			pEvent->m_pParent = pEvent;

																			pchName += strlen("<storyID>");
																			pch = strstr(pchName, "</storyID>");
																			if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																			{
																				*pch = 0;  //null term the ID!
																				pchField = (char*) malloc(strlen(pchName)+1);
																				if(pchField)  // have to at least have this field
																				{
																					strcpy(pchField, pchName);
																					pEvent->m_pszReconcileKey = pchField;
																					*pch = '<'; // put it back so we can search whole string again.

																					pchName = strstr(pchNext, "<clipId>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<clipId>");
																						pch = strstr(pchName, "</clipId>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszID = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}


																					pchName = strstr(pchNext, "<title>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<title>");
																						pch = strstr(pchName, "</title>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							*pch = 0;  //null term the ID!
																							pchField = (char*) malloc(strlen(pchName)+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, pchName);
																								pEvent->m_pszTitle = pchField;
																								*pch = '<'; // put it back so we can search whole string again.
																							}
																						}
																					}

	///////////////////////////////////////////////////////////////////
	// here we have to extract and assemble the data fields that are sub to the mosPayLoad.
	// we have to enumerate the fields desired, passed in in m_pszDataFields
	// then, we have to find which tags come first, extract the info in order, assemble a string.
	// if m_pszDataFields is null, we just use the default <data>.  If we use this default, we dont include the <data></data> tags


																					bool bFieldsFound = false;

																					if((pConn->m_ppfi)&&(pConn->m_nNumFields>0))
																					{
																						// we found some fields!
																						bFieldsFound = true;	

																						for(int i=0; i<pConn->m_nNumFields; i++)
																						{

																							if(pConn->m_ppfi[i]->m_pszFieldName)
																							{
																								char pszTag[256];
																								sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
						//AfxMessageBox(pszTag);
																								pConn->m_ppfi[i]->m_pchStartTag = strstr(pchNext, pszTag);
																								if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																								{
																									sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																									pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																									if(pConn->m_ppfi[i]->m_pchEndTag)
																									{
																										pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																									}
																									if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																									{
																										int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																										pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																										if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																										{
																											memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																											memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																										}
																									}
																								}
																							}
																						}


																						//we have to check validity of fields (top-level-ness)
																						bool bAllTopLevel = false;
																						
																						while(!bAllTopLevel)
																						{
																							bool bOverlapFound = false;
																							i=0; 
																							while((i<pConn->m_nNumFields-1)&&(!bOverlapFound))
																							{
																								for(int j=0; j<pConn->m_nNumFields-1; j++)
																								{
																									if(j!=i)
																									{
																										if(
																												(pConn->m_ppfi[i]->m_pchStartTag > pConn->m_ppfi[j]->m_pchStartTag)
																											&&(pConn->m_ppfi[i]->m_pchEndTag < pConn->m_ppfi[j]->m_pchEndTag)
																											)
																										{
																											// i is inside j, need to look for a different one and start over.
																											bOverlapFound = true;

																											char pszTag[256];
																											sprintf(pszTag,"<%s>", pConn->m_ppfi[i]->m_pszFieldName);
																											pConn->m_ppfi[i]->m_pchStartTag = strstr(pConn->m_ppfi[j]->m_pchEndTag, pszTag);  // start looking outside the top level object we were in
																											if((pConn->m_ppfi[i]->m_pchStartTag)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchStartTag<pchEndRec))
																											{
																												sprintf(pszTag,"</%s>", pConn->m_ppfi[i]->m_pszFieldName);
																												pConn->m_ppfi[i]->m_pchEndTag = strstr(pConn->m_ppfi[i]->m_pchStartTag, pszTag);
																												if(pConn->m_ppfi[i]->m_pchEndTag)
																												{
																													pConn->m_ppfi[i]->m_pchEndTag += strlen(pszTag);
																												}
																												if((pConn->m_ppfi[i]->m_pchEndTag)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEnd)&&(pConn->m_ppfi[i]->m_pchEndTag<pchEndRec))
																												{
																													int nLen = pConn->m_ppfi[i]->m_pchEndTag - pConn->m_ppfi[i]->m_pchStartTag;
																													if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo); // free the old one
																													pConn->m_ppfi[i]->m_pszFieldInfo = (char*) malloc(nLen+1);
																													if(pConn->m_ppfi[i]->m_pszFieldInfo)  // have to at least have this field
																													{
																														memcpy(pConn->m_ppfi[i]->m_pszFieldInfo, pConn->m_ppfi[i]->m_pchStartTag, nLen);
																														memset(pConn->m_ppfi[i]->m_pszFieldInfo+nLen, 0, 1); // null term
																													}
																												}
																											}
																											else
																											{
																												if(pConn->m_ppfi[i]->m_pszFieldInfo) free(pConn->m_ppfi[i]->m_pszFieldInfo);
																												pConn->m_ppfi[i]->m_pszFieldInfo = NULL;
																											}
																											break;
																										}

																									}
																								}
																								
																								i++;
																							}

																							if(!bOverlapFound) bAllTopLevel = true;
																						}

																						//assemble the data string
																						int nDataLen = 0;
																						for(i=0; i<pConn->m_nNumFields; i++)
																						{
																							if(pConn->m_ppfi[i]->m_pszFieldInfo)
																							{
																								nDataLen+= strlen(pConn->m_ppfi[i]->m_pszFieldInfo);
		//																							AfxMessageBox(pConn->m_ppfi[i]->m_pszFieldInfo);
																							}
																						}
																						
																						if(nDataLen)
																						{
																							pchField = (char*) malloc(nDataLen+1);
																							if(pchField)  // have to at least have this field
																							{
																								strcpy(pchField, "");
																								for(i=0; i<pConn->m_nNumFields; i++)
																								{
																									if(pConn->m_ppfi[i]->m_pszFieldInfo)
																									{
																										strcat(pchField, pConn->m_ppfi[i]->m_pszFieldInfo);
																										free(pConn->m_ppfi[i]->m_pszFieldInfo);
																										pConn->m_ppfi[i]->m_pszFieldInfo = NULL;

																									}
																								}
																								pEvent->m_pszData = pchField;
		//																						AfxMessageBox(pEvent->m_pszData );
																							}
																						}

																					}


																					if(!bFieldsFound)
																					{
																						// use default, minus tags
																						pchName = strstr(pchNext, "<data>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<data>");
																							pch = strstr(pchName, "</data>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
																								*pch = 0;  //null term the ID!
																								pchField = (char*) malloc(strlen(pchName)+1);
																								if(pchField)  // have to at least have this field
																								{
																									strcpy(pchField, pchName);
																									pEvent->m_pszData = pchField;
																									*pch = '<'; // put it back so we can search whole string again.
																								}
																							}
																						}
																					}


	///////////////////////////////////////////////////////////////////////////////
	//  that's the end of the data section.

																					pchName = strstr(pchNext, "<startTime>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<startTime>");
																						pch = strstr(pchName, "</startTime>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							// no need to deal with iTX here, since iTX does not send this command.
																							pEvent->m_dblOnAirTimeInternal = atof(pchName);

																							if(pConn->m_nLookaheadTimeLimitSec > 0)
																							{
																								double dblLimit = (double)(pConn->m_ulConnLastMessage +  pConn->m_nLookaheadTimeLimitSec);
																								switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																								{
																								case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																									{
																										dblLimit *= 25.0;
																									} break;
																								case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																									{
																										dblLimit *= 30.0;
																									} break;
																								default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																									{
																										dblLimit *= 29.97;
																									}
																									break;
																								}

																								if(pEvent->m_dblOnAirTimeInternal > dblLimit)	break;  //past the lookahead, discard the rest.
																							}

																							int nMS = pConn->ConvertTime(pchName, NULL, &(pEvent->m_usOnAirJulianDate), &(pEvent->m_dblOnAirTime));  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0)	pEvent->m_ulOnAirTimeMS = (unsigned long) nMS;
																							else pEvent->m_ulOnAirTimeMS = TIME_NOT_DEFINED;
																						}
																					}

																				if((!(pConn->m_ulFlags&OMNI_SYSTIME))&&((!pConn->m_bPreventServerTimeFromEvents)||(!bProcessedTime))) //2.1.1.18
	//																				if(!(pConn->m_ulFlags&OMNI_SYSTIME))
																					{
																						pchName = strstr(pchNext, "<currentTime>");
																						if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																						{
																							pchName += strlen("<currentTime>");
																							pch = strstr(pchName, "</currentTime>");
																							if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																							{
																								int nMS = pConn->ConvertTime(pchName, &(pConn->m_ulRefUnixTime), &(pConn->m_usRefJulianDate));  // returns milliseconds in current day, -1 if error.
																								if(nMS>=0) pConn->m_ulRefTimeMS = (unsigned long) nMS;
																								//else pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;  // dont reset server time
																								pConn->UpdateServerTime(false);
																								bProcessedTime = true;
																							}
																						}
																					}

																					pchName = strstr(pchNext, "<type>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<type>");
																						pch = strstr(pchName, "</type>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usType = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<dur>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<dur>");
																						pch = strstr(pchName, "</dur>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							int nMS = pConn->ConvertTime(pchName, NULL, NULL);  // returns milliseconds in current day, -1 if error.
																							if(nMS>=0)
																							{
																								pEvent->m_ulDurationMS = (unsigned long) nMS;

																								if(pConn->m_nLookbehindTimeLimitSec > 0)
																								{
																									double dblLimit = (double)(pConn->m_ulConnLastMessage -  pConn->m_nLookbehindTimeLimitSec);
																									switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																									{
																									case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																										{
																											dblLimit *= 25.0;
																										} break;
																									case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																										{
																											dblLimit *= 30.0;
																										} break;
																									default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																										{
																											dblLimit *= 29.97;
																										}
																										break;
																									}

																									double dblTest = (((double)(pEvent->m_ulDurationMS))/1000.0);
																									switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
																									{
																									case OMNI_PAL://											0x00000010  // PAL	(25 fps)
																										{
																											dblTest *= 25.0;
																										} break;
																									case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
																										{
																											dblTest *= 30.0;
																										} break;
																									default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
																										{
																											dblTest *= 29.97;
																										}
																										break;
																									}


																									if(pEvent->m_dblOnAirTimeInternal + dblTest < dblLimit)
																									{
																										pchNext = strstr(pchEndRec, "<story>");
	//																									break;  //in the past, skip and go to next.
																										continue;  //in the past , skip and go to next.
																									}
																								}
																							}
																							else pEvent->m_ulDurationMS = TIME_NOT_DEFINED;
																						}
																					}

																					pchName = strstr(pchNext, "<status>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<status>");
																						pch = strstr(pchName, "</status>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usStatus = (unsigned short)atoi(pchName);
																						}
																					}

																					pchName = strstr(pchNext, "<timeMode>");
																					if((pchName)&&(pchName<pchEnd)&&(pchName<pchEndRec))
																					{
																						pchName += strlen("<timeMode>");
																						pch = strstr(pchName, "</timeMode>");
																						if((pch)&&(pch<pchEnd)&&(pchName<pchEndRec))
																						{
																							pEvent->m_usControl = (unsigned short)atoi(pchName);
																						}
																					}

		//																			Ok, lets update the event!
																					if(nListIndex>=0)
																					{

																						if(!pConn->m_bKillThread) 
																						{
EnterCriticalSection(&pConn->m_crit);
																							CCAList* pList = pConn->GetList(nListIndex);
																							if(pList)
																							{
																								if(!pConn->m_bKillThread) 
																								{
																									double dblTime = ((double)timestamp.millitm/1000.0);
																									if(pConn->m_bUseUTC)
																									{
																										dblTime += (double)(timestamp.time); // UTC
																									}
																									else
																									{
																										dblTime += (double)(timestamp.time - (timestamp.timezone*60));  // local time
																									}

																									if(pConn->m_bUseDST)
																									{
																										dblTime += (double)(timestamp.dstflag?3600:0);  // DST
																									}
																									pEvent->m_dblUpdated = dblTime;


																									nReturnCode = pList->UpdateEvent(pEvent, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);

																									Sleep(pConn->m_ulInterEventIntervalMS);  // was zero, but pegged processor
																									
																									if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																									{
																										fp = fopen(pConn->m_pszDebugFile, "at");
																										if(fp)
																										{
																											fprintf(fp, "%d UpdateEvent (roStoryReplace) %s for channel %d returned %d\r\n", clock(), pEvent->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																											fflush(fp);
																											fclose(fp);
																										}
																									}

																									nReturnCode = pList->UpdateListStatus();

																								}
																							}

																							ulCounter++;

/*
																	EnterCriticalSection(&pConn->m_critCounter);
																						if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																						pConn->m_ulCounter++;
																	LeaveCriticalSection(&pConn->m_critCounter);
*/
LeaveCriticalSection(&pConn->m_crit);
																						}
																					}
																				}
																			}
																		}
																		//else // cant do anything with this event, so just have to skip.
																	}

																	pchNext = strstr(pchEnd, "<story>");
																}
															}
														} //if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
													}
												} 
												else
												if(!pConn->m_bKillThread)
												{
													pchName = strstr(pchXML, "<roStoryDelete>");
													if(pchName)
													{
															// it's a delete record
														pchEnd = strstr(pchName, "</roStoryDelete>");
														if(pchEnd)  // we found a correctly encapsulated record.
														{
															pchNext = strstr(pchName, "<roID>");
															if((pchNext)&&(pchNext<pchEnd))
															{
																pchNext += strlen("<roID>");
																usListID = atoi(pchNext);

if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d found %s in roStoryDelete\r\n",clock(), pchNext);
		fflush(fp);
		fclose(fp);
	}
}

																int nListIndex = OMNI_SUCCESS;
/*
																if((!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
																{
																	EnterCriticalSection(&g_actives.m_critActive);
													//				AfxMessageBox("roStoryDelete, about to enter channel active check");
																	nListIndex = g_actives.IsChannelActive(usListID);
												//				//	AfxMessageBox("roStoryDelete, left channel active check");
																	LeaveCriticalSection(&g_actives.m_critActive);
																}
*/
																if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
																{ 
	EnterCriticalSection (&pConn->m_crit);
																	nListIndex = pConn->ReturnListIndex(usListID);
	LeaveCriticalSection (&pConn->m_crit);
																}

																if((nListIndex>OMNI_ERROR)&&(!pConn->m_bKillThread))
																{
																	//pConn->m_ppList[i]->UpdateEvent(pEvent);

																	// then, find the story ID to delete
																	pchNext = strstr(pchName, "<storyID>");
																	if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))
																	{
																		pchNext += strlen("<storyID>");
																		pch = strstr(pchNext, "</storyID>");
																		if((pch)&&(pch<pchEnd))
																		{
																			*pch = 0;  //null term the ID!
																			if(!pConn->m_bKillThread) 
																			{
EnterCriticalSection(&pConn->m_crit);
																			
																				CCAList* pList = pConn->GetList(nListIndex);
																				if(pList)
																				{
				EnterCriticalSection(&pList->m_critEvents);
																					int j = pList->GetEventIndexByRecKey(pchNext);
				LeaveCriticalSection(&pList->m_critEvents);
																					if(j>=0)
																					{
																						if(!pConn->m_bKillThread)
																						{
																							nReturnCode = pList->DeleteEvent(j, pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable);
																							if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
																							{
																								fp = fopen(pConn->m_pszDebugFile, "at");
																								if(fp)
																								{
																									// cant reference pList->GetEvent((unsigned short) j), we just deleted it!
																									//fprintf(fp, "DeleteEvent (roStoryDelete) %s for channel %d returned with %d\r\n", pList->GetEvent((unsigned short) j)->m_pszReconcileKey, pList->m_usListID, nReturnCode);
																									fprintf(fp, "%d DeleteEvent (roStoryDelete) for channel %d returned with %d\r\n", clock(), pList->m_usListID, nReturnCode);
																									fflush(fp);
																									fclose(fp);
																								}
																							}

																						}

																						ulCounter++;
	/*
																		EnterCriticalSection(&pConn->m_critCounter);
																						if(pConn->m_ulCounter >= ULONG_MAX) pConn->m_ulCounter=1;
																						pConn->m_ulCounter++;
																		LeaveCriticalSection(&pConn->m_critCounter);
	*/
																					}
																				}
LeaveCriticalSection(&pConn->m_crit);
																			}
																		}
																	}
																}
															}
														}//if((pchNext)&&(pchNext<pchEnd)&&(!pConn->m_bKillThread))


													}  // else it's an unrecognized mos command, or time to die
												}
											}


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d An output message has been processed. %d\r\n", clock(), ulCounter);
		fflush(fp);
		fclose(fp);

	}
}


										}

										EnterCriticalSection(&pConn->m_critCounter);
														if(ULONG_MAX - pConn->m_ulCounter < ulCounter )
														{
															ulCounter -= (ULONG_MAX - pConn->m_ulCounter);
															pConn->m_ulCounter = ulCounter;
														}
														else	pConn->m_ulCounter += ulCounter;
										LeaveCriticalSection(&pConn->m_critCounter);


if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
{
	fp = fopen(pConn->m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d free [%d]\r\n", clock(), pchXML);
		fflush(fp);
		fclose(fp);

	}
}

// this is not a thread, it is called by a thread we do not wnat to end
//	Sleep(50);
//	_endthread();
//	Sleep(10);

										
}
