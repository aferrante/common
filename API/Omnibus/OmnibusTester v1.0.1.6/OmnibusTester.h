// OmnibusTester.h : main header file for the OMNIBUSTESTER application
//

#if !defined(AFX_OMNIBUSTESTER_H__97D65725_CC2E_430C_A7F4_B9A1099D08D1__INCLUDED_)
#define AFX_OMNIBUSTESTER_H__97D65725_CC2E_430C_A7F4_B9A1099D08D1__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// COmnibusTesterApp:
// See OmnibusTester.cpp for the implementation of this class
//

class COmnibusTesterApp : public CWinApp
{
public:
	COmnibusTesterApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COmnibusTesterApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(COmnibusTesterApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OMNIBUSTESTER_H__97D65725_CC2E_430C_A7F4_B9A1099D08D1__INCLUDED_)
