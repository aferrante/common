// OmniParse.cpp: implementation of the COmniParse class.
//
//////////////////////////////////////////////////////////////////////

#include "OmniParse.h"
#include <sys/timeb.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COmniParse::COmniParse()
{
	m_pList = NULL;
	m_ppfi = NULL;
	m_nNumFields = 0;
	m_szChannelID = NULL;
	m_ulFlags = OMNI_NTSC;
}

COmniParse::~COmniParse()
{
	if(m_pList)
	{
		delete m_pList;
		m_pList = NULL;
	}

		// clean up
	if(m_ppfi)
	{
		for(int i=0; i<m_nNumFields; i++)
		{
			if(m_ppfi[i])
			{
				delete m_ppfi[i];
			}
		}
		delete [] m_ppfi;
	}
	m_ppfi = NULL;
	m_nNumFields=0;

	if(m_szChannelID) free(m_szChannelID);
	m_szChannelID = NULL;
}

int COmniParse::ClearList()  // does not clear datafields
{
	if(m_pList)
	{
		delete m_pList;
		m_pList = NULL;
	}
	if(m_szChannelID) free(m_szChannelID);
	return OMNI_SUCCESS;
}	

int COmniParse::SetDataFields(char* pszDataFields)  // clear with valid, zero length buffer
{
	if(pszDataFields == NULL) return OMNI_ERROR;

	if(m_ppfi)
	{
		for(int i=0; i<m_nNumFields; i++)
		{
			if(m_ppfi[i])
			{
				delete m_ppfi[i];
			}
		}
		delete [] m_ppfi;
	}

	m_nNumFields = 0;
	m_ppfi = NULL;  // dynamic array of field info.

	if(strlen(pszDataFields))  // a comma separated list of tags inside the OSC file that will be assembled and put into the m_pszData member of the event items
	{
//			AfxMessageBox(pszDataFields);
																		
		// first split up the pConn->m_pszDataFields into the tag names
		CSafeBufferUtil sbu;

		char* pchField = sbu.Token(pszDataFields, strlen(pszDataFields), "><, ");
		while(pchField)
		{
			if(strlen(pchField))
			{
				CCAEventData* pfi = new CCAEventData;
				if(pfi)
				{
					CCAEventData** ppnewfi = new CCAEventData*[m_nNumFields+1];
					if(ppnewfi)
					{
						if(m_ppfi)
						{
							for(int i=0; i<m_nNumFields; i++)
							{
								ppnewfi[i] = m_ppfi[i];
							}
							delete [] m_ppfi;
						}

						m_ppfi = ppnewfi;

//			AfxMessageBox(pch);
						//pfi->m_pszFieldName = pchField;  
						pfi->m_pszFieldName = (char*)malloc(strlen(pchField)+1);
						if(pfi->m_pszFieldName) strcpy(pfi->m_pszFieldName, pchField);  
//				AfxMessageBox(m_ppfi[m_nNumFields]->m_pszFieldName);

						m_ppfi[m_nNumFields] = pfi;
						m_nNumFields++;

					}
				}
			}

			//get the next one.
//				AfxMessageBox(m_ppfi[m_nNumFields-1]->m_pszFieldName);
			pchField = sbu.Token(NULL, NULL, "><, ");
//				AfxMessageBox(m_ppfi[m_nNumFields-1]->m_pszFieldName);

/*
			for(int i=0; i<m_nNumFields; i++)
			{
				char fn[256];
				sprintf(fn, "%d: 0x%08x: %s", i, m_ppfi[i]->m_pszFieldName, m_ppfi[i]->m_pszFieldName);
				AfxMessageBox(fn);
			}
*/			
		}
	}
	return OMNI_SUCCESS;
}

int COmniParse::LoadFile(char* pszFilename)  // full path
{
	if((pszFilename == NULL)||(strlen(pszFilename)<=0)) return OMNI_ERROR;
	FILE* fp = fopen(pszFilename, "rb");
	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero
			 fclose(fp);

			char* pch = NULL;
			char key[256];
			int nNumEvents = 0;

			ClearList();

			// need to find channel id.
			int nChID = -1;

			sprintf(key, "%cchannel_id", 10);
			pch = strstr(pchFile, key);
			if(pch)
			{
				pch += 11;  // length of <10>channel_id
				m_szChannelID = (char*)malloc(256);

				if(m_szChannelID)
				{
					while(isspace(*pch)) pch++;
					int nch =0;
					while( (*pch != 10) && (*pch != 13) && (nch<255) )
					{
						*(m_szChannelID + nch) = *pch;
						nch++;
						pch++;
					}
					*(m_szChannelID + nch) = 0; //zero term
					nChID = atoi(m_szChannelID);
				}
			}

			m_pList = new CCAList(pszFilename, nChID<1?1:(unsigned short)nChID);
			if(m_pList == NULL)
			{
				free(pchFile);
				return OMNI_ERROR;
			}

			m_pList->SetFrameBasis(m_ulFlags);  // this must be set outside ahead of time

			sprintf(key, "ITEM");
			pch = strstr(pchFile, key);
			while(pch)
			{
				char* pchEnd = NULL;
				char* pchSearch = pch;

				while(!pchEnd)
				{
					sprintf(key, "%cend", 10);
					pchEnd = strstr(pchSearch, key);
					if(pchEnd == NULL) break;
					if( (*(pchEnd+4)!=10)&&(*(pchEnd+4)!=13) )
					{
						pchSearch = pchEnd+4;
						pchEnd = NULL;  // in case it's not really the end tag
					}
				}

				if(pchEnd)
				{
					sprintf(key, "%cITEM", 10);
					char* pchNew = strstr(pch+4, key);  //+4 to skip the current opener
					if(pchNew) // there is another item
					{
						if(pchEnd>pchNew) // current item is not terminated properly.
						{
							pch = pchNew;  // skip the one that wasnt terminated.
						}
					}

					//pch is the start, pchEnd is the end.
					CCAEvent* pEvent = new CCAEvent;
					if(pEvent)
					{
						pchNew = (char*)malloc(36);
						if(pchNew)
						{
							pEvent->m_pszReconcileKey = pchNew;
							sprintf(pEvent->m_pszReconcileKey, "%05d", nNumEvents+1);
						}
						
						pchNew = GetEventField(pch, pchEnd, "clip");
						if(pchNew)
						{
							pEvent->m_pszID = pchNew;
						}

						pchNew = GetEventField(pch, pchEnd, "title");
						if(pchNew)
						{
							pEvent->m_pszTitle = pchNew;
						}

						pchNew = GetEventField(pch, pchEnd, "type");
						if(pchNew)
						{
							pEvent->m_usType = (unsigned short)atol(pchNew);
							free(pchNew);
						}

						pchNew = GetEventField(pch, pchEnd, "mode");
						if(pchNew)
						{
							pEvent->m_usControl = (unsigned short)atol(pchNew);
							free(pchNew);
						}

						int nDuration=-1;
						pchNew = GetEventField(pch, pchEnd, "out_src");
						if(pchNew)
						{
							nDuration = (atoi(pchNew)*1000)/(((m_ulFlags&OMNI_FRAMEBASISMASK)==OMNI_PAL)?25:30);
							free(pchNew);
							pchNew = GetEventField(pch, pchEnd, "in_src");
							if(pchNew)
							{
								nDuration -= (atoi(pchNew)*1000)/(((m_ulFlags&OMNI_FRAMEBASISMASK)==OMNI_PAL)?25:30);
								free(pchNew);
							}
						}
						else
						{
							pchNew = GetEventField(pch, pchEnd, "tc_out");
							if(pchNew)
							{
								nDuration = 0;
								char* pchNum = pchNew;
								char* pchDelim = strchr(pchNum, ':');
								int nField = 0;
								while(pchDelim)
								{
									*pchDelim =0;
									
									switch(nField)
									{
									case 0: nDuration += atoi(pchNum)*(3600000); break;
									case 1: nDuration += atoi(pchNum)*(60000); break;
									case 2: nDuration += atoi(pchNum)*(1000); break;
									}

									nField++;
									pchNum = pchDelim+1;
									pchDelim = strchr(pchNum, ':');
								}
								nDuration += (atoi(pchNum)*1000)/(((m_ulFlags&OMNI_FRAMEBASISMASK)==OMNI_PAL)?25:30);

								free(pchNew);
								pchNew = GetEventField(pch, pchEnd, "tc_in");
								if(pchNew)
								{
									char* pchNum = pchNew;
									char* pchDelim = strchr(pchNum, ':');
									nField = 0;
									while(pchDelim)
									{
										*pchDelim =0;
										
										switch(nField)
										{
										case 0: nDuration -= atoi(pchNum)*(3600000); break;
										case 1: nDuration -= atoi(pchNum)*(60000); break;
										case 2: nDuration -= atoi(pchNum)*(1000); break;
										}

										nField++;
										pchNum = pchDelim+1;
										pchDelim = strchr(pchNum, ':');
									}
									nDuration -= (atoi(pchNum)*1000)/(((m_ulFlags&OMNI_FRAMEBASISMASK)==OMNI_PAL)?25:30);
									free(pchNew);
								}
							}
						}

						if(nDuration>=0)
						{
							pEvent->m_ulDurationMS = (unsigned long)nDuration;
						}

						// in this case we are going to set up the preset time string in the
						// m_pContextData member (later we can calc the time for real)

						pEvent->m_pContextData = (char*)malloc(256);
						if(pEvent->m_pContextData)
						{
							strcpy((char*)pEvent->m_pContextData, "");

							pchNew = GetEventField(pch, pchEnd, "preset_date");
							if(pchNew)
							{
								char ch=' ';
								switch(pEvent->m_usControl)
								{
								case 0: ch = 'M'; break;
								case 1: ch = 'A'; break;
								case 2: ch = '='; break;
								case 3: ch = '+'; break;
								case 4: ch = '-'; break;
								case 5: ch = 'F'; break;
								default: ch = ' '; break;
								}
								if((strcmp(pchNew, "01-01-1970")==0)||(ch=='+')||(ch=='-'))
								{
									sprintf((char*)pEvent->m_pContextData, "         %c ", ch);
								}
								else
								{
									sprintf((char*)pEvent->m_pContextData, "%s ", pchNew);
								}
								free(pchNew);
							}
							pchNew = GetEventField(pch, pchEnd, "preset_time");
							if(pchNew)
							{
								strcat((char*)pEvent->m_pContextData, pchNew);
								free(pchNew);
							}
						}

						// ok now the hard part.
						// assemble the data member

						if((m_nNumFields>0)&&(m_ppfi))
						{
							int n=0;
							while(n<m_nNumFields)
							{
								if((m_ppfi[n])&&(m_ppfi[n]->m_pszFieldName)&&(strlen(m_ppfi[n]->m_pszFieldName)>0))
								{
									pchNew = GetEventField(pch, pchEnd, m_ppfi[n]->m_pszFieldName);
									if(pchNew)
									{
										char* pchData = NULL;
										if(pEvent->m_pszData)
										{
											pchData = (char*)malloc(strlen(pEvent->m_pszData)+(strlen(m_ppfi[n]->m_pszFieldName)*2)+6+strlen(pchNew));
											if(pchData)
											{
												sprintf(pchData, "%s<%s>%s</%s>", pEvent->m_pszData, m_ppfi[n]->m_pszFieldName, pchNew, m_ppfi[n]->m_pszFieldName);
												free(pEvent->m_pszData);
												pEvent->m_pszData = pchData;
											}
										}
										else
										{
											pchData = (char*)malloc((strlen(m_ppfi[n]->m_pszFieldName)*2)+6+strlen(pchNew));
											sprintf(pchData, "<%s>%s</%s>", m_ppfi[n]->m_pszFieldName, pchNew, m_ppfi[n]->m_pszFieldName);
											pEvent->m_pszData = pchData;
										}

										free(pchNew);
									}
								}
								n++;
							}
						}
/*
	unsigned short m_usType;  // internal colossus type
	char* m_pszID;    // the clip ID
	char* m_pszTitle;
	char* m_pszData;  //must encode zero.
	char* m_pszReconcileKey;       // the "story ID"
	unsigned char  m_ucSegment;    // not used
	unsigned long  m_ulOnAirTimeMS;
	unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
	unsigned long  m_ulDurationMS;
	unsigned short m_usStatus;  // status (omnibus code)
	unsigned short m_usControl; // time mode

	double m_dblOnAirTimeInternal;  // for omnibus, this is frames since jan 1970
	double m_dblUpdated;            // the last time the system updated the event. format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
*/


						if(m_pList->UpdateEvent(pEvent)>=OMNI_SUCCESS)
						{
							nNumEvents++;
							m_pList->UpdateListStatus();
						}
					}

				}
				sprintf(key, "%cITEM", 10);
				pch = strstr(pch+4, key);
			}

			free(pchFile);
		}
		else fclose(fp);
	}
	else return OMNI_ERROR;

	return OMNI_SUCCESS;
}

char* COmniParse::GetEventField(char* pchEventStart, char* pchEventEnd, char* szKey)
{
	if((pchEventStart==NULL)||(pchEventEnd==NULL)||(szKey==NULL)||(strlen(szKey)<=0)) return NULL;

	char key[36];
	_snprintf(key, 32, "%c%s", 10, szKey);
	char* pch = strstr(pchEventStart, key);
	if((pch)&&(pch<pchEventEnd))  // has to exist within the event bounds
	{
		pch += strlen(key);
		char* pchReturn = (char*)malloc(256);
		if(pchReturn)
		{
			while(isspace(*pch)) pch++;
			int nch =0;
			while( (*pch != 10) && (*pch != 13) && (nch<255) )
			{
				*(pchReturn + nch) = *pch;
				nch++;
				pch++;
			}
			*(pchReturn + nch) = 0; //zero term
			return pchReturn;
		}
	}
	return NULL;
}

// utility
 // returns milliseconds in current day, -1 if error.
int COmniParse::ConvertTime(char* pszTimeString, unsigned long* pulRefUnixTime, unsigned short* pusJulianDate)  // returns milliseconds in current day, -1 if error.
{
	if((pszTimeString)&&(strlen(pszTimeString)))
	{
		int nMillisecondsInDay=-1;

		double dblFrames = atof(pszTimeString);  // frames since January 1970.  (I think it means jan 1, 12:00 am (local), but this is how it is in the omnibus documentation)
		double dblFrameBasis = 29.97;
		switch(m_ulFlags&OMNI_FRAMEBASISMASK)
		{
		case OMNI_PAL://											0x00000010  // PAL	(25 fps)
			{
				dblFrameBasis = 25.0;
			} break;
		case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
			{
				dblFrameBasis = 30.0;
			} break;
		default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
			break;
		}

		double dblMilliseconds = (dblFrames*1000.0/dblFrameBasis);
		unsigned short usJulianDate=25567;  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
		while (dblMilliseconds>86400000.0)
		{
			dblMilliseconds-=86400000.0; // need to get the remainder.
			usJulianDate++;  // add up days
		}

		nMillisecondsInDay = (int)(dblMilliseconds);

		//let's get the unixtime (though, not GMT offset, local)
		if(pulRefUnixTime)
		{
			unsigned long  ulRefUnixTime=0;
			double dblSeconds = (dblFrames/dblFrameBasis);
			if(dblSeconds<0.0) 
				ulRefUnixTime = 0;
			else
			{
				while (dblSeconds>(double)0xffffffff) dblSeconds-=(double)0xffffffff; // epoch maxed by size of unsigned long
				ulRefUnixTime = (unsigned long) dblSeconds;
			}
			*pulRefUnixTime = ulRefUnixTime;
		}

		//let's get the julian date
		if(pusJulianDate)
		{
			*pusJulianDate = usJulianDate;
		}
		return nMillisecondsInDay;
	}
	return OMNI_ERROR;
}

int	COmniParse::ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned long ulFlags) 
{
	if(ulFlags&OMNI_HEX)  // means, if BCD
	{
/*
		char temp[32];
		sprintf(temp, "%02x", ucHours);		ucHours = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucMinutes);	ucMinutes = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucSeconds);	ucSeconds = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucFrames);	ucFrames = (unsigned char)(atoi(temp));
*/		
		unsigned char ucTemp = 	(ucHours&0x0f)+(((ucHours&0xf0)>>4)*10); ucHours = ucTemp;
		ucTemp = 	(ucMinutes&0x0f)+(((ucMinutes&0xf0)>>4)*10); ucMinutes = ucTemp;
		ucTemp = 	(ucSeconds&0x0f)+(((ucSeconds&0xf0)>>4)*10); ucSeconds = ucTemp;
		ucTemp = 	(ucFrames&0x0f)+(((ucFrames&0xf0)>>4)*10); ucFrames = ucTemp;
	}
	if((ucHours>23)||(ucHours<0)) return TIME_NOT_DEFINED;
	if((ucMinutes>59)||(ucMinutes<0)) return TIME_NOT_DEFINED;
	if((ucSeconds>59)||(ucSeconds<0)) return TIME_NOT_DEFINED;

	switch(m_ulFlags&OMNI_FRAMEBASISMASK)
	{
	case OMNI_PAL://											0x00000010  // PAL	(25 fps)
		{
			if((ucFrames>24)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/25); //parens for rounding error
		} break;
	case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
		{
			if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/30); //parens for rounding error
		} break;
	default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
		{
			if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(int)(((double)(ucFrames)*1000.0)/29.97)); //parens for rounding error
		} break;

	}
}

int	COmniParse::ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned long ulFlags)
{
	*pucHours		= ((unsigned char)(ulMilliseconds/3600000L))&0xff;
	*pucMinutes = ((unsigned char)((ulMilliseconds/60000L)%60))&0xff;	
	*pucSeconds = ((unsigned char)(((ulMilliseconds/1000L)%60)%60))&0xff;

	// this fix came from Sentinel
	// frame calc had an issue where you could end up with or .30 frames if the millisecond count
	// was in between 989 and 999 for ntsc
	switch(m_ulFlags&OMNI_FRAMEBASISMASK)
	{
	case OMNI_PAL://											0x00000010  // PAL	(25 fps)
		{
			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/40)))&0xff;  //(int)(1000.0/25.0) = 40
		} break;
	case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
		{
//			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/33)))&0xff;  //(int)(1000.0/30.0)  = (int)(33.333333) = 33
		// have to make it reversible.  So anything above 989 gets converted to 29.
	 		*pucFrames	= ((unsigned char)(min(29, ((ulMilliseconds%1000L)/33))))&0xff;  //(int)(1000.0/30.0) = 33
		} break;
	default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
		{
//			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/33)))&0xff;  //(int)(1000.0/29.97) = (int)(33.366700) = 33
		// have to make it reversible.  So anything above 989 gets converted to 29.
	 		*pucFrames	= ((unsigned char)(min(29, ((ulMilliseconds%1000L)/33))))&0xff;  //(int)(1000.0/30.0) = 33
		} break;

	}



	if(ulFlags&OMNI_HEX)  // means, if BCD
	{
		unsigned char ucTemp = *pucHours;
		*pucHours = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucMinutes;
		*pucMinutes = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucSeconds;
		*pucSeconds = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucFrames;
		*pucFrames = ((ucTemp/10)*16) + (ucTemp%10);
	}
	return OMNI_SUCCESS;  // meaningless
}


/*
int COmniParse::SetConnDB(CCAConn* pConn, CDBUtil* pdb, CDBconn* pdbConn, char* pszTable) 
{
	if(pConn==NULL)
	{
//		AfxMessageBox("Could not set DB");
		return OMNI_ERROR;
	}

	if((pdb==NULL)||(pdbConn==NULL)||(pszTable==NULL)||(strlen(pszTable)<=0))
	{
		//clear out the old....
		pConn->m_pdb = NULL;  // pointer to a db util object to use for operations
		pConn->m_pdbConn = NULL;  // pointer to a db connection
		pConn->m_pszTable = NULL; // name of the table to update

		// just pointers, we arent freeing the connection objects etc.
//		AfxMessageBox("set to NULL");
	}
	else
	{
		pConn->m_pdb = pdb;  // pointer to a db util object to use for operations
		pConn->m_pdbConn = pdbConn;  // pointer to a db connection
		pConn->m_pszTable = pszTable; // name of the table to update
//		AfxMessageBox(pszTable);
	}
	return OMNI_SUCCESS;
}

*/