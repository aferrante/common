// OmnibusDefs.h: interface for the COmniComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(OMNIBUSDEFS_H_INCLUDED_)
#define OMNIBUSDEFS_H_INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <afxmt.h>
#include "../../MFC/ODBC/DBUtil.h"
#include "..\..\TXT\BufferUtil.h"


#ifndef NULL 
#define NULL 0
#endif

#define LIST_NOT_DEFINED			0xffff
#define TYPE_NOT_DEFINED			0xffff
#define STATUS_NOT_DEFINED		0xffff
#define TIME_NOT_DEFINED			0xffffffff
#define SEGMENT_NOT_DEFINED		0xff
#define INVALID_LIST					0xff

#define OMNI_SUCCESS		0
#define OMNI_ERROR			-1
#define OMNI_UNKNOWN		-2
#define OMNI_MEMEX			-3
#define OMNI_BADARGS		-4


#define OMNI_DEFAULT_TIMEOUT		10000 //milliseconds

// following for get events until playing
#define OMNI_GETEVENTS_EVENTLOOK			0x00000010  // event based lookahead
#define OMNI_GETEVENTS_TIMELOOK				0x00000020  // time based lookahead
// following for get events until ID
#define OMNI_GETEVENTS_INCLUDEDONE		0x00000040  // include done events
#define OMNI_GETEVENTS_ALL_IDS				0x00000080  // get all of the supplied events
#define OMNI_GETEVENTS_ANY_ID					0x00000100  // get any of the ids.
// various options for gets and mods
#define OMNI_GETEVENTS_DONTGET				0x00000200  // get info from the events in the canonical list, not the server
#define OMNI_NO_NOTIFY								0x00000400  // suppress sending a list change signal
#define OMNI_PROTECT_PLAY							0x00000800  // do not modify or delete events with play status
#define OMNI_PROTECT_DONE							0x00001000  // do not modify or delete events with done status
#define OMNI_PRIMARY_ONLY							0x00002000  // only apply to primary events (where applicable)
#define OMNI_SECONDARY_ONLY						0x00004000  // only apply to secondary events (where applicable)
#define OMNI_VERIFY_ALL								0x00008000  // use verification event, ensure totally identical, including all fields not commonly used.
#define OMNI_HEX											0x00010000  // expect hex encoded fields.
#define OMNI_NORMAL										0x00000000  // expect no encoding
#define OMNI_KEY_RECKEY								0x00000000  // key is Reconcile Key
#define OMNI_KEY_CLIPID								0x00000001  // key is Clip ID
#define OMNI_KEY_TITLE								0x00000002  // key is Title
#define OMNI_KEY_DATA									0x00000003  // key is "data" field
// connection options/status
#define OMNI_SOCKETERROR							0x00000001  // error
#define OMNI_PAL											0x00000010  // PAL	(25 fps)
#define OMNI_NTSCNDF									0x00000020  // NTSC no drop frame (30 fps)
#define OMNI_NTSC											0x00000030  // NTSC drop frame (29.97 fps)
#define OMNI_FRAMEBASISMASK						0x00000030  // mask
#define OMNI_FRAMEBASISSET						0x00100000  // bool
#define OMNI_SOCKETCONNECTED					0x01000000  // socket connected

// list options/status
#define OMNI_LIST_DISCONNECTED				0x00000000  // not connected
#define OMNI_LIST_CONNECTED						0x00000001  // connected

// database things
// table columns
	// fields from adaptor, all not NULL except where indicated
#define OMNI_DB_CONNIP_NAME				"conn_ip"    // of colossus connection, varchar 16
#define OMNI_DB_CONNPORT_NAME			"conn_port"  // of colossus connection, int
#define OMNI_DB_LISTID_NAME				"list_id"			// omnibus stream number, int
#define OMNI_DB_EVENTID_NAME			"event_id"    // unique ID (story ID), varchar 32, recKey on harris
#define OMNI_DB_EVENTCLIP_NAME		"event_clip"  // clip ID, varchar 32
#define OMNI_DB_EVENTTITLE_NAME		"event_title" // title, varchar 64
#define OMNI_DB_EVENTDATA_NAME		"event_data"  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
#define OMNI_DB_EVENTTYPE_NAME		"event_type"  // internal colossus type, int
#define OMNI_DB_EVENTSTATUS_NAME	"event_status"  // internal colossus status, int
#define OMNI_DB_EVENTTMODE_NAME		"event_time_mode"  // time mode, int
#define OMNI_DB_EVENTFSJ70_NAME		"event_start" // frames since Jan 1970, SQL float (C++ double)
#define OMNI_DB_EVENTDURMS_NAME		"event_duration" // duration in milliseconds, int
#define OMNI_DB_EVENTUPDATED_NAME	"event_last_update" // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
	// fields from other apps
#define OMNI_DB_APPDATA_NAME			"app_data" // application data, varchar 512, allow null
#define OMNI_DB_APPDATAAUX_NAME		"app_data_aux" // auxiliary application data, int, allow null

// db enum fields
#define OMNI_DB_CONNIP_ID					0    // of colossus connection, varchar 16
#define OMNI_DB_CONNPORT_ID				1  // of colossus connection, int
#define OMNI_DB_LISTID_ID					2			// omnibus stream number, int
#define OMNI_DB_EVENTID_ID				3    // unique ID (story ID), varchar 32, recKey on harris
#define OMNI_DB_EVENTCLIP_ID			4  // clip ID, varchar 32
#define OMNI_DB_EVENTTITLE_ID			5 // title, varchar 64
#define OMNI_DB_EVENTDATA_ID			6  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
#define OMNI_DB_EVENTTYPE_ID			7  // internal colossus type, int
#define OMNI_DB_EVENTSTATUS_ID		8  // internal colossus status, int
#define OMNI_DB_EVENTTMODE_ID			9  // time mode, int
#define OMNI_DB_EVENTFSJ70_ID			10 // frames since Jan 1970, SQL float (C++ double)
#define OMNI_DB_EVENTDURMS_ID			11 // duration in milliseconds, int
#define OMNI_DB_EVENTUPDATED_ID		12 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
	// fields from other apps
#define OMNI_DB_APPDATA_ID				13 // application data, varchar 512, allow null
#define OMNI_DB_APPDATAAUX_ID			14 // auxiliary application data, int, allow null




// omnibus defines from colossus adaptor spec V1.5
// status
#define C_ERROR												0  // item contains errors
#define C_INITIALIZED									1  // item has just been initialized and cannot be used without further work
#define C_UNCHECKED										2  // item wasn't checked
#define C_UNALLOCATED									3  // item wasn't allocated
#define C_UNAVAILABLE									4  // item isn't available
#define C_ALLOCATED										5  // item allocated
#define C_CUEING											6  // item cueing
#define C_READY												7  // item ready for playout
#define C_COMMIT											8  // item ready to be committed to air
#define C_ON_AIR											9  // item is on air
#define C_HOLD												10 // item holding time after playing to air
#define C_DONE												11 // item has been played to air

// event types
#define CL_TYPE_INTERNAL							0  // actually not an omnibus define, but a def val.
#define CL_TYPE_ITEM									1  // an item with media
#define CL_TYPE_SEGMENT								2  // not yet implemented
#define CL_TYPE_EVENT									3  // a live event
#define CL_TYPE_BREAK_START						4  // not yet implemented
#define CL_TYPE_BREAK_END							5  // not yet implemented
#define CL_TYPE_SPLIT_BREAK						6  // not yet implemented
#define CL_TYPE_MACRO									7  // not yet implemented
#define CL_TYPE_SCHEDULE							8  // a schedule that has yet to be expanded

// time modes
#define C_MODE_MANUAL									0  // a manual take item
#define C_MODE_AUTO										1  // a normal automatic item
#define C_MODE_ABS										2  // a fixed time item
#define C_MODE_PLUS										3  // an item with a start time relative to the previous event
#define C_MODE_MINUS									4  // an item with a start time relative to the next event
#define C_MODE_FIXED_END							5  // an item with a fixed end time



// an event type that is uniform, containing only what we care about
class CCAEvent  //"Colossus Adaptor" Event
{
public:
// supplied from automation system
	unsigned short m_usType;  // internal colossus type
	char* m_pszID;    // the clip ID
	char* m_pszTitle;
	char* m_pszData;  //must encode zero.
	char* m_pszReconcileKey;       // the "story ID"
	unsigned char  m_ucSegment;    // not used
	unsigned long  m_ulOnAirTimeMS;
	unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
	unsigned long  m_ulDurationMS;
	unsigned short m_usStatus;  // status (omnibus code)
	unsigned short m_usControl; // time mode

	double m_dblOnAirTimeInternal;  // for omnibus, this is frames since jan 1970
	double m_dblUpdated;            // the last time the system updated the event. format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500

// a pointer to anything we want, to contextualize this event - must use new operator to allocate
	void* m_pContextData;  //must encode zero if a string.

public:
	CCAEvent();
	virtual ~CCAEvent();
};

class CCAEventData  // used in data field search
{
public:
	char* m_pszFieldName;  // unadorned, no tags.
	char* m_pchStartTag;   // start position of the start tag including "<"
	char* m_pchEndTag;     // position of the end of the end tag including ">", = start position of next data.
	char* m_pszFieldInfo;  // contents of the data field including start and end tags.

public:
	CCAEventData();
	virtual ~CCAEventData();
};



// we need to index by list, not by connection, but automation system indexes by connection.
// we can get a list status on a connection basis, then have an array of pointers indexing into lists.
// therefore, we also need a list type.
class CCAList  //"Colossus Adaptor" List
{
private:
	CCAEvent** m_ppEvents; // a pointer to an array of event pointers.
	unsigned short m_usEventCount; 


public:
//	CRITICAL_SECTION m_critGEI;  // critical section to manage access to events.
//	CRITICAL_SECTION m_critUE;  // critical section to manage access to events.

	char* m_pszListName;			  // basically, channel name
	unsigned short m_usListID;  // numerical channel ID
	unsigned long  m_ulFlags;		// list options/status
	unsigned long  m_ulRefTick; // last status get.
	unsigned long  m_ulCounter;		// changes counter incrementer


public:
	CCAList(char* pszListName, unsigned short usListID );
	virtual ~CCAList();
	int GetEventIndex(char* pszKey, unsigned long ulFlags=OMNI_KEY_RECKEY);  // this is the unique list ID, the "story ID"
	int UpdateEvent(CCAEvent* pEvent, CDBUtil*	pdb=NULL, CDBconn* pdbConn=NULL,	char*	pszTable=NULL, char*	pszConnServer=NULL, unsigned short usConnPort=0);
	int DeleteEvent(unsigned short usIndex, CDBUtil*	pdb=NULL, CDBconn* pdbConn=NULL,	char*	pszTable=NULL);
	int DeleteAllEvents(CDBUtil*	pdb=NULL, CDBconn* pdbConn=NULL,	char*	pszTable=NULL);
	// trying to restrict access.
	unsigned short GetEventCount();
	CCAEvent* GetEvent(unsigned short usEventIndex);
	int SetFrameBasis(unsigned long ulFrameBasis);
};


class CCAConn  //"Colossus Adaptor" Connection
{
public:
// used to connect:
	char* m_pszServerName;			// friendly name for the connection to the server.  not used programmatically
	char* m_pszServerAddress;		// can be host name or IP address
	unsigned short m_usPort;		// port to connect
	unsigned long  m_ulFlags;		// connection options/status
	bool m_bKillThread;					// kill the comm thread
	bool m_bThreadStarted;			// state of comm thread
	bool m_bUseRepsonse;				// respond to XML messages with a roAck.

	bool m_bWriteXMLtoFile;
	bool m_bUseUTC;
	bool m_bUseDST;

// supplied from automation system
	unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
	unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
	unsigned long  m_ulRefUnixTime;		// compiled from "current time".

// assigned internally
private:
	CCAList** m_ppList;  // need to dynamically allocate.
	CCAList* m_pTempList; // a place to store events temporarily.  volatile.
	unsigned short m_usListCount; 

public:

	CRITICAL_SECTION m_crit;  // critical section to manage access to lists.

	unsigned long  m_ulRefTick; // last status get.
	unsigned long  m_ulCounter;		// changes counter incrementer

	unsigned long  m_ulConnectionRetries;		// how many times to try to re-establish
	unsigned long  m_ulConnectionInterval;	// how long to wait in milliseconds before retrying

	char* m_pszDebugFile;		// debug file
	char* m_pszCommFile;		// comm log file

	CDBUtil*	m_pdb;  // pointer to a db util object to use for operations
	CDBconn*	m_pdbConn;  // pointer to a db connection
	char*			m_pszTable; // name of the table to update
	int				m_nNumFields;
	CCAEventData** m_ppfi;  // dynamic array of field info.

public:
	CCAConn();
	virtual ~CCAConn();

//core
	int ConnectServer();
	int DisconnectServer();

	int ReturnListIndex(unsigned short usListID);  // use m_crit outside calls to this one
	int UpdateList(unsigned short usListID, char* pszListName, unsigned long ulFlags=OMNI_LIST_DISCONNECTED, int* pnListID=NULL);
	unsigned short GetListCount();
	CCAList* GetList(unsigned short usListIndex);


// core - event search
	int GetRelativeIndex(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags);
	int GetNumSecondaries(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags);

// utility
	int ConvertTime(char* pszTimeString, unsigned long* pulRefUnixTime=NULL, unsigned short* pusJulianDate=NULL);  // returns milliseconds in current day, -1 if error.
	int	ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned long ulFlags=OMNI_NORMAL);  
	int	ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned long ulFlags=OMNI_NORMAL);

	int SetCommLogName(char* pszFileName);
	int SetDebugName(char* pszFileName);
	int SetFrameBasis(unsigned long ulFrameBasis);

};


#endif // !defined(OMNIBUSDEFS_H_INCLUDED_)
