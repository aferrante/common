// OmniComm.h: interface for the COmniComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OMNICOMM_H__A0953E24_B5AD_4D97_9F7E_5C7A1DFF024D__INCLUDED_)
#define AFX_OMNICOMM_H__A0953E24_B5AD_4D97_9F7E_5C7A1DFF024D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


//#include "../../MSG/MessagingObject.h"
#include "OmnibusDefs.h"  // includes DB and messaging obj


class COmniComm : public CMessagingObject
{
public:

	CCAConn**	m_ppConn;  // pointer to an array of pointers to connection types
	unsigned short	m_usNumConn;

	CRITICAL_SECTION m_crit;  // critical section to manage access to connections.
	CRITICAL_SECTION m_critMem;  // critical section to manage access to connections.

public:
	COmniComm();
	virtual ~COmniComm();

	// dont need IsConnectionActive because non acitve connections cut the comm thread.

// override
//	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);

// core - main
	CCAConn* ConnectServer(char* pszServerAddress, unsigned short usPort=10540, char* pszServerName=NULL, char* pszDataFields=NULL, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL, char* pszEventsTable=NULL, char* pszExchangeTable=NULL, char* pszConnectionsTable=NULL, char* pszChannelsTable=NULL, CRITICAL_SECTION* pRetrieveCrit=NULL, void* lpfnMsg=NULL, CRITICAL_SECTION* pMsgCrit=NULL); // returns pointer to connection
	CCAConn* ConnectionExists(char* pszServerAddress); // returns pointer to connection
	int DisconnectServer(CCAConn* pConn, int nTimeoutMS);
	int DisconnectServer(char* pszServerAddress, int nTimeoutMS); // searches for conn idx
	int SetConnDB(CCAConn* pConn, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL, char* pszEventsTable=NULL, char* pszExchangeTable=NULL, char* pszConnectionsTable=NULL, char* pszChannelsTable=NULL, CRITICAL_SECTION* pRetrieveCrit=NULL); // searches for conn idx
	int SetMessager(CCAConn* pConn, void* lpfnMsg=NULL, CRITICAL_SECTION* pMsgCrit=NULL); // sets the messager

	bool CheckConnectionValid(unsigned short usConnIndex);
};

#endif // !defined(AFX_OMNICOMM_H__A0953E24_B5AD_4D97_9F7E_5C7A1DFF024D__INCLUDED_)
