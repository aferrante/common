// OmnibusDefs.cpp: implementation of the COmniComm and support classes.
//
//////////////////////////////////////////////////////////////////////

#include "OmnibusDefs.h"
#include "..\..\LAN\NetUtil.h"
#include <process.h>
#include <sys/timeb.h>
#include <time.h>
#include <direct.h>


#define HELIOS_NEW_DB_SCHEMA
// comm thread proto
#ifdef HELIOS_NEW_DB_SCHEMA
extern void HeliosAdaptorCommThread(void* pvArgs);
#else // ! HELIOS_NEW_DB_SCHEMA
extern void CommThread(void* pvArgs);
#endif// ! HELIOS_NEW_DB_SCHEMA

CCAActives g_actives;


//////////////////////////////////////////////////////////////////////
// CCAActives Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAActives::CCAActives()
{
	m_bUseActive = false;
	m_bClearEventsInit = false;
	m_pnActives = NULL;
	m_nNumActive = 0;
	InitializeCriticalSection(&m_critActive);
}

CCAActives::~CCAActives()
{
	EnterCriticalSection(&m_critActive);
	m_nNumActive = 0;
	if(m_pnActives)
	{
/*
		for(unsigned short i=0;i<m_nNumActive;i++)
		{
			if(m_ppnActives[i]) delete(m_ppnActives[i]);
			m_ppnActives[i] = NULL;
		}
*/
		try{ delete [] m_pnActives;} catch(...){}
		m_pnActives = NULL;
	}
	DeleteCriticalSection(&m_critActive);
}

int CCAActives::IsChannelActive(int nChannelID)
{
	if((m_bUseActive)&&(nChannelID>0)&&(m_pnActives)&&(m_nNumActive>0))
	{
		int n=0;
		while(n<m_nNumActive)
		{
			if( m_pnActives[n] == nChannelID )
			{
				return n;
			}
			n++;
		}
	}
	return OMNI_ERROR;
}


int CCAActives::RemoveActiveChannel(int nChannelID)
{
	if((m_bUseActive)&&(nChannelID>0)&&(m_pnActives)&&(m_nNumActive>0))
	{
		int n=0;
		while(n<m_nNumActive)
		{
			if( m_pnActives[n] == nChannelID) 
			{
				// found it.
				m_nNumActive--;
				while(n<m_nNumActive)
				{
					m_pnActives[n] = m_pnActives[n+1];
					n++;
				}
				m_pnActives[n] = NULL;

				if(m_nNumActive <= 0)
				{
					try{ delete [] m_pnActives; } catch(...){}
					m_pnActives = NULL;
					m_nNumActive = 0;
				}
				else
				{
					int* pn = new int[m_nNumActive];
					if(pn)
					{
//						memcpy(&pn, m_pnActives, sizeof(int)*m_nNumActive);  /// was this, but &pn is incorrect. 2.1.1.18
						memcpy(pn, m_pnActives, sizeof(int)*m_nNumActive);
						try{ delete [] m_pnActives; } catch(...){}
						m_pnActives = pn;
					}
				}

				return m_nNumActive;
			}
			n++;
		}
	}
	return OMNI_ERROR;
}


int CCAActives::AddActiveChannel(int nChannelID)
{
	int nRV = IsChannelActive(nChannelID); // first check it is found
	if((nRV==OMNI_ERROR)&&(m_bUseActive)&&(nChannelID>0))
	{
		int* pn = new int[m_nNumActive+1];
		if(pn)
		{
			int o=0;
			if((m_pnActives)&&(m_nNumActive>0))
			{
				while(o<m_nNumActive)
				{
					pn[o] = m_pnActives[o];
					o++;
				}
				try{ delete [] m_pnActives; } catch(...){}
			}
			pn[m_nNumActive] = nChannelID;
			m_pnActives = pn;
			m_nNumActive++;
			return m_nNumActive;
		}
	}
	return nRV;
}

//////////////////////////////////////////////////////////////////////
// CCAEvent Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAEvent::CCAEvent()
{
// supplied from automation system
	m_usType = TYPE_NOT_DEFINED;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszData = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = SEGMENT_NOT_DEFINED;
	m_ulOnAirTimeMS = TIME_NOT_DEFINED;
	m_usOnAirJulianDate =0;
	m_ulDurationMS = TIME_NOT_DEFINED;
	m_usStatus = STATUS_NOT_DEFINED;
	m_usControl = STATUS_NOT_DEFINED;

	m_dblOnAirTimeInternal=-1.0;
	m_dblOnAirTime=-1.0;
	m_dblUpdated=-1.0;

	m_nEventPosition=-1;
	m_pParent = NULL;

// a pointer to anything we want, to contextualize this event - must use new operator to allocate
	m_pContextData = NULL;

	m_ulFlags = OMNI_FLAGS_NONE;

}

CCAEvent::~CCAEvent()
{
	if(m_pszID) free(m_pszID); // must use malloc to allocate buffer;
	if(m_pszTitle) free(m_pszTitle); // must use malloc to allocate buffer;
	if(m_pszData) free(m_pszData); // must use malloc to allocate buffer;
	if(m_pszReconcileKey) free(m_pszReconcileKey); // must use malloc to allocate buffer;
	if(m_pContextData) delete(m_pContextData); // must use new to allocate object;
}


//////////////////////////////////////////////////////////////////////
// CCAEventData Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAEventData::CCAEventData()
{
	m_pszFieldName = NULL;
	m_pchStartTag = NULL;
	m_pchEndTag = NULL;
	m_pszFieldInfo = NULL;
}

CCAEventData::~CCAEventData()
{
	if(m_pszFieldName) free(m_pszFieldName); // must use malloc to allocate buffer;
	if(m_pszFieldInfo) free(m_pszFieldInfo); // must use malloc to allocate buffer;
	//must NOT free m_pchStartTag and m_pchEndTag.  these are just pointers for search positions
}



//////////////////////////////////////////////////////////////////////
// CCAList Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAList::CCAList(char* pszListName, unsigned short usListID )
{
	m_tmbLastUpdate.time = 0;
	m_tmbLastUpdate.millitm = 0;
	m_tmbLastDatabaseCheck.time = 0;
	m_tmbLastDatabaseCheck.millitm = 0;
//	InitializeCriticalSection (&m_critGEI);
//	InitializeCriticalSection (&m_critUE);
	m_pszListName = NULL;			  // basically, channel name

	if((pszListName)&&(strlen(pszListName)))
	{
		char* pch = (char*)malloc(strlen(pszListName)+1);
		if(pch)
		{
			sprintf(pch, "%s", pszListName);
//			if(m_pszListName) free(m_pszListName);  // cant be, this is the constructor
			m_pszListName = pch;

//			return OMNI_SUCCESS;
		} 
//		return OMNI_ERROR;
	}
//	else
//		return OMNI_BADARGS;

	m_usListID = usListID;
	m_ulFlags = OMNI_LIST_DISCONNECTED;
	m_ppEvents = NULL; // a pointer to an array of event pointers.
	m_usEventCount = 0; 

	m_usDoneCount = 3; //default value
	m_nLastEventIndex = -1;
	m_nLastDatabaseEventIndex=-1;

	m_pConn = NULL;

	m_ulRefTick=0; // last status get.
	m_ulCounter=0;		// changes counter incrementer
	InitializeCriticalSection(&m_critEvents);

	m_ppchXMLQueue=NULL; // queue up XML items.
	m_nXMLQueue=0;
	InitializeCriticalSection (&m_critXMLQueue);
	m_bXMLQueueStarted=false;
	m_bXMLQueueKill=true;

}

CCAList::~CCAList()
{
	m_bXMLQueueKill=true;
//	EnterCriticalSection (&m_critGEI);
//	EnterCriticalSection (&m_critUE);
	try
	{	
		if(m_pszListName) free(m_pszListName);
		EnterCriticalSection(&m_critEvents);
		if(m_ppEvents)
		{
			for(unsigned short i=0;i<m_usEventCount;i++)
			{
				if(m_ppEvents[i]) delete m_ppEvents[i];
				m_ppEvents[i] = NULL;
			}
			delete [] m_ppEvents;
			m_ppEvents = NULL;
		}
		LeaveCriticalSection(&m_critEvents);
	//	LeaveCriticalSection (&m_critUE);
	//	LeaveCriticalSection (&m_critGEI);

	//	DeleteCriticalSection (&m_critUE);
	//	DeleteCriticalSection (&m_critGEI);
		DeleteCriticalSection(&m_critEvents);
	}
	catch(...)
	{
	}

	m_bXMLQueueKill=true;
	while(m_bXMLQueueStarted)
	{
		Sleep(1);
	}

	EnterCriticalSection (&m_critXMLQueue);
	if(m_ppchXMLQueue)
	{
		for(int i=0; i<m_nXMLQueue; i++)
		{
			if(m_ppchXMLQueue[i])
			{
				delete [] m_ppchXMLQueue[i];  // char array.
			}
		}
		delete [] m_ppchXMLQueue;
	}
	LeaveCriticalSection (&m_critXMLQueue);

	m_ppchXMLQueue=NULL; // queue up XML items.
	m_nXMLQueue=0;
	DeleteCriticalSection (&m_critXMLQueue);


}

int CCAList::AddMessage(char* pchMessage)
{
	if(pchMessage==NULL) return OMNI_ERROR;
//	EnterCriticalSection(&m_critXMLQueue);
	if(m_ppchXMLQueue == NULL) {m_nXMLQueue=0;}
	char** xx = NULL;
	xx=new char*[m_nXMLQueue+1];
	if(xx)
	{
		if((m_ppchXMLQueue)&&(m_nXMLQueue))
		{
			memcpy(xx, m_ppchXMLQueue, m_nXMLQueue*sizeof(char*));
		}
		if(m_ppchXMLQueue){delete [] m_ppchXMLQueue;}
		m_ppchXMLQueue = xx;
	}
	
	if(m_ppchXMLQueue)
	{
		m_ppchXMLQueue[m_nXMLQueue]=pchMessage;
		m_nXMLQueue++;

if(((CCAConn*)m_pConn)->m_ulFlags&OMNI_DEBUG)
{
	_timeb timestamp;
	_ftime(&timestamp);
	char filename[256];
	sprintf(filename, "Logs\\omni-%s-%d.txt", ((CCAConn*)m_pConn)->m_pszServerAddress, m_usListID);
	FILE* fp = fopen(filename, "ab");
	if(fp)
	{
		char xml[256];
		memcpy(xml, pchMessage, min(255,strlen(pchMessage)));
		xml[min(255,strlen(pchMessage))]=0;

		if(*xml != '<')
		{ 
			if(*xml == 0x01) *xml = '<';
			else *xml = '%';
		}

		fprintf(fp, "%d.%03d queue now %05d, ADD %d chars [%s]\r\n", timestamp.time, timestamp.millitm, m_nXMLQueue, strlen(pchMessage), xml);
		fclose(fp);
	}
}
	}

//	LeaveCriticalSection(&m_critXMLQueue);
	return OMNI_SUCCESS;
}


char* CCAList::ReturnMessage()
{
	char filename[256];
	char* xx = NULL;
//	EnterCriticalSection(&m_critXMLQueue);
	if(m_ppchXMLQueue == NULL) {m_nXMLQueue=0; return xx;}
		
	xx = m_ppchXMLQueue[0];

	int i=0;
	while(i<m_nXMLQueue-1)
	{
		m_ppchXMLQueue[i] = m_ppchXMLQueue[i+1];
		i++;
	}
	m_ppchXMLQueue[i]=NULL;
	m_nXMLQueue--;

if(((CCAConn*)m_pConn)->m_ulFlags&OMNI_DEBUG)
{
	_timeb timestamp;
	_ftime(&timestamp);
	sprintf(filename, "Logs\\omni-%s-%d.txt", ((CCAConn*)m_pConn)->m_pszServerAddress, m_usListID);
	FILE* fp = fopen(filename, "ab");
	if(fp)
	{
		char xml[256];
		memcpy(xml, xx, min(255,strlen(xx)));
		xml[min(255,strlen(xx))]=0;

		if(*xml != '<')
		{ 
			if(*xml == 0x01) *xml = '<';
			else *xml = '%';
		}

		fprintf(fp, "%d.%03d queue now %05d, REM %d chars [%s]\r\n", timestamp.time, timestamp.millitm, m_nXMLQueue, strlen(xx), xml);
		fclose(fp);
	}
}     
	if(m_nXMLQueue == 0)
	{
		delete [] m_ppchXMLQueue;
		m_ppchXMLQueue = NULL;
	}

//	LeaveCriticalSection(&m_critXMLQueue);
	return xx;
}



	
// be sure to critical section this outside, before accessing
int CCAList::GetEventIndex(char* pszKey, unsigned long ulFlags, int nStartIndex)  // this is the unique list ID, the "story ID"
{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "GetEventIndex, checking %s\r\n", pszKey);
	fflush(fp);
	fclose(fp);
}
*/
	if((pszKey)&&(strlen(pszKey)))
	{
		if((m_ppEvents)&&(m_usEventCount>0))
		{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "GetEventIndex, checking %s, found event count %d\r\n", pszKey, m_usEventCount);
	fflush(fp);
	fclose(fp);
}
*/
			unsigned short j = 0;
			unsigned short i = min(m_nLastEventIndex+1, m_usEventCount-1);
			if((nStartIndex>=0)&&(nStartIndex<m_usEventCount)) i=nStartIndex; // override
			while(j<m_usEventCount)
			{
				if(m_ppEvents[i])
				{
					switch(ulFlags)
					{
					case OMNI_KEY_RECKEY://								0x00000000  // key is Reconcile Key
						{
							if(m_ppEvents[i]->m_pszReconcileKey)
							{
								if(strcmp(m_ppEvents[i]->m_pszReconcileKey, pszKey)==0){ m_nLastEventIndex=i; return i; }
							}
						} break;
					case OMNI_KEY_CLIPID://								0x00000001  // key is Clip ID
						{
							if(m_ppEvents[i]->m_pszID)
							{
								if(strcmp(m_ppEvents[i]->m_pszID, pszKey)==0) { m_nLastEventIndex=i; return i; }
							}
						} break;
					case OMNI_KEY_TITLE://								0x00000002  // key is Title
						{
							if(m_ppEvents[i]->m_pszTitle)
							{
								if(strcmp(m_ppEvents[i]->m_pszTitle, pszKey)==0) { m_nLastEventIndex=i; return i; }
							}
						} break;
					case OMNI_KEY_DATA://									0x00000003  // key is "data" field
						{
							if(m_ppEvents[i]->m_pszData)
							{
								if(strcmp(m_ppEvents[i]->m_pszData, pszKey)==0) { m_nLastEventIndex=i; return i; }
							}
						} break;
					}
				}
				i++;  if(i>=m_usEventCount) i=0;
				j++;
			}
		}
		return -1;
	}
	else
		return OMNI_BADARGS;

}

// be sure to critical section this outside, before accessing
// this is exactly the same as the previous function, just limited to the one comparison to avoid the switch repeatedly.
int CCAList::GetEventIndexByRecKey(char* pszKey, int nStartIndex)  // this is the unique list ID, the "story ID"
{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "GetEventIndex, checking %s\r\n", pszKey);
	fflush(fp);
	fclose(fp);
}
*/
	if((pszKey)&&(strlen(pszKey)))
	{
		if((m_ppEvents)&&(m_usEventCount>0))
		{
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "GetEventIndex, checking %s, found event count %d\r\n", pszKey, m_usEventCount);
	fflush(fp);
	fclose(fp);
}
*/
			unsigned short j = 0;
			unsigned short i = min(m_nLastEventIndex+1, m_usEventCount-1);
			if((nStartIndex>=0)&&(nStartIndex<m_usEventCount)) i=nStartIndex; // override
			while(j<m_usEventCount)
			{
				if(m_ppEvents[i])
				{
					if(m_ppEvents[i]->m_pszReconcileKey)
					{
						if(strcmp(m_ppEvents[i]->m_pszReconcileKey, pszKey)==0){ m_nLastEventIndex=i; return i; }
					}
				}
				i++;  if(i>=m_usEventCount) i=0;
				j++;
			}
		}
		return -1;
	}
	else
		return OMNI_BADARGS;

}



unsigned short CCAList::GetEventCount()
{
EnterCriticalSection(&m_critEvents);
	if(m_ppEvents)
	{
LeaveCriticalSection(&m_critEvents);
		return m_usEventCount;
	}
LeaveCriticalSection(&m_critEvents);
	return 0;
}

CCAEvent* CCAList::GetEvent(int nIndex)
{
	if((nIndex>=0)&&(nIndex<m_usEventCount))
	{
EnterCriticalSection(&m_critEvents);
		if((m_ppEvents)&&(m_ppEvents[nIndex]))
		{
LeaveCriticalSection(&m_critEvents);
			return m_ppEvents[nIndex];
		}
LeaveCriticalSection(&m_critEvents);
	}
	return NULL;
}


// be sure to critical section this outside, before accessing
int CCAList::UpdateEvent(CCAEvent* pEvent, CDBUtil*	pdb, CDBconn* pdbConn, char* pszTable, char* pszConnServer, unsigned short usConnPort)
{ 
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "updating event\r\n");
	fprintf(fp, "%d %d %d [%s]\r\n", (pdb),(pdbConn),(pdbConn->m_bConnected),(pszTable?pszTable:"null"));
	fflush(fp);
	fclose(fp);
}
*/
	if((pEvent==NULL)||(pEvent->m_pszReconcileKey==NULL)||(strlen(pEvent->m_pszReconcileKey)<=0)) return OMNI_BADARGS;
	CCAEvent* pParent = (CCAEvent*) pEvent->m_pParent; 

	CCAConn* pConn = (CCAConn*) m_pConn;

  int nRV = OMNI_ERROR;
//	EnterCriticalSection (&m_critGEI);
EnterCriticalSection(&m_critEvents);
	int nIndex = GetEventIndexByRecKey(pEvent->m_pszReconcileKey); // must check if it's there

//	return 1; 
	if(nIndex>=0)
	{
//LeaveCriticalSection(&m_critEvents);
//return 1;
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "event found at %d\r\n", nIndex);
	fflush(fp);
	fclose(fp);
}
*/

		//first check if the pointer is not the same
		if(pEvent != m_ppEvents[nIndex])
		{
			pEvent->m_ulFlags = m_ppEvents[nIndex]->m_ulFlags;  // must preserve db insertion state
			// if it's different, switch the thing
//EnterCriticalSection(&m_critEvents);
			if(m_ppEvents[nIndex])
			{
				try
				{
					delete m_ppEvents[nIndex];
				}
				catch(...)
				{
				}
			}
			m_ppEvents[nIndex] = pEvent;
//LeaveCriticalSection(&m_critEvents);
//		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
		}
//	LeaveCriticalSection (&m_critGEI);
		nRV = OMNI_SUCCESS;  // it's already there, we dont overwrite the list name tho.
	}
	else
	{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "adding new event, %s, %s, %s, %s\r\n",
	pEvent->m_pszReconcileKey,       // the "story ID"
	pEvent->m_pszID,    // the clip ID
	pEvent->m_pszTitle,
	pEvent->m_pszData  //must encode zero.
);
	fflush(fp);
	fclose(fp);
}

*/


		// we have to add it.
		CCAEvent** ppEvents = NULL;
		int nCount = ((int)(m_usEventCount))+1; // this ridiculousness is due to some issue with using unsigned short for indexing.

		int nRetries = 0;
		while((ppEvents == NULL)&&(nRetries<2))
		{
			bool bCritsec=false;
			try
			{
	EnterCriticalSection(pConn->m_pcritMem);
	bCritsec = true;
			}
			catch(...)
			{
				if((((CCAConn*) m_pConn)->m_pszDebugFile)&&(strlen(((CCAConn*) m_pConn)->m_pszDebugFile)))
				{
					FILE* fp = fopen(((CCAConn*) m_pConn)->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Exception entering critsec, retry %d\r\n", nRetries);
						fflush(fp);
						fclose(fp);
					}
				}
			}

			try
			{

				ppEvents = new CCAEvent*[nCount];
//	LeaveCriticalSection(pConn->m_pcritMem);
	/*
				if(m_ppEvents == NULL)
				{
					m_ppEvents =new CCAEvent*[10000];
				}

				if(m_ppEvents)
				{
					m_ppEvents[0] = pEvent;
					m_usEventCount = 1;
				}
	*/
			}
			catch(...)
			{
				if((((CCAConn*) m_pConn)->m_pszDebugFile)&&(strlen(((CCAConn*) m_pConn)->m_pszDebugFile)))
				{
					FILE* fp = fopen(((CCAConn*) m_pConn)->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Exception allocating event array, retry %d\r\n", nRetries);
						fflush(fp);
						fclose(fp);
					}
				}

				Sleep(50); // give it a short rest.

				ppEvents = NULL; // have to...
			}

			try
			{
				if(bCritsec)
				{
	LeaveCriticalSection(pConn->m_pcritMem);
				}
			}
			catch(...)
			{
				if((((CCAConn*) m_pConn)->m_pszDebugFile)&&(strlen(((CCAConn*) m_pConn)->m_pszDebugFile)))
				{
					FILE* fp = fopen(((CCAConn*) m_pConn)->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Exception leaving critsec, retry %d\r\n", nRetries);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			nRetries++;
		}

		if(ppEvents)
		{
			nCount--;
			int  i=0;
//>>>>
//			EnterCriticalSection(&m_critEvents);

					
			if(m_ppEvents)
			{
				if(nCount>0)
				{
					memcpy(ppEvents, m_ppEvents, nCount*sizeof(CCAEvent*));
				}
				try
				{
					delete [] m_ppEvents;
				}
				catch(...)
				{
				}
				m_ppEvents = NULL;
			}

			ppEvents[nCount] = pEvent; // assign new one

			m_ppEvents = ppEvents;
			m_usEventCount = (unsigned short)(nCount+1);



/*
			if((m_ppEvents)&&(m_usEventCount))
			{
				while(i<m_usEventCount)
				{
					ppEvents[i] = m_ppEvents[i];
					i++;
				}
				try
				{
					delete [] m_ppEvents;
				}
				catch(...)
				{
				}
			}
			ppEvents[m_usEventCount] = pEvent; // assign new one

			m_ppEvents = ppEvents;
			m_usEventCount++;
*/

//>>>>
//LeaveCriticalSection(&m_critEvents);
//		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
//	LeaveCriticalSection (&m_critGEI);
			nRV = OMNI_SUCCESS;  
			pEvent->m_ulFlags |= OMNI_FLAGS_CHANGED;
		}
		else
		{
			// couldnt allocate array!
//	LeaveCriticalSection (&m_critGEI);
			nRV = OMNI_MEMEX;
		}
//LeaveCriticalSection(&m_critEvents);
//return 1;


	}
	LeaveCriticalSection(&m_critEvents);

	if(nRV == OMNI_SUCCESS)
	{
		if((pConn)&&(pConn->m_nAsyncDatabaseLookaheadSec>0))
		{
			_timeb timestamp;
			_ftime( &timestamp );

			double dblLimit = ((double)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0) + pConn->m_nAsyncDatabaseLookaheadSec )) +	((double)(timestamp.millitm))/1000.0;
			switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
			{
			case OMNI_PAL://											0x00000010  // PAL	(25 fps)
				{
					dblLimit *= 25.0;
				} break;
			case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
				{
					dblLimit *= 30.0;
				} break;
			default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
				{
					dblLimit *= 29.97;
				}	break;
			}
//			char buf[256]; sprintf (buf, "%.3f ?< %.3f",pEvent->m_dblOnAirTimeInternal, dblLimit);
//			MessageBox(GetActiveWindow(), buf,"yoo",MB_OK);

			if(pEvent->m_dblOnAirTimeInternal < dblLimit)
			{
//			MessageBox(GetActiveWindow(), "TRUE","yoo",MB_OK);
				nRV = UpdateDatabaseEvent(pEvent, pdb, pdbConn, pszTable, pszConnServer, usConnPort);
//			char buf[256]; sprintf (buf, "%.3f ?< %.3f RV=%d",pEvent->m_dblOnAirTimeInternal, dblLimit,nRV);
//			MessageBox(GetActiveWindow(), buf,"yoo",MB_OK);
			}
			else
			{
				m_ulFlags |= OMNI_LIST_UNCOMMITTED_EXISTS;
			}
		}
		else
		{
			nRV = UpdateDatabaseEvent(pEvent, pdb, pdbConn, pszTable, pszConnServer, usConnPort);
		}
	}

/*
	if((pConn))//&&(nRV == OMNI_SUCCESS)) // we did an insertion or found the existing.  check active  // do this even if not mem successful - we want to add to db if we can, even if not in mem array
	{
		if((pConn)&&(!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
		{
			EnterCriticalSection(&g_actives.m_critActive);
			nRV = g_actives.IsChannelActive(m_usListID);
			LeaveCriticalSection(&g_actives.m_critActive);
		}	
	}


	if(nRV >= OMNI_SUCCESS) // we did an insertion or found the existing.  and we are active now deal with database
	{
		if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
		{
		// we are using DB, so check it out and update if exists, insert if not

			//we need to encode all the values:
			char* pszEncoded[OMNI_DB_APPDATA_ID]; // only need a few of these
			for (int i=0; i<OMNI_DB_APPDATA_ID; i++)
			{
				pszEncoded[i] = NULL;
			}


			pszEncoded[OMNI_DB_CONNIP_ID] =  pdb->EncodeQuotes(pszConnServer);   // of colossus connection, varchar 16
	//OMNI_DB_CONNPORT_ID				1  // of colossus connection, int
	//OMNI_DB_LISTID_ID					2			// omnibus stream number, int
			pszEncoded[OMNI_DB_EVENTID_ID] =  pdb->EncodeQuotes(pEvent->m_pszReconcileKey);    // unique ID (story ID), varchar 32, recKey on harris
			pszEncoded[OMNI_DB_EVENTCLIP_ID] =  pdb->EncodeQuotes(pEvent->m_pszID);   // clip ID, varchar 32
			pszEncoded[OMNI_DB_EVENTTITLE_ID] =  pdb->EncodeQuotes(pEvent->m_pszTitle);  // title, varchar 64
			pszEncoded[OMNI_DB_EVENTDATA_ID] =  pdb->EncodeQuotes(pEvent->m_pszData);   // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
	//OMNI_DB_EVENTTYPE_ID			7  // internal colossus type, int
	//OMNI_DB_EVENTSTATUS_ID		8  // internal colossus status, int
	//OMNI_DB_EVENTTMODE_ID			9  // time mode, int
	//OMNI_DB_EVENTFSJ70_ID			10 // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
	//OMNI_DB_EVENTDURMS_ID			11 // duration in milliseconds, int
	//OMNI_DB_EVENTUPDATED_ID		12 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
			if(pParent)	pszEncoded[OMNI_DB_PARENTID_ID] =  pdb->EncodeQuotes(pParent->m_pszReconcileKey);   // unique ID (story ID), varchar 32, recKey on harris


			if(pEvent->m_ulFlags&OMNI_FLAGS_INSERTED)
			{
				// update
				_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
	"UPDATE %s SET \
%s = '%s', \
%s = %d, \
%s = '%s', \
%s = '%s', \
%s = '%s', \
%s = %d, \
%s = %d, \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %d, \
%s = %.03f, \
%s = %d, \
%s = '%s', \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %.0f \
WHERE %s = %d \
AND %s = '%s'", 
							pszTable,
							OMNI_DB_CONNIP_NAME, pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
							OMNI_DB_CONNPORT_NAME, usConnPort,  // of colossus connection, int
							OMNI_DB_EVENTCLIP_NAME,	pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
							OMNI_DB_EVENTTITLE_NAME, pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
							OMNI_DB_EVENTDATA_NAME, pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
							OMNI_DB_EVENTTYPE_NAME, pEvent->m_usType,  // internal colossus type, int
							OMNI_DB_EVENTSTATUS_NAME, pEvent->m_usStatus,  // internal colossus status, int
							OMNI_DB_EVENTTMODE_NAME, pEvent->m_usControl,  // time mode, int
							OMNI_DB_EVENTCALCSTART_NAME, pEvent->m_dblOnAirTime,  // there is no calc...
							OMNI_DB_EVENTSTART_NAME, pEvent->m_dblOnAirTime, // 
							OMNI_DB_EVENTDURMS_NAME, pEvent->m_ulDurationMS,  // duration in milliseconds, int
							OMNI_DB_EVENTUPDATED_NAME, pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
							
							OMNI_DB_EVENTPOSITION_NAME, pEvent->m_nEventPosition,
							OMNI_DB_PARENTID_NAME, pszEncoded[OMNI_DB_PARENTID_ID]?pszEncoded[OMNI_DB_PARENTID_ID]:"",
							OMNI_DB_PARENTPOS_NAME, pParent?pParent->m_nEventPosition:-1,
							OMNI_DB_PARENTSTART_NAME, pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL 
							OMNI_DB_PARENTCALCSTART_NAME, pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL
							OMNI_DB_PARENTDURMS_NAME, pParent?pParent->m_ulDurationMS:-1, // int 
							OMNI_DB_PARENTCALCEND_NAME, pParent?(pParent->m_dblOnAirTime + ((double)(pParent->m_ulDurationMS))/1000.0):-1.0, // decimal(20,3)
							OMNI_DB_EVENTCALCEND_NAME, (pEvent->m_dblOnAirTime + ((double)(pEvent->m_ulDurationMS))/1000.0), // decimal(20,3)
							OMNI_DB_EVENTFSJ70_NAME, pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
							OMNI_DB_LISTID_NAME, m_usListID,
							OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
							);

	//		AfxMessageBox(szSQL);
			}
			else
			{ // must insert

				// but first have to delete in case there is some problem, dont want repeated events inserted, 
				// we are no longer checking for presence in db, its all memory now.
				if(!g_actives.m_bClearEventsInit)  //  but only if we didn't clear stuff out at the beginning.
				{
					_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
										"DELETE FROM %s \
WHERE %s = %d \
AND %s = '%s'", 
										pszTable,
										OMNI_DB_LISTID_NAME, m_usListID,
										OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
										);

					if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
					{
						FILE* fp = fopen(pConn->m_pszDebugFile, "at");
						if(fp)
						{
							fprintf(fp, "Delete before Insert SQL: %s\r\n", pConn->m_szSQL);
							fflush(fp);
							fclose(fp);
						}
					}

					if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
					if(pdb->ExecuteSQL(pdbConn, pConn->m_szSQL, pConn->m_errorstring)<DB_SUCCESS)
					{
						if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
						{
							FILE* fp = fopen(pConn->m_pszDebugFile, "at");
							if(fp)
							{
								fprintf(fp, "Delete before Insert ERROR: %s\r\n", pConn->m_errorstring);
								fflush(fp);
								fclose(fp);
							}
						}
					}
					if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
				}

				_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
"INSERT INTO %s (\
%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (\
'%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %.03f, %.03f, %d, %.03f, %d, '%s', %d, %.03f, %.03f, %d, %.03f, %.03f, %.0f)", 
									pszTable,
									OMNI_DB_CONNIP_NAME,  // of colossus connection, varchar 16
									OMNI_DB_CONNPORT_NAME,  // of colossus connection, int
									OMNI_DB_LISTID_NAME,			// omnibus stream number, int
									OMNI_DB_EVENTID_NAME,   // unique ID (story ID), varchar 32, recKey on harris
									OMNI_DB_EVENTCLIP_NAME,  // clip ID, varchar 32
									OMNI_DB_EVENTTITLE_NAME, // title, varchar 64
									OMNI_DB_EVENTDATA_NAME,  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
									OMNI_DB_EVENTTYPE_NAME,  // internal colossus type, int
									OMNI_DB_EVENTSTATUS_NAME,  // internal colossus status, int
									OMNI_DB_EVENTTMODE_NAME,  // time mode, int
									OMNI_DB_EVENTCALCSTART_NAME,//		"event_calc_start"
									OMNI_DB_EVENTSTART_NAME, 
									OMNI_DB_EVENTDURMS_NAME,// duration in milliseconds, int
									OMNI_DB_EVENTUPDATED_NAME, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
									OMNI_DB_EVENTPOSITION_NAME,//		"event_position" // int NOT NULL 
									OMNI_DB_PARENTID_NAME,//					"parent_id" // varchar(32) NULL 
									OMNI_DB_PARENTPOS_NAME,//				"parent_position" // int NULL
									OMNI_DB_PARENTSTART_NAME,//			"parent_start" // decimal(20,3) NULL 
									OMNI_DB_PARENTCALCSTART_NAME,//	"parent_calc_start" // decimal(20,3) NULL
									OMNI_DB_PARENTDURMS_NAME,//			"parent_duration" // int 
									OMNI_DB_PARENTCALCEND_NAME,//		"parent_calc_end" // decimal(20,3)
									OMNI_DB_EVENTCALCEND_NAME,//			"event_calc_end" // decimal(20,3)
									OMNI_DB_EVENTFSJ70_NAME, // frames since Jan 1970, SQL float (C++ double)
									pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
									usConnPort,  // of colossus connection, int
									m_usListID,
									pszEncoded[OMNI_DB_EVENTID_ID]?pszEncoded[OMNI_DB_EVENTID_ID]:"",
									pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
									pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
									pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
									pEvent->m_usType,  // internal colossus type, int
									pEvent->m_usStatus,  // internal colossus status, int
									pEvent->m_usControl,  // time mode, int
									pEvent->m_dblOnAirTime, // no calc, just insert same
									pEvent->m_dblOnAirTime, // 
									pEvent->m_ulDurationMS,  // duration in milliseconds, int
									pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
									pEvent->m_nEventPosition,
									pszEncoded[OMNI_DB_PARENTID_ID]?pszEncoded[OMNI_DB_PARENTID_ID]:"",
									pParent?pParent->m_nEventPosition:-1,
									pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL 
									pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL
									pParent?pParent->m_ulDurationMS:-1, // int 
									pParent?(pParent->m_dblOnAirTime + ((double)(pParent->m_ulDurationMS))/1000.0):-1.0, // decimal(20,3)
									(pEvent->m_dblOnAirTime + ((double)(pEvent->m_ulDurationMS))/1000.0), // decimal(20,3)
									pEvent->m_dblOnAirTimeInternal  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
									);


			}


			if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				FILE* fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "%d %s UpdateEvent SQL: %s\r\n", clock(), pszConnServer, pConn->m_szSQL);
					fflush(fp);
					fclose(fp);
				}
			}

			if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
			try
			{
				nIndex = pdb->ExecuteSQL(pdbConn, pConn->m_szSQL, pConn->m_errorstring);
			}
			catch(...)
			{
				nIndex = DB_ERROR_EX;
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent had an exception: %s\r\n", clock(), pConn->m_errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			if(nIndex<DB_SUCCESS)
			{
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent ERROR: %s\r\n", clock(), pConn->m_errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			else
			{
				pEvent->m_ulFlags |= OMNI_FLAGS_INSERTED;
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);

				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent returned %d\r\n", clock(), nIndex);
						fflush(fp);
						fclose(fp);
					}
				}

			}

/*
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent freeing\r\n", clock());
						fflush(fp);
						fclose(fp);
					}
				}
* /				
			for (i=0; i<OMNI_DB_APPDATA_ID; i++)
			{
				if(pszEncoded[i]){ try{ free(pszEncoded[i]);} catch(...){} }
			}
/*
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent freed\r\n", clock());
						fflush(fp);
						fclose(fp);
					}
				}
* /
		}
	}
*/	
	return nRV;
}

// be sure to critical section this outside, before accessing
int CCAList::UpdateDatabaseEvent(CCAEvent* pEvent, CDBUtil*	pdb, CDBconn* pdbConn, char* pszTable, char* pszConnServer, unsigned short usConnPort)
{
	
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "updating event\r\n");
	fprintf(fp, "%d %d %d [%s]\r\n", (pdb),(pdbConn),(pdbConn->m_bConnected),(pszTable?pszTable:"null"));
	fflush(fp);
	fclose(fp);
}
*/


	if((pEvent==NULL)||(pEvent->m_pszReconcileKey==NULL)||(strlen(pEvent->m_pszReconcileKey)<=0)) return OMNI_BADARGS;
	CCAEvent* pParent = (CCAEvent*) pEvent->m_pParent; 

	CCAConn* pConn = (CCAConn*) m_pConn;

/*
  int nRV = OMNI_ERROR;


//	EnterCriticalSection (&m_critGEI);
EnterCriticalSection(&m_critEvents);
	int nIndex = GetEventIndexByRecKey(pEvent->m_pszReconcileKey); // must check if it's there

//	return 1; 
	if(nIndex>=0)
	{
//LeaveCriticalSection(&m_critEvents);
//return 1;
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "event found at %d\r\n", nIndex);
	fflush(fp);
	fclose(fp);
}
* /

		//first check if the pointer is not the same
		if(pEvent != m_ppEvents[nIndex])
		{
			pEvent->m_ulFlags = m_ppEvents[nIndex]->m_ulFlags;  // must preserve db insertion state
			// if it's different, switch the thing
//EnterCriticalSection(&m_critEvents);
			if(m_ppEvents[nIndex])
			{
				try
				{
					delete m_ppEvents[nIndex];
				}
				catch(...)
				{
				}
			}
			m_ppEvents[nIndex] = pEvent;
//LeaveCriticalSection(&m_critEvents);
//		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
		}
//	LeaveCriticalSection (&m_critGEI);
		nRV = OMNI_SUCCESS;  // it's already there, we dont overwrite the list name tho.
	}
	else
	{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "adding new event, %s, %s, %s, %s\r\n",
	pEvent->m_pszReconcileKey,       // the "story ID"
	pEvent->m_pszID,    // the clip ID
	pEvent->m_pszTitle,
	pEvent->m_pszData  //must encode zero.
);
	fflush(fp);
	fclose(fp);
}

* /


		// we have to add it.
		CCAEvent** ppEvents = NULL;
		int nCount = ((int)(m_usEventCount))+1; // this ridiculousness is due to some issue with using unsigned short for indexing.

		int nRetries = 0;
		while((ppEvents == NULL)&&(nRetries<2))
		{
			bool bCritsec=false;
			try
			{
	EnterCriticalSection(pConn->m_pcritMem);
	bCritsec = true;
			}
			catch(...)
			{
				if((((CCAConn*) m_pConn)->m_pszDebugFile)&&(strlen(((CCAConn*) m_pConn)->m_pszDebugFile)))
				{
					FILE* fp = fopen(((CCAConn*) m_pConn)->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Exception entering critsec, retry %d\r\n", nRetries);
						fflush(fp);
						fclose(fp);
					}
				}
			}

			try
			{

				ppEvents = new CCAEvent*[nCount];
//	LeaveCriticalSection(pConn->m_pcritMem);
	/*
				if(m_ppEvents == NULL)
				{
					m_ppEvents =new CCAEvent*[10000];
				}

				if(m_ppEvents)
				{
					m_ppEvents[0] = pEvent;
					m_usEventCount = 1;
				}
	* /
			}
			catch(...)
			{
				if((((CCAConn*) m_pConn)->m_pszDebugFile)&&(strlen(((CCAConn*) m_pConn)->m_pszDebugFile)))
				{
					FILE* fp = fopen(((CCAConn*) m_pConn)->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Exception allocating event array, retry %d\r\n", nRetries);
						fflush(fp);
						fclose(fp);
					}
				}

				Sleep(50); // give it a short rest.

				ppEvents = NULL; // have to...
			}

			try
			{
				if(bCritsec)
				{
	LeaveCriticalSection(pConn->m_pcritMem);
				}
			}
			catch(...)
			{
				if((((CCAConn*) m_pConn)->m_pszDebugFile)&&(strlen(((CCAConn*) m_pConn)->m_pszDebugFile)))
				{
					FILE* fp = fopen(((CCAConn*) m_pConn)->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "Exception leaving critsec, retry %d\r\n", nRetries);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			nRetries++;
		}

		if(ppEvents)
		{
			nCount--;
			int  i=0;
//>>>>
//			EnterCriticalSection(&m_critEvents);

					
			if(m_ppEvents)
			{
				if(nCount>0)
				{
					memcpy(ppEvents, m_ppEvents, nCount*sizeof(CCAEvent*));
				}
				try
				{
					delete [] m_ppEvents;
				}
				catch(...)
				{
				}
				m_ppEvents = NULL;
			}

			ppEvents[nCount] = pEvent; // assign new one

			m_ppEvents = ppEvents;
			m_usEventCount = (unsigned short)(nCount+1);



/*
			if((m_ppEvents)&&(m_usEventCount))
			{
				while(i<m_usEventCount)
				{
					ppEvents[i] = m_ppEvents[i];
					i++;
				}
				try
				{
					delete [] m_ppEvents;
				}
				catch(...)
				{
				}
			}
			ppEvents[m_usEventCount] = pEvent; // assign new one

			m_ppEvents = ppEvents;
			m_usEventCount++;
* /

//>>>>
//LeaveCriticalSection(&m_critEvents);
//		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
//	LeaveCriticalSection (&m_critGEI);
			nRV = OMNI_SUCCESS;  
		}
		else
		{
			// couldnt allocate array!
//	LeaveCriticalSection (&m_critGEI);
			nRV = OMNI_MEMEX;
		}
//LeaveCriticalSection(&m_critEvents);
//return 1;


	}
	LeaveCriticalSection(&m_critEvents);
*/

	int nRV = OMNI_SUCCESS;

	if((pConn))//&&(nRV == OMNI_SUCCESS)) // we did an insertion or found the existing.  check active  // do this even if not mem successful - we want to add to db if we can, even if not in mem array
	{
		if((pConn)&&(!pConn->m_bKillThread)&&(g_actives.m_bUseActive)) // if these aren't set, we default to trying it out.
		{
			EnterCriticalSection(&g_actives.m_critActive);
			nRV = g_actives.IsChannelActive(m_usListID);
			LeaveCriticalSection(&g_actives.m_critActive);
		}	
	}


	if(nRV >= OMNI_SUCCESS) // we did an insertion or found the existing.  and we are active now deal with database
	{
		if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
		{
		// we are using DB, so check it out and update if exists, insert if not

			//we need to encode all the values:
			char* pszEncoded[OMNI_DB_APPDATA_ID]; // only need a few of these
			for (int i=0; i<OMNI_DB_APPDATA_ID; i++)
			{
				pszEncoded[i] = NULL;
			}


			pszEncoded[OMNI_DB_CONNIP_ID] =  pdb->EncodeQuotes(pszConnServer);   // of colossus connection, varchar 16
	//OMNI_DB_CONNPORT_ID				1  // of colossus connection, int
	//OMNI_DB_LISTID_ID					2			// omnibus stream number, int
			pszEncoded[OMNI_DB_EVENTID_ID] =  pdb->EncodeQuotes(pEvent->m_pszReconcileKey);    // unique ID (story ID), varchar 32, recKey on harris
			pszEncoded[OMNI_DB_EVENTCLIP_ID] =  pdb->EncodeQuotes(pEvent->m_pszID);   // clip ID, varchar 32
			pszEncoded[OMNI_DB_EVENTTITLE_ID] =  pdb->EncodeQuotes(pEvent->m_pszTitle);  // title, varchar 64
			pszEncoded[OMNI_DB_EVENTDATA_ID] =  pdb->EncodeQuotes(pEvent->m_pszData);   // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
	//OMNI_DB_EVENTTYPE_ID			7  // internal colossus type, int
	//OMNI_DB_EVENTSTATUS_ID		8  // internal colossus status, int
	//OMNI_DB_EVENTTMODE_ID			9  // time mode, int
	//OMNI_DB_EVENTFSJ70_ID			10 // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
	//OMNI_DB_EVENTDURMS_ID			11 // duration in milliseconds, int
	//OMNI_DB_EVENTUPDATED_ID		12 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
			if(pParent)	pszEncoded[OMNI_DB_PARENTID_ID] =  pdb->EncodeQuotes(pParent->m_pszReconcileKey);   // unique ID (story ID), varchar 32, recKey on harris


			if(pEvent->m_ulFlags&OMNI_FLAGS_INSERTED)
			{
				// update
				_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
	"UPDATE %s SET \
%s = '%s', \
%s = %d, \
%s = '%s', \
%s = '%s', \
%s = '%s', \
%s = %d, \
%s = %d, \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %d, \
%s = %.03f, \
%s = %d, \
%s = '%s', \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %.0f \
WHERE %s = %d \
AND %s = '%s'", 
							pszTable,
							OMNI_DB_CONNIP_NAME, pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
							OMNI_DB_CONNPORT_NAME, usConnPort,  // of colossus connection, int
							OMNI_DB_EVENTCLIP_NAME,	pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
							OMNI_DB_EVENTTITLE_NAME, pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
							OMNI_DB_EVENTDATA_NAME, pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
							OMNI_DB_EVENTTYPE_NAME, pEvent->m_usType,  // internal colossus type, int
							OMNI_DB_EVENTSTATUS_NAME, pEvent->m_usStatus,  // internal colossus status, int
							OMNI_DB_EVENTTMODE_NAME, pEvent->m_usControl,  // time mode, int
							OMNI_DB_EVENTCALCSTART_NAME, pEvent->m_dblOnAirTime,  // there is no calc...
							OMNI_DB_EVENTSTART_NAME, pEvent->m_dblOnAirTime, // 
							OMNI_DB_EVENTDURMS_NAME, pEvent->m_ulDurationMS,  // duration in milliseconds, int
							OMNI_DB_EVENTUPDATED_NAME, pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
							
							OMNI_DB_EVENTPOSITION_NAME, pEvent->m_nEventPosition,
							OMNI_DB_PARENTID_NAME, pszEncoded[OMNI_DB_PARENTID_ID]?pszEncoded[OMNI_DB_PARENTID_ID]:"",
							OMNI_DB_PARENTPOS_NAME, pParent?pParent->m_nEventPosition:-1,
							OMNI_DB_PARENTSTART_NAME, pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL 
							OMNI_DB_PARENTCALCSTART_NAME, pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL
							OMNI_DB_PARENTDURMS_NAME, pParent?pParent->m_ulDurationMS:-1, // int 
							OMNI_DB_PARENTCALCEND_NAME, pParent?(pParent->m_dblOnAirTime + ((double)(pParent->m_ulDurationMS))/1000.0):-1.0, // decimal(20,3)
							OMNI_DB_EVENTCALCEND_NAME, (pEvent->m_dblOnAirTime + ((double)(pEvent->m_ulDurationMS))/1000.0), // decimal(20,3)
							OMNI_DB_EVENTFSJ70_NAME, pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
							OMNI_DB_LISTID_NAME, m_usListID,
							OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
							);

	//		AfxMessageBox(szSQL);
			}
			else
			{ // must insert

				// but first have to delete in case there is some problem, dont want repeated events inserted, 
				// we are no longer checking for presence in db, its all memory now.
				if(!g_actives.m_bClearEventsInit)  //  but only if we didn't clear stuff out at the beginning.
				{
					_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
										"DELETE FROM %s \
WHERE %s = %d \
AND %s = '%s'", 
										pszTable,
										OMNI_DB_LISTID_NAME, m_usListID,
										OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
										);

					if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
					{
						FILE* fp = fopen(pConn->m_pszDebugFile, "at");
						if(fp)
						{
							fprintf(fp, "Delete before Insert SQL: %s\r\n", pConn->m_szSQL);
							fflush(fp);
							fclose(fp);
						}
					}

					if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
					if(pdb->ExecuteSQL(pdbConn, pConn->m_szSQL, pConn->m_errorstring)<DB_SUCCESS)
					{
						if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
						{
							FILE* fp = fopen(pConn->m_pszDebugFile, "at");
							if(fp)
							{
								fprintf(fp, "Delete before Insert ERROR: %s\r\n", pConn->m_errorstring);
								fflush(fp);
								fclose(fp);
							}
						}
					}
					if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
				}

				_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
"INSERT INTO %s (\
%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (\
'%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %.03f, %.03f, %d, %.03f, %d, '%s', %d, %.03f, %.03f, %d, %.03f, %.03f, %.0f)", 
									pszTable,
									OMNI_DB_CONNIP_NAME,  // of colossus connection, varchar 16
									OMNI_DB_CONNPORT_NAME,  // of colossus connection, int
									OMNI_DB_LISTID_NAME,			// omnibus stream number, int
									OMNI_DB_EVENTID_NAME,   // unique ID (story ID), varchar 32, recKey on harris
									OMNI_DB_EVENTCLIP_NAME,  // clip ID, varchar 32
									OMNI_DB_EVENTTITLE_NAME, // title, varchar 64
									OMNI_DB_EVENTDATA_NAME,  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
									OMNI_DB_EVENTTYPE_NAME,  // internal colossus type, int
									OMNI_DB_EVENTSTATUS_NAME,  // internal colossus status, int
									OMNI_DB_EVENTTMODE_NAME,  // time mode, int
									OMNI_DB_EVENTCALCSTART_NAME,//		"event_calc_start"
									OMNI_DB_EVENTSTART_NAME, 
									OMNI_DB_EVENTDURMS_NAME,// duration in milliseconds, int
									OMNI_DB_EVENTUPDATED_NAME, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
									OMNI_DB_EVENTPOSITION_NAME,//		"event_position" // int NOT NULL 
									OMNI_DB_PARENTID_NAME,//					"parent_id" // varchar(32) NULL 
									OMNI_DB_PARENTPOS_NAME,//				"parent_position" // int NULL
									OMNI_DB_PARENTSTART_NAME,//			"parent_start" // decimal(20,3) NULL 
									OMNI_DB_PARENTCALCSTART_NAME,//	"parent_calc_start" // decimal(20,3) NULL
									OMNI_DB_PARENTDURMS_NAME,//			"parent_duration" // int 
									OMNI_DB_PARENTCALCEND_NAME,//		"parent_calc_end" // decimal(20,3)
									OMNI_DB_EVENTCALCEND_NAME,//			"event_calc_end" // decimal(20,3)
									OMNI_DB_EVENTFSJ70_NAME, // frames since Jan 1970, SQL float (C++ double)
									pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
									usConnPort,  // of colossus connection, int
									m_usListID,
									pszEncoded[OMNI_DB_EVENTID_ID]?pszEncoded[OMNI_DB_EVENTID_ID]:"",
									pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
									pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
									pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
									pEvent->m_usType,  // internal colossus type, int
									pEvent->m_usStatus,  // internal colossus status, int
									pEvent->m_usControl,  // time mode, int
									pEvent->m_dblOnAirTime, // no calc, just insert same
									pEvent->m_dblOnAirTime, // 
									pEvent->m_ulDurationMS,  // duration in milliseconds, int
									pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
									pEvent->m_nEventPosition,
									pszEncoded[OMNI_DB_PARENTID_ID]?pszEncoded[OMNI_DB_PARENTID_ID]:"",
									pParent?pParent->m_nEventPosition:-1,
									pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL 
									pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL
									pParent?pParent->m_ulDurationMS:-1, // int 
									pParent?(pParent->m_dblOnAirTime + ((double)(pParent->m_ulDurationMS))/1000.0):-1.0, // decimal(20,3)
									(pEvent->m_dblOnAirTime + ((double)(pEvent->m_ulDurationMS))/1000.0), // decimal(20,3)
									pEvent->m_dblOnAirTimeInternal  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
									);


			}


			int nIndex=0;

			if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				FILE* fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "%d %s UpdateEvent SQL: %s\r\n", clock(), pszConnServer, pConn->m_szSQL);
					fflush(fp);
					fclose(fp);
				}
			}

			if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
			try
			{
				nIndex = pdb->ExecuteSQL(pdbConn, pConn->m_szSQL, pConn->m_errorstring);
			}
			catch(...)
			{
				nIndex = DB_ERROR_EX;
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent had an exception: %s\r\n", clock(), pConn->m_errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			if(nIndex<DB_SUCCESS)
			{
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent ERROR: %s\r\n", clock(), pConn->m_errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			else
			{
				pEvent->m_ulFlags |= OMNI_FLAGS_INSERTED;
				pEvent->m_ulFlags &= ~OMNI_FLAGS_CHANGED;
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);

				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent returned %d\r\n", clock(), nIndex);
						fflush(fp);
						fclose(fp);
					}
				}

			}

/*
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent freeing\r\n", clock());
						fflush(fp);
						fclose(fp);
					}
				}
*/				
			for (i=0; i<OMNI_DB_APPDATA_ID; i++)
			{
				if(pszEncoded[i]){ try{ free(pszEncoded[i]);} catch(...){} }
			}
/*
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "%d UpdateEvent freed\r\n", clock());
						fflush(fp);
						fclose(fp);
					}
				}
*/
		}
	}	
	return nRV;
}

int CCAList::CheckDatabaseEvents(int nStartIndex, int nLimit)
{
	CCAConn* pConn = (CCAConn*)m_pConn;
	if((pConn)&&(pConn->m_nAsyncDatabaseLookaheadSec>0)&&(m_ppEvents)&&(m_usEventCount>0))
	{
		int j=0;
		int i=min(m_nLastDatabaseEventIndex+1, m_usEventCount-1);
		int n=0;
		int u=0;
		if((nStartIndex>=0)&&(nStartIndex<m_usEventCount)) i=nStartIndex; // override

		_timeb timestamp;
		_ftime( &timestamp );
		double dblLimit = ((double)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0) +pConn->m_nAsyncDatabaseLookaheadSec )) +	((double)(timestamp.millitm))/1000.0;
		switch(pConn->m_ulFlags&OMNI_FRAMEBASISMASK)
		{
		case OMNI_PAL://											0x00000010  // PAL	(25 fps)
			{
				dblLimit *= 25.0;
			} break;
		case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
			{
				dblLimit *= 30.0;
			} break;
		default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
			{
				dblLimit *= 29.97;
			}	break;
		}

		m_tmbLastDatabaseCheck.time=timestamp.time;
		m_tmbLastDatabaseCheck.millitm=timestamp.millitm;

EnterCriticalSection(&m_critEvents);

		while(j<m_usEventCount)
		{
			if(m_ppEvents[i])
			{
				if((m_ppEvents[i]->m_ulFlags&OMNI_FLAGS_CHANGED)||(!(m_ppEvents[i]->m_ulFlags&OMNI_FLAGS_INSERTED)))
				{
					u++;
					if(m_ppEvents[i]->m_dblOnAirTimeInternal < dblLimit)
					{
						int nRV = UpdateDatabaseEvent(m_ppEvents[i], pConn->m_pdb, pConn->m_pdbConn, pConn->m_pszEventsTable, pConn->m_pszServerAddress, pConn->m_usPort);
						if(nRV==OMNI_SUCCESS)
						{
							u--;
							m_nLastDatabaseEventIndex = i;
							n++;
						}
						if((nLimit>0)&&(n>=nLimit))
						{
LeaveCriticalSection(&m_critEvents);
							return n;
						}
					}
					else
					{
						m_ulFlags |= OMNI_LIST_UNCOMMITTED_EXISTS;
					}
				}
			}
			i++;  if(i>=m_usEventCount) i=0;
			j++;
		}

		if ((u==0)&&(j>=m_usEventCount)) //we went all the way around and found nothing left.
		{
			m_ulFlags &= ~OMNI_LIST_UNCOMMITTED_EXISTS;
			m_nLastDatabaseEventIndex = -1;
		}
LeaveCriticalSection(&m_critEvents);
	

		return n;
	}
	return OMNI_ERROR;
}


int CCAList::UpdateListStatus(bool bForce)
{
	CCAConn* pConn = (CCAConn*)m_pConn;

	if((pConn)&&(pConn->m_pdb)&&(pConn->m_pdbConn))
	{
		int nReturn = OMNI_SUCCESS;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];

		_timeb timestamp;
		_ftime( &timestamp );

		if((pConn->m_nMinimumListUpdateIntervalMS>0)&&(!bForce)) // if forcing, dont do the timecheck 2.1.1.18
		{
			if(
					(m_tmbLastUpdate.time + (pConn->m_nMinimumListUpdateIntervalMS/1000) > timestamp.time )
				||(
						( (((double)(m_tmbLastUpdate.time))*1000.0) + ((double)(pConn->m_nMinimumListUpdateIntervalMS + m_tmbLastUpdate.millitm)) ) >
						( (((double)(timestamp.time))*1000.0) + ((double)(timestamp.millitm)) ) 
					)
				)
			{
				return OMNI_ESCAPE;
			}
		}

		nReturn = OMNI_SUCCESS;
		if((pConn->m_pszExchangeTable)&&(strlen(pConn->m_pszExchangeTable)))
		{

//		EnterCriticalSection (&m_critCounter);
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('List_Status', '%s:%05d|%d%03d|%d|%d|%d|%d', %d)",
				pConn->m_pszExchangeTable,
				pConn->m_pszServerAddress,
				pConn->m_usPort,
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				m_ulCounter,				// incremented whenever list size changes  , not really.  just any list change.
				m_ulCounter,				// incremented whenever an event changes
				m_ulFlags,			// incremented when list related values are changed. not really, just the flags.   but if they change....
				m_usEventCount,					// number of events in list 
				0 // list state.... but dont really have anything...
				);
//		LeaveCriticalSection (&m_critCounter);


			if(pConn->m_pcritSQL) EnterCriticalSection(pConn->m_pcritSQL);
			if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				nReturn |= 0x0100;
				if(pConn->m_pcritSQL) LeaveCriticalSection(pConn->m_pcritSQL);
				if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "UpdateListStatus ERROR: %s\r\n", errorstring);
						fflush(fp);
						fclose(fp);
					}

				}
			}
			else
			{
				if(pConn->m_pcritSQL) LeaveCriticalSection(pConn->m_pcritSQL);
	//			return OMNI_SUCCESS;
				nReturn |= 1;
			}
		}
		if((pConn->m_pszChannelsTable)&&(strlen(pConn->m_pszChannelsTable)))
		{

//		EnterCriticalSection (&m_critCounter);
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
conn_ip = '%s', \
conn_port = %d, \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
ID = %d",
				pConn->m_pszChannelsTable,
				pConn->m_pszServerAddress, 
				pConn->m_usPort,
				m_ulFlags, // list state.... but dont really have anything... so flags will indicate connected or not.
				m_ulCounter,				// incremented whenever list size changes  , not really.  just any list change.
				m_ulCounter,				// incremented whenever an event changes
				m_ulFlags,			// incremented when list related values are changed. not really, just the flags.   but if they change....
				m_usEventCount,					// number of events in list 
				m_usEventCount,					// lookahead value.. no such thing so use count

				(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,

				m_usListID
				);
//		LeaveCriticalSection (&m_critCounter);

		
			if(pConn->m_pcritSQL) EnterCriticalSection(pConn->m_pcritSQL);
			if(pConn->m_pdb->ExecuteSQL(pConn->m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				nReturn |= 0x0200;
				if(pConn->m_pcritSQL) LeaveCriticalSection(pConn->m_pcritSQL);
				if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "UpdateListStatus ERROR: %s\r\n", errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			else
			{
				if(pConn->m_pcritSQL) LeaveCriticalSection(pConn->m_pcritSQL);
	//			return OMNI_SUCCESS;
				m_tmbLastUpdate.time = timestamp.time;
				m_tmbLastUpdate.millitm = timestamp.millitm;
//				nReturn =  OMNI_SUCCESS;
				if(nReturn!=OMNI_ERROR) nReturn |= 2;
			} 

		}
		return nReturn;
//		return OMNI_SUCCESS;  // one success gives total success... prob want to change this.
	}
	return OMNI_BADARGS;
}

 

int CCAList::DeleteAllEvents(CDBUtil*	pdb, CDBconn* pdbConn,	char*	pszTable)
{
	if((m_ppEvents)&&(m_usEventCount>0))
	{
//	EnterCriticalSection (&m_critGEI);


		if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
		{
		// we are using DB, so remove!

			char szSQL[DB_SQLSTRING_MAXLEN];
			char errorstring[DB_ERRORSTRING_LEN];
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
								"DELETE FROM %s WHERE %s = %d", 
								pszTable,
								OMNI_DB_LISTID_NAME, m_usListID
								);


			CCAConn* pConn = (CCAConn*) m_pConn;

			if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				FILE* fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "DeleteAllEvents SQL: %s\r\n", szSQL);
					fflush(fp);
					fclose(fp);
				}
			}

			if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
			if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "DeleteAllEvents ERROR: %s\r\n", errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);

		}

		// remove all
		EnterCriticalSection(&m_critEvents);
		int nEventCount = ((int)(m_usEventCount));
		m_usEventCount = 0; // clear the events before deleting so no outside access.
		LeaveCriticalSection(&m_critEvents);

		EnterCriticalSection(&m_critEvents);
		if(m_ppEvents)
		{
			int i=0;
			for(i=0;i<nEventCount;i++)
			{
				if(m_ppEvents[i]) { try{ delete m_ppEvents[i]; } catch(...){} }
				m_ppEvents[i] = NULL;
			}
			try{ delete [] m_ppEvents; } catch(...){}
			m_ppEvents = NULL;
		}
		LeaveCriticalSection(&m_critEvents);

//		EnterCriticalSection (&m_critCounter);
		if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
		m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
	}
	m_nLastEventIndex = -1;
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_SUCCESS;
}


// be sure to critical section this outside, before accessing
int CCAList::DeleteEvent(int nIndex, CDBUtil*	pdb, CDBconn* pdbConn,	char*	pszTable)
{
	if((m_ppEvents)&&(m_usEventCount>0)&&(nIndex>=0)&&(nIndex<m_usEventCount)&&(m_ppEvents[nIndex]))
	{
//	EnterCriticalSection (&m_critGEI);
		CCAConn* pConn = (CCAConn*) m_pConn;

		if((pConn)&&(pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
		{
		// we are using DB, so remove!
			char* pszEncoded = NULL;

			pszEncoded =  pdb->EncodeQuotes(m_ppEvents[nIndex]->m_pszReconcileKey);    // unique ID (story ID), varchar 32, recKey on harris

			if(pszEncoded)
			{

				_snprintf(pConn->m_szSQL, DB_SQLSTRING_MAXLEN-1,
									"DELETE FROM %s \
WHERE %s = %d \
AND %s = '%s'", 
									pszTable,
									OMNI_DB_LISTID_NAME, m_usListID,
									OMNI_DB_EVENTID_NAME, pszEncoded
									);


				if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "DeleteEvent SQL: %s\r\n", pConn->m_szSQL);
						fflush(fp);
						fclose(fp);
					}
				}

				if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
				if(pdb->ExecuteSQL(pdbConn, pConn->m_szSQL, pConn->m_errorstring)<DB_SUCCESS)
				{
					if((pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
					{
						FILE* fp = fopen(pConn->m_pszDebugFile, "at");
						if(fp)
						{
							fprintf(fp, "DeleteEvent ERROR: %s\r\n", pConn->m_errorstring);
							fflush(fp);
							fclose(fp);
						}
					}
				}
				if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);

				free(pszEncoded);
			}
		}

//CCAConn* pConn = (CCAConn*) m_pConn;

//if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile))){	FILE* fp = fopen(pConn->m_pszDebugFile, "at");
//	if(fp)	{	fprintf(fp, "DeleteEvent entering critical section.\r\n");	fflush(fp);	fclose(fp);	}}

		// remove it
EnterCriticalSection(&m_critEvents);
if(m_ppEvents[nIndex]) { try{ delete m_ppEvents[nIndex];} catch(...){} }
		m_ppEvents[nIndex] = NULL;
		m_usEventCount--;
//if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile))){	FILE* fp = fopen(pConn->m_pszDebugFile, "at");
//	if(fp)	{	fprintf(fp, "DeleteEvent subtracted.\r\n");	fflush(fp);	fclose(fp);	}}


		int nCount = ((int)(m_usEventCount));

		CCAEvent** ppEvents = NULL;
		if(nCount>0)
		{
			ppEvents = new CCAEvent*[nCount];
		}
		else
		{
			try{ delete [] m_ppEvents;} catch(...){} 
			m_ppEvents = NULL;
//		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
LeaveCriticalSection(&m_critEvents);
//	LeaveCriticalSection (&m_critGEI);
			return OMNI_SUCCESS;
		}

		if(ppEvents)
		{
//if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile))){	FILE* fp = fopen(pConn->m_pszDebugFile, "at");
//	if(fp)	{	fprintf(fp, "DeleteEvent new array.\r\n");	fflush(fp);	fclose(fp);	}}
			if(m_ppEvents)
			{
				if(nIndex>0) memcpy(ppEvents, m_ppEvents, sizeof(CCAEvent*)*nIndex);
				while(nIndex<nCount)
				{
					ppEvents[nIndex] = m_ppEvents[nIndex+1];
					nIndex++;
				}
				try{ delete [] m_ppEvents;} catch(...){}
			}
			m_ppEvents = ppEvents;
		}
		else  // could not alloc, so just shuffle
		{
//if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile))){	FILE* fp = fopen(pConn->m_pszDebugFile, "at");
//	if(fp)	{	fprintf(fp, "DeleteEvent shuffling array.\r\n");	fflush(fp);	fclose(fp);	}}
			if(m_ppEvents)
			{
				while(nIndex<nCount)
				{
					m_ppEvents[nIndex] = m_ppEvents[nIndex+1];
					nIndex++;
				}
				m_ppEvents[nIndex] = NULL;
			}
		}
//		EnterCriticalSection (&m_critCounter);
		if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
		m_ulCounter++;
//		LeaveCriticalSection (&m_critCounter);
//if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile))){	FILE* fp = fopen(pConn->m_pszDebugFile, "at");
//	if(fp)	{	fprintf(fp, "DeleteEvent leaving critical section.\r\n");	fflush(fp);	fclose(fp);	}}
LeaveCriticalSection(&m_critEvents);
//	LeaveCriticalSection (&m_critGEI);
		return OMNI_SUCCESS;
	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_ERROR;
}

int CCAList::SetFrameBasis(unsigned long ulFrameBasis)
{
	if(ulFrameBasis&OMNI_FRAMEBASISMASK)
	{
		m_ulFlags |= (ulFrameBasis&OMNI_FRAMEBASISMASK);
		m_ulFlags |= OMNI_FRAMEBASISSET;

		return OMNI_SUCCESS;
	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_ERROR;
}
//////////////////////////////////////////////////////////////////////
// CCAConn Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAConn::CCAConn()
{
	m_tmbLastUpdate.time = 0;
	m_tmbLastUpdate.millitm = 0;
	InitializeCriticalSection (&m_crit);
	InitializeCriticalSection (&m_critCounter);
	InitializeCriticalSection (&m_critOperations);
	InitializeCriticalSection (&m_critAux);
	pAux=NULL;
	
//	InitializeCriticalSection (&m_critRLI);
//	InitializeCriticalSection (&m_critUL);

	m_socket = NULL;
	m_nMessageEventCountLimit=-1;

	// used to connect:
	m_pszServerName = NULL;				// friendly name for the connection to the server.  not used programmatically
	m_pszServerAddress = NULL;		// can be host name or IP address
	m_usPort = 10540;							// port to connect
	m_ulFlags = OMNI_NTSC;				// connection options
	m_bKillThread = true;  // kill the comm thread
	m_bThreadStarted = false;  // state of comm thread
	m_bUseResponse = false;
	m_bWriteXMLtoFile = false;
	m_bLightMode = false;

// supplied from automation system
	m_usRefJulianDate=0;	// compiled from "current time" offset from January 1, 1900
	m_ulRefTimeMS=0;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
	m_ulRefUnixTime=0;		// compiled from "current time".

// assigned internally
	m_ppList = NULL;			// need to dynamically allocate.
	m_usListCount = 0;
	m_pTempList = NULL;		// a place to store events temporarily.  volatile.
	m_ulRefTick=0;					// last status get.

	m_ulCounter = 0;
	m_ulConnectionRetries = LONG_MAX-1;		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
	m_ulConnectionInterval = 30;	// how long to wait in seconds before retrying
	m_ulInterEventIntervalMS=1;   	// time to stall between committing each event to the database
	m_ulThreadDelayMS = 1000;

	m_pszDebugFile=NULL;   // debug file
	m_pszCommFile=NULL;	   // comm log file

//////////////////////////////////////////////////////////////////////
// if these are null we dont do them
	m_pdb = NULL;  // pointer to a db util object to use for operations
	m_pdbConn = NULL;  // pointer to a db connection
	m_pszEventsTable = NULL; // name of the table to update
	m_pszExchangeTable = NULL; // name of the table to update
	m_pszConnectionsTable = NULL; // name of the table to update
	m_pszChannelsTable = NULL; // name of the table to update
	m_lpfnSendMsg = NULL;
	m_pcritSendMsg =  NULL;
	m_pcritSQL = NULL;
	m_pcritMem = NULL;

//
//////////////////////////////////////////////////////////////////////
	
	m_nNumFields = 0;
	m_ppfi = NULL;  // dynamic array of field info.

	m_bUseUTC = true;
	m_bUseDST = false;

	m_ulCheckInterval=0;	// number of seconds between checks (0 turns off)
	m_ulExpiryPeriod=10800;	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
	m_ulLastPurge = 0;
	m_ulConnTimeout = 120;
	m_ulConnLastMessage = 0;

	m_nLookaheadTimeLimitSec=-1;  // Seconds from "now" to not process adds and updates. discard the rest.  Deletions are not filtered.
	m_nLookbehindTimeLimitSec=-1;  // Seconds before "now" to not process adds and updates. discard the rest.  Deletions are not filtered.

	m_nAsyncDatabaseLookaheadSec=3600;  // immediately commit to the DB any events that have an on air time of "now" plus this number of seconds, or prior. otherwise commit events as they come into the window.
	m_nAsyncDatabaseCheckIntervalMS=5000;  // only check for uncommitted events every so often.
	m_nMinimumListUpdateIntervalMS=5000;  // prevent database updates on list status faster than this interval
	m_nMinimumServerUpdateIntervalMS=5000;  // prevent database updates on server status faster than this interval
	m_bPreventServerTimeFromEvents=true;			// ignore the current time reference within events.

	m_ppchXMLQueue=NULL; // queue up XML items.
	m_nXMLQueue=0;
	InitializeCriticalSection (&m_critXMLQueue);
	m_bXMLQueueStarted=false;
	m_bXMLQueueKill=true;

}

CCAConn::~CCAConn()
{
	m_bXMLQueueKill=true;
	m_bKillThread = true;  // kill the comm thread
//	DisconnectServer();  // if already disconnected, no harm
	// wait!
/*
	while(m_bThreadStarted)
	{
		Sleep(1);
	}
*/
	try
	{
		if(m_pszServerName) free(m_pszServerName); m_pszServerName=NULL;
		if(m_pszServerAddress) free(m_pszServerAddress); m_pszServerAddress=NULL;
		EnterCriticalSection (&m_crit);
	//	EnterCriticalSection (&m_critRLI);
	//	EnterCriticalSection (&m_critUL);
		if(m_ppList)
		{
			for(int i=0;i<m_usListCount;i++)
			{
				if(m_ppList[i]) delete m_ppList[i];
				m_ppList[i] = NULL;
			}
			delete [] m_ppList;
			m_ppList = NULL;
		}
		if(m_pTempList)
		{
			delete m_pTempList;
			m_pTempList = NULL;
		}
		LeaveCriticalSection (&m_crit);
//	LeaveCriticalSection (&m_critUL);
//	LeaveCriticalSection (&m_critRLI);

		DeleteCriticalSection (&m_critCounter);
		DeleteCriticalSection (&m_crit);
	//	DeleteCriticalSection (&m_critUL);
	//	DeleteCriticalSection (&m_critRLI);
		if(m_pszDebugFile) free(m_pszDebugFile);
		if(m_pszCommFile) free(m_pszCommFile);



		// this is right, it is allocated new for the one connection
		if(m_pcritSQL) 		DeleteCriticalSection(m_pcritSQL);


//	if(m_pszEventsTable) free(m_pszEventsTable);  // table name set from outside, freed outside, dont do here
//	if(m_pszExchangeTable) free(m_pszExchangeTable);  // table name set from outside, freed outside, dont do here
//	if(m_pszConnectionsTable) free(m_pszConnectionsTable);  // table name set from outside, freed outside, dont do here


			// clean up
		if(m_ppfi)
		{
			for(int i=0; i<m_nNumFields; i++)
			{
				if(m_ppfi[i])
				{
					delete m_ppfi[i];
				}
			}
			delete [] m_ppfi;
		}
	}
	catch(...)
	{
	}

	m_bXMLQueueKill=true;
	while(m_bXMLQueueStarted)
	{
		Sleep(1);
	}

	EnterCriticalSection (&m_critXMLQueue);
	if(m_ppchXMLQueue)
	{
		for(int i=0; i<m_nXMLQueue; i++)
		{
			if(m_ppchXMLQueue[i])
			{
				delete [] m_ppchXMLQueue[i];  // char array.
			}
		}
		delete [] m_ppchXMLQueue;
	}
	LeaveCriticalSection (&m_critXMLQueue);

	m_ppchXMLQueue=NULL; // queue up XML items.
	m_nXMLQueue=0;
	DeleteCriticalSection (&m_critXMLQueue);
	DeleteCriticalSection (&m_critOperations);
	DeleteCriticalSection (&m_critAux);


}

int CCAConn::KillChannelThreads()
{

	unsigned short i=0;
	while(i<m_usListCount)
	{
		if(m_ppList[i])
		{
			m_ppList[i]->m_bXMLQueueKill=true;

			while( m_ppList[i]->m_bXMLQueueStarted) Sleep(1);
		}
		i++;
	}

	return i;
}




int CCAConn::AddMessage(char* pchMessage, bool bInitialConnection)
{
	if(pchMessage==NULL) return OMNI_ERROR;
//	EnterCriticalSection(&m_critXMLQueue);
	if(m_ppchXMLQueue == NULL) {m_nXMLQueue=0;}
	char** xx = NULL;
	xx=new char*[m_nXMLQueue+1];
	if(xx)
	{
		if((m_ppchXMLQueue)&&(m_nXMLQueue))
		{
			memcpy(xx, m_ppchXMLQueue, m_nXMLQueue*sizeof(char*));
		}
		if(m_ppchXMLQueue){delete [] m_ppchXMLQueue;}
		m_ppchXMLQueue = xx;
	}
	
	if(m_ppchXMLQueue)
	{

if(m_ulFlags&OMNI_DEBUG)
{
	_timeb timestamp;
	_ftime(&timestamp);
	char filename[256];
	sprintf(filename, "Logs\\omni-%s.txt", m_pszServerAddress);
	FILE* fp = fopen(filename, "ab");
	if(fp)
	{
		char xml[256];
		memcpy(xml, pchMessage, min(255,strlen(pchMessage)) );
		xml[min(255,strlen(pchMessage))]=0;

		fprintf(fp, "%d.%03d queue now %05d, ADD %d chars [%s]\r\n", timestamp.time, timestamp.millitm, m_nXMLQueue+1, strlen(pchMessage), xml);
		fclose(fp);
	}
}
		if((m_ulFlags&OMNI_COMPAREONCONNECT)&&(bInitialConnection))
		{

// 000 0001	001	1	01	SOM	SOH	?	^A		Start of Heading
// 000 0010	002	2	02	EOA	STX	?	^B		Start of Text
			if(*pchMessage == '<') *pchMessage = 0x01; // set it to 1, we replace with < later.
			else *pchMessage = 0x02; // something other than 1, but not in the normal printable XML range.
		}
		m_ppchXMLQueue[m_nXMLQueue]=pchMessage;
		m_nXMLQueue++;

	}


//	LeaveCriticalSection(&m_critXMLQueue);
	return OMNI_SUCCESS;
}


char* CCAConn::ReturnMessage()
{
	char filename[256];
	char* xx = NULL;
//	EnterCriticalSection(&m_critXMLQueue);
	if(m_ppchXMLQueue == NULL) {m_nXMLQueue=0; return xx;}
		
	xx = m_ppchXMLQueue[0];

	int i=0;
	while(i<m_nXMLQueue-1)
	{
		m_ppchXMLQueue[i] = m_ppchXMLQueue[i+1];
		i++;
	}
	m_ppchXMLQueue[i]=NULL;
	m_nXMLQueue--;

if(m_ulFlags&OMNI_DEBUG)
{
	_timeb timestamp;
	_ftime(&timestamp);
	sprintf(filename, "Logs\\omni-%s.txt", m_pszServerAddress);
	FILE* fp = fopen(filename, "ab");
	if(fp)
	{
		char xml[256];
		memcpy(xml, xx, min(255,strlen(xx)));
		xml[min(255,strlen(xx))]=0;

		if(*xml != '<')
		{ 
			if(*xml == 0x01) *xml = '<';
			else *xml = '%';
		}

		fprintf(fp, "%d.%03d queue now %05d, REM %d chars [%s]\r\n", timestamp.time, timestamp.millitm, m_nXMLQueue, strlen(xx), xml);
		fclose(fp);
	}
}

	if(m_nXMLQueue == 0)
	{
		delete [] m_ppchXMLQueue;
		m_ppchXMLQueue = NULL;
	}

//	LeaveCriticalSection(&m_critXMLQueue);
	return xx;
}



int CCAConn::CheckAllListsForUncommitted(bool* pbXMLQueueKill)
{
	unsigned short i=0;
	if((m_ppList==NULL)||(pbXMLQueueKill==NULL)) return -1;
EnterCriticalSection(&m_crit);
	while((i<m_usListCount)&&(*pbXMLQueueKill == false))
	{
		if(m_ppList[i])
		{
			if(m_ppList[i]->m_ulFlags&OMNI_LIST_UNCOMMITTED_EXISTS)  // this ony gets set if m_nAsyncDatabaseLookaheadSec > 0
			{
				_timeb timestamp;
				_ftime(&timestamp);

				if(
						(*pbXMLQueueKill == false)
						&&
						(!(
							(m_ppList[i]->m_tmbLastDatabaseCheck.time + (m_nAsyncDatabaseCheckIntervalMS/1000) > timestamp.time )
						||(
								( (((double)(m_ppList[i]->m_tmbLastDatabaseCheck.time))*1000.0) + ((double)(m_nAsyncDatabaseCheckIntervalMS + m_ppList[i]->m_tmbLastDatabaseCheck.millitm)) ) >
								( (((double)(timestamp.time))*1000.0) + ((double)(timestamp.millitm)) ) 
							)
						) )
					)
				{
					m_ppList[i]->CheckDatabaseEvents();// start at last known index, do whole list
				}	
			}
		}

		i++;
	}
LeaveCriticalSection(&m_crit);

	return i;
}



int CCAConn::UpdateServerTime(bool bUseSystemClock)
{
	if((m_pdb)&&(m_pdbConn))
	{
		int nReturn = OMNI_ERROR;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];

		_timeb timestamp;
		_ftime( &timestamp );

		if(bUseSystemClock)
		{
			m_ulRefUnixTime = (unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0));
			m_ulRefTimeMS = ((m_ulRefUnixTime%86400)*1000) + timestamp.millitm;
			m_usRefJulianDate = 25567 + ((unsigned short)(m_ulRefUnixTime/86400));  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
		}

		if(m_nMinimumServerUpdateIntervalMS>0)
		{
			if(
					(m_tmbLastUpdate.time + (m_nMinimumServerUpdateIntervalMS/1000) > timestamp.time )
				||(
						( (((double)(m_tmbLastUpdate.time))*1000.0) + ((double)(m_nMinimumServerUpdateIntervalMS + m_tmbLastUpdate.millitm)) ) >
						( (((double)(timestamp.time))*1000.0) + ((double)(timestamp.millitm)) ) 
					)
				)
			{
				return OMNI_ERROR;
			}
		}


		if((m_pszExchangeTable)&&(strlen(m_pszExchangeTable)))
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%05d|%d%03d', mod = %d WHERE criterion = 'Server_Time' AND flag LIKE '%s:%05d%%'",
				m_pszExchangeTable,
				m_pszServerAddress,
				m_usPort,
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				m_ulRefTimeMS,
				m_pszServerAddress,
				m_usPort
				);

			if(m_pcritSQL) EnterCriticalSection(m_pcritSQL);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				if(m_pcritSQL) LeaveCriticalSection(m_pcritSQL);
				if((m_pszDebugFile)&&(strlen(m_pszDebugFile)))
				{
					FILE* fp = fopen(m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "UpdateServerTime ERROR: %s\r\n", errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			else
			{
				if(m_pcritSQL) LeaveCriticalSection(m_pcritSQL);
	//			return OMNI_SUCCESS;
				nReturn =  OMNI_SUCCESS;
			}
		}
		if((m_pszConnectionsTable)&&(strlen(m_pszConnectionsTable)))
		{

		EnterCriticalSection (&m_critCounter);
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_time = %d%03d, \
server_status = %d, \
server_lists = %d, \
server_changed = %d, \
server_last_update = %d%03d WHERE \
conn_ip = '%s' AND conn_port = %d",
				m_pszConnectionsTable,
				m_ulRefUnixTime, m_ulRefTimeMS%1000,  // milliseconds since jan 1970
				m_bThreadStarted,
 				m_usListCount,
				m_ulCounter,

				(unsigned long)(timestamp.time), //unixtime// - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,

				m_pszServerAddress, m_usPort
				);

		LeaveCriticalSection (&m_critCounter);
			
			if(m_pcritSQL) EnterCriticalSection(m_pcritSQL);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				if(m_pcritSQL) LeaveCriticalSection(m_pcritSQL);

				if((m_pszDebugFile)&&(strlen(m_pszDebugFile)))
				{
					FILE* fp = fopen(m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "UpdateServerTime ERROR: %s\r\n", errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			else
			{
				if(m_pcritSQL) LeaveCriticalSection(m_pcritSQL);
	//			return OMNI_SUCCESS;
				nReturn =  OMNI_SUCCESS;
			}

		}

		if(nReturn ==  OMNI_SUCCESS)
		{
			m_tmbLastUpdate.time = timestamp.time;
			m_tmbLastUpdate.millitm = timestamp.millitm;
		}
		return nReturn;  // one success gives total success... prob want to change this.
	}
	return OMNI_ERROR;
}

int CCAConn::SetCommLogName(char* pszFileName)
{
	if((pszFileName == NULL)||(strlen(pszFileName)<=0))	return OMNI_ERROR;
	if((m_pszServerAddress == NULL)||(strlen(m_pszServerAddress)<=0))	return OMNI_ERROR;

	char filename[MAX_PATH];
	strcpy(filename, "Logs");
	_mkdir(filename);  // if exists already np
	strcat(filename, "\\");
	strcat(filename, ((m_pszServerName!=NULL)&&(strlen(m_pszServerName)>0))? m_pszServerName : m_pszServerAddress);
	_mkdir(filename);  // if exists already np

	char* pch = NULL;
	pch = (char*)malloc(strlen(filename)+strlen(pszFileName)+2);
	if(pch)
	{
		sprintf(pch, "%s\\%s", filename, pszFileName);
		if(m_pszCommFile) free(m_pszCommFile);
		m_pszCommFile = pch;
		return OMNI_SUCCESS;
	}
	return OMNI_ERROR;
}

int CCAConn::SetDebugName(char* pszFileName)
{
	if((pszFileName == NULL)||(strlen(pszFileName)<=0))	return OMNI_ERROR;
	if((m_pszServerAddress == NULL)||(strlen(m_pszServerAddress)<=0))	return OMNI_ERROR;

	char filename[MAX_PATH];
	strcpy(filename, "Logs");
	_mkdir(filename);  // if exists already np
	strcat(filename, "\\");
	strcat(filename, ((m_pszServerName!=NULL)&&(strlen(m_pszServerName)>0))? m_pszServerName : m_pszServerAddress);
	_mkdir(filename);  // if exists already np

	char* pch = NULL;
	pch = (char*)malloc(strlen(filename)+strlen(pszFileName)+2);
	if(pch)
	{
		sprintf(pch, "%s\\%s", filename, pszFileName);
		if(m_pszDebugFile) free(m_pszDebugFile);
		m_pszDebugFile = pch;
		return OMNI_SUCCESS;
	}
	return OMNI_ERROR;
}

int CCAConn::SetFrameBasis(unsigned long ulFrameBasis)
{
	if(ulFrameBasis&OMNI_FRAMEBASISMASK)
	{
	//	char x[356]; sprintf(x,"flags before = %08x", m_ulFlags); AfxMessageBox(x);
		m_ulFlags &= (~OMNI_FRAMEBASISMASK);

		m_ulFlags |= (ulFrameBasis&OMNI_FRAMEBASISMASK);
		m_ulFlags |= OMNI_FRAMEBASISSET;

		//char x[356];
	//	sprintf(x,"flags after = %08x", m_ulFlags); AfxMessageBox(x);

		return OMNI_SUCCESS;
	}
//	LeaveCriticalSection (&m_critGEI);
	return OMNI_ERROR;
}


CCAList* CCAConn::GetList(unsigned short usListIndex)
{
	if(usListIndex<m_usListCount)
	{
		if((m_ppList)&&(m_ppList[usListIndex]))
		{
			return m_ppList[usListIndex];
		}
	}
	return NULL;
}

unsigned short CCAConn::GetListCount()
{
	if(m_ppList)
	{
		return m_usListCount;
	}
	return 0;
}

int CCAConn::ConnectServer()
{
	if((m_pszServerAddress)&&(strlen(m_pszServerAddress)))
	{
		m_bKillThread = false;  // dont kill the comm thread
		
		m_ulFlags &= ~OMNI_SOCKETCONNECTED; // make sure it's disconnected
#ifdef HELIOS_NEW_DB_SCHEMA
		if(_beginthread(HeliosAdaptorCommThread, 0, this)!=-1)
		{
			return OMNI_SUCCESS;
		}
		else m_bKillThread = true;  // kill the comm thread
#else // ! HELIOS_NEW_DB_SCHEMA
		if(_beginthread(CommThread, 0, this)!=-1)
		{
			return OMNI_SUCCESS;
		}
		else m_bKillThread = true;  // kill the comm thread
#endif // ! HELIOS_NEW_DB_SCHEMA

	
	}
	return OMNI_ERROR;
}

int CCAConn::DisconnectServer()
{
	m_bKillThread = true;  // kill the comm thread

	return OMNI_SUCCESS;
}

int CCAConn::ReturnListIndex(unsigned short usListID)
{

/// debug
/*
FILE* fp;
fp = fopen("omnibus.log", "at");


if(fp)
{
	fprintf(fp, "before lookup: listing assignment\r\n");
	fflush(fp);
}

				unsigned short i=0;
				if((m_ppList)&&(m_usListCount))
				{
					while(i<m_usListCount)
					{
if(fp)
{
	fprintf(fp, "looking: %d has 0x%08x\r\n",  i, m_ppList[i]);
	fflush(fp);
}

						i++;
					}
				}
if(fp)
{
	fprintf(fp, "done with pre-lookup listing\r\n");
	fflush(fp);
	fclose(fp);
}
////// end debug
*/

	if((m_ppList)&&(m_usListCount>0))
	{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "checking %d lists for %d\r\n", m_usListCount, usListID);
	fflush(fp);
	fclose(fp);
}
*/
		unsigned short i=0;
		while(i<m_usListCount)
		{
			if(m_ppList[i])
			{

/*fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "checking [%d]:  %d ?= %d\r\n", i, m_ppList[i]->m_usListID, usListID);
	fflush(fp);
	fclose(fp);
}
*/
				if(usListID == m_ppList[i]->m_usListID) return i;
			}
			i++;
		}
	}
	return -1;
}

int CCAConn::UpdateList(unsigned short usListID, char* pszListName, unsigned long ulFlags, int* pnListID)
{
	if(pszListName==NULL) return OMNI_BADARGS;
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "update list called: %s, %d\r\n", pszListName, usListID);
	fflush(fp);
	fclose(fp);
}
*/
//	EnterCriticalSection (&m_critRLI);

	int nIndex=-1;

	if((pnListID)&&(*pnListID>=0))
		nIndex = *pnListID;
	else
		nIndex = ReturnListIndex(usListID);
/*
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "looked up list %s, %d: %d\r\n", pszListName, usListID, nIndex);
	fflush(fp);
	fclose(fp);
}
*/
	if(nIndex>=0)
	{
		if(m_ppList[nIndex]->m_ulFlags != ulFlags)  // this part is OK but the assignement below should only be involving the connected state
		{
		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
			if(m_ppList[nIndex]->m_ulCounter >= ULONG_MAX) m_ppList[nIndex]->m_ulCounter=0;
			m_ppList[nIndex]->m_ulCounter++;
		LeaveCriticalSection (&m_critCounter);
		}

		if(ulFlags&OMNI_LIST_CONNECTED)
			m_ppList[nIndex]->m_ulFlags |= OMNI_LIST_CONNECTED;  // only involving the connected state
		else
			m_ppList[nIndex]->m_ulFlags &= ~OMNI_LIST_CONNECTED;

//		LeaveCriticalSection (&m_critRLI);

		return OMNI_SUCCESS;  // it's already there, we dont overwrite the list name tho.
	}
	else
	{

if((m_pszDebugFile)&&(strlen(m_pszDebugFile)))
{
	FILE* fp = fopen(m_pszDebugFile, "at");
	if(fp)
	{
		fprintf(fp, "%d UpdateList new: %s: %d\r\n", clock(), pszListName, usListID);
		fflush(fp);
		fclose(fp);
	}
}

		// we have to add it.
		CCAList* pList = new CCAList(pszListName, usListID);
		if(pList)
		{
			pList->m_pConn = this;
			pList->m_ulFlags = ulFlags;  // This can be assigned cleanly, immediately, since it is new.
			CCAList** ppList = NULL;
			int nListCount = (int)m_usListCount;
			ppList = new CCAList*[nListCount+1];
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "adding list %s, %d\r\n", pszListName, usListID);
	fflush(fp);
	fclose(fp);
}
*/
			if(ppList)
			{
				int i=0;
				if((m_ppList)&&(nListCount>0))
				{
					while(i<nListCount)
					{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "assigning list 0x%08x to %d\r\n", m_ppList[i], i);
	fflush(fp);
	fclose(fp);
}
*/
						ppList[i] = m_ppList[i];
						i++;
					}
//					try{delete [] m_ppList;} catch(...){}
				}
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "assigning list 0x%08x to %d\r\n", pList, m_usListCount);
	fflush(fp);
	fclose(fp);
}
*/
				ppList[nListCount] = pList; // assign new one
				m_ppList = ppList;

				if(pnListID) *pnListID = (int)m_usListCount;
				m_usListCount++;  // increment after all is set up
				
/// debug
/*
FILE* fp;
fp = fopen("omnibus.log", "at");

if(fp)
{
	fprintf(fp, "listing assignment\r\n");
	fflush(fp);
}
				i=0;
				if((m_ppList)&&(m_usListCount))
				{
					while(i<m_usListCount)
					{
if(fp)
{
	fprintf(fp, "assigned: %d has 0x%08x\r\n",  i, m_ppList[i]);
	fflush(fp);
}

						i++;
					}
				}
if(fp)
{
	fprintf(fp, "done with assignment\r\n");
	fflush(fp);
	fclose(fp);
}
////// end debug
*/

				// signal change
		EnterCriticalSection (&m_critCounter);
				if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
				m_ulCounter++;
		LeaveCriticalSection (&m_critCounter);
//	LeaveCriticalSection (&m_critRLI);

				return OMNI_SUCCESS;  
			}
			else
			{
				// couldnt allocate array!
//	LeaveCriticalSection (&m_critRLI);
				return OMNI_MEMEX;
			}
		}
		else
		{
			// couldnt allocate object!
//	LeaveCriticalSection (&m_critRLI);
			return OMNI_MEMEX;
		}
	}
}


// core - event search
int CCAConn::GetRelativeIndex(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags)
{

	return -1;
}

int CCAConn::GetNumSecondaries(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags)
{

	return -1;
}

// utility
 // returns milliseconds in current day, -1 if error.
int CCAConn::ConvertTime(char* pszTimeString, unsigned long* pulRefUnixTime, unsigned short* pusJulianDate, double* pdblMilliUnix)  // returns milliseconds in current day, -1 if error.
{
	if((pszTimeString)&&(strlen(pszTimeString)))
	{
		int nMillisecondsInDay=-1;

		double dblValue = atof(pszTimeString);  // frames since January 1970.  (I think it means jan 1, 12:00 am (local), but this is how it is in the omnibus documentation)
		/* if iTX, the value is different:

	iTX 1.5 uses standard Windows system ticks to measure time. They are the internal units used by the .NET types System.DateTime and System.TimeSpan.
A system tick is a 100-nano second interval. There are 10,000 ticks in a millisecond. A tick is 0.0000001 of a second.
0 in system ticks represents 12:00:00 midnight, January 1, 0001 Anno Domini (Common Era) as an absolute point in time.

	An important value is 621355968000000000.  this is the number of dateTime ticks from 0001 to 1970.  this offset gets you back to unix time

	*/

		double dblFrameBasis = 29.97;
		switch(m_ulFlags&OMNI_FRAMEBASISMASK)
		{
		case OMNI_PAL://											0x00000010  // PAL	(25 fps)
			{
				dblFrameBasis = 25.0;
			} break;
		case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
			{
				dblFrameBasis = 30.0;
			} break;
		default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
			break;
		}
		unsigned short usJulianDate=0;  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)

		if(m_ulFlags&OMNI_ITX)
		{
			double dblMilliseconds = ((dblValue>621355968000000000.0)?(dblValue - 621355968000000000.0):dblValue)/10000.0;
			if(pulRefUnixTime)
			{
				*pulRefUnixTime = (((unsigned long)(dblMilliseconds/86400000.0)) * 86400);
			}
			usJulianDate=25567 + ((unsigned short)(dblMilliseconds/86400000.0));  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
			nMillisecondsInDay =  (int)( dblMilliseconds - ((double)((unsigned long)(dblMilliseconds/86400000.0)))*86400000.0 );
			if(pdblMilliUnix)
			{
				*pdblMilliUnix = dblMilliseconds/1000.0;  //bam!

			}

		}
		else
		{
//////// one way of doing...
/*
		double dblMilliseconds = (dblFrames*1000.0/dblFrameBasis);
		unsigned short usJulianDate=25567;  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
		while (dblMilliseconds>86400000.0)
		{
			dblMilliseconds-=86400000.0; // need to get the remainder.
			usJulianDate++;  // add up days
		}
*/

//////// another way of doing...
			double dblFrames = dblValue;
			double dblMilliseconds = (dblFrames/(dblFrameBasis*86400.0));
			usJulianDate=25567 + unsigned short(dblMilliseconds);  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
			nMillisecondsInDay = (int)( (dblFrames*1000.0/dblFrameBasis) - (((double)usJulianDate - 25567.0)*86400000.0) );

			//let's get the unixtime (though, not GMT offset, local)
			if((pulRefUnixTime)||(pdblMilliUnix))
			{
				unsigned long  ulRefUnixTime=0;
				double dblSeconds = (dblFrames/dblFrameBasis);
				if(dblSeconds<0.0) 
					ulRefUnixTime = 0;
				else
				{
					while (dblSeconds>(double)0xffffffff) dblSeconds-=(double)0xffffffff; // epoch maxed by size of unsigned long
					ulRefUnixTime = (unsigned long) dblSeconds;
				}
				if(pulRefUnixTime) *pulRefUnixTime = ulRefUnixTime;
				if(pdblMilliUnix) *pdblMilliUnix = (double)ulRefUnixTime + ((double)(nMillisecondsInDay%1000)/1000.0);
			}
		}

		//let's get the julian date
		if(pusJulianDate)
		{
			*pusJulianDate = usJulianDate;
		}
		return nMillisecondsInDay;
	}
	return OMNI_ERROR;
}

int	CCAConn::ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned long ulFlags) 
{
	if(ulFlags&OMNI_HEX)  // means, if BCD
	{
/*
		char temp[32];
		sprintf(temp, "%02x", ucHours);		ucHours = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucMinutes);	ucMinutes = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucSeconds);	ucSeconds = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucFrames);	ucFrames = (unsigned char)(atoi(temp));
*/		
		unsigned char ucTemp = 	(ucHours&0x0f)+(((ucHours&0xf0)>>4)*10); ucHours = ucTemp;
		ucTemp = 	(ucMinutes&0x0f)+(((ucMinutes&0xf0)>>4)*10); ucMinutes = ucTemp;
		ucTemp = 	(ucSeconds&0x0f)+(((ucSeconds&0xf0)>>4)*10); ucSeconds = ucTemp;
		ucTemp = 	(ucFrames&0x0f)+(((ucFrames&0xf0)>>4)*10); ucFrames = ucTemp;
	}
	if((ucHours>23)||(ucHours<0)) return TIME_NOT_DEFINED;
	if((ucMinutes>59)||(ucMinutes<0)) return TIME_NOT_DEFINED;
	if((ucSeconds>59)||(ucSeconds<0)) return TIME_NOT_DEFINED;

	switch(m_ulFlags&OMNI_FRAMEBASISMASK)
	{
	case OMNI_PAL://											0x00000010  // PAL	(25 fps)
		{
			if((ucFrames>24)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/25); //parens for rounding error
		} break;
	case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
		{
			if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/30); //parens for rounding error
		} break;
	default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
		{
			if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
			return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(int)(((double)(ucFrames)*1000.0)/29.97)); //parens for rounding error
		} break;

	}
}

int	CCAConn::ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned long ulFlags)
{
	*pucHours		= ((unsigned char)(ulMilliseconds/3600000L))&0xff;
	*pucMinutes = ((unsigned char)((ulMilliseconds/60000L)%60))&0xff;	
	*pucSeconds = ((unsigned char)(((ulMilliseconds/1000L)%60)%60))&0xff;
	// this fix came from Sentinel
	// frame calc had an issue where you could end up with or .30 frames if the millisecond count
	// was in between 989 and 999 for ntsc
	switch(m_ulFlags&OMNI_FRAMEBASISMASK)
	{
	case OMNI_PAL://											0x00000010  // PAL	(25 fps)
		{
			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/40)))&0xff;  //(int)(1000.0/25.0) = 40
		} break;
	case OMNI_NTSCNDF://									0x00000020  // NTSC no drop frame (30 fps)
		{
//			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/33)))&0xff;  //(int)(1000.0/30.0)  = (int)(33.333333) = 33
		// have to make it reversible.  So anything above 989 gets converted to 29.
	 		*pucFrames	= ((unsigned char)(min(29, ((ulMilliseconds%1000L)/33))))&0xff;  //(int)(1000.0/30.0) = 33
		} break;
	default: //default to NTSC drop frame  OMNI_NTSC  0x00000030  // NTSC drop frame (29.97 fps)
		{
//			*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/33)))&0xff;  //(int)(1000.0/29.97) = (int)(33.366700) = 33
		// have to make it reversible.  So anything above 989 gets converted to 29.
	 		*pucFrames	= ((unsigned char)(min(29, ((ulMilliseconds%1000L)/33))))&0xff;  //(int)(1000.0/30.0) = 33
		} break;

	}

	if(ulFlags&OMNI_HEX)  // means, if BCD
	{
		unsigned char ucTemp = *pucHours;
		*pucHours = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucMinutes;
		*pucMinutes = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucSeconds;
		*pucSeconds = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucFrames;
		*pucFrames = ((ucTemp/10)*16) + (ucTemp%10);
	}
	return OMNI_SUCCESS;  // meaningless
}

/*
#ifdef OLDANDUNUSEDUPDATEEVENT

int CCAList::UpdateEvent(CCAEvent* pEvent, CDBUtil*	pdb, CDBconn* pdbConn,	char*	pszTable, char*	pszConnServer, unsigned short usConnPort)
{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "updating event\r\n");
	fprintf(fp, "%d %d %d [%s]\r\n", (pdb),(pdbConn),(pdbConn->m_bConnected),(pszTable?pszTable:"null"));
	fflush(fp);
	fclose(fp);
}
* /
	if((pEvent==NULL)||(pEvent->m_pszReconcileKey==NULL)||(strlen(pEvent->m_pszReconcileKey)<=0)) return OMNI_BADARGS;
	CCAEvent* pParent = (CCAEvent*) pEvent->m_pParent; 


	if((pdb)&&(pdbConn)&&(pdbConn->m_bConnected)&&(pszTable)&&(strlen(pszTable)))
	{
	// we are using DB, so check it out and update if exists, insert if not

		//we need to encode all the values:

		char* pszEncoded[OMNI_DB_APPDATA_ID]; // only need a few of these
		for (int i=0; i<OMNI_DB_APPDATA_ID; i++)
		{
			pszEncoded[i] = NULL;
		}


		pszEncoded[OMNI_DB_CONNIP_ID] =  pdb->EncodeQuotes(pszConnServer);   // of colossus connection, varchar 16
//OMNI_DB_CONNPORT_ID				1  // of colossus connection, int
//OMNI_DB_LISTID_ID					2			// omnibus stream number, int
		pszEncoded[OMNI_DB_EVENTID_ID] =  pdb->EncodeQuotes(pEvent->m_pszReconcileKey);    // unique ID (story ID), varchar 32, recKey on harris
		pszEncoded[OMNI_DB_EVENTCLIP_ID] =  pdb->EncodeQuotes(pEvent->m_pszID);   // clip ID, varchar 32
		pszEncoded[OMNI_DB_EVENTTITLE_ID] =  pdb->EncodeQuotes(pEvent->m_pszTitle);  // title, varchar 64
		pszEncoded[OMNI_DB_EVENTDATA_ID] =  pdb->EncodeQuotes(pEvent->m_pszData);   // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
//OMNI_DB_EVENTTYPE_ID			7  // internal colossus type, int
//OMNI_DB_EVENTSTATUS_ID		8  // internal colossus status, int
//OMNI_DB_EVENTTMODE_ID			9  // time mode, int
//OMNI_DB_EVENTFSJ70_ID			10 // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
//OMNI_DB_EVENTDURMS_ID			11 // duration in milliseconds, int
//OMNI_DB_EVENTUPDATED_ID		12 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
		if(pParent)	pszEncoded[OMNI_DB_PARENTID_ID] =  pdb->EncodeQuotes(pParent->m_pszReconcileKey);   // unique ID (story ID), varchar 32, recKey on harris

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];

		CCAConn* pConn = (CCAConn*) m_pConn;

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"SELECT %s FROM %s \
WHERE %s = %d \
AND %s = '%s'", 
							OMNI_DB_EVENTID_NAME, pszTable,
							OMNI_DB_LISTID_NAME, m_usListID,
							OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
							);


//AfxMessageBox(szSQL);
		int nNumRecords=0;
		// run the query
		if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);

		CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
		if(prs == NULL) 
		{
			if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				FILE* fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "UpdateEvent Retrieve ERROR: %s\r\n", errorstring);
					fflush(fp);
					fclose(fp);
				}
			}
		}
		else
		{
			while ((!prs->IsEOF()))//&&(nIndex<nCount))
			{
				nNumRecords++;
//				CString szTemp;
//				prs->GetFieldValue(OMNI_DB_EVENTID_ID, szTemp);
//				AfxMessageBox(szTemp);
				prs->MoveNext();
			}
			prs->Close();
			delete prs;   // no longer needed.
			prs = NULL; // reset it.

			// check the items.
		}

		if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);

		if(nNumRecords>0)
		{
#ifdef HELIOS_NEW_DB_SCHEMA
			// update
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"UPDATE %s SET \
%s = '%s', \
%s = %d, \
%s = '%s', \
%s = '%s', \
%s = '%s', \
%s = %d, \
%s = %d, \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %d, \
%s = %.03f, \
%s = %d, \
%s = '%s', \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %d, \
%s = %.03f, \
%s = %.03f, \
%s = %.0f \
WHERE %s = %d \
AND %s = '%s'", 
						pszTable,
						OMNI_DB_CONNIP_NAME, pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
						OMNI_DB_CONNPORT_NAME, usConnPort,  // of colossus connection, int
						OMNI_DB_EVENTCLIP_NAME,	pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
						OMNI_DB_EVENTTITLE_NAME, pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
						OMNI_DB_EVENTDATA_NAME, pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
						OMNI_DB_EVENTTYPE_NAME, pEvent->m_usType,  // internal colossus type, int
						OMNI_DB_EVENTSTATUS_NAME, pEvent->m_usStatus,  // internal colossus status, int
						OMNI_DB_EVENTTMODE_NAME, pEvent->m_usControl,  // time mode, int
						OMNI_DB_EVENTCALCSTART_NAME, pEvent->m_dblOnAirTime,  // there is no calc...
						OMNI_DB_EVENTSTART_NAME, pEvent->m_dblOnAirTime, // 
						OMNI_DB_EVENTDURMS_NAME, pEvent->m_ulDurationMS,  // duration in milliseconds, int
						OMNI_DB_EVENTUPDATED_NAME, pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
						
						OMNI_DB_EVENTPOSITION_NAME, pEvent->m_nEventPosition,
						OMNI_DB_PARENTID_NAME, pszEncoded[OMNI_DB_PARENTID_ID]?pszEncoded[OMNI_DB_PARENTID_ID]:"",
						OMNI_DB_PARENTPOS_NAME, pParent?pParent->m_nEventPosition:-1,
						OMNI_DB_PARENTSTART_NAME, pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL 
						OMNI_DB_PARENTCALCSTART_NAME, pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL
						OMNI_DB_PARENTDURMS_NAME, pParent?pParent->m_ulDurationMS:-1, // int 
						OMNI_DB_PARENTCALCEND_NAME, pParent?(pParent->m_dblOnAirTime + ((double)(pParent->m_ulDurationMS))/1000.0):-1.0, // decimal(20,3)
						OMNI_DB_EVENTCALCEND_NAME, (pEvent->m_dblOnAirTime + ((double)(pEvent->m_ulDurationMS))/1000.0), // decimal(20,3)
						OMNI_DB_EVENTFSJ70_NAME, pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
						OMNI_DB_LISTID_NAME, m_usListID,
						OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
						);
#else // ! HELIOS_NEW_DB_SCHEMA
			// update
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"UPDATE %s SET \
%s = '%s', \
%s = %d, \
%s = '%s', \
%s = '%s', \
%s = '%s', \
%s = %d, \
%s = %d, \
%s = %d, \
%s = %.0f, \
%s = %.03f, \
%s = %d, \
%s = %.03f \
WHERE %s = %d \
AND %s = '%s'", 
						pszTable,
						OMNI_DB_CONNIP_NAME, pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
						OMNI_DB_CONNPORT_NAME, usConnPort,  // of colossus connection, int
						OMNI_DB_EVENTCLIP_NAME,	pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
						OMNI_DB_EVENTTITLE_NAME, pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
						OMNI_DB_EVENTDATA_NAME, pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
						OMNI_DB_EVENTTYPE_NAME, pEvent->m_usType,  // internal colossus type, int
						OMNI_DB_EVENTSTATUS_NAME, pEvent->m_usStatus,  // internal colossus status, int
						OMNI_DB_EVENTTMODE_NAME, pEvent->m_usControl,  // time mode, int
						OMNI_DB_EVENTFSJ70_NAME, pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
						OMNI_DB_EVENTSTART_NAME, pEvent->m_dblOnAirTime, // 
						OMNI_DB_EVENTDURMS_NAME,pEvent->m_ulDurationMS,  // duration in milliseconds, int
						OMNI_DB_EVENTUPDATED_NAME, pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
						OMNI_DB_LISTID_NAME, m_usListID,
						OMNI_DB_EVENTID_NAME, pszEncoded[OMNI_DB_EVENTID_ID]
						);
#endif // ! HELIOS_NEW_DB_SCHEMA

//		AfxMessageBox(szSQL);
		}
		else
		{ // must insert

#ifdef HELIOS_NEW_DB_SCHEMA
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"INSERT INTO %s (\
%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (\
'%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %.03f, %.03f, %d, %.03f, %d, '%s', %d, %.03f, %.03f, %d, %.03f, %.03f, %.0f);", 
								pszTable,
								OMNI_DB_CONNIP_NAME,  // of colossus connection, varchar 16
								OMNI_DB_CONNPORT_NAME,  // of colossus connection, int
								OMNI_DB_LISTID_NAME,			// omnibus stream number, int
								OMNI_DB_EVENTID_NAME,   // unique ID (story ID), varchar 32, recKey on harris
								OMNI_DB_EVENTCLIP_NAME,  // clip ID, varchar 32
								OMNI_DB_EVENTTITLE_NAME, // title, varchar 64
								OMNI_DB_EVENTDATA_NAME,  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
								OMNI_DB_EVENTTYPE_NAME,  // internal colossus type, int
								OMNI_DB_EVENTSTATUS_NAME,  // internal colossus status, int
								OMNI_DB_EVENTTMODE_NAME,  // time mode, int
								OMNI_DB_EVENTCALCSTART_NAME,//		"event_calc_start"
								OMNI_DB_EVENTSTART_NAME, 
								OMNI_DB_EVENTDURMS_NAME,// duration in milliseconds, int
								OMNI_DB_EVENTUPDATED_NAME, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
								OMNI_DB_EVENTPOSITION_NAME,//		"event_position" // int NOT NULL 
								OMNI_DB_PARENTID_NAME,//					"parent_id" // varchar(32) NULL 
								OMNI_DB_PARENTPOS_NAME,//				"parent_position" // int NULL
								OMNI_DB_PARENTSTART_NAME,//			"parent_start" // decimal(20,3) NULL 
								OMNI_DB_PARENTCALCSTART_NAME,//	"parent_calc_start" // decimal(20,3) NULL
								OMNI_DB_PARENTDURMS_NAME,//			"parent_duration" // int 
								OMNI_DB_PARENTCALCEND_NAME,//		"parent_calc_end" // decimal(20,3)
								OMNI_DB_EVENTCALCEND_NAME,//			"event_calc_end" // decimal(20,3)
								OMNI_DB_EVENTFSJ70_NAME, // frames since Jan 1970, SQL float (C++ double)
								pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
								usConnPort,  // of colossus connection, int
								m_usListID,
								pszEncoded[OMNI_DB_EVENTID_ID]?pszEncoded[OMNI_DB_EVENTID_ID]:"",
								pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
								pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
								pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
								pEvent->m_usType,  // internal colossus type, int
								pEvent->m_usStatus,  // internal colossus status, int
								pEvent->m_usControl,  // time mode, int
								pEvent->m_dblOnAirTime, // no calc, just insert same
								pEvent->m_dblOnAirTime, // 
								pEvent->m_ulDurationMS,  // duration in milliseconds, int
								pEvent->m_dblUpdated, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
								pEvent->m_nEventPosition,
								pszEncoded[OMNI_DB_PARENTID_ID]?pszEncoded[OMNI_DB_PARENTID_ID]:"",
								pParent?pParent->m_nEventPosition:-1,
								pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL 
								pParent?pParent->m_dblOnAirTime:-1.0, // decimal(20,3) NULL
								pParent?pParent->m_ulDurationMS:-1, // int 
								pParent?(pParent->m_dblOnAirTime + ((double)(pParent->m_ulDurationMS))/1000.0):-1.0, // decimal(20,3)
								(pEvent->m_dblOnAirTime + ((double)(pEvent->m_ulDurationMS))/1000.0), // decimal(20,3)
								pEvent->m_dblOnAirTimeInternal  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
								);

#else // ! HELIOS_NEW_DB_SCHEMA
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
"INSERT INTO %s (\
%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (\
'%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %.0f, %.03f, %d, %.03f);", 
								pszTable,
								OMNI_DB_CONNIP_NAME,  // of colossus connection, varchar 16
								OMNI_DB_CONNPORT_NAME,  // of colossus connection, int
								OMNI_DB_LISTID_NAME,			// omnibus stream number, int
								OMNI_DB_EVENTID_NAME,   // unique ID (story ID), varchar 32, recKey on harris
								OMNI_DB_EVENTCLIP_NAME,  // clip ID, varchar 32
								OMNI_DB_EVENTTITLE_NAME, // title, varchar 64
								OMNI_DB_EVENTDATA_NAME,  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
								OMNI_DB_EVENTTYPE_NAME,  // internal colossus type, int
								OMNI_DB_EVENTSTATUS_NAME,  // internal colossus status, int
								OMNI_DB_EVENTTMODE_NAME,  // time mode, int
								OMNI_DB_EVENTFSJ70_NAME, // frames since Jan 1970, SQL float (C++ double)
								OMNI_DB_EVENTSTART_NAME, 
								OMNI_DB_EVENTDURMS_NAME,// duration in milliseconds, int
								OMNI_DB_EVENTUPDATED_NAME, // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
								pszEncoded[OMNI_DB_CONNIP_ID]?pszEncoded[OMNI_DB_CONNIP_ID]:"",    // of colossus connection, varchar 16
								usConnPort,  // of colossus connection, int
								m_usListID,
								pszEncoded[OMNI_DB_EVENTID_ID]?pszEncoded[OMNI_DB_EVENTID_ID]:"",
								pszEncoded[OMNI_DB_EVENTCLIP_ID]?pszEncoded[OMNI_DB_EVENTCLIP_ID]:"",  // clip ID, varchar 32
								pszEncoded[OMNI_DB_EVENTTITLE_ID]?pszEncoded[OMNI_DB_EVENTTITLE_ID]:"", // title, varchar 64
								pszEncoded[OMNI_DB_EVENTDATA_ID]?pszEncoded[OMNI_DB_EVENTDATA_ID]:"",  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
								pEvent->m_usType,  // internal colossus type, int
								pEvent->m_usStatus,  // internal colossus status, int
								pEvent->m_usControl,  // time mode, int
								pEvent->m_dblOnAirTimeInternal,  // frames since Jan 1970, SQL float (C++ double) *** changed to unixtime.milliseconds like event_last_update
								pEvent->m_dblOnAirTime, // 
								pEvent->m_ulDurationMS,  // duration in milliseconds, int
								pEvent->m_dblUpdated // time of last update from colossus, SQL real (C++ float): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
								);
//AfxMessageBox(szSQL);
#endif // ! HELIOS_NEW_DB_SCHEMA

		}

		if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
		{
			FILE* fp = fopen(pConn->m_pszDebugFile, "at");
			if(fp)
			{
				fprintf(fp, "UpdateEvent SQL: %s\r\n", szSQL);
				fflush(fp);
				fclose(fp);
			}
		}

		if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
			if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
			{
				FILE* fp = fopen(pConn->m_pszDebugFile, "at");
				if(fp)
				{
					fprintf(fp, "UpdateEvent ERROR: %s\r\n", errorstring);
					fflush(fp);
					fclose(fp);
				}
			}
/*
		//	Sleep(1000);  // try again after a second no good, just exit.  creates timeouts if the db is hosed.
			if((pConn)&&(pConn->m_pcritSQL)) EnterCriticalSection(pConn->m_pcritSQL);
			if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				if((pConn)&&(pConn->m_pszDebugFile)&&(strlen(pConn->m_pszDebugFile)))
				{
					FILE* fp = fopen(pConn->m_pszDebugFile, "at");
					if(fp)
					{
						fprintf(fp, "UpdateEvent ERROR2: %s\r\n", errorstring);
						fflush(fp);
						fclose(fp);
					}
				}
			}
			if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
* /
		}
		else
		{
			if((pConn)&&(pConn->m_pcritSQL)) LeaveCriticalSection(pConn->m_pcritSQL);
		}

		for (i=0; i<OMNI_DB_APPDATA_ID; i++)
		{
			if(pszEncoded[i]) free(pszEncoded[i]);
		}

	}

//	EnterCriticalSection (&m_critGEI);
	int nIndex = GetEventIndex(pEvent->m_pszReconcileKey); // must check if it's there
	if(nIndex>=0)
	{
/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "event found at %d\r\n", nIndex);
	fflush(fp);
	fclose(fp);
}
* /
		//first check if the pointer is not the same
		if(pEvent != m_ppEvents[nIndex])
		{
			// if it's different, switch the thing
EnterCriticalSection(&m_critEvents);
			if(m_ppEvents[nIndex]) delete m_ppEvents[nIndex];
			m_ppEvents[nIndex] = pEvent;
LeaveCriticalSection(&m_critEvents);
		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
		LeaveCriticalSection (&m_critCounter);
		}
//	LeaveCriticalSection (&m_critGEI);
		return OMNI_SUCCESS;  // it's already there, we dont overwrite the list name tho.
	}
	else
	{

/*
FILE* fp;
fp = fopen("omnibus.log", "at");
if(fp)
{
	fprintf(fp, "adding new event, %s, %s, %s, %s\r\n",
	pEvent->m_pszReconcileKey,       // the "story ID"
	pEvent->m_pszID,    // the clip ID
	pEvent->m_pszTitle,
	pEvent->m_pszData  //must encode zero.
);
	fflush(fp);
	fclose(fp);
}

* /
		// we have to add it.
		CCAEvent** ppEvents = NULL;
		ppEvents = new CCAEvent*[m_usEventCount+1];

		if(ppEvents)
		{
			unsigned short i=0;
EnterCriticalSection(&m_critEvents);
			if((ppEvents)&&(m_ppEvents)&&(m_usEventCount))
			{
				while(i<m_usEventCount)
				{
					ppEvents[i] = m_ppEvents[i];
					i++;
				}
				delete [] m_ppEvents;
			}
			ppEvents[m_usEventCount] = pEvent; // assign new one

			m_ppEvents = ppEvents;
			m_usEventCount++;
LeaveCriticalSection(&m_critEvents);
		EnterCriticalSection (&m_critCounter);
			if(m_ulCounter >= ULONG_MAX) m_ulCounter=1;
			m_ulCounter++;
		LeaveCriticalSection (&m_critCounter);
//	LeaveCriticalSection (&m_critGEI);
			return OMNI_SUCCESS;  
		}
		else
		{
			// couldnt allocate array!
//	LeaveCriticalSection (&m_critGEI);
			return OMNI_MEMEX;
		}
	}
}

#endif


*/


