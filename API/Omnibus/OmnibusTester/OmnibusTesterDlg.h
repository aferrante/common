// OmnibusTesterDlg.h : header file
//

#if !defined(AFX_OMNIBUSTESTERDLG_H__DE52BFB7_BEBE_472A_88C6_530528BD3F49__INCLUDED_)
#define AFX_OMNIBUSTESTERDLG_H__DE52BFB7_BEBE_472A_88C6_530528BD3F49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../OmniComm.h"
#include "../OmniParse.h"
#include "..\..\..\MFC\ListCtrlEx\ListCtrlEx.h"

#define ID_LC_CH			0
#define ID_LC_EV			1
#define ID_BN_CN			2
#define ID_BN_PS			3
#define ID_ST_EV			4

#define DLG_NUM_MOVING_CONTROLS 5

/////////////////////////////////////////////////////////////////////////////
// COmnibusTesterDlg dialog

class COmnibusTesterDlg : public CDialog
{
// Construction
public:
	COmnibusTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(COmnibusTesterDlg)
	enum { IDD = IDD_OMNIBUSTESTER_DIALOG };
	CListCtrlEx	m_lcEvents;
	CListCtrlEx	m_lcChannels;
	CString	m_szIP;
	int		m_nPort;
	CString	m_szHost;
	BOOL	m_bWriteXML;
	BOOL	m_bITX;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COmnibusTesterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:
	COmniComm m_omni;
	COmniParse m_omniparse;
	CCAConn* m_pConn;
	CDBUtil m_db;
	CDBconn* m_pdbConn;
	char m_pszTable[256];
	char m_pszDB[256];
	int m_nColPercents[9];
	int m_nChColPercents[2];

	int m_nSelChan;
	int m_nSelEventID;
	bool m_bUpdating;
	bool m_bChangeSel;

	int m_nColSort;
	bool m_bColSortDesc;

	int m_nChColSort;
	bool m_bChColSortDesc;

	CRITICAL_SECTION m_critEvents;  // critical section to manage selection
	CRITICAL_SECTION m_critChannels;  // critical section to manage selection


	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[DLG_NUM_MOVING_CONTROLS];

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(COmnibusTesterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonConnect();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClickListChannels(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnColumnclickListEvents(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnColumnclickListChannels(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickListEvents(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ); 
	afx_msg void OnButtonParse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OMNIBUSTESTERDLG_H__DE52BFB7_BEBE_472A_88C6_530528BD3F49__INCLUDED_)
