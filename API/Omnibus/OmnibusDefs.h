// OmnibusDefs.h: interface for the COmniComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(OMNIBUSDEFS_H_INCLUDED_)
#define OMNIBUSDEFS_H_INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <afxmt.h>
#include "../../MFC/ODBC/DBUtil.h"
#include "..\..\TXT\BufferUtil.h"
#include "..\..\LAN\NetUtil.h"


//  need a way for the threads to send messages out, so we use the following function proto:
typedef void (__stdcall* LPFNHMSG)(int, char*, char*); // calls int SendMsg(int nType, char* pszSender, char* pszError, ...);


#ifndef NULL 
#define NULL 0
#endif

#define LIST_NOT_DEFINED			0xffff
#define TYPE_NOT_DEFINED			0xffff
#define STATUS_NOT_DEFINED		0xffff
#define TIME_NOT_DEFINED			0xffffffff
#define SEGMENT_NOT_DEFINED		0xff
#define INVALID_LIST					0xff

#define OMNI_SUCCESS		0
#define OMNI_ERROR			-1
#define OMNI_UNKNOWN		-2
#define OMNI_MEMEX			-3
#define OMNI_BADARGS		-4
#define OMNI_ESCAPE 		-5


#define OMNI_DEFAULT_TIMEOUT		10000 //milliseconds

// following for get events until playing
#define OMNI_GETEVENTS_EVENTLOOK			0x00000010  // event based lookahead
#define OMNI_GETEVENTS_TIMELOOK				0x00000020  // time based lookahead
// following for get events until ID
#define OMNI_GETEVENTS_INCLUDEDONE		0x00000040  // include done events
#define OMNI_GETEVENTS_ALL_IDS				0x00000080  // get all of the supplied events
#define OMNI_GETEVENTS_ANY_ID					0x00000100  // get any of the ids.
// various options for gets and mods
#define OMNI_GETEVENTS_DONTGET				0x00000200  // get info from the events in the canonical list, not the server
#define OMNI_NO_NOTIFY								0x00000400  // suppress sending a list change signal
#define OMNI_PROTECT_PLAY							0x00000800  // do not modify or delete events with play status
#define OMNI_PROTECT_DONE							0x00001000  // do not modify or delete events with done status
#define OMNI_PRIMARY_ONLY							0x00002000  // only apply to primary events (where applicable)
#define OMNI_SECONDARY_ONLY						0x00004000  // only apply to secondary events (where applicable)
#define OMNI_VERIFY_ALL								0x00008000  // use verification event, ensure totally identical, including all fields not commonly used.
#define OMNI_HEX											0x00010000  // expect hex encoded fields.
#define OMNI_NORMAL										0x00000000  // expect no encoding
#define OMNI_KEY_RECKEY								0x00000000  // key is Reconcile Key
#define OMNI_KEY_CLIPID								0x00000001  // key is Clip ID
#define OMNI_KEY_TITLE								0x00000002  // key is Title
#define OMNI_KEY_DATA									0x00000003  // key is "data" field
// connection options/status
#define OMNI_SOCKETERROR							0x00000001  // error
#define OMNI_PAL											0x00000010  // PAL	(25 fps)
#define OMNI_NTSCNDF									0x00000020  // NTSC no drop frame (30 fps)
#define OMNI_NTSC											0x00000030  // NTSC drop frame (29.97 fps)
#define OMNI_FRAMEBASISMASK						0x00000030  // mask
#define OMNI_FRAMEBASISSET						0x00100000  // bool
#define OMNI_SOCKETCONNECTED					0x01000000  // socket connected

// list options/status
#define OMNI_LIST_DISCONNECTED				0x00000000  // not connected
#define OMNI_LIST_CONNECTED						0x00000001  // connected
#define OMNI_LIST_UNCOMMITTED_EXISTS	0x00200000  // bool - there are items that are in the list not yet in the DB.

// iTX (connection options flag)
#define OMNI_ITX											0x00000040  // iTXv1.5 or greater, uses different time calculations
#define OMNI_SYSTIME									0x00000080  // use system time for server time.

// multi threading
#define OMNI_MULTITHREAD							0x00001000  // multi thread channel processing

// debug
#define OMNI_DEBUG										0x00000100  // use debug logging
#define OMNI_DEBUG_PROCESS						0x00000200  // use debug logging for full process

//event handling
#define OMNI_CLEARONCONNECT						0x00010000  // clear events db on connect
#define OMNI_COMPAREONCONNECT					0x00020000  // compare events on connect and delete anything not given inthe first message.



// database things
// table columns
	// fields from adaptor, all not NULL except where indicated
#define OMNI_DB_ITEMID_NAME						"itemid"    // of the DB event.
#define OMNI_DB_CONNIP_NAME						"conn_ip"    // of colossus connection, varchar 32
#define OMNI_DB_CONNPORT_NAME					"conn_port"  // of colossus connection, int
#define OMNI_DB_LISTID_NAME						"list_id"			// omnibus stream number, int
#define OMNI_DB_EVENTID_NAME					"event_id"    // unique ID (story ID), varchar 32, recKey on harris
#define OMNI_DB_EVENTCLIP_NAME				"event_clip"  // clip ID, varchar 64
#define OMNI_DB_EVENTTITLE_NAME				"event_title" // title, varchar 64
#define OMNI_DB_EVENTDATA_NAME				"event_data"  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
#define OMNI_DB_EVENTTYPE_NAME				"event_type"  // internal colossus type, int
#define OMNI_DB_EVENTSTATUS_NAME			"event_status"  // internal colossus status, int
#define OMNI_DB_EVENTTMODE_NAME				"event_time_mode"  // time mode, int
#define OMNI_DB_EVENTCALCSTART_NAME		"event_calc_start"
#define OMNI_DB_EVENTSTART_NAME				"event_start" // start time of event, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
#define OMNI_DB_EVENTDURMS_NAME				"event_duration" // duration in milliseconds, int
#define OMNI_DB_EVENTUPDATED_NAME			"event_last_update" // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500

#define OMNI_DB_EVENTPOSITION_NAME		"event_position" // int NOT NULL 
#define OMNI_DB_PARENTID_NAME					"parent_id" // varchar(32) NULL 
#define OMNI_DB_PARENTPOS_NAME				"parent_position" // int NULL
#define OMNI_DB_PARENTSTART_NAME			"parent_start" // decimal(20,3) NULL 
#define OMNI_DB_PARENTCALCSTART_NAME	"parent_calc_start" // decimal(20,3) NULL
#define OMNI_DB_PARENTDURMS_NAME			"parent_duration" // int 
#define OMNI_DB_PARENTCALCEND_NAME		"parent_calc_end" // decimal(20,3)
#define OMNI_DB_EVENTCALCEND_NAME			"event_calc_end" // decimal(20,3)

// fields from other apps
#define OMNI_DB_APPDATA_NAME					"app_data" // application data, varchar 512, allow null
#define OMNI_DB_APPDATAAUX_NAME				"app_data_aux" // auxiliary application data, int, allow null
// omnibus internal
#define OMNI_DB_EVENTFSJ70_NAME				"event_start_internal" // frames since Jan 1970, SQL float (C++ double) 


#define OMNI_FLAGS_NONE				0x00000000
#define OMNI_FLAGS_INSERTED		0x00000001   // events only.
#define OMNI_FLAGS_FOUND		  0x00000010
#define OMNI_FLAGS_CHANGED		0x00000100
#define OMNI_FLAGS_ACTIVE			0x00001000

/*
// older enum
// db enum fields
#define OMNI_DB_ITEMID_ID					0    // of the DB event.
#define OMNI_DB_CONNIP_ID					1    // of colossus connection, varchar 32
#define OMNI_DB_CONNPORT_ID				2  // of colossus connection, int
#define OMNI_DB_LISTID_ID					3			// omnibus stream number, int
#define OMNI_DB_EVENTID_ID				4    // unique ID (story ID), varchar 32, recKey on harris
#define OMNI_DB_EVENTCLIP_ID			5  // clip ID, varchar 64
#define OMNI_DB_EVENTTITLE_ID			6 // title, varchar 64
#define OMNI_DB_EVENTDATA_ID			7  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
#define OMNI_DB_EVENTTYPE_ID			8  // internal colossus type, int
#define OMNI_DB_EVENTSTATUS_ID		9  // internal colossus status, int
#define OMNI_DB_EVENTTMODE_ID			10  // time mode, int
#define OMNI_DB_EVENTFSJ70_ID			11 // frames since Jan 1970, SQL float (C++ double)
#define OMNI_DB_EVENTSTART_ID			12 // // start time of event, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
#define OMNI_DB_EVENTDURMS_ID			13 // duration in milliseconds, int
#define OMNI_DB_EVENTUPDATED_ID		14 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
	// fields from other apps
#define OMNI_DB_APPDATA_ID				15 // application data, varchar 512, allow null
#define OMNI_DB_APPDATAAUX_ID			16 // auxiliary application data, int, allow null
*/

#define OMNI_DB_ITEMID_ID						0    // of the DB event.
#define OMNI_DB_CONNIP_ID						1    // of colossus connection, varchar 32
#define OMNI_DB_CONNPORT_ID					2  // of colossus connection, int
#define OMNI_DB_LISTID_ID						3			// omnibus stream number, int
#define OMNI_DB_EVENTID_ID					4    // unique ID (story ID), varchar 32, recKey on harris
#define OMNI_DB_EVENTCLIP_ID				5  // clip ID, varchar 64
#define OMNI_DB_EVENTTITLE_ID				6 // title, varchar 64
#define OMNI_DB_EVENTDATA_ID				7  // data, varchar 4096  // omni has max 2048, we put 4096 for equiv with Harris - allow null
#define OMNI_DB_EVENTTYPE_ID				8  // internal colossus type, int
#define OMNI_DB_EVENTSTATUS_ID			9  // internal colossus status, int
#define OMNI_DB_EVENTTMODE_ID				10  // time mode, int
#define OMNI_DB_EVENTCALCSTART_ID		11
#define OMNI_DB_EVENTSTART_ID				12 // // start time of event, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
#define OMNI_DB_EVENTDURMS_ID				13 // duration in milliseconds, int
#define OMNI_DB_EVENTUPDATED_ID			14 // time of last update from colossus, SQL float (C++ double): format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
#define OMNI_DB_EVENTPOSITION_ID		15
#define OMNI_DB_PARENTID_ID					16
#define OMNI_DB_PARENTPOS_ID				17
#define OMNI_DB_PARENTSTART_ID			18
#define OMNI_DB_PARENTCALCSTART_ID	19
#define OMNI_DB_PARENTDURMS_ID			20
#define OMNI_DB_PARENTCALCEND_ID		21
#define OMNI_DB_EVENTCALCEND_ID			22
	// fields from other apps
#define OMNI_DB_APPDATA_ID					23 // application data, varchar 512, allow null
#define OMNI_DB_APPDATAAUX_ID				24 // auxiliary application data, int, allow null
#define OMNI_DB_EVENTFSJ70_ID				25 // frames since Jan 1970, SQL float (C++ double)


// omnibus defines from colossus adaptor spec V1.5
// status
#define C_ERROR												0  // item contains errors
#define C_INITIALIZED									1  // item has just been initialized and cannot be used without further work
#define C_UNCHECKED										2  // item wasn't checked
#define C_UNALLOCATED									3  // item wasn't allocated
#define C_UNAVAILABLE									4  // item isn't available
#define C_ALLOCATED										5  // item allocated
#define C_CUEING											6  // item cueing
#define C_READY												7  // item ready for playout
#define C_COMMIT											8  // item ready to be committed to air
#define C_ON_AIR											9  // item is on air
#define C_HOLD												10 // item holding time after playing to air
#define C_DONE												11 // item has been played to air

// event types
#define CL_TYPE_INTERNAL							0  // actually not an omnibus define, but a def val.
#define CL_TYPE_ITEM									1  // an item with media
#define CL_TYPE_SEGMENT								2  // not yet implemented
#define CL_TYPE_EVENT									3  // a live event
#define CL_TYPE_BREAK_START						4  // not yet implemented
#define CL_TYPE_BREAK_END							5  // not yet implemented
#define CL_TYPE_SPLIT_BREAK						6  // not yet implemented
#define CL_TYPE_MACRO									7  // not yet implemented
#define CL_TYPE_SCHEDULE							8  // a schedule that has yet to be expanded

// time modes
#define C_MODE_MANUAL									0  // a manual take item
#define C_MODE_AUTO										1  // a normal automatic item
#define C_MODE_ABS										2  // a fixed time item
#define C_MODE_PLUS										3  // an item with a start time relative to the previous event
#define C_MODE_MINUS									4  // an item with a start time relative to the next event
#define C_MODE_FIXED_END							5  // an item with a fixed end time

class CCAActives
{
public:
	CCAActives();
	virtual ~CCAActives();

	int* m_pnActives;
	int m_nNumActive;
	CRITICAL_SECTION m_critActive;
	bool m_bUseActive;
	bool m_bClearEventsInit;

	int IsChannelActive(int nChannelID=-1);
	int RemoveActiveChannel(int nChannelID=-1);
	int AddActiveChannel(int nChannelID=-1);

};

// an event type that is uniform, containing only what we care about
class CCAEvent  //"Colossus Adaptor" Event
{
public:
// supplied from automation system
	unsigned short m_usType;  // internal colossus type
	char* m_pszID;    // the clip ID
	char* m_pszTitle;
	char* m_pszData;  //must encode zero.
	char* m_pszReconcileKey;       // the "story ID"
	unsigned char  m_ucSegment;    // not used
	unsigned long  m_ulOnAirTimeMS;
	unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
	unsigned long  m_ulDurationMS;
	unsigned short m_usStatus;  // status (omnibus code)
	unsigned short m_usControl; // time mode

	double m_dblOnAirTimeInternal;  // for omnibus, this is frames since jan 1970
	double m_dblOnAirTime;          // format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
	double m_dblUpdated;            // the last time the system updated the event. format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500

// a pointer to anything we want, to contextualize this event - must use new operator to allocate
	void* m_pContextData;  //must encode zero if a string.

	void* m_pParent;  //CCAEvent* to parent object

	int m_nEventPosition;
	unsigned long  m_ulFlags;

public:
	CCAEvent();
	virtual ~CCAEvent();
};

class CCAEventData  // used in data field search
{
public:
	char* m_pszFieldName;  // unadorned, no tags.
	char* m_pchStartTag;   // start position of the start tag including "<"
	char* m_pchEndTag;     // position of the end of the end tag including ">", = start position of next data.
	char* m_pszFieldInfo;  // contents of the data field including start and end tags.

public:
	CCAEventData();
	virtual ~CCAEventData();
};



// we need to index by list, not by connection, but automation system indexes by connection.
// we can get a list status on a connection basis, then have an array of pointers indexing into lists.
// therefore, we also need a list type.
class CCAList  //"Colossus Adaptor" List
{

public:
	CCAList(char* pszListName, unsigned short usListID );
	virtual ~CCAList();

	// trying to restrict access.
	unsigned short GetEventCount();
	CCAEvent* GetEvent(int nIndex);
	int GetEventIndex(char* pszKey, unsigned long ulFlags=OMNI_KEY_RECKEY, int nStartIndex=-1);  // this is the unique list ID, the "story ID"
	int GetEventIndexByRecKey(char* pszKey, int nStartIndex=-1);  // this is the unique list ID, the "story ID"
	int SetFrameBasis(unsigned long ulFrameBasis);

	int UpdateListStatus(bool bForce=false);
	int UpdateEvent(CCAEvent* pEvent, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL, char* pszTable=NULL, char* pszConnServer=NULL, unsigned short usConnPort=0);
	int UpdateDatabaseEvent(CCAEvent* pEvent, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL, char* pszTable=NULL, char* pszConnServer=NULL, unsigned short usConnPort=0);
	int DeleteEvent(int nIndex, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL,	char*	pszTable=NULL);
	int DeleteAllEvents(CDBUtil*	pdb=NULL, CDBconn* pdbConn=NULL,	char*	pszTable=NULL);

	int CheckDatabaseEvents(int nStartIndex=-1, int nLimit=-1);

private:
	CCAEvent** m_ppEvents; // a pointer to an array of event pointers.
	unsigned short m_usEventCount; 
	int m_nLastEventIndex;
	int m_nLastDatabaseEventIndex;

public:
//	CRITICAL_SECTION m_critGEI;  // critical section to manage access to events.
//	CRITICAL_SECTION m_critUE;  // critical section to manage access to events.

	char* m_pszListName;			  // basically, channel name
	unsigned short m_usListID;  // numerical channel ID
	unsigned long  m_ulFlags;		// list options/status
	unsigned long  m_ulRefTick; // last status get.
	unsigned long  m_ulCounter;		// changes counter incrementer

	unsigned short m_usDoneCount;  // for iTX only, the number of events to retain as done events (the iTX snapshots start with the currently playing event, so have to maintain a history)

	void* m_pConn; // pointer to the parent connection.
	CRITICAL_SECTION m_critEvents;  // for event array access

	char**			m_ppchXMLQueue; // queue up XML items.
	int				  m_nXMLQueue;
	CRITICAL_SECTION m_critXMLQueue;
	bool m_bXMLQueueStarted;
	bool m_bXMLQueueKill;

	_timeb m_tmbLastUpdate;
	_timeb m_tmbLastDatabaseCheck;

	int AddMessage(char* pchMessage);
	char* ReturnMessage();
};


class CCAConn  //"Colossus Adaptor" Connection
{
public:
// used to connect:
	char* m_pszServerName;			// friendly name for the connection to the server.  not used programmatically
	char* m_pszServerAddress;		// can be host name or IP address
	unsigned short m_usPort;		// port to connect
	unsigned long  m_ulFlags;		// connection options/status
	bool m_bKillThread;					// kill the comm thread
	bool m_bThreadStarted;			// state of comm thread
	bool m_bUseResponse;				// respond to XML messages with a roAck.
	bool m_bLightMode;					// light or full mode.

	bool m_bWriteXMLtoFile;
	bool m_bUseUTC;
	bool m_bUseDST;

// supplied from automation system
	unsigned short m_usRefJulianDate; // compiled from "current time" offset from January 1, 1900
	unsigned long  m_ulRefTimeMS;			// compiled from "current time".  // can mod 1000 this time to get the millisecond complement to the unix time (below)
	unsigned long  m_ulRefUnixTime;		// compiled from "current time".

// purge expired events (needed because omnibus connection does not always send delete messages)
	unsigned long  m_ulCheckInterval;	// number of seconds between checks (0 turns off)
	unsigned long  m_ulExpiryPeriod;	// when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
	unsigned long  m_ulLastPurge;			// Last time the events were purged.
	unsigned long  m_ulConnTimeout;   	// when the connection has received no update for number of seconds, the connection is restarted 
	unsigned long  m_ulConnLastMessage;   	// Last time a message was received (unixtime);
	unsigned long  m_ulInterEventIntervalMS;   	// time to stall between committing each event to the database
	unsigned long  m_ulThreadDelayMS;   	// time to stall between starting the connection thread and getting events

	int m_nMessageEventCountLimit;

	int m_nLookaheadTimeLimitSec;  // Seconds from "now" to not process adds and updates. discard the rest.  Deletions are not filtered.
	int m_nLookbehindTimeLimitSec;  // Seconds before "now" to not process adds and updates. discard the rest.  Deletions are not filtered.

	int m_nAsyncDatabaseLookaheadSec;  // immediately commit to the DB any events that have an on air time of "now" plus this number of seconds, or prior. otherwise commit events as they come into the window.
	int m_nAsyncDatabaseCheckIntervalMS;  // only check for uncommitted events every so often.
	int m_nMinimumListUpdateIntervalMS;  // prevent database updates on list status faster than this interval
	int m_nMinimumServerUpdateIntervalMS;  // prevent database updates on server status faster than this interval
	bool m_bPreventServerTimeFromEvents;			// ignore the current time reference within events.

	// SQL vars
	char m_szSQL[DB_SQLSTRING_MAXLEN];  // for use in update list, lists come in serially on comm so only need per conn
	char m_errorstring[DB_ERRORSTRING_LEN];


// assigned internally
private:
	CCAList** m_ppList;  // need to dynamically allocate.
	CCAList* m_pTempList; // a place to store events temporarily.  volatile.
	unsigned short m_usListCount; 

public:

	SOCKET m_socket;

	CRITICAL_SECTION m_crit;  // critical section to manage access to lists.
	CRITICAL_SECTION m_critCounter;  // critical section to manage access to list counter only.

	unsigned long  m_ulRefTick; // last status get.
	unsigned long  m_ulCounter;		// changes counter incrementer

	unsigned long  m_ulConnectionRetries;		// how many times to try to re-establish
	unsigned long  m_ulConnectionInterval;	// how long to wait in seconds before retrying

	char* m_pszDebugFile;		// debug file
	char* m_pszCommFile;		// comm log file

	CDBUtil*	m_pdb;  // pointer to a db util object to use for operations
	CDBconn*	m_pdbConn;  // pointer to a db connection
	char*			m_pszEventsTable; // name of the table to update
	char*			m_pszExchangeTable; // name of the table to update
	char*			m_pszConnectionsTable; // name of the table to update
	char*			m_pszChannelsTable; // name of the table to update

	CRITICAL_SECTION* m_pcritSQL;
	CRITICAL_SECTION* m_pcritMem;
	int				m_nNumFields;
	CCAEventData** m_ppfi;  // dynamic array of field info.

	LPFNHMSG   m_lpfnSendMsg;
	CRITICAL_SECTION* m_pcritSendMsg;

	// 2.1.1.16 added asynchronous database access, to take it out of critical path of the TCP communication.  Omnibus times out on large lists, have to receive as fasta as possible.
	char**			m_ppchXMLQueue; // queue up XML items.
	int				  m_nXMLQueue;
	CRITICAL_SECTION m_critXMLQueue;
	bool m_bXMLQueueStarted;
	bool m_bXMLQueueKill;

	CRITICAL_SECTION m_critOperations;
	CRITICAL_SECTION m_critAux;
	void* pAux;

	_timeb m_tmbLastUpdate;


public:
	CCAConn();
	virtual ~CCAConn();

//core
	int ConnectServer();
	int DisconnectServer();

	int ReturnListIndex(unsigned short usListID);  // use m_crit outside calls to this one
	int UpdateList(unsigned short usListID, char* pszListName, unsigned long ulFlags=OMNI_LIST_DISCONNECTED, int* pnListID=NULL);
	unsigned short GetListCount();
	CCAList* GetList(unsigned short usListIndex);
	int UpdateServerTime(bool bUseSystemClock);

// core - event search
	int GetRelativeIndex(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags);
	int GetNumSecondaries(CCAConn* pConn, unsigned char ucChannel, unsigned short usIndex, unsigned long ulFlags);
	int CheckAllListsForUncommitted(bool* pbXMLQueueKill);

// utility
	int ConvertTime(char* pszTimeString, unsigned long* pulRefUnixTime=NULL, unsigned short* pusJulianDate=NULL, double* pdblMilliUnix=NULL);  // returns milliseconds in current day, -1 if error.
	int	ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned long ulFlags=OMNI_NORMAL);  
	int	ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned long ulFlags=OMNI_NORMAL);

	int SetCommLogName(char* pszFileName);
	int SetDebugName(char* pszFileName);
	int SetFrameBasis(unsigned long ulFrameBasis);

	int AddMessage(char* pchMessage, bool bInitialConnection);
	char* ReturnMessage();

	int KillChannelThreads();
};


#endif // !defined(OMNIBUSDEFS_H_INCLUDED_)
