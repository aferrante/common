// OmniComm.cpp: implementation of the COmniComm class.
//
//////////////////////////////////////////////////////////////////////

#include "OmniComm.h"
#include <sys/timeb.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COmniComm::COmniComm()
{
	m_ppConn = NULL;
	m_usNumConn = 0;
}

COmniComm::~COmniComm()
{
	if(m_ppConn)
	{
		for(unsigned short i=0;i<m_usNumConn;i++)
		{
			if(m_ppConn[i]) delete(m_ppConn[i]);
			m_ppConn[i] = NULL;
		}
		delete [] m_ppConn;
		m_ppConn = NULL;
	}

}

bool COmniComm::CheckConnectionValid(unsigned short usConnIndex)
{
	if(usConnIndex<m_usNumConn)
	{
		if((m_ppConn)&&(m_ppConn[usConnIndex]))
		{
			return true;
		}
	}
	return false;
}

CCAConn* COmniComm::ConnectServer(char* pszServerAddress, unsigned short usPort, char* pszServerName, char* pszDataFields, CDBUtil* pdb, CDBconn* pdbConn, char* pszTable) // returns pointer to connection
{
	CCAConn* pConn = new CCAConn;
	if(pConn)
	{
		if(pszServerAddress)
		{
			pConn->m_pszServerAddress = (char*)malloc(strlen(pszServerAddress+1));		//	hostname
			if(pConn->m_pszServerAddress) 
			{
				strcpy(pConn->m_pszServerAddress, pszServerAddress);
			} else return NULL;
		}else return NULL;    // need a server address!

		if(pszServerName)
		{
			pConn->m_pszServerName = (char*)malloc(strlen(pszServerName+1));				// friendly name for the connection to the server.  not used programmatically
			if(pConn->m_pszServerName) strcpy(pConn->m_pszServerName, pszServerName);
		}

		if((pszDataFields)&&(strlen(pszDataFields)))  // a comma separated list of tags inside the mosPayLoad that will be assembled and put into the m_pszData member of the event items
		{

//			AfxMessageBox(pszDataFields);
			pConn->m_nNumFields = 0;
			pConn->m_ppfi = NULL;  // dynamic array of field info.
																			
			// first split up the pConn->m_pszDataFields into the tag names
			CSafeBufferUtil sbu;

			char* pchField = sbu.Token(pszDataFields, strlen(pszDataFields), "><, ");
			while(pchField)
			{
				if(strlen(pchField))
				{
					CCAEventData* pfi = new CCAEventData;
					if(pfi)
					{
						CCAEventData** ppnewfi = new CCAEventData*[pConn->m_nNumFields+1];
						if(ppnewfi)
						{
							if(pConn->m_ppfi)
							{
								for(int i=0; i<pConn->m_nNumFields; i++)
								{
									ppnewfi[i] = pConn->m_ppfi[i];
								}
								delete [] pConn->m_ppfi;
							}

							pConn->m_ppfi = ppnewfi;

	//			AfxMessageBox(pch);
							//pfi->m_pszFieldName = pchField;  
							pfi->m_pszFieldName = (char*)malloc(strlen(pchField)+1);
							if(pfi->m_pszFieldName) strcpy(pfi->m_pszFieldName, pchField);  
//				AfxMessageBox(pConn->m_ppfi[pConn->m_nNumFields]->m_pszFieldName);

							pConn->m_ppfi[pConn->m_nNumFields] = pfi;
							pConn->m_nNumFields++;

						}
					}
				}

				//get the next one.
//				AfxMessageBox(pConn->m_ppfi[pConn->m_nNumFields-1]->m_pszFieldName);
				pchField = sbu.Token(NULL, NULL, "><, ");
//				AfxMessageBox(pConn->m_ppfi[pConn->m_nNumFields-1]->m_pszFieldName);

/*
				for(int i=0; i<pConn->m_nNumFields; i++)
				{
					char fn[256];
					sprintf(fn, "%d: 0x%08x: %s", i, pConn->m_ppfi[i]->m_pszFieldName, pConn->m_ppfi[i]->m_pszFieldName);
					AfxMessageBox(fn);
				}
*/			
			}

		}
/*
		for(int i=0; i<pConn->m_nNumFields; i++)
		{
			char fn[256];
			sprintf(fn, "final %d: 0x%08x: %s", i, pConn->m_ppfi[i]->m_pszFieldName, pConn->m_ppfi[i]->m_pszFieldName);
			AfxMessageBox(fn);
		}
*/
		pConn->m_usPort = usPort;							// port to connect

		SetConnDB(pConn, pdb, pdbConn, pszTable);

		if( pConn->ConnectServer() == OMNI_SUCCESS)  // we began the thread, but did it connect?
		{
			_timeb timestamp;
			_timeb checktime;
			_ftime( &timestamp );
			checktime.time = timestamp.time ; 
			checktime.millitm = timestamp.millitm + 500; // many timeouts
			if(checktime.millitm>999)
			{
				checktime.time++;
				checktime.millitm%=1000;
			}

			while(!pConn->m_bThreadStarted)
			{   
				_ftime( &timestamp );
				if(
						(timestamp.time > checktime.time)
					||((timestamp.time == checktime.time)&&(timestamp.millitm >= checktime.millitm))
					)
				{
					break;
				}
			}  // just do something

			if((!((pConn->m_ulFlags)&OMNI_SOCKETERROR))&&(pConn->m_bThreadStarted))
			{
				// add this connection to the matrix and return the pointer.
				CCAConn** ppConn = NULL;
				ppConn = new CCAConn*[m_usNumConn+1];

				if(ppConn)
				{
					unsigned short i=0;
					if((m_ppConn)&&(m_usNumConn>0))
					{
						while(i<m_usNumConn)
						{
							ppConn[i] = m_ppConn[i];
							i++;
						}
						delete [] m_ppConn;
					}
					ppConn[i] = pConn; // assign new one

					m_ppConn = ppConn;
					m_usNumConn++;
					return pConn;
				}
				else
				{
					// couldnt allocate array so must disconnect!
					pConn->DisconnectServer();
					return NULL;
				}
			}
			else
				pConn->DisconnectServer();

		}
	}
	return NULL;
}

CCAConn* COmniComm::ConnectionExists(char* pszServerAddress) // returns pointer to connection
{
	if((m_ppConn)&&(m_usNumConn>0))
	{
		unsigned short i=0;
		while(i<m_usNumConn)
		{
			if(m_ppConn[i])
			{
				if(stricmp(pszServerAddress, m_ppConn[i]->m_pszServerAddress)==0) return m_ppConn[i];
			}
			i++;
		}
	}
	return NULL;
}

int COmniComm::DisconnectServer(CCAConn* pConn)
{
	if(pConn)
	{
		// have to disconnect and remove from connection list
		pConn->DisconnectServer();  // always returns success

		// have to wait here for the conn to close and thread to die.

		_timeb timestamp;
		_timeb checktime;
		_ftime( &timestamp );
		checktime.time = timestamp.time ; 
		checktime.millitm = timestamp.millitm + 250;
		if(checktime.millitm>999)
		{
			checktime.time++;
			checktime.millitm%=1000;
		}

		int nRetries=0;
		while(pConn->m_bThreadStarted)
		{   
			_ftime( &timestamp );
			if(
					(timestamp.time > checktime.time)
				||((timestamp.time == checktime.time)&&(timestamp.millitm >= checktime.millitm))
				)
			{
				checktime.time = timestamp.time ; 
				checktime.millitm = timestamp.millitm + 100;
				if(checktime.millitm>999)
				{
					checktime.time++;
					checktime.millitm%=1000;
				}


				nRetries++;
				if(nRetries>50)
				{
					// five seconds has elapsed
					//that's abnormal so lets just force it.
					pConn->m_bThreadStarted = false;
				}

			}
		}  // just do something while the thread is still there;


		if((m_ppConn)&&(m_usNumConn>0))
		{
			unsigned short i=0;
			while(i<m_usNumConn)
			{
				if(m_ppConn[i] == pConn)
				{
					// remove it
					delete pConn;
					pConn = NULL;
					m_usNumConn--;

					CCAConn** ppConn = NULL;
					if(m_usNumConn>0)
					{
						ppConn = new CCAConn*[m_usNumConn];
					}
					else
					{
						delete [] m_ppConn;
						m_ppConn = NULL;
						return OMNI_SUCCESS;
					}

					if(ppConn)
					{
						while(i<m_usNumConn)
						{
							ppConn[i] = m_ppConn[i+1];
							i++;
						}
						delete [] m_ppConn;
						m_ppConn = ppConn;
					}
					else  // could not alloc, so just shuffle
					{
						while(i<m_usNumConn)
						{
							m_ppConn[i] = m_ppConn[i+1];
							i++;
						}
						m_ppConn[i] = NULL;
					}
					return OMNI_SUCCESS;
				}
				i++;
			}
		}
	}
	return OMNI_ERROR;
}

int COmniComm::DisconnectServer(char* pszServerAddress) // searches for conn idx
{
	CCAConn* pConn = ConnectionExists(pszServerAddress);
	return DisconnectServer(pConn);
}

int COmniComm::SetConnDB(CCAConn* pConn, CDBUtil* pdb, CDBconn* pdbConn, char* pszTable) 
{
	if(pConn==NULL)
	{
//		AfxMessageBox("Could not set DB");
		return OMNI_ERROR;
	}

	if((pdb==NULL)||(pdbConn==NULL)||(pszTable==NULL)||(strlen(pszTable)<=0))
	{
		//clear out the old....
		pConn->m_pdb = NULL;  // pointer to a db util object to use for operations
		pConn->m_pdbConn = NULL;  // pointer to a db connection
		pConn->m_pszTable = NULL; // name of the table to update

		// just pointers, we arent freeing the connection objects etc.
//		AfxMessageBox("set to NULL");
	}
	else
	{
		pConn->m_pdb = pdb;  // pointer to a db util object to use for operations
		pConn->m_pdbConn = pdbConn;  // pointer to a db connection
		pConn->m_pszTable = pszTable; // name of the table to update
//		AfxMessageBox(pszTable);
	}
	return OMNI_SUCCESS;
}

