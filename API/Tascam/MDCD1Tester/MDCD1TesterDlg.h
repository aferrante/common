// MDCD1TesterDlg.h : header file
//

#if !defined(AFX_MDCD1TESTERDLG_H__21C238BF_67AC_49BD_8676_CBAB2A78322E__INCLUDED_)
#define AFX_MDCD1TESTERDLG_H__21C238BF_67AC_49BD_8676_CBAB2A78322E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../MDCD1.h"

/////////////////////////////////////////////////////////////////////////////
// CMDCD1TesterDlg dialog

class CMDCD1TesterDlg : public CDialog
{
// Construction
public:
	CMDCD1TesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMDCD1TesterDlg)
	enum { IDD = IDD_MDCD1TESTER_DIALOG };
	CString	m_szCmd;
	CString	m_szData;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDCD1TesterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:
	CMDCD1 m_mdcd1;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMDCD1TesterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonSend();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MDCD1TESTERDLG_H__21C238BF_67AC_49BD_8676_CBAB2A78322E__INCLUDED_)
