// MDCD1TesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MDCD1Tester.h"
#include "MDCD1TesterDlg.h"
#include "../../../TXT/BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDCD1TesterDlg dialog

CMDCD1TesterDlg::CMDCD1TesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMDCD1TesterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMDCD1TesterDlg)
	m_szCmd = _T("0f");
	m_szData = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMDCD1TesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMDCD1TesterDlg)
	DDX_Text(pDX, IDC_EDIT_CMD, m_szCmd);
	DDX_Text(pDX, IDC_EDIT_DATA, m_szData);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMDCD1TesterDlg, CDialog)
	//{{AFX_MSG_MAP(CMDCD1TesterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDCD1TesterDlg message handlers

BOOL CMDCD1TesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMDCD1TesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMDCD1TesterDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMDCD1TesterDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CMDCD1TesterDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CMDCD1TesterDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CMDCD1TesterDlg::OnButtonConnect() 
{
	if (m_mdcd1.IsConnected())
	{
		m_mdcd1.EndComm();
	}
	else
	{
		m_mdcd1.SetComm();
		m_mdcd1.InitComm();
	}
	Sleep(250);

	if (m_mdcd1.IsConnected())
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
	else
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
	
}

void CMDCD1TesterDlg::OnButtonSend() 
{
	UpdateData(TRUE);

	char cmd[4];
	char data[120];

	_snprintf(cmd, 3, "%s", m_szCmd);
	_snprintf(data, 100, "%s", m_szData);
	CBufferUtil bu;
	unsigned char uch = (unsigned char) bu.xtol(cmd, 2);
//		m_mdcd1.SetComm();
//	if(m_mdcd1.m_serial.OpenConnection()==SERIAL_SUCCESS)
//	{

	EnterCriticalSection (&m_mdcd1.m_critGlobal);
	if(m_mdcd1.QueueCommand(uch, data)>0)
	{
		unsigned char* pucCmd;
		char* pchID;
		char** ppchData = NULL;
		int nNumReplies = m_mdcd1.ReceiveCommand(&pucCmd, &ppchData, &pchID);
		if(nNumReplies>0)
		{
			int i=0;
			while(i<nNumReplies)
			{
				m_szCmd.Format("%02X", pucCmd[i]);
				m_szData.Format("%s", ppchData[i]?ppchData[i]:"");
				sprintf(data, "received data on device id %c", pchID[i]);
				if(ppchData[i]) AfxMessageBox(ppchData[i]);
				UpdateData(FALSE);
				if(ppchData[i]) free(ppchData[i]);
				i++;
			}
			delete [] pucCmd;
			delete [] pchID;
			delete [] ppchData;
		}
		else 		AfxMessageBox("Error receiving or nothing to receive");
	}
	else 		AfxMessageBox("Error sending");
	LeaveCriticalSection (&m_mdcd1.m_critGlobal);

	//m_mdcd1.m_serial.CloseConnection();
//	}
}
