========================================================================
       MICROSOFT FOUNDATION CLASS LIBRARY : MDCD1Tester
========================================================================


AppWizard has created this MDCD1Tester application for you.  This application
not only demonstrates the basics of using the Microsoft Foundation classes
but is also a starting point for writing your application.

This file contains a summary of what you will find in each of the files that
make up your MDCD1Tester application.

MDCD1Tester.h
    This is the main header file for the application.  It includes other
    project specific headers (including Resource.h) and declares the
    CMDCD1TesterApp application class.

MDCD1Tester.cpp
    This is the main application source file that contains the application
    class CMDCD1TesterApp.

MDCD1Tester.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
	Developer Studio.

res\MDCD1Tester.ico
    This is an icon file, which is used as the application's icon.  This
    icon is included by the main resource file MDCD1Tester.rc.

res\MDCD1Tester.rc2
    This file contains resources that are not edited by Microsoft 
	Developer Studio.  You should place all resources not
	editable by the resource editor in this file.

MDCD1Tester.clw
    This file contains information used by ClassWizard to edit existing
    classes or add new classes.  ClassWizard also uses this file to store
    information needed to create and edit message maps and dialog data
    maps and to create prototype member functions.


/////////////////////////////////////////////////////////////////////////////

AppWizard creates one dialog class:

MDCD1TesterDlg.h, MDCD1TesterDlg.cpp - the dialog
    These files contain your CMDCD1TesterDlg class.  This class defines
    the behavior of your application's main dialog.  The dialog's
    template is in MDCD1Tester.rc, which can be edited in Microsoft
	Developer Studio.


/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named MDCD1Tester.pch and a precompiled types file named StdAfx.obj.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Developer Studio reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

If your application uses MFC in a shared DLL, and your application is 
in a language other than the operating system's current language, you
will need to copy the corresponding localized resources MFC40XXX.DLL
from the Microsoft Visual C++ CD-ROM onto the system or system32 directory,
and rename it to be MFCLOC.DLL.  ("XXX" stands for the language abbreviation.
For example, MFC40DEU.DLL contains resources translated to German.)  If you
don't do this, some of the UI elements of your application will remain in the
language of the operating system.

/////////////////////////////////////////////////////////////////////////////
