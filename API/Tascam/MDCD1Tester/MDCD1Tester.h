// MDCD1Tester.h : main header file for the MDCD1TESTER application
//

#if !defined(AFX_MDCD1TESTER_H__CB4E0342_345F_4432_B693_D1F2BC979DD2__INCLUDED_)
#define AFX_MDCD1TESTER_H__CB4E0342_345F_4432_B693_D1F2BC979DD2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMDCD1TesterApp:
// See MDCD1Tester.cpp for the implementation of this class
//

class CMDCD1TesterApp : public CWinApp
{
public:
	CMDCD1TesterApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDCD1TesterApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMDCD1TesterApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MDCD1TESTER_H__CB4E0342_345F_4432_B693_D1F2BC979DD2__INCLUDED_)
