// CDMD1.h: interface for the CMDCD1 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMDCD1_H__7E5F8936_A9C5_4E4A_875E_616811CC0D0A__INCLUDED_)
#define AFX_CMDCD1_H__7E5F8936_A9C5_4E4A_875E_616811CC0D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../TTY/Serial.h"

#define MDCD1_MD '1'
#define MDCD1_CD '2'
#define MDCD1_GLOBAL '0'

#define MDCD1_COMM			0x00000001  // communication has been established with the unit.
#define MDCD1_DISC			0x00000002  // a disc is present
#define MDCD1_PLAY			0x00000004  // a disc is playing
#define MDCD1_AUTO			0x00000008  // autoready is set
#define MDCD1_ERR				0x10000000  // an error condition persists

#define MDCD1_TIME_TOTAL			0x00000000  // total time
#define MDCD1_TIME_ELAPSED		0x00000001  // elapsed time
#define MDCD1_TIME_REMAIN			0x00000002  // remaining time

#define MDCD1_SUCCESS		0
#define MDCD1_ERROR			-1



class CMDCD1 : public CMessagingObject 
{
public:
	CMDCD1();
	virtual ~CMDCD1();

	int SetComm(char chComPort=1, unsigned long ulBaud=19200, char chByteSize=8, char chStopBits=ONESTOPBIT, char chParity=NOPARITY);
	int InitComm();
	int EndComm();
	int QueueCommand(unsigned char ucCmd, char* pszData=NULL);
	int ReceiveCommand(unsigned char** ppucCmd, char*** pppszData, char** ppchID);
	int SetDevice(char chID);
	bool IsConnected();

// command specific
	int Play();
	int Stop();
	int Eject();
	int SetAutoReady(bool bOn);
	int SelectTrack(int nTrack);

	// get
	char*	GetTitleInfo(int nTrack);  // any track, not just current.  0 gives you disc title, -1 gives you current track
	int		GetTimeInfo(int nTrack, unsigned long m_ulType);  // any track, not just current. 0 gives you disc title, -1 gives you current track

	// setup thread
	int GetInitialInfo();  // checks if a disc is there, sets up all the info

public:
	CRITICAL_SECTION m_crit;  // critical section to manage commands -  the thread manages status continually.
	CRITICAL_SECTION m_critGlobal;  // the thread polls receive buffer continually, so this is provided for the outside to send a cmd and receive a reply
	bool m_bKillThread;
	bool m_bThreadStarted;
	bool m_bResetConnection;
	CSerial m_serial;

// queue command
	char** m_ppszCommand;
	int    m_nNumCommands;

	int    m_nPollingIntervalMS;

protected:
	unsigned long m_ulStatus;

	char	m_chDevice;
	char	m_pszCurrentDiscTitle[100];
	char	m_pszCurrentTrackTitle[100];
	int		m_nCurrentTrackNumber;
	int		m_nCurrentTrackTimeLeft;
	int		m_nCurrentTrackTimeElapsed;
	int		m_nCurrentTrackTimeTotal;
	int		m_nCurrentDiscTimeLeft;
	int		m_nCurrentDiscTimeElapsed;
	int		m_nCurrentDiscTimeTotal;
	int		m_nCurrentDiscNumTracks;

};

#endif // !defined(AFX_CMDCD1_H__7E5F8936_A9C5_4E4A_875E_616811CC0D0A__INCLUDED_)
