// CMDCD1.cpp: implementation of the CMDCD1 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // need for overlapped structs, etc in serial comm obj
#include "MDCD1.h"
#include <process.h>
#include "../../TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CDMD1CommThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMDCD1::CMDCD1()
{
	m_bEnableMessaging = false; // turn off module messaging by default

	m_ulStatus = 0;
	m_chDevice = MDCD1_MD;
	m_bKillThread = true;
	m_bThreadStarted = false;
	m_bResetConnection = true;
	InitializeCriticalSection(&m_crit);
	InitializeCriticalSection(&m_critGlobal);

	m_ppszCommand = NULL;
	m_nNumCommands = 0;

	m_nPollingIntervalMS = 100;
}

CMDCD1::~CMDCD1()
{
	m_bKillThread = true;
	while(m_bThreadStarted)
	{
		Sleep(50);
	}
	DeleteCriticalSection(&m_crit);
	DeleteCriticalSection(&m_critGlobal);
}

int CMDCD1::SetComm(char chComPort, unsigned long ulBaud, char chByteSize, char chStopBits, char chParity)
{
	if(m_serial.m_bConnected) return MDCD1_ERROR;

	m_serial.m_chComPort		= chComPort;
	m_serial.m_ulBaud				= ulBaud;
	m_serial.m_chByteSize		= chByteSize;
	m_serial.m_chStopBits		= chStopBits;
	m_serial.m_chParity			= chParity;

	return MDCD1_SUCCESS;
}

int CMDCD1::InitComm()
{
	if((m_serial.m_bConnected)||(m_bThreadStarted)) return MDCD1_ERROR;
	m_bKillThread = false;

	if(_beginthread( CDMD1CommThread, 0, (void*)(this) )<0)
		return MDCD1_ERROR;
	return MDCD1_SUCCESS;
}

int CMDCD1::EndComm()
{
	m_bKillThread = true;
	return MDCD1_SUCCESS;
}

int CMDCD1::QueueCommand(unsigned char ucCmd, char* pszData)
{
	if((!m_serial.m_bConnected)||(!m_bThreadStarted)) return MDCD1_ERROR;  // have to be connected to send
	EnterCriticalSection (&m_crit);

//	A command is formatted as follows:
// [LF][ascii device ID][ascii of first hex digit][ascii of second hex digit][data][data][...][CR]

	char* pch = NULL;
	int nLen = 0;
	if((pszData)&&(strlen(pszData)))
	{
		nLen = strlen(pszData);
	}
	pch = (char*)malloc(nLen+6);

	if(pch)
	{
		*pch = 0x0a;  //LF
		*(pch+1) = m_chDevice;  // device ID
// OK now, if we have a command such as 0x4e, we want first byte to be '4' and the second to be 'E' (they use uppercase for letter hex chars)
		sprintf((pch+2), "%02X", ucCmd);  // this "ought" to work.

		// then the rest of the data.
		if(nLen)
		{
			strncpy((pch+4), pszData, nLen);
		}

		*(pch+4+nLen) = 0x0d;  // CR
		*(pch+5+nLen) = 0;  // a just in case zero term.


char foo[256]; sprintf(foo, "sending [%s]", pch);		AfxMessageBox(foo);

	//	nLen = m_serial.SendData(pch, nLen+5); // do this in thread

		char** ppszCommand = new char*[m_nNumCommands+1];
		if(ppszCommand)
		{

			if(m_ppszCommand)
			{
				int n=0;
				while(n<m_nNumCommands)
				{
					ppszCommand[n] = m_ppszCommand[n];
					n++;
				}
				delete [] m_ppszCommand;
				m_ppszCommand = NULL;
			}
			ppszCommand[m_nNumCommands] = pch;
			m_nNumCommands++;
			m_ppszCommand = ppszCommand;
		}
		LeaveCriticalSection (&m_crit);
		return (MDCD1_SUCCESS);
	}
	LeaveCriticalSection (&m_crit);
	return MDCD1_ERROR;
}

// returns number of commands received, if non zero must free/delete mem outside
int CMDCD1::ReceiveCommand(unsigned char** ppucCmd, char*** pppszData, char** ppchID)
{
	if(ppucCmd==NULL) return MDCD1_ERROR;  // have to have at min one pointer to fill with data
	if((!m_serial.m_bConnected)||(!m_bThreadStarted)) return MDCD1_ERROR;  // have to be connected to receive
	EnterCriticalSection (&m_crit);
	unsigned long ulDataLength = 4096;
	char pchData[4096]; // max buffer
	int nLen = m_serial.ReceiveData((char**)(&pchData), &ulDataLength);
	int nNumRecords = 0;

char foo[256]; sprintf(foo, "received %d bytes", nLen);		AfxMessageBox(foo);

	if((nLen>0)&&(pchData))
	{

char foo[256]; sprintf(foo, "received [%s]", pchData);		AfxMessageBox(foo);

		// decompile the data.
		CBufferUtil bu;
		int nNumRecordsExpected = bu.CountChar(pchData, nLen, '\r'); // CR marks the end of a record
		if(nNumRecordsExpected>0)
		{
			unsigned char* pucCmd = new unsigned char[nNumRecordsExpected];
			char** ppszData = NULL;
			if(pppszData) ppszData = new char*[nNumRecordsExpected];
			char* pchID = NULL;
			if(ppchID) pchID = new char[nNumRecordsExpected];

	// have to parse out in case multiple replies received at once.
	//	A command is formatted as follows:
	// [LF][ascii device ID][ascii of first hex digit][ascii of second hex digit][data][data][...][CR]
	// so we can simply search for the CR.

			CSafeBufferUtil sbu;
			char* pchRecord = sbu.Token(pchData, nLen, "\r\n"); // we can also strip the leading LF

			while(pchRecord)
			{
				int nRecordLen = strlen(pchRecord);
				if(nRecordLen>2)
				{
					if(pchID) pchID[nNumRecords] = *(pchRecord);  // device ID
					CBufferUtil bu;
					pucCmd[nNumRecords] = (unsigned char) bu.xtol((pchRecord+1), 2);
					if(ppszData)
					{
						if(nLen>5)  // otherwise no data.
						{
							ppszData[nNumRecords] = (char*)malloc(nRecordLen-2);
							if(ppszData[nNumRecords])
							{
								strncpy(ppszData[nNumRecords], pchRecord+3, nRecordLen-3);
								*(ppszData[nNumRecords]+nRecordLen-3) = 0; // zero term
							}
						}
					}
					nNumRecords++;
				}
				pchRecord = sbu.Token(NULL, NULL, "\r\n");
			}
			if(nNumRecords<=0)
			{
				if(pucCmd) delete [] pucCmd;
				if(ppszData) delete [] ppszData;
				if(pchID) delete [] pchID;
				if(ppucCmd) *ppucCmd = NULL;
				if(pppszData) *pppszData = NULL;
				if(ppchID) *ppchID = NULL;
			}
			else
			{
				if(pppszData) *pppszData = ppszData;
				if(ppchID) *ppchID = pchID;
			}
		}
		LeaveCriticalSection (&m_crit);
//		if(pchData) free(pchData);
		return nNumRecords;
	}

	LeaveCriticalSection (&m_crit);
//	if(pchData) free(pchData);
	return MDCD1_ERROR;
}

int CMDCD1::SetDevice(char chID)
{
	if((chID!=MDCD1_MD)&&(chID!=MDCD1_CD)&&(chID!=MDCD1_GLOBAL))
		return MDCD1_ERROR;
	m_chDevice = chID;
	return MDCD1_SUCCESS;
}


// command specific
int CMDCD1::Play()
{
	return MDCD1_ERROR;
}

int CMDCD1::Stop()
{
	return MDCD1_ERROR;
}

int CMDCD1::Eject()
{
	return MDCD1_ERROR;
}

int CMDCD1::SetAutoReady(bool bOn)
{
	return MDCD1_ERROR;
}

int CMDCD1::SelectTrack(int nTrack)
{
	return MDCD1_ERROR;
}

// get
char*	CMDCD1::GetTitleInfo(int nTrack)  // any track, not just current.  0 gives you disc title, -1 gives you current track
{
	return NULL;
}

int	CMDCD1::GetTimeInfo(int nTrack, unsigned long m_ulType)  // any track, not just current. 0 gives you disc title, -1 gives you current track
{
	return MDCD1_ERROR;
}

// setup thread
int CMDCD1::GetInitialInfo()  // checks if a disc is there, sets up all the info
{
	return MDCD1_ERROR;
}

bool CMDCD1::IsConnected()
{
	if((m_bThreadStarted)&&(m_serial.m_bConnected)) return true;
	return false;
}

void CDMD1CommThread(void* pvArgs)
{
//AfxMessageBox("begin thread");
	CMDCD1* pcdmd1 = (CMDCD1*)pvArgs;
	if(pcdmd1)
	{
		pcdmd1->m_bThreadStarted = true;

		bool bConnectErrorReport = false;
		while (!pcdmd1->m_bKillThread)
		{
			// open the conn...
			EnterCriticalSection (&pcdmd1->m_crit);
//AfxMessageBox("open conn");
			if(pcdmd1->m_serial.OpenConnection()==SERIAL_SUCCESS)
			{
				bConnectErrorReport = false;
				// get the info
				pcdmd1->GetInitialInfo();
				LeaveCriticalSection (&pcdmd1->m_crit);

				pcdmd1->m_bResetConnection = false;

				_timeb timestamp;
				_timeb polltime;
				_ftime( &timestamp );
				_ftime( &polltime );

				while ((!pcdmd1->m_bKillThread)&&(!pcdmd1->m_bResetConnection))
				{
					// parse all the timing stuff and messages...

					EnterCriticalSection (&pcdmd1->m_critGlobal);
					EnterCriticalSection (&pcdmd1->m_crit);

					// deal with queued commands
					if((pcdmd1->m_nNumCommands)&&(pcdmd1->m_ppszCommand))
					{
						while(pcdmd1->m_nNumCommands>0)
						{
							if(pcdmd1->m_ppszCommand[0])  // pull the one off the top
							{
								int nLen = pcdmd1->m_serial.SendData(pcdmd1->m_ppszCommand[0], strlen(pcdmd1->m_ppszCommand[0])); // do this in thread
								free(pcdmd1->m_ppszCommand[0]);
								pcdmd1->m_nNumCommands--;
								if(pcdmd1->m_nNumCommands==0)
								{
									delete [] pcdmd1->m_ppszCommand;
									pcdmd1->m_ppszCommand = NULL;
								}
								else
								{
									// cascade the commands.
									int n=0;
									while(n<pcdmd1->m_nNumCommands)
									{
										pcdmd1->m_ppszCommand[n] = pcdmd1->m_ppszCommand[n+1];
										n++;
									}
									pcdmd1->m_ppszCommand[n] = NULL;
								}

								if(nLen>0)
								{
									// get the reply
//									int nNumReplies = pcdmd1->ReceiveCommand(unsigned char** ppucCmd, char*** pppszData, char** ppchID)
									unsigned char* pucCmd;
									char* pchID;
									char** ppchData = NULL;
									int nNumReplies = pcdmd1->ReceiveCommand(&pucCmd, &ppchData, &pchID);
									if(nNumReplies>0)
									{
										int i=0;
										while(i<nNumReplies)
										{
											if(ppchData[i]) AfxMessageBox(ppchData[i]);
											if(ppchData[i]) free(ppchData[i]);
										}
										delete [] pucCmd;
										delete [] pchID;
										delete [] ppchData;
									}
//									else 		AfxMessageBox("Error receiving or nothing to receive");
								}
								else  //send error
								{
								}
							}
						}
					}

					LeaveCriticalSection (&pcdmd1->m_crit);
					LeaveCriticalSection (&pcdmd1->m_critGlobal);

				
					_ftime( &timestamp );

					if(
							(timestamp.time > polltime.time)
						||((timestamp.time == polltime.time)&&(timestamp.millitm >= polltime.millitm))
						)
					{
						polltime.time = timestamp.time + pcdmd1->m_nPollingIntervalMS/1000; 
						polltime.millitm = timestamp.millitm + (unsigned short)(pcdmd1->m_nPollingIntervalMS%1000); // fractional second updates
						if(polltime.millitm>999)
						{
							polltime.time++;
							polltime.millitm%=1000;
						}

						// poll the machine for status to update pcdmd1->m_ulStatus


					}

					Sleep(2); // dont peg processor

				} // inner while, keep monitoring.

				// close connection
				EnterCriticalSection (&pcdmd1->m_crit);
				pcdmd1->m_serial.CloseConnection();
				LeaveCriticalSection (&pcdmd1->m_crit);

			}
			else
			{
				LeaveCriticalSection (&pcdmd1->m_crit);
				if(!bConnectErrorReport)
				{
					pcdmd1->Message(MSG_ICONERROR|MSG_PRI_HIGH, "Error connecting to CD-MD1", "CDMD1CommThread");
					bConnectErrorReport = true;
				}
				Sleep(5000);  // wait 5 seconds before trying again, the unit may not even be on.
			}

		} // outer while, resets connections etc
		pcdmd1->m_bThreadStarted = false;
	}
//AfxMessageBox("end thread");
}
