// DedicateLiveFeedTesterDlg.h : header file
//

#if !defined(AFX_DEDICATELIVEFEEDTESTERDLG_H__6B35CFB8_C7D1_43B4_8C13_2A8495AA37BF__INCLUDED_)
#define AFX_DEDICATELIVEFEEDTESTERDLG_H__6B35CFB8_C7D1_43B4_8C13_2A8495AA37BF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveFeedTesterDlg dialog

class CDedicateLiveFeedTesterDlg : public CDialog
{
// Construction
public:
	CDedicateLiveFeedTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDedicateLiveFeedTesterDlg)
	enum { IDD = IDD_DEDICATELIVEFEEDTESTER_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDedicateLiveFeedTesterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDedicateLiveFeedTesterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEDICATELIVEFEEDTESTERDLG_H__6B35CFB8_C7D1_43B4_8C13_2A8495AA37BF__INCLUDED_)
