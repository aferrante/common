// DedicateLiveFeedTester.h : main header file for the DEDICATELIVEFEEDTESTER application
//

#if !defined(AFX_DEDICATELIVEFEEDTESTER_H__39D8B8B2_1DB5_45D4_B569_A97A3C53FEA8__INCLUDED_)
#define AFX_DEDICATELIVEFEEDTESTER_H__39D8B8B2_1DB5_45D4_B569_A97A3C53FEA8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveFeedTesterApp:
// See DedicateLiveFeedTester.cpp for the implementation of this class
//

class CDedicateLiveFeedTesterApp : public CWinApp
{
public:
	CDedicateLiveFeedTesterApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDedicateLiveFeedTesterApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDedicateLiveFeedTesterApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEDICATELIVEFEEDTESTER_H__39D8B8B2_1DB5_45D4_B569_A97A3C53FEA8__INCLUDED_)
