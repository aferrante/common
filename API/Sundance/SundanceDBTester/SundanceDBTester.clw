; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSundanceDBTesterDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SundanceDBTester.h"

ClassCount=3
Class1=CSundanceDBTesterApp
Class2=CSundanceDBTesterDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SUNDANCEDBTESTER_DIALOG

[CLS:CSundanceDBTesterApp]
Type=0
HeaderFile=SundanceDBTester.h
ImplementationFile=SundanceDBTester.cpp
Filter=N

[CLS:CSundanceDBTesterDlg]
Type=0
HeaderFile=SundanceDBTesterDlg.h
ImplementationFile=SundanceDBTesterDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_LIST_EVENTS

[CLS:CAboutDlg]
Type=0
HeaderFile=SundanceDBTesterDlg.h
ImplementationFile=SundanceDBTesterDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SUNDANCEDBTESTER_DIALOG]
Type=1
Class=CSundanceDBTesterDlg
ControlCount=12
Control1=IDOK,button,1208025088
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_DSN,edit,1350631552
Control4=IDC_EDIT_USER,edit,1350631552
Control5=IDC_EDIT_PW,edit,1350631552
Control6=IDC_BUTTON_CONNECTDB,button,1342242817
Control7=IDC_LIST_EVENTS,SysListView32,1350631949
Control8=IDC_EDIT_POLLMS,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_LIST_STATS,SysListView32,1350681101

