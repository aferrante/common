// SundanceDBTesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SundanceDBTester.h"
#include "SundanceDBTesterDlg.h"
#include "..\..\..\LAN\NetUtil.h"
#include "..\..\..\MFC\ODBC\DBUtil.h"
#include "..\..\..\MSG\Messager.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void SundanceAnalysisThread(void* pvArgs);
int CALLBACK CompareItems(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

typedef struct listsortinfo_t
{
	CListCtrl* pList;
	int nItem;
	bool bColSortDesc;
} listsortinfo_t;



/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()





CSundanceEvent::CSundanceEvent()
{
}

CSundanceEvent::~CSundanceEvent()
{
}


/////////////////////////////////////////////////////////////////////////////
// CSundanceDBTesterDlg dialog

CSundanceDBTesterDlg::CSundanceDBTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSundanceDBTesterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSundanceDBTesterDlg)
	m_nPollMS = 0;
	m_szDSN = _T("Sundance");
	m_szPw = _T("Raptors2006");
	m_szUser = _T("sa");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bKillThread = true;
	m_bThreadStarted = false;

	m_szError="";

	m_nLastRetrieveTime=0;
	m_nAveragedRetrieveTime=0;

	m_nLastAnalysisTime=0;
	m_nAveragedAnalysisTime=0;

	m_nLastErrorTime=0;
	m_nAveragedErrorTime=0;

	m_nNumCycles=0;
	m_nNumCyclesError=0;

	m_nColSort = 0;
	m_bColSortDesc = false;

	m_ppEvent=NULL;
	m_nNumEvents=0;

}

void CSundanceDBTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSundanceDBTesterDlg)
	DDX_Control(pDX, IDC_LIST_STATS, m_lcStats);
	DDX_Control(pDX, IDC_LIST_EVENTS, m_lcEvents);
	DDX_Text(pDX, IDC_EDIT_POLLMS, m_nPollMS);
	DDV_MinMaxInt(pDX, m_nPollMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_DSN, m_szDSN);
	DDX_Text(pDX, IDC_EDIT_PW, m_szPw);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSundanceDBTesterDlg, CDialog)
	//{{AFX_MSG_MAP(CSundanceDBTesterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECTDB, OnButtonConnectdb)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_EVENTS, OnColumnclickListEvents)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSundanceDBTesterDlg message handlers

BOOL CSundanceDBTesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	CRect rc; 
	m_lcStats.GetWindowRect(&rc);
	m_lcStats.InsertColumn(0, "", LVCFMT_LEFT, rc.Width()-16, 0 );

	m_lcEvents.GetWindowRect(&rc);
	m_lcEvents.InsertColumn(0, "air time", LVCFMT_LEFT, 60, 0 );
	m_lcEvents.InsertColumn(1, "event info", LVCFMT_LEFT, rc.Width()-16-60-60, 1 );
	m_lcEvents.InsertColumn(2, "last mod", LVCFMT_LEFT, 60, 1 );
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSundanceDBTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSundanceDBTesterDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSundanceDBTesterDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSundanceDBTesterDlg::OnButtonConnectdb() 
{
	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0))
	{
		GetDlgItem(IDC_BUTTON_CONNECTDB)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_CONNECTDB)->SetWindowText("Disconnecting...");
		m_db.DisconnectDatabase(m_db.m_ppdbConn[0]);
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
//		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
//		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
//		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);
//		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_bKillThread = true;

		while(m_bThreadStarted) Sleep(10);
		m_db.RemoveConnection(m_db.m_ppdbConn[0]);
		GetDlgItem(IDC_BUTTON_CONNECTDB)->SetWindowText("Connect to DB");
		GetDlgItem(IDC_BUTTON_CONNECTDB)->EnableWindow(TRUE);
	}
	else
	{
		if(!UpdateData(TRUE)) return;
		char szError[8096];

		CDBconn* pdb = m_db.CreateNewConnection(m_szDSN.GetBuffer(1),m_szUser.GetBuffer(1),m_szPw.GetBuffer(1));
		if((pdb)&&(m_db.ConnectDatabase(pdb,szError))>=NET_SUCCESS)
		{
//			AfxMessageBox("Connected!");

//			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);
//			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
//			GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
//			GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);
			m_bKillThread = false;
			GetDlgItem(IDC_BUTTON_CONNECTDB)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECTDB)->SetWindowText("Connecting...");

			if(_beginthread(SundanceAnalysisThread, 0, (void*)this)==-1)
			{
				//error.
				GetDlgItem(IDC_BUTTON_CONNECTDB)->SetWindowText("Connect to DB");
			}
			else
			{
				while(!m_bThreadStarted) Sleep(10);

				GetDlgItem(IDC_BUTTON_CONNECTDB)->SetWindowText("Disconnect DB");
			}
			GetDlgItem(IDC_BUTTON_CONNECTDB)->EnableWindow(TRUE);
		}
		else
		{
			AfxMessageBox(szError);
			if(pdb)
			{
				m_db.DisconnectDatabase(pdb);
				m_db.RemoveConnection(pdb);
			}
		}
	}
	
}


void SundanceAnalysisThread(void* pvArgs)
{
	CSundanceDBTesterDlg* pdlg = (CSundanceDBTesterDlg*)pvArgs;
	if(pdlg == NULL) return;

	pdlg->m_bThreadStarted = true;

	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	while(!pdlg->m_bKillThread)
	{
		// get all the items in the PlayView.
		// loop thru all of them until playing event is located
		// get time of retrieve, time of loop analysis

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM PlayView ORDER BY something"	);

		if((pdlg->m_db.m_ppdbConn)&&(pdlg->m_db.m_ucNumConnections>0)&&(pdlg->m_db.m_ppdbConn[0]))
		{
			int nIndex=-1;
			CRecordset* prs = pdlg->m_db.Retrieve(pdlg->m_db.m_ppdbConn[0], szSQL, errorstring);
			if(prs != NULL) // no error
			{
				while((!prs->IsEOF())&&(!pdlg->m_bKillThread))
				{

					bool bError = false;
					try
					{
						CString szTemp;
						prs->GetFieldValue("itemid", szTemp);
						nIndex = atoi(szTemp);



						pdlg->m_szError.Format("Retrieved %d items, no errors.", nIndex);
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						bError = true;
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							pdlg->m_szError.Format("Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							pdlg->m_szError.Format("Retrieve: Caught exception: out of memory.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							pdlg->m_szError.Format("Retrieve: Caught other exception.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();

					} 
					catch( ... )
					{
						bError = true;
						pdlg->m_szError.Format("Retrieve: Caught exception.\n%s", szSQL);
					}
					prs->MoveNext();
				}
				
				prs->Close();

				nIndex++;

				delete prs;
			}
			else
			{
			//	prs was null
				// do not add to time statistics.
			}
		
		}
		else
		{ 
			// problem.
			

		}



		if((pdlg->m_nPollMS>0)&&(!pdlg->m_bKillThread))
			Sleep(pdlg->m_nPollMS);
		else
			Sleep(0);
	}

	pdlg->m_bThreadStarted = false;

	_endthread();
}

void CSundanceDBTesterDlg::OnColumnclickListEvents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
//AfxMessageBox("column click");
	listsortinfo_t info;
	info.pList =  &m_lcEvents;
	info.nItem = pNMListView->iSubItem;

	if(m_nColSort != pNMListView->iSubItem)
	{
		m_bColSortDesc = false;
	}
	else
		m_bColSortDesc = !m_bColSortDesc;

	info.bColSortDesc = m_bColSortDesc;

	m_nColSort = pNMListView->iSubItem;
//	char d[256];
//	sprintf(d, "%d %d", pNMListView->iItem, pNMListView->iSubItem);
//	AfxMessageBox(d);
	m_lcEvents.SortItems(CompareItems, (LPARAM) &info);
	
	*pResult = 0;
}


static int CALLBACK CompareItems(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	// lParamSort contains a pointer to the list view control.
	// The lParam of an item is just its index.
	listsortinfo_t* pInfo = (listsortinfo_t*) lParamSort;

	static LV_FINDINFO fi;
	static int		 nItem1, nItem2;
 
	fi.flags = LVFI_PARAM;
	fi.lParam = lParam1;
	nItem1 = pInfo->pList->FindItem( &fi, -1 );
 
	fi.lParam = lParam2;
	nItem2 = pInfo->pList->FindItem( &fi, -1 );
 
	 CString    strItem1 = pInfo->pList->GetItemText(nItem1, pInfo->nItem);
	 CString    strItem2 = pInfo->pList->GetItemText(nItem2, pInfo->nItem);
//AfxMessageBox("sort");
//	char d[2256];
//	sprintf(d, "col %d: %d[%s] vs %d[%s]", pInfo->nItem, nItem1, strItem1, nItem2, strItem2);
//	AfxMessageBox(d);
	if (pInfo->bColSortDesc)
		return strcmp(strItem2, strItem1);
	else
		return strcmp(strItem1, strItem2);
}