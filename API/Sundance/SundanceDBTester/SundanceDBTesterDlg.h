// SundanceDBTesterDlg.h : header file
//

#if !defined(AFX_SUNDANCEDBTESTERDLG_H__399FECB8_CB5E_40E9_9A7D_EEBC3C26C5B2__INCLUDED_)
#define AFX_SUNDANCEDBTESTERDLG_H__399FECB8_CB5E_40E9_9A7D_EEBC3C26C5B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\..\..\MFC\ODBC\DBUtil.h"

class CSundanceEvent
{
public:
	CSundanceEvent();
	virtual ~CSundanceEvent();

	CString	m_szID;
	CString	m_szTitle;

};




/////////////////////////////////////////////////////////////////////////////
// CSundanceDBTesterDlg dialog

class CSundanceDBTesterDlg : public CDialog
{
// Construction
public:
	CSundanceDBTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSundanceDBTesterDlg)
	enum { IDD = IDD_SUNDANCEDBTESTER_DIALOG };
	CListCtrl	m_lcStats;
	CListCtrl	m_lcEvents;
	int		m_nPollMS;
	CString	m_szDSN;
	CString	m_szPw;
	CString	m_szUser;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSundanceDBTesterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL


public:
	CDBUtil m_db;
	bool m_bKillThread;
	bool m_bThreadStarted;

	CString	m_szError;

	int m_nLastRetrieveTime;
	int m_nAveragedRetrieveTime;

	int m_nLastAnalysisTime;
	int m_nAveragedAnalysisTime;

	int m_nLastErrorTime;
	int m_nAveragedErrorTime;

	int m_nNumCycles;
	int m_nNumCyclesError;


	int m_nColSort;
	bool m_bColSortDesc;


	CSundanceEvent** m_ppEvent;
	int m_nNumEvents;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSundanceDBTesterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnectdb();
	afx_msg void OnColumnclickListEvents(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUNDANCEDBTESTERDLG_H__399FECB8_CB5E_40E9_9A7D_EEBC3C26C5B2__INCLUDED_)
