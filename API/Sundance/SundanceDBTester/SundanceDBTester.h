// SundanceDBTester.h : main header file for the SUNDANCEDBTESTER application
//

#if !defined(AFX_SUNDANCEDBTESTER_H__3319C238_6FD9_4388_AC38_C603F1045DED__INCLUDED_)
#define AFX_SUNDANCEDBTESTER_H__3319C238_6FD9_4388_AC38_C603F1045DED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSundanceDBTesterApp:
// See SundanceDBTester.cpp for the implementation of this class
//

class CSundanceDBTesterApp : public CWinApp
{
public:
	CSundanceDBTesterApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSundanceDBTesterApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSundanceDBTesterApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUNDANCEDBTESTER_H__3319C238_6FD9_4388_AC38_C603F1045DED__INCLUDED_)
