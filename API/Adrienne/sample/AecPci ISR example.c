//
// AecPci ISR example.c
//--------------------------------------------------------
//	 AecPci ISR example
//	Uses the Win32 API calls to create a simple example of
//	how to use the interrupt service routine in connection
//	with the AEC PCI-TC devices.
//
//   Displays " hh:mm:ss:ff" in client area and interrupt count
//				for reader (and generator and video field) interrupts
//
//	REVISION: 
//		May 2000	NEW - A2
//
//
//	NOTE: This is NOT an MFC based program.
//			In order to modify for MFC, you will need to ...????? (TBD)
//--------------------------------------------------------
//#include "stdafx.h"
#include <windows.h>
#include <winioctl.h>		// in mstools\include dir
#include <stdio.h>
#include <stdlib.h>

#include <winioctl.h>
#include "AECioctl.h"        // This defines the IOCTL constants.
#include "Time.h"

//#include "AECiNTTC.h"		//PROTOTYPES OF THE AEC_NTTC.DLL FUNCTIONS
//This example (new for PCI devices) makes the calls directly to the DRIVER
//The DLL will be modified to include the calls to this driver TOO.

#define		true	1
#define		false	0
#define		NONE	0x0000
#define		VLTC	0x0050
#define		LTC		0x0098
#define		VITC	0x0093

#define     PCI_DEVICE_NAME     "AecPci"	//BASE device Name - Hard coded in the DRIVER

HANDLE	hDevice = INVALID_HANDLE_VALUE;

char *sLinkName = "\\\\.\\AecPci0";
char *sLinkNameE = "\\\\.\\AecPCIe0";	//'AEC' device zero
//char *sLinkName = "\\\\.\\AecPCIe0";	//'AEC' device zero


//VARIABLE DEFINITIONS

// -- These are used in ISR too!
volatile int	iOtherCnt, iVidCnt, iGenCnt, iReadCnt, iGpiCnt;
int				i1_4A, i5_8A, i9_12A, i13pA, i0A;
int				i1_4B, i5_8B, i9_12B, i13pB, i0B;
int				i1_4C, i5_8C, i9_12C, i13pC, i0C;
int				i1g,i2g,i3g, i4g, i5g, i6g, i7g;
volatile int	intertime0, intertimeMinus, intertimePlus;
int a,b,c,d,e,f;
volatile int	intspan1, intspan2, intspan3, iWait, iTCwait, iNext;


DWORD	Cnt1, Cnt2, Cnt3, DelFq, DelCnt, CntDif, toc;
LARGE_INTEGER	lpFq, lpCnt1, lpCnt2;


char buffer[30];
volatile BOOL	bRnew, bGnew, bVnew, bGPInew;
ULONG	lRTdat, lRUdat;
ULONG	lGTdat, lGUdat;
ULONG	lGPIdat;
HANDLE	hAecPci; //, hAecPciX;

HWND	 hwnd;			//GLOBAL access
int		iMB;
BOOL	ReaderAvailable, GeneratorAvailable, VideoAvailable;
BOOL	returnval1;
volatile BOOL exiting = FALSE;
volatile BOOL DONEexitingIsrThread = false;
int min = 999;
int max = 0;
int count = 0;
DWORD	RetDw;
int		aecint;
BOOL    IoctlResult;

UCHAR	iCode;
char	hhmmssff[50];		//data string to hold formated unpacked dataC
char	charStr[20], rev[4];//character string to hold data sent to screen
LARGE_INTEGER first1 = {0, 0};
LARGE_INTEGER second1 = {0, 0};
LARGE_INTEGER third1 = {0, 0};
LARGE_INTEGER first2 = {0, 0};
LARGE_INTEGER second2 = {0, 0};
LARGE_INTEGER third2 = {0, 0};

LARGE_INTEGER frequency;
//LARGE_INTEGER last = {0, 0};



ULONG data4, len, portaddress;

//FUNCTION PROTOTYPES
DWORD	TcISR1(HANDLE);	//my 1 isr
BOOL	DoReadWriteAction(HANDLE, enum AECPCI_ACTION_ITEM, ULONG, PULONG, PULONG);
BOOL	DoReadWriteActionISR(HANDLE, enum AECPCI_ACTION_ITEM, ULONG, PULONG, PULONG);
BOOL	DoReadWriteActionEventSetup(HANDLE, enum AECPCI_ACTION_ITEM, HANDLE, PULONG, PULONG);
int		LtoStrTm(ULONG, char*);
int		LtoStr12B(ULONG, char*);
UCHAR	deviceName[MAX_PATH];

//use this next line for win32 build
long WINAPI WndProc (HWND, UINT, WPARAM, LPARAM) ; //window proceedure (WndProc) PROTOTYPE
//Use Next line if x64 build
//LRESULT WndProc (HWND, UINT, WPARAM, LPARAM) ; //window proceedure (WndProc) PROTOTYPE

HDC			hdc;				
PAINTSTRUCT	ps;					
RECT		rect;				
MSG			msg;	//Global message structure that several
				    // functions use as a parameter

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{


	BOOL	device_opened = FALSE;		//if successful in opening PCI device-->true
	HANDLE hThreadTcISR1;               // Thread TcISR handle
	LONG	threadIdTcISR1;              // ID of thread TcISR
	BOOL    IoctlResult;
	int		iMB;
	UCHAR	BrdCapCode;					//byte read from offset 08h
	int i;

    static char szAppName[] = "AEC PCI?-TC INTERRUPT-EVENT TEST" ;
    //HWND	 hwnd ;		/changed to global
    WNDCLASS	 wndclass ;
	bRnew = FALSE;
	bGnew = FALSE;
	bVnew = FALSE;

	
	i = clock();

//	QueryPerformanceFrequency ( &frequency );	//for ISR use

    if (!hPrevInstance) // used to allow the creation of only one wndclass
          {
		  wndclass.style	 = CS_HREDRAW | CS_VREDRAW ;		
		  wndclass.lpfnWndProc	 = WndProc ;				
		  wndclass.cbClsExtra	 = 0 ;					
		  wndclass.cbWndExtra	 = 0 ;					
		  wndclass.hInstance	 = hInstance ;				
		  wndclass.hIcon	 = LoadIcon (NULL, IDI_APPLICATION) ;	
		  wndclass.hCursor	 = LoadCursor (NULL, IDC_ARROW) ;	
		  wndclass.hbrBackground = GetStockObject (WHITE_BRUSH) ;	
		  wndclass.lpszMenuName  = NULL ;				
		  wndclass.lpszClassName = szAppName ;				

		  RegisterClass (&wndclass) ;				
		  }

    hwnd = CreateWindowEx (WS_EX_TOPMOST, // ext. window style(always on top)
		    szAppName,		 // address of registered class name
		    "AecPCI?-TC ISR-Event EXAMPLE",		     // window text/header
		    //WS_THICKFRAME | WS_SYSMENU,	     // window style
			WS_BORDER | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX,
		    300,				     // initial x position
		    100,				     // initial y position
		    0x200,			     // window width
		    0x60,			     // window height
            NULL,                // parent window handle
            NULL,                // window menu handle
            hInstance,           // program instance handle
		    NULL) ;				// creation parameters

    // Copy the device name into the name buffer.
	//The driver will be consistent in the device number it assigns to each
	//of the AEC PCI-TC boards in the machine. The device number depends upon
	//the PCI slot the board is installed in and its position relative to any
	//others in the machine. In any case, the first board that is found in the
	//scan (enumeration) will be labled "0"


/*++
////////////////////////////////////////////////////////////////////////
// OpenByName
//		Open a handle to the requested device
HANDLE OpenByName(void)
{
	// Create a handle to the driver
	return CreateFile(sLinkName,
					  GENERIC_READ | GENERIC_WRITE,
					  FILE_SHARE_READ,
					  NULL,
					  OPEN_EXISTING,
					  0,
					  NULL);
}
--*/
//		strcpy(deviceName, "\\\\.\\");
//		strcat(deviceName, PCI_DEVICE_NAME);	//
//		strcat(deviceName, "0");	//This value will open Device"0"
//
//		hAecPci = CreateFile(
//                deviceName,	//  "\\\\.\\AecPci0",       // Open the Device "file"

		//Try to open PCIe-TC first
		hAecPci = CreateFile(
                sLinkNameE,	//  "\\\\.\\AecPCIe0",       // Open the Device "file"
                GENERIC_READ | GENERIC_WRITE,
                FILE_SHARE_READ | FILE_SHARE_WRITE,	//NULL (do not share if COM device)
                NULL,
                OPEN_EXISTING,
                0,		//FILE_FLAG_OVERLAPPED, ????????
                NULL
                );
               
    if (hAecPci == INVALID_HANDLE_VALUE)        // Was the device opened?
    {
//=========
		//if NOT PCIe-TC, then try PCI-TC...
		hAecPci = CreateFile(
                sLinkName,	//  "\\\\.\\AecPci0",       // Open the Device "file"
                GENERIC_READ | GENERIC_WRITE,
                FILE_SHARE_READ | FILE_SHARE_WRITE,	//NULL (do not share if COM device)
                NULL,
                OPEN_EXISTING,
                0,		//FILE_FLAG_OVERLAPPED, ????????
                NULL
                );
               
		if (hAecPci == INVALID_HANDLE_VALUE)        // Was the device opened?
		{
			//printf("Unable to open the device.\n");
			//int MessageBox(HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType); 
			iMB = MessageBox(hwnd, "Unable to open the Aec PCI?-TC device.",
				NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
				MB_ICONSTOP | MB_OK); 
			exit(1);
		}
		else 
		{
			returnval1 = TRUE;
		}
//=========
//        //printf("Unable to open the device.\n");
//		//int MessageBox(HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType); 
//		iMB = MessageBox(hwnd, "Unable to open the Aec PCIe-TC device.",
//			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
//			MB_ICONSTOP | MB_OK); 
//
//        exit(1);
    }
	else 
	{
		returnval1 = TRUE;
	}

//--------------------------------
		// This is important!! Make this program have highest Priority
		//		or interrupts will not be processed quickly enough
		SetPriorityClass ( GetCurrentProcess(), REALTIME_PRIORITY_CLASS );

//--------------------------------
	//----------
	//Do NOT Set the IntA enable bit at offset FE.4 for POLLED operations
	//  OR until you have set up things to be ready for IRQs.
	// This call makes sure that the INTA enable  is NOT set
	// writing 0x00000000 to Offset FC (Write4 must be used!)
/*++
	portaddress = 0x000000FC;	//Write4 to 0xFC	
	data4 = 0x00000000;			//IRQ DISABLE
	IoctlResult = DoReadWriteAction(hAecPci, Write4, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
	    //printf("\nW4 0@FC Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "Couldn't clear INTERRUPT enable.",
			NULL, MB_ICONSTOP | MB_OK);
		goto exit_routine;	
	}
	//   --*/
	//----------


//*++
// Determine what this device can do!
	portaddress = 0x00000008;		//Read1 from 0x08	
	data4 = 0x00000000;
	IoctlResult = DoReadWriteAction(hAecPci, Read1, portaddress, &data4, &len);
    if (!IoctlResult)    //error  // Did the IOCTL fail?
    {
        //printf("Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "R1 08",
			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
			MB_ICONSTOP | MB_OK); 
		goto exit_routine;				//Do NOT go on, bad problem!	
    }
	//Set capabilities flags
	BrdCapCode = (UCHAR)data4;
		//If bit is set... (Register mapping)
		//bit 7=Vitc	Generator	installed
		//bit 6=Vitc	Reader		installed
		//bit 5=LTC		Generator	installed
		//bit 4=LTC		Reader		installed
		//bit ?=OSD					installed
		//bit 1=Serial				installed
		ReaderAvailable = FALSE;
		GeneratorAvailable = FALSE;
		VideoAvailable = FALSE;

	if ((BrdCapCode & BIT7) != 0)	//VGen
	{
		GeneratorAvailable = TRUE;
		VideoAvailable = TRUE;
	}
	if ((BrdCapCode & BIT6) != 0)	//VRdr
	{
		ReaderAvailable = TRUE;
		VideoAvailable = TRUE;	//if OSD?
	}
	if ((BrdCapCode & BIT5) != 0)	//LGen
	{
		GeneratorAvailable = TRUE;
		VideoAvailable = TRUE;
	}
	if ((BrdCapCode & BIT4) != 0)	//LRdr
	{
		ReaderAvailable = TRUE;
		//VideoAvailable = TRUE;
	}
			//      --*/
/*++
//	Put the board into IDLE mode
	portaddress = 0x00000020;	//Write IDLE command to 0x2F
	data4 = 0x00000000;			//Do all time code available mode
	IoctlResult = DoReadWriteAction(hAecPci, SND_CMD, portaddress, &data4, &len);

	Sleep(100);

//	Start all of the board's reader/generators to run
//		simultaneously. 
	portaddress = 0x0000002F;	//Do ALL time code available mode
	data4 = 0x00000000;			//Do all time code available mode
	IoctlResult = DoReadWriteAction(hAecPci, SND_CMD, portaddress, &data4, &len);
	//NOTE: unless this is a VLTC/RG1 model, this will return a different
	//		command echo than 2Fh. Only "available" bits will be set/returned

	if (GeneratorAvailable)	//Start the generator RUNNING IF there is one
	{
		data4 = 0x00000000;			//START generating command 
		portaddress = 0x00000048;	//Write START generating command to 0x2F
		IoctlResult = DoReadWriteAction(hAecPci, SND_CMD, portaddress, &data4, &len);
	}

//The following items will initialize the PCI time code boards ready 
//to handle interrupts within an ISR Thread!

	//----------
	//cause a command response (will NAK) - no ISR/IRQ enable yet
	portaddress = 0x0000002F;	//Write1 to 0x2F	
	data4 = 0xFF;			//
	IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
	    //printf("W1 FF@2F Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "W1 FF@2F",
			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
			MB_ICONSTOP | MB_OK); 
		goto exit_routine;	
	}
	--*/
	//----------
	Sleep(50);	//Let the command be processed

	//----------
	portaddress = 0x00000033;	//Write1 to 0x33	//This is a device register
								//that is otherwise used in the comparator ops.
	data4 = 0x00;				//Normal "not allowed/not used" value
								//This program uses this register to handshake threads
								//This is a hard way to pass info to the ISR thread
	IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
		iMB = MessageBox(hwnd, "W1 00h@33h",
			NULL, MB_ICONSTOP | MB_OK); 
	}
	//----------
/*++
	//set the ENABLE bits so that when these interrupts occur, we will be told!
	//(Software interrupt only UNLESS FE.4 is set, see next)
	portaddress = 0x0000002E;	//Write1 to 0x2E	
	data4 = 0x07;			//INT READER/GEN/VIDEO FLAG ENABLES
	IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
		//printf("Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "W1 07@2E",
			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
			MB_ICONSTOP | MB_OK); 
		goto exit_routine;	
	}
	//----------
	//Set the IntA enable bit at offset FE.4 by
	// writing 0x00000010 to Offset FC (Write4 must be used!)
	portaddress = 0x000000FC;	//Write4 to 0xFC	
	data4 = 0x00000010;			//IRQ ENABLE
	IoctlResult = DoReadWriteAction(hAecPci, Write4, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
	    //printf("W4 10@FC Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "W4 10@FC",
			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
			MB_ICONSTOP | MB_OK); 
		goto exit_routine;	
	}
	--*/

/*++
At this point, the NAK should be available at the command response
register (offset 0Fh). No IRQ will be driven UNTIL this register is
read (Reading from 0Fh will clear FE.0) AND a 00h value is written 
to the "Command" input register (offset 2Fh) to RE-ENABLE the next 
interrupt to occur.
--*/

	/*++
	//Read from offset 0Fh
	portaddress = 0x0000000F;		//Read1 from 0x0F	//TIME data
	data4 = 0x00000000;
	IoctlResult = DoReadWriteAction(hAecPci, Read1, portaddress, &data4, &len);
    if (!IoctlResult)                            // Did the IOCTL fail?
    {
        //printf("Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "R1 0F",
			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
			MB_ICONSTOP | MB_OK); 
		goto exit_routine;		
    }
	--*/

/*++
Now when the ISR thread is started, it will place the "get interrupt"
 request THEN it writes a 00H value to offset 2Fh, enableing the next
 interrupt to generate an ISR that is ready to be handled.
--*/
//----------

////-------------- 
// Now create the thread to handle the Interrupts.
// SINCE THE BOARD has ALREADY been put into a "paused" mode, (see all of 
// the above) the thread may be started once the PRIORITY has been set.
////--------------
	
	if (returnval1)	// if not fail
	{
		hThreadTcISR1  = CreateThread(
			NULL, 
			0,
			(LPTHREAD_START_ROUTINE)TcISR1, 
			hAecPci,			//does not do anything
			CREATE_SUSPENDED, 
			&threadIdTcISR1);

		// This is important!! Make this Thread have highest Priority
		//		or interrupts will not be processed quickly enough
		SetThreadPriority(hThreadTcISR1, THREAD_PRIORITY_TIME_CRITICAL);
	} 
        // Start up the isr threads
	if (returnval1)	// if not fail
		RetDw = ResumeThread(hThreadTcISR1); // start this thread now

    ShowWindow (hwnd, nCmdShow) ;	
    UpdateWindow (hwnd) ;			
	
	InvalidateRect ( hwnd, NULL, 0 );
	GetMessage (&msg, NULL, 0, 0);
    do					//LOOP TILL THE PROGRAM IS EXITED
	{
		TranslateMessage (&msg) ;				
		DispatchMessage (&msg) ;				
		//InvalidateRect ( hwnd, NULL, 0 );		// forces redraw of window
		do
		{
			Sleep(50);	//don't be a processor hog!
			if (bRnew || bGnew  || bVnew || bGPInew)	//These values are SET in ISR thread
			{
				InvalidateRect ( hwnd, NULL, 0 );		// forces redraw of window
			}			
		}
		while (!(PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))); //while no msg
		//Nonzero -> success, Zero -> failure
	    // look for any messages (allows Windows access to Main loop)

		TranslateMessage (&msg) ;				
		DispatchMessage (&msg) ;				
		InvalidateRect ( hwnd, NULL, 0 );		// forces redraw of window
	}
    while (GetMessage (&msg, NULL, 0, 0));		
 
	TranslateMessage (&msg) ;					
    DispatchMessage (&msg) ;					

	//======================================================================
	//----------
	portaddress = 0x00000033;	//Write1 to 0x33	//This is a device register
								//that is otherwise used in the comparator ops.
	data4 = 0xFF;				//Normal "not allowed/not used" value
								//This program uses this register to handshake threads
								//This is a hard way to pass info to the ISR thread
	IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
		iMB = MessageBox(hwnd, "W1 FFh@33h",
			NULL, MB_ICONSTOP | MB_OK); 
	}
	//----------
exit_routine:
		exiting = TRUE;
		//	THE NEXT ITEMS ARE IMPORTANT WHEN EXITING THE PROGRAM
		//  THAT HAS BEEN USING INTERRUPTS.
		//
		//1)Force an interrupt,  by sending a dummy command
		//  Purpose: This will allow the ISR thread to exit cleanly
		//	if it has not already done so. 
		// Repeat... for queued IRPs? (until isr thread has exited...)
		//2)
		//  Do this AFTER confirmation that the ISR thread has
		//  exited!
		//  Clear the IntA enable bit at offset FE.4 by
		//  writing 0x00000000 to Offset FC (Write4 must be used!)
		//
		//These are done in the following segements!
		Sleep(1000);
		
//----------
	portaddress = 0x0000002E;	//Write1 to 0x2E	
	data4 = 0x00;			//DISABLE INT READER FLAG ENABLES on exit
	IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
	    //printf("\nW1 0@2E Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "W1 00@2E",
			NULL, MB_ICONSTOP | MB_OK); 
		goto final_exit;	
	}
//----------
	//do .... 
	//{
	// w1(2f) ffh	//dummy command
	//check to see if there is a pending 'int'
	// r1(fe.0)	//note value
	// r1(0f)
	// w1(2f) 00h	//enable next
	// w1(2f) ffh	//dummy command
	//check to see if there is a pending 'int'
	// r1(fe.0) //see if different than prior (should have bit set here, even if not before)
	//if '1'
	//		r1(0f)	
	//		w1(2f) 00
	//if '0'
	//		w1(2f) 00	}
	//		while (!DONEexitingIsrThread);

	//cause a command response - IRQ looping has been turned off (ie: exiting = TRUE)
	//This will cause a "phony command response" interrupt to occur, but will 
	//NOT re-enable the board to generate any more driver calls to handle interupts. 
	//The ISR will clear 
	//this interrupt and if there is an ISR thread running, it will end 
	//because the EXITING flag is now TRUE! See the "while (!exiting)" code.
	if (!DONEexitingIsrThread)
	{
		do
		{
			portaddress = 0x0000002F;	//Write1 to 0x2F	
			data4 = 0xFF;			//
			IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
			if (!IoctlResult)                            // Did the IOCTL fail?
			{
				//printf("\nW1 FF@2F Ioctl failed with code %ld\n", GetLastError() );
				iMB = MessageBox(hwnd, "W1 FF@2F",
					NULL, MB_ICONSTOP | MB_OK); 
				goto final_exit;	
			}
			Sleep(100);
			//----------
			//Read from offset 0Fh
			portaddress = 0x0000000F;		//Read1 from 0x0F	//TIME data
			data4 = 0x00000000;
			IoctlResult = DoReadWriteAction(hAecPci, Read1, portaddress, &data4, &len);
			Sleep(100);
		} while (!DONEexitingIsrThread);
	}
//----------
	// on this pass, prior to writing the 00h to 2Fh, clear the hardware Interrupt line driving
	//ability. Note: Better code would make sure that the ISR thread has closed prior to doing
	//this next step. If not closed, loop on the above until it is closed. This will keep
	//the driver happy and allow for proper application closing/exiting.
	/*++
	portaddress = 0x000000FC;	//Write4 to 0xFC	
	data4 = 0x00000000;			//IRQ DISABLE
	IoctlResult = DoReadWriteAction(hAecPci, Write4, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
	    //printf("\nW4 0@FC Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "Couldn't clear INTERRUPT enable.",
			NULL, MB_ICONSTOP | MB_OK);
		goto final_exit;
	}
	//----------
	Sleep(100);
	//----------
	portaddress = 0x0000002F;	//Write1 to 0x2F	
	data4 = 0x00;			//
	IoctlResult = DoReadWriteAction(hAecPci, Write1, portaddress, &data4, &len);
	if (!IoctlResult)                            // Did the IOCTL fail?
	{
	    //printf("\nW1 FF@2F Ioctl failed with code %ld\n", GetLastError() );
		iMB = MessageBox(hwnd, "W1 00@2F",
			NULL, MB_ICONSTOP | MB_OK); 
		goto final_exit;	
	}	// --*/
	//----------
final_exit:
    if (!CloseHandle(hAecPci))                  // Close the Device "file".
    {
        //printf("Failed to close device.\n");
		iMB = MessageBox(hwnd, "Unable to Close the Aec PCI?-TC device.",
			NULL, MB_ICONSTOP | MB_OK);
    }
//----------

	return 0;
}
//################################################################
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// *********************  WINDOW PROC  ***************************
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//use this next line for win32 build
long WINAPI WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
//Use Next line if x64 build
//LRESULT WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
     {
     switch (message)						
          {
	  case WM_PAINT:
		  hdc = BeginPaint (hwnd, &ps) ;			
		  GetClientRect (hwnd, &rect) ;			


//Handle reader, video field and Generator interrupts
//
//		  DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
//		       hhmmssff,		//print the number of interrupts that each
//		       wsprintf (hhmmssff,"%d  %d  %d  %d", //of the ISRs have processed
//			   iOtherCnt, iVidCnt, iGenCnt, iReadCnt), // **NEW** prints out int timing info Too.
//		       &rect,
//		       DT_SINGLELINE | DT_BOTTOM | DT_RIGHT);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (bRnew)
		{
			bRnew = FALSE;		//reset this flag
			//lRTdat, lRUdat iReadCnt

			aecint = LtoStrTm(lRTdat, charStr);
			DrawText (hdc,			//PRINT IN THE WINDOW
		       charStr,
		       aecint,
		       &rect,
		       DT_SINGLELINE | DT_CENTER);
			DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
				hhmmssff,
				wsprintf (hhmmssff,"%d",
				iReadCnt),
				&rect,
				DT_SINGLELINE | DT_RIGHT );

//	//--->
//			DrawText (hdc,		
//				hhmmssff,
//				wsprintf (hhmmssff,"A=%d %d %d %d %d  B=%d %d %d %d %d C=%d %d %d %d %d",
//					i0A, i1_4A, i5_8A, i9_12A, i13pA, i0B, i1_4B, i5_8B, i9_12B, i13pB, i0C, 
//					i1_4C, i5_8C, i9_12C, i13pC),
//				&rect,
//				DT_SINGLELINE | DT_BOTTOM );
//
//			DrawText (hdc,		
//				hhmmssff,
//				wsprintf (hhmmssff,"Span=%d %d %d %d %d %d %d", i1g,i2g,i3g, i4g, i5g, i6g, i7g),
//				&rect,
//				DT_SINGLELINE | DT_VCENTER );
//int				i1_4A, i5_8A, i9_12A, i13pA;
//int				i1_4B, i5_8B, i9_12B, i13pB;
//int				i1_4C, i5_8C, i9_12C, i13pC;
//int				i1g,i2g,i3g;
	//  <----
		}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (bGnew)			//print the GENERATOR data
		{		
			bGnew = FALSE;		//reset this flag
			//lGTdat, lGUdat
			aecint = LtoStrTm(lGTdat, charStr);
			DrawText (hdc,			//PRINT IN THE WINDOW
			    charStr,
				aecint,
				&rect,
				DT_SINGLELINE | DT_VCENTER | DT_CENTER );
			DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
				hhmmssff,
				wsprintf (hhmmssff,"%d",
				iGenCnt),
				&rect,
				DT_SINGLELINE | DT_VCENTER | DT_RIGHT );
		}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (bVnew)	//print the video field interrupt timing...
		{
			bVnew = FALSE;		//reset flag value
			
//			DrawText (hdc,			//PRINT IN THE WINDOW
//			    charStr,
//				aecint,
//				&rect,
//				DT_SINGLELINE | DT_RIGHT | DT_BOTTOM);
			DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
				hhmmssff,
				wsprintf (hhmmssff,"%d",
				iVidCnt),
				&rect,
				//DT_SINGLELINE | DT_RIGHT | DT_BOTTOM);
				DT_SINGLELINE | DT_CENTER | DT_BOTTOM);
		}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if (bGPInew)
		{
			bGPInew = false;
			//iGpiCnt
			DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
				hhmmssff,
				wsprintf (hhmmssff,"%d",
				iGpiCnt),
				&rect,
				//DT_SINGLELINE | DT_RIGHT | DT_BOTTOM);
				DT_SINGLELINE | DT_CENTER | DT_BOTTOM);
			//lGPIdat
			aecint = LtoStr12B(lGPIdat, charStr);
			DrawText (hdc,			//PRINT IN THE WINDOW
		       charStr,
		       aecint,
		       &rect,
		       DT_SINGLELINE | DT_RIGHT | DT_BOTTOM);
		}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		else	//something else	//Revision info???, size change
		{
			DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
				hhmmssff,
				wsprintf (hhmmssff,"READER"),
				&rect,
				DT_SINGLELINE );
			if (!ReaderAvailable)
				DrawText (hdc,		
					hhmmssff,
					wsprintf (hhmmssff,"N/A"),
					&rect,
					DT_SINGLELINE | DT_CENTER );
			//----------------------------------------------
			DrawText (hdc,		
				hhmmssff,
				wsprintf (hhmmssff,"GENERATOR"),
				&rect,
				DT_SINGLELINE | DT_VCENTER );
			if (!GeneratorAvailable)
				DrawText (hdc,		
					hhmmssff,
					wsprintf (hhmmssff,"N/A"),
					&rect,
					DT_SINGLELINE | DT_VCENTER | DT_CENTER );
			//-----------------------------------------------
			//*++	Leave for GPI
			DrawText (hdc,		
				hhmmssff,
				wsprintf (hhmmssff,"GPI"),
				&rect,
				DT_SINGLELINE | DT_BOTTOM );	// --*/
			/*++	Remove if NOT GPI
			if(!VideoAvailable)
				DrawText (hdc,		
					hhmmssff,
					wsprintf (hhmmssff,"N/A"),
					&rect,
					DT_SINGLELINE | DT_BOTTOM | DT_CENTER );	//  --*/
	//-----------------------------------------------
			//this is NOT needed here becasue we are NOT using
			// the AEC_nttc.DLL file
//			DrawText (hdc,		//FORMAT TEXT FOR THE WINDOW
//				hhmmssff,
//				wsprintf (hhmmssff,"Rev: %c%c", rev[0], rev[1]),
//				&rect,
//				DT_SINGLELINE | DT_RIGHT );
		}
		EndPaint (hwnd, &ps) ;					
	       return 0 ;								

	  case WM_DESTROY:								
	       PostQuitMessage (0) ;					
	       return 0 ;								
          }

     return DefWindowProc (hwnd, message, wParam, lParam) ;	
     }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-----------------------------------------------------------
//Convert a LONG (4byte) value into a character string of TIME (Tm)
//This does NOT mask embedded bits 
// use "dw = (dw & 0x3F7F7F3F);" to mask embedded bits IF needed
//before calling this routine.
//------------------------------------------------------------
int LtoStrTm(ULONG dw, char *chs)	
{
	wsprintf (chs,"%8.8X",dw);
	chs[11] = 0;	//EOL
	chs[10] = chs[7];
	chs[9] = chs[6];
	chs[8] = ':';
	chs[7] = chs[5];
	chs[6] = chs[4];
	chs[5] = ':';
	chs[4] = chs[3];
	chs[3] = chs[2];
	chs[2] = ':';
	return 11;
}
//
//-----------------------------------------------------------
//Convert a LONG (4byte) value into a 12 0/1 character string
// use "dw = (dw & 0x3F7F7F3F);" to mask embedded bits IF needed
//before calling this routine.
//------------------------------------------------------------
int LtoStr12B(ULONG dw, char *chs)	
{
	ULONG d, d12;
	d = (dw & 0x0FFF0000);	//mask all but the 12 GPI bits
	//d = d / 0x10000;		//shift down 16 bits
	//wsprintf (chs,"%8.8X",dw);
	//12
	if (d & 0x08000000)
		chs[0] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[0] = '0';
	//11
	if (d & 0x04000000)
		chs[1] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[1] = '0';
	//10
	if (d & 0x02000000)
		chs[2] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[2] = '0';
	//9
	if (d & 0x01000000)
		chs[3] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[3] = '0';

	chs[4] = ' ';	//add a space after each nibble

	//8
	if (d & 0x00800000)
		chs[5] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[5] = '0';
	//7
	if (d & 0x00400000)
		chs[6] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[6] = '0';
	//6
	if (d & 0x00200000)
		chs[7] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[7] = '0';
	//5
	if (d & 0x00100000)
		chs[8] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[8] = '0';

	chs[9] = ' ';	//add a space after each nibble

	//4
	if (d & 0x00080000)
		chs[10] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[10] = '0';
	//3
	if (d & 0x00040000)
		chs[11] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[11] = '0';
	//2
	if (d & 0x00020000)
		chs[12] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[12] = '0';
	//1
	if (d & 0x00010000)
		chs[13] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[13] = '0';

	chs[14] = 0; //EOL
	return 14;
}
//-----------------------------------------------------------
/*++	****** DEFINED ACTIONS SUPPORTED BY THE DRIVER ******
AECPCI_ACTION_ITEM parameter must be one of the following!
NOTE: The values ARE case sensitive!

    NOP_THING,         //  No operation		0                   0
    Read1,              //  input byte			offset				data (in)	
    Read2,              //  input word			offset				data (in)
    Read4,              //  input long			offset				data (in)
    Write1,             //  output byte			offset				data (out)
    Write2,             //  output word 		offset				data (out)
    Write4,             //  output long 		offset				data (out)

	SND_CMD,		//send command			command(out)		intcode(in)
					//read the NEXT INTCODE - fixed OFFSET read / enable next
	GET_INT			//get int code			0 (don't care?)		intcode(in)
					//
	RTM,			//  input long			0 (don't care?)		data (out)
					//Read the TIME bits at offset 10h (transition tolerent) DWORD
	RUB,			//  input long			0 (don't care?)		data (out)
					//Read the USER bits at offset 14h (transition tolerent) DWORD
	REB,			//  input byte			0 (don't care?)		data (out)
					//Read the EMBEDDED bits at offset 18h (transition tolerent) UCHAR
	NextInt,		//  output byte			0 (don't care)		data (out)
					//Queue an IRP for the ISR/DPC to complete

				--*/
BOOL 
DoReadWriteAction(
				  IN HANDLE hOpen, 
				  IN enum AECPCI_ACTION_ITEM act, 
				  IN ULONG port,
				  IN OUT PULONG pdata,
				  IN PULONG pLB
				  )
{

	BOOL status;
		//enum AECPCI_ACTION	action;		// action to perform
		//ULONG					port;       // port address
		//ULONG					value;      // input or output data
	AECPCI_ACTION_ITEM		ai;			// *PAECPCI_ACTION_ITEM;
	//IOCTL_AECPCI_PROCESS_BUFFER
	//ULONG   LengthBack;

	ai.action = act;
	ai.port = port;
	ai.value = *pdata;

	//Nonzero indicates success. 
	//Zero indicates failure. 
	//To get extended error information, call GetLastError. 

	//status = TRUE;		//TEMP debug
	//*pdata = 0x13;		//TEMP debug

	if (status = DeviceIoControl(		
                hOpen,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_BUFFER,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ai,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ai),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ai,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ai),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                )				
		)
	{		*pdata = ai.value;		}

	return status;
}
//---------------------------------------------------------------------------------
BOOL 
DoReadWriteActionISR(
				  IN HANDLE hOpen, 
				  IN enum AECPCI_ACTION_ITEM act, 
				  IN ULONG port,
				  IN OUT PULONG pdata,
				  IN PULONG pLB
				  )
{

	BOOL status;
		//enum AECPCI_ACTION	action;		// action to perform
		//ULONG					port;       // port address
		//ULONG					value;      // input or output data
	AECPCI_ACTION_ITEM		ai;			// *PAECPCI_ACTION_ITEM;
	//IOCTL_AECPCI_PROCESS_BUFFER
	//ULONG   LengthBack;

	ai.action = act;
	ai.port = port;
	ai.value = *pdata;

	//Nonzero indicates success. 
	//Zero indicates failure. 
	//To get extended error information, call GetLastError. 

	//status = TRUE;		//TEMP debug
	//*pdata = 0x13;		//TEMP debug

	if (status = DeviceIoControl(		
                hOpen,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_BUFFER,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ai,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ai),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ai,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ai),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                )				
		)
	{		*pdata = ai.value;		}

	return status;
}

//---------------------------------------------------------------------------------
BOOL 
DoReadWriteActionEventSetup(
				  IN HANDLE hOpen, 
				  IN enum AECPCI_ACTION_ITEM act, 
				  IN HANDLE hEvent,
				  IN OUT PULONG pdata,
				  IN PULONG pLB
				  )
{

	BOOL status;
		//enum AECPCI_ACTION	action;		// action to perform
		//ULONG					port;       // port address
		//ULONG					value;      // input or output data
	AECPCI_EVENT_HANDLE_ITEM		ae;			// *PAECPCI_ACTION_ITEM;
	//IOCTL_AECPCI_PROCESS_BUFFER
	//ULONG   LengthBack;

	ae.action = act;
	ae.hWaitEventHandle = hEvent;

	//Nonzero indicates success. 
	//Zero indicates failure. 
	//To get extended error information, call GetLastError. 

	//status = TRUE;		//TEMP debug
	//*pdata = 0x13;		//TEMP debug

	//if (
		//If the operation fails or is pending, the return value is zero. 
		//To get extended error information, call GetLastError.
		status = DeviceIoControl(		
                hOpen,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_EVENT,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ae,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ae),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ae,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ae),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                );				
	//	)
	//{		*pdata = ae.value;		}

	return status;
}
//============================================================================
//-----------------------------------------------------------
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DWORD TcISR1(HANDLE hAecPciX)	//FIRST BOARD THAT THE DRIVER SEES (Device0)
{
// ++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++
//	int a,b,c;
//	char buffer[30];
// ++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++

	ULONG		pAddr, d4, dLn, uL4, d4CMD;
	DWORD		dwRet;
	BOOLEAN		IOResult;
	//BOOLEAN		ok_q;
	UCHAR		iCd;
	UCHAR		BrdCapCode, b1;
	UCHAR		tempE0, tempE1;
	HANDLE		hIsrEvent;
//	LARGE_INTEGER now;

/////////////////////////////
	BOOL status;
	AECPCI_EVENT_HANDLE_ITEM		ae;	
/////////////////////////////



	exiting = false;

/*++
	HANDLE	hAecPciInt;

	hAecPciInt = CreateFile(
            //deviceName,	//  "\\\\.\\AecPci0",       
			sLinkName,
            GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ | FILE_SHARE_WRITE,	
            NULL,
            OPEN_EXISTING,
            0,		
            NULL
            );

    if (hAecPciInt == INVALID_HANDLE_VALUE)        // Was the device opened?
    {
        //printf("Unable to open the device.\n");
		//int MessageBox(HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType); 
		iMB = MessageBox(hwnd, "Unable to open the Aec PCI ISR device.",
			NULL, //default is "Error" if null "Caption-Unable to open the device.\n", 
			MB_ICONSTOP | MB_OK); 

		hAecPciInt = NULL;
        exiting = TRUE;
    }
--*/

//		ok_q = QueryPerformanceFrequency(&frequency);
////		DelFq = lpFq.LowPart;
////		DelCnt = DelFq / 1000;	// for ~ 1ms freq. count

// <--------- 

	pAddr = 0x00000008;	//board 'capabilities' register
	//Do some general setups to enable all of the modes for TC in/out available 
	//for this test/demonstration example
	d4 = 0x00;
	DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
	BrdCapCode = (UCHAR)d4;

	//---------
	//Disable hardware interrupts on the way out....
	pAddr = 0x000000FC;		//hardware interupt enable/disable reg.
	d4 = 0x00000000;				//'disable' value
	DoReadWriteAction(hAecPciX, Write4, pAddr, &d4, &dLn);
	pAddr = 0x000000FC;		//hardware interupt enable/disable reg.
	d4 = 0x00000000;				//'disable' value
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);
	//---------

	//Disable 'int' enable bit at 2Eh
	pAddr = 0x0000002E;		//Write1 to 0x2E	
	d4 = 0x00;			//INT READER/GEN/VIDEO FLAG ENABLES
	IoctlResult = DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);
	
	d4CMD = 0x00000020;	//start with 'IDLE'
	//Now add to this to enable all the features available
//	if ((BrdCapCode & 0x80) != 0)	//there is a VITC generator
//		d4CMD = d4CMD + 0x08;	//add 'Bit3'
	if ((BrdCapCode & 0x40) != 0)	//there is a VITC reader
		d4CMD = d4CMD + 0x02;	//add 'Bit3'
	if ((BrdCapCode & 0x20) != 0)	//there is a LTC generator
		d4CMD = d4CMD + 0x04;	//add 'Bit3'
	if ((BrdCapCode & 0x10) != 0)	//there is a LTC reader
		d4CMD = d4CMD + 0x01;	//add 'Bit3'

	pAddr = 0x20;				//command to send
	d4 = 0x00000000;			//null return value
	if (DoReadWriteAction(hAecPciX, SND_CMD, pAddr, &d4, &dLn))
		uL4 = d4;

	pAddr = 0x000000FE;	//Int Pending?
	d4 = 0x00;
	DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
	b1 = (UCHAR)d4;

	//pAddr = 0x0000002F;		//command to send
	pAddr = d4CMD;				//command to send
	d4 = 0x00000000;			//null return value
	if (DoReadWriteAction(hAecPciX, SND_CMD, pAddr, &d4, &dLn))
		uL4 = d4;

	pAddr = 0x000000FE;	//Int Pending? DEBUG 
	d4 = 0x00;
	DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
	b1 = (UCHAR)d4;

	Sleep(100);

	if ((BrdCapCode & 0xA0) != 0)	//there is a generator to start
	{
		//send "command" to board, (write byte to 2Fh0
//		int i = 0;
//		do {

			pAddr = 0x00000048;			//command to send
			d4 = 0x00000000;			//start "generator"
			if (DoReadWriteAction(hAecPciX, SND_CMD, pAddr, &d4, &dLn))
				uL4 = d4;
			pAddr = 0x000000FE;	//Int Pending? DEBUG
			d4 = 0x00;
			DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
			b1 = (UCHAR)d4;

//		} while ((uL4 != 0x48) && (i < 20));
	}
	Sleep(200);

	//This 'read' is for debug to see if generator (if present) has started
	pAddr = 0x00000040;//Read4 from 0x0C - Status Codes
	d4 = 0x00000000;
	//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
	if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
		lRUdat = d4;

	//"Prime the ISR pump" for use with the PCI-TC & PCIe-TC card use

	//-----------
	//send partial "command" to board, (write 'cmd' byte to 2Fh0) not the same as SND_CMD
	pAddr = 0x0000002F;			//command write address
	d4 = 0x0000005B;			//cmd, a 'do nothing commmand value'
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);
	Sleep(10);

	//read '0Fh' (clear int pending bit)
	pAddr = 0x0000000F;	//board 'int code' output
	d4 = 0x00;
	DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);

	//enable hardware interrupts (write4 10h to FCh
	pAddr = 0x000000FC;		//hardware interupt enable/disable reg.
	d4 = 0x00000010;				//'enable' value
	DoReadWriteAction(hAecPciX, Write4, pAddr, &d4, &dLn);
	pAddr = 0x000000FC;		//hardware interupt enable/disable reg.
	d4 = 0x00000010;				//'enable' value
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);

	pAddr = 0x000000FE;	//Int Pending? debug check
	d4 = 0x00;
	DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
	b1 = (UCHAR)d4;

	//reset the 'int enable' bits at 2Eh	Allow all 'normal' interrupts
	pAddr = 0x0000002E;			//command input reg.
	d4 = 0x00000007F;			//enable interrupts
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);


//	Sleep(2000);
//	MessageBeep(0xFFFFFFFF);

// / *++ EVENT SETUP-  InterruptServiceRoutine (ISR) SETUP section. Required if using EVENT method. 
		//typedef struct _AECPCI_EVENT_HANDLE_ITEM
		//	{
		//		enum AECPCI_ACTION  action; // action to perform
		//		HANDLE				hWaitEventHandle;	//handle from 'CreateEvent'.
		//	} AECPCI_EVENT_HANDLE_ITEM, *PAECPCI_EVENT_HANDLE_ITEM;
		//IOCTL_AECPCI_PROCESS_EVENT
		//if DoReadWriteActionEventSetup(hAecPciX, WAITEVENT, hEvent, &d4, &dLn)
		// ...
    
	hIsrEvent = CreateEvent(NULL, TRUE, FALSE, NULL); //Get Event Handle
	//NULL security... / TRUE must RESET / NOT Signaled initially / no name

	if (hIsrEvent == NULL)	//error
	{
			exiting = true;	//exiting will occur befor loops
	}
	else //make IOCTL call to driver to setup Event...
	{
/*
	///*++ EVENT handler shutdown START
	DoReadWriteActionEventSetup(hAecPciX, WAITEVENT, NULL, &d4, &dLn);	//pass a null handle to 'decrement' in driver	
	CloseHandle(hIsrEvent);

*/
		if (DoReadWriteActionEventSetup(hAecPciX, WAITEVENT, hIsrEvent, &d4, &dLn))
/*++
			ae.action = WAITEVENT;
			ae.hWaitEventHandle = hIsrEvent;
			status = DeviceIoControl(		
                hAecPciX,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_EVENT,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ae,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ae),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ae,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ae),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				&dLn, //pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                );
		if (status)	// --*/
		{
			exiting = false;
		}
		else exiting = true;
	}



	/*++
	//debug++
	pAddr = 0x00000098;//Read4 from 0x98 - 
	d4 = 0x00000000;
	//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
	if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
		lRUdat = d4;
	pAddr = 0x00000099;//Read4 from 0x99 - 
	d4 = 0x00000000;
	//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
	if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
		lRTdat = d4;	//lRUdat, lRTdat
	pAddr = 0x0000000C;//Read4 from 0x0C - Status Codes
	d4 = 0x00000000;
	//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
	if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
		lRUdat = d4;
	Sleep(100);

	pAddr = 0x00000040;//Read4 from 0x0C - Status Codes
	d4 = 0x00000000;
	//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
	if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
		lRUdat = d4;
	//debug--		//  --*/
// EVENT SETUP END 
// --*/ 		


	//exiting = true;

	if (!exiting)
	{		
		pAddr = 0x00000006;	//Software Rev letter
		d4 = 0x00;
		DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
		b1 = (UCHAR)d4;

		//Enable the GPIO operations (set bit E9.7, CUSTOM GPI CARD)
		pAddr = 0x000000E9;			//command input reg.
		d4 = 0x0000008F;			//enable GPIO by setting bit7
		if (b1 == 'R')	//CUSTOM GPIO12
			DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);
	}

	//continue into normal ISR operations
    							//THIS 'WHILE' LOOP IS THE ISR FOR THE 
								//PCI (DEVICE0) BOARD.  THIS EXAMPLE UPDATES A
								//COUNTER THAT SHOWS THE NUMBER OF EACH TYPE OF
								//INTERRUPT THAT HAS OCCURED.  
								//THIS CODE DOES ASSUME THAT THE BOARD IS CAPABLE OF
								//READING TIME CODE.  
								//THE ISR WILL LOOP INDEFINITLY.  TO STOP
								//THE ISR, set the "exiting" flag TRUE!
								//ALSO, CLEAR THE BIT OFFSET FE.4 BY MAKING A
								//"Write4" CALL TO OFFSET FCh WITH A VALUE OF 00h.
								//THIS WILL STOP ANY OF THE SOFTWARE INTERRUPTS FROM
								//GENERATING HARDWARE INTERRUPTS.
								//IN THIS EXAMPLE, THIS IS DONE JUST AFTER THE MAIN LOOP
	while ( ! exiting )			//BEGINS THE EXIT CODE.
    {  
//
		//Sleep(800);
		//MessageBeep(0xFFFFFFFF);

		/* ++ IRP InterruptServiceRoutine (ISR) code START
		//pAddr = 0x00000010;
		//d4 = 0x00000000;
		//POLLED method
		//IOResult = DoReadWriteAction(hAecPci, GET_INT, pAddr, &d4, &dLn);

		//ISR QUEUED  method
		//Sleep(33);	//TEMP debug
		//This call is similar to the DLL file "AEC_PCTC_ISR_1"
		pAddr = 0x00000000;	// n/a
		//This is the driver IRP that will "park and wait" for the interrupt to occur
		//and then return with the interrupt code once it happens.
		IOResult = DoReadWriteActionISR(hAecPciX, NextInt, pAddr, &d4, &dLn);
// IRP InterruptServiceRoutine (ISR) code END --*/ 		

///* ++ EVENT InterruptServiceRoutine (ISR) code START
			//1st - Write the value '00h' to offset 2Fh. This will "enable" the next interrupt when it occurs.
		// It might enable an interrupt that is already pending, so the "wait" in the following lines
		// may already be satisfied when the call to wait is made.
		pAddr = 0x0000002F;	//Enable Next "command"
		d4 = 0x00000000;
		DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);

			//2nd - Wait for EVENT. You can set this wait for INFINITE or some timeout period.
		//	This example will use a ONE second timeout to ensure the loop runs but does NOT
		//	loop through the entire loop, just allows for "exiting" checks
		do
		{
            dwRet = WaitForSingleObject(hIsrEvent, 1000);	
				//Unless nothing is enabled for harware interrupts and/or not signals are
				// active, this 'timeout' period will generally not occur.
										
				//----------
			//Did the wait time out? check for exiting is YES, then go back and wait
			if (dwRet == WAIT_TIMEOUT)
			{
				pAddr = 0x00000033;	//read from 0x33	//This is a device register
												//that is otherwise used in the comparator ops.
				d4 = 0x00;				//Normal "not allowed/not used" value
												//This program uses this register to handshake threads
												//This is a hard way to pass info to the ISR thread
				DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
				if ((d4 & 0x000000FF) == 0x000000FF)
				{
					exiting = true;	//cause loop to exit
					dwRet = 0;
				}
			}
				//---------
		} while (dwRet == WAIT_TIMEOUT);

			//3rd - Go read the "interupt code" (ie: read1 from 0Fh). This value is low byte of 'd4' on return.
		// Then continue the same as for the IRP method
		pAddr = 0x0000000F;
		d4 = 0x00000000;
		IOResult = DoReadWriteAction(hAecPci, Read1, pAddr, &d4, &dLn);

			//4th - Reset the event for next time around the loop. 
		//The device driver SETS the event. The application REsets the event.
		ResetEvent(hIsrEvent);

// EVENT InterruptServiceRoutine (ISR) code END --*/ 		



		// READ output---->
		if (IOResult)                            // Did the IOCTL succeed?
		{
			iCd = (UCHAR)d4;		//convert retruned ulong value to "code" byte
			switch (iCd)	//process the interrupt depending upon what it is
			{
			case (0x13):	//tc reader interrupt (get time & user (don't worry about Embedded)			
				bRnew = TRUE;
				iReadCnt++;				
				pAddr = 0x00000010;//Read4 from 0x10 - TIME data -(RTM)
				d4 = 0x00000000;
				//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
				if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
					lRTdat = (d4 & 0x3F7F7F3F);
				//pAddr = 0x00000014;//Read4 from 0x14 - USER data -(RUB)
				//d4 = 0x00000000;
				//if (DoReadWriteAction(hAecPci, Read4, pAddr, &d4, &dLn))
				//	lRUdat = d4;

//				bGPInew = true;
//				//lGPIdat
//				pAddr = 0x00000008;//Read4 from 0x08 - (device type + filtered GPI at 0Ah, 0Bh)
//				d4 = 0x00000000;
//				//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
//				if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
//					lGPIdat = d4;



				break;
			case (0x4C):	//tc generator interrupt
				bGnew = TRUE;
				iGenCnt++;
				pAddr = 0x00000040;//Read4 from 0x40 - Gen. TIME data -
				d4 = 0x00000000;
				if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
					lGTdat = (d4 & 0x3F7F7F3F);
//				pAddr = 0x00000044;//Read4 from 0x44 - Gen. USER data -
//				d4 = 0x00000000;
//				if (DoReadWriteAction(hAecPci, Read4, pAddr, &d4, &dLn))
//					lGUdat = d4;
				break;

			case (0x44):	//Video field int

				iVidCnt++;
				bVnew = TRUE;

				//calculate the number of video field interrupts / second ?
				// see if interrupts occur too fast or too slow - keep track?
//				QueryPerformanceCounter(&now);
//				if ( last.QuadPart )
//				{
//					DWORD n = (DWORD) ((now.QuadPart - last.QuadPart) / (frequency.QuadPart / 1000));
//					if ( n < min )
//						min = n;
//					if ( n > max )
//						max = n;	// **NEW***
//					if ( n > 17 )	// may need to change this value if not detecting
//						count += 1;	// 60 /Second video field rate interrupt.
//								//    Use '34' for 30 frame reader/gen
//								//    Use '41' for 25 frame reader/gen (PAL)
//								//    Use '21' for 50 field video rate interrupt (PAL)
//				}
//
//				last.QuadPart = now.QuadPart;
			case (0x88):
				iGpiCnt++;
				bGPInew = true;
				//lGPIdat
				pAddr = 0x00000008;//Read4 from 0x08 - (device type + filtered GPI at 0Ah, 0Bh)
				d4 = 0x00000000;
				//if (DoReadWriteAction(hAecPciInt, Read4, pAddr, &d4, &dLn))
				if (DoReadWriteAction(hAecPciX, Read4, pAddr, &d4, &dLn))
					lGPIdat = d4;
				break;

			default:	//command response, compare, serial...NAK
				iOtherCnt++;
			}
		}    
		else 
		{
			Sleep(500);
			MessageBeep(0xFFFFFFFF);
		}
			
		//----------
		pAddr = 0x00000033;	//read from 0x33	//This is a device register
										//that is otherwise used in the comparator ops.
		d4 = 0x00;				//Normal "not allowed/not used" value
										//This program uses this register to handshake threads
										//This is a hard way to pass info to the ISR thread
		DoReadWriteAction(hAecPciX, Read1, pAddr, &d4, &dLn);
		if ((d4 & 0x000000FF) == 0x000000FF)
			exiting = true;	//cause loop to exit
		//---------

	}	//end of WHILE loop

	DONEexitingIsrThread = true;

	///*++ EVENT handler shutdown START
	DoReadWriteActionEventSetup(hAecPciX, WAITEVENT, NULL, &d4, &dLn);	//pass a null handle to 'decrement' in driver	
/*++
			ae.action = WAITEVENT;
			ae.hWaitEventHandle = NULL;
			status = DeviceIoControl(		
                hAecPciX,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_EVENT,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ae,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ae),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ae,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ae),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				&dLn, //pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                );	
// --*/
	CloseHandle(hIsrEvent);
	// --*/ //EVENT handler shutdown END
	//---------
	//Disable hardware interrupts on the way out....
	pAddr = 0x000000FC;		//hardware interupt enable/disable reg.
	d4 = 0x00000000;				//'disable' value
	DoReadWriteAction(hAecPciX, Write4, pAddr, &d4, &dLn);
	pAddr = 0x000000FC;		//hardware interupt enable/disable reg.
	d4 = 0x00000000;				//'disable' value
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);

	//---------
	pAddr = 0x00000033;		//read from 0x33	//This is a device register
									//that is otherwise used in the comparator ops.
	d4 = 0x000000F0;				//Normal "not allowed/not used" value
									//This program uses this register to handshake threads
									//This is a hard way to pass info to the ISR thread
	//Set this register to a 'response' value
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);
	//-----------
	pAddr = 0x0000002F;			//command input reg.
	d4 = 0x00000000;			//enable next (normal/Non-isr mode)
	DoReadWriteAction(hAecPciX, Write1, pAddr, &d4, &dLn);
	//-----------

/*++
	if (hAecPciInt)
	{
		if (!CloseHandle(hAecPciInt))                  // Close the Device "file".
		{
	        //printf("Failed to close device.\n");
			iMB = MessageBox(hwnd, "Unable to Close the Aec PCI ISR device.",
				NULL, MB_ICONSTOP | MB_OK);
	    }
	}
--*/
    return(NO_ERROR);       // can't get here till exiting!
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	      // END OF CODE	END OF CODE		END OF CODE