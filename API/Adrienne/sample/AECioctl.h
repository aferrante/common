// AECioctl.h    Include file for AecPciTC/AecUsbTC/AecPCieTC... Driver(s)
//
//	aec?????.sys Windows NT DRIVER definitions
//	Needs to be included in source code files that call an AEC PCI-TC/PCIe-TC/USB-TC
//	device. (defined for aec????.sys driver)
//	
// Define the IOCTL codes we will use.  The IOCTL code contains a command
// identifier, plus other information about the device, the type of access
// with which the file must have been opened, and the type of buffering.
//
// In order to keep any changes made to this file consistent legacy devices
// and future devices, the following Revision has been put into place to
// aid in keeping the files aligned with most current requirments.
//
// File Revision:
//	A1	PCI-TC 
//	B1	USB-TC added
//	C1	PCIe-TC added (08/2008)
//
//

// GUID definition are required to be outside of header inclusion pragma to avoid
// error during precompiled headers.
//
//
//Class=SDHost
//Class=AecPCIeTC
//  {A0A588A4-C46F-4B37-B7EA-C82FE89870C6}	;SDcontroller, Windows "defined"!

// /*++
DEFINE_GUID(GUID_DEVCLASS_AECPCIETC,
	0xA0A588A4, 0xC46F, 0x4B37, 0xB7, 0xEA, 0xC8, 0x2F, 0xE8, 0x98, 0x70, 0xC6);



#ifndef __AecUsbTCioctl__h_
#define __AecUsbTCioctl__h_
#endif

#ifndef __AECioctl__h_
#define __AECioctl__h_
#endif

#define AecUsbTCDevice_CLASS_GUID \
 { 0x70528942, 0x4a04, 0x42d6, { 0x81, 0xfd, 0xaf, 0xa1, 0xe7, 0x19, 0xcc, 0xfe } }
// --*/

// Device type           -- in the "User Defined" range."
#define AECPCI_TYPE 40000
#define AecPCIeTC 40010
#define AEC_TYPE 40010

#define	BIT0	0x01
#define	BIT1	0x02
#define	BIT2	0x04
#define	BIT3	0x08
#define	BIT4	0x10
#define	BIT5	0x20
#define	BIT6	0x40
#define	BIT7	0x80

// ------------------------------------------------------------------------------
/*++
// Example of the enum keyword
enum Days               // Declare enum type Days
{
   saturday,            // saturday = 0 by default
   sunday = 0,          // sunday = 0 as well
   monday,              // monday = 1
   tuesday,             // tuesday = 2
   wednesday,           // etc.
   thursday,
   friday
} today;                // Variable today has type Days
--*/
//DO NOT EDIT this ENUM values! - The values used are already implemented in the
//DRIVER. Editing may cause incorrect "actions" to take place with
//unknow results.
enum AECPCI_ACTION  // ACTION for AECPCI_ACTION_ITEM "action" buffer
{                      // action naming convention:
                        //       data size
                        //     1 - byte  8 bits,   2 - word 16 bits,  4 - long 32 bits
                        //       port or memory mapped I/O from base address of device
                        //                  param1            param2
    NOP_THING,         //  No operation		0                   0	//DONT'T USE
    Read1,              //  input byte			offset				data (in)	
    Read2,              //  input word			offset				data (in)
    Read4,              //  input long			offset				data (in)
    Write1,             //  output byte			offset				data (out)
    Write2,             //  output word 		offset				data (out)
    Write4,             //  output long 		offset				data (out)


	WAITEVENT=255,		//Added 8/08 for new "EVENT" message ISR method
	R256=256,		//NOT supported YET! - (fixed offset write/read... operation)
	SND_CMD,		//send command			command(out)		intcode(in)
					//read the NEXT INTCODE - fixed OFFSET read / enable next
	GET_INT,		//get int code			0 (don't care?)		intcode(in) UCHAR
	RTM,			//  input long			0 (don't care?)		data (out)
					//Read the TIME bits at offset 10h (transition tolerent) DWORD
	RUB,			//  input long			0 (don't care?)		data (out)
					//Read the USER bits at offset 14h (transition tolerent) DWORD
	REB,			//  input byte			0 (don't care?)		data (out)
					//Read the EMBEDDED bits at offset 18h (transition tolerent) UCHAR
	NextInt,		//  output byte			0 (don't care)		data (out)
					//Queue an IRP for the ISR/DPC to complete
	GGT,			//*  input long			0 (don't care?)		data (out)
					//Read (Get Generator) TIME bits at offset 40h (uses VALIDATION) DWORD

	GGU,			//*  input long			0 (don't care?)		data (out)
					//Read (Get Generator) USER bits at offset 44h (uses VALIDATION) DWORD

	GGE,			//*  input byte			0 (don't care?)		data (out)
					//Read (Get Generator) EMBEDDED bits at offset 48h (uses VALIDATION) UCHAR
					//** GGT, GGU, GGE New for Win2K/XP driver ONLY Oct 2002
	//WAITEVENT,		//Added 8/08 for new "EVENT" message ISR method
	//-----------------------------------------------------------------------
	LAST			//nothing!
} aecACTION; 

// ---------------------------------------------------------------------------

//#define IOCTL_AEC_READ_PORT_UCHAR CTL_CODE(FILE_DEVICE_UNKNOWN, 0x900, METHOD_BUFFERED, FILE_READ_DATA)
//#define IOCTL_AEC_READ_PORT_USHORT CTL_CODE(FILE_DEVICE_UNKNOWN, 0x901, METHOD_BUFFERED, FILE_READ_DATA)
//#define IOCTL_AEC_READ_PORT_ULONG CTL_CODE(FILE_DEVICE_UNKNOWN, 0x902, METHOD_BUFFERED, FILE_READ_DATA)
//#define IOCTL_AEC_WRITE_PORT_UCHAR CTL_CODE(FILE_DEVICE_UNKNOWN, 0x910, METHOD_BUFFERED, FILE_WRITE_DATA)
//#define IOCTL_AEC_WRITE_PORT_USHORT CTL_CODE(FILE_DEVICE_UNKNOWN, 0x911, METHOD_BUFFERED, FILE_WRITE_DATA)
//#define IOCTL_AEC_WRITE_PORT_ULONG CTL_CODE(FILE_DEVICE_UNKNOWN, 0x912, METHOD_BUFFERED, FILE_WRITE_DATA)
//#define IOCTL_AECPCI_PROCESS_BUFFER CTL_CODE(FILE_DEVICE_UNKNOWN, 0xA00, METHOD_BUFFERED, FILE_ANY_ACCESS)


// The Device IOCTL function codes
/* ++
// The IOCTL function codes from 0x800 to 0xFFF are for customer use.

#define IOCTL_AEC_READ_PORT_UCHAR \
    CTL_CODE( AECPCI_TYPE, 0x900, METHOD_BUFFERED, FILE_READ_ACCESS )

#define IOCTL_AEC_READ_PORT_USHORT \
    CTL_CODE( AECPCI_TYPE, 0x901, METHOD_BUFFERED, FILE_READ_ACCESS )

#define IOCTL_AEC_READ_PORT_ULONG \
    CTL_CODE( AECPCI_TYPE, 0x902, METHOD_BUFFERED, FILE_READ_ACCESS )

#define IOCTL_AEC_WRITE_PORT_UCHAR \
    CTL_CODE(AECPCI_TYPE,  0x910, METHOD_BUFFERED, FILE_WRITE_ACCESS)

#define IOCTL_AEC_WRITE_PORT_USHORT \
    CTL_CODE(AECPCI_TYPE,  0x911, METHOD_BUFFERED, FILE_WRITE_ACCESS)

#define IOCTL_AEC_WRITE_PORT_ULONG \
    CTL_CODE(AECPCI_TYPE,  0x912, METHOD_BUFFERED, FILE_WRITE_ACCESS)

-- */
// ----------------------------------------------------------------------------


//  -- */
// ----------------------------------------------------------------------------

//typedef struct  _GENPORT_WRITE_INPUT {
//    ULONG   PortNumber;     // Port # to write to
//    union   {               // Data to be output to port
//        ULONG   LongData;
//        USHORT  ShortData;
//        UCHAR   CharData;
//    };
//}   GENPORT_WRITE_INPUT;
// ----------------------------------------------------------------------------
            // Process buffer item for input to IOCTL_AECPCI_PROCESS_BUFFER
            //                              and IOCTL_AECPCI_WAIT_ON_INTERRUPT
typedef struct _AECPCI_ACTION_ITEM
{
    //enum AECPCI_ACTION  action; // action to perform
    enum AECPCI_ACTION	action;		// action to perform
    ULONG               port;       // port address
    ULONG               value;      // input or output data
} AECPCI_ACTION_ITEM, *PAECPCI_ACTION_ITEM;

typedef struct _AECPCI_EVENT_HANDLE_ITEM
{
    enum AECPCI_ACTION  action; // action to perform
	HANDLE				hWaitEventHandle;	//handle from 'CreateEvent'.
    //ULONG               port;       // port address
    //ULONG               value;      // input or output data
} AECPCI_EVENT_HANDLE_ITEM, *PAECPCI_EVENT_HANDLE_ITEM;


        // Process AECPCI command buffer
        //   input/output:  use array of AECPCI_ACTION_ITEMs for input & output
#define IOCTL_AECPCI_PROCESS_BUFFER CTL_CODE( AEC_TYPE, 0xA00, METHOD_BUFFERED, FILE_ANY_ACCESS )
#define IOCTL_AECPCI_PROCESS_EVENT CTL_CODE( AEC_TYPE, 0xA01, METHOD_BUFFERED, FILE_ANY_ACCESS )

// ----------------------------------------------------------------------------
//+++++++++++++++++++++++++++++++++++++++++
// Name used to open device
//char *sLinkName = "\\\\.\\AecPCIe0";	//device zero
////////////////////////////////////////////////////////////////////////
// OpenByName
//		Open a handle to the requested device
/*++ Example
HANDLE OpenByName(void)
{	// Create a handle to the driver
	return CreateFile(sLinkName,	//char *sLinkName = "\\\\.\\AecPCIe0";	//device zero
			  GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
}
--*/
//	Open handle to device
//	hDev = CreateFile(_T("\\\\.\\AecPCIe0"), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, 0 );

// Shortcut macros 

        // AecPciOpenDevice - get a handle to PCI device driver
        //     In: n  - device number 0 - max
        //                  (0 opens "AecPci0" device)
        //     Out: returns - HANDLE to device or
        //              INVALID_HANDLE_VALUE if there is an error.
#define OpenAecPciExpressDevice(n)\
            CreateFile("\\\\.\\AecPCIe"#n,\
                GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,\
                NULL, OPEN_EXISTING, 0, NULL)  
#define OpenAecPciDevice(n)\
            CreateFile("\\\\.\\AecPci"#n,\
                0, FILE_SHARE_READ | FILE_SHARE_WRITE,\
                NULL, OPEN_EXISTING, 0, NULL)  

#define OpenAecUsbDevice(n)\
            CreateFile("\\\\.\\AecUsbTCDevice"#n,\
                0, FILE_SHARE_READ | FILE_SHARE_WRITE,\
                NULL, OPEN_EXISTING, 0, NULL)  

        // CloseAecPciDevice - closes the handle to device driver
        //     In:  h  - handle to device 
        //                  (returned from OpenAecPciDevice())       
        //     Out: returns - error if zero
        //                   (call GetLastError() to error code)

#define CloseAecPciDevice(h)\
            CloseHandle(h)

/*++
        // GetAecPciConfiguration - gets driver/device configuration
        //     Inputs:  h  - handle to device 
        //     Outputs: pC  - pointer to buffer
        //                  of type AECPCI_CONFIGURATION 
        //              pLen  - pointer to variable which contains
        //                  length of returned buffer (DWORD)
        //              returns - error if zero
        //                   (call GetLastError() to error code)                     
#define GetAecPciConfiguration(h,pBuff,pLen)\
            DeviceIoControl(\
                h,\
                (DWORD)IOCTL_AECPCI_PROCESS_BUFFER,\
                NULL, 0,\
                pBuff,\
                sizeof(AECPCI_CONFIGURATION),\
                pLen,\
                NULL)
--*/

        // AecIOAction - process an input/output buffer
        //     Inputs:  h  - handle to device 
        //                  (returned from OpenDevice())
        //              pBuf - pointer to i/o buffer
        //                  (array of type AECPCI_ACTION_ITEM)
        //              size  - size off i/o buf       
        //     Outputs: pLen  - pointer to variable which contains
        //                  length of returned buffer (DWORD)
        //              returns - error if zero
        //                   (call GetLastError() to error code)                     
        //      notes: pB is both an input & output
#define AecIOAction(hAecDev,pBuf,size,pLen)\
            DeviceIoControl(\
                hAecDev, (DWORD)IOCTL_AECPCI_PROCESS_BUFFER,\
                pBuf, size, pBuf, size, pLen, NULL)

#define AecIOEvent(hAecDev,pBuf,size,pLen)\
            DeviceIoControl(\
                hAecDev, (DWORD)IOCTL_AECPCI_PROCESS_EVENT,\
                pBuf, size, pBuf, size, pLen, NULL)
// ----------------------------------------------------------------------------

//#endif