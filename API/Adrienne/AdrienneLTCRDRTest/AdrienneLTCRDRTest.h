// AdrienneLTCRDRTest.h : main header file for the ADRIENNELTCRDRTEST application
//

#if !defined(AFX_ADRIENNELTCRDRTEST_H__33181D33_EC4C_4916_B712_512BA747877B__INCLUDED_)
#define AFX_ADRIENNELTCRDRTEST_H__33181D33_EC4C_4916_B712_512BA747877B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CAdrienneLTCRDRTestApp:
// See AdrienneLTCRDRTest.cpp for the implementation of this class
//

class CAdrienneLTCRDRTestApp : public CWinApp
{
public:
	CAdrienneLTCRDRTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdrienneLTCRDRTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CAdrienneLTCRDRTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADRIENNELTCRDRTEST_H__33181D33_EC4C_4916_B712_512BA747877B__INCLUDED_)
