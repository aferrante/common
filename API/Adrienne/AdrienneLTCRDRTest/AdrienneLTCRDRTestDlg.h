// AdrienneLTCRDRTestDlg.h : header file
//

#if !defined(AFX_ADRIENNELTCRDRTESTDLG_H__312F8872_EB13_4028_A286_1B5AD53DAEC4__INCLUDED_)
#define AFX_ADRIENNELTCRDRTESTDLG_H__312F8872_EB13_4028_A286_1B5AD53DAEC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../AdrienneUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CAdrienneLTCRDRTestDlg dialog

class CAdrienneLTCRDRTestDlg : public CDialog
{
// Construction
public:
	CAdrienneLTCRDRTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CAdrienneLTCRDRTestDlg)
	enum { IDD = IDD_ADRIENNELTCRDRTEST_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdrienneLTCRDRTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CAdrienneLTCRDRTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnInitMenuPopup( CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu );
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	BOOL m_bThreadKill;
	BOOL m_bThreadStarted;
	HANDLE m_hThread;
	BOOL m_bSync;

	CAdrienneUtil u;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADRIENNELTCRDRTESTDLG_H__312F8872_EB13_4028_A286_1B5AD53DAEC4__INCLUDED_)
