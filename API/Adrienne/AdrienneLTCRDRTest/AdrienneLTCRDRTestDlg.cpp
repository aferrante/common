// AdrienneLTCRDRTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AdrienneLTCRDRTest.h"
#include "AdrienneLTCRDRTestDlg.h"
#include <process.h>
#include <sys\timeb.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void DataThread(void* pvArgs);

extern CAdrienneLTCRDRTestApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdrienneLTCRDRTestDlg dialog

CAdrienneLTCRDRTestDlg::CAdrienneLTCRDRTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAdrienneLTCRDRTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAdrienneLTCRDRTestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bThreadKill=FALSE;
	m_bThreadStarted=FALSE;
	m_hThread = NULL;
	m_bSync = FALSE;
}

void CAdrienneLTCRDRTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAdrienneLTCRDRTestDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAdrienneLTCRDRTestDlg, CDialog)
	//{{AFX_MSG_MAP(CAdrienneLTCRDRTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_INITMENUPOPUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdrienneLTCRDRTestDlg message handlers

BOOL CAdrienneLTCRDRTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}

		// add two items // temp removed

//		pSysMenu->AppendMenu(MF_SEPARATOR);
//		pSysMenu->AppendMenu(MF_STRING, IDM_SYNC, "Enable system clock sync");

	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	SetWindowText(" Adrienne LTC time code reader test");
	SetWindowPos(&wndTopMost, 10, 10, 0, 0, SWP_NOSIZE);

	if(_beginthread(DataThread, 0, (void*)this) == -1)
	{
		AfxMessageBox("Could not start time retrieval thread!\r\nRestart application to retry.");
		SetWindowText(" Adrienne LTC/RDR ** ERROR **");
	}

	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAdrienneLTCRDRTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	if ((nID & 0xFFF0) == IDM_SYNC)
	{
		if(!m_bSync)
		{
			if(MessageBox("Are you sure you want to engage synchronization of the system clock to the time code source?\r\n(If the time code source is interrupted, this mode will automatically disengage.)","Confrim action",MB_YESNO)!=IDYES) return;
			m_bSync=TRUE;
			CRect rc;
			GetWindowRect(&rc);
			SetWindowPos(NULL, 10, 10, rc.Width(), 40, SWP_NOZORDER|SWP_NOMOVE);

		}
		else
		{
			m_bSync=FALSE;
			CRect rc;
			GetWindowRect(&rc);
			SetWindowPos(NULL, 10, 10, rc.Width(), 23, SWP_NOZORDER|SWP_NOMOVE);

		}
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAdrienneLTCRDRTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAdrienneLTCRDRTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CAdrienneLTCRDRTestDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	if(m_hThread)	SetThreadPriority(m_hThread, NORMAL_PRIORITY_CLASS);

	SetPriorityClass ( GetCurrentProcess(), NORMAL_PRIORITY_CLASS );
//	AfxMessageBox("Setting thread kill");
	m_bThreadKill=TRUE;
//	AfxMessageBox("Set thread kill");
	while( m_bThreadStarted )
	{
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();
		Sleep(50);
	}

	CDialog::OnCancel();
}


void DataThread(void* pvArgs)
{
	CAdrienneLTCRDRTestDlg* p = (CAdrienneLTCRDRTestDlg*)(theApp.m_pMainWnd);
	
	p->m_bThreadStarted=TRUE;
	p->m_hThread = GetCurrentThread();
	SetThreadPriority(p->m_hThread, REALTIME_PRIORITY_CLASS);

	BOOL bNewInput = TRUE;

	int nLastSleep = -1;
	int nLastValidTime = -1;
	int nValidLoops = 0;
	unsigned long ulLastTC=0xffffffff;
	int nLastTCTime = -1;
	int nNumTCRepeats = 0;

	while(!p->m_bThreadKill)
	{
		unsigned long tick = clock();
		unsigned char df =0x80; //default to DF
		if(p->u.m_hConnection == INVALID_HANDLE_VALUE)
		{
			p->SetWindowText(" Adrienne LTC/RDR: no device");
			// connect.
			p->u.ConnectToCard();

			if(p->u.m_hConnection != INVALID_HANDLE_VALUE)
			{
				p->SetWindowText(" Adrienne LTC/RDR: connected!");

				p->u.GetBoardCapabilitesCode();
				if(!(p->u.m_ucBoardCapabilitesCode & 0x10)) // no capabilities
				{
					bNewInput = TRUE;
					p->m_bSync = FALSE; // disengage
					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not installed");


					p->SetWindowText("Adrienne LTC/RDR: no LTC RDR");
					p->u.DisconnectFromCard();  // nothing else to do.
					Sleep(300);
				}
				else
				{
					p->SetWindowText(" Adrienne LTC/RDR: LTC installed");
					nLastValidTime = clock(); // to reset timing
					nLastTCTime = clock(); // to reset timing
				}
			}
		}
		else
		{
		
			//connected, so just grab status and time if status allows.
			int nBoardCheck = clock();
			p->u.GetBoardInputsAndOpModeCode();
			int nBoardEnd = clock();

//			p->u.GetBoardOpModeCode();
//			p->u.GetBoardInputsCode();
			//inputs
//If bit is set... (Register mapping)
//Bit 6 => L21 input data OK
//Bit 5 => VITC input data OK
//Bit 4 => LTC input data OK
//Bit 3 => Wide VSYNC pulses detected
//Bit 1 => ODD(1) or EVEN(0) video field
//Bit 0 => Video input OK	
			//op modes
//If bit is set... (Register mapping)
//Bit 7 => Diagnostics active
//Bit 6 => FILM(24fps) mode selected
//Bit 5 => LTC(1) or VITC(0) selected
//Bit 4 => EBU(25fps) mode selected
//Bit 3 => VITC Generator active
//Bit 2 => LTC Generator active
//Bit 1 => VITC and/or L21 Reader active
//Bit 0 => LTC Reader active	
			CString t; 
			CString g; 

			if(p->u.m_ucBoardOpModeCode&0x20)
			{
				if(p->u.m_ucBoardInputsCode&0x10)
				{
					if(bNewInput)
					{
						df = p->u.GetTimeCodeBits();  //get actual DF
						bNewInput = FALSE;
					}

/*
1) Read and save the status of the VALIDATION byte at 1Ah.
2) Go back to step #1 if bit 7 is high (update in progress).
3) Quickly read and save all of the time, user, and embedded bits which are required by your application. Do not process yet.
4) Read the VALIDATION byte again, then compare it with the value read in step #1.
5) Go back to step #1 if the VALIDATION byte has changed.
6) If desired, read the input signal status byte at 0Ch to make sure that the time code data you just read is current.
7) Process and/or display the time code data as desired. Note that if you attempt to display the time bits as they are read, on many PC�s the
   next time code field or frame will appear before you have finished displaying the previous one. The results will not be pleasant, hence our recommendations.
8) Update the rest of your program operations.
9) Go back to step #1 (NOT STEP #3!).
*/
					int nValidCheck = 0;
					int nValidStart = clock();
					int nValidEnd = 0;
					int nTimeEnd = 0;

					unsigned char vc = p->u.GetValidationCode();
					while((!p->m_bThreadKill)&&(vc&0x80))
					{
						Sleep(1);
						vc = p->u.GetValidationCode();
						nValidCheck++;
					}
					nValidEnd = clock();
					unsigned long TC = p->u.GetTimeCode();
					nTimeEnd = clock();

					if((!p->m_bThreadKill)&&(vc == p->u.GetValidationCode()))
					{
						if(TC != 0xffffffff)
						{
							int nValidTime = clock() - nLastValidTime;
							if(nValidTime > 34) // >1 frame, hardcode for now.  means we skipped a frame
							{
								FILE* fp;
								fp = fopen("LTCRDR.txt","ab");
								if(fp)
								{
									_timeb timestamp;
									_ftime(&timestamp);
									char logtmbuf[48]; // need 33;
									tm* theTime = localtime( &timestamp.time	);
									strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

									fprintf(fp, "%s%03d Delayed update: %d ms since last update; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
										nValidTime,
										nBoardCheck, nBoardEnd,
										nValidStart,
										nValidEnd,
										nValidCheck,
										nTimeEnd,
										nValidLoops,
										nLastSleep
										);

									fclose(fp);
								}
							}
							
							int nTCTime = nTimeEnd - nLastTCTime;
							if(ulLastTC != TC) // time address has changed
							{

								if(nTCTime > 3*16) // 3 16ms timeslices at most, sampling error for one frame with 16 ms tolerance
								{
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d Delayed register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}

								}

								if((TC<0x00000010)||(TC>0x23595915)) // log around midnight
								{
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d CHECK register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}

								}


								nLastTCTime = nTimeEnd;
								ulLastTC = TC;
								nNumTCRepeats = 0;
							}
							else
							{
								nNumTCRepeats++;
							}

							nValidLoops=0;
							nLastValidTime = nTimeEnd; // just reset

							t.Format(" Adrienne LTC/RDR: %02x:%02x:%02x%s%02x  op:0x%02x in:0x%02x",
								(unsigned char)((TC&0xff000000)>>24),
								(unsigned char)((TC&0x00ff0000)>>16),
								(unsigned char)((TC&0x0000ff00)>>8),  df&0x80?";":":",
								(unsigned char)(TC&0x000000ff),
								p->u.m_ucBoardOpModeCode,
								p->u.m_ucBoardInputsCode
								);


							if(p->m_bSync)
							{
								int nHours = (unsigned char)((TC&0xff000000)>>24); 
								int nMinutes = (unsigned char)((TC&0x00ff0000)>>16); 
								int nSeconds = (unsigned char)((TC&0x0000ff00)>>8); 
								int nFrames = (unsigned char)(TC&0x000000ff);

								// going to assume NTSC df for this.
						 
								int nNumFrames = 
										((nHours/16)*10		+ (nHours%16)) * 60 * 60 * 30 + 
										((nMinutes/16)*10 + (nMinutes%16)) * 60 * 30 + 
										((nSeconds/16)*10 + (nSeconds%16)) * 30 + 
										((nFrames/16)*10	+ (nFrames%16));

								int nNumMinutes = ((nHours/16)*10		+ (nHours%16)) * 60 + ((nMinutes/16)*10 + (nMinutes%16));

								int nDF = ( nNumMinutes - ( nNumMinutes / 10 ) - 1 ) * 2; // the - 1 is for the zeroth minute not dropped.

								int nMilliseconds = (int)( ((double)( nNumFrames - nDF )) * ( 1001.0 / 30.0 ) + 0.5 );   //0.5 is rounding
						

								// need to to a back conversion to system time.  not good accuracy because only frame accurate, not ms, but that's as close as we can get.
								nHours = (nMilliseconds/3600000L)%24; // mod 24 to deal with the 3 frames after DF second midnight and before real midnight
								nMinutes = ((nMilliseconds/60000L)%60);
								nSeconds = (((nMilliseconds/1000L)%60)%60);
								nFrames = (nMilliseconds%1000L); // milliseconds now not frames.

								if((nMilliseconds/3600000L) != 24) // dont mess with the sys time just around midnight.
								{
									SYSTEMTIME SystemTime;
									GetSystemTime(&SystemTime);
									SystemTime.wHour = (unsigned short)nHours;
									SystemTime.wMinute = (unsigned short)nMinutes;
									SystemTime.wSecond = (unsigned short)nSeconds;
									SystemTime.wMilliseconds = (unsigned short)nFrames;
//								SetSystemTime(&SystemTime);
								}
								p->GetDlgItem(IDC_STATIC_TEXT)->GetWindowText(g);
								CString Q; Q.Format("  Milliseconds %08d   %02d:%02d:%02d.%03d", nMilliseconds, nHours, nMinutes, nSeconds, nFrames);
								if(g.Compare(Q))	p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText(Q);

							}

						}
						else
						{
							t.Format(" Adrienne LTC/RDR: --:--:--%s--  op:0x%02x in:0x%02x %d",
									df&0x80?";":":",
									p->u.m_ucBoardOpModeCode,
									p->u.m_ucBoardInputsCode, clock()  // added clock so disply does something
								);

							FILE* fp;
							fp = fopen("LTCRDR.txt","ab");
							if(fp)
							{
								_timeb timestamp;
								_ftime(&timestamp);
								char logtmbuf[48]; // need 33;
								tm* theTime = localtime( &timestamp.time	);
								strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

								fprintf(fp, "%s%03d TC ERROR %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
									
									nTimeEnd - nLastValidTime, nNumTCRepeats,
									nBoardCheck, nBoardEnd,
									nValidStart,
									nValidEnd,
									nValidCheck,
									nTimeEnd,
									nValidLoops,
									nLastSleep
									);

								fclose(fp);
							}

						}
						p->GetWindowText(g);
						if(g.Compare(t))	p->SetWindowText(t);			// compare makes it less blinky
					}
					else
					{
						nValidLoops++;
						// p->SetWindowText("Adrienne LTC/RDR: no LTC input"); // just leave it at last update
					}

				}
				else
				{
					bNewInput = TRUE;
					p->m_bSync = FALSE; // disengage
					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  Lost input");

					p->GetWindowText(g);
					t.Format(" Adrienne LTC/RDR: no LTC input op:0x%02x in:0x%02x", p->u.m_ucBoardOpModeCode, p->u.m_ucBoardInputsCode);
					if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
				}
			}
			else
			{
				bNewInput = TRUE;
				p->m_bSync = FALSE; // disengage
				p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not selected");

				p->GetWindowText(g);
				t.Format(" Adrienne LTC/RDR: LTC not selected op:0x%02x cap:0x%02x", p->u.m_ucBoardOpModeCode, p->u.m_ucBoardCapabilitesCode);
				if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
			}
		}

/*
		tick = 10 - (clock() - tick);

		if(tick<10)  // this should allow 3 attempts per frame (time address).
		{
			nLastSleep = tick;
			Sleep(tick);
		}
		else
		{
			Sleep(0);
			nLastSleep = 0;
		}
*/
		Sleep(1) ;// 33 times per frame, possibly
		nLastSleep = 1;
	}
	//		AfxMessageBox("exiting");


	p->SetWindowText(" Disconnecting"); Sleep(1);
	p->u.DisconnectFromCard();

	p->SetWindowText(" Disconnected");
	p->m_bThreadStarted=FALSE;

}

void CAdrienneLTCRDRTestDlg::OnInitMenuPopup( CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu )
{
	if(!bSysMenu) return;
	// TODO: Add your message handler code here
//AfxMessageBox("here");
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
//AfxMessageBox("here 2");
		pSysMenu->EnableMenuItem(IDM_SYNC, MF_BYCOMMAND|MF_ENABLED);
		if(m_bSync)
		{
			pSysMenu->ModifyMenu(IDM_SYNC, MF_STRING|MF_BYCOMMAND, IDM_SYNC,  "Disable system clock sync");
		}
		else
		{
			if(!((u.m_ucBoardInputsCode&0x10)&&(u.m_ucBoardOpModeCode&0x20)&&(u.m_hConnection != INVALID_HANDLE_VALUE)))
			{
//				AfxMessageBox("Grey");
				pSysMenu->ModifyMenu(IDM_SYNC, MF_STRING|MF_BYCOMMAND|MF_GRAYED, IDM_SYNC,  "Enable system clock sync");
//				pSysMenu->EnableMenuItem(IDM_SYNC, MF_BYCOMMAND|MF_GRAYED);
			}
			else
			{
				pSysMenu->ModifyMenu(IDM_SYNC, MF_STRING|MF_BYCOMMAND, IDM_SYNC,  "Enable system clock sync");
			}

		}
	}
}
