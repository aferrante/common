// AdrienneUtil.h: interface for the CSCTE104Util class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADRLTCRDRUTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_)
#define AFX_ADRLTCRDRUTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef NULL
#define NULL 0
#endif

class CAdrienneUtil  
{
public:
	CAdrienneUtil();
	virtual ~CAdrienneUtil();

	// make sure the thread priority is set to critical for to call time code functions:
	//		SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL);

	HANDLE ConnectToCard();
	int DisconnectFromCard();
	unsigned char GetBoardCapabilitesCode();
	unsigned char GetBoardInputsCode();
	unsigned char GetBoardOpModeCode();
	unsigned long GetTimeCode();
	unsigned char GetTimeCodeBits();
	unsigned char GetValidationCode();
	unsigned short GetBoardInputsAndOpModeCode();
	
	BOOL	DoReadWriteAction(HANDLE, enum AECPCI_ACTION, ULONG, PULONG, PULONG);
	BOOL	DoReadWriteActionISR(HANDLE, enum AECPCI_ACTION, ULONG, PULONG, PULONG);
	BOOL	DoReadWriteActionEventSetup(HANDLE, enum AECPCI_ACTION, HANDLE, PULONG, PULONG);
	int		LtoStrTm(ULONG, char*);
	int		LtoStr12B(ULONG, char*);

	char* m_pchLinkName;
	char* m_pchLinkNameE;	//'AEC' device zero
	
	HANDLE m_hConnection;

	unsigned char m_ucBoardCapabilitesCode;
	unsigned char m_ucBoardInputsCode;
	unsigned char m_ucBoardOpModeCode;
};

#endif // !defined(AFX_ADRLTCRDRUTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_)
