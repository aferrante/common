// AdrienneUtil.cpp: implementation of the CSCTE104Util class and other support classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  //temp if need AfxMessageBox
#include <winioctl.h>
#include "AECioctl.h"
#include "Time.h"
#include "AdrienneUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define		NONE	0x0000
#define		VLTC	0x0050
#define		LTC		0x0098
#define		VITC	0x0093

#define     PCI_DEVICE_NAME     "AecPci"	//BASE device Name - Hard coded in the DRIVER

//////////////////////////////////////////////////////////////////////
// CAdrienneUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAdrienneUtil::CAdrienneUtil()
{
	m_pchLinkName = "\\\\.\\AecPci0";
	m_pchLinkNameE = "\\\\.\\AecPCIe0";	//'AEC' device zero

	m_hConnection = INVALID_HANDLE_VALUE;
	m_ucBoardCapabilitesCode = 0x00;
	m_ucBoardInputsCode = 0x00;
	m_ucBoardOpModeCode = 0x00;
}

CAdrienneUtil::~CAdrienneUtil()
{
	if(	m_hConnection != INVALID_HANDLE_VALUE ) DisconnectFromCard();
}

HANDLE CAdrienneUtil::ConnectToCard()
{
	HANDLE h;

	//Try to open PCIe-TC first
	h = CreateFile(
              m_pchLinkNameE,	//  "\\\\.\\AecPCIe0",       // Open the Device "file"
              GENERIC_READ | GENERIC_WRITE,
              FILE_SHARE_READ | FILE_SHARE_WRITE,	//NULL (do not share if COM device)
              NULL,
              OPEN_EXISTING,
              0,		//FILE_FLAG_OVERLAPPED, ????????
              NULL
              );
             
  if (h == INVALID_HANDLE_VALUE)        // Was the device opened?
  {
//=========
	//if NOT PCIe-TC, then try PCI-TC...
		h = CreateFile(
								m_pchLinkName,	//  "\\\\.\\AecPci0",       // Open the Device "file"
								GENERIC_READ | GENERIC_WRITE,
								FILE_SHARE_READ | FILE_SHARE_WRITE,	//NULL (do not share if COM device)
								NULL,
								OPEN_EXISTING,
								0,		//FILE_FLAG_OVERLAPPED, ????????
								NULL
								);
	}
	if (h != INVALID_HANDLE_VALUE)        // Was the device opened?
	{
//--------------------------------
		// This is important!! Make this program have highest Priority
		//		or interrupts will not be processed quickly enough
		SetPriorityClass ( GetCurrentProcess(), REALTIME_PRIORITY_CLASS );

	}

	m_hConnection = h;

	return h;
}

int CAdrienneUtil::DisconnectFromCard()
{
	if(m_hConnection != INVALID_HANDLE_VALUE)
	{
		BOOL r = CloseHandle(m_hConnection);
		m_hConnection = INVALID_HANDLE_VALUE;
		m_ucBoardCapabilitesCode = 0x00;
		m_ucBoardInputsCode = 0x00;
		m_ucBoardOpModeCode = 0x00;

		if(!r)	return -1; // just to indicate it failed.
	}
	return 0;
}


//-----------------------------------------------------------
/*++	****** DEFINED ACTIONS SUPPORTED BY THE DRIVER ******
AECPCI_ACTION_ITEM parameter must be one of the following!
NOTE: The values ARE case sensitive!

    NOP_THING,         //  No operation		0                   0
    Read1,              //  input byte			offset				data (in)	
    Read2,              //  input word			offset				data (in)
    Read4,              //  input long			offset				data (in)
    Write1,             //  output byte			offset				data (out)
    Write2,             //  output word 		offset				data (out)
    Write4,             //  output long 		offset				data (out)

	SND_CMD,		//send command			command(out)		intcode(in)
					//read the NEXT INTCODE - fixed OFFSET read / enable next
	GET_INT			//get int code			0 (don't care?)		intcode(in)
					//
	RTM,			//  input long			0 (don't care?)		data (out)
					//Read the TIME bits at offset 10h (transition tolerent) DWORD
	RUB,			//  input long			0 (don't care?)		data (out)
					//Read the USER bits at offset 14h (transition tolerent) DWORD
	REB,			//  input byte			0 (don't care?)		data (out)
					//Read the EMBEDDED bits at offset 18h (transition tolerent) UCHAR
	NextInt,		//  output byte			0 (don't care)		data (out)
					//Queue an IRP for the ISR/DPC to complete

				--*/

BOOL	CAdrienneUtil::DoReadWriteAction(
					IN HANDLE hOpen, 
				  IN enum AECPCI_ACTION act, 
				  IN ULONG port,
				  IN OUT PULONG pdata,
				  IN PULONG pLB
					)
{
	BOOL status;
		//enum AECPCI_ACTION	action;		// action to perform
		//ULONG					port;       // port address
		//ULONG					value;      // input or output data
	AECPCI_ACTION_ITEM		ai;			// *PAECPCI_ACTION_ITEM;
	//IOCTL_AECPCI_PROCESS_BUFFER
	//ULONG   LengthBack;

	ai.action = act;
	ai.port = port;
	ai.value = *pdata;

	//Nonzero indicates success. 
	//Zero indicates failure. 
	//To get extended error information, call GetLastError. 

	//status = TRUE;		//TEMP debug
	//*pdata = 0x13;		//TEMP debug

	if (status = DeviceIoControl(		
                hOpen,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_BUFFER,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ai,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ai),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ai,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ai),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                )				
		)
	{		*pdata = ai.value;		}

	return status;

}

BOOL	CAdrienneUtil::DoReadWriteActionISR(
				  IN HANDLE hOpen, 
				  IN enum AECPCI_ACTION act, 
				  IN ULONG port,
				  IN OUT PULONG pdata,
				  IN PULONG pLB
				  )

{
	BOOL status;
		//enum AECPCI_ACTION	action;		// action to perform
		//ULONG					port;       // port address
		//ULONG					value;      // input or output data
	AECPCI_ACTION_ITEM		ai;			// *PAECPCI_ACTION_ITEM;
	//IOCTL_AECPCI_PROCESS_BUFFER
	//ULONG   LengthBack;

	ai.action = act;
	ai.port = port;
	ai.value = *pdata;

	//Nonzero indicates success. 
	//Zero indicates failure. 
	//To get extended error information, call GetLastError. 

	//status = TRUE;		//TEMP debug
	//*pdata = 0x13;		//TEMP debug

	if (status = DeviceIoControl(		
                hOpen,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_BUFFER,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ai,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ai),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ai,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ai),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                )				
		)
	{		*pdata = ai.value;		}

	return status;
}

BOOL	CAdrienneUtil::DoReadWriteActionEventSetup(
					IN HANDLE hOpen, 
				  IN enum AECPCI_ACTION act, 
				  IN HANDLE hEvent,
				  IN OUT PULONG pdata,
				  IN PULONG pLB
					)
{
	BOOL status;
		//enum AECPCI_ACTION	action;		// action to perform
		//ULONG					port;       // port address
		//ULONG					value;      // input or output data
	AECPCI_EVENT_HANDLE_ITEM		ae;			// *PAECPCI_ACTION_ITEM;
	//IOCTL_AECPCI_PROCESS_BUFFER
	//ULONG   LengthBack;

	ae.action = act;
	ae.hWaitEventHandle = hEvent;

	//Nonzero indicates success. 
	//Zero indicates failure. 
	//To get extended error information, call GetLastError. 

	//status = TRUE;		//TEMP debug
	//*pdata = 0x13;		//TEMP debug

	//if (
		//If the operation fails or is pending, the return value is zero. 
		//To get extended error information, call GetLastError.
		status = DeviceIoControl(		
                hOpen,			//1 Handle to device
                IOCTL_AECPCI_PROCESS_EVENT,//2 IO Control code -- (Read)/ (Write)
								//pointer to buffer to supply input data
				&ae,			//3++ Buffer to driver.  Holds port & data (if any).
								//size of input buffer
				sizeof(ae),		//4+++ Length of buffer in bytes.
								//pointer to buffer to receive output data
                &ae,			//5++++ Buffer from driver.
								//size of output buffer
                sizeof(ae),		//6+++++ Length of buffer in bytes. 1/2/4 (char/word/dword)
								//pointer to variable to receive output byte count
				pLB,			//7 Bytes placed in DataBufferOut. (store # upon return)
                NULL			//8 NULL means wait till op. completes.
                );				
	//	)
	//{		*pdata = ae.value;		}

	return status;
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//-----------------------------------------------------------
//Convert a LONG (4byte) value into a character string of TIME (Tm)
//This does NOT mask embedded bits 
// use "dw = (dw & 0x3F7F7F3F);" to mask embedded bits IF needed
//before calling this routine.
//------------------------------------------------------------
int		CAdrienneUtil::LtoStrTm(ULONG dw, char* chs)
{
	wsprintf (chs,"%8.8X",dw);
	chs[11] = 0;	//EOL
	chs[10] = chs[7];
	chs[9] = chs[6];
	chs[8] = ':';
	chs[7] = chs[5];
	chs[6] = chs[4];
	chs[5] = ':';
	chs[4] = chs[3];
	chs[3] = chs[2];
	chs[2] = ':';
	return 11;
}

//-----------------------------------------------------------
//Convert a LONG (4byte) value into a 12 0/1 character string
// use "dw = (dw & 0x3F7F7F3F);" to mask embedded bits IF needed
//before calling this routine.
//------------------------------------------------------------
int		CAdrienneUtil::LtoStr12B(ULONG dw, char* chs)
{
	ULONG d;
	d = (dw & 0x0FFF0000);	//mask all but the 12 GPI bits
	//d = d / 0x10000;		//shift down 16 bits
	//wsprintf (chs,"%8.8X",dw);
	//12
	if (d & 0x08000000)
		chs[0] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[0] = '0';
	//11
	if (d & 0x04000000)
		chs[1] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[1] = '0';
	//10
	if (d & 0x02000000)
		chs[2] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[2] = '0';
	//9
	if (d & 0x01000000)
		chs[3] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[3] = '0';

	chs[4] = ' ';	//add a space after each nibble

	//8
	if (d & 0x00800000)
		chs[5] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[5] = '0';
	//7
	if (d & 0x00400000)
		chs[6] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[6] = '0';
	//6
	if (d & 0x00200000)
		chs[7] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[7] = '0';
	//5
	if (d & 0x00100000)
		chs[8] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[8] = '0';

	chs[9] = ' ';	//add a space after each nibble

	//4
	if (d & 0x00080000)
		chs[10] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[10] = '0';
	//3
	if (d & 0x00040000)
		chs[11] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[11] = '0';
	//2
	if (d & 0x00020000)
		chs[12] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[12] = '0';
	//1
	if (d & 0x00010000)
		chs[13] = 'X'; //'X' when SET, '0' when CLEAR
	else chs[13] = '0';

	chs[14] = 0; //EOL
	return 14;

}

unsigned char CAdrienneUtil::GetBoardCapabilitesCode()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
		m_ucBoardCapabilitesCode = 0x00;
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x00000008;		//Read1 from 0x08	
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read1, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
			m_ucBoardCapabilitesCode = 0x00;
		}
		else
		{
			m_ucBoardCapabilitesCode = (unsigned char)data4;
		}
//If bit is set... (Register mapping)
//bit 7=Vitc	Generator	installed
//bit 6=Vitc	Reader		installed
//bit 5=LTC		Generator	installed
//bit 4=LTC		Reader		installed
//bit ?=OSD					installed
//bit 1=Serial				installed
	}
	return m_ucBoardCapabilitesCode;
}

unsigned char CAdrienneUtil::GetBoardInputsCode()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
		m_ucBoardInputsCode = 0x00;
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x0000000c;		//Read1 from 0x0c
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read1, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
			m_ucBoardInputsCode = 0x00;
		}
		else
		{
			m_ucBoardInputsCode = (unsigned char)data4;
		}
//If bit is set... (Register mapping)
//Bit 6 => L21 input data OK
//Bit 5 => VITC input data OK
//Bit 4 => LTC input data OK
//Bit 3 => Wide VSYNC pulses detected
//Bit 1 => ODD(1) or EVEN(0) video field
//Bit 0 => Video input OK	
	}
	return m_ucBoardInputsCode;
}


unsigned char CAdrienneUtil::GetBoardOpModeCode()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
		m_ucBoardOpModeCode = 0x00;
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x0000000d;		//Read1 from 0x0d
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read1, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
			m_ucBoardOpModeCode = 0x00;
		}
		else
		{
			m_ucBoardOpModeCode = (unsigned char)data4;
		}
//If bit is set... (Register mapping)
//Bit 7 => Diagnostics active
//Bit 6 => FILM(24fps) mode selected
//Bit 5 => LTC(1) or VITC(0) selected
//Bit 4 => EBU(25fps) mode selected
//Bit 3 => VITC Generator active
//Bit 2 => LTC Generator active
//Bit 1 => VITC and/or L21 Reader active
//Bit 0 => LTC Reader active	
		}
	return m_ucBoardOpModeCode;
}

unsigned short CAdrienneUtil::GetBoardInputsAndOpModeCode()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
		m_ucBoardInputsCode = 0x00;
		m_ucBoardOpModeCode = 0x00;
		return 0;
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x0000000c;		//Read1 from 0x0c
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read2, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
			m_ucBoardInputsCode = 0x00;
			m_ucBoardOpModeCode = 0x00;
		}
		else
		{
			m_ucBoardOpModeCode = (unsigned char)(data4>>8);
			m_ucBoardInputsCode = (unsigned char)data4;
		}
//If bit is set... (Register mapping)
//Bit 6 => L21 input data OK
//Bit 5 => VITC input data OK
//Bit 4 => LTC input data OK
//Bit 3 => Wide VSYNC pulses detected
//Bit 1 => ODD(1) or EVEN(0) video field
//Bit 0 => Video input OK	

//If bit is set... (Register mapping)
//Bit 7 => Diagnostics active
//Bit 6 => FILM(24fps) mode selected
//Bit 5 => LTC(1) or VITC(0) selected
//Bit 4 => EBU(25fps) mode selected
//Bit 3 => VITC Generator active
//Bit 2 => LTC Generator active
//Bit 1 => VITC and/or L21 Reader active
//Bit 0 => LTC Reader active	}
		return (unsigned short)data4;
	}

}



unsigned long CAdrienneUtil::GetTimeCode()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
//		AfxMessageBox("invalid");
		return 0xffffffff;  // invalid!
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x00000010;		//Read1 from 0x10
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read4, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
//			AfxMessageBox("failed");
			return 0xffffffff;  // invalid!
		}
		else
		{
			return data4;
		}
	}
//10h 00h RO Selected reader time bits frames.
//11h 00h RO Selected reader time bits seconds.
//12h 00h RO Selected reader time bits minutes.
//13h 00h RO Selected reader time bits hours.
//(all data is in packed BCD format)	}
}

unsigned char CAdrienneUtil::GetTimeCodeBits()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
//		AfxMessageBox("invalid");
		return 0xff;  // invalid!
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x00000018;		//Read1 from 0x18
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read1, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
//			AfxMessageBox("failed");
			return 0xff;  // invalid!
		}
		else
		{
			return (unsigned char) data4;
		}
	}
//18h 00h RO Selected reader embedded bits:
//Bit 7 => LTC bit 10 (drop frame),
//VITC bit 14 (drop frame)
//Bit 6 => LTC bit 11 (color framed),
//VITC bit 15 (color framed)
//Bit 3 => LTC bit 27 (unassigned),
//VITC bit 35 (NTSC field ID)
//Bit 2 => LTC bit 59 (unassigned),
//VITC bit 75 (PAL field ID)
//Bit 1 => LTC bit 58 (unassigned),
//VITC bit 74 (unassigned)
//Bit 0 => LTC bit 43 (unassigned),
//VITC bit 55 (unassigned)
}


unsigned char CAdrienneUtil::GetValidationCode()
{
	if(m_hConnection == INVALID_HANDLE_VALUE)
	{
		return 0xff;  // invalid!
	}
	else
	{
		ULONG data4, len, portaddress;

		portaddress = 0x0000001a;		//Read1 from 0x1a
		data4 = 0x00000000;
		BOOL IoctlResult = DoReadWriteAction(m_hConnection, Read1, portaddress, &data4, &len);
		if (!IoctlResult)    //error  // Did the IOCTL fail?
		{
			return 0xff;  // invalid!
		}
		else
		{
			return (unsigned char)data4;
		}
	}

}
