// SNMPTest.h : main header file for the SNMPTEST application
//

#if !defined(AFX_SNMPTEST_H__EBDE3D6F_9965_44C3_BC3E_19CE3BCF2D79__INCLUDED_)
#define AFX_SNMPTEST_H__EBDE3D6F_9965_44C3_BC3E_19CE3BCF2D79__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#define II_TYPE_DNF 0
#define II_TYPE_LYR 1
#define II_TYPE_ICON 2
#define II_TYPE_G7 3

/////////////////////////////////////////////////////////////////////////////
// CSNMPTestApp:
// See SNMPTest.cpp for the implementation of this class
//

class CSNMPTestApp : public CWinApp
{
public:
	CSNMPTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSNMPTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSNMPTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SNMPTEST_H__EBDE3D6F_9965_44C3_BC3E_19CE3BCF2D79__INCLUDED_)
