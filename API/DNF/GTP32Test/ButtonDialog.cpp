// ButtonDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SNMPTest.h"
#include "ButtonDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CButtonDialog dialog


CButtonDialog::CButtonDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CButtonDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CButtonDialog)
	m_szEventLabel = _T("");
	m_szValue = _T("");
	m_nInvertState = 0;
	m_nOverrideIndexValue = 0;
	m_bOverrideIndex = FALSE;
	//}}AFX_DATA_INIT
	m_szTitle = _T("Button Setup");
}


void CButtonDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CButtonDialog)
	DDX_Text(pDX, IDC_EDIT1, m_szEventLabel);
	DDX_Text(pDX, IDC_EDIT2, m_szValue);
	DDX_Radio(pDX, IDC_RADIO1, m_nInvertState);
	DDX_Text(pDX, IDC_EDIT3, m_nOverrideIndexValue);
	DDV_MinMaxInt(pDX, m_nOverrideIndexValue, 0, 86);
	DDX_Check(pDX, IDC_CHECK1, m_bOverrideIndex);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CButtonDialog, CDialog)
	//{{AFX_MSG_MAP(CButtonDialog)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CButtonDialog message handlers

void CButtonDialog::OnOK() 
{
	// TODO: Add extra validation here

	if(((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck()==FALSE)
	{
		m_nOverrideIndexValue=0;
		UpdateData(FALSE);
	}


	if(UpdateData(TRUE)==FALSE) return;
	
	CDialog::OnOK();
}


BOOL CButtonDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	SetWindowText(m_szTitle);

	if(m_nOverrideIndexValue<1)
	{
		m_bOverrideIndex = FALSE;
		GetDlgItem(IDC_EDIT3)->EnableWindow(FALSE);
		m_nOverrideIndexValue = 0;
	}
	else
	{
		m_bOverrideIndex = TRUE;
		GetDlgItem(IDC_EDIT3)->EnableWindow(TRUE);
	}

	if(m_nOverrideIndexValue>86)
	{
		m_nOverrideIndexValue=86;  // upper limit, its a char anyway
	}
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CButtonDialog::OnCheck1() 
{
	// TODO: Add your control notification handler code here
//	if(UpdateData(TRUE)==FALSE) return;
	GetDlgItem(IDC_EDIT3)->EnableWindow(((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck());
}
