// SNMPTestDlg.h : header file
//

#if !defined(AFX_SNMPTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
#define AFX_SNMPTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//#include "../IS2Core.h"
#include "..\..\..\MFC\ODBC\DBUtil.h"
#include "..\..\..\LAN\NetUtil.h"
#include "..\..\..\TXT\FileUtil.h"
#include "..\..\..\MFC\ColorButton\ColorButton.h"
/////////////////////////////////////////////////////////////////////////////
// CSNMPTestDlg dialog

#define LOGFILENAME "gtp32-tx.log"

/*

class CColorBGButton: public CButton
{
public:
//	CColorBGButton();
//	virtual ~CColorBGButton();
	BOOL OnEraseBkgnd(CDC* pDC);
};
* /
class CColorButton : public CButton
{
public:
  CColorButton();
  CColorButton(COLORREF color);
  virtual ~CColorButton();
  afx_msg HBRUSH CtlColor (CDC* pDC, UINT nCtlColor);

protected:
  COLORREF m_Color;
  CBrush m_BackBrush;

  DECLARE_MESSAGE_MAP()

  DECLARE_DYNAMIC(CColorButton)
};
*/


typedef struct ButtonEvent_t
{
	char event[128];
	char value[64];
	bool bInvert;
	char index;
} ButtonEvent_t;

typedef struct MessageLogging_t
{
	char* pchReadableHexSnd;
	unsigned long ulReadableHexSnd;
	char* pchReadableSnd;
	unsigned long ulReadableSnd;
	char* pchReadableHexRcv;
	unsigned long ulReadableHexRcv;
	char* pchReadableRcv;
	unsigned long ulReadableRcv;

} MessageLogging_t;





class CSNMPTestDlg : public CDialog
{
// Construction
public:
	CSNMPTestDlg(CWnd* pParent = NULL);	// standard constructor
	int LoadFields();
	void OnMonitorButtonRecv();

	BOOL m_bMonitorSocket;
	BOOL m_bSocketMonitorStarted;

	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[5];
	CSize m_sizeDlgMin;

	CString m_szQueueTableName;
	CString m_szLibrettoFileLocation;

	CString m_szCmd;
	void* m_pvMessage;

	unsigned char* m_pucLastResponse;

	int m_nClock;
	SOCKET m_s;
	BOOL m_bInCommand;

	BOOL m_bStates;
	BOOL m_bSupressEditChange;

	int m_nCmdClock;

	BOOL m_bForceKeepAlive;
	BOOL m_bFormatKeepAlive;

	BOOL m_bTallyState[16];
	CColorButton m_button[16];
	ButtonEvent_t m_buttonevent[16];
	//CColorButton* m_pButton1;
	int		m_nButtonPressDelay;


	CColorButton m_buttonTest;

	BOOL m_bInPrettyLightsButton;

	sockaddr_in m_saiGTPhost;



// Dialog Data
	//{{AFX_DATA(CSNMPTestDlg)
	enum { IDD = IDD_SNMPTEST_DIALOG };
	CListCtrl	m_lcResponses;
	CString	m_szHost;
	CString	m_szPort;
	CString	m_szDSN;
	CString	m_szPw;
	CString	m_szUser;
	int		m_nRecvTimeout;
	int		m_nSendTimeout;
	int		m_nFlavor;
	BOOL	m_bLogTransactions;
	CString	m_szCmdName;
	BOOL	m_bAutoKeepAlive;
	int		m_nHeartbeatInterval;
	CString	m_szCommunity;
	BOOL	m_bPrettyLights;
	BOOL	m_Do_GPO;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSNMPTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

public:
	CDBUtil m_db;
	CNetUtil m_net;

	void SetupFlavor();

	int TruncateText(CString* pszText, int nLimit=32);
	int FormatTime(CString* pszText);
	void PressButton(int nNum);
	void SetButtonText();
	void StartServer();
	void StopServer();

	void ButtonPush(WPARAM wParam, LPARAM lParam);
	void ButtonRelease(WPARAM wParam, LPARAM lParam);

	void DoButtons();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSNMPTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonSend();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnButtonConnect2();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonRecv();
	virtual void OnOK();
	afx_msg void OnRadioDnf();
	afx_msg void OnRadioIcon();
	afx_msg void OnRadioLyr();
	afx_msg void OnButtonViewlog();
	afx_msg void OnRadioG7();
	afx_msg void OnCloseupComboCommand();
	afx_msg void OnCheckKeepAlive();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSelchangeComboCommand();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton7();
	afx_msg void OnButton8();
	afx_msg void OnButton9();
	afx_msg void OnButton10();
	afx_msg void OnButton11();
	afx_msg void OnButton12();
	afx_msg void OnButton13();
	afx_msg void OnButton14();
	afx_msg void OnButton15();
	afx_msg void OnButton16();
	afx_msg void OnClickList3(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkList3(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonSetup();
	afx_msg void OnCheckLogxact();
	afx_msg void OnItemchangedList3(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCheckPrettylights();
	afx_msg void OnCheckDoGpo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SNMPTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
