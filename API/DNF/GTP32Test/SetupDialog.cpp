// SetupDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SNMPTest.h"
#include "SetupDialog.h"
#include "ButtonDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetupDialog dialog


CSetupDialog::CSetupDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSetupDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetupDialog)
	m_nButtonPressDelay = 250;
	//}}AFX_DATA_INIT
}


void CSetupDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetupDialog)
	DDX_Control(pDX, IDC_LIST1, m_lc);
	DDX_Text(pDX, IDC_EDIT1, m_nButtonPressDelay);
	DDV_MinMaxInt(pDX, m_nButtonPressDelay, 0, 10000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetupDialog, CDialog)
	//{{AFX_MSG_MAP(CSetupDialog)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetupDialog message handlers

void CSetupDialog::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	int nCount = m_lc.GetItemCount();
	int nItem = m_lc.GetNextItem( -1,  LVNI_SELECTED );

	CString szLabel;
	CString szValue;
	if((nItem>=0)&&(nItem<nCount))
	{
//    m_list.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
 //   m_list.SetItemState(nItem, INDEXTOSTATEIMAGEMASK(2), LVIS_STATEIMAGEMASK);

		szLabel = m_lc.GetItemText(nItem, 0);

		CButtonDialog dlg;

		int i=15;
		while(i>=0)
		{
			szValue.Format("Button %d", i+1);
			int nLen = szValue.GetLength();

			if(szValue.CompareNoCase(szLabel.Left(nLen))==0)
			{
				dlg.m_szTitle = szValue;
				break;
			}
			i--;
		}

		if(i>=0)
		{
			dlg.m_szEventLabel = m_buttonevent[i].event;
			dlg.m_szValue = m_buttonevent[i].value;
			dlg.m_nInvertState = m_buttonevent[i].bInvert?1:0;
			dlg.m_nOverrideIndexValue = (int)m_buttonevent[i].index;

			if(dlg.DoModal() == IDOK)
			{
				strcpy(m_buttonevent[i].event, dlg.m_szEventLabel.GetBuffer(0));
				strcpy(m_buttonevent[i].value, dlg.m_szValue.GetBuffer(0));
				m_buttonevent[i].bInvert =  (dlg.m_nInvertState==0)?false:true;

				if(dlg.m_bOverrideIndex)
				{
					m_buttonevent[i].index = dlg.m_nOverrideIndexValue;
				}
				else
				{
					m_buttonevent[i].index = 0;
				}

				// change text in list.  confusing otherwise.
				if((dlg.m_bOverrideIndex)&&(m_buttonevent[i].index>0))
				{
					szValue.Format("Button %d (%d)", i+1, (int)m_buttonevent[i].index);
				}
				else
				{
					szValue.Format("Button %d", i+1);
				}

				m_lc.SetItemText(nItem, 0, szValue);
				
//				AfxMessageBox(dlg.m_szValue);
//				AfxMessageBox(m_buttonevent[i].value);
				m_lc.SetItemText(nItem, 1,	m_buttonevent[i].event);
				m_lc.SetItemText(nItem, 2,	m_buttonevent[i].value);
				m_lc.SetItemText(nItem, 3,	m_buttonevent[i].bInvert?"Invert":"Match");
				m_lc.Invalidate();
			}
		}

	}
	
	*pResult = 0;
}

BOOL CSetupDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CRect rc; 
	ListView_SetExtendedListViewStyle(m_lc.m_hWnd, LVS_EX_FULLROWSELECT);

	m_lc.GetWindowRect(&rc);
	m_lc.InsertColumn(0, "Button", LVCFMT_LEFT, (rc.Width()-20)*19/64, 0 );
	m_lc.InsertColumn(1, "Label", LVCFMT_LEFT, (rc.Width()-20)*15/32, 1 );
	m_lc.InsertColumn(2, "Value", LVCFMT_LEFT,(rc.Width()-20)*3/32, 2 );
	m_lc.InsertColumn(3, "Action", LVCFMT_LEFT,(rc.Width()-20)*3/16, 3 );

	int i=0;
	while(i<16)
	{
		CString s;
		if(m_buttonevent[i].index>0)
		{
			s.Format("Button %d (%d)", i+1, m_buttonevent[i].index);
		}
		else
		{
			s.Format("Button %d", i+1);
		}
		m_lc.InsertItem(i, s);
		m_lc.SetItemText(i,1,	m_buttonevent[i].event);
		m_lc.SetItemText(i,2,	m_buttonevent[i].value);
		m_lc.SetItemText(i,3,	m_buttonevent[i].bInvert?"Invert":"Match");


		i++;
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
