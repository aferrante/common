// SNMPTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "..\..\..\SNMP\SNMPUtil.h"
#include "SNMPTest.h"
#include "SNMPTestDlg.h"
#include <process.h>
#include "..\..\..\MSG\Messager.h"
#include "..\..\..\TXT\BufferUtil.h"
#include <direct.h>
#include "SetupDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void SocketMonitorThread(void* pvArgs);
void USP16ServerHandlerThread(void* pvArgs);


CSNMPUtil u;  //global just so the message number is global
CBufferUtil  gbu;

#define LISTBOXMAX 500

/*
IMPLEMENT_DYNAMIC(CColorButton, CButton)

CColorButton::CColorButton()
{
    m_Color = ::GetSysColor(COLOR_WINDOW);
    m_BackBrush.CreateSolidBrush(m_Color);
}

CColorButton::CColorButton(COLORREF color) :
    m_Color(color),
    m_BackBrush(color)
{}

CColorButton::~CColorButton()
{}


BEGIN_MESSAGE_MAP(CColorButton, CButton)
    ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

HBRUSH CColorButton::CtlColor(CDC* pDC, UINT nCtlColor)
{
    return (HBRUSH)m_BackBrush;
}


/*
BOOL CColorBGButton::OnEraseBkgnd(CDC* pDC)
{
	CRect rc;
	GetClientRect(&rc);
	pDC->FillSolidRect(rc, RGB(0,255,0));
	return TRUE;
}
/*
CColorBGButton::CColorBGButton()
{
}
CColorBGButton::~CColorBGButton()
{
}
*/
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSNMPTestDlg dialog

CSNMPTestDlg::CSNMPTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSNMPTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSNMPTestDlg)
	m_szHost = _T("127.0.0.1");
	m_szPort = _T("161");
	m_szDSN = _T("Libretto");
	m_szPw = _T("");
	m_szUser = _T("sa");
	m_nRecvTimeout = 5000;
	m_nSendTimeout = 5000;
	m_nFlavor = 0;
	m_bLogTransactions = FALSE;
	m_szCmdName = _T("");
	m_bAutoKeepAlive = FALSE;
	m_nHeartbeatInterval = 5000;
	m_szCommunity = _T("public");
	m_bPrettyLights = FALSE;
	m_Do_GPO = FALSE;
	//}}AFX_DATA_INIT

	m_bInCommand = FALSE;
	m_szQueueTableName =  _T("Queue");
	m_szLibrettoFileLocation =  _T("libretto.csf");

	m_nCmdClock = -1;
	m_pvMessage = NULL;
	m_pucLastResponse = NULL;

	m_nButtonPressDelay = 250;

	//��#
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_s = NULL;
	m_net.Initialize();
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_sizeDlgMin = CSize(488,609);
	
	m_bMonitorSocket=FALSE;
	m_bSocketMonitorStarted=FALSE;
	m_bStates = FALSE;

	m_bSupressEditChange = FALSE;

	m_bForceKeepAlive = FALSE;
	m_bFormatKeepAlive = TRUE;

	m_bInPrettyLightsButton = FALSE;

	memset(((char *)&m_saiGTPhost), 0, (sizeof (m_saiGTPhost)));

//	m_pButton1 = NULL;
}

void CSNMPTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSNMPTestDlg)
	DDX_Control(pDX, IDC_LIST3, m_lcResponses);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_CBString(pDX, IDC_COMBO_PORT, m_szPort);
	DDX_Text(pDX, IDC_EDIT_DSN, m_szDSN);
	DDX_Text(pDX, IDC_EDIT_PW, m_szPw);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_RECVTM, m_nRecvTimeout);
	DDX_Text(pDX, IDC_EDIT_SENDTM, m_nSendTimeout);
	DDX_Radio(pDX, IDC_RADIO_DNF, m_nFlavor);
	DDX_Check(pDX, IDC_CHECK_LOGXACT, m_bLogTransactions);
	DDX_CBString(pDX, IDC_COMBO_COMMAND, m_szCmdName);
	DDX_Check(pDX, IDC_CHECK_KEEP_ALIVE, m_bAutoKeepAlive);
	DDX_Text(pDX, IDC_EDIT_INTERVAL, m_nHeartbeatInterval);
	DDX_Text(pDX, IDC_EDIT_COMMUNITY, m_szCommunity);
	DDX_Check(pDX, IDC_CHECK_PRETTYLIGHTS, m_bPrettyLights);
	DDX_Check(pDX, IDC_CHECK_DO_GPO, m_Do_GPO);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSNMPTestDlg, CDialog)
	//{{AFX_MSG_MAP(CSNMPTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT2, OnButtonConnect2)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_RECV, OnButtonRecv)
	ON_BN_CLICKED(IDC_RADIO_DNF, OnRadioDnf)
	ON_BN_CLICKED(IDC_RADIO_ICON, OnRadioIcon)
	ON_BN_CLICKED(IDC_RADIO_LYR, OnRadioLyr)
	ON_BN_CLICKED(IDC_BUTTON_VIEWLOG, OnButtonViewlog)
	ON_BN_CLICKED(IDC_RADIO_G7, OnRadioG7)
	ON_CBN_CLOSEUP(IDC_COMBO_COMMAND, OnCloseupComboCommand)
	ON_BN_CLICKED(IDC_CHECK_KEEP_ALIVE, OnCheckKeepAlive)
	ON_WM_TIMER()
	ON_CBN_SELCHANGE(IDC_COMBO_COMMAND, OnSelchangeComboCommand)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_BUTTON8, OnButton8)
	ON_BN_CLICKED(IDC_BUTTON9, OnButton9)
	ON_BN_CLICKED(IDC_BUTTON10, OnButton10)
	ON_BN_CLICKED(IDC_BUTTON11, OnButton11)
	ON_BN_CLICKED(IDC_BUTTON12, OnButton12)
	ON_BN_CLICKED(IDC_BUTTON13, OnButton13)
	ON_BN_CLICKED(IDC_BUTTON14, OnButton14)
	ON_BN_CLICKED(IDC_BUTTON15, OnButton15)
	ON_BN_CLICKED(IDC_BUTTON16, OnButton16)
	ON_NOTIFY(NM_CLICK, IDC_LIST3, OnClickList3)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST3, OnDblclkList3)
	ON_BN_CLICKED(IDC_BUTTON_SETUP, OnButtonSetup)
	ON_BN_CLICKED(IDC_CHECK_LOGXACT, OnCheckLogxact)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST3, OnItemchangedList3)
	ON_BN_CLICKED(IDC_CHECK_PRETTYLIGHTS, OnCheckPrettylights)
	ON_BN_CLICKED(IDC_CHECK_DO_GPO, OnCheckDoGpo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSNMPTestDlg message handlers

BOOL CSNMPTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	srand ( time(NULL) );

	CRect rc; 

//	m_button.SubclassDlgItem(IDC_BUTTON1, this);

//	m_pButton1 = new CColorButton(RGB(0,255,0));

//	m_pButton1->SubclassDlgItem(IDC_BUTTON1, this);

	m_button[0].Attach(IDC_BUTTON1, this); 
	m_button[1].Attach(IDC_BUTTON2, this); 
	m_button[2].Attach(IDC_BUTTON3, this); 
	m_button[3].Attach(IDC_BUTTON4, this); 
	m_button[4].Attach(IDC_BUTTON5, this); 
	m_button[5].Attach(IDC_BUTTON6, this); 
	m_button[6].Attach(IDC_BUTTON7, this); 
	m_button[7].Attach(IDC_BUTTON8, this); 
	m_button[8].Attach(IDC_BUTTON9, this); 
	m_button[9].Attach(IDC_BUTTON10, this); 
	m_button[10].Attach(IDC_BUTTON11, this); 
	m_button[11].Attach(IDC_BUTTON12, this); 
	m_button[12].Attach(IDC_BUTTON13, this); 
	m_button[13].Attach(IDC_BUTTON14, this); 
	m_button[14].Attach(IDC_BUTTON15, this); 
	m_button[15].Attach(IDC_BUTTON16, this); 

	m_buttonTest.Attach(IDC_BUTTON17, this);


	CString szLabel;
	int i=0;
	while(i<16)
	{
		szLabel.Format("GPO_%02d_MAN_EVT", i+1);
		strcpy(m_buttonevent[i].event, szLabel.GetBuffer(0));
		strcpy(m_buttonevent[i].value, "1");
		m_buttonevent[i].bInvert = false;
		m_buttonevent[i].index = 0;  // just means do not override

		// overrides
		if (i==0) strcpy(m_buttonevent[i].event, "GPO_01_AUTO");
		else if (i==1)
		{
			strcpy(m_buttonevent[i].event, "GPO_03_BLOCK");
			m_buttonevent[i].bInvert = true;
		}
		else if (i==2) strcpy(m_buttonevent[i].event, "GPO_03_BLOCK");
//		else if (i==3) strcpy(m_buttonevent[i].event, "GPO_04_MAN_EVT");
		else if (i==6) strcpy(m_buttonevent[i].event, "GPO_07_ALARM");
		else if (i==7) strcpy(m_buttonevent[i].event, "GPO_08_ALT_AUTO");

		i++;
	}

	m_lcResponses.GetWindowRect(&rc);
	m_lcResponses.InsertColumn(0, "-", LVCFMT_LEFT, 16, 0 );
	m_lcResponses.InsertColumn(1, "Command", LVCFMT_LEFT, (rc.Width()-40)/2, 1 );
	m_lcResponses.InsertColumn(2, "Response", LVCFMT_LEFT,(rc.Width()-40)/2, 2 );

	ListView_SetExtendedListViewStyle(m_lcResponses.m_hWnd, LVS_EX_FULLROWSELECT);
	//set up the buttons.

	GetDlgItem(IDC_STATIC_BUTTONFRAME)->GetWindowRect(&rc);
	ScreenToClient(rc);

//	AfxMessageBox("here");
	int t = rc.top;
	int l = rc.left;
	int w = (rc.Width() -1 -(3*2))/4;
	int h = (rc.Height()-1 -(3*2))/4;


	// four rows of four
	GetDlgItem(IDC_BUTTON1)->SetWindowPos( 
			&wndTop, 
			l, 
			t,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON2)->SetWindowPos( 
			&wndTop, 
			l+w+2, 
			t,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON3)->SetWindowPos( 
			&wndTop, 
			l+w+w+4, 
			t,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON4)->SetWindowPos( 
			&wndTop, 
			l+w+w+w+6, 
			t,
			w,
			h, 
			SWP_NOZORDER
			);

	GetDlgItem(IDC_BUTTON5)->SetWindowPos( 
			&wndTop, 
			l, 
			t+h+2,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON6)->SetWindowPos( 
			&wndTop, 
			l+w+2, 
			t+h+2,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON7)->SetWindowPos( 
			&wndTop, 
			l+w+w+4, 
			t+h+2,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON8)->SetWindowPos( 
			&wndTop, 
			l+w+w+w+6, 
			t+h+2,
			w,
			h, 
			SWP_NOZORDER
			);


	GetDlgItem(IDC_BUTTON9)->SetWindowPos( 
			&wndTop, 
			l, 
			t+h+h+4,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON10)->SetWindowPos( 
			&wndTop, 
			l+w+2, 
			t+h+h+4,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON11)->SetWindowPos( 
			&wndTop, 
			l+w+w+4, 
			t+h+h+4,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON12)->SetWindowPos( 
			&wndTop, 
			l+w+w+w+6, 
			t+h+h+4,
			w,
			h, 
			SWP_NOZORDER
			);

	GetDlgItem(IDC_BUTTON13)->SetWindowPos( 
			&wndTop, 
			l, 
			t+h+h+h+6,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON14)->SetWindowPos( 
			&wndTop, 
			l+w+2, 
			t+h+h+h+6,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON15)->SetWindowPos( 
			&wndTop, 
			l+w+w+4, 
			t+h+h+h+6,
			w,
			h, 
			SWP_NOZORDER
			);
	GetDlgItem(IDC_BUTTON16)->SetWindowPos( 
			&wndTop, 
			l+w+w+w+6, 
			t+h+h+h+6,
			w,
			h, 
			SWP_NOZORDER
			);


	GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON4)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON5)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON6)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON7)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON8)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON9)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON10)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON11)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON12)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON13)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON14)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON15)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON16)->EnableWindow(FALSE);



	int b=0;
	while(b<16)
	{
		m_bTallyState[b]=FALSE;
		b++;
	}
	//AfxMessageBox("X1");

	// TODO: Add extra initialization here
	FILE* fp = fopen("gtp32host.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;int nSelP=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				int q=0;
				if(*pch=='*')
				{
					nSel = i; q=1; 
					pch++;
				}
				if(strlen(pch))
				{
				
					char* pchDel = strchr(pch, ':');
					if(pchDel)
					{
						*pchDel = 0; pchDel++;
					}

					if(strlen(pch)) {((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch); i++;}
				
					CComboBox* ptr = ((CComboBox*)GetDlgItem(IDC_COMBO_PORT));

					if((pchDel)&&(strlen(pchDel)))
					{
						int s = ptr->FindStringExact( -1, pchDel );
						if(s==CB_ERR)
						{
							ptr->AddString(pchDel);
						}
						if(q==1)
						{
							nSelP=ptr->FindStringExact( -1, pchDel );
						}
					}
					else
					{
						int s = ptr->FindStringExact( -1, "161" );
						if(s==CB_ERR)
						{
							ptr->AddString("161");
						}
						if(q==1)
						{
							nSelP=ptr->FindStringExact( -1, "161" );
						}
					}
				}

				pch = strtok(NULL, "\r\n");
				
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				if (nSelP>=0)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SetCurSel(nSelP);
				}
				else
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SetCurSel(0);
				}


				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}
//	AfxMessageBox("X2");
//	AfxMessageBox("here");
	
	SetupFlavor();
//	AfxMessageBox("X3");
//	AfxMessageBox("here");
	SetButtonText();

	SetTimer(666, 10, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSNMPTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSNMPTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSNMPTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSNMPTestDlg::OnButtonConnect() 
{
	CWaitCursor c;
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		m_bMonitorSocket=FALSE;
		while(m_bSocketMonitorStarted) Sleep(10);

/*
		GetDlgItem(IDC_BUTTON_CONNECT2)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
*/
		m_net.CloseConnection(m_s);
		StopServer();
		m_s = NULL;
		m_nCmdClock = -1;
		DoButtons(); // do send recv and key buttons AFTER m_s is set
	}
	else
	{
		UpdateData(TRUE);
//		char conn[256];
//		sprintf(conn, "%s", m_szHost);  // was m_szIP
//		AfxMessageBox(m_szHost);

		if((m_szHost.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
			((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
		}
		if((m_szPort.GetLength()>0)&&(((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->FindStringExact( -1, m_szPort )==CB_ERR))
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->AddString(m_szPort);
			((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->SelectString(-1, m_szPort);
		}
		char szError[8096];
		
		if(m_nSendTimeout<=0)
		{
			m_nSendTimeout=5000;
			GetDlgItem(IDC_EDIT_SENDTM)->SetWindowText("5000");
		}
		if(m_nRecvTimeout<=0)
		{
			m_nRecvTimeout=5000;
			GetDlgItem(IDC_EDIT_RECVTM)->SetWindowText("5000");
		}

		if(m_net.OpenConnection(m_szHost.GetBuffer(0), (short)(atol(m_szPort)), 
			&m_s, 
			m_nSendTimeout,
			m_nRecvTimeout,
			szError,
			AF_INET,SOCK_DGRAM,IPPROTO_UDP  // UDP for SNMP
			
			)<NET_SUCCESS)
		{
			m_s = NULL;
			AfxMessageBox(szError);
		}
		else
		{
			hostent* h=NULL;

			h = gethostbyname(m_szHost);
			if(h!=NULL)
			{
				memcpy(&m_saiGTPhost.sin_addr, h->h_addr, 4);
/*
				CString foo; foo.Format("Got %d.%d.%d.%d for %s", 
					m_saiGTPhost.sin_addr.s_net, 
					m_saiGTPhost.sin_addr.s_host, 
					m_saiGTPhost.sin_addr.s_lh, 
					m_saiGTPhost.sin_addr.s_impno,
					m_szHost);
				AfxMessageBox(foo);
*/
			}



			DoButtons(); // do send recv and key buttons AFTER m_s is set
			GetDlgItem(IDC_BUTTON_CONNECT2)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);

			m_bMonitorSocket=FALSE;
			while(m_bSocketMonitorStarted) Sleep(10);

			m_bMonitorSocket=TRUE;
			_beginthread(SocketMonitorThread, 0, (void*)this);

			StartServer();

		}
	}
}

void CSNMPTestDlg::OnButtonSend() 
{
	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
	{
		UpdateData(TRUE);
		char szError[8096];
		char szSQL[DB_SQLSTRING_MAXLEN];
		int nItem = clock();

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username, event_itemid) \
VALUES ('%s','', 256, '%s', 0, 'sys', %d)", //HARDCODE
			m_szQueueTableName.GetBuffer(0),
			m_szCmd.GetBuffer(0),
			m_szHost.GetBuffer(0),
			nItem
		);
//AfxMessageBox(szSQL);


		int nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
		if(nReturn<DB_SUCCESS)
		{
			CString foo;
			foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
			AfxMessageBox(foo);
		}
		else
		{
			//recv the response.
			Sleep(250);

			char szSQL[DB_SQLSTRING_MAXLEN];
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action=128 and event_itemid = %d", //HARDCODE
							m_szQueueTableName.GetBuffer(0),
							nItem);
			int nIndex = 0;
			while(nIndex<=0)
			{
				Sleep(100);
//AfxMessageBox(szSQL);
				CRecordset* prs = m_db.Retrieve(m_db.m_ppdbConn[0], szSQL, szError);
				if(prs)
				{
		//			while ((!prs->IsEOF()))
					if (!prs->IsEOF())  // just do the one record, if there is one
					{
						try
						{
							CString szMessage;
							prs->GetFieldValue("remote", szMessage);//HARDCODE
//AfxMessageBox(szMessage);
							int nCount = m_lcResponses.GetItemCount();
							m_lcResponses.InsertItem(nCount, m_szCmd);
							m_lcResponses.SetItemText(nCount, 1, szMessage.GetBuffer(0) );
							m_lcResponses.EnsureVisible(nCount, FALSE);
							while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest

							m_nCmdClock = clock();
							nIndex++;
						}
						catch(CException* e)// CDBException *e, CMemoryException *m)  
						{
AfxMessageBox("exception");
							e->Delete();
						} 
						catch( ... )
						{
AfxMessageBox("exception2");
						}

						prs->MoveNext();
					}

					prs->Close();

					delete prs;
				}
			}
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE event_itemid = %d", //HARDCODE
				m_szQueueTableName.GetBuffer(0),
				nItem);

//AfxMessageBox(szSQL);
			nReturn=	m_db.ExecuteSQL(m_db.m_ppdbConn[0], szSQL, szError);
			if(nReturn<DB_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);
			}
		
		}
	}
	else
	if(m_s)
	{
		if(m_bInCommand) return; // have to exit, busy already
		m_bInCommand = TRUE;
		UpdateData(TRUE);
		char szError[8096];
		int nReturn;
		if(m_nSendTimeout<=0)
		{
			m_nSendTimeout=5000;
			GetDlgItem(IDC_EDIT_SENDTM)->SetWindowText("5000");
		}


		if(m_pvMessage)
		{
			unsigned char* buffer = NULL;
			unsigned long buflen=0;
			unsigned char ucMsg = u.IncrementMessageNumber();

			((CSNMPMessage*)m_pvMessage)->m_ulRequest = (unsigned long)ucMsg;

			buffer = u.ReturnMessageBuffer((CSNMPMessage*)m_pvMessage);
			if(buffer)
			{	
				buflen = u.ReturnLength(&buffer[1]);
				buflen += u.ReturnLength(buflen)+1;
			}
			
//			CBufferUtil bu;
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

unsigned long ulSend = buflen;
char* pchSnd = gbu.ReadableHex((char*)buffer, &ulSend, MODE_FULLHEX);

		m_nClock = clock();
		
		nReturn=	m_net.SendLine(buffer, buflen, m_s, EOLN_NONE, false, m_nSendTimeout, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString foo;
			foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
			AfxMessageBox(foo);
		}
		else
		{


if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);



				fprintf(logfp, "%s%03d Sent %d bytes: ", logtmbuf, timestamp.millitm, buflen);

				if(pchSnd)
				{
					fwrite(pchSnd, sizeof(char), ulSend, logfp);
//					free(pchSnd);
				}
				else
				{
					fwrite(buffer, sizeof(char), buflen, logfp);
				}
				fprintf(logfp, "%c%c", 13, 10);
				fclose(logfp);
			}
}

			GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			m_nClock = clock();
			//recv the response.
	//		unsigned char* puc2 = NULL;
	//		unsigned long ulLen2=0
			unsigned char* puc = NULL;
			unsigned long ulLen=0;
	//		Sleep(500);
			nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
			if(nReturn<NET_SUCCESS)
			{
				CString foo;
				foo.Format("Error sending command, return code %d\r\n%s", nReturn,szError);
				AfxMessageBox(foo);

				if(m_pucLastResponse) delete m_pucLastResponse;
				m_pucLastResponse=NULL;

				// let's disconnect it though.

/*
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
				m_net.CloseConnection(m_s);
				StopServer();
				m_s = NULL;
				m_nCmdClock = -1;

				DoButtons(); // set after setting m_s

				AfxMessageBox("disconnected");



			}
			else
			{

//CString Q;
//Q.Format("buflen %d, puc = %08x, %d", ulLen, puc, nReturn);
//AfxMessageBox(Q);
//AfxMessageBox(szError);


//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

/*
				//check if more data:
				bool bMore=false;

				timeval tv;
				tv.tv_sec = 0; 
				tv.tv_usec = 500000;  // timeout value
				int nNumSockets;
				fd_set fds;
				FD_ZERO(&fds);  // Zero this out each time
				FD_SET(m_s, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);
				if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
				{
					
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(m_s, &fds)))
					) 
				{ 

				} 
				else // there is recv data.
				{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
					//recv the response.
					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, char* szError);
					if(nReturn<NET_SUCCESS)
					{
						CString foo;
						foo.Format("Error sending command, return code %d\r\n%s", nReturn, szError);
						AfxMessageBox(foo);
					}
					else
					{
						if(ulLen==0) //disconnected
						{
							GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
							GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
							GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
							m_net.CloseConnection(m_s);
							m_s = NULL;
							AfxMessageBox("disconnected");
						}
						else
							bMore=true;
					}
				}
				if(bMore)
				{// use the second set to parse
				}
*/


				if(ulLen==0) //disconnected
				{
/*
					GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
					GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
					GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
					GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
					m_net.CloseConnection(m_s);
					StopServer();
					m_s = NULL;
					m_nCmdClock = -1;
				
					DoButtons(); // set after setting m_s

					AfxMessageBox("disconnected");
				}
				else
				{
					if(puc)
					{
						if(m_pucLastResponse) delete m_pucLastResponse;
						m_pucLastResponse=puc;


unsigned long ulRecv = ulLen;
char* pchRecv = gbu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, ulLen);
				
				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
				}
				else
				{
					fwrite((char*)puc, sizeof(char), ulLen, logfp);
				}

				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();

						m_lcResponses.InsertItem(nCount, "->");
/*
						if(pchSnd)
						{
							m_lcResponses.InsertItem(nCount, pchSnd);
						}
						else
						{
							m_lcResponses.InsertItem(nCount, (char*)buffer);
						}

						
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}

*/
						if(pchSnd)
						{
							m_lcResponses.SetItemText(nCount, 1, pchSnd);
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)buffer);
						}
						
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 2, (char*)pchRecv );
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 2, (char*)puc );
						}
						m_lcResponses.EnsureVisible(nCount, FALSE);
						while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
							

						// let's make the command timer just react on keep_alive only. or init, since init will force the keep alive

				//		if(( nCommand == 0x0001 )||( nCommand == 0x0003 ))  // just do it all the time.
						{
							m_nCmdClock = clock();
						}


if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}


/*
			if( nCommand == 0x0001 )
			{
				if(m_bAutoKeepAlive) m_bForceKeepAlive=TRUE;
//				AfxMessageBox("TRUE");
			}
*/

				//		free(puc); // dont free it!
						if(pchRecv) free(pchRecv);


					}
					else
					{
						AfxMessageBox("valid repsonse not received");
					}
				}
			}

		}
		m_bInCommand = FALSE;
		if(pchSnd) free(pchSnd);

		if(buffer) free(buffer);
		}
		else
		{
			AfxMessageBox("Error: no message object");
		}

	}
	else
	{
/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
				CString Q; Q.Format("POPUP!");
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(fp, "%s%03d %s%c%c", 
					logtmbuf, timestamp.millitm, 
					Q, 13, 10);
				fclose(fp);
			}
*/
		AfxMessageBox("no connection");
/*
 		  fp = fopen( "C:\\msglog.txt", "ab");

			if(fp)
			{
				CString Q; Q.Format("POPUP! down");
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(fp, "%s%03d %s%c%c", 
					logtmbuf, timestamp.millitm, 
					Q, 13, 10);
				fclose(fp);
			}
*/
	}	
}

void CSNMPTestDlg::OnDestroy() 
{

	CDialog::OnDestroy();
	
}

void CSNMPTestDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	KillTimer(666);
	
	FILE* fp = fopen("gtp32host.cfg", "wb");
	if(fp)
	{
		UpdateData(TRUE);
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		int nCountP=((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetCount();
		int nSelP=((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s:%s\n", m_szHost, m_szPort);
		}
		if(nCount>0)
		{
			int i=0;  int j=0;
			while(i<max(nCount, nCountP))
			{
				CString szText, szPort;
				if(i<nCount)((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				else szText="";

				if(nSel==i)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetLBText( nSelP, szPort );
					fprintf(fp, "*");
					fprintf(fp, "%s:%s\n", szText, szPort);
				}
				else
				{
					if(nSelP==j)
					{
						j++;
					}
					if(j<nCountP)
					{
						((CComboBox*)GetDlgItem(IDC_COMBO_PORT))->GetLBText( j, szPort );
					}
					else szPort="";
					fprintf(fp, "%s:%s\n", szText, szPort);
				}
				j++;
				i++;
			}
		}
		fclose(fp);
	}



	CDialog::OnCancel();
}




void CSNMPTestDlg::OnButtonConnect2() 
{
	// TODO: Add your control notification handler code here
	if(m_s)
	{
		if(AfxMessageBox("The Channel Box must not be connected directly, in order to send commands to Libretto.\r\nDisconnect and continue?", MB_YESNO)!=IDYES) return;
		OnButtonConnect();
	}

	if((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0))
	{
		m_db.DisconnectDatabase(m_db.m_ppdbConn[0]);
		GetDlgItem(IDC_BUTTON_CONNECT2)->SetWindowText("Connect to Libretto");
		GetDlgItem(IDC_EDIT_DSN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_USER)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PW)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		m_db.RemoveConnection(m_db.m_ppdbConn[0]);
	}
	else
	{
		UpdateData(TRUE);
		char szError[8096];


		// Lets get the latest csf.
		CFileUtil file;
		// get settings.
//AfxMessageBox(m_szLibrettoFileLocation);

		int rv = file.GetSettings(m_szLibrettoFileLocation.GetBuffer(0), false);


		while(!(rv&FILEUTIL_FILE_EXISTS))
		{
			CFileDialog fdlg(TRUE,
											 "csf",
											 "Libretto.csf",
												OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_LONGNAMES|OFN_NOCHANGEDIR|OFN_PATHMUSTEXIST );

			if(fdlg.DoModal()==IDOK)
			{
				m_szLibrettoFileLocation = fdlg.GetPathName();
				AfxGetApp()->WriteProfileString("Libretto","CSFFilePath", m_szLibrettoFileLocation);
			

				rv = file.GetSettings(m_szLibrettoFileLocation.GetBuffer(0), false);

				if(!(rv&FILEUTIL_FILE_EXISTS))
				{
					if(AfxMessageBox("The setting could not be extracted.\r\nDo you wish to select another file?", MB_YESNO)!=IDYES) break;
				}
			}
			else //cancelled
				return;

		}

		if(rv&FILEUTIL_FILE_EXISTS)
		{
			m_szQueueTableName = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue");  // the CommandQueue table name
//AfxMessageBox("got from csf");
		}
		else 
		{
			return;
		}


//AfxMessageBox(m_szQueueTableName);


		CDBconn* pdb = m_db.CreateNewConnection(m_szDSN.GetBuffer(1),m_szUser.GetBuffer(1),m_szPw.GetBuffer(1));
		if((pdb)&&(m_db.ConnectDatabase(pdb,szError))>=NET_SUCCESS)
		{
//			AfxMessageBox("Connected!");

			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DSN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_USER)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_PW)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_CONNECT2)->SetWindowText("Disconnect Libretto");
			GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);
		}
		else
		{
			AfxMessageBox(szError);
			if(pdb)
			{
				m_db.DisconnectDatabase(pdb);
				m_db.RemoveConnection(pdb);
			}
		}
	}
}

void CSNMPTestDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
//sizing stuff
//	AfxMessageBox("OSW1");
	if(m_bNewSizeInit)
	{
		int nCtrlID;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<5;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST3;break;
			case 1: nCtrlID=IDC_EDIT_COMMAND;break;
			case 2: nCtrlID=IDC_BUTTON_SEND;break;
			case 3: nCtrlID=IDC_BUTTON_RECV;break;
			case 4: nCtrlID=IDC_STATIC_TIMEOUT;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void CSNMPTestDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_EDIT_COMMAND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[1].Width()-dx,  // goes with right edge
			m_rcCtrl[1].Height(), 
			SWP_NOZORDER|SWP_NOMOVE
			);

		pWnd=GetDlgItem(IDC_STATIC_TIMEOUT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[4].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[4].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_BUTTON_SEND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[2].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[2].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_BUTTON_RECV);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[3].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[3].top,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_LIST3);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			m_rcCtrl[0].Width()-dx,  // goes with right edge
			m_rcCtrl[0].Height()-dy,  // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		int col1w= m_lcResponses.GetColumnWidth(1);
		m_lcResponses.SetColumnWidth(1, (m_rcCtrl[0].Width()-dx-40-col1w) ); 


		for (int i=0;i<5;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST3;break;
			case 1: nCtrlID=IDC_EDIT_COMMAND;break;
			case 2: nCtrlID=IDC_BUTTON_SEND;break;
			case 3: nCtrlID=IDC_BUTTON_RECV;break;
			case 4: nCtrlID=IDC_STATIC_TIMEOUT;break;
			}
			
			GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
}

void CSNMPTestDlg::SetupFlavor() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	switch(m_nFlavor)
	{
	case II_TYPE_LYR:
		{
		} break;
	case II_TYPE_G7:
		{		
		} break;
	case II_TYPE_ICON:
		{
		} break;

	case II_TYPE_DNF:
	default:
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->ResetContent();
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("keypress"); // is a set_request
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->AddString("heartbeat");
			((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, "heartbeat");
//			OnCloseupComboCommand();
			OnSelchangeComboCommand();

//			GetDlgItem(IDC_COMBO_COMMAND)->EnableWindow(TRUE);
//			GetDlgItem(IDC_COMBO_COMMAND)->ShowWindow(SW_SHOW);

		} break;
	}
}



void CSNMPTestDlg::OnMonitorButtonRecv()
{
	if(m_bInCommand) return;
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
/*
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
		m_net.CloseConnection(m_s);
		StopServer();
		m_s = NULL;
		m_nCmdClock = -1;
		DoButtons(); // set after setting m_s
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
//		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
/*
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
			m_nCmdClock = -1;
			m_net.CloseConnection(m_s);
			StopServer();
			m_s = NULL;
			DoButtons(); // set after setting m_s
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
/*
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
				
				m_net.CloseConnection(m_s);
				StopServer();
				m_nCmdClock = -1;
				m_s = NULL;
				DoButtons(); // set after setting m_s
				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{

unsigned long ulRecv = ulLen;
char* pchRecv = gbu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, ulLen);
				
				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
				}
				else
				{
					fwrite((char*)puc, sizeof(char), ulLen, logfp);
				}
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);

				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();

						m_lcResponses.InsertItem(nCount, "");
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 2, (char*)pchRecv );
							free(pchRecv);
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 2, (char*)puc );
						}
						m_lcResponses.EnsureVisible(nCount, FALSE);
	
						while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
						
/*
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
					int nCount = m_lcResponses.GetItemCount();
					m_lcResponses.InsertItem(nCount, "");
					m_lcResponses.SetItemText(nCount, 1, (char*)puc );
					m_lcResponses.EnsureVisible(nCount, FALSE);
*/
					free(puc);
				}
			}
		}

	}
}

void CSNMPTestDlg::OnButtonRecv() 
{
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
/*
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
		m_nCmdClock = -1;
		m_net.CloseConnection(m_s);
		StopServer();
		m_s = NULL;
		DoButtons(); // set after setting m_s
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
/*
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
			m_nCmdClock = -1;
			m_net.CloseConnection(m_s);
			StopServer();
			m_s = NULL;
			DoButtons(); // set after setting m_s
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
/*
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
*/
				m_nCmdClock = -1;
				m_net.CloseConnection(m_s);
				StopServer();
				m_s = NULL;
				DoButtons(); // set after setting m_s
				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{



unsigned long ulRecv = ulLen;
char* pchRecv = gbu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, ulLen);
				
				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
				}
				else
				{
					fwrite((char*)puc, sizeof(char), ulLen, logfp);
				}
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);

				fclose(logfp);
			}
}

						int nCount = m_lcResponses.GetItemCount();

						m_lcResponses.InsertItem(nCount, "");
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 2, (char*)pchRecv );
							free(pchRecv);
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 2, (char*)puc );
						}
						m_lcResponses.EnsureVisible(nCount, FALSE);

						while(nCount>LISTBOXMAX){  m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
/*
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
					int nCount = m_lcResponses.GetItemCount();
					m_lcResponses.InsertItem(nCount, "");
					m_lcResponses.SetItemText(nCount, 1, (char*)puc );
					m_lcResponses.EnsureVisible(nCount, FALSE);
*/
					free(puc);
				}
			}
		}

	}
}

void CSNMPTestDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();  prevent cancellation with enter.
}

void CSNMPTestDlg::OnRadioDnf() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CSNMPTestDlg::OnRadioIcon() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CSNMPTestDlg::OnRadioLyr() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void CSNMPTestDlg::OnRadioG7() 
{
	// TODO: Add your control notification handler code here
	SetupFlavor();
}

void SocketMonitorThread(void* pvArgs)
{
	CSNMPTestDlg* p = (CSNMPTestDlg*)pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{
			timeval tv;
			tv.tv_sec = 0; 
			tv.tv_usec = 500;  // timeout value
			int nNumSockets;
			fd_set fds;

			FD_ZERO(&fds);  // Zero this out each time
			FD_SET(p->m_s, &fds);

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
			if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
			{
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(p->m_s, &fds)))
				) 
			{ 
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else // there is recv data.
			{ // wait some delay.
				if((clock() > p->m_nClock+1000)&&(!p->m_bInCommand))
				{
					p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
					if(clock() > p->m_nClock+1000) // five seconds is a huge timeout.
						p->OnMonitorButtonRecv();
				}
			}
			if(p->m_bMonitorSocket) Sleep(100);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}



void CSNMPTestDlg::OnButtonViewlog() 
{
	HINSTANCE hi;
	hi=ShellExecute((HWND) NULL, NULL, LOGFILENAME, NULL, NULL, SW_HIDE);
}


void CSNMPTestDlg::OnCloseupComboCommand() 
{
//	OnSelchangeComboCommand();

}


void CSNMPTestDlg::OnCheckKeepAlive() 
{
	UpdateData(TRUE);

//	if(m_bAutoKeepAlive) AfxMessageBox("Auto Keep Alive"); else  AfxMessageBox("NOT Auto Keep Alive"); 

	if(m_bAutoKeepAlive)
	{
	}
	else
	{
	}
}

void CSNMPTestDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent==666)
	{
		if(m_nCmdClock>0)
		{
			CString s;
			int nElapsed = clock()-m_nCmdClock;
			s.Format("%d ms", nElapsed);
			GetDlgItem(IDC_STATIC_TIMEOUT)->SetWindowText(s);

			if(!m_bInCommand)
			if(m_bAutoKeepAlive)
			if(
					(
						((m_db.m_ppdbConn)&&(m_db.m_ucNumConnections>0)&&(m_db.m_ppdbConn[0]))
					||(m_s)
					)
				&&(
				    (nElapsed>m_nHeartbeatInterval)
					||(m_bForceKeepAlive)
					)
				) 
			{
				// get old selection so we can set it back
				CComboBox* p =((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND));

				int s = p->GetCurSel();

				CString szCmd;
				if(s>=0) 
				{
					int len = p->GetLBTextLen( s );
					p->GetLBText( s, szCmd.GetBuffer(len) );
					szCmd.ReleaseBuffer();
				}

//				((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, "heartbeat");
				m_bFormatKeepAlive = TRUE;
				OnSelchangeComboCommand();

				// send the command when able
				while(m_bInCommand) Sleep(1);
				OnButtonSend();

				// set everything back:
				m_bFormatKeepAlive = FALSE;

//				((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->SelectString(-1, szCmd);
//				OnSelchangeComboCommand();

				m_bForceKeepAlive = FALSE;
			}
		}
		else
		{
			if(m_nCmdClock<0)
			{
				m_nCmdClock=0;
				GetDlgItem(IDC_STATIC_TIMEOUT)->SetWindowText("time elapsed");
			}
		}


	}
	else
	if(nIDEvent==69)
	{
		if(m_bPrettyLights)
		{
			if((m_bInPrettyLightsButton)||(m_s==NULL)) return;
			m_bInPrettyLightsButton = TRUE;

			int n = rand()%16;

			ButtonPush(0, LPARAM(m_button[n].m_hWnd));
			Sleep(m_nButtonPressDelay);

			ButtonRelease(0, LPARAM(m_button[n].m_hWnd));

			m_bInPrettyLightsButton = FALSE;
		}
	}
	
	
	CDialog::OnTimer(nIDEvent);
}


int CSNMPTestDlg::TruncateText(CString* pszText, int nLimit)
{
	if(nLimit<0) nLimit=0;
	if(pszText)
	{
		char* p = pszText->GetBuffer(nLimit+1);
		if(p)
		{
//			AfxMessageBox(p);
			*(p+nLimit) = 0;
//			AfxMessageBox(p);
		}
		pszText->ReleaseBuffer(nLimit);
//			AfxMessageBox(*pszText);
	}

	return 0;
}

int CSNMPTestDlg::FormatTime(CString* pszText)
{
	if(pszText)
	{
		char* p = pszText->GetBuffer(12);
		if(p)
		{
			int n = 0;
			if((	*(p+0) < 48)||(	*(p+0) > 57)) *(p+0) = '0';
			if((	*(p+1) < 48)||(	*(p+1) > 57)) *(p+1) = '0';

			n = ((*p)-48)*10 + ((*(p+1))-48);
			if(n>23)
			{
			 *(p+0) = '2';
			 *(p+1) = '3';
			}
			
			if(	*(p+2) != ':') *(p+2) = ':';

			if((	*(p+3) < 48)||(	*(p+3) > 57)) *(p+3) = '0';
			if((	*(p+4) < 48)||(	*(p+4) > 57)) *(p+4) = '0';

			n = ((*(p+3))-48)*10 + ((*(p+4))-48);
			if(n>59)
			{
			 *(p+3) = '5';
			 *(p+4) = '9';
			}
			
			if(	*(p+5) != ':') *(p+5) = ':';

			if((	*(p+6) < 48)||(	*(p+6) > 57)) *(p+6) = '0';
			if((	*(p+7) < 48)||(	*(p+7) > 57)) *(p+7) = '0';

			n = ((*(p+6))-48)*10 + ((*(p+7))-48);
			if(n>59)
			{
			 *(p+6) = '5';
			 *(p+7) = '9';
			}

			if(	*(p+8) != ';') *(p+8) = ';';

			if((	*(p+9) < 48)||(	*(p+9) > 57)) *(p+9) = '0';
			if((	*(p+10) < 48)||(	*(p+10) > 57)) *(p+10) = '0';
		
			n = ((*(p+9))-48)*10 + ((*(p+10))-48);
			if(n>29)
			{
			 *(p+9) = '2';
			 *(p+10) = '9';
			}
			*(p+11) = 0;
		}
		pszText->ReleaseBuffer();
	}

	return 0;
}


void CSNMPTestDlg::OnSelchangeComboCommand() 
{
	// TODO: Add your control notification handler code here
	CComboBox* p =((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND));

	int s = p->GetCurSel();

	if(s>=0) 
	{
    int len = p->GetLBTextLen( s );
    p->GetLBText( s, m_szCmdName.GetBuffer(len) );
    m_szCmdName.ReleaseBuffer();
	}

	int n = ((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->FindStringExact( -1, m_szCmdName );

	if(m_bFormatKeepAlive){ m_szCmdName = "heartbeat"; n=0;}

	if((m_szCmdName.GetLength()>0)&&(n!=CB_ERR))
	{

//	AfxMessageBox("here");
		CString szText;
		// make the appropriate command:
		void* pm = NULL;

		unsigned char* pVal = (unsigned char*)malloc(10);
		if(pVal) pVal[0] = 1;

		if(m_szCmdName.CompareNoCase("heartbeat")==0)
		{

/*
        "Heartbeat1= USP Heartbeat:                    
        Byte0=  polling rate in multiples of 250ms
        Byte1-4: unique 4 byte count. LSB first
        Byte5-6: GPO 16-bit bitmap, MS to LS  1=ON 0=OFF  
                 (Byte5,bit7= GPO#16, Byte6,bit0= GPO#1 
        Byte7-8: GP1 16-bit bitmap, MS to LS  1=ON 0=OFF
                 (Byte7,bit7= GPI#16, Byte8,bit0= GPI#1 
*/

			if(pVal)
			{
				pVal[0] = 0xff&(m_nHeartbeatInterval/250);
				pVal[1] = 0x00;
				pVal[2] = 0x00;
				pVal[3] = 0x00;
				pVal[4] = 0x00;

				// just make the GPO state be the button state 
				int i=0;
				pVal[5] = 0x00;
				while(i<8)
				{
					pVal[5] |= (((m_bTallyState[15-i])?1:0)<<(7-i));
					i++;
				}
				i=0;
				pVal[6] = 0x00;
				while(i<8)
				{
					pVal[6] |= (((m_bTallyState[7-i])?1:0)<<(7-i));
					i++;
				}
				 // and forget GPI
				pVal[7] = 0x00;
				pVal[8] = 0x00;
				
			}

			pm = u.CreateSNMPMessage(SNMP_SETREQUESTPDU, m_szCommunity.GetBuffer(0), "1.3.6.1.4.1.21541.8.1.0", pVal, 9);
		}
		else
		{
			// just key press for now.
			pm = u.CreateSNMPMessage(SNMP_SETREQUESTPDU, m_szCommunity.GetBuffer(0), "1.3.6.1.4.1.21541.6.1.0", pVal, 1); // this is a stand in value
		}

		if(pVal) free(pVal);

	((CSNMPMessage*)pm)->m_ulRequest =  u.m_ucLastMessageNumber+1; // expected value!
	if(((CSNMPMessage*)pm)->m_ulRequest>=254) ((CSNMPMessage*)pm)->m_ulRequest=0; // loop around. but keep value 255 separate.  it is reserved

//	AfxMessageBox("here");

//ReturnMessageBuffer(CSNMPMessage* pMessage)


		DoButtons();
	

		if(pm)
		{

//		AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);
		if(m_pvMessage) delete m_pvMessage;
			m_pvMessage = pm;

			char* pch = NULL;
			unsigned char* pchBuf = NULL;
//	AfxMessageBox("here");

			pchBuf = u.ReturnMessageBuffer((CSNMPMessage*)pm);
			if(pchBuf)
			{
//	CString Q;
//	Q.Format("[%s] %08x", pchBuf, pchBuf);
//	AfxMessageBox(Q);
//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

				unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
				ulLen += u.ReturnLength(ulLen)+1;

//	Q.Format("buflen %d", ulLen);
//	AfxMessageBox(Q);

				unsigned long ulHdr = ulLen;
				unsigned long ulReadable = 0;

				char* pchReadable = (char*)u.ReturnReadableMessageBuffer((CSNMPMessage*)pm);
				char* pchHdr = gbu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM64BIT);

				if(pchReadable) ulReadable = strlen(pchReadable);

				pch = (char*)malloc(ulReadable + ulHdr +3); //term 0
				if(pch)
				{
					memcpy(pch, pchReadable, ulReadable);
					memset(pch+ulReadable, 13, 1);
					memset(pch+ulReadable+1, 10, 1);
					memcpy(pch+ulReadable+2, pchHdr, ulHdr);
					*(pch+ulReadable + ulHdr +2) = 0;
	//AfxMessageBox(pch);

				}
				if(pchReadable) free(pchReadable);
				if(pchHdr) free(pchHdr);

			}
			
	//AfxMessageBox("delete buf");
			if(pchBuf) free(pchBuf);
			pchBuf = NULL;
	//AfxMessageBox("deleted buf");


			if(pch)
			{
				//now fill the text box
				GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(pch);
	//AfxMessageBox("delete other buf");
				free(pch); // used malloc
			}
			else
			{
				GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message buffer!");
			}

		}
		else
		{
			//now fill the text box
			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message object!");
		}
	}
	else
	{
		//now fill the text box
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Not valid");
	}

}



void CSNMPTestDlg::OnButton1()   { PressButton(1); }
void CSNMPTestDlg::OnButton2()   { PressButton(2); }
void CSNMPTestDlg::OnButton3()   { PressButton(3); }
void CSNMPTestDlg::OnButton4()   { PressButton(4); }
void CSNMPTestDlg::OnButton5()   { PressButton(5); }
void CSNMPTestDlg::OnButton6()   { PressButton(6); }
void CSNMPTestDlg::OnButton7()   { PressButton(7); }
void CSNMPTestDlg::OnButton8()   { PressButton(8); }
void CSNMPTestDlg::OnButton9()   { PressButton(9); }
void CSNMPTestDlg::OnButton10()  { PressButton(10); }
void CSNMPTestDlg::OnButton11()  { PressButton(11); }
void CSNMPTestDlg::OnButton12()  { PressButton(12); }
void CSNMPTestDlg::OnButton13()  { PressButton(13); }
void CSNMPTestDlg::OnButton14()  { PressButton(14); }
void CSNMPTestDlg::OnButton15()  { PressButton(15); }
void CSNMPTestDlg::OnButton16()  { PressButton(16); }

void CSNMPTestDlg::PressButton(int nNum) 
{
	return; // exit for now, stuff moved to button press and button release
	// if this is being hit, then definitely the thing thing this thinged, and we have an keypress object in there already.
	// just need to update the OID and the value and send it.

	// we are going to make it a toggle for now.

	// give an indication that the button is being pressed

	m_button[nNum-1].SetDisabledColor(0x00005555);
	m_button[nNum-1].SetBGColor(0x0000ffff, TRUE);

	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		AfxGetApp()->PumpMessage();



	CSNMPMessage* pMessage = ((CSNMPMessage*)m_pvMessage);


	if(pMessage->m_pchOID)
	{
		free(pMessage->m_pchOID);
	}
	char buffer[256];
	sprintf(buffer, "1.3.6.1.4.1.21541.%d.%d.0", (m_Do_GPO?2:6), (m_buttonevent[nNum-1].index>0)?(int)m_buttonevent[nNum-1].index:nNum);


	pMessage->m_pchOID = (char*)malloc(strlen(buffer)+1);
	if(pMessage->m_pchOID)
	{
		strcpy(pMessage->m_pchOID, buffer);
	}

	// change the value.
	if(pMessage->m_pucValue)
	{
		free(pMessage->m_pucValue);
	}
	unsigned char* pVal = (unsigned char*)malloc(2);
	if(pVal)
	{
/* // this was before, i was toggling the states.  at this point we just want =1, key press down.  keypress up comes after this.
		if(m_bTallyState[nNum-1])
		{
			pVal[0] = 0;
		}
		else
*/
		{
			pVal[0] = 1;
		}
		pMessage->m_ulValueLength=1;
	}
	pMessage->m_pucValue = pVal; // even if null

	pMessage->m_ulRequest =  u.m_ucLastMessageNumber+1; // expected value!
	if(pMessage->m_ulRequest>=254) pMessage->m_ulRequest=0; // loop around. but keep value 255 separate.  it is reserved

	// update the window, then send

	char* pch = NULL;
	unsigned char* pchBuf = NULL;
//	AfxMessageBox("here");

	pchBuf = u.ReturnMessageBuffer(pMessage);
	if(pchBuf)
	{
//	CString Q;
//	Q.Format("[%s] %08x", pchBuf, pchBuf);
//	AfxMessageBox(Q);
//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

		unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
		ulLen += u.ReturnLength(ulLen)+1;

//	Q.Format("buflen %d", ulLen);
//	AfxMessageBox(Q);

		unsigned long ulHdr = ulLen;
		unsigned long ulReadable = 0;

		char* pchReadable = (char*)u.ReturnReadableMessageBuffer(pMessage);
		char* pchHdr = gbu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM64BIT);

		if(pchReadable) ulReadable = strlen(pchReadable);

		pch = (char*)malloc(ulReadable + ulHdr +3); //term 0
		if(pch)
		{
			memcpy(pch, pchReadable, ulReadable);
			memset(pch+ulReadable, 13, 1);
			memset(pch+ulReadable+1, 10, 1);
			memcpy(pch+ulReadable+2, pchHdr, ulHdr);
			*(pch+ulReadable + ulHdr +2) = 0;
//AfxMessageBox(pch);

		}
		if(pchReadable) free(pchReadable);
		if(pchHdr) free(pchHdr);

	}
	
//AfxMessageBox("delete buf");
	if(pchBuf) free(pchBuf);
	pchBuf = NULL;
//AfxMessageBox("deleted buf");


	if(pch)
	{
		//now fill the text box
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(pch);
//AfxMessageBox("delete other buf");
		free(pch); // used malloc
	}
	else
	{
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message buffer!");
	}

	OnButtonSend();  // send the key press.


	Sleep(m_nButtonPressDelay); //delay!
	// now, need to send the "unpress" to indicate that the button is now "up" again.

	// change the value.
	if(pMessage->m_pucValue)
	{
		free(pMessage->m_pucValue);
	}
	pVal = (unsigned char*)malloc(2);
	if(pVal)
	{
		pVal[0] = 0;
		pMessage->m_ulValueLength=1;
	}
	pMessage->m_pucValue = pVal; // even if null

	pMessage->m_ulRequest =  u.m_ucLastMessageNumber+1; // expected value!
	if(pMessage->m_ulRequest>=254) pMessage->m_ulRequest=0; // loop around. but keep value 255 separate.  it is reserved

	// update the window, then send

	pch = NULL;
	pchBuf = NULL;
//	AfxMessageBox("here");

	pchBuf = u.ReturnMessageBuffer(pMessage);
	if(pchBuf)
	{
//	CString Q;
//	Q.Format("[%s] %08x", pchBuf, pchBuf);
//	AfxMessageBox(Q);
//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

		unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
		ulLen += u.ReturnLength(ulLen)+1;

//	Q.Format("buflen %d", ulLen);
//	AfxMessageBox(Q);

		unsigned long ulHdr = ulLen;
		unsigned long ulReadable = 0;

		char* pchReadable = (char*)u.ReturnReadableMessageBuffer(pMessage);
		char* pchHdr = gbu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM64BIT);

		if(pchReadable) ulReadable = strlen(pchReadable);

		pch = (char*)malloc(ulReadable + ulHdr +3); //term 0
		if(pch)
		{
			memcpy(pch, pchReadable, ulReadable);
			memset(pch+ulReadable, 13, 1);
			memset(pch+ulReadable+1, 10, 1);
			memcpy(pch+ulReadable+2, pchHdr, ulHdr);
			*(pch+ulReadable + ulHdr +2) = 0;
//AfxMessageBox(pch);

		}
		if(pchReadable) free(pchReadable);
		if(pchHdr) free(pchHdr);

	}
	
//AfxMessageBox("delete buf");
	if(pchBuf) free(pchBuf);
	pchBuf = NULL;
//AfxMessageBox("deleted buf");


	if(pch)
	{
		//now fill the text box
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(pch);
//AfxMessageBox("delete other buf");
		free(pch); // used malloc
	}
	else
	{
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message buffer!");
	}

	OnButtonSend();  // send the key un-press.


/*
	if(m_pucLastResponse)
	{
		// let;s set it up.
		unsigned long ulLen = u.ReturnLength(&(m_pucLastResponse[1])); // length is just of the thing.
		ulLen += u.ReturnLength(ulLen)+1;

//	Q.Format("buflen %d", ulLen);
//	AfxMessageBox(Q);

		CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer(m_pucLastResponse);

		if(pMessage)
		{
//			CString Q;
//			Q.Format("pMessage->m_ulErrorStatus %d", pMessage->m_ulErrorStatus);
//			AfxMessageBox(Q);


//			if(pMessage->m_ulErrorStatus == 0) //  yeah, except that DNF has a bug.	//from Dan Fogel:  Disregard the error-status = noSuchName (0x02).  Appears to be a typo that never got corrected.
			{
				if(m_bTallyState[nNum-1])
				{
					m_bTallyState[nNum-1] = FALSE;
//					AfxMessageBox("FALSE");

				}
				else
				{
					m_bTallyState[nNum-1] = TRUE;
//					AfxMessageBox("TRUE");
				}
			}
			
			delete(pMessage);
		}

	}
	*/
//	SetButtonText(); // dont do this here, let the GPO staus come in from the GTP

	// set the color back.
	m_button[nNum-1].SetDisabledColor((m_bTallyState[nNum-1]?0x00005500:GetSysColor(COLOR_BTNFACE)));
	m_button[nNum-1].SetBGColor((m_bTallyState[nNum-1]?0x0000ff00:GetSysColor(COLOR_BTNFACE)), TRUE);

	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		AfxGetApp()->PumpMessage();
}

void CSNMPTestDlg::OnClickList3(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnDblclkList3(pNMHDR, pResult);

	*pResult = 0;
}

void CSNMPTestDlg::OnDblclkList3(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	int nCount = m_lcResponses.GetItemCount();
	int nItem = m_lcResponses.GetNextItem( -1,  LVNI_SELECTED );

	CString szSend;
	CString szRecv;
	if((nItem>=0)&&(nItem<nCount))
	{
//    m_list.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
 //   m_list.SetItemState(nItem, INDEXTOSTATEIMAGEMASK(2), LVIS_STATEIMAGEMASK);


		BOOL bOutgoing = TRUE;

		if(m_lcResponses.GetItemText(nItem, 0).CompareNoCase("->")) bOutgoing = FALSE;
		szSend = m_lcResponses.GetItemText(nItem, 1);
		szRecv = m_lcResponses.GetItemText(nItem, 2);


		// update the window, then send

		char* pch = NULL;
		unsigned char* pchBuf = NULL;
	//	AfxMessageBox("here");

		unsigned long ulSnd = szSend.GetLength();
		char* pchSnd = szSend.GetBuffer(0);

		unsigned long ulRcv = szRecv.GetLength();
		char* pchRcv = szRecv.GetBuffer(0);


		CString szOut;
		unsigned long ulIndex=0;
		if(ulSnd)
		{
			pchBuf = (unsigned char*)gbu.DecodeReadableHex(pchSnd, &ulSnd);
			if(pchBuf)
			{
		//	CString Q;
		//	Q.Format("[%s] %08x", pchBuf, pchBuf);
		//	AfxMessageBox(Q);
		//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

				unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
				ulLen += u.ReturnLength(ulLen)+1;

		//	Q.Format("buflen %d", ulLen);
		//	AfxMessageBox(Q);

				CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer(pchBuf);

				char* pchReadable = (char*)u.ReturnReadableMessageBuffer(pMessage);

				szOut.Format("%s%c%c%s%c%c%c%c",bOutgoing?"Sent:":"Received:",13,10, pchReadable, 13,10,13,10) ;
						 
		//AfxMessageBox(pch);
			
				if(pchReadable) free(pchReadable);
				if(pMessage) delete(pMessage);
			}
		}
		else
		{
			szOut.Format("%s%c%c[zero bytes]%c%c%c%c",bOutgoing?"Sent:":"Received:",13,10,13,10,13,10) ;
		}

		if(pchBuf) free(pchBuf);
		pchBuf = NULL;

		CString szAdd;

		if(ulRcv)
		{
			pchBuf = (unsigned char*)gbu.DecodeReadableHex(pchRcv, &ulRcv);
			if(pchBuf)
			{
		//	CString Q;
		//	Q.Format("[%s] %08x", pchBuf, pchBuf);
		//	AfxMessageBox(Q);
		//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

				unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
				ulLen += u.ReturnLength(ulLen)+1;

		//	Q.Format("buflen %d", ulLen);
		//	AfxMessageBox(Q);

				CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer(pchBuf);

				char* pchReadable = (char*)u.ReturnReadableMessageBuffer(pMessage);

				szAdd.Format("%s%c%c%s ",bOutgoing?"Received":"Sent:",13,10, pchReadable, 13,10) ;
						 
		//AfxMessageBox(pch);
			
				if(pchReadable) free(pchReadable);
				if(pMessage) delete(pMessage);
			}
		}
		else
		{
			szAdd.Format("%s%c%c[zero bytes] ",bOutgoing?"Received":"Sent:",13,10) ;
		}

		szOut+=szAdd;

		
	//AfxMessageBox("delete buf");
		if(pchBuf) free(pchBuf);
		pchBuf = NULL;
	//AfxMessageBox("deleted buf");


		//now fill the text box
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(szOut);



	}


	*pResult = 0;
}

void CSNMPTestDlg::SetButtonText() 
{

	CString s;
	int i=0;
	while(i<16)
	{
//		s.Format("%x;%d: %s", (m_button[i].GetState()&0x00000004),i+1,(m_bTallyState[i]?"ON":"OFF"));
		s.Format("%d: %s", (m_buttonevent[i].index>0)?(int)m_buttonevent[i].index:i+1,(m_bTallyState[i]?"ON":"OFF"));
//		s.Format("%d: %s", i+1,(m_bTallyState[i]?"ON":"OFF"));
		m_button[i].SetWindowText(s);

		if(m_button[i].GetState()&0x00000004) // if pressed
		{ // because if it is pressed we want to 
			m_button[i].SetDisabledColor((m_bTallyState[i]?0x00227766:0x00005555));
			m_button[i].SetBGColor((m_bTallyState[i]?0x0044ddbb:0x0000ffff), TRUE);
		}
		else// if not pressed
		{
			m_button[i].SetDisabledColor((m_bTallyState[i]?0x00005500:GetSysColor(COLOR_BTNFACE)));
			m_button[i].SetBGColor((m_bTallyState[i]?0x0000ff00:GetSysColor(COLOR_BTNFACE)), TRUE);
		}
		i++;
	}
}


void CSNMPTestDlg::StartServer() 
{
	CNetServer* pServer = new CNetServer;
	pServer->m_usPort = 161;
	pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

//	pServer->m_nAf = PF_INET;
	pServer->m_nType = SOCK_DGRAM; 
	pServer->m_nProtocol = 0;

	pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
	if(pServer->m_pszName)strcpy(pServer->m_pszName, "SNMP server");

	pServer->m_pszStatus;				// status buffer with error messages from thread
	pServer->m_lpfnHandler = USP16ServerHandlerThread;			// pointer to the thread that handles the request.
	pServer->m_lpObject = this;								// pointer to the object passed to the handler thread.
	pServer->m_lpMsgObj = &m_net;					// pointer to the object with the Message function.

	char errorstring[DB_ERRORSTRING_LEN];

	int n = m_net.StartServer(pServer, &m_net, 10000, errorstring);
	if(n<NET_SUCCESS)
	{
		//report failure
		CString Q; Q.Format("Failed to start server with error %d", n);
		AfxMessageBox(Q);
	}
	else
	{

	}
}

void CSNMPTestDlg::StopServer() 
{
	m_net.StopServer(161);
}

void USP16ServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	CSNMPTestDlg* pDlg = (CSNMPTestDlg*)pClient->m_lpObject;
	CSNMPUtil snmpu;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		char* pchSnd = NULL;
		unsigned long ulSnd=0;

		CNetUtil net(false); // local object for utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
//		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it


		CNetDatagramData** ppData=NULL;
		int nNumDataObj=0;

		struct timeval tv;
		tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
		fd_set fds;
		int nNumSockets;
		FD_ZERO(&fds);

		FD_SET(pClient->m_socket, &fds);

		nNumSockets = select(0, &fds, NULL, NULL, &tv);

//		SOCKADDR_IN si;
		int nSize = sizeof(pClient->m_si);

		while (
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&(nNumSockets>0)
					)
		{
			char* pchRecv = (char*)malloc(NET_COMMBUFFERSIZE);
			if(pchRecv)
			{
				int nNumBytes 
					=  recvfrom(
											pClient->m_socket,
											pchRecv,
											NET_COMMBUFFERSIZE,
											0,
											(struct sockaddr*)&(pClient->m_si),
											&nSize
										);

				if(
						(nNumBytes>0)
					&&(pDlg->m_saiGTPhost.sin_addr.s_net == pClient->m_si.sin_addr.s_net)
					&&(pDlg->m_saiGTPhost.sin_addr.s_host == pClient->m_si.sin_addr.s_host)
					&&(pDlg->m_saiGTPhost.sin_addr.s_lh == pClient->m_si.sin_addr.s_lh)
					&&(pDlg->m_saiGTPhost.sin_addr.s_impno == pClient->m_si.sin_addr.s_impno) // only accept communication from the GTP32!
					)
				{

					MessageLogging_t* pMsgLog = new MessageLogging_t;
					if(pMsgLog)
					{
						pMsgLog->pchReadableHexSnd=NULL;
						pMsgLog->ulReadableHexSnd=0;
						pMsgLog->pchReadableSnd=NULL;
						pMsgLog->ulReadableSnd=0;
						pMsgLog->pchReadableHexRcv=NULL;
						pMsgLog->ulReadableHexRcv=0;
						pMsgLog->pchReadableRcv=NULL;
						pMsgLog->ulReadableRcv=0;
					}

/*
typedef struct MessageLogging_t
{
	char* pchReadableHexSnd;
	unsigned long ulReadableHexSnd;
	char* pchReadableSnd;
	unsigned long ulReadableSnd;
	char* pchReadableHexRcv;
	unsigned long ulReadableHexRcv;
	char* pchReadableRcv;
	unsigned long ulReadableRcv;

} MessageLogging_t;
*/
					if(pMsgLog)
					{
pMsgLog->ulReadableRcv = nNumBytes;
pMsgLog->pchReadableHexRcv = gbu.ReadableHex((char*)pchRecv, &pMsgLog->ulReadableRcv, MODE_FULLHEX);
					}

if((pDlg)&&(pDlg->m_bLogTransactions)&&(pMsgLog))
{

	if(pMsgLog->pchReadableHexRcv)
	{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(logfp, "%s%03d Listener Recd %d bytes from %d.%d.%d.%d: %s%c%c", 
					logtmbuf, timestamp.millitm, 
					nNumBytes,
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,

					pMsgLog->pchReadableHexRcv, 13, 10);

				//free(pchSnd);

				CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)pchRecv);
				pMsgLog->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pMessage);
				delete pMessage;

				fprintf(logfp, "%s%03d Listener Recd message: %s%c%c", 
					logtmbuf, timestamp.millitm, 
					pMsgLog->pchReadableSnd, 13, 10);

				fclose(logfp);
			}
	}
}


					BOOL bNew = TRUE;  // Always do new, at least for now.  these SNMP messages are short and come in fast, but they come in whole.  So what was happening was that two would come in immediately, and we would assemble them into one buffer, and then process just the first part of the buffer, effectively ignoring the second msg.

/*
					BOOL bNew = FALSE;

					// first check the host list
					if(ppData)
					{
						int i=0;
						BOOL bFound = FALSE;
						while(i<nNumDataObj)
						{
							if(
									(ppData[i]->m_si.sin_port == si.sin_port) 
								&&(ppData[i]->m_si.sin_addr.S_un.S_addr == si.sin_addr.S_un.S_addr)
								) // check both port and address.
							{
								// add here
								unsigned char* pucNew = (unsigned char*)malloc(nNumBytes + ppData[i]->m_ulRecv);
								if(pucNew)
								{
									if(ppData[i]->m_pucRecv)
									{
										memcpy(pucNew, ppData[i]->m_pucRecv, ppData[i]->m_ulRecv);			
										free(ppData[i]->m_pucRecv);
									}

									memcpy(pucNew + ppData[i]->m_ulRecv, pchRecv, nNumBytes);

									ppData[i]->m_ulRecv += nNumBytes;
									ppData[i]->m_pucRecv = pucNew;
								}
								bFound = TRUE;
								break;
							}
						
							i++;
						}
						if(!bFound) bNew = TRUE;
					}
					else
					{
						bNew = TRUE;
						// add new.
					}
*/

					if(bNew)
					{
						CNetDatagramData* pData=new CNetDatagramData;

						if(pData)
						{
							pData->m_lpObject = NULL;
							pData->m_pucRecv = NULL;
							pData->m_ulRecv = 0;

							if(pMsgLog) pData->m_lpObject = pMsgLog;

							CNetDatagramData** ppTempData = new CNetDatagramData*[nNumDataObj+1];
							if(ppTempData)
							{
								if(ppData)
								{
									memcpy(ppTempData, ppData, nNumDataObj*sizeof(CNetDatagramData*));
									delete [] ppData;
								}

								ppTempData[nNumDataObj++] = pData;
								ppData = ppTempData;

							}
							unsigned char* pucNew = (unsigned char*)malloc(nNumBytes);
							if(pucNew)
							{
								memcpy(pucNew, pchRecv, nNumBytes);

								pData->m_ulRecv = nNumBytes;
								pData->m_pucRecv = pucNew;
								memcpy(&pData->m_si, &(pClient->m_si), sizeof(pClient->m_si));
								//pData->m_si.sin_addr.S_un.S_addr = si.sin_addr.S_un.S_addr;
							}

						}
					}
				}

				free(pchRecv); // free this, we have copied it to the object buffer (or failed, separately)
			}

			tv.tv_sec = 0; tv.tv_usec = 1000;  // longer timeout value for data continuation
			FD_ZERO(&fds);
			FD_SET(pClient->m_socket, &fds);
			nNumSockets = select(0, &fds, NULL, NULL, &tv);

		}

		// no more data on pipe - respond!

		int q=0;
		while(q<nNumDataObj)
		{
			if(ppData[q])
			{

				if(ppData[q]->m_pucRecv)
				{
					// create a message from the buffer.

					CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)ppData[q]->m_pucRecv);

					if(pMessage)
					{

//CString Q; Q.Format("Error status %d; index %d", pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex);
//AfxMessageBox(Q);

						if(pMessage->m_pucValue)
						{

if((pDlg)&&(pDlg->m_bLogTransactions))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

			fprintf(logfp, "%s%03d checking event handlers for event %s:%c%c", 
				logtmbuf, timestamp.millitm, pMessage->m_pucValue, 13, 10);

			fclose(logfp);
		}
	
}
							// deal with the event value.
							int i=0;
							while(i<16)
							{
								char buffer[128];
								sprintf(buffer, "%s:",pDlg->m_buttonevent[i].event);
								int nLen = strlen(buffer);


if((pDlg)&&(pDlg->m_bLogTransactions))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

			fprintf(logfp, "%s%03d checking event handler %d: %s%c%c", 
				logtmbuf, timestamp.millitm, i+1,buffer, 13, 10);

			fclose(logfp);
		}
	
}




								if(strnicmp(buffer, (char*)(pMessage->m_pucValue), nLen)==0)
								{
									// event matches
//									AfxMessageBox((char*)pMessage->m_pucValue + nLen);
									bool bVal = (atoi((char*)pMessage->m_pucValue + nLen)==0)?false:true;
									

if((pDlg)&&(pDlg->m_bLogTransactions))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

			fprintf(logfp, "%s%03d MATCH event handler %d: %s: value is %s, %d%c%c", 
				logtmbuf, timestamp.millitm, i+1,(char*)(pMessage->m_pucValue), (char*)pMessage->m_pucValue + nLen, bVal, 13, 10);

			fclose(logfp);
		}
	
}

									if(pDlg->m_buttonevent[i].bInvert)
									{
										pDlg->m_bTallyState[i] = bVal?FALSE:TRUE;
									}
									else
									{
										pDlg->m_bTallyState[i] = bVal?TRUE:FALSE;
									}
									pDlg->m_button[i].Invalidate();
								}
								i++;
							}



if((pDlg)&&(pDlg->m_bLogTransactions))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

			fprintf(logfp, "%s%03d finished checking %d event handlers for event %s:%c%c%c%c", 
				logtmbuf, timestamp.millitm, i,pMessage->m_pucValue, 13, 10, 13, 10);

			fclose(logfp);
		}
	
}
							free(pMessage->m_pucValue);

						}
						pMessage->m_pucValue = NULL;
						pMessage->m_ulValueLength=0;
						pMessage->m_ucPDUType = SNMP_GETRESPONSEPDU;
						pMessage->m_ulErrorStatus = 0;
						pMessage->m_ulErrorIndex = 0;


//CString Q; Q.Format("Error status %d; index %d", pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex);
//AfxMessageBox(Q);


						// then send a response

						pchSnd = (char*)u.ReturnMessageBuffer(pMessage);
						ulSnd = u.ReturnLength((unsigned char*)pchSnd+1);
						ulSnd += u.ReturnLength(ulSnd)+1;

						sendto(
							pClient->m_socket,
							(char*)pchSnd, 
							ulSnd,
							0,
							(struct sockaddr *) &(ppData[q]->m_si),
							nSize
						);

if(ppData[q]->m_lpObject)
{
	((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd = ulSnd;
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd = gbu.ReadableHex((char*)pchSnd, &((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd, MODE_FULLHEX);
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pMessage);
}
						delete pMessage;

						free(pchSnd);  pchSnd=NULL;
					}
					else
					{
						// just echo it

						sendto(
							pClient->m_socket,
							(char*)ppData[q]->m_pucRecv, 
							ppData[q]->m_ulRecv,
							0,
							(struct sockaddr *) &(ppData[q]->m_si),
							nSize
						);
ulSnd = ppData[q]->m_ulRecv;
if(ppData[q]->m_lpObject)
{
	((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd = ulSnd;
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd = gbu.ReadableHex((char*)ppData[q]->m_pucRecv, &((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd, MODE_FULLHEX);

	CSNMPMessage* pTempMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)ppData[q]->m_pucRecv);
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pTempMessage);
	delete pTempMessage;
}
					}



if((pDlg)&&(pDlg->m_bLogTransactions)&&(ppData[q]->m_lpObject))
{

	if(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd)
	{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(logfp, "%s%03d Listener Sent %d bytes: %s%c%c", 
					logtmbuf, timestamp.millitm, 
					ulSnd,
					((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd, 13, 10);



				fprintf(logfp, "%s%03d Listener Sent message: %s%c%c%c%c", 
					logtmbuf, timestamp.millitm, 
					((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd, 13, 10, 13, 10);

				fclose(logfp);

			}
	//		free(pchRecv);  // no, use below.
	}
}


						int nCount = pDlg->m_lcResponses.GetItemCount();

						pDlg->m_lcResponses.InsertItem(nCount, "<-");
/*
						if(pchSnd)
						{
							m_lcResponses.InsertItem(nCount, pchSnd);
						}
						else
						{
							m_lcResponses.InsertItem(nCount, (char*)buffer);
						}

						
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}

*/
						if((ppData[q]->m_lpObject)&&(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexRcv))
						{
							pDlg->m_lcResponses.SetItemText(nCount, 1, ((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexRcv);
						}
						else
						{
							pDlg->m_lcResponses.SetItemText(nCount, 1, (char*)ppData[q]->m_pucRecv);
						}
						
						if((ppData[q]->m_lpObject)&&(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd))
						{
							pDlg->m_lcResponses.SetItemText(nCount, 2, (char*)((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd );
						}
						else
						{
							pDlg->m_lcResponses.SetItemText(nCount, 2, (char*)ppData[q]->m_pucRecv );
						}
						pDlg->m_lcResponses.EnsureVisible(nCount, FALSE);
						while(nCount>LISTBOXMAX){  pDlg->m_lcResponses.DeleteItem(0); nCount--; } //delete oldest

							
				}


				if(ppData[q]->m_lpObject)
				{
					// have to free all the buffers.
					MessageLogging_t* pMsgLog = (MessageLogging_t*) ppData[q]->m_lpObject;

					if(pMsgLog->pchReadableHexRcv) free(pMsgLog->pchReadableHexRcv);
					if(pMsgLog->pchReadableHexSnd) free(pMsgLog->pchReadableHexSnd);
					if(pMsgLog->pchReadableRcv) free(pMsgLog->pchReadableRcv);
					if(pMsgLog->pchReadableSnd) free(pMsgLog->pchReadableSnd);

					delete ppData[q]->m_lpObject;
				}
				if(ppData[q]->m_pucRecv) free(ppData[q]->m_pucRecv);

				delete ppData[q];
			}
			q++;
		}


		delete [] ppData;

		pDlg->SetButtonText();

		if(pchSnd) free(pchSnd);

	}



	(*(pClient->m_pulConnections))--;

	delete pClient; // was created with new in the thread that spawned this one.
}



#ifdef CHICKENMYCHICKENO

// the following thread had buffer assembly going on in there, and parsing of several messages per packet.
// for now we will skip all that, and in the thread above treat no more bytes on the wire as end of single command.
void OldUSP16ServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
 // AfxMessageBox("X");

	CSNMPTestDlg* pDlg = (CSNMPTestDlg*)pClient->m_lpObject;
	CSNMPUtil snmpu;

	char szCortexSource[MAX_PATH]; 
	strcpy(szCortexSource, "SNMPHandler");

	CBufferUtil bu;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szCortexSource);

		CNetUtil net(false); // local object for utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these persistent
		bool bPersist = true;  // make these persistent
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
//		char filename[MAX_PATH];
//		char lastrxfilename[MAX_PATH];
//		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		// initialize random seed
		srand ( time(NULL) );

	
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

if((pDlg)&&(pDlg->m_bLogTransactions))
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Connection from %d.%d.%d.%d on socket %d established.%c%c", 
					logtmbuf, timestamp.millitm, 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,
					pClient->m_socket, 13, 10);
				fclose(logfp);
			}
}

		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received data.
				// have to keep accumulating until we have received the number of bytes.....
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
						//	while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;  // don't skip anything

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{
							// check the buffer for an SNMP message.
							unsigned long ulSNMPLength = snmpu.ReturnLength((unsigned char*)(&(pchXMLStream)[1]));
							ulSNMPLength += snmpu.ReturnLength(ulSNMPLength)+1; // length of BER length encoding

							pchEnd = pchXMLStream+ulSNMPLength;

							if(pchEnd>pchXMLStream+ulAccumulatedBufferLen) pchEnd=NULL; // not enough data yet.

					//		pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
							//	pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
							//	while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

				//				if(pchNext<pchEnd+strlen(pchEnd))
								if(pchNext<pchXMLStream+ulAccumulatedBufferLen)
								{
									// we found a remainder.
					//				nLen = strlen(pchNext);
									nLen = (pchXMLStream+ulAccumulatedBufferLen) - pchNext;
									pch = (char*) malloc(nLen+1);  //term 0 even though not nec.
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;

										ulAccumulatedBufferLen=nLen;
									} else ulAccumulatedBufferLen=0;
								}
								else
								{
									pch = NULL;
									ulAccumulatedBufferLen = 0;
								}


								pchXML = pchXMLStream; // just use it.
			//					*pchEnd = 0; //null terminate it  NO do not! this is SNMP.

								pchXMLStream = pch;  // take the rest of the stream.
/*
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

	now, dealt with above.
*/



if((pDlg)&&(pDlg->m_bLogTransactions))
{
unsigned long ulSend = ulSNMPLength;
char* pchSnd = bu.ReadableHex((char*)pchXML, &ulSend, MODE_FULLHEX);

if(pchSnd)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(logfp, "%s%03d Listener Recd %d bytes: %s%c%c", 
					logtmbuf, timestamp.millitm, 
					ulSNMPLength,
					ulSend, 13, 10);

				fclose(logfp);
			}
			free(pchSnd);
}
}

								// do stuff here.

// like, for instance, make a response.....


								CSNMPMessage* rm = snmpu.CreateSNMPMessageFromBuffer((unsigned char*)pchXML);

								if(rm == NULL)
								{
									// just log it.
								}
								else
								{
									// turn it into the response

									rm->m_ulErrorStatus = 0;
									rm->m_ulErrorIndex = 0;
									rm->m_ulRequest = u.IncrementMessageNumber();  // not the local object, the GLOBAL one!
									rm->m_ucPDUType = SNMP_GETRESPONSEPDU;
								}
								
								_ftime(&timeactive);  // reset the inactivity timer.

								unsigned char* ucSend = snmpu.ReturnMessageBuffer(rm);

								ulSNMPLength = snmpu.ReturnLength(&(ucSend)[1]);
								ulSNMPLength += snmpu.ReturnLength(ulSNMPLength)+1; // length of BER length encoding

								int nReturn = net.SendLine(ucSend, ulSNMPLength, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
								if(nReturn<NET_SUCCESS)
								{

									break; // break out and discontinue sending.
								}
								else
								{

if((pDlg)&&(pDlg->m_bLogTransactions))
{
unsigned long ulSend = ulSNMPLength;
char* pchSnd = bu.ReadableHex((char*)pchXML, &ulSend, MODE_FULLHEX);

if(pchSnd)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(logfp, "%s%03d Listener Sent %d bytes: %s%c%c", 
					logtmbuf, timestamp.millitm, 
					ulSNMPLength,
					ulSend, 13, 10);

				fclose(logfp);
			}
			free(pchSnd);
}
}

								}
								// log it

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } 
									catch(...)
									{				
							//			g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception in XML free");  //(Dispatch message)
									}

								}
								pchXML = NULL;
//AfxMessageBox("004");

//AfxMessageBox("005");
								if (pchXMLStream) 
								{
									if(ulAccumulatedBufferLen>2) // really need to check this out more
									{
										// check the buffer for an SNMP message.
										unsigned long ulSNMPLength = snmpu.ReturnLength((unsigned char*)(&(pchXMLStream)[1]));
										ulSNMPLength += snmpu.ReturnLength(ulSNMPLength)+1; // length of BER length encoding

										pchEnd = pchXMLStream+ulSNMPLength;
										if(pchEnd>pchXMLStream+ulAccumulatedBufferLen) pchEnd=NULL; // not enough data yet.
									}
									else pchEnd=NULL; // not enough data yet.
								}
								else pchEnd = NULL;


								



//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

if((pDlg)&&(pDlg->m_bLogTransactions))
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Lost connection from %d.%d.%d.%d.%c%c", 
					logtmbuf, timestamp.millitm, 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,
					13, 10);
				fclose(logfp);
			}
}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;

if((pDlg)&&(pDlg->m_bLogTransactions))
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Connection from %d.%d.%d.%d timed out.%c%c", 
					logtmbuf, timestamp.millitm, 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,
					13, 10);
				fclose(logfp);
			}
}


				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "check %d", clock());  //(Dispatch message)

/*
						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}
*/

						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

//cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
//		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");
if((pDlg)&&(pDlg->m_bLogTransactions))
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

				fprintf(logfp, "%s%03d Connection from %d.%d.%d.%d on socket %d closed.%c%c", 
					logtmbuf, timestamp.millitm, 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,
					pClient->m_socket,
					13, 10);
				fclose(logfp);
			}
}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");

//		AfxMessageBox("end thread");
}

#endif

void CSNMPTestDlg::OnButtonSetup() 
{
	// TODO: Add your control notification handler code here
	CSetupDialog	 dlg;

	memcpy(&dlg.m_buttonevent[0], &m_buttonevent[0], sizeof(ButtonEvent_t)*16);
	dlg.m_nButtonPressDelay  = m_nButtonPressDelay;

	if(dlg.DoModal() == IDOK)
	{

		memcpy(&m_buttonevent[0], &dlg.m_buttonevent[0], sizeof(ButtonEvent_t)*16);
		m_nButtonPressDelay = dlg.m_nButtonPressDelay;
		int i=0;
		SetButtonText();

/*		if(m_s)  // cant remember why I did this but it causes issues, so remove
		{
			OnButtonConnect(); // disconnect
			OnButtonConnect(); // reconnect
		}
		*/
	}
}

void CSNMPTestDlg::OnCheckLogxact() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	
}

void CSNMPTestDlg::OnItemchangedList3(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	OnDblclkList3(pNMHDR, pResult);
	
	*pResult = 0;
}


/*
	Notification Messages from Buttons

When the user clicks a button, its state changes, and the button sends notification codes, in the form of WM_COMMAND messages, to its parent window. 
For example, a push button control sends the BN_CLICKED notification code whenever the user chooses the button. 
In all cases (except for BCN_HOTITEMCHANGE), the low-order word of the wParam parameter contains the control identifier, 
the high-order word of wParam contains the notification code, and the lParam parameter contains the control window handle.

	typedef struct tagMSG {
    HWND        hwnd;
    UINT        message;
    WPARAM      wParam;
    LPARAM      lParam;
    DWORD       time;
    POINT       pt;
#ifdef _MAC
    DWORD       lPrivate;
#endif
} MSG, *PMSG, NEAR *NPMSG, FAR *LPMSG;
*/

LRESULT CSNMPTestDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
//	if((message == WM_COLORBUTTONUP)||(message == WM_COLORBUTTONUP))
//	{
//		if(HIWORD(wParam) == BN_PUSHED)
		if(message == WM_COLORBUTTONDOWN)
		{

/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("DOWN message 0x%08x wparam 0x%08x lparam 0x%08x", message, wParam, lParam);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}
*/
			ButtonPush(wParam, lParam);
		}
		else 
		if(message == WM_COLORBUTTONUP)
	 //if(HIWORD(wParam) == BN_UNPUSHED)
		{
/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("UP message 0x%08x wparam 0x%08x lparam 0x%08x", message, wParam, lParam);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}
*/
			ButtonRelease(wParam, lParam);
		}
//	}
	
	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CSNMPTestDlg::ButtonPush(WPARAM wParam, LPARAM lParam) 
{
	// identify correct button.
/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("INIT PUSH wparam 0x%08x lparam 0x%08x", wParam, lParam);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}

*/
	int nNum=0;
	while(nNum<16)
	{
		if(LPARAM(m_button[nNum].m_hWnd) == lParam)
		{
			break;
		}

		nNum++;
	}
	if(nNum>=16) return;

	nNum++; // 1  based

			/*FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("CONT PUSH wparam 0x%08x lparam 0x%08x = BUTTON %d", wParam, lParam,nNum);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}
*/
	// if this is being hit, then definitely the thing thing this thinged, and we have an keypress object in there already.
	// just need to update the OID and the value and send it.

	// we are going to make it a toggle for now.

	// give an indication that the button is being pressed

	m_button[nNum-1].SetDisabledColor((m_bTallyState[nNum-1]?0x00227766:0x00005555));
	m_button[nNum-1].SetBGColor((m_bTallyState[nNum-1]?0x0044ddbb:0x0000ffff), TRUE);

//	MSG msg;
//	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
//		AfxGetApp()->PumpMessage();



	CSNMPMessage* pMessage = ((CSNMPMessage*)m_pvMessage);


	if(pMessage->m_pchOID)
	{
		free(pMessage->m_pchOID);
	}
	char buffer[256];
	sprintf(buffer, "1.3.6.1.4.1.21541.%d.%d.0", (m_Do_GPO?2:6), (m_buttonevent[nNum-1].index>0)?(int)m_buttonevent[nNum-1].index:nNum);


	pMessage->m_pchOID = (char*)malloc(strlen(buffer)+1);
	if(pMessage->m_pchOID)
	{
		strcpy(pMessage->m_pchOID, buffer);
	}

	// change the value.
	if(pMessage->m_pucValue)
	{
		free(pMessage->m_pucValue);
	}
	unsigned char* pVal = (unsigned char*)malloc(2);
	if(pVal)
	{
/* // this was before, i was toggling the states.  at this point we just want =1, key press down.  keypress up comes after this.
		if(m_bTallyState[nNum-1])
		{
			pVal[0] = 0;
		}
		else
*/
		{
			pVal[0] = 1;
		}
		pMessage->m_ulValueLength=1;
	}
	pMessage->m_pucValue = pVal; // even if null

	pMessage->m_ulRequest =  u.m_ucLastMessageNumber+1; // expected value!
	if(pMessage->m_ulRequest>=254) pMessage->m_ulRequest=0; // loop around. but keep value 255 separate.  it is reserved

	// update the window, then send

	char* pch = NULL;
	unsigned char* pchBuf = NULL;
//	AfxMessageBox("here");

	pchBuf = u.ReturnMessageBuffer(pMessage);
	if(pchBuf)
	{
//	CString Q;
//	Q.Format("[%s] %08x", pchBuf, pchBuf);
//	AfxMessageBox(Q);
//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

		unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
		ulLen += u.ReturnLength(ulLen)+1;

//	Q.Format("buflen %d", ulLen);
//	AfxMessageBox(Q);

		unsigned long ulHdr = ulLen;
		unsigned long ulReadable = 0;

		char* pchReadable = (char*)u.ReturnReadableMessageBuffer(pMessage);
		char* pchHdr = gbu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM64BIT);

		if(pchReadable) ulReadable = strlen(pchReadable);

		pch = (char*)malloc(ulReadable + ulHdr +3); //term 0
		if(pch)
		{
			memcpy(pch, pchReadable, ulReadable);
			memset(pch+ulReadable, 13, 1);
			memset(pch+ulReadable+1, 10, 1);
			memcpy(pch+ulReadable+2, pchHdr, ulHdr);
			*(pch+ulReadable + ulHdr +2) = 0;
//AfxMessageBox(pch);

		}
		if(pchReadable) free(pchReadable);
		if(pchHdr) free(pchHdr);

	}
	
//AfxMessageBox("delete buf");
	if(pchBuf) free(pchBuf);
	pchBuf = NULL;
//AfxMessageBox("deleted buf");


	if(pch)
	{
		//now fill the text box
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(pch);
//AfxMessageBox("delete other buf");
		free(pch); // used malloc
	}
	else
	{
		GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message buffer!");
	}

	OnButtonSend();  // send the key press.

	Sleep(m_nButtonPressDelay); //delay!

}

void CSNMPTestDlg::ButtonRelease(WPARAM wParam, LPARAM lParam) 
{
	// identify correct button.
/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("INIT UP wparam 0x%08x lparam 0x%08x", wParam, lParam);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}

*/
	int nNum=0;
	while(nNum<16)
	{
		if(LPARAM(m_button[nNum].m_hWnd) == lParam)
		{
/*
		//	AfxMessageBox("found");
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("FOUND UP wparam 0x%08x lparam 0x%08x = BUTTON %d", wParam, lParam,nNum+1);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}
*/
			break;
		}
		nNum++;
	}

	if(nNum>=16) return;
	
	nNum++; // 1  based

	if(m_s) // only do this if connected, if not connected we got a msg box on the keypress down.
	{

	/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("CONT UP wparam 0x%08x lparam 0x%08x = BUTTON %d", wParam, lParam,nNum);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}

	*/
	//	Sleep(m_nButtonPressDelay); //delay!
		// now, need to send the "unpress" to indicate that the button is now "up" again.

		CSNMPMessage* pMessage = ((CSNMPMessage*)m_pvMessage);


		if(pMessage->m_pchOID)
		{
			free(pMessage->m_pchOID);
		}
		char buffer[256];
		sprintf(buffer, "1.3.6.1.4.1.21541.%d.%d.0", (m_Do_GPO?2:6), (m_buttonevent[nNum-1].index>0)?(int)m_buttonevent[nNum-1].index:nNum);


		pMessage->m_pchOID = (char*)malloc(strlen(buffer)+1);
		if(pMessage->m_pchOID)
		{
			strcpy(pMessage->m_pchOID, buffer);
		}

		// change the value.
		if(pMessage->m_pucValue)
		{
			free(pMessage->m_pucValue);
		}
		unsigned char* pVal = (unsigned char*)malloc(2);
		if(pVal)
		{
			pVal[0] = 0;
			pMessage->m_ulValueLength=1;
		}
		pMessage->m_pucValue = pVal; // even if null

		pMessage->m_ulRequest =  u.m_ucLastMessageNumber+1; // expected value!
		if(pMessage->m_ulRequest>=254) pMessage->m_ulRequest=0; // loop around. but keep value 255 separate.  it is reserved

		// update the window, then send

		char* pch = NULL;
		unsigned char* pchBuf = NULL;
	//	AfxMessageBox("here");

		pchBuf = u.ReturnMessageBuffer(pMessage);
		if(pchBuf)
		{
	//	CString Q;
	//	Q.Format("[%s] %08x", pchBuf, pchBuf);
	//	AfxMessageBox(Q);
	//	AfxMessageBox(((CSNMPMessage*)pm)->m_pchOID);

			unsigned long ulLen = u.ReturnLength(&(pchBuf[1])); // length is just of the thing.
			ulLen += u.ReturnLength(ulLen)+1;

	//	Q.Format("buflen %d", ulLen);
	//	AfxMessageBox(Q);

			unsigned long ulHdr = ulLen;
			unsigned long ulReadable = 0;

			char* pchReadable = (char*)u.ReturnReadableMessageBuffer(pMessage);
			char* pchHdr = gbu.ReadableHex((char*)pchBuf, &ulHdr, MODE_DELIM64BIT);

			if(pchReadable) ulReadable = strlen(pchReadable);

			pch = (char*)malloc(ulReadable + ulHdr +3); //term 0
			if(pch)
			{
				memcpy(pch, pchReadable, ulReadable);
				memset(pch+ulReadable, 13, 1);
				memset(pch+ulReadable+1, 10, 1);
				memcpy(pch+ulReadable+2, pchHdr, ulHdr);
				*(pch+ulReadable + ulHdr +2) = 0;
	//AfxMessageBox(pch);

			}
			if(pchReadable) free(pchReadable);
			if(pchHdr) free(pchHdr);

		}
		
	//AfxMessageBox("delete buf");
		if(pchBuf) free(pchBuf);
		pchBuf = NULL;
	//AfxMessageBox("deleted buf");


		if(pch)
		{
			//now fill the text box
			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(pch);
	//AfxMessageBox("delete other buf");
			free(pch); // used malloc
		}
		else
		{
			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText("Error creating message buffer!");
		}

		OnButtonSend();  // send the key un-press.
	}
	// set the color back.
	m_button[nNum-1].SetDisabledColor((m_bTallyState[nNum-1]?0x00005500:GetSysColor(COLOR_BTNFACE)));
	m_button[nNum-1].SetBGColor((m_bTallyState[nNum-1]?0x0000ff00:GetSysColor(COLOR_BTNFACE)), TRUE);

			/*FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
		//	if(HIWORD(wParam) == BN_PUSHED)
				CString Q; Q.Format("FIN UP wparam 0x%08x lparam 0x%08x = BUTTON %d", wParam, lParam, nNum);
	//			GetDlgItem(IDC_EDIT_COMMAND)->SetWindowText(Q);
		//		AfxMessageBox(Q);
			
				fprintf(fp,"%s\r\n", Q);
				
					fclose(fp);
			}\*/

//	MSG msg;
//	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
//		AfxGetApp()->PumpMessage();

}

void CSNMPTestDlg::DoButtons()
{
	bool bDo = FALSE;
	if(m_s)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect host");
		GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(FALSE);

		CComboBox* p =((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND));

		int s = p->GetCurSel();

		if(s>=0) 
		{
			int len = p->GetLBTextLen( s );
			p->GetLBText( s, m_szCmdName.GetBuffer(len) );
			m_szCmdName.ReleaseBuffer();
		}

		int n = ((CComboBox*)GetDlgItem(IDC_COMBO_COMMAND))->FindStringExact( -1, m_szCmdName );

		if((m_szCmdName.GetLength()>0)&&(n!=CB_ERR))
		{

			if(m_szCmdName.Compare("keypress")==0)
			{

	//AfxMessageBox("HERE");
				bDo = TRUE;
				GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);

			}
		}
	}
	else
	{

		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect to host");
		GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_RECVTM)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_SENDTM)->EnableWindow(TRUE);

		m_bPrettyLights = FALSE;
		((CButton*)GetDlgItem(IDC_CHECK_PRETTYLIGHTS))->SetCheck(FALSE);


	}
	if(bDo)
	{
		if(!GetDlgItem(IDC_BUTTON1)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON2)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON2)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON3)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON3)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON4)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON4)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON5)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON5)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON6)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON6)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON7)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON7)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON8)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON8)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON9)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON9)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON10)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON10)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON11)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON11)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON12)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON12)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON13)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON13)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON14)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON14)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON15)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON15)->EnableWindow(TRUE);
		if(!GetDlgItem(IDC_BUTTON16)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON16)->EnableWindow(TRUE);
//		if(GetDlgItem(IDC_BUTTON_SEND)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(FALSE);

		if(!GetDlgItem(IDC_CHECK_PRETTYLIGHTS)->IsWindowEnabled()) GetDlgItem(IDC_CHECK_PRETTYLIGHTS)->EnableWindow(TRUE);
		
	}
	else
	{
		if(GetDlgItem(IDC_BUTTON1)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON2)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON2)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON3)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON4)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON4)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON5)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON5)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON6)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON6)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON7)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON7)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON8)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON8)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON9)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON9)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON10)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON10)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON11)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON11)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON12)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON12)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON13)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON13)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON14)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON14)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON15)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON15)->EnableWindow(FALSE);
		if(GetDlgItem(IDC_BUTTON16)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON16)->EnableWindow(FALSE);
//		if(!GetDlgItem(IDC_BUTTON_SEND)->IsWindowEnabled()) GetDlgItem(IDC_BUTTON_SEND)->EnableWindow(TRUE);

		if(GetDlgItem(IDC_CHECK_PRETTYLIGHTS)->IsWindowEnabled()) GetDlgItem(IDC_CHECK_PRETTYLIGHTS)->EnableWindow(FALSE);

	}

}

void CSNMPTestDlg::OnCheckPrettylights() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_bPrettyLights)
	{
		SetTimer(69, m_nButtonPressDelay, NULL);
	}
	else
	{
		KillTimer(69);
	}
	
}

void CSNMPTestDlg::OnCheckDoGpo() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

}
