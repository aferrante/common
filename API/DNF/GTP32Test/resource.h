//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SNMPTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   128
#define IDD_SNMPTEST_DIALOG             130
#define IDD_SETUP_DIALOG                131
#define IDD_BUTTON_DIALOG               132
#define IDC_BUTTON_CONNECT              1001
#define IDC_EDIT_COMMAND                1002
#define IDC_BUTTON_SEND                 1003
#define IDC_EDIT_ESCIN                  1004
#define IDC_BUTTON_CONNECT2             1004
#define IDC_EDIT_ESCOUT                 1005
#define IDC_COMBO_HOST                  1006
#define IDC_BUTTON_ESC                  1007
#define IDC_COMBO_PORT                  1008
#define IDC_COMBO_SCENE                 1009
#define IDC_BUTTON_OP                   1010
#define IDC_BUTTON_DELIM                1011
#define IDC_BUTTON_LO                   1012
#define IDC_BUTTON_PL                   1013
#define IDC_BUTTON_ST                   1014
#define IDC_BUTTON_CL                   1015
#define IDC_BUTTON_AN                   1016
#define IDC_COMBO_DELIM                 1017
#define IDC_BUTTON_UP                   1018
#define IDC_LIST1                       1019
#define IDC_BUTTON_AV                   1020
#define IDC_BUTTON_HEX                  1021
#define IDC_LIST2                       1022
#define IDC_LIST3                       1023
#define IDC_COMBO_FIELD                 1024
#define IDC_COMBO_VALUE                 1025
#define IDC_CHECK_SENDQ                 1026
#define IDC_CHECK_SENDQ2                1027
#define IDC_CHECK_SEND                  1028
#define IDC_COMBO_HEX                   1029
#define IDC_EDIT_DSN                    1030
#define IDC_EDIT_USER                   1031
#define IDC_EDIT_PW                     1032
#define IDC_EDIT_RECVTM                 1033
#define IDC_CHECK_UTF8                  1034
#define IDC_EDIT_SENDTM                 1035
#define IDC_COMBO_ALIAS                 1036
#define IDC_CHECK_LYRIC                 1037
#define IDC_BUTTON_POLL                 1037
#define IDC_CHECK_STAR                  1038
#define IDC_COMBO_BUFFER                1039
#define IDC_STATIC_ALIAS                1040
#define IDC_STATIC_FB                   1041
#define IDC_BUTTON_RECV                 1042
#define IDC_RADIO_DNF                   1043
#define IDC_RADIO_LYR                   1044
#define IDC_RADIO_ICON                  1045
#define IDC_STATIC_SCENE                1046
#define IDC_STATIC_DELIM                1047
#define IDC_BUTTON_QS                   1048
#define IDC_BUTTON_QS2                  1049
#define IDC_STATIC_FIELDS               1050
#define IDC_BUTTON_TH                   1051
#define IDC_RADIO_G7                    1052
#define IDC_CHECK_THUMB_CP              1053
#define IDC_CHECK_LOGXACT               1054
#define IDC_BUTTON_VIEWLOG              1055
#define IDC_COMBO_COMMAND               1056
#define IDC_STATIC_TIMEOUT              1058
#define IDC_CHECK_KEEP_ALIVE            1059
#define IDC_EDIT_ID                     1060
#define IDC_EDIT_DESC                   1061
#define IDC_STATIC_DESC                 1062
#define IDC_STATIC_ID                   1063
#define IDC_STATIC_START                1064
#define IDC_EDIT_INTERVAL               1064
#define IDC_EDIT_START                  1065
#define IDC_BUTTON1                     1065
#define IDC_STATIC_DUR                  1066
#define IDC_STATIC_BUTTONFRAME          1066
#define IDC_EDIT_DUR                    1067
#define IDC_BUTTON2                     1067
#define IDC_STATIC_ID2                  1068
#define IDC_BUTTON3                     1068
#define IDC_EDIT_ID2                    1069
#define IDC_BUTTON4                     1069
#define IDC_STATIC_DESC2                1070
#define IDC_BUTTON5                     1070
#define IDC_EDIT_DESC2                  1071
#define IDC_BUTTON6                     1071
#define IDC_STATIC_START2               1072
#define IDC_BUTTON7                     1072
#define IDC_EDIT_START2                 1073
#define IDC_BUTTON8                     1073
#define IDC_STATIC_DUR2                 1074
#define IDC_BUTTON9                     1074
#define IDC_EDIT_DUR2                   1075
#define IDC_BUTTON10                    1075
#define IDC_BUTTON11                    1076
#define IDC_BUTTON12                    1077
#define IDC_BUTTON13                    1078
#define IDC_BUTTON14                    1079
#define IDC_BUTTON15                    1080
#define IDC_BUTTON16                    1081
#define IDC_EDIT_COMMUNITY              1082
#define IDC_BUTTON_SETUP                1083
#define IDC_EDIT1                       1084
#define IDC_EDIT2                       1085
#define IDC_RADIO1                      1086
#define IDC_RADIO2                      1087
#define IDC_BUTTON17                    1087
#define IDC_CHECK_PRETTYLIGHTS          1088
#define IDC_CHECK_DO_GPO                1089
#define IDC_EDIT3                       1091
#define IDC_CHECK1                      1092

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1093
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
