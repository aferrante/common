// EventProperties.cpp : implementation file
//

#include "stdafx.h"
#include "HarrisAPITester.h"
#include "EventProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEventProperties dialog


CEventProperties::CEventProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CEventProperties::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEventProperties)
	m_szEventControl = _T("0007");
	m_szDuration = _T("00:00:00.00");
	m_szID = _T("");
	m_szStatus = _T("0000");
	m_szTime = _T("00:00:00.00");
	m_szTitle = _T("");
	m_szType = _T("0000");
	//}}AFX_DATA_INIT
}


void CEventProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEventProperties)
	DDX_Text(pDX, IDC_EDIT_CTRL, m_szEventControl);
	DDX_Text(pDX, IDC_EDIT_DUR, m_szDuration);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDX_Text(pDX, IDC_EDIT_STATUS, m_szStatus);
	DDX_Text(pDX, IDC_EDIT_TIME, m_szTime);
	DDX_Text(pDX, IDC_EDIT_TITLE, m_szTitle);
	DDX_Text(pDX, IDC_EDIT_TYPE, m_szType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEventProperties, CDialog)
	//{{AFX_MSG_MAP(CEventProperties)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventProperties message handlers
