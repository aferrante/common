#if !defined(AFX_EVENTPROPERTIES_H__054A4E91_DD10_41E0_BE6F_69009108D005__INCLUDED_)
#define AFX_EVENTPROPERTIES_H__054A4E91_DD10_41E0_BE6F_69009108D005__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EventProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEventProperties dialog

class CEventProperties : public CDialog
{
// Construction
public:
	CEventProperties(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEventProperties)
	enum { IDD = IDD_EVENT_DIALOG };
	CString	m_szEventControl;
	CString	m_szDuration;
	CString	m_szID;
	CString	m_szStatus;
	CString	m_szTime;
	CString	m_szTitle;
	CString	m_szType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventProperties)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEventProperties)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTPROPERTIES_H__054A4E91_DD10_41E0_BE6F_69009108D005__INCLUDED_)
