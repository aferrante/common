// HarrisAPITesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HarrisAPITester.h"
#include "HarrisAPITesterDlg.h"
#include "../ADC.h"
#include "..\..\..\TXT\BufferUtil.h"

#include "EventProperties.h"
// the following line forces a link with the winsock lib
#pragma comment(lib, "ws2_32.lib")
#include <winsock2.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHarrisAPITesterDlg dialog

CHarrisAPITesterDlg::CHarrisAPITesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHarrisAPITesterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHarrisAPITesterDlg)
	m_szServer = _T("MAIN-DS");
	m_szClient = _T("VDS1");
	m_nNumEvents = 10;
	m_nListNum = 1;
	m_nIndex = 0;
	m_bFile = FALSE;
	m_szFile = _T("NOVEMBER 1.lst");
	m_bStreamToFile = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nSelIdx = -1;
	m_bConnect = TRUE;
	m_pszCompleteHost=NULL;
	m_pszHost=NULL;
}

void CHarrisAPITesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHarrisAPITesterDlg)
	DDX_Control(pDX, IDC_LIST_DATA, m_lcData);
	DDX_Control(pDX, IDC_LIST1, m_lcConn);
	DDX_Text(pDX, IDC_EDIT_SERVER, m_szServer);
	DDX_Text(pDX, IDC_EDIT_CLIENT, m_szClient);
	DDX_Text(pDX, IDC_EDIT_NUM, m_nNumEvents);
	DDX_Text(pDX, IDC_EDIT_LISTNUM, m_nListNum);
	DDX_Text(pDX, IDC_EDIT_IDX, m_nIndex);
	DDX_Check(pDX, IDC_CHECK_FILE, m_bFile);
	DDX_Text(pDX, IDC_EDIT_FILE, m_szFile);
	DDX_Check(pDX, IDC_CHECK_STREAM, m_bStreamToFile);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CHarrisAPITesterDlg, CDialog)
	//{{AFX_MSG_MAP(CHarrisAPITesterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(NM_CLICK, IDC_LIST1, OnClickList1)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST1, OnKillfocusList1)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_GETEVENTS, OnButtonGetevents)
	ON_EN_SETFOCUS(IDC_EDIT_SERVER, OnSetfocusEditServer)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_FILE, OnCheckFile)
	ON_BN_CLICKED(IDC_BUTTON_INSEVENTS, OnButtonInsevents)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	ON_BN_CLICKED(IDC_BUTTON_GETEVENTS2, OnButtonGetevents2)
	ON_BN_CLICKED(IDC_BUTTON_DELEVENTS, OnButtonDelevents)
	ON_BN_CLICKED(IDC_BUTTON_SAVEEVENTS, OnButtonSaveevents)
	ON_BN_CLICKED(IDC_BUTTON_SAVEEVENTS2, OnButtonSaveevents2)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, OnButtonModify)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHarrisAPITesterDlg message handlers

BOOL CHarrisAPITesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	CRect rc;
	m_lcConn.GetWindowRect(&rc);
	m_lcConn.InsertColumn(0, "Connection", LVCFMT_LEFT, rc.Width()-5, 0);
	m_lcData.GetWindowRect(&rc);
	m_lcData.InsertColumn(0, "List Data", LVCFMT_LEFT, rc.Width()-5, 0);

	SetTimer(27, 33, NULL);

#ifdef CLIENT_UI
	GetDlgItem(IDC_BUTTON_GETEVENTS)->SetWindowText("Get top 10 events");
#endif

	m_szClient = _T("VDS1");

	WSADATA wd;
	int nError = WSAStartup(MAKEWORD(2,2), &wd);

	if(nError ==0)
	{
		if(GetHost()==0)
		{
			CString szHost;
			szHost.Format("%s", m_pszHost);
			m_szClient.Format("%sCL%d", szHost.Left(4), (clock()%10) );
		}
	}
	UpdateData(FALSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}


int	CHarrisAPITesterDlg::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}
							return 0;
						}
					}
				}
			}
		}
	}//else AfxMessageBox("socket err");


	return -1;
}

void CHarrisAPITesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHarrisAPITesterDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHarrisAPITesterDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CHarrisAPITesterDlg::OnCancel() 
{
	KillTimer(27);

  CDialog::OnCancel();
}

void CHarrisAPITesterDlg::OnOK() 
{
//CDialog::OnOK();
}

void CHarrisAPITesterDlg::OnClickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nindex = m_lcConn.GetNextItem(-1, LVNI_FOCUSED|LVNI_SELECTED);
	if(nindex>=0)
	{
		m_nSelIdx = nindex;
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
		m_bConnect = FALSE;
		CString szSel = m_lcConn.GetItemText(nindex, 0);
		int nidx = szSel.Find(" (");
		if (nidx>0) szSel = szSel.Left(nidx);
		CString szText;
		szText.Format("Selected server: [%s]", szSel);
		GetDlgItem(IDC_STATIC_SEL)->SetWindowText(szText);
	}
	
	*pResult = 0;
}

void CHarrisAPITesterDlg::OnKillfocusList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nindex = m_lcConn.GetNextItem(-1, LVNI_FOCUSED|LVNI_SELECTED);
	if(nindex<0)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		m_bConnect = TRUE;
	}
	*pResult = 0;
}

void CHarrisAPITesterDlg::OnButtonConnect() 
{
	if(m_bConnect)
	{
		// do a connection.
		UpdateData(TRUE);
		char server[32];
		sprintf(server,"%s",m_szServer);
		char client[32];
		sprintf(client,"%s",m_szClient);

		CAConnection* pconn = m_adc.ConnectServer(server, client);

		if(pconn==NULL)
		{
			AfxMessageBox("Could not establish connection.");
		}
		else
		{
		}

		Display();

	}
	else
	{
		// get the selected thing and disconnect.
		if(m_nSelIdx>=0) 
		{
			char server[32];
			CString szText = m_lcConn.GetItemText(m_nSelIdx, 0);
			int nidx = szText.Find(" (");
			if (nidx>0) szText = szText.Left(nidx);
			sprintf(server,"%s",szText);

			AfxMessageBox("Disconnecting " + szText);
			if(m_adc.DisconnectServer(server)<ADC_SUCCESS)
				AfxMessageBox("error disconnecting");

			m_nSelIdx = -1; 
			m_lcConn.DeleteAllItems();  //reset
			Display();
			GetDlgItem(IDC_STATIC_SEL)->SetWindowText("no selection");
		}
	}
}

void CHarrisAPITesterDlg::Display() 
{
	// TODO: Add your control notification handler code here

	int n = m_lcConn.GetItemCount();
//	m_lcConn.DeleteAllItems();
	if((m_adc.m_ppConn)&&(m_adc.m_nNumConn))  // pointer to an array of pointers to connection types
	{
		unsigned short usIndex=0;
		for(usIndex=0; usIndex<m_adc.m_nNumConn; usIndex++)
		{
			if(m_adc.m_ppConn[usIndex])
			{
				m_adc.GetStatusData(m_adc.m_ppConn[usIndex]); // get status data
			}

			CString szItem;
			unsigned char h,m,s,f;
			m_adc.ConvertMillisecondsToHMSF(
				m_adc.m_ppConn[usIndex]->m_ulRefTimeMS,
				&h,&m,&s,&f, 
				m_adc.m_ppConn[usIndex]->m_SysData.systemfrx), // pass in the systemfrx from the system data

#ifdef CLIENT_UI

			szItem.Format("%s (%d) %02d:%02d:%02d.%02d", 
				((m_adc.m_ppConn[usIndex])&&(m_adc.m_ppConn[usIndex]->m_pszServerName))?m_adc.m_ppConn[usIndex]->m_pszServerName:"N/A",
				(m_adc.m_ppConn[usIndex])?m_adc.m_ppConn[usIndex]->m_uchHandle:-1,
				m_adc.m_ppConn[usIndex]->m_SysData.refhour,
				m_adc.m_ppConn[usIndex]->m_SysData.refmin,
				m_adc.m_ppConn[usIndex]->m_SysData.refsec,
				m_adc.m_ppConn[usIndex]->m_SysData.refframe

				);
#else
			szItem.Format("%s (%d) %d=%02d:%02d:%02d.%02d(0x%02x) = %02d:%02d:%02d.%02d (j%d = u%d)", 
				((m_adc.m_ppConn[usIndex])&&(m_adc.m_ppConn[usIndex]->m_pszServerName))?m_adc.m_ppConn[usIndex]->m_pszServerName:"N/A",
				(m_adc.m_ppConn[usIndex])?m_adc.m_ppConn[usIndex]->m_uchHandle:-1,
				m_adc.m_ppConn[usIndex]->m_ulRefTimeMS,
				m_adc.m_ppConn[usIndex]->m_SysData.refhour,
				m_adc.m_ppConn[usIndex]->m_SysData.refmin,
				m_adc.m_ppConn[usIndex]->m_SysData.refsec,
				m_adc.m_ppConn[usIndex]->m_SysData.refframe,
				m_adc.m_ppConn[usIndex]->m_SysData.systemfrx,
				h,m,s,f,
				m_adc.m_ppConn[usIndex]->m_SysData.systemdate,
				(m_adc.m_ppConn[usIndex])?m_adc.m_ppConn[usIndex]->m_ulRefUnixTime:-1
				);

#endif

			if(usIndex<n)
			{
				m_lcConn.SetItemText(usIndex, 0, szItem);
			}
			else
			{
				m_lcConn.InsertItem(m_lcConn.GetItemCount(), szItem);
			}
		}
		while(m_lcConn.GetItemCount()>m_adc.m_nNumConn)
		{
			m_lcConn.DeleteItem(m_lcConn.GetItemCount()-1);
		}
	}
}

void CHarrisAPITesterDlg::OnSetfocusEditServer() 
{
	GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
	m_bConnect = TRUE;
}

void CHarrisAPITesterDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==27)
	{
		Display();
	} 
	else
		CDialog::OnTimer(nIDEvent);
}

void CHarrisAPITesterDlg::OnButtonGetevents() 
{
	// TODO: Add your control notification handler code here

	if(0)//m_bFile)
	{
		if(!UpdateData(TRUE))return; // get filename
		if(m_szFile.GetLength()<=0) 
		{
			AfxMessageBox("You must specify a filename");
			return;
		}

		CString szItem; 
		szItem.Format("Getting %d events from file %s, starting at position %d",
			m_nNumEvents,
			m_szFile,
			m_nIndex+1 //1 based index
			);

		m_lcData.DeleteAllItems();  //reset
		
		m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);

		FILE* fp;
		fp=fopen(m_szFile, "rb");
		if(!fp)
		{
			AfxMessageBox("The file could not be opened.");
			return;
		}

		fseek(fp,0,SEEK_END);
		unsigned long ulLen = ftell(fp);
		fseek(fp,0,SEEK_SET);

		// allocate buffer and read in file contents
		char* pch = (char*) malloc(ulLen+1); // for 0
		if(pch!=NULL)
		{
			fread(pch,sizeof(char),ulLen,fp);
			memset(pch+ulLen, 0, 1);

			CAList list;

			int nEvents = m_adc.CountEventsInStream((unsigned char*)pch, ulLen);

			if(nEvents>0)
			{
				EnterCriticalSection(&list.m_crit);  //  critical section.
				list.EnsureAllocated(nEvents); // should check ADC_SUCCESS, but for now we assume success

				int nParsedItems = m_adc.StreamToArray(&list, 0, (unsigned char*) pch, 0x29, nEvents);
				LeaveCriticalSection(&list.m_crit);  //  critical section.

				if(nParsedItems<0)
				{
				AfxMessageBox("An error occurred parsing the file");
				}
				else
				{

	//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
					szItem.Format("%d events were in the file%s",nParsedItems, nParsedItems>0?":":".");
			
					m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
	//				szItem.Format("Received %d events, event count=%d, buffersize=%d",nParsedItems,m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount, m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventBufferSize);
	//		AfxMessageBox(szItem);

					if(nParsedItems)
					{
						list.m_nEventCount = nParsedItems; 
						for(int i=m_nIndex; i<min(list.m_nEventCount, m_nIndex+m_nNumEvents) ; i++)
						{
							unsigned char h,m,s,f, dh,dm,ds,df;
							CString szOnAirTime;
							CString szDuration;

							m_adc.ConvertMillisecondsToHMSF(
								list.m_ppEvents[i]->m_ulOnAirTimeMS,
								&h,&m,&s,&f, 
								0x29); // pass in the systemfrx from the system data
							m_adc.ConvertMillisecondsToHMSF(
								list.m_ppEvents[i]->m_ulDurationMS,
								&dh,&dm,&ds,&df, 
								0x29); // pass in the systemfrx from the system data

							if(list.m_ppEvents[i]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)
								szOnAirTime.Format("__:__:__.__");
							else
								szOnAirTime.Format("%02d:%02d:%02d.%02d",h,m,s,f);

							if(list.m_ppEvents[i]->m_ulDurationMS==TIME_NOT_DEFINED)
								szDuration.Format("__:__:__.__");
							else
								szDuration.Format("%02d:%02d:%02d.%02d",dh,dm,ds,df);
	#ifdef CLIENT_UI
							szItem.Format("%02d [%s][dur %s] [%s] [%s]", 
								i+1, //1 based index
								szOnAirTime,
								szDuration,
								list.m_ppEvents[i]->m_pszID?list.m_ppEvents[i]->m_pszID:"",
								list.m_ppEvents[i]->m_pszTitle?list.m_ppEvents[i]->m_pszTitle:""
								);

	#else
							szItem.Format("%03d: [on air %s][dur %s][t0x%04x|0x%04x][s0x%04x][ID:%s][%s][seg0x%02x][j%d]", 
								i+1, //1 based index
								szOnAirTime,
								szDuration,
								list.m_ppEvents[i]->m_usType,
								list.m_ppEvents[i]->m_ulControl,
								list.m_ppEvents[i]->m_usStatus,

								list.m_ppEvents[i]->m_pszID?list.m_ppEvents[i]->m_pszID:"",
								list.m_ppEvents[i]->m_pszTitle?list.m_ppEvents[i]->m_pszTitle:"",
								list.m_ppEvents[i]->m_ucSegment,
								list.m_ppEvents[i]->m_usOnAirJulianDate
								);
	#endif
							m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
						}
					}
				}
			}
			else
			{
				AfxMessageBox("No events were found in the file");
			}




			free(pch);

		}
		fclose(fp);
	}
	else
	if(m_nSelIdx>=0)
	{
		if(!UpdateData(TRUE))return;
		m_szFile.TrimLeft(); m_szFile.TrimRight();

//		char server[32];
		CString szText = m_lcConn.GetItemText(m_nSelIdx, 0);
		int nidx = szText.Find(" (");
		if (nidx>0) szText = szText.Left(nidx);

		CString szItem; 
		szItem.Format("Getting %d events from %s, list %d, starting at position %d",
			m_nNumEvents,
			szText,
			m_nListNum,
			m_nIndex+1 //1 based index
			);

		m_lcData.DeleteAllItems();  //reset
		


		m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);


//		AfxMessageBox(szItem);

		if(m_adc.m_ppConn[m_nSelIdx])
		{
			EnterCriticalSection(&m_adc.m_crit);

			if((m_bStreamToFile)&&(m_szFile.GetLength())	)
			{
				if(m_adc.m_pchDebugFilename) free(m_adc.m_pchDebugFilename);
				m_adc.m_pchDebugFilename = (char*)malloc(m_szFile.GetLength()+1);
				if(m_adc.m_pchDebugFilename) strcpy(m_adc.m_pchDebugFilename, m_szFile.GetBuffer(0));
			}
			int numevents = m_adc.GetEvents(m_adc.m_ppConn[m_nSelIdx],
				m_nListNum, 
				m_nIndex, 
				m_nNumEvents,
				ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_FULLLIST);
			LeaveCriticalSection(&m_adc.m_crit);
			if(numevents>=ADC_SUCCESS)
			{
//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
				szItem.Format("Received %d events%s",numevents, numevents>0?":":".");
		
				m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
//				szItem.Format("Received %d events, event count=%d, buffersize=%d",numevents,m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount, m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventBufferSize);
//		AfxMessageBox(szItem);

				if(numevents)
				{
					for(int i=m_nIndex; i<m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount; i++)
					{
						unsigned char h,m,s,f, dh,dm,ds,df;
						CString szOnAirTime;
						CString szDuration;
						m_adc.ConvertMillisecondsToHMSF(
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_ulOnAirTimeMS,
							&h,&m,&s,&f, 
							m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx); // pass in the systemfrx from the system data
						m_adc.ConvertMillisecondsToHMSF(
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_ulDurationMS,
							&dh,&dm,&ds,&df, 
							m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx); // pass in the systemfrx from the system data

							if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)
								szOnAirTime.Format("__:__:__.__");
							else
								szOnAirTime.Format("%02d:%02d:%02d.%02d",h,m,s,f);

							if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_ulDurationMS==TIME_NOT_DEFINED)
								szDuration.Format("__:__:__.__");
							else
								szDuration.Format("%02d:%02d:%02d.%02d",dh,dm,ds,df);
#ifdef CLIENT_UI
							szItem.Format("%02d [%s][dur %s] [%s] [%s]", 
							i+1, //1 based index
							szOnAirTime,
							szDuration,
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszID?m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszID:"",
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszTitle?m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszTitle:""
							);

#else
							szItem.Format("%03d: [on air %s][dur %s][typ0x%04x|ctrl0x%08x][status0x%04x][ID:%s][%s][seg0x%02x][j%d][dev%04x]", 
							i+1, //1 based index
							szOnAirTime,
							szDuration,
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_usType,
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_ulControl,
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_usStatus,

							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszID?m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszID:"",
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszTitle?m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszTitle:"",
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_ucSegment,
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_usOnAirJulianDate,
							m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_usDevice

							);

						if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszData) 
						{
							szDuration.Format("[data: %s]", m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]->m_pszData);
							szItem += szDuration;
						}
#endif
						m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
					}
				}
			}
		}
	}
	else AfxMessageBox("No selection");
	
}

void CHarrisAPITesterDlg::OnCheckFile() 
{
	// TODO: Add your control notification handler code here
	
	m_bFile = ((CButton*)GetDlgItem(IDC_CHECK_FILE))->GetCheck();

	GetDlgItem(IDC_EDIT_FILE)->EnableWindow(m_bFile);

}

void CHarrisAPITesterDlg::OnButtonInsevents() 
{
	// TODO: Add your control notification handler code here

	if(1)//m_bFile)
	{
		if(!UpdateData(TRUE))return; // get filename
		if(m_szFile.GetLength()<=0) 
		{
			AfxMessageBox("You must specify a filename");
			return;
		}
	}
	else
	{
		AfxMessageBox("You must enable a file containing the events to insert, and specify a filename");
		return;
	}

	if(m_nSelIdx>=0)
	{
		if(!UpdateData(TRUE))return;

//		char server[32];
		CString szText = m_lcConn.GetItemText(m_nSelIdx, 0);
		int nidx = szText.Find(" (");
		if (nidx>0) szText = szText.Left(nidx);


		// show the file contents

		FILE* fp;
		fp=fopen(m_szFile, "rb");
		if(!fp)
		{
			AfxMessageBox("The file could not be opened.");
			return;
		}

		fseek(fp,0,SEEK_END);
		unsigned long ulLen = ftell(fp);
		fseek(fp,0,SEEK_SET);

		// allocate buffer and read in file contents
		char* pch = (char*) malloc(ulLen+1); // for 0
		if(pch!=NULL)
		{
			fread(pch,sizeof(char),ulLen,fp);
			memset(pch+ulLen, 0, 1);

			CAList list;

			int nEvents = m_adc.CountEventsInStream((unsigned char*)pch, ulLen);
			CString szItem; 
			szItem.Format("Inserting %d events into %s, list %d, starting at position %d",
				nEvents,
				szText,
				m_nListNum,
				m_nIndex+1 //1 based index
				);

			m_lcData.DeleteAllItems();  //reset
			
			m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);

			if(nEvents>0)
			{
				EnterCriticalSection(&list.m_crit);  //  critical section.
				list.EnsureAllocated(nEvents); // should check ADC_SUCCESS, but for now we assume success

				int nParsedItems = m_adc.StreamToArray(&list, 0, (unsigned char*) pch, 0x29, nEvents);
				LeaveCriticalSection(&list.m_crit);  //  critical section.

				if(nParsedItems<0)
				{
				AfxMessageBox("An error occurred parsing the file");
				}
				else
				{

	//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
					szItem.Format("%d events were in the file%s",nParsedItems, nParsedItems>0?":":".");
			
					m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
	//				szItem.Format("Received %d events, event count=%d, buffersize=%d",nParsedItems,m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount, m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventBufferSize);
	//		AfxMessageBox(szItem);

					if(nParsedItems)
					{
						list.m_nEventCount = nParsedItems; 
						for(int i=m_nIndex; i<min(list.m_nEventCount, nParsedItems) ; i++)
						{
							unsigned char h,m,s,f, dh,dm,ds,df;
							m_adc.ConvertMillisecondsToHMSF(
								list.m_ppEvents[i]->m_ulOnAirTimeMS,
								&h,&m,&s,&f, 
								0x29), // pass in the systemfrx from the system data
							m_adc.ConvertMillisecondsToHMSF(
								list.m_ppEvents[i]->m_ulDurationMS,
								&dh,&dm,&ds,&df, 
								0x29), // pass in the systemfrx from the system data

	#ifdef CLIENT_UI
								szItem.Format("%02d [%02d:%02d:%02d.%02d][dur %02d:%02d:%02d.%02d] [%s] [%s]", 
								i+1, //1 based index
								h,m,s,f,dh,dm,ds,df,
								list.m_ppEvents[i]->m_pszID?list.m_ppEvents[i]->m_pszID:"",
								list.m_ppEvents[i]->m_pszTitle?list.m_ppEvents[i]->m_pszTitle:""
								);

	#else
								szItem.Format("%03d: [on air %02d:%02d:%02d.%02d][dur %02d:%02d:%02d.%02d][t0x%04x][s0x%04x][ID:%s][%s][seg0x%02x][j%d]", 
								i+1, //1 based index
								h,m,s,f,dh,dm,ds,df,
								list.m_ppEvents[i]->m_usType,
								list.m_ppEvents[i]->m_usStatus,

								list.m_ppEvents[i]->m_pszID?list.m_ppEvents[i]->m_pszID:"",
								list.m_ppEvents[i]->m_pszTitle?list.m_ppEvents[i]->m_pszTitle:"",
								list.m_ppEvents[i]->m_ucSegment,
								list.m_ppEvents[i]->m_usOnAirJulianDate
								);
	#endif
							m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
						}
					}
				}
			}
			else
			{
				AfxMessageBox("No events were found in the file");
			}





// m_pucEventStreamBuffer // assign this? or, assign to:
//		memcpy(&(m_SendData.thenrevent.aevent[0]), pch, ulLen);


			if((m_adc.m_ppConn[m_nSelIdx])&&(nEvents>0))
			{
//			just assign m_pucEventStreamBuffer, the insert will do the memcpy.
				m_adc.m_pucEventStreamBuffer = (unsigned char*)pch;  //a buffer for loading up a bunch of events
				m_adc.m_usStreamBufferLength = (unsigned short)ulLen;  //length of the stream events buffer

				EnterCriticalSection(&m_adc.m_crit);
				int numevents = m_adc.InsertEventAt(m_adc.m_ppConn[m_nSelIdx],
					m_nListNum, 
					m_nIndex, 
//					nEvents,
					ADC_NORMAL);
				LeaveCriticalSection(&m_adc.m_crit);
				if(numevents>=ADC_SUCCESS)
				{
	//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
					szItem.Format("Successfully inserted %d events from file %s (%d bytes).",numevents, m_szFile,ulLen);
			
					m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
				}
			}
			else
			{
				AfxMessageBox("No list or no events!");
			}

			free(pch);
			m_adc.m_pucEventStreamBuffer = NULL;  //a buffer for loading up a bunch of events
			m_adc.m_usStreamBufferLength = 0;  //length of the stream events buffer
		
		}
		fclose(fp);

	}
	else AfxMessageBox("No selection");

//		AfxMessageBox(szItem);

}

void CHarrisAPITesterDlg::OnButtonBrowse() 
{
	// TODO: Add your control notification handler code here
	CFileDialog fdlg(TRUE,
									 "lst",
									  NULL,
										OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_LONGNAMES|OFN_NOCHANGEDIR|OFN_PATHMUSTEXIST,
										"Playlist Files (*.lst;*.cur)|*.lst; *.cur|All Files (*.*)|*.*||");

	if(fdlg.DoModal()==IDOK)
	{
		m_szFile = fdlg.GetPathName();
		UpdateData(FALSE);
	}
	else //nothing
	{
	}

	
}

void CHarrisAPITesterDlg::OnButtonGetevents2() 
{
	if(1)//m_bFile)
	{
		if(!UpdateData(TRUE))return; // get filename
		if(m_szFile.GetLength()<=0) 
		{
			AfxMessageBox("You must specify a filename");
			return;
		}
	}
	else
	{
		AfxMessageBox("You must enable a file containing the events to insert, and specify a filename");
		return;
	}

	// show the file contents

	FILE* fp;
	fp=fopen(m_szFile, "rb");
	if(!fp)
	{
		AfxMessageBox("The file could not be opened.");
		return;
	}

	fseek(fp,0,SEEK_END);
	unsigned long ulLen = ftell(fp);
	fseek(fp,0,SEEK_SET);

	// allocate buffer and read in file contents
	char* pch = (char*) malloc(ulLen+1); // for 0
	if(pch!=NULL)
	{
		fread(pch,sizeof(char),ulLen,fp);
		memset(pch+ulLen, 0, 1);

		CAList list;
		CString szItem;
		int nParsedItems =0;

		int nEvents = m_adc.CountEventsInStream((unsigned char*)pch, ulLen);

		if(nEvents>0)
		{
			EnterCriticalSection(&list.m_crit);  //  critical section.
			list.EnsureAllocated(nEvents); // should check ADC_SUCCESS, but for now we assume success

			nParsedItems = m_adc.StreamToArray(&list, 0, (unsigned char*) pch, 0x29, nEvents);
			LeaveCriticalSection(&list.m_crit);  //  critical section.

			if(nParsedItems<0)
			{
			AfxMessageBox("An error occurred parsing the file");
			}
			else
			{

//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
				szItem.Format("%d events were in the file%s",nParsedItems, nParsedItems>0?":":".");
		
				m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
//				szItem.Format("Received %d events, event count=%d, buffersize=%d",nParsedItems,m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount, m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventBufferSize);
//		AfxMessageBox(szItem);

				if(nParsedItems)
				{
					list.m_nEventCount = nParsedItems; 
					for(int i=m_nIndex; i<nParsedItems ; i++)
					{
						unsigned char h,m,s,f, dh,dm,ds,df;
						m_adc.ConvertMillisecondsToHMSF(
							list.m_ppEvents[i]->m_ulOnAirTimeMS,
							&h,&m,&s,&f, 
							0x29), // pass in the systemfrx from the system data
						m_adc.ConvertMillisecondsToHMSF(
							list.m_ppEvents[i]->m_ulDurationMS,
							&dh,&dm,&ds,&df, 
							0x29), // pass in the systemfrx from the system data

#ifdef CLIENT_UI
							szItem.Format("%02d [%02d:%02d:%02d.%02d][dur %02d:%02d:%02d.%02d] [%s] [%s]", 
							i+1, //1 based index
							h,m,s,f,dh,dm,ds,df,
							list.m_ppEvents[i]->m_pszID?list.m_ppEvents[i]->m_pszID:"",
							list.m_ppEvents[i]->m_pszTitle?list.m_ppEvents[i]->m_pszTitle:""
							);

#else
							szItem.Format("%03d: [on air %02d:%02d:%02d.%02d][dur %02d:%02d:%02d.%02d][t0x%04x][s0x%04x][ID:%s][%s][seg0x%02x][j%d]", 
							i+1, //1 based index
							h,m,s,f,dh,dm,ds,df,
							list.m_ppEvents[i]->m_usType,
							list.m_ppEvents[i]->m_usStatus,

							list.m_ppEvents[i]->m_pszID?list.m_ppEvents[i]->m_pszID:"",
							list.m_ppEvents[i]->m_pszTitle?list.m_ppEvents[i]->m_pszTitle:"",
							list.m_ppEvents[i]->m_ucSegment,
							list.m_ppEvents[i]->m_usOnAirJulianDate
							);
#endif
						m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
					}
				}
			}
		}
		else
		{
			AfxMessageBox("No events were found in the file");
		}


		szItem.Format("Successfully analyzed %d events from file %s (%d bytes).", nParsedItems, m_szFile,ulLen);

		m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);

		free(pch);
	
	}
	fclose(fp);

//		AfxMessageBox(szItem);
}

void CHarrisAPITesterDlg::OnButtonDelevents() 
{
	// TODO: Add your control notification handler code here
	if(m_nSelIdx>=0)
	{
		if(!UpdateData(TRUE))return;

//		char server[32];
		CString szText = m_lcConn.GetItemText(m_nSelIdx, 0);
		int nidx = szText.Find(" (");
		if (nidx>0) szText = szText.Left(nidx);

		CString szItem; 
		szItem.Format("Deleting %d events from %s, list %d, starting at position %d",
			m_nNumEvents,
			szText,
			m_nListNum,
			m_nIndex+1 //1 based index
			);

		m_lcData.DeleteAllItems();  //reset
		
		m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);


//		AfxMessageBox(szItem);

		if(m_adc.m_ppConn[m_nSelIdx])
		{
			EnterCriticalSection(&m_adc.m_crit);
			int numevents = m_adc.DeleteEventAt(m_adc.m_ppConn[m_nSelIdx],
				m_nListNum, 
				m_nIndex, 
				m_nNumEvents,
				ADC_NORMAL);
			LeaveCriticalSection(&m_adc.m_crit);
			if(numevents>=ADC_SUCCESS)
			{
//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
				szItem.Format("Deleted %d events, return code %d",m_nNumEvents, numevents);
		
				m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
			}
		}
	}
	else AfxMessageBox("No selection");
}

void CHarrisAPITesterDlg::OnButtonSaveevents() 
{
	// TODO: Add your control notification handler code here
	if(m_nSelIdx>=0)
	{
		if(!UpdateData(TRUE))return;

		if(m_nIndex>=m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount){ AfxMessageBox("Index larger than number of retrieved events."); return; }

		FILE* fp;
		CString szFile;
		szFile.Format("SavedEvents%d.lst", clock()); // clock to just make a unique filename, prob.
		fp=fopen(szFile, "wb");
		if(!fp)
		{
			AfxMessageBox("The file could not be opened.");
			return;
		}

		if(m_adc.m_ppConn[m_nSelIdx])
		{
			if((m_nListNum>0)&&(m_nListNum<=MAXSYSLISTS)&&(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]))
			{
				for(int i=m_nIndex; i<min(m_nNumEvents, (m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount-m_nIndex)); i++) //m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount
				{
					if(
							(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents)
						&&(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i])
						)
					{
						m_adc.EventToStream(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[i]);
						
	//		AfxMessageBox("writing event");
						//size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream );
						if(fp) 
						{
							if(m_adc.m_Event.Type == 0x01)
							{
								fwrite ( (&(m_adc.m_Event)), sizeof(char), sizeof(revent), fp );
								if((m_adc.m_pucExtendedEventBuffer)&&(m_adc.m_usExtendedBufferLength))
									fwrite ( m_adc.m_pucExtendedEventBuffer, sizeof(char), m_adc.m_usExtendedBufferLength, fp );
								fflush(fp);
							}
							else
							{
								fwrite ( (&(m_adc.m_Event.eventtype)), sizeof(char), sizeof(revent)-sizeof(Byte)-sizeof(Word), fp );
								fflush(fp);
							}
						}

					}
				}
			}
		}
		if(fp) 
		{
			fclose(fp);
		}

	}
	else AfxMessageBox("No selection");
}

void CHarrisAPITesterDlg::OnButtonSaveevents2() 
{
	// TODO: Add your control notification handler code here
	
}

void CHarrisAPITesterDlg::OnButtonModify() 
{
	if(m_nSelIdx>=0)
	{
		if(!UpdateData(TRUE))return;

//		char server[32];
		CString szText = m_lcConn.GetItemText(m_nSelIdx, 0);
		int nidx = szText.Find(" (");
		if (nidx>0) szText = szText.Left(nidx);

//		AfxMessageBox(szItem);

		if(m_adc.m_ppConn[m_nSelIdx])
		{
			EnterCriticalSection(&m_adc.m_crit);
			int numevents = m_adc.GetEvents(m_adc.m_ppConn[m_nSelIdx],
				m_nListNum, 
				m_nIndex, 
				1, // one event only
				ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_FULLLIST);
			LeaveCriticalSection(&m_adc.m_crit);
			if(numevents>=ADC_SUCCESS)
			{
//				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]==NULL){AfxMessageBox("NULL"); return;}
//				szItem.Format("Received %d events%s",numevents, numevents>0?":":".");
		
//				m_lcData.InsertItem(m_lcData.GetItemCount(), szItem);
//				szItem.Format("Received %d events, event count=%d, buffersize=%d",numevents,m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventCount, m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_nEventBufferSize);
//		AfxMessageBox(szItem);

/*
				if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_prevent)
				{
					if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_prevent->Type = 0x01)
		AfxMessageBox("got extended event");
					else
		AfxMessageBox("got non-extended event");
				}
*/
				if(numevents)
				{

					CEventProperties dlg;

					unsigned char h,m,s,f, dh,dm,ds,df;

					m_adc.ConvertMillisecondsToHMSF(
						m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_ulOnAirTimeMS,
						&h,&m,&s,&f, 
						m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx); // pass in the systemfrx from the system data


					m_adc.ConvertMillisecondsToHMSF(
						m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_ulDurationMS,
						&dh,&dm,&ds,&df, 
						m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx); // pass in the systemfrx from the system data

					if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)
						dlg.m_szTime.Format("__:__:__.__");
					else
						dlg.m_szTime.Format("%02d:%02d:%02d.%02d",h,m,s,f);

					if(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_ulDurationMS==TIME_NOT_DEFINED)
						dlg.m_szDuration.Format("__:__:__.__");
					else
						dlg.m_szDuration.Format("%02d:%02d:%02d.%02d",dh,dm,ds,df);


					dlg.m_szEventControl.Format("%08x",m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_ulControl);
					dlg.m_szID.Format("%s",m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_pszID?m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_pszID:"");
					dlg.m_szStatus.Format("%04x",m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_usStatus);
					dlg.m_szTitle.Format("%s",m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_pszTitle?m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_pszTitle:"");
					dlg.m_szType.Format("%04x", m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]->m_usType);

					if(dlg.DoModal()==IDOK)
					{
						// modify.

						if((m_adc.m_ppConn[m_nSelIdx]))
						{
							CAEvent evt = *(m_adc.m_ppConn[m_nSelIdx]->m_pList[m_nListNum-1]->m_ppEvents[m_nIndex]);

							CBufferUtil bu;

							evt.m_pszID = dlg.m_szID.GetBuffer(0);
							evt.m_pszTitle = dlg.m_szTitle.GetBuffer(0);
							evt.m_usStatus = (unsigned short)bu.xtol(dlg.m_szStatus.GetBuffer(0), 4);
							evt.m_usType = (unsigned short)bu.xtol(dlg.m_szType.GetBuffer(0), 4);
							evt.m_ulControl = (unsigned short)bu.xtol(dlg.m_szEventControl.GetBuffer(0), 4);

							evt.m_ulDurationMS = 0;
							evt.m_ulOnAirTimeMS = 0;

							CSafeBufferUtil sbu;

//AfxMessageBox(dlg.m_szDuration);
							char* pch = sbu.Token(dlg.m_szDuration.GetBuffer(0), 11, ":.", MODE_SINGLEDELIM);

							int nField = 0;
							while((pch)&&(nField<4))
							{
								switch(nField)
								{
								case 0: evt.m_ulDurationMS = atol(pch)*60*60*1000; break;
								case 1: evt.m_ulDurationMS += atol(pch)*60*1000; break;
								case 2: evt.m_ulDurationMS += atol(pch)*1000; break;
								case 3: 
									{
										if(m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx == 0x24)
											evt.m_ulDurationMS += (atol(pch)*1000)/25; 
										else
											evt.m_ulDurationMS += (atol(pch)*1000)/30; 
									} break;
								default: pch = NULL; continue; break;
								}
								pch = sbu.Token(NULL, NULL, ":.", MODE_SINGLEDELIM);
								nField++;
							}

/*
CString foo; 
foo.Format("%d ms dur", evt.m_ulDurationMS);
AfxMessageBox(foo);
AfxMessageBox(dlg.m_szTime);
*/
							pch = sbu.Token(dlg.m_szTime.GetBuffer(0), 11, ":.", MODE_SINGLEDELIM);

							nField = 0;
							while((pch)&&(nField<4))
							{
								switch(nField)
								{
								case 0: evt.m_ulOnAirTimeMS = atol(pch)*60*60*1000; break;
								case 1: evt.m_ulOnAirTimeMS += atol(pch)*60*1000; break;
								case 2: evt.m_ulOnAirTimeMS += atol(pch)*1000; break;
								case 3: 
									{
										if(m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx == 0x24)
											evt.m_ulOnAirTimeMS += (atol(pch)*1000)/25; 
										else
											evt.m_ulOnAirTimeMS += (atol(pch)*1000)/30;
									}break;
								default: pch = NULL; continue; break;
								}
								pch = sbu.Token(NULL, NULL, ":.", MODE_SINGLEDELIM);
								nField++;
							}

//foo.Format("%d ms on air", evt.m_ulOnAirTimeMS);
//AfxMessageBox(foo);

							m_adc.EventToStream(&evt,0, m_adc.m_ppConn[m_nSelIdx]->m_SysData.systemfrx);

							nField = sizeof(revent);

							if(m_adc.m_VerifyEvent.Type == 0x01)
							{
//AfxMessageBox("Extended Event");
								m_adc.m_pucEventStreamBuffer = (unsigned char*)malloc(nField+m_adc.m_usVerifyExtendedBufferLength+1);
								if(m_adc.m_pucEventStreamBuffer)
								{
//foo.Format("extended event len %d %s", m_adc.m_usVerifyExtendedBufferLength, m_adc.m_pucVerifyExtendedEventBuffer+4);
//AfxMessageBox(foo);
									m_adc.m_usStreamBufferLength = nField+m_adc.m_usVerifyExtendedBufferLength;
									memcpy(m_adc.m_pucEventStreamBuffer, &m_adc.m_VerifyEvent, nField);
									memcpy(m_adc.m_pucEventStreamBuffer+nField, m_adc.m_pucVerifyExtendedEventBuffer, m_adc.m_usVerifyExtendedBufferLength);
								}
								else
								{
									m_adc.m_usStreamBufferLength = 0;
								}
							}
							else
							{
//AfxMessageBox("Non-extended Event");
								nField-=3;
								m_adc.m_pucEventStreamBuffer = (unsigned char*)malloc(nField);
								if(m_adc.m_pucEventStreamBuffer)
								{
									m_adc.m_usStreamBufferLength = nField;
									memcpy(m_adc.m_pucEventStreamBuffer, &(m_adc.m_VerifyEvent)+3, nField);
								}
								else
								{
									m_adc.m_usStreamBufferLength = 0;
								}
							}

//							m_adc.m_pucEventStreamBuffer = (unsigned char*)pch;  //a buffer for loading up a bunch of events
//							m_adc.m_usStreamBufferLength = (unsigned short)ulLen;  //length of the stream events buffer
//foo.Format("About to modify with %d stream buf len", m_adc.m_usStreamBufferLength);
//AfxMessageBox(foo);

FILE* fp;
fp=fopen("eventstream.lst", "wb");
if(fp)
{
	fwrite(m_adc.m_pucEventStreamBuffer, m_adc.m_usStreamBufferLength, 1, fp);
	fclose(fp);
}

							EnterCriticalSection(&m_adc.m_crit);
							int numevents = m_adc.ModifyEventAt(m_adc.m_ppConn[m_nSelIdx],
								m_nListNum, 
								m_nIndex, 
			//					nEvents,
								ADC_NORMAL);
							LeaveCriticalSection(&m_adc.m_crit);

//AfxMessageBox("freeing buffer");
							if(m_adc.m_pucEventStreamBuffer) free(m_adc.m_pucEventStreamBuffer);
							m_adc.m_pucEventStreamBuffer = NULL;
							m_adc.m_usStreamBufferLength = 0;

							evt.m_pszID = NULL;
							evt.m_pszTitle = NULL;
//AfxMessageBox("nulled stuff");

							if(numevents>=ADC_SUCCESS)
							{
							}
							else AfxMessageBox("Error modifying event!");
						}
						else
						{
							AfxMessageBox("No list or no events!");
						}

//						free(pch);
						m_adc.m_pucEventStreamBuffer = NULL;  //a buffer for loading up a bunch of events
						m_adc.m_usStreamBufferLength = 0;  //length of the stream events buffer

//AfxMessageBox("exiting");


					}

					
				}
			}
			else AfxMessageBox("Error retrieving event to edit");

		}
	}
	else AfxMessageBox("No selection");
}
