//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by HarrisAPITester.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_HARRISAPITESTER_DIALOG      102
#define IDD_HARRISAPITESTER_CLIENT_DIALOG 103
#define IDR_MAINFRAME                   128
#define IDD_EVENT_DIALOG                130
#define IDC_EDIT_SERVER                 1002
#define IDC_BUTTON_CONNECT              1003
#define IDC_LIST1                       1004
#define IDC_LIST_DATA                   1005
#define IDC_EDIT_IDX                    1006
#define IDC_EDIT_NUM                    1007
#define IDC_BUTTON_GETEVENTS            1008
#define IDC_EDIT_CLIENT                 1009
#define IDC_EDIT_LISTNUM                1010
#define IDC_STATIC_SEL                  1011
#define IDC_STATIC_STATUS               1012
#define IDC_EDIT_FILE                   1013
#define IDC_CHECK_FILE                  1014
#define IDC_BUTTON_INSEVENTS            1015
#define IDC_BUTTON_GETEVENTS2           1016
#define IDC_BUTTON_BROWSE               1017
#define IDC_BUTTON_DELEVENTS            1018
#define IDC_BUTTON_SAVEEVENTS2          1019
#define IDC_BUTTON_SAVEEVENTS           1020
#define IDC_BUTTON_MODIFY               1021
#define IDC_EDIT_ID                     1022
#define IDC_EDIT_TITLE                  1023
#define IDC_EDIT_TIME                   1024
#define IDC_EDIT_DUR                    1025
#define IDC_EDIT_CTRL                   1026
#define IDC_EDIT_TYPE                   1027
#define IDC_CHECK_STREAM                1027
#define IDC_EDIT_STATUS                 1028

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
