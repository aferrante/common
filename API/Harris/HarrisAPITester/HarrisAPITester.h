// HarrisAPITester.h : main header file for the HARRISAPITESTER application
//

#if !defined(AFX_HARRISAPITESTER_H__91C33097_B2B9_481A_B923_A599C50CCCE8__INCLUDED_)
#define AFX_HARRISAPITESTER_H__91C33097_B2B9_481A_B923_A599C50CCCE8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CHarrisAPITesterApp:
// See HarrisAPITester.cpp for the implementation of this class
//

class CHarrisAPITesterApp : public CWinApp
{
public:
	CHarrisAPITesterApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHarrisAPITesterApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CHarrisAPITesterApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HARRISAPITESTER_H__91C33097_B2B9_481A_B923_A599C50CCCE8__INCLUDED_)
