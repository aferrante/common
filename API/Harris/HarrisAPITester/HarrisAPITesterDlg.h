// HarrisAPITesterDlg.h : header file
//

#if !defined(AFX_HARRISAPITESTERDLG_H__63D9AF94_B86E_4C11_B0E7_63C269B2E09E__INCLUDED_)
#define AFX_HARRISAPITESTERDLG_H__63D9AF94_B86E_4C11_B0E7_63C269B2E09E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CHarrisAPITesterDlg dialog

#include "..\ADC.h"

//#define CLIENT_UI

class CHarrisAPITesterDlg : public CDialog
{
// Construction
public:
	CHarrisAPITesterDlg(CWnd* pParent = NULL);	// standard constructor
/*
#ifdef CLIENT_UI
	enum { IDD = IDD_HARRISAPITESTER_CLIENT_DIALOG };
#else
	enum { IDD = IDD_HARRISAPITESTER_DIALOG };
#endif
*/

// Dialog Data
	//{{AFX_DATA(CHarrisAPITesterDlg)
	enum { IDD = IDD_HARRISAPITESTER_DIALOG };
	CListCtrl	m_lcData;
	CListCtrl	m_lcConn;
	CString	m_szServer;
	CString	m_szClient;
	int		m_nNumEvents;
	int		m_nListNum;
	int		m_nIndex;
	BOOL	m_bFile;
	CString	m_szFile;
	BOOL	m_bStreamToFile;
	//}}AFX_DATA
	CADC m_adc;
	int m_nSelIdx;
	BOOL m_bConnect;

//	CAEvent MakeVDSEvent(char* pszTitle, char* buffer, unsigned int nbufferlen, int nOATMS=0, int nDurMS=0);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHarrisAPITesterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	void Display();
	int	GetHost();

	char* m_pszCompleteHost;
	char* m_pszHost;


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CHarrisAPITesterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnClickList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonGetevents();
	afx_msg void OnSetfocusEditServer();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnCheckFile();
	afx_msg void OnButtonInsevents();
	afx_msg void OnButtonBrowse();
	afx_msg void OnButtonGetevents2();
	afx_msg void OnButtonDelevents();
	afx_msg void OnButtonSaveevents();
	afx_msg void OnButtonSaveevents2();
	afx_msg void OnButtonModify();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HARRISAPITESTERDLG_H__63D9AF94_B86E_4C11_B0E7_63C269B2E09E__INCLUDED_)
