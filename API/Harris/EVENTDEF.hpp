//----------------------------------------------------------------------------
// eventdef.hpp
//
// 06/06/1997       First version
// 06/23/1997       Put in pack pragma
// 06/26/1997       Removed sets since MS C++ does not use
// 06/26/1997       Use " for includes of apiinc.hpp and timecode.hpp
// 08/16/1997       Removed apiinc.hpp usage
// 10/04/1998       SLO; Some additions, this document currently lacks
//                  extended data.
// 04/02/1999       SLO; Some additions, this document currently lacks
//                  extended events.
//
//----------------------------------------------------------------------------
#ifndef eventdefHPP
#define eventdefHPP
//----------------------------------------------------------------------------
#include "TimeCode.hpp"
#include "sysdefs.h"
namespace Eventdef
{
//-- type declarations -------------------------------------------------------
enum teventrunstatus {
     eventdone,
     eventrunning,
     playednextvideo,
     eventprerolled,
     eventpostrolled,
     eventidtitle,
     eventstandbyon,
     notplayed,
     eventranshort,
     eventskipped,
     eventpreped,
     eventnotswitched,
     eventpreviewed,
     rollingnext,
     eventshort,
     eventlong };

typedef Word teventstatusset;  // a set of teventrunstatus

enum teventcontrol {
     autoplay,
     autothread,
     autoswitch,
     autorecord,
     autotimed,
     autoexception,
     autoupcount,
     manualstart,
     autocontactstart,
     automarktime,
     autodeadroll,
     switchvideoonly,
     switchaudioonly,
     switchrejoin,
     userbitonly,
     switchAudioVideoIndependent };

typedef Word teventcontrolset; // a set of teventcontrol

enum mspottype {
     USECOMPILEID,
     SPOTAOK,
     SPOTBOK,
     USESPOTA,
     USESPOTB };

typedef Byte mspotset; // a set of mspottype

#pragma pack(push,1)
struct revent
{
        Byte Type;
        Word ExtendedType;

        Byte eventtype;
        char reconcilekey[8];
        Byte oaday;
        Byte oamonth;
        Byte oayear;
        Byte oaframe;
        Byte oasec;
        Byte oamin;
        Byte oahour;
        char eid[8];
        char etitle[16];
        Byte esomf;
        Byte esoms;
        Byte esomm;
        Byte esomh;
        Byte edurf;
        Byte edurs;
        Byte edurm;
        Byte edurh;
        Byte echannel;
        Byte qualifier4;
        Byte esegment;
        Byte emsindex;
        Byte elsindex;
        Byte ehibin;
        Byte elobin;
        Byte qualifier1;
        Byte qualifier2;
        Byte qualifier3;
        Word datetoair;
        teventcontrolset eventcontrol;
        teventstatusset eventstatus;
        char compiletape[8];
        Byte compilesom[4];
        char abox[8];
        Byte aboxsom[4];
        char bbox[8];
        Byte bboxsom[4];
        mspotset mspotcontrol;
        Byte backupemsindex;
        Byte backupelsindex;
        Byte extrathree;
} ;
#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define AVEVENT (Byte)(0)
#define AUDIOEVENT (Byte)(1)
#define VIDEOEVENT (Byte)(2)
#define BREAKEVENT (Byte)(16)
#define LOGOEVENT (Byte)(32)
#define BREAKSYNC (Byte)(33)
#define SECONDARYEVENT (Byte)(128)
#define SECAVEVENT (Byte)(128)
#define SECAUDIOEVENT (Byte)(129)
#define SECVIDEOEVENT (Byte)(130)
#define SECKEYEVENT (Byte)(131)
#define SECTRANSKEY (Byte)(132)
#define SECBACKAVEVENT (Byte)(133)
#define SECBACKTIMEGPI (Byte)(136)
#define SECGPIEVENT (Byte)(137)
#define SECTRANSAUDOVER (Byte)(144)
#define SECAUDOVER (Byte)(145)
#define SECDATAEVENT (Byte)(160)
#define SECSYSTEMEVENT (Byte)(164)
#define SECBACKSYSTEM (Byte)(165)
#define SECRECORDSWITCH (Byte)(176)
#define SECSOURCESWITCH (Byte)(177)
#define SECRECORDDEVICE (Byte)(181)
#define SECCOMMENT (Byte)(224)
#define SECCOMPILEID (Byte)(225)
#define SECAPPFLAG (Byte)(226)
#define SECBARTERSPOT (Byte)(227)
#define INVALIDEVENT (Byte)(255)

//-- template instantiations -------------------------------------------------

}       /* namespace Eventdef */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace Eventdef;
#endif
//-- end unit ----------------------------------------------------------------
#endif  // eventdef
