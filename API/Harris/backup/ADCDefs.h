// ADCDefs.h: support for the CADC class (ADC.h).
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADCDEFS_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
#define AFX_ADCDEFS_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "apilib32.hpp"
#include "statusdf.hpp"
#include "eventdef.hpp"
#include "netdefs.hpp"

#define DEVICE_NOT_DEFINED		0xffff
#define TYPE_NOT_DEFINED			0xffff
#define STATUS_NOT_DEFINED		0xffff
#define TIME_NOT_DEFINED			0xffffffff
#define DATE_NOT_DEFINED			0xffff
#define SEGMENT_NOT_DEFINED		0xff
#define INVALID_LIST					0xff

#define ADC_ERRORSTRING_LEN	512


#define ADC_SUCCESS		0
#define ADC_ERROR			-1
#define ADC_UNKNOWN		-2
#define ADC_NO_PLAYINGEVENT		-1
#define ADC_NO_EVENTS		-11

#define ADC_DEFAULT_TIMEOUT		10000 //milliseconds

// following for all get events functions
#define ADC_GETEVENTS_LIST					0x00000001  // put the events in the canonical list
#define ADC_GETEVENTS_TEMPLIST			0x00000002  // put the events in the volatile temp array
#define ADC_GETEVENTS_USEOFFSET			0x00000004  // update list with events at the obtained positions
#define ADC_GETEVENTS_LOCK					0x00000008  // lock list for get.  call fails if this set and lock fails.
#define ADC_GETEVENTS_FULLLIST			0x00000010  // get entire list count, not just up to lookahead
#define ADC_GETEVENTS_BREAKONCHANGE	0x00000020  // if a list or event change has occurred, break out
// following for get events until playing
#define ADC_GETEVENTS_EVENTLOOK			0x00000040  // event based lookahead
#define ADC_GETEVENTS_TIMELOOK			0x00000080  // time based lookahead
// following for get events until ID
#define ADC_GETEVENTS_INCLUDEDONE		0x00000100  // include done events
#define ADC_GETEVENTS_ALL_IDS				0x00000200  // get all of the supplied events
#define ADC_GETEVENTS_ANY_ID				0x00000400  // get any of the ids.
// various options for gets and mods
#define ADC_GETEVENTS_DONTGET				0x00000800  // get info from the events in the canonical list, not the server
#define ADC_NO_NOTIFY								0x00001000  // suppress sending a list change signal
#define ADC_PROTECT_PLAY						0x00002000  // do not modify or delete events with play status
#define ADC_PROTECT_DONE						0x00004000  // do not modify or delete events with done status
#define ADC_PRIMARY_ONLY						0x00008000  // only apply to primary events (where applicable)
#define ADC_SECONDARY_ONLY					0x00010000  // only apply to secondary events (where applicable)
#define ADC_VERIFY_ALL							0x00020000  // use verification event, ensure totally identical, including all fields not commonly used.
#define ADC_HEX											0x00040000  // expect hex encoded fields.
#define ADC_NORMAL									0x00000000  // expect no encoding

#define ADC_VERIFY_TYPE							0x00100000  // use verification event, ensure type match only.
#define ADC_VERIFY_ID								0x00200000  // use verification event, ensure id match only.
#define ADC_VERIFY_TITLE						0x00400000  // use verification event, ensure title match only.
#define ADC_VERIFY_DATA							0x00800000  // use verification event, ensure data match only.
#define ADC_VERIFY_RECKEY						0x01000000  // use verification event, ensure rec key match only.
#define ADC_VERIFY_STATUS						0x02000000  // use verification event, ensure status only.
#define ADC_VERIFY_TIME							0x04000000  // use verification event, ensure on air time only.
#define ADC_VERIFY_DUR							0x08000000  // use verification event, ensure duration only.

#define ADC_TARGET_SWAP_EVENT				0x00000001  // use swap event, and use verification event as supplemental source.  else vice versa
#define ADC_SWAP_EVENT							0x00000002  // use swap event, else use verification event
#define ADC_NO_CAEVENT							0x00000004  // ignore passed in CAEvent.
#define ADC_FORCE_EXTENDED					0x10000000  // force an extended event

#define ADC_SETEVENTS_LOCK					0x00000008  // lock list for list mods.  call fails if this set and lock fails.


// an event type that is uniform, containing only what we care about
class CAEvent  //Automation Event
{
public:
// supplied from automation system
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszData;  //must encode zero.
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;  // as appears in the list
	unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
	unsigned long  m_ulDurationMS;
	unsigned long  m_ulSOMMS;
	unsigned short m_usStatus;
	unsigned short m_usDevice;
//	unsigned short m_usControl; // was this
	unsigned long m_ulControl;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

//	revent* m_prevent; //the stream data for reference.

// a pointer to anything we want, to contextualize this event - must use new operator to allocate
	void* m_pContextData;  //must encode zero if a string.

public:
	CAEvent();
	virtual ~CAEvent();
	void Reset();
};


// we need to index by list, not by connection, but automation system indexes by connection.
// we can get a list status on a connection basis, then have an array of pointers indexing into lists.
// therefore, we also need a list type.
class CAList  //Automation Connection
{
public:
	CAEvent** m_ppEvents; // a pointer to an array of event pointers.
	int m_nEventCount;  // enumerators are int
	int m_nEventBufferSize;// enumerators are int
	int m_nPackCount; // needed to store pack count to return after lock list
	int m_nDefaultPackCount; // to unlock list when the unlock info is not available 
	CRITICAL_SECTION m_crit;  //  critical section.for list item access

public:
	CAList();
	virtual ~CAList();

	int EnsureAllocated(int nEventBufferSize);
};


class CAConnection  //Automation Connection
{
public:
// used to connect:
	char* m_pszServerName;
	char* m_pszClientName;

// supplied from automation system
	unsigned char  m_uchHandle;
	unsigned short m_usRefJulianDate; // compiled from m_SysData.systemdate offset, from January 1, 1900
	unsigned long  m_ulRefTimeMS;			// compiled from m_SysData.  // can mod 1000 this time to get the millisecond complement to the unix time (below)
	unsigned long  m_ulRefUnixTime;		// compiled from m_SysData.

	systemstatus				m_SysData; 
	tlistdata						m_ListData[MAXSYSLISTS];
	TConnectionStatus   m_Status;

	bool m_bListActive[MAXSYSLISTS];

// assigned internally
	CAList* m_pList[MAXSYSLISTS];  // only 16 lists allowed, so no need to dynamically allocate.
	CAList* m_pTempList; // a place to store events temporarily.  volatile.
	unsigned long  m_ulRefTick; // last status get.

public:
	CAConnection();
	virtual ~CAConnection();
};


#endif // !defined(AFX_ADCDEFS_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
