// ADevice.cpp: implementation of the CADevice class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ADevice.h"
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// global thread;
void ListenerThread(void* pvArgs);
void ErrorThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Design Philosophy
//////////////////////////////////////////////////////////////////////
// The CADevice class controls the XML communication
// to the ADC Universal SerCom Device.
// There is a server listener that the ADC communicates with.
// The ADC sends control instructions, expects a reply (if specified).
// The conditions for reply must be checked outside this class.
// This class manages the interface only.

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CADevice::CADevice()
{
	m_usListenPort = 0;
	m_bWinsockSet = false;
	m_bXMLSet = false;
	m_bListenerThreadStarted = false;
	m_bWatchThreadStarted = false;
	m_bKillListenerThread = true;
	m_pszXMLStream = NULL;
	m_ulXMLBufferLength=0;
	m_socketServer=NULL;
	m_ucListenerErrors=0;
	m_ulMaxCmdBufferLength=0;
	m_lpfnCmd = NULL;
	m_chNak = 0x15; //default is ASCII NAK, 0xff means dont send.

	for(int i=0;i<ADEV_NUMCMD;i++)
	{
		m_pchCmd[i][ADEV_CMD]=NULL;
		m_ulCmdLength[i][ADEV_CMD]=0;
		m_pchCmd[i][ADEV_ACK]=NULL;
		m_ulCmdLength[i][ADEV_ACK]=0;
	}
}

CADevice::~CADevice()
{
	if(m_pszXMLStream) free(m_pszXMLStream);
	for(int i=0;i<ADEV_NUMCMD;i++)
	{
		if(m_pchCmd[i][ADEV_CMD]) free(m_pchCmd[i][ADEV_CMD]);
		if(m_pchCmd[i][ADEV_ACK]) free(m_pchCmd[i][ADEV_ACK]);
	}
	if(m_bWinsockSet||m_bListenerThreadStarted)
	{ 
		StopListener();
	}
}

// core
int CADevice::StartListener(unsigned short usPort)
{

	if(m_bListenerThreadStarted)
	{
		if((usPort==m_usListenPort)&&(usPort!=0))
		{
			return ADC_SUCCESS;
		}
		else  // have to kill the existing listener.
		{
			m_bKillListenerThread = true;
		}
	}

	if(!m_bWinsockSet)
	{

		unsigned short usVersionRequested;
		WSADATA wsaData;
		int nError;

		usVersionRequested = MAKEWORD( 2, 2 );
		nError = WSAStartup( usVersionRequested, &wsaData );
		if ( nError != 0 ) 
		{
			// Tell the user that we could not find a usable Winsock DLL.
			Message(MSG_PRI_HIGH|MSG_ICONERROR, 
				"Unable to start networking (no winsock).",
				"ADevice:StartListener");
			return ADC_ERROR;
		}
	
		// Confirm that the Winsock DLL supports 2.2.
		// Note that if the DLL supports versions greater
		// than 2.2 in addition to 2.2, it will still return
		// 2.2 in wVersion since that is the version we requested. 

		if ( LOBYTE( wsaData.wVersion ) != 2 ||	HIBYTE( wsaData.wVersion ) != 2 ) 
		{
			// Tell the user that we could not find a usable WinSock DLL. 
			Message(MSG_PRI_HIGH|MSG_ICONERROR, 
				"Unable to start networking (incompatible winsock version).",
				"ADevice:StartListener");
			return ADC_ERROR;
		}

		m_bWinsockSet = true;
	}

	// actually start the listener.
	while(m_bListenerThreadStarted) Sleep(1);
	m_usListenPort = usPort;


	m_socketServer = socket(AF_INET,SOCK_STREAM,0);
	if ( m_socketServer == INVALID_SOCKET )
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, 
			"Invalid socket.",
			"ADevice:StartListener");
		return ADC_ERROR;
	}
	
	SOCKADDR_IN si;

	si.sin_family = AF_INET;
	si.sin_port = htons(m_usListenPort);		// port
	si.sin_addr.s_addr = htonl(INADDR_ANY);

	if ( bind(m_socketServer, (struct sockaddr*) &si, sizeof(SOCKADDR_IN)) == SOCKET_ERROR )
	{
		Message(MSG_PRI_HIGH|MSG_ICONERROR, 
			"Error binding socket.",
			"ADevice:StartListener");
		closesocket(m_socketServer);
		return ADC_ERROR;
	}

	m_bKillListenerThread = false;

	_beginthread( ListenerThread, 0, (void*)(this) ); 
	if(!m_bWatchThreadStarted) _beginthread( ErrorThread, 0, (void*)(this) ); 

	return ADC_SUCCESS;
}

int CADevice::StopListener()
{
	m_bKillListenerThread = true;

	while(m_bListenerThreadStarted) Sleep(1); // let the sockets close

	if(m_bWinsockSet)
	{
		WSACleanup();
		m_bWinsockSet=false;
	}

	return ADC_SUCCESS;
}

int CADevice::SetXML(char* pszXMLStream, unsigned long ulBufferLength)
{
	// ulBufferLength is passed in to allow not searching past a certain marker.
	// after the Event, there may be other "payload XML" that isnt related to device control.
	if(pszXMLStream==NULL) return ADC_ERROR;
	int nReturn = ADC_ERROR;
	char* pchBegin; 
	char* pchEnd; 
	char* pchMarker = pszXMLStream+ulBufferLength; 
	
	pchBegin = strstr(m_pszXMLStream, "<aEvt>");
	pchEnd = strstr(m_pszXMLStream, "</aEvt>");

	if((pchBegin!=NULL)&&(pchEnd!=NULL)&&(pchEnd>pchBegin)&&(pchBegin<pchMarker)&&(pchEnd<pchMarker)) // valid event
	{
		if((m_pszXMLStream)&&(ulBufferLength>0))
		{
			for(int i=0;i<ADEV_NUMCMD;i++)
			{
				if(m_pchCmd[i][ADEV_CMD]) free(m_pchCmd[i][ADEV_CMD]);
				if(m_pchCmd[i][ADEV_ACK]) free(m_pchCmd[i][ADEV_ACK]);
				m_pchCmd[i][ADEV_CMD]=NULL;
				m_ulCmdLength[i][ADEV_CMD]=0;
				m_pchCmd[i][ADEV_ACK]=NULL;
				m_ulCmdLength[i][ADEV_ACK]=0;

				pchBegin = NULL;
				pchEnd = NULL;
				switch(i)
				{
				case	ADEV_REG://	0 //	register
					{
						pchBegin = strstr(m_pszXMLStream, "<aReg>");
						pchEnd = strstr(m_pszXMLStream, "</aReg>");
					} break;
				case	ADEV_CUE://	1 //	cue
					{
						pchBegin = strstr(m_pszXMLStream, "<aCue>");
						pchEnd = strstr(m_pszXMLStream, "</aCue>");
					} break;
				case	ADEV_PRE://	2 //	preroll
					{
						pchBegin = strstr(m_pszXMLStream, "<aPre>");
						pchEnd = strstr(m_pszXMLStream, "</aPre>");
					} break;
				case	ADEV_PLAY:// 3 //	play
					{
						pchBegin = strstr(m_pszXMLStream, "<aPly>");
						pchEnd = strstr(m_pszXMLStream, "</aPly>");
					} break;
				case	ADEV_END://	4 //	end
					{
						pchBegin = strstr(m_pszXMLStream, "<aEnd>");
						pchEnd = strstr(m_pszXMLStream, "</aEnd>");
					} break;
				case	ADEV_POST://	5 //	postroll
					{
						pchBegin = strstr(m_pszXMLStream, "<aPos>");
						pchEnd = strstr(m_pszXMLStream, "</aPos>");
					} break;
				}

				if((pchBegin!=NULL)&&(pchEnd!=NULL)&&(pchEnd>pchBegin)&&(pchBegin<pchMarker)&&(pchEnd<pchMarker)) // valid tag
				{
					char* pchStart = strstr(pchBegin, "<aCmd>");
					char* pchFinish = strstr(pchBegin, "</aCmd>");
					unsigned long ulBufferLength;

					if(pchStart) pchStart+=6; // skip the tag

					if((pchStart!=NULL)&&(pchFinish!=NULL)
						&&(pchFinish>pchStart)
						&&(pchStart<pchEnd)&&(pchFinish<pchEnd)) // valid Cmd
					{
						ulBufferLength = pchFinish-pchStart;
						m_pchCmd[i][ADEV_CMD] = DecodeHex(pchStart, &ulBufferLength);
						m_ulCmdLength[i][ADEV_CMD]=ulBufferLength;
					}

					pchStart = strstr(pchBegin, "<aRpy>");
					pchFinish = strstr(pchBegin, "</aRpy>");

					if(pchStart) pchStart+=6; // skip the tag

					if((pchStart!=NULL)&&(pchFinish!=NULL)
						&&(pchFinish>pchStart)
						&&(pchStart<pchEnd)&&(pchFinish<pchEnd)) // valid Reply
					{
						ulBufferLength = pchFinish-pchStart;
						m_pchCmd[i][ADEV_ACK] = DecodeHex(pchStart, &ulBufferLength);
						m_ulCmdLength[i][ADEV_ACK]=ulBufferLength;
					}
				}
			}
		}
		nReturn = ADC_SUCCESS;
	}
	return nReturn; 
}

int CADevice::SetCommandFn(LPFNCMD lpfnCmd)
{
	if(lpfnCmd==NULL)	return ADC_ERROR;
	m_lpfnCmd = lpfnCmd;

	return ADC_SUCCESS;
}

// interface
int CADevice::SendReply(SOCKET socket, char chCmd)
{
	// check conditions in here.

	if(chCmd==ADEV_ERROR) return ADC_SUCCESS; // just skip.

	if((m_lpfnCmd)&&(socket))
	{
		if(m_lpfnCmd(chCmd)==ADC_SUCCESS)
		{
			if((m_pchCmd[chCmd][ADEV_ACK])&&(m_ulCmdLength[chCmd][ADEV_ACK]>0))
				send(socket, m_pchCmd[chCmd][ADEV_ACK], m_ulCmdLength[chCmd][ADEV_ACK], 0);

			return ADC_SUCCESS;
		}
		else  // couldnt comply with command, send NAK
		{
			if(m_chNak!=0xff)  //0xff means don't send
			{
				send(socket, &m_chNak, 1, 0);
			}
		}
	}
	return ADC_ERROR;
}

char CADevice::InterpretCmd(char* pchCmd, unsigned long ulBufferLength)
{
	char chReturn = ADEV_ERROR;
	if(pchCmd)
	{
		for(char i=0;i<ADEV_NUMCMD;i++)
		{
			if(m_pchCmd[i][ADEV_CMD]) 
			{
				if(ulBufferLength==m_ulCmdLength[i][ADEV_CMD])
				{
					if(memcmp(m_pchCmd[i][ADEV_ACK], pchCmd, ulBufferLength)==0)
					{
						chReturn = i;
						break;
					}
				}
			}
		}
	}
	return chReturn;
}


// utility
char* CADevice::EncodeHex(char* pchCmd, unsigned long* pulBufferLength)
{
	char* pch = (char*)malloc((*pulBufferLength)*4); // this will be enough to encode every single char.
	if(pch==NULL)
	{
		*pulBufferLength = 0;
		return NULL;
	}

	unsigned long x=0;
	for (unsigned long f=0; f<*pulBufferLength; f++)
	{
		if((*(pchCmd+f)>31)&&(*(pchCmd+f)<127)&&(*(pchCmd+f)!=0x7b)&&(*(pchCmd+f)!=0x7d))
		{
			pch[x]=*(pchCmd+f);
		}
		else
		{
			char hex[5];
			sprintf(hex, "{%02x}", (*(pchCmd+f))&0xff);
			pch[x++] = hex[0];
			pch[x++] = hex[1];
			pch[x++] = hex[2];
			pch[x] = hex[3];
		}
		x++;
	}
	*pulBufferLength = x;
	return pch;
}

char* CADevice::DecodeHex(char* pchCmd, unsigned long* pulBufferLength)
{
	char* pch = (char*)malloc(*pulBufferLength); // this will be enough to decode;
	if(pch==NULL)
	{
		*pulBufferLength = 0;
		return NULL;
	}

	unsigned long x=0;
	for (unsigned long f=0; f<*pulBufferLength; f++)
	{
		if(*(pchCmd+f)!=0x7b)
		{
			pch[x]=*(pchCmd+f);
		}
		else  // we have a bracket char.
		{
			if(f+3<*pulBufferLength)
			{
				f++;
				pch[x]=0;
				if((*(pchCmd+f)>=48)&&(*(pchCmd+f)<58))	pch[x] = *(pchCmd+f)-48;
				else
				if((*(pchCmd+f)>=65)&&(*(pchCmd+f)<71))	pch[x] = *(pchCmd+f)-55;
				else
				if((*(pchCmd+f)>=97)&&(*(pchCmd+f)<103))	pch[x] = *(pchCmd+f)-87;

				pch[x]*=16;

				f++;
				if((*(pchCmd+f)>=48)&&(*(pchCmd+f)<58))	pch[x] += *(pchCmd+f)-48;
				else
				if((*(pchCmd+f)>=65)&&(*(pchCmd+f)<71))	pch[x] += *(pchCmd+f)-55;
				else
				if((*(pchCmd+f)>=97)&&(*(pchCmd+f)<103))	pch[x] += *(pchCmd+f)-87;
				
				f++; // eat up the last bracket;
			}
		}
		x++;
	}
	*pulBufferLength = x;
	return pch;
}


void ListenerThread(void* pvArgs)
{
	CADevice* pdev = (CADevice*)pvArgs;
	if(pdev)
	{
		pdev->Message(MSG_PRI_NORMAL, 
			"Starting the Listener Thread.",
			"ADevice:ListenerThread");
		int nSize = 0;
		int nTotalSize = 0;
		int nBufferSize = max(ADEV_COMM_SIZE, pdev->m_ulMaxCmdBufferLength);
		char* pch = (char*)malloc(nBufferSize);

		SOCKET socketClient;
		SOCKADDR_IN addrClient;
		int nClientAddressLen = sizeof(SOCKADDR_IN);
		pdev->m_bListenerThreadStarted = true;

		while(!pdev->m_bKillListenerThread)
		{
			socketClient = accept(pdev->m_socketServer, (struct sockaddr *)&addrClient, &nClientAddressLen);
			if ( socketClient == INVALID_SOCKET )
			{
				pdev->Message(MSG_PRI_HIGH|MSG_ICONERROR, 
					"Error in accept.",
					"ADevice:ListenerThread");
					closesocket(pdev->m_socketServer);
					pdev->m_socketServer = NULL;
					pdev->m_bListenerThreadStarted = false;
				pdev->m_ucListenerErrors++;
				return;
			}

			pdev->m_ucListenerErrors = 0; // we suceeded so we can reset the error counter
			// for each connection just stall the thread here.
			while((!pdev->m_bKillListenerThread)&&(nSize!=SOCKET_ERROR))
			{
				if(pch)
				{
					nTotalSize = 0;
					do
					{
						nSize = recv(socketClient, pch+nTotalSize, nBufferSize-nTotalSize, 0);
						if((nSize!=0)&&(nSize!=SOCKET_ERROR))
						{
							nTotalSize += nSize;
						}
					} while((!pdev->m_bKillListenerThread)&&(nSize!=0)&&(nSize!=SOCKET_ERROR)&&(nTotalSize<nBufferSize));

					if((nSize!=SOCKET_ERROR)&&(nTotalSize>0))
					{
						// here we have a buffer full of the input command.
						char chCmd = pdev->InterpretCmd(pch, (unsigned long)nBufferSize);

						if(pdev->SendReply(socketClient, chCmd)!=ADC_SUCCESS)
							nSize=SOCKET_ERROR;
					}
				} 
				else nSize=SOCKET_ERROR;  // not really a socket error, but an error anyway
			}
			closesocket(socketClient);  // close the connection.
		}

		if(pch) free(pch);

		closesocket(pdev->m_socketServer);
		pdev->m_socketServer = NULL;
		pdev->m_bListenerThreadStarted = false;
		pdev->Message(MSG_PRI_NORMAL, 
			"Stopping the Listener Thread.",
			"ADevice:ListenerThread");
	}
}

void ErrorThread(void* pvArgs)
{
	CADevice* pdev = (CADevice*)pvArgs;

	if(pdev)
	{
		pdev->m_bWatchThreadStarted = true;
		while((!pdev->m_bKillListenerThread)&&(pdev->m_ucListenerErrors<3))
		{
			if(pdev->m_bListenerThreadStarted == false) // an error has occurred.
			{
				pdev->StartListener(pdev->m_usListenPort);  // restart the listener
				pdev->Message(MSG_PRI_HIGH|MSG_ICONHAND, 
					"An error was detected.  Attempting to restart Listener",
					"ADevice:ErrorThread");
				while(!pdev->m_bListenerThreadStarted) Sleep(1); // let the thread start up.  otherwise we hammer StartListener repeatedly.
			}
		}
		if(pdev->m_ucListenerErrors>=3)
		{
			pdev->Message(MSG_PRI_HIGH|MSG_ICONHAND, 
				"Shutting down Error Thread.  Too many errors have occurred.",
				"ADevice:ErrorThread");
		}
		pdev->m_bWatchThreadStarted = false;
	}
}
