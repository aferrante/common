// ADevice.h: interface for the CADevice class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADEVICE_H__1389AD96_B7A0_49E3_A7A2_7E755124B44F__INCLUDED_)
#define AFX_ADEVICE_H__1389AD96_B7A0_49E3_A7A2_7E755124B44F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../MSG/MessagingObject.h"
#include "ADCDefs.h"
#include <winsock2.h>

#pragma comment(lib, "ws2_32.lib")

#define	ADEV_ERROR -1 //	error
#define	ADEV_REG	0 //	register
#define	ADEV_CUE	1 //	cue
#define	ADEV_PRE	2 //	preroll
#define	ADEV_PLAY 3 //	play
#define	ADEV_END	4 //	end
#define	ADEV_POST	5 //	postroll
#define	ADEV_NUMCMD	6 

#define	ADEV_CMD	0 //	command
#define	ADEV_ACK	1 //	reply

#define	ADEV_COMM_SIZE	1024

// callback to condition check.
typedef int (__stdcall* LPFNCMD)(char); //pointer to function, command device and check condition met after command

class CADevice : public CMessagingObject
{
public:
	bool m_bWinsockSet;
	bool m_bXMLSet;
	bool m_bListenerThreadStarted;
	bool m_bWatchThreadStarted;
	bool m_bKillListenerThread;
	unsigned char m_ucListenerErrors;
	unsigned short m_usListenPort;
	char* m_pszXMLStream;
	unsigned long m_ulXMLBufferLength;
	unsigned long m_ulMaxCmdBufferLength;
	SOCKET m_socketServer;
	LPFNCMD m_lpfnCmd;
	char m_chNak;


	char* m_pchCmd[ADEV_NUMCMD][2];
	unsigned long m_ulCmdLength[ADEV_NUMCMD][2];

public:
	CADevice();
	virtual ~CADevice();

// core
	int StartListener(unsigned short usPort);
	int StopListener();
	int SetXML(char* pchXMLStream, unsigned long ulBufferLength);
	int SetCommandFn(LPFNCMD lpfnCmd);

// interface
	int SendReply(SOCKET socket, char chCmd);
	char InterpretCmd(char* pchCmd, unsigned long ulBufferLength);

// utility
	char* EncodeHex(char* pchCmd, unsigned long* pulBufferLength);
	char* DecodeHex(char* pchCmd, unsigned long* pulBufferLength);
};

#endif // !defined(AFX_ADEVICE_H__1389AD96_B7A0_49E3_A7A2_7E755124B44F__INCLUDED_)
