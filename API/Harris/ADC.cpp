// ADC.cpp: implementation of the CADC class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "ADC.h"
#include "../../TXT/BufferUtil.h"
/*
#include <time.h>
#include <direct.h>
#include <process.h>
#include <ctype.h>
*/
#include <winbase.h> // needed to halt thread execution with Sleep.


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Design Philosophy
//////////////////////////////////////////////////////////////////////
// The CADC class is designed to manage connections to Harris ADC
// automation servers.  Since the NBIOS32 and APILIB32 dlls allow
// connections to multiple servers, the CADC class must track and
// process data for mutiple servers.  Since each server may have
// multiple lists to track, each list is in effect its own "destination"
// object.  Each server is tracked continuously.  CADC uses the Harris 
// API DLLs to communicate with these servers, so we use a serial
// approach so as not to make asynchronous calls thru the DLL.  If
// a list indicates a change, a refresh of its data is done, according
// to the properties set for each list.  We use dynamic allocation of
// list objects, because there are up to 10,000 events for each list, 
// making static arrays too large.


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CADC::CADC()
{
	m_ppConn = NULL;
	m_nNumConn = 0;

	m_SendData.MessageID = ncomevent;
	m_SendData.thenrevent.nlistcmd = npack;
	m_SendData.thenrevent.nlistid = 1;
	m_SendData.thenrevent.nlistindex = 0;
	m_SendData.thenrevent.nothervalue = 0;
	m_SendData.thenrevent.numberofevents = 0;

	m_ReceiveData.MessageID = ncomevent;
	m_ReceiveData.thenrevent.nlistcmd = npack;
	m_ReceiveData.thenrevent.nlistid = 1;
	m_ReceiveData.thenrevent.nlistindex = 0;
	m_ReceiveData.thenrevent.nothervalue = 0;
	m_ReceiveData.thenrevent.numberofevents = 0;

	m_pucEventStreamBuffer=NULL;  //a buffer for loading up a bunch of events
	m_usStreamBufferLength=0;  //length of the stream events buffer

	m_pucExtendedEventBuffer=NULL;  //extended data swap buffer
	m_usExtendedBufferLength=0;  // length of the buffer and the data. (max 4096)

	m_pucVerifyExtendedEventBuffer=NULL;  //extended data swap buffer
	m_usVerifyExtendedBufferLength=0;  // length of the buffer and the data. (max 4096)

	m_pchDebugFilename = NULL;

	InitEvent(ADC_SWAP_EVENT);
	InitEvent(0);

	InitializeCriticalSection(&m_crit);  //  critical section.
	InitializeCriticalSection(&m_critGetEvents);  //  critical section.
	
}

CADC::~CADC()
{
	if(m_ppConn)
	{
		for(int i=0; i<m_nNumConn; i++)
		{
			CAConnection* pconn = m_ppConn[i];
			if(pconn)
			{
				EnterCriticalSection(&m_crit);  //  critical section.
				DisConnect(pconn->m_uchHandle);
				LeaveCriticalSection(&m_crit);  //  critical section.
				delete pconn;
				pconn = NULL;
			}
		}
		delete [] m_ppConn;
	}

	//m_pucEventStreamBuffer // we will let whatever set this on the outside free it as well.
	if(m_pucExtendedEventBuffer) free(m_pucExtendedEventBuffer);   //must use malloc
	if(m_pucVerifyExtendedEventBuffer) free(m_pucVerifyExtendedEventBuffer); //must use malloc
	if(m_pchDebugFilename) free(m_pchDebugFilename); //must use malloc
	
	
	DeleteCriticalSection(&m_crit);  //  critical section.
	DeleteCriticalSection(&m_critGetEvents);  //  critical section.

}

/*
int CADC::HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	return 0;
}
*/

//////////////////////////////////////////////////////////////////////
// core - main
//////////////////////////////////////////////////////////////////////

// the pointer char*** is the address of an array of char*, which marks the last char of each event.
// this can be used to subtract from the previous to get the stream limits of each event
// the array must be deleted after use, unless NULL is passed in, or there are no events in stream, in which case no array is allocated
// this does not take a message rec, it takes an event stream wihtout hte rest of the message rec
int CADC::CountEventsInStream(unsigned char* pchEventStream, unsigned long ulStreamLen, unsigned char*** pppchEventMarkers)
{
	if((ulStreamLen==0)||(pchEventStream==NULL)) return ADC_ERROR;
	int nReturn = 0;
  unsigned char* pch;
  unsigned char* pchEvent = (unsigned char*)pchEventStream;
	unsigned short usType = 0x0000;

	if(pppchEventMarkers) *pppchEventMarkers = NULL;
	unsigned char** ppchReturnEM = NULL;
//CString foo;
	while(pchEvent<(pchEventStream+ulStreamLen))
	{
//foo.Format("event %d: pppchEventMarkers = %d", nReturn+1,pppchEventMarkers);	AfxMessageBox(foo);
		if(*pchEvent == 0x01)
		{
//foo.Format("event %d: extended", nReturn+1);	AfxMessageBox(foo);
    // extended type
			usType = ((revent*)pchEvent)->ExtendedType;

			// EXTENDED DATA EVENTS
			if ((usType == 0x0100) ||
					(usType == 0x0180) ||
					(usType == 0x0185) ||
					(usType == 0x01A0))
			{
				unsigned long ulDataLength = 0;
				unsigned long ulDataPointer = 0;

				pch = pchEvent + 107;

				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				if(ulDataLength>4096) ulDataLength=4096;

				ulDataPointer = 109 + ulDataLength; // 107+2 bytes for the data length bytes.

				pchEvent += ulDataPointer;

				if(pppchEventMarkers)
				{// add.
//AfxMessageBox("not null");
					unsigned char** ppchEM = new unsigned char*[nReturn+1];

					if(ppchEM)
					{
//AfxMessageBox("alloc");
						if(ppchReturnEM)
						{
//AfxMessageBox("memcpy");
							if(nReturn>0)
							{
								int i=0;
								while(i<nReturn){ppchEM[i] = ppchReturnEM[i]; i++;}
//								memcpy(&ppchEM, &ppchReturnEM, (nReturn)*sizeof(unsigned char*));
							}
							delete [] (ppchReturnEM);
						}
						ppchEM[nReturn] = pchEvent-1;
//CString szItem; szItem.Format("event: %d, char*: %d",nReturn,ppchEM[nReturn]); AfxMessageBox(szItem);

						ppchReturnEM = ppchEM;
					}
				}
				nReturn++;

			}
			// EXTENDED ID/TITLE EVENTS
			else if (
					(usType == 0x0000) ||
					(usType == 0x0080) ||
					(usType == 0x0085) ||
					(usType == 0x00E0) ||
					(usType == 0x00A0) ||
					(usType == 0x00A4) ||
					(usType == 0x00A5) ||
					(usType == 0x00B0) ||
					(usType == 0x00B1) ||
					(usType == 0x00B5))
			{
				unsigned long ulDataLength = 0;
				unsigned long ulDataPointer = 0;

				pch = pchEvent + 107;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				
				if(ulDataLength==0)
				{
					ulDataPointer = 109;
				}
				else
				{
					ulDataPointer = 109+ulDataLength;
				}

				//if theres any extended Title data.
				pch = pchEvent + ulDataPointer;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));

				if(ulDataLength==0)
				{
					ulDataPointer += 2;
				}
				else
				{
					ulDataPointer += 2;
					ulDataPointer += ulDataLength;
				}

				
				pchEvent += ulDataPointer;
				if(pppchEventMarkers)
				{// add.
//AfxMessageBox("not null");
					unsigned char** ppchEM = new unsigned char*[nReturn+1];

					if(ppchEM)
					{
//AfxMessageBox("alloc");
						if(ppchReturnEM)
						{
//AfxMessageBox("memcpy");
							if(nReturn>0)
							{
								int i=0;
								while(i<nReturn){ppchEM[i] = ppchReturnEM[i]; i++;}
//								memcpy(&ppchEM, &ppchReturnEM, (nReturn)*sizeof(unsigned char*));
							}
							delete [] (ppchReturnEM);
						}
						ppchEM[nReturn] = pchEvent-1;
//CString szItem; szItem.Format("event: %d, char*: %d",nReturn,ppchEM[nReturn]); AfxMessageBox(szItem);
						ppchReturnEM = ppchEM;
					}
				}
				nReturn++;
			}
			// EXTENDED ID/TITLE EVENTS WITH EXTENDED DATA
			else if (
					(((revent*)pchEvent)->ExtendedType == 0x0200) ||
					(((revent*)pchEvent)->ExtendedType == 0x0280))
			{

				unsigned long ulDataLength = 0;
				unsigned long ulDataPointer = 0;

				pch = pchEvent + 107;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				
				if(ulDataLength==0)
				{
					ulDataPointer = 109;
				}
				else
				{
					ulDataPointer = 109+ulDataLength;
				}

				//first see if theres any extended Title data.
				pch = pchEvent + ulDataPointer;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));

				if(ulDataLength==0)
				{
					ulDataPointer += 2;
				}
				else
				{
					ulDataPointer += 2;
					ulDataPointer += ulDataLength;
				}

				pch = pchEvent + ulDataPointer;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				if(ulDataLength>4096) ulDataLength=4096;

				ulDataPointer = ulDataLength + 2; // 2 bytes for the data length bytes.

				pchEvent += ulDataPointer;
				if(pppchEventMarkers)
				{// add.
//AfxMessageBox("not null");
					unsigned char** ppchEM = new unsigned char*[nReturn+1];

					if(ppchEM)
					{
//AfxMessageBox("alloc");
						if(ppchReturnEM)
						{
//AfxMessageBox("memcpy");
							if(nReturn>0)
							{
								int i=0;
								while(i<nReturn){ppchEM[i] = ppchReturnEM[i]; i++;}
//								memcpy(&ppchEM, &ppchReturnEM, (nReturn)*sizeof(unsigned char*));
							}
							delete [] (ppchReturnEM);
						}
						ppchEM[nReturn] = pchEvent-1;
//CString szItem; szItem.Format("event: %d, char*: %d",nReturn,ppchEM[nReturn]); AfxMessageBox(szItem);
						ppchReturnEM = ppchEM;
					}
				}
				nReturn++;
			}
			else
			{
				//shouldnt get here.
				// unknown type.  we dont know how to process...
				//we dont know how long, dont know how far to skip ahead...
				break; // have to break out then
			}
		}
		else  // it's not an extended event
		{
//foo.Format("event %d: not extended", nReturn+1);	AfxMessageBox(foo);
			unsigned long ulDataLength = 0;
			pchEvent += 104;
			if(pppchEventMarkers)
			{// add.
//AfxMessageBox("not null");
				unsigned char** ppchEM = new unsigned char*[nReturn+1];

				if(ppchEM)
				{
//AfxMessageBox("alloc");
					if(ppchReturnEM)
					{
//AfxMessageBox("memcpy");
						if(nReturn>0)
						{
							int i=0;
							while(i<nReturn){ppchEM[i] = ppchReturnEM[i]; i++;}
//								memcpy(&ppchEM, &ppchReturnEM, (nReturn)*sizeof(unsigned char*));
						}
						delete [] (ppchReturnEM);
					}
					ppchEM[nReturn] = pchEvent-1;
//CString szItem; szItem.Format("event: %d, char*: %d",nReturn,ppchEM[nReturn]); AfxMessageBox(szItem);
					ppchReturnEM = ppchEM;
				}
			}
			nReturn++;
		}
	}
	if(pppchEventMarkers)
	{
		if(ppchReturnEM == NULL)
			*pppchEventMarkers = NULL;
		else
			*pppchEventMarkers = ppchReturnEM;	
	}
	return nReturn;
}


int CADC::StreamToArray(CAList* pList, int nStartIndex, unsigned char* pchMessageRec, unsigned char ucFrameBasis, int nNumLimitEvents, int nSafeBufferLength)
{
	if((pList==NULL)||(pchMessageRec==NULL)) return ADC_ERROR;
	char errorstring[ADC_ERRORSTRING_LEN];
	char hexbuf[8];
	if((nSafeBufferLength>=0)&&(nSafeBufferLength<7)) // need this many bytes to determine number of events!
	{
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] did not have a sufficient buffer: only %d bytes", clock(), nSafeBufferLength);
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:StreamToArray");// Sleep(50);
		return ADC_ERROR;
	}
	int nReturn = 0;
  unsigned char* pch = pchMessageRec+6;
  unsigned char* pchEvent = pch+2;
	int nNumEvents = 0;//(int)((unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch)));
	int i;
	char chNulls[2] = {(char)0x20, (char)0xff};

	messageRec* pmsgrc = (messageRec*) pchMessageRec;

		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] stream message bytes [%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x]", clock() ,
			*(pchMessageRec+0),
			*(pchMessageRec+1),
			*(pchMessageRec+2),
			*(pchMessageRec+3),
			*(pchMessageRec+4),
			*(pchMessageRec+5),
			*(pchMessageRec+6),
			*(pchMessageRec+7)
			);
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:StreamToArray");// Sleep(50);

		// let's make sure it is a natget response!
	if(pmsgrc->MessageID != ncomevent)
	{
		if(nSafeBufferLength<0)
		{
			_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "StreamToArray [%d] MessageID was not ncomevent! Message bytes [%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x]", clock(),
				*(pchMessageRec+0),
				*(pchMessageRec+1),
				*(pchMessageRec+2),
				*(pchMessageRec+3),
				*(pchMessageRec+4),
				*(pchMessageRec+5),
				*(pchMessageRec+6),
				*(pchMessageRec+7)
				);
		}
		else
		{
			_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "StreamToArray [%d] MessageID was not ncomevent! %d Message bytes ", clock(), nSafeBufferLength);
			int st=0; 
			i=0;
			while((st<ADC_ERRORSTRING_LEN-5)&&(i<nSafeBufferLength))
			{
				if( (*(pchMessageRec+i)>31) && (*(pchMessageRec+i)<127)  )
				{
					sprintf(hexbuf, "%c", *(pchMessageRec+i));
				}
				else
				{
					sprintf(hexbuf, "[%02x]", *(pchMessageRec+i));
				}
				strcat(errorstring, hexbuf);
				i++;
				st = strlen(errorstring);
			}
		}
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:StreamToArray"); //Sleep(50);
		return ADC_ERROR;
	}
	if(pmsgrc->thenrevent.nlistcmd != natget)
	{
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "StreamToArray [%d] list command was not natget! Message bytes [%02x][%02x][%02x][%02x][%02x][%02x][%02x][%02x]", clock(),
			*(pchMessageRec+0),
			*(pchMessageRec+1),
			*(pchMessageRec+2),
			*(pchMessageRec+3),
			*(pchMessageRec+4),
			*(pchMessageRec+5),
			*(pchMessageRec+6),
			*(pchMessageRec+7)
			);
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:StreamToArray"); //Sleep(50);
		
		return ADC_ERROR;
	}

	if(nNumLimitEvents>0)
	{
		// it's a file or a stream that is not a messagerec, have to use the passed in number.
		pch = pchMessageRec;
		nNumEvents = nNumLimitEvents;
		pchEvent = pch;
	}
	else
	{
		nNumEvents = (int)((unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch)));

		if(m_pchDebugFilename)
		{
			FILE* fp = fopen(m_pchDebugFilename, "ab");
			if(fp)
			{
				fwrite(pchMessageRec, 1, 8, fp);
				fclose(fp);
			}
		}
	}



	int nReceivedStartIndex = (int)((unsigned short)((((unsigned long)((unsigned char) *(pch-1))) << 8) + (unsigned char)(*(pch-2))));


	if((nNumLimitEvents<=0)&&(nStartIndex!=nReceivedStartIndex))  // dont enforce this on passed in streams like for traffic
	{
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] stream begins at index %d events, destination value %d", clock(), nReceivedStartIndex, nStartIndex);
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:StreamToArray");// Sleep(50);

		return ADC_ERROR;  // have to!
	}
//	CString foo; 
//	foo.Format("converting %d events", nNumEvents);
//	AfxMessageBox(foo);

	CAEvent* pEvent = NULL;

  for (i=0; i<nNumEvents; i++)
  {
		pEvent = NULL;
		if(pList->m_ppEvents[nStartIndex+i]==NULL)
		{
			pEvent = new CAEvent;
			pList->m_ppEvents[nStartIndex+i] = pEvent;
		}
		else
		{
			pEvent = pList->m_ppEvents[nStartIndex+i];
		}

		char* pchBuffer=NULL;


/*
		FILE* fp;
		fp=fopen("stream611.lst", "ab");
		if(fp)
		{
			fprintf(fp, "*>");
			fclose(fp);
		}
*/

		if(*pchEvent == 0x01)
		{
//foo.Format("event %d: extended", i);	AfxMessageBox(foo);
    // extended type
			pEvent->m_usType = ((revent*)pchEvent)->ExtendedType;

/*  list of events with problems in v12
BREAKEVENT = $10;
LOGOEVENT = $20;
BREAKSYNC = $21;

SECAUDIOEVENT = $81;
SECVIDEOEVENT = $82;

SECKEYEVENT = $83;
SECTRANSKEY = $84; (* Secondary transition key, only if switcher can *)

SECBACKTIMEGPI = $88; { BackTimed from Event }
SECGPIEVENT = $89; (* From Beginning *)

SECTRANSAUDOVER = $90; { Bring on Audio with transition }
SECAUDOVER = $91; { Bring on Audio when ever }

SECAUDIOROUT = $B2;

SECCOMPILEID = $E1;
SECAPPFLAG = $E2; { APPENDING FILE EVENT }
SECBARTERSPOT = $E3; { barter spot for asrun logs }
*/

			// EXTENDED DATA EVENTS
			if (
				  (pEvent->m_usType == 0x0100) ||
					(pEvent->m_usType == 0x0180) ||
					(pEvent->m_usType == 0x0185) ||
					(pEvent->m_usType == 0x01A0) ||

					// list of events with problems in v12

					(pEvent->m_usType == 0x0010) ||  // new with v12
					(pEvent->m_usType == 0x0020) ||  // new with v12
					(pEvent->m_usType == 0x0021) ||  // new with v12
					(pEvent->m_usType == 0x0081) ||  // new with v12
					(pEvent->m_usType == 0x0082) ||  // new with v12
					(pEvent->m_usType == 0x0083) ||  // new with v12
					(pEvent->m_usType == 0x0084) ||  // new with v12
					(pEvent->m_usType == 0x0088) ||  // new with v12
					(pEvent->m_usType == 0x0089) ||  // new with v12
					(pEvent->m_usType == 0x0090) ||  // new with v12
					(pEvent->m_usType == 0x0091) ||  // new with v12
					(pEvent->m_usType == 0x00B2) ||  // new with v12
					(pEvent->m_usType == 0x00E1) ||  // new with v12
					(pEvent->m_usType == 0x00E2) ||  // new with v12
					(pEvent->m_usType == 0x00E3)     // new with v12

				 )
			{
				unsigned long ulDataLength = 0;
				unsigned long ulDataPointer = 0;

				if(pEvent)
				{
					// ID.
					// only reset if different
					pch = (unsigned char*)&(((revent*)pchEvent)->eid[0]);
					ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
	//					ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

					if(pEvent->m_pszID) 
					{

//char bx[256]; sprintf(bx, "event 0x%04x (%s) - %d", pEvent->m_usType, pEvent->m_pszID, clock());
//Message(MSG_PRI_HIGH|MSG_ICONHAND, bx, "CAList:EnsureAllocated"); Sleep(10);

						if( (ulDataLength == strlen(pEvent->m_pszID))
							&&(memcmp(pEvent->m_pszID, (char*)pch, ulDataLength) == 0)) // dont do anything.
						{
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszID); //clear old data if any.
							pEvent->m_pszID = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the ID string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszID = pchBuffer;
					}

					// Title.
					// only reset if different
					pch = (unsigned char*)&(((revent*)pchEvent)->etitle[0]);
					ulDataLength = FieldLength((char*)pch, titlelen, chNulls, 2);
	//					ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

					if(pEvent->m_pszTitle)
					{
						if( (ulDataLength == strlen(pEvent->m_pszTitle))
							&&(memcmp(pEvent->m_pszTitle, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszTitle); //clear old data if any.
							pEvent->m_pszTitle = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the Title string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszTitle = pchBuffer;
					}

					// Reconcile Key.
					// only reset if different
					pch = (unsigned char*)&(((revent*)pchEvent)->reconcilekey[0]);
					ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
	//					ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

					if(pEvent->m_pszReconcileKey)
					{
						if( (ulDataLength == strlen(pEvent->m_pszReconcileKey))
							&&(memcmp(pEvent->m_pszReconcileKey, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszReconcileKey); //clear old data if any.
							pEvent->m_pszReconcileKey = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the reckey string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszReconcileKey = pchBuffer;
					}

					// Extended data.  // always reset this.  have to malloc to compare encoded anyway, so just reset it.
					if(pEvent->m_pszData)
					{
						free(pEvent->m_pszData); //clear old data if any.
						pEvent->m_pszData = NULL;
					}
//				}


					if (
							(pEvent->m_usType == 0x0100) ||
							(pEvent->m_usType == 0x0180) ||
							(pEvent->m_usType == 0x0185) ||
							(pEvent->m_usType == 0x01A0)
						 )
					{
						pch = pchEvent + 107;

						ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
						if(ulDataLength>4096) ulDataLength=4096;

						ulDataPointer = 109 + ulDataLength; // 107+2 bytes for the data length bytes.
					}
					else
					{
						ulDataPointer = 107;
						ulDataLength = 0;
					}


//				if(pEvent)
//				{
					if(ulDataLength>0)
					{
						pchBuffer = EncodeZero((char*)pch+2, &ulDataLength, ZERO_ENCODE_USEZEROTERM);
						pEvent->m_pszData = pchBuffer;
					}
					else
					{
						pEvent->m_pszData=NULL;
					}
// was this, but segment is BCD.
//					pEvent->m_ucSegment = ((revent*)pchEvent)->esegment; //null = SEGMENT_NOT_DEFINED;
// was this, but then this broke 0xff for null, in v2.1.1.20
//					pEvent->m_ucSegment = (((revent*)pchEvent)->esegment&0x0f)|(((((revent*)pchEvent)->esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					//v2.1.1.21
					pEvent->m_ucSegment = ((revent*)pchEvent)->esegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(((revent*)pchEvent)->esegment&0x0f)|(((((revent*)pchEvent)->esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					pEvent->m_ulOnAirTimeMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->oahour,
								((revent*)pchEvent)->oamin,
								((revent*)pchEvent)->oasec,
								((revent*)pchEvent)->oaframe,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_usOnAirJulianDate = ((revent*)pchEvent)->datetoair;  //offset from January 1, 1900
					pEvent->m_ulDurationMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->edurh,
								((revent*)pchEvent)->edurm,
								((revent*)pchEvent)->edurs,
								((revent*)pchEvent)->edurf,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_ulSOMMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->esomh,
								((revent*)pchEvent)->esomm,
								((revent*)pchEvent)->esoms,
								((revent*)pchEvent)->esomf,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_usStatus = ((revent*)pchEvent)->eventstatus;
					pEvent->m_usDevice = (((((revent*)pchEvent)->emsindex)<<8)&0xff00) | ((((revent*)pchEvent)->elsindex)&0x00ff);
					// was this.
//					pEvent->m_usControl = ((revent*)pchEvent)->eventcontrol;
					// now, increased size of control to unsigned long and storing extrathree (extended event control) in the upper word.
					pEvent->m_ulControl = ((((revent*)pchEvent)->eventcontrol&0x0000ffff) | (((((revent*)pchEvent)->extrathree)<<16)&0x00ff0000) );
				}

/*
				FILE* fp;
				fp=fopen("stream611.lst", "ab");
				if(fp)
				{
					fwrite(pchEvent, 1, ulDataPointer,fp);
					fclose(fp);
				}
*/

				if(m_pchDebugFilename)
				{
					FILE* fp = fopen(m_pchDebugFilename, "ab");
					if(fp)
					{
						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "\n\nEVENT[%04x]-START>", pEvent->m_usType);
						}

						fwrite(pchEvent, 1, ulDataPointer, fp);

						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "<EVENT[%04x]-END\n\n", pEvent->m_usType);
						}
						fclose(fp);
					}
				}


				pchEvent += ulDataPointer;
				nReturn++;
			}
			// EXTENDED ID/TITLE EVENTS
			else if (
					(pEvent->m_usType == 0x0000) ||
					(pEvent->m_usType == 0x0080) ||
					(pEvent->m_usType == 0x0085) ||
					(pEvent->m_usType == 0x00E0) ||
					(pEvent->m_usType == 0x00A0) ||
					(pEvent->m_usType == 0x00A4) ||
					(pEvent->m_usType == 0x00A5) ||
					(pEvent->m_usType == 0x00B0) ||
					(pEvent->m_usType == 0x00B1) ||
					(pEvent->m_usType == 0x00B5))
			{
				unsigned long ulDataLength = 0;
				unsigned long ulDataPointer = 0;

				// ID.
				// only reset if different

				char tempbuffer[36]; // only need 32.
				//first see if theres any extended ID data.
				pch = pchEvent + 107;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				
				pchBuffer = NULL;

				if(ulDataLength==0)
				{
					ulDataPointer = 109;
					pch = (unsigned char*)&(((revent*)pchEvent)->eid[0]);
					ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
//						ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.
				}
				else
				{
					ulDataPointer = 109+ulDataLength;
					pch = (unsigned char*)&(((revent*)pchEvent)->eid[0]);
//					pchBuffer = (char*)malloc(ulDataLength+idlen+1); // 8 original + 1 for zero term.
					memcpy(tempbuffer, pch, idlen);
					memcpy(tempbuffer+idlen, pchEvent+109, ulDataLength);
					memset(tempbuffer+idlen+ulDataLength, 0, 1);
					pch = (unsigned char*) tempbuffer;
					ulDataLength+=idlen;
				}

				if(pEvent)
				{
					if(pEvent->m_pszID) 
					{
//char bx[256]; sprintf(bx, "event 0x%04x (%s) - %d", pEvent->m_usType, pEvent->m_pszID, clock());
//Message(MSG_PRI_HIGH|MSG_ICONHAND, bx, "CAList:EnsureAllocated"); Sleep(10);
						if( (ulDataLength == strlen(pEvent->m_pszID))
							&&(memcmp(pEvent->m_pszID, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
//							if(pchBuffer) free(pchBuffer);
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszID); //clear old data if any.
							pEvent->m_pszID = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the ID string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszID = pchBuffer;
					}
				}

				// Title.
				// only reset if different

				//first see if theres any extended Title data.
				pch = pchEvent + ulDataPointer;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));

				pchBuffer = NULL;

				if(ulDataLength==0)
				{
					ulDataPointer += 2;
					pch = (unsigned char*)&(((revent*)pchEvent)->etitle[0]);
					ulDataLength = FieldLength((char*)pch, titlelen, chNulls, 2);
//						ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.
				}
				else
				{
					ulDataPointer += 2;
					pch = (unsigned char*)&(((revent*)pchEvent)->etitle[0]);
//					pchBuffer = (char*)malloc(ulDataLength+titlelen+1); // 16 original + 1 for zero term.
					memcpy(tempbuffer, pch, titlelen);
					memcpy(tempbuffer+titlelen, pchEvent+ulDataPointer, ulDataLength);
					memset(tempbuffer+titlelen+ulDataLength, 0, 1);
					pch = (unsigned char*) tempbuffer;
					ulDataPointer += ulDataLength;
					ulDataLength+=titlelen;
				}

				
				if(pEvent)
				{
					if(pEvent->m_pszTitle)
					{
						if( (ulDataLength == strlen(pEvent->m_pszTitle))
							&&(memcmp(pEvent->m_pszTitle, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
//							if(pchBuffer) free(pchBuffer);
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszTitle); //clear old data if any.
							pEvent->m_pszTitle = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the Title string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszTitle = pchBuffer;
					}

					// Reconcile Key.
					// only reset if different
					pch = (unsigned char*)&(((revent*)pchEvent)->reconcilekey[0]);
					ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
	//						ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

					if(pEvent->m_pszReconcileKey)
					{
						if( (ulDataLength == strlen(pEvent->m_pszReconcileKey))
							&&(memcmp(pEvent->m_pszReconcileKey, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszReconcileKey); //clear old data if any.
							pEvent->m_pszReconcileKey = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the ID string.
						pchBuffer = EncodeZero((char*)pch, &ulDataLength, ZERO_ENCODE_USEZEROTERM);
						pEvent->m_pszReconcileKey = pchBuffer;
					}

					// No Extended data.  
					if(pEvent->m_pszData) free(pEvent->m_pszData); //clear old data if any.
					pEvent->m_pszData = NULL;


// was this, but segment is BCD.
//					pEvent->m_ucSegment = ((revent*)pchEvent)->esegment; //null = SEGMENT_NOT_DEFINED;
// was this, but then this broke 0xff for null, in v2.1.1.20
//					pEvent->m_ucSegment = (((revent*)pchEvent)->esegment&0x0f)|(((((revent*)pchEvent)->esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					//v2.1.1.21
					pEvent->m_ucSegment = ((revent*)pchEvent)->esegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(((revent*)pchEvent)->esegment&0x0f)|(((((revent*)pchEvent)->esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					pEvent->m_ulOnAirTimeMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->oahour,
								((revent*)pchEvent)->oamin,
								((revent*)pchEvent)->oasec,
								((revent*)pchEvent)->oaframe,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_usOnAirJulianDate = ((revent*)pchEvent)->datetoair;  //offset from January 1, 1900
					pEvent->m_ulDurationMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->edurh,
								((revent*)pchEvent)->edurm,
								((revent*)pchEvent)->edurs,
								((revent*)pchEvent)->edurf,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_ulSOMMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->esomh,
								((revent*)pchEvent)->esomm,
								((revent*)pchEvent)->esoms,
								((revent*)pchEvent)->esomf,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_usDevice = (((((revent*)pchEvent)->emsindex)<<8)&0xff00) | ((((revent*)pchEvent)->elsindex)&0x00ff);
					pEvent->m_usStatus = ((revent*)pchEvent)->eventstatus;
					// was this.
//					pEvent->m_usControl = ((revent*)pchEvent)->eventcontrol;
					// now, increased size of control to unsigned long and storing extrathree (extended event control) in the upper word.
					pEvent->m_ulControl = ((((revent*)pchEvent)->eventcontrol&0x0000ffff) | (((((revent*)pchEvent)->extrathree)<<16)&0x00ff0000) );
				}
/*
				FILE* fp;
				fp=fopen("stream611.lst", "ab");
				if(fp)
				{
					fwrite(pchEvent, 1, ulDataPointer,fp);
					fclose(fp);
				}
*/

				if(m_pchDebugFilename)
				{
					FILE* fp = fopen(m_pchDebugFilename, "ab");
					if(fp)
					{
						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "\n\nEVENT[%04x]-START>", pEvent->m_usType);
						}

						fwrite(pchEvent, 1, ulDataPointer, fp);

						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "<EVENT[%04x]-END\n\n", pEvent->m_usType);
						}
						fclose(fp);
					}
				}

				pchEvent += ulDataPointer;
				nReturn++;
			}
			// EXTENDED ID/TITLE EVENTS WITH EXTENDED DATA
			else if (
					(((revent*)pchEvent)->ExtendedType == 0x0200) ||
					(((revent*)pchEvent)->ExtendedType == 0x0280))
			{

				unsigned long ulDataLength = 0;
				unsigned long ulDataPointer = 0;

				// ID.
				// only reset if different

				char tempbuffer[36]; // only need 32.
				//first see if theres any extended ID data.
				pch = pchEvent + 107;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				
				pchBuffer = NULL;

				if(ulDataLength==0)
				{
					ulDataPointer = 109;
					pch = (unsigned char*)&(((revent*)pchEvent)->eid[0]);
					ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
//						ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.
				}
				else
				{
					ulDataPointer = 109+ulDataLength;
					pch = (unsigned char*)&(((revent*)pchEvent)->eid[0]);
//					pchBuffer = (char*)malloc(ulDataLength+idlen+1); // 8 original + 1 for zero term.
					memcpy(tempbuffer, pch, idlen);
					memcpy(tempbuffer+idlen, pchEvent+109, ulDataLength);
					memset(tempbuffer+idlen+ulDataLength, 0, 1);
					pch = (unsigned char*) tempbuffer;
					ulDataLength+=idlen;
				}

				if(pEvent)
				{
					if(pEvent->m_pszID) 
					{
//char bx[256]; sprintf(bx, "event 0x%04x (%s) - %d", pEvent->m_usType, pEvent->m_pszID, clock());
//Message(MSG_PRI_HIGH|MSG_ICONHAND, bx, "CAList:EnsureAllocated"); Sleep(10);
						if( (ulDataLength == strlen(pEvent->m_pszID))
							&&(memcmp(pEvent->m_pszID, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
	//						if(pchBuffer) free(pchBuffer);
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszID); //clear old data if any.
							pEvent->m_pszID = NULL;
						}
					}
				
					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the ID string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszID = pchBuffer;
					}

				}
				// Title.
				// only reset if different

				//first see if theres any extended Title data.
				pch = pchEvent + ulDataPointer;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));

				pchBuffer = NULL;

				if(ulDataLength==0)
				{
					ulDataPointer += 2;
					pch = (unsigned char*)&(((revent*)pchEvent)->etitle[0]);
					ulDataLength = FieldLength((char*)pch, titlelen, chNulls, 2);
//						ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.
				}
				else
				{
					ulDataPointer += 2;
					pch = (unsigned char*)&(((revent*)pchEvent)->etitle[0]);
//					pchBuffer = (char*)malloc(ulDataLength+titlelen+1); // 16 original + 1 for zero term.
					memcpy(tempbuffer, pch, titlelen);
					memcpy(tempbuffer+titlelen, pchEvent+ulDataPointer, ulDataLength);
					memset(tempbuffer+titlelen+ulDataLength, 0, 1);
					pch = (unsigned char*) tempbuffer;
					ulDataPointer += ulDataLength;
					ulDataLength+=titlelen;
				}

				
				if(pEvent)
				{
					if(pEvent->m_pszTitle)
					{
						if( (ulDataLength == strlen(pEvent->m_pszTitle))
							&&(memcmp(pEvent->m_pszTitle, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
							if(pchBuffer) free(pchBuffer);
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszTitle); //clear old data if any.
							pEvent->m_pszTitle = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the title string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszTitle = pchBuffer;
					}

					// Reconcile Key.
					// only reset if different
					pch = (unsigned char*)&(((revent*)pchEvent)->reconcilekey[0]);
					ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
	//						ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

					if(pEvent->m_pszReconcileKey)
					{
						if( (ulDataLength == strlen(pEvent->m_pszReconcileKey))
							&&(memcmp(pEvent->m_pszReconcileKey, (char*)pch, ulDataLength) == 0)) //dont do anything.
						{
							pch = NULL;
						}
						else
						{
							free(pEvent->m_pszReconcileKey); //clear old data if any.
							pEvent->m_pszReconcileKey = NULL;
						}
					}

					if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
					{
						// allocate the ID string.
						pchBuffer = (char*)malloc(ulDataLength+1);
						if(pchBuffer)
						{
							memcpy(pchBuffer, pch, ulDataLength);
							*(pchBuffer+ulDataLength)=0;
						}
						pEvent->m_pszReconcileKey = pchBuffer;
					}

					// Extended data.  // always reset this.  have to malloc to compare encoded anyway, so just reset it.
					if(pEvent->m_pszData) free(pEvent->m_pszData); //clear old data if any.

					pEvent->m_pszData = NULL;
				}

				pch = pchEvent + ulDataPointer;
				ulDataLength = (unsigned short)((((unsigned long)((unsigned char) *(pch+1))) << 8) + (unsigned char)(*pch));
				if(ulDataLength>4096) ulDataLength=4096;

				ulDataPointer += ulDataLength + 2; // 2 bytes for the data length bytes.

				if(pEvent)
				{
					if(ulDataLength>0)
					{
						pchBuffer = EncodeZero((char*)pch+2, &ulDataLength, ZERO_ENCODE_USEZEROTERM);
						pEvent->m_pszData = pchBuffer;
					}
					else
					{
						pEvent->m_pszData=NULL;
					}

// was this, but segment is BCD.
//					pEvent->m_ucSegment = ((revent*)pchEvent)->esegment; //null = SEGMENT_NOT_DEFINED;
// was this, but then this broke 0xff for null, in v2.1.1.20
//					pEvent->m_ucSegment = (((revent*)pchEvent)->esegment&0x0f)|(((((revent*)pchEvent)->esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					//v2.1.1.21
					pEvent->m_ucSegment = ((revent*)pchEvent)->esegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(((revent*)pchEvent)->esegment&0x0f)|(((((revent*)pchEvent)->esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					pEvent->m_ulOnAirTimeMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->oahour,
								((revent*)pchEvent)->oamin,
								((revent*)pchEvent)->oasec,
								((revent*)pchEvent)->oaframe,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_usOnAirJulianDate = ((revent*)pchEvent)->datetoair;  //offset from January 1, 1900
					pEvent->m_ulDurationMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->edurh,
								((revent*)pchEvent)->edurm,
								((revent*)pchEvent)->edurs,
								((revent*)pchEvent)->edurf,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_ulSOMMS = 
						ConvertHMSFToMilliseconds(
								((revent*)pchEvent)->esomh,
								((revent*)pchEvent)->esomm,
								((revent*)pchEvent)->esoms,
								((revent*)pchEvent)->esomf,
								ucFrameBasis, ADC_HEX
							);
					pEvent->m_usDevice = (((((revent*)pchEvent)->emsindex)<<8)&0xff00) | ((((revent*)pchEvent)->elsindex)&0x00ff);
					pEvent->m_usStatus = ((revent*)pchEvent)->eventstatus;
					// was this.
//					pEvent->m_usControl = ((revent*)pchEvent)->eventcontrol;
					// now, increased size of control to unsigned long and storing extrathree (extended event control) in the upper word.
					pEvent->m_ulControl = ((((revent*)pchEvent)->eventcontrol&0x0000ffff) | (((((revent*)pchEvent)->extrathree)<<16)&0x00ff0000) );
				}

/*
				FILE* fp;
				fp=fopen("stream611.lst", "ab");
				if(fp)
				{
					fwrite(pchEvent, 1, ulDataPointer,fp);
					fclose(fp);
				}
*/


				if(m_pchDebugFilename)
				{
					FILE* fp = fopen(m_pchDebugFilename, "ab");
					if(fp)
					{
						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "\n\nEVENT[%04x]-START>", pEvent->m_usType);
						}

						fwrite(pchEvent, 1, ulDataPointer, fp);
						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "<EVENT[%04x]-END\n\n", pEvent->m_usType);
						}
						fclose(fp);
					}
				}

				pchEvent += ulDataPointer;
				nReturn++;
			}
			else
			{
				//shouldnt get here.
				// unknown type.  we dont know how to process...
				//we dont know how long, dont know how far to skip ahead...
/*
				FILE* fp;
				fp=fopen("stream611.lst", "ab");
				if(fp)
				{
					fprintf(fp, "*ERROR extended type was 0x%04x, type was 0x%02x ERROR*", ((revent*)pchEvent)->ExtendedType,  ((revent*)pchEvent)->eventtype);
					fclose(fp);
				}
*/

				if(m_pchDebugFilename)
				{
					FILE* fp = fopen(m_pchDebugFilename, "ab");
					if(fp)
					{
						fprintf(fp, "\n\n*ERROR extended type was 0x%04x, type was 0x%02x ERROR\n\n*", ((revent*)pchEvent)->ExtendedType,  ((revent*)pchEvent)->eventtype);
						fclose(fp);
					}
				}
			
			}
		}
		else  // it's not an extended event
		{
			unsigned long ulDataLength = 0;
//foo.Format("event %d: not extended", i);	AfxMessageBox(foo);

			// not extended type
			// have to create a temporary event to deal with the old revent structure, translate to the new.
			revent EventEx;

			EventEx.Type = 0;
			EventEx.ExtendedType = 0; 
			
			unsigned char* pucTemp = (unsigned char *)(&EventEx);
			pucTemp += 3;
			memcpy(pucTemp, pchEvent, 104);

			if(pEvent)
			{
				pEvent->m_usType = EventEx.eventtype;
//AfxMessageBox(EventEx.eid);

				// ID
				// only reset if different
				pch = (unsigned char*)&(EventEx.eid[0]);
				ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
//					ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.
//foo.Format("FieldLength %d", ulDataLength);	AfxMessageBox(foo);

				if(pEvent->m_pszID) 
				{
					if( (ulDataLength == strlen(pEvent->m_pszID))
						&&(memcmp(pEvent->m_pszID, (char*)pch, ulDataLength) == 0)) //dont do anything.
					{
						pch = NULL;
					}
					else
					{
						free(pEvent->m_pszID); //clear old data if any.
						pEvent->m_pszID = NULL;
					}
				}
//	AfxMessageBox("X2");

				if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
				{
					// allocate the ID string.
					pchBuffer = (char*)malloc(ulDataLength+1);
					if(pchBuffer)
					{
						memcpy(pchBuffer, pch, ulDataLength);
						*(pchBuffer+ulDataLength)=0;
					}
					pEvent->m_pszID = pchBuffer;
				}
//	AfxMessageBox(pEvent->m_pszID);
				// Title.
				// only reset if different
				pch = (unsigned char*)&(EventEx.etitle[0]);
				ulDataLength = FieldLength((char*)pch, titlelen, chNulls, 2);
	//					ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

				if(pEvent->m_pszTitle)
				{
					if( (ulDataLength == strlen(pEvent->m_pszTitle))
						&&(memcmp(pEvent->m_pszTitle, (char*)pch, ulDataLength) == 0)) //dont do anything.
					{
						pch = NULL;
					}
					else
					{
						free(pEvent->m_pszTitle); //clear old data if any.
						pEvent->m_pszTitle = NULL;
					}
				}

				if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
				{
					// allocate the Title string.
					pchBuffer = (char*)malloc(ulDataLength+1);
					if(pchBuffer)
					{
						memcpy(pchBuffer, pch, ulDataLength);
						*(pchBuffer+ulDataLength)=0;
					}
					pEvent->m_pszTitle = pchBuffer;
				}

				// Reconcile Key.
				// only reset if different
				pch = (unsigned char*)&(EventEx.reconcilekey[0]);
				ulDataLength = FieldLength((char*)pch, idlen, chNulls, 2);
	//					ulDataLength = strlen((char*)pch);  // cant use strlen, no zero term.

				if(pEvent->m_pszReconcileKey)
				{
					if( (ulDataLength == strlen(pEvent->m_pszReconcileKey))
						&&(memcmp(pEvent->m_pszReconcileKey, (char*)pch, ulDataLength) == 0)) //dont do anything.
					{
						pch = NULL;
					}
					else
					{
						free(pEvent->m_pszReconcileKey); //clear old data if any.
						pEvent->m_pszReconcileKey = NULL;
					}
				}

				if((ulDataLength>0)&&(pch))  // doesnt reassign if it was the same
				{
					// allocate the reckey string.
					pchBuffer = (char*)malloc(ulDataLength+1);
					if(pchBuffer)
					{
						memcpy(pchBuffer, pch, ulDataLength);
						*(pchBuffer+ulDataLength)=0;
					}
					pEvent->m_pszReconcileKey = pchBuffer;
				}

				// Extended data.  // always reset this.  have to malloc to compare encoded anyway, so just reset it.
				if(pEvent->m_pszData) free(pEvent->m_pszData); //clear old data if any.
				pEvent->m_pszData=NULL;  // no extended data for this

// was this, but segment is BCD.
//				pEvent->m_ucSegment = EventEx.esegment; //null = SEGMENT_NOT_DEFINED;
// was this, but then this broke 0xff for null, in v2.1.1.20
//					pEvent->m_ucSegment = (EventEx.esegment&0x0f)|(((EventEx.esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

					//v2.1.1.21
					pEvent->m_ucSegment = EventEx.esegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(EventEx.esegment&0x0f)|(((EventEx.esegment&0xf0)>>4)*10); //null = SEGMENT_NOT_DEFINED;

				pEvent->m_ulOnAirTimeMS = 
					ConvertHMSFToMilliseconds(
							EventEx.oahour,
							EventEx.oamin,
							EventEx.oasec,
							EventEx.oaframe,
							ucFrameBasis, ADC_HEX
						);
				pEvent->m_usOnAirJulianDate = EventEx.datetoair;  //offset from January 1, 1900
				pEvent->m_ulDurationMS = 
					ConvertHMSFToMilliseconds(
							EventEx.edurh,
							EventEx.edurm,
							EventEx.edurs,
							EventEx.edurf,
							ucFrameBasis, ADC_HEX
						);
				pEvent->m_ulSOMMS = 
					ConvertHMSFToMilliseconds(
							((revent*)pchEvent)->esomh,
							((revent*)pchEvent)->esomm,
							((revent*)pchEvent)->esoms,
							((revent*)pchEvent)->esomf,
							ucFrameBasis, ADC_HEX
						);
				pEvent->m_usDevice = (((EventEx.emsindex)<<8)&0xff00) | ((EventEx.elsindex)&0x00ff);
				pEvent->m_usStatus = EventEx.eventstatus;
					// was this.
//				pEvent->m_usControl = EventEx.eventcontrol;
				// now, increased size of control to unsigned long and storing extrathree (extended event control) in the upper word.
				pEvent->m_ulControl = ( (EventEx.eventcontrol&0x0000ffff) | (((EventEx.extrathree)<<16)&0x00ff0000) );


//CString szfoo; szfoo.Format("o%d, d%d, j%d, sx%02x size=%d",pEvent->m_ulOnAirTimeMS, pEvent->m_ulDurationMS, pEvent->m_usOnAirJulianDate, pEvent->m_usStatus,  sizeof(revent)-3);
//AfxMessageBox(szfoo);
			}
/*
				FILE* fp;
				fp=fopen("stream611.lst", "ab");
				if(fp)
				{
					fwrite(pchEvent, 1, 104 ,fp);
					fclose(fp);
				}
*/
				if(m_pchDebugFilename)
				{
					FILE* fp = fopen(m_pchDebugFilename, "ab");
					if(fp)
					{
						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "\n\nEVENT[%04x]-START>", pEvent->m_usType);
						}

						fwrite(pchEvent, 1, 104, fp);

						if(strstr(m_pchDebugFilename, ".hrd")) // human readable debug file
						{
  						fprintf(fp, "\n\nEVENT[%04x]-START>", pEvent->m_usType);
						}
						fclose(fp);
					}
				}


			pchEvent += 104;
			nReturn++;
		}

/*
		fp=fopen("stream611.lst", "ab");
		if(fp)
		{
			fprintf(fp, "<*");
			fclose(fp);
		}
*/
	}
	return nReturn;
}

int CADC::EventToStream(CAEvent* pEvent, unsigned long ulFlags, unsigned char ucFrameBasis)
{
	if(pEvent==NULL) return ADC_ERROR;

	// have to use 0xff to pad ASCII fields if unused, or ox20 if they are partially filled,
	// so says recfm80 but it is only specifically called out for the ID field.
	// so, currently (31-Oct-2008) only coded that way for ID, but could possibly be that way
	// for reconcile key, title, etc... No harm to keep title 20H padded though, so will do.

  revent*					pSourceEvent =  &m_Event;
	unsigned char**	ppucSourceExtendedEventBuffer = &m_pucExtendedEventBuffer;
	unsigned short*	pusSourceExtendedBufferLength = &m_usExtendedBufferLength;

  revent*					pTargetEvent = &m_VerifyEvent;
	unsigned char**	ppucTargetExtendedEventBuffer = &m_pucVerifyExtendedEventBuffer;
	unsigned short*	pusTargetExtendedBufferLength = &m_usVerifyExtendedBufferLength; 

	if(ulFlags&ADC_TARGET_SWAP_EVENT)
	{
		pSourceEvent = &m_VerifyEvent;
		ppucSourceExtendedEventBuffer = &m_pucVerifyExtendedEventBuffer;
		pusSourceExtendedBufferLength = &m_usVerifyExtendedBufferLength;

		pTargetEvent = &m_Event;
		ppucTargetExtendedEventBuffer = &m_pucExtendedEventBuffer;
		pusTargetExtendedBufferLength = &m_usExtendedBufferLength;
	}

	memcpy(pTargetEvent, pSourceEvent, sizeof(revent)); // fill it in.

	if(ulFlags&ADC_NO_CAEVENT) // take extended data direct from buffer
	{
		if((*pusSourceExtendedBufferLength>0)&&(*ppucSourceExtendedEventBuffer!=NULL))
		{
			if(*ppucTargetExtendedEventBuffer==NULL) 
				*ppucTargetExtendedEventBuffer = (unsigned char*) malloc(*pusSourceExtendedBufferLength);
			else
			if(*pusTargetExtendedBufferLength<*pusSourceExtendedBufferLength) 
			{
				free(*ppucTargetExtendedEventBuffer);
				*ppucTargetExtendedEventBuffer = (unsigned char*) malloc(*pusSourceExtendedBufferLength);
			}

			if(*ppucTargetExtendedEventBuffer==NULL) return ADC_ERROR;
			memcpy(*ppucTargetExtendedEventBuffer, *ppucSourceExtendedEventBuffer, *pusSourceExtendedBufferLength); // fill it in.
			*pusTargetExtendedBufferLength = *pusSourceExtendedBufferLength;
		}
		else
		{
			if(*ppucTargetExtendedEventBuffer) free(*ppucTargetExtendedEventBuffer);
			*pusTargetExtendedBufferLength = 0;
		}
	}
	else	// fields in event override source revent values.
	{
		pTargetEvent->ExtendedType = pEvent->m_usType;
		pTargetEvent->eventtype = (unsigned char)(0xff&(pEvent->m_usType));

		if(ulFlags&ADC_FORCE_EXTENDED)
		{
			pTargetEvent->Type = 0x01;
		}
		else // check it for extended if not already
		if(pTargetEvent->Type == 0x00)
		{
			if((pEvent->m_usType)&0xff00) pTargetEvent->Type = 0x01;
			else
			if((pEvent->m_pszID!=NULL)&&(strlen(pEvent->m_pszID)>idlen)) pTargetEvent->Type = 0x01;
			else
			if((pEvent->m_pszTitle!=NULL)&&(strlen(pEvent->m_pszTitle)>titlelen)) pTargetEvent->Type = 0x01;
			else
			if((pEvent->m_pszData!=NULL)&&(strlen(pEvent->m_pszData)>0)) pTargetEvent->Type = 0x01;
		}
    if (pTargetEvent->Type == 0x01)
    {
			unsigned long ulBufferLength =0;
			unsigned long usBufferLength =0;
      // EXTENDED DATA EVENTS
      if ((pEvent->m_usType == 0x0100) ||
          (pEvent->m_usType == 0x0180) ||
          (pEvent->m_usType == 0x0185) ||
          (pEvent->m_usType == 0x01A0))
      {

				if(pEvent->m_pszID)
				{
					if(strlen(pEvent->m_pszID)>0)
					{
						for(int i =0;i<idlen;i++)
							pTargetEvent->eid[i] = ' ';// must be blank padded if used
						memcpy(&pTargetEvent->eid[0], pEvent->m_pszID, min(idlen,strlen(pEvent->m_pszID)));
					}
					else
					{
						for(int i =0;i<idlen;i++)
							pTargetEvent->eid[i] = (char)0xff;  // must use 0xff null if unused.
					}
				}

				if(pEvent->m_pszTitle)
				{
					for(int i=0;i<titlelen;i++)
					pTargetEvent->etitle[i]=' '; // must be blank padded.
					memcpy(&pTargetEvent->etitle[0], pEvent->m_pszTitle, min(titlelen,strlen(pEvent->m_pszTitle)));
				}

				// pEvent->m_pszData;  //must decode zero.
				if(pEvent->m_pszData)
				{
					ulBufferLength = strlen(pEvent->m_pszData);
					char* pch = DecodeZero(pEvent->m_pszData, &ulBufferLength);
					ulBufferLength += 2;


					//ulBufferLength is now calculated.
					if(*ppucTargetExtendedEventBuffer)
					{
						try
						{
							free(*ppucTargetExtendedEventBuffer);
						}
						catch(...)
						{
						}
					}
					*ppucTargetExtendedEventBuffer = NULL;
					*pusTargetExtendedBufferLength = 0;

					*ppucTargetExtendedEventBuffer = (unsigned char*)malloc(ulBufferLength);
					if(*ppucTargetExtendedEventBuffer==NULL) return ADC_ERROR;

					usBufferLength = (unsigned short)(0xffff&ulBufferLength);
					memcpy(*ppucTargetExtendedEventBuffer, &usBufferLength, 2);
					if(pch)
					{
						memcpy((*ppucTargetExtendedEventBuffer)+2, pch, ulBufferLength-2);
						*pusTargetExtendedBufferLength = (unsigned short)ulBufferLength;

						free(pch);
					}
				}

				if(pEvent->m_pszReconcileKey)
				{
					for(int i =0;i<idlen;i++)
						pTargetEvent->reconcilekey[i] = (char)0xff;
					memcpy(&pTargetEvent->reconcilekey[0], pEvent->m_pszReconcileKey, min(idlen,strlen(pEvent->m_pszReconcileKey)));
				}

// was this, but segment is BCD.
//				pTargetEvent->esegment = pEvent->m_ucSegment;
// was this, but then this broke 0xff for null, in v2.1.1.20
//				pTargetEvent->esegment = (pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

					//v2.1.1.21
					pTargetEvent->esegment = pEvent->m_ucSegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

				ConvertMillisecondsToHMSF(
					pEvent->m_ulOnAirTimeMS,
					&(pTargetEvent->oahour),
					&(pTargetEvent->oamin),
					&(pTargetEvent->oasec),
					&(pTargetEvent->oaframe),
					ucFrameBasis, ADC_HEX);

				pTargetEvent->datetoair = pEvent->m_usOnAirJulianDate;  //offset from January 1, 1900

				ConvertMillisecondsToHMSF(
					pEvent->m_ulDurationMS,
					&(pTargetEvent->edurh),
					&(pTargetEvent->edurm),
					&(pTargetEvent->edurs),
					&(pTargetEvent->edurf),
					ucFrameBasis, ADC_HEX);

				ConvertMillisecondsToHMSF(
					pEvent->m_ulSOMMS,
					&(pTargetEvent->esomh),
					&(pTargetEvent->esomm),
					&(pTargetEvent->esoms),
					&(pTargetEvent->esomf),
					ucFrameBasis, ADC_HEX);
				
				pTargetEvent->eventstatus = pEvent->m_usStatus;
				pTargetEvent->emsindex = (unsigned char)((pEvent->m_usDevice&0xff00)>>8);
				pTargetEvent->elsindex = (unsigned char)(pEvent->m_usDevice&0x00ff);
				// was this:
//				pTargetEvent->eventcontrol = pEvent->m_usControl;
				// now changed to this to include extended control
				pTargetEvent->eventcontrol = (unsigned short)(pEvent->m_ulControl&0x0000ffff);
				pTargetEvent->extrathree = (unsigned char)((pEvent->m_ulControl&0x00ff0000)>>16);
			}
      // EXTENDED ID/TITLE EVENTS
      else if (
          (pEvent->m_usType == 0x0000) ||
          (pEvent->m_usType == 0x0080) ||
          (pEvent->m_usType == 0x0085) ||
          (pEvent->m_usType == 0x00E0) ||
          (pEvent->m_usType == 0x00A0) ||
          (pEvent->m_usType == 0x00A4) ||
          (pEvent->m_usType == 0x00A5) ||
          (pEvent->m_usType == 0x00B0) ||
          (pEvent->m_usType == 0x00B1) ||
          (pEvent->m_usType == 0x00B5))
      {
				usBufferLength = 4; // for 2 words of buffer lengths
				unsigned short usIDLength=0;
				unsigned short usTitleLength=0;
				if(pEvent->m_pszID)
				{
					for(int i =0;i<idlen;i++)
						pTargetEvent->eid[i] = (char)0xff;

					usIDLength = strlen(pEvent->m_pszID);
					if(usIDLength>0)
					{
						for(int i =0;i<idlen;i++)
							pTargetEvent->eid[i] = ' ';// must be blank padded if used
						memcpy(&pTargetEvent->eid[0], pEvent->m_pszID, min(idlen,usIDLength));
					}
					else
					{
						for(int i =0;i<idlen;i++)
							pTargetEvent->eid[i] = (char)0xff;  // must use 0xff null if unused.
					}

					if(usIDLength>idlen)
					{	
						usIDLength-=idlen;
						usBufferLength+=usIDLength;
					}
					else usIDLength =0;

				}

				if(pEvent->m_pszTitle)
				{
					for(int i=0;i<titlelen;i++)
					pTargetEvent->etitle[i]=' '; // must be blank padded.
					usTitleLength = strlen(pEvent->m_pszTitle);
					memcpy(&pTargetEvent->etitle[0], pEvent->m_pszTitle, min(titlelen,usTitleLength));
					if(usTitleLength>titlelen)
					{	
						usTitleLength-=titlelen;
						usBufferLength+=usTitleLength;
					}
					else usTitleLength =0;
				}

				// pEvent->m_pszData;  //must decode zero.
//CString foo;
//foo.Format("i%d t%d",usIDLength,usTitleLength);
//AfxMessageBox(foo);

				ulBufferLength = usBufferLength;
				//ulBufferLength is now calculated.
				if(*ppucTargetExtendedEventBuffer)
				{
					try
					{
						free(*ppucTargetExtendedEventBuffer);
					}
					catch(...)
					{
					}
				}
				*ppucTargetExtendedEventBuffer = NULL;
				*pusTargetExtendedBufferLength = 0;

				*ppucTargetExtendedEventBuffer = (unsigned char*)malloc(ulBufferLength);
				if(*ppucTargetExtendedEventBuffer==NULL) return ADC_ERROR;

				*pusTargetExtendedBufferLength = (unsigned short)ulBufferLength;

/*
foo.Format("*buf %d v%d e%d id%d title%d\r\n%d%d %d%d", *ppucTargetExtendedEventBuffer, m_pucVerifyExtendedEventBuffer, m_pucExtendedEventBuffer,
					 usIDLength, usTitleLength,
					 *((char*)&usIDLength),
					 *((char*)(&usIDLength)+1),
					 *(char*)(&usTitleLength),
					 *((char*)(&usTitleLength)+1)
					 );
AfxMessageBox(foo);
*/
//				*(*ppucTargetExtendedEventBuffer) = *((unsigned char*)&usIDLength);
//				*((*ppucTargetExtendedEventBuffer)+1) = *((unsigned char*)(&usIDLength)+1);
				
				memcpy(*ppucTargetExtendedEventBuffer, &usIDLength, 2); 									
				ulBufferLength = 2;
//foo.Format("ebI %d 00:%d 01:%d",*ppucTargetExtendedEventBuffer, *((char*)(*ppucTargetExtendedEventBuffer)), *((char*)(*ppucTargetExtendedEventBuffer)+1));
//AfxMessageBox(foo);

				if(usIDLength)
				{
					memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, pEvent->m_pszID+idlen, usIDLength);
					ulBufferLength += usIDLength;
//AfxMessageBox(pEvent->m_pszID+idlen);
				}

				memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, &usTitleLength, 2); 
//foo.Format("ebT %d 00:%d 01:%d",(*ppucTargetExtendedEventBuffer)+ulBufferLength, *((char*)((*ppucTargetExtendedEventBuffer)+ulBufferLength)), *((char*)((*ppucTargetExtendedEventBuffer)+ulBufferLength)+1));
//AfxMessageBox(foo);


				ulBufferLength += 2;
				if(usTitleLength) 
				{
					memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, pEvent->m_pszTitle+titlelen, usTitleLength);
//AfxMessageBox(pEvent->m_pszTitle+titlelen);
				}
//AfxMessageBox((char*)(*ppucTargetExtendedEventBuffer)+ulBufferLength);
				if(pEvent->m_pszReconcileKey)
				{
					for(int i =0;i<8;i++)
						pTargetEvent->reconcilekey[i] = (char)0xff;
					memcpy(&pTargetEvent->reconcilekey[0], pEvent->m_pszReconcileKey, min(8,strlen(pEvent->m_pszReconcileKey)));
				}

// was this, but segment is BCD.
//				pTargetEvent->esegment = pEvent->m_ucSegment;
// was this, but then this broke 0xff for null, in v2.1.1.20
//				pTargetEvent->esegment = (pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

					//v2.1.1.21
					pTargetEvent->esegment = pEvent->m_ucSegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

				ConvertMillisecondsToHMSF(
					pEvent->m_ulOnAirTimeMS,
					&(pTargetEvent->oahour),
					&(pTargetEvent->oamin),
					&(pTargetEvent->oasec),
					&(pTargetEvent->oaframe),
					ucFrameBasis, ADC_HEX);

				pTargetEvent->datetoair = pEvent->m_usOnAirJulianDate;  //offset from January 1, 1900

				ConvertMillisecondsToHMSF(
					pEvent->m_ulDurationMS,
					&(pTargetEvent->edurh),
					&(pTargetEvent->edurm),
					&(pTargetEvent->edurs),
					&(pTargetEvent->edurf),
					ucFrameBasis, ADC_HEX);

				ConvertMillisecondsToHMSF(
					pEvent->m_ulSOMMS,
					&(pTargetEvent->esomh),
					&(pTargetEvent->esomm),
					&(pTargetEvent->esoms),
					&(pTargetEvent->esomf),
					ucFrameBasis, ADC_HEX);

				pTargetEvent->eventstatus = pEvent->m_usStatus;
				pTargetEvent->emsindex = (unsigned char)((pEvent->m_usDevice&0xff00)>>8);
				pTargetEvent->elsindex = (unsigned char)(pEvent->m_usDevice&0x00ff);
				// was this:
//				pTargetEvent->eventcontrol = pEvent->m_usControl;
				// now changed to this to include extended control
				pTargetEvent->eventcontrol = (unsigned short)(pEvent->m_ulControl&0x0000ffff);
				pTargetEvent->extrathree = (unsigned char)((pEvent->m_ulControl&0x00ff0000)>>16);


			}
      // EXTENDED ID/TITLE EVENTS WITH EXTENDED DATA
      else if (
          (pEvent->m_usType == 0x0200) ||
          (pEvent->m_usType == 0x0280))
      {
				usBufferLength = 6; // for 3 words of buffer lengths
				unsigned short usIDLength=0;
				unsigned short usTitleLength=0;
				unsigned short usDataLength=0;
				if(pEvent->m_pszID)
				{
					for(int i =0;i<idlen;i++)
						pTargetEvent->eid[i] = (char)0xff;
					usIDLength = strlen(pEvent->m_pszID);
					if(usIDLength>0)
					{
						for(int i =0;i<idlen;i++)
							pTargetEvent->eid[i] = ' ';// must be blank padded if used
						memcpy(&pTargetEvent->eid[0], pEvent->m_pszID, min(idlen,usIDLength));
					}
					else
					{
						for(int i =0;i<idlen;i++)
							pTargetEvent->eid[i] = (char)0xff;  // must use 0xff null if unused.
					}

					if(usIDLength>idlen)
					{	
						usIDLength-=idlen;
						usBufferLength+=usIDLength;
					}
					else usIDLength =0;

				}

				if(pEvent->m_pszTitle)
				{
					for(int i=0;i<titlelen;i++)
					pTargetEvent->etitle[i]=' '; // must be blank padded.
					usTitleLength = strlen(pEvent->m_pszTitle);
					memcpy(&pTargetEvent->etitle[0], pEvent->m_pszTitle, min(titlelen,usTitleLength));
					if(usTitleLength>titlelen)
					{	
						usTitleLength-=titlelen;
						usBufferLength+=usTitleLength;
					}
					else usTitleLength =0;
				}

				// pEvent->m_pszData;  //must decode zero.
				char* pch = NULL;
				if(pEvent->m_pszData)
				{
					ulBufferLength = strlen(pEvent->m_pszData);
					pch = DecodeZero(pEvent->m_pszData, &ulBufferLength);

					usBufferLength +=2;
					usDataLength = (unsigned short)(0xffff&ulBufferLength);
				}

				ulBufferLength = usBufferLength;
					//ulBufferLength is now calculated.
				if(*ppucTargetExtendedEventBuffer)
				{
					try
					{
						free(*ppucTargetExtendedEventBuffer);
					}
					catch(...)
					{
					}
				}
				*ppucTargetExtendedEventBuffer = NULL;
				*pusTargetExtendedBufferLength = 0;

				*ppucTargetExtendedEventBuffer = (unsigned char*)malloc(ulBufferLength);
				if(*ppucTargetExtendedEventBuffer==NULL) return ADC_ERROR;

				*pusTargetExtendedBufferLength = (unsigned short)ulBufferLength;

				memcpy(*ppucTargetExtendedEventBuffer, &usIDLength, 2); 									ulBufferLength = 2;

				if(usIDLength) memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, pEvent->m_pszID+idlen, usIDLength);
				ulBufferLength += usIDLength;

				memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, &usTitleLength, 2); ulBufferLength += 2;

				if(usTitleLength) memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, pEvent->m_pszTitle+titlelen, usTitleLength);
				ulBufferLength += usTitleLength;

				memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, &usDataLength, 2); 	ulBufferLength += 2;

				if(pch)
				{
					memcpy((*ppucTargetExtendedEventBuffer)+ulBufferLength, pch, usDataLength);
					free(pch);
				}

				if(pEvent->m_pszReconcileKey)
				{
					for(int i =0;i<8;i++)
						pTargetEvent->reconcilekey[i] = (char)0xff;
					memcpy(&pTargetEvent->reconcilekey[0], pEvent->m_pszReconcileKey, min(8,strlen(pEvent->m_pszReconcileKey)));
				}

// was this, but segment is BCD.
//				pTargetEvent->esegment = pEvent->m_ucSegment;
// was this, but then this broke 0xff for null, in v2.1.1.20
//				pTargetEvent->esegment = (pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

					//v2.1.1.21
					pTargetEvent->esegment = pEvent->m_ucSegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

				ConvertMillisecondsToHMSF(
					pEvent->m_ulOnAirTimeMS,
					&(pTargetEvent->oahour),
					&(pTargetEvent->oamin),
					&(pTargetEvent->oasec),
					&(pTargetEvent->oaframe),
					ucFrameBasis, ADC_HEX);

				pTargetEvent->datetoair = pEvent->m_usOnAirJulianDate;  //offset from January 1, 1900

				ConvertMillisecondsToHMSF(
					pEvent->m_ulDurationMS,
					&(pTargetEvent->edurh),
					&(pTargetEvent->edurm),
					&(pTargetEvent->edurs),
					&(pTargetEvent->edurf),
					ucFrameBasis, ADC_HEX);

				ConvertMillisecondsToHMSF(
					pEvent->m_ulSOMMS,
					&(pTargetEvent->esomh),
					&(pTargetEvent->esomm),
					&(pTargetEvent->esoms),
					&(pTargetEvent->esomf),
					ucFrameBasis, ADC_HEX);

				pTargetEvent->eventstatus = pEvent->m_usStatus;
				pTargetEvent->emsindex = (unsigned char)((pEvent->m_usDevice&0xff00)>>8);
				pTargetEvent->elsindex = (unsigned char)(pEvent->m_usDevice&0x00ff);
//				pTargetEvent->eventcontrol = pEvent->m_usControl;
				// now changed to this to include extended control
				pTargetEvent->eventcontrol = (unsigned short)(pEvent->m_ulControl&0x0000ffff);
				pTargetEvent->extrathree = (unsigned char)((pEvent->m_ulControl&0x00ff0000)>>16);

			}
			else
			{
				return ADC_UNKNOWN;
			}
		}
		else
		{
			// non-extended event
			if(*ppucTargetExtendedEventBuffer)
			{
				try
				{
					free(*ppucTargetExtendedEventBuffer);
				}
				catch(...)
				{
				}
			}
			*ppucTargetExtendedEventBuffer = NULL;
			*pusTargetExtendedBufferLength = 0;

			if(pEvent->m_pszID)
			{
				if(strlen(pEvent->m_pszID)>0)
				{
					for(int i =0;i<idlen;i++)
						pTargetEvent->eid[i] = ' ';// must be blank padded if used
					memcpy(&pTargetEvent->eid[0], pEvent->m_pszID, min(idlen,strlen(pEvent->m_pszID)));
				}
				else
				{
					for(int i =0;i<idlen;i++)
						pTargetEvent->eid[i] = (char)0xff;  // must use 0xff null if unused.
				}
			}

			if(pEvent->m_pszTitle)
			{
				for(int i=0;i<titlelen;i++)
				pTargetEvent->etitle[i]=' '; // must be blank padded.
				memcpy(&(pTargetEvent->etitle[0]), pEvent->m_pszTitle, min(titlelen,strlen(pEvent->m_pszTitle)));
			}

			// pEvent->m_pszData;  //must decode zero.


			if(pEvent->m_pszReconcileKey)
			{
				for(int i =0;i<8;i++)
					pTargetEvent->reconcilekey[i] = (char)0xff;
				memcpy(&(pTargetEvent->reconcilekey[0]), pEvent->m_pszReconcileKey, min(8,strlen(pEvent->m_pszReconcileKey)));
			}

// was this, but segment is BCD.
//				pTargetEvent->esegment = pEvent->m_ucSegment;
// was this, but then this broke 0xff for null, in v2.1.1.20
//				pTargetEvent->esegment = (pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

			//v2.1.1.21
			pTargetEvent->esegment = pEvent->m_ucSegment==SEGMENT_NOT_DEFINED?SEGMENT_NOT_DEFINED:(pEvent->m_ucSegment%10)|((pEvent->m_ucSegment/10)*16);

			ConvertMillisecondsToHMSF(
				pEvent->m_ulOnAirTimeMS,
				&(pTargetEvent->oahour),
				&(pTargetEvent->oamin),
				&(pTargetEvent->oasec),
				&(pTargetEvent->oaframe),
				ucFrameBasis, ADC_HEX);

			pTargetEvent->datetoair = pEvent->m_usOnAirJulianDate;  //offset from January 1, 1900

			ConvertMillisecondsToHMSF(
				pEvent->m_ulDurationMS,
				&(pTargetEvent->edurh),
				&(pTargetEvent->edurm),
				&(pTargetEvent->edurs),
				&(pTargetEvent->edurf),
				ucFrameBasis, ADC_HEX);

			ConvertMillisecondsToHMSF(
				pEvent->m_ulSOMMS,
				&(pTargetEvent->esomh),
				&(pTargetEvent->esomm),
				&(pTargetEvent->esoms),
				&(pTargetEvent->esomf),
				ucFrameBasis, ADC_HEX);

			pTargetEvent->eventstatus = pEvent->m_usStatus;
			pTargetEvent->emsindex = (unsigned char)((pEvent->m_usDevice&0xff00)>>8);
			pTargetEvent->elsindex = (unsigned char)(pEvent->m_usDevice&0x00ff);
//				pTargetEvent->eventcontrol = pEvent->m_usControl;
			// now changed to this to include extended control
			pTargetEvent->eventcontrol = (unsigned short)(pEvent->m_ulControl&0x0000ffff);
			pTargetEvent->extrathree = (unsigned char)((pEvent->m_ulControl&0x00ff0000)>>16);

		}
	}

	return ADC_SUCCESS;

}

int CADC::InitEvent(unsigned long ulFlags)
{
  revent*					pTargetEvent = &m_VerifyEvent;
	unsigned char**	ppucTargetExtendedEventBuffer = &m_pucVerifyExtendedEventBuffer;
	unsigned short*	pusTargetExtendedBufferLength = &m_usVerifyExtendedBufferLength; 

	if(ulFlags==ADC_SWAP_EVENT)
	{
		pTargetEvent = &m_Event;
		ppucTargetExtendedEventBuffer = &m_pucExtendedEventBuffer;
		pusTargetExtendedBufferLength = &m_usExtendedBufferLength;
	}

	pTargetEvent->Type=0;
  pTargetEvent->ExtendedType=0;

	pTargetEvent->eventtype = 0;  // see eventtype defs, 0 is standard AV event.
	for(int i =0;i<8;i++)
		pTargetEvent->reconcilekey[i] = (char)0xff;//"rec_key");
	pTargetEvent->oaday   = 0;
	pTargetEvent->oamonth = 0;
	pTargetEvent->oayear  = 0;
	pTargetEvent->oaframe = 0xff;
	pTargetEvent->oasec   = 0xff;
	pTargetEvent->oamin   = 0xff;
	pTargetEvent->oahour  = 0xff;

	for(i=0;i<idlen;i++)
		pTargetEvent->eid[i]=(char)0xff;//"ev_id");           // must be blank padded
	for(i=0;i<titlelen;i++)
		pTargetEvent->etitle[i]=' ';//"event title"); // must be blank padded.

	pTargetEvent->esomf   = 0xff;      // make sure all time is BCD
	pTargetEvent->esoms   = 0xff;
	pTargetEvent->esomm   = 0xff;
	pTargetEvent->esomh   = 0xff;
	pTargetEvent->edurf   = 0xff;
	pTargetEvent->edurs   = 0xff;
	pTargetEvent->edurm   = 0xff;
	pTargetEvent->edurh   = 0xff;
	pTargetEvent->echannel   = 0;
	pTargetEvent->qualifier4 = 0;
	pTargetEvent->esegment = 0xff;
	pTargetEvent->emsindex = 0;
	pTargetEvent->elsindex = 0;
	pTargetEvent->ehibin = 0;
	pTargetEvent->elobin = 0;
	pTargetEvent->qualifier1 = 0;
	pTargetEvent->qualifier2 = 0;
	pTargetEvent->qualifier3 = 0;
	pTargetEvent->datetoair  = 0xffff;
	pTargetEvent->eventcontrol = 0;
	pTargetEvent->eventstatus = 0;
	for(i=0;i<8;i++)
		pTargetEvent->compiletape[i]=(char)0xff;//"ev_id");           // must be blank padded
//	strcpy(pTargetEvent->compiletape, "");//"tape"); // must be blank padded
	pTargetEvent->compilesom[0] = 0xff;
	pTargetEvent->compilesom[1] = 0xff;
	pTargetEvent->compilesom[2] = 0xff;
	pTargetEvent->compilesom[3] = 0xff;
	for(i=0;i<8;i++)
		pTargetEvent->abox[i]=(char)0xff;//"ev_id");           // must be blank padded
//	strcpy(pTargetEvent->abox, "");//"a_box");// must be blank padded
	pTargetEvent->aboxsom[0] = 0xff;
	pTargetEvent->aboxsom[1] = 0xff;
	pTargetEvent->aboxsom[2] = 0xff;
	pTargetEvent->aboxsom[3] = 0xff;
	for(i=0;i<8;i++)
		pTargetEvent->bbox[i]=(char)0xff;//"ev_id");           // must be blank padded
//	strcpy(pTargetEvent->bbox, "");//"b_box");// must be blank padded
	pTargetEvent->bboxsom[0] = 0xff;
	pTargetEvent->bboxsom[1] = 0xff;
	pTargetEvent->bboxsom[2] = 0xff;
	pTargetEvent->bboxsom[3] = 0xff;
	pTargetEvent->mspotcontrol = 0;
	pTargetEvent->backupemsindex = 0;
	pTargetEvent->backupelsindex = 0;
	pTargetEvent->extrathree = 0;

	if(*ppucTargetExtendedEventBuffer)
	{
		try
		{
			free(*ppucTargetExtendedEventBuffer);
		}
		catch(...)
		{
		}
	}	
	*ppucTargetExtendedEventBuffer = NULL;
	*pusTargetExtendedBufferLength = 0;

	return ADC_SUCCESS;
}


CAConnection* CADC::ConnectServer(char* pszServerName, char* pszClientName) // returns pointer to connection
{
	if((pszServerName==NULL)||(strlen(pszServerName)<1)) return NULL;
	
//		Message(MSG_PRI_HIGH|MSG_ICONERROR, pszServerName, "ADC:ConnectServer:debug");

	// search for existing connection.
	CAConnection* pconn = ConnectionExists(pszServerName);
	if(pconn!=NULL) return pconn;

	// if not existing connection, connect
 	pconn = new CAConnection;
	if(pconn==NULL) return NULL;  // failure.

	char pstrError[256], pstrClient[17]; // null term
	if((pszClientName==NULL)||(strlen(pszClientName)<1))
	{
		sprintf(pstrClient, "VDS%d", clock()%100000); /// try to be unique, 5 digits, one in 100,000 chance....
		pszClientName = pstrClient;
	}
	if(strlen(pszClientName)>titlelen)
	{
		strncpy(pstrClient, pszClientName, titlelen);
		memset(pstrClient+titlelen, 0, 1);
		pszClientName = pstrClient;
	}

  pconn->m_uchHandle = connect(pszServerName, pszClientName, pstrError);
	if(pconn->m_uchHandle)
	{
		// have to add this connection to register.
		pconn->m_pszServerName = new char[strlen(pszServerName)+1]; //term 0
//		pconn->m_pszServerName = (char*) malloc(strlen(pszServerName)+1); //term 0
		if(pconn->m_pszServerName) strcpy(pconn->m_pszServerName, pszServerName);
		pconn->m_pszClientName = new char[strlen(pszClientName)+1]; //term 0
//		pconn->m_pszClientName = (char*) malloc(strlen(pszClientName)+1); //term 0
		if(pconn->m_pszClientName) strcpy(pconn->m_pszClientName, pszClientName);

    SetupStatusPtr(pconn->m_uchHandle, -1, &pconn->m_SysData);
    SetupStatusPtr(pconn->m_uchHandle,  0, &pconn->m_ListData);  // are c arrays, zero based
// docs say following not required.  we omit, no device data required.
//    for (i=1; i<FULLCHANNELS; i++) SetupStatusPtr(pconn->m_uchHandle, i, &(m_DevBlock[i-1])); // are c arrays, zero based

		// now, add to list.
		CAConnection** ppConn = new CAConnection*[(m_nNumConn+1)];  // pointer to an array of pointers to connection types
//		CAConnection** ppConn = (CAConnection**)malloc((m_nNumConn+1)*sizeof(CAConnection*));  // pointer to an array of pointers to connection types
		if(ppConn)
		{
			if(m_ppConn)
			{
				int nIndex=0;
				while(nIndex<m_nNumConn)
				{
					ppConn[nIndex]=m_ppConn[nIndex];
					nIndex++;
				}
//				memcpy(ppConn, m_ppConn, (m_nNumConn)*sizeof(CAConnection*));
				try { delete [] m_ppConn; } catch(...){}
			}

			m_ppConn = ppConn;
			m_ppConn[m_nNumConn] = pconn;
			m_nNumConn++;
		}  // else just dont add to list. (this shouldnt happen).
		return pconn;
	}
	else // failed
	{
		// send error message back
		char errorstring[ADC_ERRORSTRING_LEN];
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "Harris error [%s] connecting %s to %s", pstrError, pszClientName, pszServerName );
		Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "ADC:ConnectServer");
		try { delete pconn; } catch(...){}
		return NULL;
	}
}

CAConnection* CADC::ConnectionExists(char* pszServerName) // returns pointer to connection
{
	if(m_ppConn)
	{
		for(int i=0; i<m_nNumConn; i++)
		{
			if(m_ppConn[i])
			{
				if(m_ppConn[i]->m_pszServerName!=NULL)
				{
					if(strcmp(m_ppConn[i]->m_pszServerName, pszServerName) == 0 )
					{
						return m_ppConn[i];
					}
				}
			}
		}
	}

	return NULL;
}

int CADC::DisconnectServer(CAConnection* pConn)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle))
	{
		if(m_ppConn)
		{
			for(int i=0; i<m_nNumConn; i++)
			{
				CAConnection* pconn = m_ppConn[i];
				if(m_ppConn[i])
				{
					if(m_ppConn[i]->m_uchHandle!=NULL)
					{
						if(m_ppConn[i]->m_uchHandle == pConn->m_uchHandle )
						{
							if(DisConnect(m_ppConn[i]->m_uchHandle))
							{
								nReturn = ADC_SUCCESS;
							}
							else // DisConnect unsuccessful
							{
								// send error message back
								Message(MSG_PRI_HIGH|MSG_ICONERROR, "DisConnect not successful", "ADC:DisconnectServer");
							}
							// remove from array, even if unsuccessful.
							if(m_nNumConn>1)
							{
								m_nNumConn--;

								CAConnection** ppConn = new CAConnection*[(m_nNumConn)];  // pointer to an array of pointers to connection types
//									CAConnection** ppConn = (CAConnection**)malloc((m_nNumConn)*size);  // pointer to an array of pointers to connection types
								if(ppConn)
								{
									if(m_ppConn)
									{
/*
										if(i>0)	memcpy(ppConn, m_ppConn, (i*size));
										if(i<m_nNumConn)	
											memcpy(
												ppConn+(i*size), 
												m_ppConn+((i+1)*size), 
												((m_nNumConn-i)*size)
												);
*/
										for(int j=0; j<i; j++)
										{
											ppConn[j] = m_ppConn[j];
										}
										while(j<m_nNumConn)
										{
											ppConn[j] = m_ppConn[j+1];
											j++;
										}

										try{ delete [] m_ppConn;	 } catch(...){}				// free the old array.
									}
									m_ppConn = ppConn;		// reassign the now smaller array
								}
								else  // just null it.
								{
									try{ delete m_ppConn[i]; } catch(...){}
									m_ppConn[i] = NULL;
								}
							}
							else // remove everything.
							{
								for(int j=0; j<m_nNumConn; j++)
								{
									if(m_ppConn[j]) { try{ delete m_ppConn[j]; } catch(...){}}
									m_ppConn[j] = NULL;
								}
								try{ delete [] m_ppConn; } catch(...){}
								m_ppConn = NULL;
								m_nNumConn=0;
							}
							
							i=m_nNumConn; //breaks us out of loop
						}
					}
				}
			}
		}
	}
	return nReturn;
}

int CADC::DisconnectServer(unsigned char ucHandle) // searches for connection object
{
	if(ucHandle==NULL) return ADC_ERROR;
	int nReturn = ADC_ERROR;
	if(m_ppConn)
	{
		for(int i=0; i<m_nNumConn; i++)
		{
			if(m_ppConn[i])
			{
				if(m_ppConn[i]->m_uchHandle!=NULL)
				{
					if(m_ppConn[i]->m_uchHandle == ucHandle )
					{
						nReturn = DisconnectServer(m_ppConn[i]);
						i=m_nNumConn; //breaks us out of loop
					}
				}
			}
		}
	}

	if(nReturn == ADC_ERROR)
	{
		if(DisConnect(ucHandle))
		{
			return ADC_SUCCESS;
		}
	}

	return nReturn;
}

int CADC::DisconnectServer(char* pszServerName) // searches for handle and connection object
{
	if((pszServerName==NULL)||(strlen(pszServerName)<1)) return ADC_ERROR;
	int nReturn = ADC_ERROR;
	if(m_ppConn)
	{
		for(int i=0; i<m_nNumConn; i++)
		{
			if(m_ppConn[i])
			{
				if(m_ppConn[i]->m_pszServerName!=NULL)
				{
					if(strcmp(m_ppConn[i]->m_pszServerName, pszServerName) == 0 )
					{
						nReturn = DisconnectServer(m_ppConn[i]);
						i=m_nNumConn; //breaks us out of loop
					}
				}
			}
		}
	}
	return nReturn;
}

int CADC::SendListChange(CAConnection*  pConn, int nList) // List is 1 based
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
		// Notify that the list has changed.
		m_SendData.thenrevent.nlistcmd = nlistchanged;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nlistindex = 0;
		m_SendData.thenrevent.nothervalue = 0;
		m_SendData.thenrevent.numberofevents = 0;
		if(SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec))==0)	nReturn = ADC_SUCCESS;
	}
	return nReturn;
}


int CADC::LockList(CAConnection*  pConn, int nList) // List is 1 based
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
//AfxMessageBox("valid list to lock");
		int nListZero = nList-1;
		// have to see if the list has been locked by another station
		if(GetStatusData(pConn)==ADC_SUCCESS)
		{
			if(pConn->m_ListData[nListZero].packcount>0)
			{
//CString foo; foo.Format("pack count is %d",pConn->m_ListData[nListZero].packcount);
//AfxMessageBox(foo);
				if(pConn->m_pList[nListZero]==NULL)	pConn->m_pList[nListZero] = new CAList;
				if(pConn->m_pList[nListZero]!=NULL) // can only lock list if there's a place to record the packcount.
				{
					pConn->m_pList[nListZero]->m_nPackCount = pConn->m_ListData[nListZero].packcount;
//AfxMessageBox("setting to 0");

					m_SendData.thenrevent.nlistcmd = listpackcount;
					m_SendData.thenrevent.nlistid = nList;
					m_SendData.thenrevent.nlistindex = 0; //	{ pack count value }
					m_SendData.thenrevent.nothervalue = 0;
					m_SendData.thenrevent.numberofevents = 0;
					if(SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec))==0)	nReturn = ADC_SUCCESS;
//foo.Format("return code is %d", nReturn);
//AfxMessageBox(foo);
				}
			}// else have to return false if not locked by this, could be unlocked at any time.
			else
			{
				if(
						( strlen(pConn->m_pszClientName)==(unsigned int)(pConn->m_ListData[nListZero].lockname[0]))
					&&( memcmp(
											&(pConn->m_ListData[nListZero].lockname[1]),
											pConn->m_pszClientName,
											pConn->m_ListData[nListZero].lockname[0]
										) == 0
						)
					)
					return ADC_SUCCESS;
			}
		}
//AfxMessageBox("exiting lock list");
	}
	return nReturn;
}


int CADC::UnlockList(CAConnection* pConn, int nList)// List is 1 based
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
		int nListZero = nList-1;
		// have to see if the list has been locked by another station
		if(GetStatusData(pConn)==ADC_SUCCESS)
		{
			if(pConn->m_ListData[nListZero].packcount==0)  // only unlock it if it's not unlocked already.
			{
				if(pConn->m_pList[nListZero]!=NULL)
				{
					m_SendData.thenrevent.nlistcmd = listpackcount;
					m_SendData.thenrevent.nlistid = nList;
					m_SendData.thenrevent.nlistindex = ((pConn->m_pList[nListZero]->m_nPackCount>0)?pConn->m_pList[nListZero]->m_nPackCount:pConn->m_pList[nListZero]->m_nDefaultPackCount); //	{ pack count value }
					m_SendData.thenrevent.nothervalue = 0;
					m_SendData.thenrevent.numberofevents = 0;
					if(SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec))==0)	nReturn = ADC_SUCCESS;
				}
			}
			else nReturn = ADC_SUCCESS;
		}
	}
	return nReturn;
}

//////////////////////////////////////////////////////////////////////
// core - read only and low overhead
//////////////////////////////////////////////////////////////////////

int CADC::GetStatusData(CAConnection* pConn)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle>0))
	{
		unsigned long  ulRefTick = GetTickCount(); // last status get.
		if(pConn->m_ulRefTick > ulRefTick) // we wrapped
		{
			pConn->m_ulRefTick = ulRefTick;
			return nReturn;
		}

		if(pConn->m_ulRefTick+(((pConn->m_SysData.systemfrx) == 0x24)?40:33) < ulRefTick) // we are not requesting too rapidly. (once per frame == 33ms in NTSC, 40ms in PAL.)
		{
			pConn->m_ulRefTick = ulRefTick;
//			EnterCriticalSection(&m_crit);  //  critical section.
			pConn->m_Status = GetStatus(pConn->m_uchHandle);
//			LeaveCriticalSection(&m_crit);  //  critical section.

//			AfxMessageBox("foo");
			// now, compile the info
			if((pConn->m_Status&0xff) == netConnected)
			{
				if((pConn->m_SysData.systemfrx) == 0x24) //PAL (Harris uses hex encoded value)
				{
					if(
							(((int)pConn->m_SysData.refhour <24)&&((int)pConn->m_SysData.refhour >=0))
						&&(((int)pConn->m_SysData.refmin  <60)&&((int)pConn->m_SysData.refmin  >=0))
						&&(((int)pConn->m_SysData.refsec  <60)&&((int)pConn->m_SysData.refsec  >=0))
						&&(((int)pConn->m_SysData.refframe<25)&&((int)pConn->m_SysData.refframe>=0))
						)
						pConn->m_ulRefTimeMS = 
							((int)(pConn->m_SysData.refhour)*3600000
							+(int)(pConn->m_SysData.refmin)*60000
							+(int)(pConn->m_SysData.refsec)*1000
							+((int)(pConn->m_SysData.refframe)*1000)/25);  // else it doesnt get reassigned.
				}
				else
				{
					if(
							(((int)pConn->m_SysData.refhour <24)&&((int)pConn->m_SysData.refhour >=0))
						&&(((int)pConn->m_SysData.refmin  <60)&&((int)pConn->m_SysData.refmin  >=0))
						&&(((int)pConn->m_SysData.refsec  <60)&&((int)pConn->m_SysData.refsec  >=0))
						&&(((int)pConn->m_SysData.refframe<30)&&((int)pConn->m_SysData.refframe>=0))
						)
						pConn->m_ulRefTimeMS = 
							((int)(pConn->m_SysData.refhour)*3600000
							+(int)(pConn->m_SysData.refmin)*60000
							+(int)(pConn->m_SysData.refsec)*1000
							+((int)(pConn->m_SysData.refframe)*1000)/30);  // else it doesnt get reassigned.
				}


				// from the Harris interapi.doc:
				// For Windows applications the conversion to a standard TDateTime is 
				// TDateTime :=	datetoair + 693596;

				// TDateTime is a Delphi structure, not Windows.
				// In Delphi 1, Delphi defined a TDateTime as the number of days that have passed since 1/1/0001.
				// The fractional part represents a fractional part of a day.
				// In Delphi 2 and later, the TDateTime represents the number of days since 30 December 1899.  
				// A TDateTime of 0 corresponds to 30 Dec 1899; a value of 1 is 31 Dec 1899 = 0 Jan 1900.

				// The DateDelta constant (693594) can be used to convert from D1 to post-D1 TDateTime values.

				// this all implies that the Harris Julian date is counting from Jan 1 1900
				// now, unixtime counts from (D2) TDateTime 25569.  so, we can compile accordingly, using 25567.


				pConn->m_usRefJulianDate = pConn->m_SysData.systemdate;  // number of days since jan 1 1900
				pConn->m_ulRefUnixTime = ((pConn->m_SysData.systemdate - 25567)*86400) + (pConn->m_ulRefTimeMS/1000);  // truncate the milliseconds
				nReturn = ADC_SUCCESS;
			}
			else // not a valid conn.
			{
				pConn->m_ulRefTimeMS = TIME_NOT_DEFINED;
				pConn->m_usRefJulianDate = 0;
				pConn->m_ulRefUnixTime = 0;
			}
		}
		else 	nReturn = ADC_SUCCESS; // just return success
	}
	return nReturn;
}

// just queries the event list, doesnt get events from server
int CADC::GetNextPrimaryIndex(CAConnection* pConn, int nList, int nIndex, unsigned long ulFlags)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(nList>=0)&&(nList<=MAXSYSLISTS)) // must have a valid list.  if 0, use temp list.
	{
		int nListZero = nList-1;
		CAList* pList = NULL;
		if(ulFlags&ADC_GETEVENTS_TEMPLIST)
		{
			if(pConn->m_pTempList) pList=pConn->m_pTempList;
		}
		else
		{
			if(pConn->m_pList[nListZero]) pList=pConn->m_pList[nListZero];
		}

		nIndex++; // start checking with next one.
		if((pList)&&(nIndex<pList->m_nEventCount)&&(pList->m_ppEvents!=NULL))  // in bounds of list
		{
			bool bFound=false;

			while((nIndex<pList->m_nEventCount)&&(!bFound))
			{
				if(pList->m_ppEvents[nIndex])
				{
					if((pList->m_ppEvents[nIndex]->m_usType!=INVALIDEVENT)
						&&(!(pList->m_ppEvents[nIndex]->m_usType&SECONDARYEVENT)))  // we found a primary event
					{
						bFound=true;
						nReturn = nIndex;
					}
				}
				nIndex++;
			}
		}
	}
	return nReturn;
}

// just queries the event list, doesnt get events from server
int CADC::GetPreviousPrimaryIndex(CAConnection* pConn, int nList, int nIndex, unsigned long ulFlags)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(nList>=0)&&(nList<=MAXSYSLISTS)&&(nIndex>0)) // must have a valid list.  if 0, use temp list.
	{
		int nListZero = nList-1;
		CAList* pList = NULL;
		if(ulFlags&ADC_GETEVENTS_TEMPLIST)
		{
			if(pConn->m_pTempList) pList=pConn->m_pTempList;
		}
		else
		{
			if(pConn->m_pList[nListZero]) pList=pConn->m_pList[nListZero];
		}

		if((pList)&&(nIndex<pList->m_nEventCount)&&(pList->m_ppEvents!=NULL))  // in bounds of list
		{
			bool bFound=false;

			while((nIndex>0)&&(!bFound))
			{
				nIndex--;
				if(pList->m_ppEvents[nIndex])
				{
					if((pList->m_ppEvents[nIndex]->m_usType!=INVALIDEVENT)
						&&(!(pList->m_ppEvents[nIndex]->m_usType&SECONDARYEVENT)))  // we found a primary event
					{
						bFound=true;
						nReturn = nIndex;
					}
				}
			}
		}
	}
	return nReturn;
}

// just queries the event list, doesnt get events from server
int CADC::GetNumSecondaries(CAConnection* pConn, int nList, int nIndex, unsigned long ulFlags)
{
	int nReturn = ADC_ERROR; // will return -1 if usIndex not a primary 
	if((pConn)&&(nList>=0)&&(nList<=MAXSYSLISTS)) // must have a valid list.  if 0, use temp list.
	{
		int nListZero = nList-1;
		CAList* pList = NULL;
		if(ulFlags&ADC_GETEVENTS_TEMPLIST)
		{
			if(pConn->m_pTempList) pList=pConn->m_pTempList;
		}
		else
		{
			if(pConn->m_pList[nListZero]) pList=pConn->m_pList[nListZero];
		}

		if((pList)&&(nIndex<pList->m_nEventCount)&&(pList->m_ppEvents!=NULL))  // in bounds of list
		{
			if((pList->m_ppEvents[nIndex]->m_usType!=INVALIDEVENT)
				&&(!(pList->m_ppEvents[nIndex]->m_usType&SECONDARYEVENT)))  // we found a primary event
			{
				bool bFound=false;

				nReturn = 0;
				while((nIndex<pList->m_nEventCount)&&(!bFound))
				{
					if(pList->m_ppEvents[nIndex])
					{
						if(pList->m_ppEvents[nIndex]->m_usType!=INVALIDEVENT)
						{
							if(pList->m_ppEvents[nIndex]->m_usType&SECONDARYEVENT)  // we found a secondary event
							{
								nReturn++;
							}
							else
							{
								bFound=true;  // found the next primary
							}
						}
					}
					nIndex++;
				}
			}
		}
	}
	return nReturn;
}




//////////////////////////////////////////////////////////////////////
// core - read-write and higher overhead
//////////////////////////////////////////////////////////////////////
// NOTE: passed in Lists are 1-based, but events are zero-based

// returns number of events gotten
int CADC::GetEvents(CAConnection* pConn, int nList, int nIndex, int nNumEvents, unsigned long ulFlags, unsigned long ulTimeoutMS, bool* pbStatusChange, tlistdata* pinitlistdata)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list. to actually get from.
	{

		// send error message back
		char errorstring[ADC_ERRORSTRING_LEN];
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] A", clock() );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);


		int nListZero = nList-1;
		int nOriginalIndex = nIndex;
		int nEventsReturned = 0;
		int nEventsValid = 0;
		int nEventsRemaining = nNumEvents;
		unsigned char ucFailures = 0;
		CAList* pList = NULL;  //only one list per call
		bool bLocked = false;
		bool bStatusChanged = false;
		if(pbStatusChange) *pbStatusChange = false;

		if(ulFlags&ADC_GETEVENTS_LOCK)
		{
			if(LockList(pConn, nList)==ADC_ERROR) return nReturn;  // have to return false if locked by a different client.
			bLocked = true;
		}

		// prepare the send data.
		m_SendData.thenrevent.nlistcmd = natget;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nothervalue = 0; // not needed
		int nEventCount=0; 

		bool bListDataInitialized = false;
		tlistdata listdata;

		if(pinitlistdata!=NULL)
		{
			bListDataInitialized = true;
			memcpy(&listdata, pinitlistdata, sizeof(tlistdata));
		}


		while((nEventsRemaining>0)&&(ucFailures<3)&&(!bStatusChanged))
		{
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] GetStatusData %d %d %d", clock(), nEventsRemaining, ucFailures, bStatusChanged );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);

			nReturn = GetStatusData(pConn);
			if(nReturn == ADC_SUCCESS)
			{
				if(!bListDataInitialized)
				{
					bListDataInitialized = true;
					memcpy(&listdata, &pConn->m_ListData[nListZero], sizeof(tlistdata));
				}

				if(
						(listdata.listchanged!=pConn->m_ListData[nListZero].listchanged)
					||(listdata.listdisplay!=pConn->m_ListData[nListZero].listdisplay)
					||(listdata.listcount!=pConn->m_ListData[nListZero].listcount)
					)
				{
					if(pbStatusChange) *pbStatusChange = true;
					if(ulFlags&ADC_GETEVENTS_BREAKONCHANGE)
					{
						bStatusChanged = true;
						break;  // break out of the while and return
					}
				}

				if(ulFlags&ADC_GETEVENTS_FULLLIST)  // get entire list count, not just up to lookahead
					nEventsValid = pConn->m_ListData[nListZero].listcount;
				else
					nEventsValid = min(pConn->m_ListData[nListZero].listcount, pConn->m_ListData[nListZero].lookahead);
				if(nEventsValid < nIndex+nEventsRemaining)
				{
					if(nIndex >= nEventsValid) 
						nEventsRemaining=0;
					else
						nEventsRemaining = nEventsValid - nIndex;
				}
				m_SendData.thenrevent.nlistindex = nIndex;
				m_SendData.thenrevent.numberofevents = nEventsRemaining;


				if(nEventsRemaining)
				{
//					AfxMessageBox("sending");
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] entering crit", clock() );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);

					EnterCriticalSection(&m_crit);  //  critical section.

//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] SendData on %d", clock(), pConn->m_uchHandle );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);

					nReturn=SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
//					AfxMessageBox("sent");
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] SendData returned %d", clock(), nReturn );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);

					if(nReturn==0) //success
					{
						int nReceiveLength;
//					AfxMessageBox("receiving");
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] RecData receving on %d", clock(), pConn->m_uchHandle );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);
						nReturn=RecData(pConn->m_uchHandle, &m_ReceiveData, &nReceiveLength);
//					AfxMessageBox("received");
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] RecData returned %d, %d bytes", clock(), nReturn, nReceiveLength );
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); //Sleep(50);

						if(nReturn==0) //success
						{
							// allocate list if necessary
							if(nEventsReturned==0) // just do this at the beginning.
							{
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] alloc list", clock() );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);
								if(ulFlags&ADC_GETEVENTS_LIST)
								{
									if(pConn->m_pList[nListZero]==NULL) pConn->m_pList[nListZero] = new CAList;
									pList = pConn->m_pList[nListZero];
								}
								else
								{
									if(pConn->m_pTempList==NULL) pConn->m_pTempList = new CAList;
									pList = pConn->m_pTempList;
								}

								if(pList==NULL)
								{
									ucFailures++;
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] alloc list FAILED", clock() );
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); //Sleep(50);
								}
								else
								{
									// calculate necessary event list size.
									// events needed:
									int nEventBufferSize = nEventsRemaining;
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] alloc list OK, %d bufsize", clock() ,nEventBufferSize);
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);
									/*
									if(pConn->m_ListData[nListZero].listcount < nIndex+nNumEvents)
									{
										nEventBufferSize = pConn->m_ListData[nListZero].listcount - nIndex;
									}
									*/
									if(ulFlags&ADC_GETEVENTS_USEOFFSET)
									{ // add index.
										nEventBufferSize += nIndex;
										nEventCount=nIndex;
									}

									if((pList->m_nEventBufferSize<nEventBufferSize)||(pList->m_ppEvents==NULL))
									{

										EnterCriticalSection(&pList->m_crit);  //  critical section.
//										int n=
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] ensuring alloc", clock() );
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);
										pList->EnsureAllocated(nEventBufferSize); // should check ADC_SUCCESS, but for now we assume success
										LeaveCriticalSection(&pList->m_crit);  //  critical section.
//char bx[256]; sprintf(bx, "ensured %d events (%d)", pList->m_nEventBufferSize,n);
//Message(MSG_PRI_HIGH|MSG_ICONHAND, bx, "CAList:EnsureAllocated");
									}
								}
							} 

							if(pList->m_ppEvents) // if we are here, we are assured a buffer size of sufficient capacity.
							{
//		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] array %d, list %d, event count %d, conn %d", clock() , pList->m_ppEvents, pList, nEventCount, pConn);
//		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents"); Sleep(50);
								
								// parse out the whole buffer
				EnterCriticalSection(&pList->m_crit);  //  critical section.
								int nEventsParsed = StreamToArray(pList, nEventCount, (unsigned char*)&m_ReceiveData, pConn->m_SysData.systemfrx, -1, nReceiveLength );
				LeaveCriticalSection(&pList->m_crit);  //  critical section.
//										CString foo; foo.Format("converted %d events, %d numevents, %d count, %d returned, %d remain", 
//											nEventsParsed,m_ReceiveData.thenrevent.numberofevents, nEventCount, nEventsReturned, nEventsRemaining);	AfxMessageBox(foo);

								if(nEventsParsed>ADC_ERROR)
								{
									nEventCount+=nEventsParsed; 
									if(pList!=NULL)
									{
										pList->m_nEventCount = nEventCount;
									}

									nEventsReturned  += m_ReceiveData.thenrevent.numberofevents;
									nEventsRemaining -= m_ReceiveData.thenrevent.numberofevents;
									nIndex += m_ReceiveData.thenrevent.numberofevents;
//										CString foo; foo.Format("converted %d events, %d numevents, %d count, %d returned, %d remain", 
//											nEventsParsed,m_ReceiveData.thenrevent.numberofevents, nEventCount, nEventsReturned, nEventsRemaining);	AfxMessageBox(foo);
								}
								else
								{
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] parse error!", clock());
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents");// Sleep(50);

//									Sleep(40);// sleep for a frame or more, just to give it a rest.  Make this config later.
								}
							}
							
							LeaveCriticalSection(&m_crit);  //  critical section.
						}			
						else
						{
							LeaveCriticalSection(&m_crit);  //  critical section.
							ucFailures++;
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] RecData returned error %d", clock(), nReturn );
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents");// Sleep(50);
//									Sleep(40);// sleep for a frame or more, just to give it a rest.  Make this config later.

						}
					}
					else
					{
						LeaveCriticalSection(&m_crit);  //  critical section.
						ucFailures++;
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] SendData returned error %d", clock(), nReturn );
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents");// Sleep(50);
//									Sleep(40);// sleep for a frame or more, just to give it a rest.  Make this config later.

					}
				}

				if(pList!=NULL)
				{
					pList->m_nEventCount = nEventCount;
					if(pConn->m_ListData[nListZero].packcount>0)
					{
						pList->m_nPackCount = pConn->m_ListData[nListZero].packcount;
					}
				}

			}
			else
			{
				ucFailures++;
		_snprintf(errorstring, ADC_ERRORSTRING_LEN-1, "GetEvents [%d] GetStatusData returned error %d", clock(), nReturn );
		Message(MSG_PRI_HIGH|MSG_ICONINFO, errorstring, "ADC:GetEvents");// Sleep(50);
				nReturn = ADC_ERROR;
			}
		}

		if(bLocked)
		{
			if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
			{
				Sleep(34); // try once more after one frame
				if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
				{
					Sleep(34); // try once more after one frame
					UnlockList(pConn, nList);  // let it go even if error. 
					//(if we are locking and unlocking, we may be able to unlock later)
				}
			}
		}

		nReturn = nEventsReturned;
	}
//	AfxMessageBox("get returning");
	return nReturn;
}

// returns zero based index of playing event
// if there are no playing events, but some events exist, it returns ADC_NO_PLAYINGEVENT
// if there are no events at all, it returns ADC_NO_EVENTS
int CADC::GetEventsUntilPlaying(CAConnection* pConn, int nList, int nStartIndex, int nMaxEvents, unsigned long ulFlags, unsigned long ulTimeoutMS, bool* pbStatusChange, tlistdata* pinitlistdata)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list. to actually get from.
	{
		int nListZero = nList-1;
		int nOriginalIndex = nStartIndex;
		int nEventsReturned = 0;
		int nEventsValid = 0;
		int nEventsRemaining = nMaxEvents;
		unsigned char ucFailures = 0;
		CAList* pList = NULL;  //only one list per call
		bool bLocked = false;
		bool bStatusChanged = false;
		if(pbStatusChange) *pbStatusChange = false;

		int nPlayingFound = -1;
		bool bAfterPlayingFound = false;

		if(ulFlags&ADC_GETEVENTS_LOCK)
		{
			if(LockList(pConn, nList)==ADC_ERROR) return nReturn;  // have to return false if locked by a different client.
			bLocked = true;
		}

		// prepare the send data.
		m_SendData.thenrevent.nlistcmd = natget;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nothervalue = 0; // not needed
		int nEventCount=0; 

		bool bListDataInitialized = false;
		tlistdata listdata;
		if(pinitlistdata!=NULL)
		{
			bListDataInitialized = true;
			memcpy(&listdata, pinitlistdata, sizeof(tlistdata));
		}

		while((nEventsRemaining>0)&&(ucFailures<3)&&(!bStatusChanged)&&(!bAfterPlayingFound))
		{
			if(GetStatusData(pConn) == ADC_SUCCESS)
			{
				if(!bListDataInitialized)
				{
					bListDataInitialized = true;
					memcpy(&listdata, &pConn->m_ListData[nListZero], sizeof(tlistdata));
				}

				if(listdata.listcount == 0)
				{
					nEventsReturned = ADC_NO_EVENTS;
					break;  // break out of the while and return
				}
				else
				if(!(listdata.liststate&(1<<LISTISPLAYING)))
				{
					nEventsReturned = ADC_NO_PLAYINGEVENT;
					break;  // break out of the while and return
				}

				if(
						(listdata.listchanged!=pConn->m_ListData[nListZero].listchanged)
					||(listdata.listdisplay!=pConn->m_ListData[nListZero].listdisplay)
					||(listdata.listcount!=pConn->m_ListData[nListZero].listcount)
					)
				{
					if(pbStatusChange) *pbStatusChange = true;
					if(ulFlags&ADC_GETEVENTS_BREAKONCHANGE)
					{
						bStatusChanged = true;
						nEventsReturned = ADC_NO_PLAYINGEVENT;
						break;  // break out of the while and return
					}
				}

				if(ulFlags&ADC_GETEVENTS_FULLLIST)  // get entire list count, not just up to lookahead
					nEventsValid = pConn->m_ListData[nListZero].listcount;
				else
					nEventsValid = min(pConn->m_ListData[nListZero].listcount, pConn->m_ListData[nListZero].lookahead);
				if(nEventsValid < nStartIndex+nEventsRemaining)
				{
					if(nStartIndex >= nEventsValid) 
						nEventsRemaining=0;
					else
						nEventsRemaining = nEventsValid - nStartIndex;
				}
				m_SendData.thenrevent.nlistindex = nStartIndex;
				m_SendData.thenrevent.numberofevents = nEventsRemaining;


				if(nEventsRemaining)
				{
//					AfxMessageBox("sending");
					EnterCriticalSection(&m_crit);  //  critical section.
					nReturn=SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
//					AfxMessageBox("sent");

					if(nReturn==0) //success
					{
						int nReceiveLength;
//					AfxMessageBox("receiving");
						nReturn=RecData(pConn->m_uchHandle, &m_ReceiveData, &nReceiveLength);
//					AfxMessageBox("received");

						if(nReturn==0) //success
						{
							// allocate list if necessary
							if(nEventsReturned==0) // just do this at the beginning.
							{
								if(ulFlags&ADC_GETEVENTS_LIST)
								{
									if(pConn->m_pList[nListZero]==NULL) pConn->m_pList[nListZero] = new CAList;
									pList = pConn->m_pList[nListZero];
								}
								else
								{
									if(pConn->m_pTempList==NULL) pConn->m_pTempList = new CAList;
									pList = pConn->m_pTempList;
								}

								if(pList==NULL)
								{
									ucFailures++;
								}
								else
								{
									// calculate necessary event list size.
									// events needed:
									int nEventBufferSize = nEventsRemaining;
									/*
									if(pConn->m_ListData[nListZero].listcount < nIndex+nNumEvents)
									{
										nEventBufferSize = pConn->m_ListData[nListZero].listcount - nIndex;
									}
									*/
									if(ulFlags&ADC_GETEVENTS_USEOFFSET)
									{ // add index.
										nEventBufferSize += nStartIndex;
										nEventCount=nStartIndex;
									}

									if((pList->m_nEventBufferSize<nEventBufferSize)||(pList->m_ppEvents==NULL))
									{

										EnterCriticalSection(&pList->m_crit);  //  critical section.
//										int n=
										pList->EnsureAllocated(nEventBufferSize); // should check ADC_SUCCESS, but for now we assume success
										LeaveCriticalSection(&pList->m_crit);  //  critical section.
//char bx[256]; sprintf(bx, "ensured %d events (%d)", pList->m_nEventBufferSize,n);
//Message(MSG_PRI_HIGH|MSG_ICONHAND, bx, "CAList:EnsureAllocated");
									}
								}
							} 

							if(pList->m_ppEvents) // if we are here, we are assured a buffer size of sufficient capacity.
							{
								
								// parse out the whole buffer
				EnterCriticalSection(&pList->m_crit);  //  critical section.
								int nEventsParsed = StreamToArray(pList, nEventCount, (unsigned char*)&m_ReceiveData, pConn->m_SysData.systemfrx, -1, nReceiveLength);
				LeaveCriticalSection(&pList->m_crit);  //  critical section.
//										CString foo; foo.Format("converted %d events, %d numevents, %d count, %d returned, %d remain", 
//											nEventsParsed,m_ReceiveData.thenrevent.numberofevents, nEventCount, nEventsReturned, nEventsRemaining);	AfxMessageBox(foo);

								if(nEventsParsed>ADC_ERROR)
								{
									// let's find the playing event if it exists.
									int nCheckPlay = nEventCount;

									nEventCount+=nEventsParsed; 
									if(pList!=NULL)
									{
										pList->m_nEventCount = nEventCount;

				EnterCriticalSection(&pList->m_crit);  //  critical section.
										if((pList->m_ppEvents)&&(pList->m_ppEvents[nCheckPlay]))
										{
											if(pList->m_ppEvents[nCheckPlay]->m_usType&SECAVEVENT)
											{
												nCheckPlay = GetNextPrimaryIndex(pConn, nList, nCheckPlay);
											}
										}

										while((nCheckPlay<nEventCount)&&(nCheckPlay>=ADC_SUCCESS))
										{
											if(nPlayingFound>=0)
											{
												// look for after playing primary
												if(!(pList->m_ppEvents[nCheckPlay]->m_usType&SECAVEVENT))
												{
													// found it, lets break out.
													nEventsReturned = nPlayingFound;
													bAfterPlayingFound = true;
													break;
												}
											}
											else
											{
												// look for playing primary
												if(pList->m_ppEvents[nCheckPlay]->m_usStatus&(1<<eventrunning)) 
												{
													nPlayingFound = nCheckPlay;
												}
											}

											nCheckPlay = GetNextPrimaryIndex(pConn, nList, nCheckPlay);
										}
				LeaveCriticalSection(&pList->m_crit);  //  critical section.
									}

									if(!bAfterPlayingFound)
									{
										nEventsReturned  += m_ReceiveData.thenrevent.numberofevents;
										nEventsRemaining -= m_ReceiveData.thenrevent.numberofevents;
										nStartIndex += m_ReceiveData.thenrevent.numberofevents;
//										CString foo; foo.Format("converted %d events, %d numevents, %d count, %d returned, %d remain", 
//											nEventsParsed,m_ReceiveData.thenrevent.numberofevents, nEventCount, nEventsReturned, nEventsRemaining);	AfxMessageBox(foo);
									}
								}
							}
							
							LeaveCriticalSection(&m_crit);  //  critical section.
						}			
						else
						{
							LeaveCriticalSection(&m_crit);  //  critical section.
							ucFailures++;
						}
					}
					else
					{
						LeaveCriticalSection(&m_crit);  //  critical section.
						ucFailures++;
					}
				}

				if(pList!=NULL)
				{
					pList->m_nEventCount = nEventCount;
					if(pConn->m_ListData[nListZero].packcount>0)
					{
						pList->m_nPackCount = pConn->m_ListData[nListZero].packcount;
					}
				}

			}
			else ucFailures++;
		}//		while((nEventsRemaining>0)&&(ucFailures<3)&&(!bStatusChanged))

		if(bLocked)
		{
			if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
			{
				Sleep(34); // try once more after one frame
				if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
				{
					Sleep(34); // try once more after one frame
					UnlockList(pConn, nList);  // let it go even if error. 
					//(if we are locking and unlocking, we may be able to unlock later)
				}
			}
		}

		if(bAfterPlayingFound)
		{
			nReturn = nEventsReturned;
		}
		else
		{
			if(listdata.listcount == 0)
			{
				nReturn = ADC_NO_EVENTS;
			}
			else
			{
				nReturn = ADC_NO_PLAYINGEVENT;
			}
		}
	}
//	AfxMessageBox("get returning");
	return nReturn;
}

// returns number of IDs found.  puts indices of found IDs in the passed in array
int CADC::GetEventsUntilIDs(CAConnection* pConn, int nList, int nStartIndex, int nMaxEvents, char* pchIDs[], int* pnIndex[], int nNumIDs, unsigned long ulFlags, unsigned long ulTimeoutMS, bool* pbStatusChange, tlistdata* pinitlistdata)
{	
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
		// prepare the send data.
		m_SendData.thenrevent.nlistcmd = natget;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nothervalue = 0; // not needed
	}
	return nReturn;
}

// returns index of the event matching the relevant fields of the verification event.
int CADC::GetEventsUntilEvent(CAConnection* pConn, int nList, int nStartIndex, int nMaxEvents, unsigned long ulFlags, unsigned long ulTimeoutMS, bool* pbStatusChange, tlistdata* pinitlistdata)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
		// prepare the send data.
		m_SendData.thenrevent.nlistcmd = natget;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nothervalue = 0; // not needed
	}
	return nReturn;
}


int CADC::MoveEventsBlockToIndex(CAConnection* pConn, int nList, int nStartIndex, int nNumEvents, int nTargetIndex, unsigned long ulFlags)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
		/*
	// removes numevents from list, moves the other events up to fill the gap, then inserts the num events at the target index.
	m_nEventPosition=nStartIndex;   // interest, or source event
	if(nStartIndex<nTargetIndex)
		m_nOtherValue=nTargetIndex+nNumEvents;  // unused, or destination event
	else
		m_nOtherValue=nTargetIndex;  // unused, or destination event
	m_nNumberOfEvents=nNumEvents;
	return MoveEvent( );
	*/
		// prepare the send data.
		m_SendData.thenrevent.nlistcmd = natget;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nothervalue = 0; // not needed
	}
	return nReturn;
}

int CADC::SwapEventBlocks(CAConnection* pConn, int nList, int nBlockOneStartIndex, int nBlockOneNumEvents, int nBlockTwoStartIndex, int nBlockTwoNumEvents, unsigned long ulFlags)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{
	}
	return nReturn;
}

// must set the swap event and the buffer if nec.  verification is not used.
// protect play means no insertions are allowed before the playing event.
// protect done also means no insertions are allowed before the playing event - cant look past the playing event, some secondaries might be done.
int	CADC::InsertEventAt(CAConnection* pConn, int nList, int nIndex, /*int nNumEvents,*/ unsigned long ulFlags)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)&&(m_pucEventStreamBuffer)&&(m_usStreamBufferLength>0)) // must have a valid list.and a valid buffer to insert
	{

		unsigned char** ppchEventMarkers = NULL;
//AfxMessageBox("calling CountEventsInStream");

		int nNumEvents = CountEventsInStream(m_pucEventStreamBuffer, m_usStreamBufferLength, &ppchEventMarkers);
//		else
//			m_SendData.thenrevent.numberofevents = nNumEvents;
//AfxMessageBox("CountEventsInStream returned");


		// assign bufferoni
//		m_pucEventStreamBuffer is already assigned on the way in.  Now, must copy that over to the DLL's structure.
//		There is probably a space limit on this, 4576 bytes perhaps?  need to test (as of 30-Oct-2008)
//		could be defined by this part of the structure: Byte eventbuffer[4576];
//    but may have been redefined when they went to streaming...
//		tested this, it is limited to the 4576 bytes, as many events as fit in there.... so have to calc and repeat.

		if(nNumEvents>0)
		{
			bool bLocked = false;
			//could consider locking the list first.... prob should, OK flag it.
			if(ulFlags&ADC_SETEVENTS_LOCK)
			{
				if(LockList(pConn, nList)==ADC_ERROR)
				{
					if(ppchEventMarkers) delete [] ppchEventMarkers;
					return nReturn;  // have to return false if locked by a different client.
				}
				bLocked = true;
			}

		// prepare the send data.
			m_SendData.thenrevent.nlistcmd = natinsert;
			m_SendData.thenrevent.nlistid = nList;
			m_SendData.thenrevent.nothervalue = 0; // not needed
			m_SendData.thenrevent.nlistindex = nIndex;
			m_SendData.thenrevent.numberofevents= nNumEvents;
			
			if(ppchEventMarkers == NULL) // just try...
			{
//AfxMessageBox("could not allocate buffer");
				//let's just memcpy for now. m_pucEventStreamBuffer,m_usStreamBufferLength
				memcpy(&(m_SendData.thenrevent.aevent[0]), m_pucEventStreamBuffer, m_usStreamBufferLength);

				if(m_SendData.thenrevent.numberofevents>0)
				{
					nReturn = SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
					if(nReturn==ADC_SUCCESS)
					{
						if(!(ulFlags&ADC_NO_NOTIFY)) SendListChange(pConn, nList);
						nReturn = m_SendData.thenrevent.numberofevents;
					}
				}
			}
			else
			{
				// we have to try to count them out and loop.  
				// have to be within the 4576 byte limit

				int i=0;
				int nIns = m_SendData.thenrevent.numberofevents; 

				bool bFail =false;
				unsigned char* puc = m_pucEventStreamBuffer;
				int nInserted = 0;
				while((i<nIns)&&(!bFail))
				{
					while((i<nIns)&&((ppchEventMarkers[i] - puc)<4576))
					{
						i++;
					}
//CString szItem; szItem.Format("in: %d, out %s: %d, diff: %d",puc, ((i<nIns)?"exceeded":"end"), ((i<nIns)?ppchEventMarkers[i]:ppchEventMarkers[nIns-1]), (((i<nIns)?ppchEventMarkers[i]:ppchEventMarkers[nIns-1]) - puc)); AfxMessageBox(szItem);
					if(i==0) i=1; // minimum one event, though the buffer is too big... we'll see what happens.
					// ok, we have found the one that exceeds.
					m_SendData.thenrevent.numberofevents = i-nInserted;
					i--;
//szItem.Format("in: %d, out: %d, diff: %d, num: %d",puc,ppchEventMarkers[i], (ppchEventMarkers[i] - puc), m_SendData.thenrevent.numberofevents); AfxMessageBox(szItem);

					memcpy(&(m_SendData.thenrevent.aevent[0]), puc, (ppchEventMarkers[i] - puc)+1);
					puc = ppchEventMarkers[i]+1;
					i++;

					if(m_SendData.thenrevent.numberofevents>0)
					{
						nReturn = SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
						if(nReturn==ADC_SUCCESS)
						{
							nInserted += m_SendData.thenrevent.numberofevents;
							m_SendData.thenrevent.nlistindex += m_SendData.thenrevent.numberofevents;
						}
						else
						{
							if(nInserted>0)
								nReturn = nInserted;
							else
								nReturn = ADC_ERROR;
							bFail =true;
						}
					}
					else // have to break!
					{
						if(nInserted>0)
							nReturn = nInserted;
						else
							nReturn = ADC_ERROR;
						bFail =true;
					}

				} // while((i<nIns)&&(!bFail))
				if(nReturn!=ADC_ERROR)
				{
					if(!(ulFlags&ADC_NO_NOTIFY)) SendListChange(pConn, nList);
					nReturn = nInserted;
				}

			}

			if(bLocked)
			{
				if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
				{
					Sleep(34); // try once more after one frame
					if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
					{
						Sleep(34); // try once more after one frame
						UnlockList(pConn, nList);  // let it go even if error. 
						//(if we are locking and unlocking, we may be able to unlock later)
					}
				}
			}
		}//if(m_SendData.thenrevent.numberofevents>0)

		if(ppchEventMarkers) delete [] ppchEventMarkers;


/*
	BOOL bSuccess = FALSE;
	m_SendData.thenrevent.nlistcmd = ncomevent;

	if(m_louthEvent.Type!=0x01)
	{
//		AfxMessageBox("standard");
	  unsigned char* pchar =  (unsigned char *) &(m_louthEvent);
		memcpy(&(m_SendData.thenrevent.aevent[0]), pchar+3, sizeof(revent)-3);
//		memcpy(&(m_SendData.thenrevent.aevent[0]), (&(m_louthEvent)+3), sizeof(revent)-3);
	}
	else //it's an extended event
	{
//		AfxMessageBox("extended");
	  unsigned char* pchar =  (unsigned char *) &(m_louthEvent);
		memcpy(&(m_SendData.thenrevent.aevent[0]), pchar, sizeof(revent));

		pchar=(unsigned char *)&(m_SendData.thenrevent.aevent[0]);
		pchar+=sizeof(revent);

//	CString foo;
//	unsigned int x=0;
		
//	foo.Format("2: buflen = %d\n buffer: ", m_nExtendedBufferLength);
//	for (x=0; x<m_nExtendedBufferLength; x++) foo.Format("%s<%02x>", foo, *(m_chExtendedEventBuffer+x));
//	AfxMessageBox(foo);

		memcpy(pchar, (unsigned char *) m_chExtendedEventBuffer, m_nExtendedBufferLength); // I wonder what happens to the extra characters, hopefully they just get ignored at server

//		CString foo;
//		foo.Format("3: buflen = revent %d + buf %d\n buffer: ", sizeof(revent), m_nExtendedBufferLength);
//		for (x=0; x<sizeof(revent)+m_nExtendedBufferLength; x++)
//		{
//			unsigned char* pch = (unsigned char* )m_SendData.thenrevent.aevent+x;
//			if(x%10==0)foo+="\n";
//			foo.Format("%s<%03d:%02x>", foo, x, *pch);
//		}
//		AfxMessageBox(foo);
	}

	m_SendData.thenrevent.nlistcmd = natinsert;
	m_SendData.thenrevent.nlistid = m_nCurrentList;
	m_SendData.thenrevent.nlistindex = m_nEventPosition;
	m_SendData.thenrevent.numberofevents = m_nNumberOfEvents;
	SendData(m_nConnectionHandle[m_nCurrentConnection], &m_SendData, sizeof(messageRec));
		if(!(ulFlags&ADC_SUPPRESS_CHANGE)) SendListChange(pConn, nList);
	return bSuccess;
*/
	}
	return nReturn;
}

// if verify, must set the verification event and the buffer if nec.  
// protect play means no deletions are allowed if event is playing.
// protect done means no deletions are allowed if event is done.
int	CADC::DeleteEventAt(CAConnection* pConn, int nList, int nIndex, int nNumEvents, unsigned long ulFlags)
{
	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{

//	InitEvent();
	//	m_SendData.thenrevent.aevent[0] = m_louthEvent;
		bool bLocked = false;
		//could consider locking the list first.... prob should, OK flag it.
		if(ulFlags&ADC_SETEVENTS_LOCK)
		{
			if(LockList(pConn, nList)==ADC_ERROR)
			{
				return nReturn;  // have to return false if locked by a different client.
			}
			bLocked = true;
		}

		m_SendData.thenrevent.nlistcmd = natdelete;
		m_SendData.thenrevent.nlistid = nList;
		m_SendData.thenrevent.nlistindex = nIndex;
		m_SendData.thenrevent.numberofevents = nNumEvents;

		nReturn=SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
		if(!(ulFlags&ADC_NO_NOTIFY)) SendListChange(pConn, nList);
		if(bLocked)
		{
			if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
			{
				Sleep(34); // try once more after one frame
				if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
				{
					Sleep(34); // try once more after one frame
					UnlockList(pConn, nList);  // let it go even if error. 
					//(if we are locking and unlocking, we may be able to unlock later)
				}
			}
		}
		return nReturn;

	}
	return nReturn;
}

// must set the swap event and the buffer if nec. // no, actually doing the same thing as insert event.
// if verify, must set the verification event and the buffer if nec.  
// protect play means no mods are allowed if event is playing.
// protect done means no mods are allowed if event is done.
int	CADC::ModifyEventAt(CAConnection* pConn, int nList, int nIndex, /*int nNumEvents,*/ unsigned long ulFlags)
{

	int nReturn = ADC_ERROR;
	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)&&(m_pucEventStreamBuffer)&&(m_usStreamBufferLength>0)) // must have a valid list.and a valid buffer to insert
//	if((pConn)&&(pConn->m_uchHandle)&&(nList>0)&&(nList<=MAXSYSLISTS)) // must have a valid list.
	{

		unsigned char** ppchEventMarkers = NULL;
//AfxMessageBox("calling CountEventsInStream");

//		if(nNumEvents<0)
		int nNumEvents = CountEventsInStream(m_pucEventStreamBuffer, m_usStreamBufferLength, &ppchEventMarkers);
//		else
//			m_SendData.thenrevent.numberofevents = nNumEvents;
//AfxMessageBox("CountEventsInStream returned");


		// assign bufferoni
//		m_pucEventStreamBuffer is already assigned on the way in.  Now, must copy that over to the DLL's structure.
//		There is probably a space limit on this, 4576 bytes perhaps?  need to test (as of 30-Oct-2008)
//		could be defined by this part of the structure: Byte eventbuffer[4576];
//    but may have been redefined when they went to streaming...
//		tested this, it is limited to the 4576 bytes, as many events as fit in there.... so have to calc and repeat.

		if(nNumEvents>0)
		{
			bool bLocked = false;
			//could consider locking the list first.... prob should, OK flag it.
			if(ulFlags&ADC_SETEVENTS_LOCK)
			{
				if(LockList(pConn, nList)==ADC_ERROR)
				{
//AfxMessageBox("lock list failed");
					if(ppchEventMarkers) delete [] ppchEventMarkers;
					return nReturn;  // have to return false if locked by a different client.
				}
				bLocked = true;
			}
//CString foo; foo.Format("locked: %d, markers: %d",bLocked?1:0, ppchEventMarkers?1:0);
//AfxMessageBox(foo);


// have to modify the command back after lock list
			// prepare the send data.
			m_SendData.thenrevent.nlistcmd = natput;
			m_SendData.thenrevent.nlistid = nList;
			m_SendData.thenrevent.nothervalue = 0; // not needed
			m_SendData.thenrevent.nlistindex = nIndex;
			m_SendData.thenrevent.numberofevents = nNumEvents;

			if(ppchEventMarkers == NULL) // just try...
			{
//AfxMessageBox("could not allocate buffer");
				//let's just memcpy for now. m_pucEventStreamBuffer,m_usStreamBufferLength
				memcpy(&(m_SendData.thenrevent.aevent[0]), m_pucEventStreamBuffer, m_usStreamBufferLength);

				if(m_SendData.thenrevent.numberofevents>0)
				{
					nReturn = SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
					if(nReturn==ADC_SUCCESS)
					{
						if(!(ulFlags&ADC_NO_NOTIFY)) SendListChange(pConn, nList);
						nReturn = m_SendData.thenrevent.numberofevents;
					}
				}
			}
			else
			{
				// we have to try to count them out and loop.  
				// have to be within the 4576 byte limit

				int i=0;
				int nIns = m_SendData.thenrevent.numberofevents; 
//CString foo; foo.Format("numevents: %d",m_SendData.thenrevent.numberofevents);
//AfxMessageBox(foo);

				bool bFail =false;
				unsigned char* puc = m_pucEventStreamBuffer;
				int nInserted = 0;
				while((i<nIns)&&(!bFail))
				{
					while((i<nIns)&&((ppchEventMarkers[i] - puc)<4576))
					{
						i++;
					}
//CString szItem; szItem.Format("in: %d, out %s: %d, diff: %d",puc, ((i<nIns)?"exceeded":"end"), ((i<nIns)?ppchEventMarkers[i]:ppchEventMarkers[nIns-1]), (((i<nIns)?ppchEventMarkers[i]:ppchEventMarkers[nIns-1]) - puc)); AfxMessageBox(szItem);
					if(i==0) i=1; // minimum one event, though the buffer is too big... we'll see what happens.
					// ok, we have found the one that exceeds.
					m_SendData.thenrevent.numberofevents = i-nInserted;
					i--;
//szItem.Format("in: %d, out: %d, diff: %d, num: %d",puc,ppchEventMarkers[i], (ppchEventMarkers[i] - puc), m_SendData.thenrevent.numberofevents); AfxMessageBox(szItem);

					memcpy(&(m_SendData.thenrevent.aevent[0]), puc, (ppchEventMarkers[i] - puc)+1);
					puc = ppchEventMarkers[i]+1;
					i++;

					if(m_SendData.thenrevent.numberofevents>0)
					{
						nReturn = SendData(pConn->m_uchHandle, &m_SendData, sizeof(messageRec));
						if(nReturn==ADC_SUCCESS)
						{
							nInserted += m_SendData.thenrevent.numberofevents;
							m_SendData.thenrevent.nlistindex += m_SendData.thenrevent.numberofevents;
						}
						else
						{
//AfxMessageBox("modify failed");
							if(nInserted>0)
								nReturn = nInserted;
							else
								nReturn = ADC_ERROR;
							bFail =true;
						}
					}
					else // have to break!
					{
						if(nInserted>0)
							nReturn = nInserted;
						else
							nReturn = ADC_ERROR;
						bFail =true;
					}

				} // while((i<nIns)&&(!bFail))
				if(nReturn!=ADC_ERROR)
				{
					if(!(ulFlags&ADC_NO_NOTIFY)) SendListChange(pConn, nList);
					nReturn = nInserted;
				}

			}

			if(bLocked)
			{
				if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
				{
					Sleep(34); // try once more after one frame
					if(UnlockList(pConn, nList)==ADC_ERROR) // try again.
					{
						Sleep(34); // try once more after one frame
						UnlockList(pConn, nList);  // let it go even if error. 
						//(if we are locking and unlocking, we may be able to unlock later)
					}
				}
			}
		}//if(m_SendData.thenrevent.numberofevents>0)

		if(ppchEventMarkers) delete [] ppchEventMarkers;



/*
	int nReturn = 0; //success 
	if(m_louthEvent.Type!=0x01)
	{
		memcpy(&(m_SendData.thenrevent.aevent[0]), (&(m_louthEvent)+3), sizeof(revent)-3);
	}
	else //it's an extended event
	{
//		AfxMessageBox("extended");
	  unsigned char* pchar =  (unsigned char *) &(m_louthEvent);
		memcpy(&(m_SendData.thenrevent.aevent[0]), pchar, sizeof(revent));

		pchar=(unsigned char *)&(m_SendData.thenrevent.aevent[0]);
		pchar+=sizeof(revent);

//	CString foo;
//	unsigned int x=0;
		
//	foo.Format("2: buflen = %d\n buffer: ", m_nExtendedBufferLength);
//	for (x=0; x<m_nExtendedBufferLength; x++) foo.Format("%s<%02x>", foo, *(m_chExtendedEventBuffer+x));
//	AfxMessageBox(foo);

		memcpy(pchar, (unsigned char *) m_chExtendedEventBuffer, m_nExtendedBufferLength); // I wonder what happens to the extra characters, hopefully they just get ignored at server
	}
	m_SendData.thenrevent.nlistcmd = natput;
	m_SendData.thenrevent.nlistid = m_nCurrentList;
	m_SendData.thenrevent.nlistindex = m_nEventPosition;
	m_SendData.thenrevent.numberofevents = m_nNumberOfEvents;
	nReturn = SendData(m_nConnectionHandle[m_nCurrentConnection], &m_SendData, sizeof(messageRec));
		if(!(ulFlags&ADC_NO_NOTIFY)) SendListChange(pConn, nList);
	return nReturn;
*/
	
	}
	return nReturn;
}



//////////////////////////////////////////////////////////////////////
// utility
//////////////////////////////////////////////////////////////////////

unsigned long CADC::ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned char ucFrameBasis, unsigned long ulFlags) // pass in the systemfrx from the system data
{
	if(ulFlags!=ADC_NORMAL)
	{
/*
		char temp[32];
		sprintf(temp, "%02x", ucHours);		ucHours = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucMinutes);	ucMinutes = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucSeconds);	ucSeconds = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucFrames);	ucFrames = (unsigned char)(atoi(temp));
*/		
		unsigned char ucTemp = 	(ucHours&0x0f)+(((ucHours&0xf0)>>4)*10); ucHours = ucTemp;
		ucTemp = 	(ucMinutes&0x0f)+(((ucMinutes&0xf0)>>4)*10); ucMinutes = ucTemp;
		ucTemp = 	(ucSeconds&0x0f)+(((ucSeconds&0xf0)>>4)*10); ucSeconds = ucTemp;
		ucTemp = 	(ucFrames&0x0f)+(((ucFrames&0xf0)>>4)*10); ucFrames = ucTemp;
	}
	if((ucHours>23)||(ucHours<0)) return TIME_NOT_DEFINED;
	if((ucMinutes>59)||(ucMinutes<0)) return TIME_NOT_DEFINED;
	if((ucSeconds>59)||(ucSeconds<0)) return TIME_NOT_DEFINED;
	if(ucFrameBasis == 0x24) //PAL (Harris uses hex encoded value)
	{
		if((ucFrames>24)||(ucFrames<0)) return TIME_NOT_DEFINED;
		return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/25); //parens for rounding error
	}
	else //default to NTSC
	{
		if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
		return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/30); //parens for rounding error
	}
}

void  CADC::ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned char ucFrameBasis, unsigned long ulFlags) // pass in the systemfrx from the system data
{	
//	CString foo; foo.Format("%d",ulMilliseconds );
//	AfxMessageBox(foo);

	if(ulMilliseconds == TIME_NOT_DEFINED)
	{
		*pucHours		= 0xff;
		*pucMinutes = 0xff;	
		*pucSeconds = 0xff;
		*pucFrames  = 0xff;
		return;
	}

	*pucHours		= ((unsigned char)(ulMilliseconds/3600000L))&0xff;
	*pucMinutes = ((unsigned char)((ulMilliseconds/60000L)%60))&0xff;	
	*pucSeconds = ((unsigned char)(((ulMilliseconds/1000L)%60)%60))&0xff;

	// frame calc had an issue where you could end up with or .30 frames if the millisecond count
	// was in between 989 and 999 for ntsc
	if(ucFrameBasis == 0x24) //PAL (Harris uses hex encoded value)
	{
		*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/40)))&0xff;  //(int)(1000.0/25.0) = 40
	}
	else //default to NTSC
	{
		// have to make it reversible.  So anything above 989 gets converted to 29.
		*pucFrames	= ((unsigned char)(min(29, ((ulMilliseconds%1000L)/33))))&0xff;  //(int)(1000.0/30.0) = 33
	}
	if(ulFlags!=ADC_NORMAL)
	{
		unsigned char ucTemp = *pucHours;
		*pucHours = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucMinutes;
		*pucMinutes = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucSeconds;
		*pucSeconds = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucFrames;
		*pucFrames = ((ucTemp/10)*16) + (ucTemp%10);
	}
}

int	CADC::FieldLength(char* pchArray, int nArrayLength, char* pchNulls, int nNumNulls)
{
	if(pchArray==NULL) return 0;
	if(pchNulls==NULL) return nArrayLength;
	int i=0;
	bool bNull = false;

	while (nArrayLength>0) 
	{
		bNull = false;
		for(i=0; i<nNumNulls; i++)
		{
			if( (*(pchArray+nArrayLength-1)) == (*(pchNulls+i)) ) 
			{
				bNull = true;
				break;
			}
		}
		if(bNull == false) break;

		nArrayLength--;
	} 

	return nArrayLength;
}

// compares the swap event with the verification event.
bool CADC::IsIdenticalEvent(unsigned long ulFlags)
{
  if(memcmp(&m_Event, &m_VerifyEvent, sizeof(revent))!=0) return false;
	if((m_pucExtendedEventBuffer==NULL)&&(m_pucVerifyExtendedEventBuffer!=NULL)) return false;
	if((m_pucExtendedEventBuffer!=NULL)&&(m_pucVerifyExtendedEventBuffer==NULL)) return false;
	if((m_pucExtendedEventBuffer)&&(m_pucVerifyExtendedEventBuffer))
	{
		if(m_usExtendedBufferLength != m_usVerifyExtendedBufferLength) return false;
		if(memcmp(m_pucExtendedEventBuffer, m_pucVerifyExtendedEventBuffer, m_usExtendedBufferLength)!=0) return false;
	}
	return true;
}
