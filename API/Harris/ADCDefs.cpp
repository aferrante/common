#include "ADCDefs.h"

CAEvent::CAEvent()
{
// supplied from automation system
	m_usType = TYPE_NOT_DEFINED;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszData = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = SEGMENT_NOT_DEFINED;
	m_ulOnAirTimeMS = TIME_NOT_DEFINED;
	m_usOnAirJulianDate =0;
	m_ulDurationMS = TIME_NOT_DEFINED;
	m_ulSOMMS = TIME_NOT_DEFINED;
	m_usStatus = STATUS_NOT_DEFINED;
//	m_usControl = STATUS_NOT_DEFINED;
	m_ulControl = TIME_NOT_DEFINED;
	m_usDevice = DEVICE_NOT_DEFINED;

//	m_prevent = NULL;

// a pointer to anything we want, to contextualize this event - must use new operator to allocate
	m_pContextData = NULL;
}

CAEvent::~CAEvent()
{
	if(m_pszID) free(m_pszID); // must use malloc to allocate buffer;
	if(m_pszTitle) free(m_pszTitle); // must use malloc to allocate buffer;
	if(m_pszData) free(m_pszData); // must use malloc to allocate buffer;
	if(m_pszReconcileKey) free(m_pszReconcileKey); // must use malloc to allocate buffer;
	if(m_pContextData) delete(m_pContextData); // must use new to allocate object;
//	if(m_prevent) delete(m_prevent); // must use new to allocate object;
}

void CAEvent::Reset()
{
	// have to free any data that exists.
	if(m_pszID) free(m_pszID); // must use malloc to allocate buffer;
	if(m_pszTitle) free(m_pszTitle); // must use malloc to allocate buffer;
	if(m_pszData) free(m_pszData); // must use malloc to allocate buffer;
	if(m_pszReconcileKey) free(m_pszReconcileKey); // must use malloc to allocate buffer;
	if(m_pContextData) delete(m_pContextData); // must use new to allocate object;
//	if(m_prevent) delete(m_prevent); // must use new to allocate object;

	// then re-initialize
// supplied from automation system
	m_usType = TYPE_NOT_DEFINED;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszData = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = SEGMENT_NOT_DEFINED;
	m_ulOnAirTimeMS = TIME_NOT_DEFINED;
	m_usOnAirJulianDate =0;
	m_ulDurationMS = TIME_NOT_DEFINED;
	m_ulSOMMS = TIME_NOT_DEFINED;
	m_usStatus = STATUS_NOT_DEFINED;
//	m_usControl = STATUS_NOT_DEFINED;
	m_ulControl = TIME_NOT_DEFINED;
	m_usDevice = DEVICE_NOT_DEFINED;

// assigned internally
	m_pContextData = NULL;
}



CAList::CAList()
{
	m_ppEvents = NULL;
	m_nEventCount = 0;
	m_nEventBufferSize = 0;
	m_nPackCount = 0; // needed to store pack count to return after lock 
	m_nDefaultPackCount=3;// reasonable default
	InitializeCriticalSection(&m_crit);  //  critical section.
}

CAList::~CAList()
{
	if(m_ppEvents)
	{
		for(int i=0; i<m_nEventCount; i++)
		{
			if(m_ppEvents[i]) {try{delete m_ppEvents[i];} catch(...){}}  // must use new to allocate
		}
		try{ delete [] m_ppEvents; } catch(...){}  // must use new to allocate
	}
	DeleteCriticalSection(&m_crit);  //  critical section.
}

int CAList::EnsureAllocated(int nEventBufferSize)
{
	if(nEventBufferSize>0)
	{

		if(m_ppEvents)
		{
			if(m_nEventBufferSize>=nEventBufferSize)
			{
				return ADC_SUCCESS;
			}
		}

		CAEvent** ppEvents = new CAEvent*[nEventBufferSize]; // a pointer to an array of event pointers.
		int ix=0;
		if(ppEvents)
		{
			if(m_ppEvents)
			{
				while(ix<m_nEventBufferSize)
				{
					ppEvents[ix] = m_ppEvents[ix];
					ix++;
				}
				delete [] m_ppEvents;
			}
			while(ix<nEventBufferSize)
			{
				ppEvents[ix] = NULL;
				ix++;
			}
			m_nEventBufferSize = ix;
			m_ppEvents = ppEvents;

			return m_nEventBufferSize;
		}
	}

	return ADC_ERROR;
}


CAConnection::CAConnection()
{
// used to connect:
	m_pszServerName = NULL;
	m_pszClientName = NULL;

// supplied from automation system
	m_uchHandle = NULL;
	m_ulRefTimeMS = TIME_NOT_DEFINED;
	m_Status = netDisconnect;
	m_usRefJulianDate =0;
	m_ulRefUnixTime = 0;

// assigned internally
	for(int i=0; i<MAXSYSLISTS; i++)
	{
		m_bListActive[i] = false;
		m_pList[i] = NULL;
		m_ListData[i].listtype=0;
		m_ListData[i].listchanged=0;
		m_ListData[i].listdisplay=0;
		m_ListData[i].listsyschange=0;
		m_ListData[i].listcount=0;
		m_ListData[i].lookahead=0;
		m_ListData[i].maxlistevents=0;
		m_ListData[i].liststate=0;
		m_ListData[i].packcount=0;
		m_ListData[i].lockname[0]=0;
		m_ListData[i].listname[0]=0;
		m_ListData[i].threadtime[0]=0;
		m_ListData[i].threadtime[1]=0;
		m_ListData[i].threadtime[2]=0;
		m_ListData[i].threadtime[3]=0;
		m_ListData[i].offtime[0]=0;
		m_ListData[i].offtime[1]=0;
		m_ListData[i].offtime[2]=0;
		m_ListData[i].offtime[3]=0;
		m_ListData[i].timetonext[0]=0;
		m_ListData[i].timetonext[1]=0;
		m_ListData[i].timetonext[2]=0;
		m_ListData[i].timetonext[3]=0;
	}
	m_pTempList = NULL;
	m_ulRefTick = 0;
}

CAConnection::~CAConnection()
{
	if(m_pszServerName) delete [] m_pszServerName; // must use new to allocate buffer;
	if(m_pszClientName) delete [] m_pszClientName; // must use new to allocate buffer;

	for(int i=0; i<MAXSYSLISTS; i++)
	{
		if(m_pList[i]) delete m_pList[i]; // must use new to allocate
	}
	if(m_pTempList) delete m_pTempList; // must use new to allocate
}


