//----------------------------------------------------------------------------
//
// apiinc.hpp
//
// 06/06/1997       First version
// 06/23/1997       Put in pack pragmas
// 10/06/1998       SLO; Updated
// 04/04/1999       SLO; Update with new devices
// 12/15/1999       SLO; Update with new devices and system protocols
//
//----------------------------------------------------------------------------
#ifndef apiincHPP
#define apiincHPP
//----------------------------------------------------------------------------
#include "sysdefs.h"
#include "timecode.hpp"
#include "eventdef.hpp"
namespace apiinc
{
//-- type declarations -------------------------------------------------------

enum TTransition {
        TCUT,
        TMIX,
        TWIPE,
        TFADEFADE,
        TCUTFADE,
        TFADECUT,
        TMIXEDAV };

enum TSwitchRates {
        TSLOW,
        TMEDIUM,
        TFAST,
        TFASTCUT };

enum TKeysrc {
        KeySelf,
        Extrnl,
        Chroma };

enum TKeymod {
        Normal,
        Shadow,
        DropShadow,
        Border };

enum TKeyhold {
        LoseKey,
        HoldKey };

enum TMattemod {
        MatteOff,
        MatteOn };

typedef Word TKey;

typedef Shortint TRatio;

enum tAudMode {
        TSTEREO,
        TMONO,
        TMONOLEFT,
        TMONORIGHT };

enum tdevices {
        NODEV,
        VTRDEV,
        LMSDEV,
        BCRDEV,
        SWITCHERDEV,
        OTHERMEDDEV,
        GPIDEV,         // These are obsolete
        DISKDEV,
        ANNOTATOR
        };

typedef Byte etdevices;

typedef tdevices *ptdevices;

enum tvtrdevices {
        SONYVTR,
        AMPEXVTR,
        SONYDEMOVTR,
        VPR300VTR };

typedef Byte etvtrdevices;

enum tlmsdevices {
        SONYBETACART,
        SONYDVC80,
        SONYBVC80,
        SONYDVC300,
        SONYBVC400,
        SONYDVC500,
        SONYBVC500,
        SONYDVC1000,
        SONYBVC1000,
        FLEXICART,
        SONYDEMOBETA,
        SONYDEMO1000,
        ODETICSTCS90,
        ODETICSTCS90M,
        ODETICSTCS90LEM,
        ODETICSTCS90A,
        ODETICSTCS2000,
        SONYBVC1000A,
        MARCCART201,
        MARCCART401,
        MARCCART204,
        MARCCART404,
        MARCCART208,
        MARCCART408,
        MARCCART212,
        MARCCART412,
        ODETICSPROPHET,
        THOMSONPROCART,
        SONYBVC400B,
        MARCSMARTCART,
        DVCAMFLEXICART };

typedef Byte etlmsdevices;

enum tbcrdevices {
        SONYBVBR10,
        PCWAND30 };

typedef Byte etbcrdevices;

enum tswitchdevices {
        DATATEK_D4300A,
        GVG_100,
        MCS_2000,
        GVG_TENXL,
        GVG_MASTER21,
        GVG_HORIZON,
        GVG_440,
        GVG_7000,
        GVG_20TEN,
        USI_160,
        DATA_2400,
        RELAY_8X1,
        SONY_ROUTER,
        MCS_3000,
        USI_500,
        IMAGE_VIDEO,
        PRO_BEL2,
        SONY_1616,
        DEMO_ROUTER,
        BTS_SATURN,
        SONY_3232,
        SONY_6464,
        MARC_SWTC,
        THOM_9920,
        ROSS_316,
        PESA_2400,
        PROBELSWITCH,
        MCS_1600,
        USI_UDI,
        LEITCH_ROUTER,
        LEITCHX1_ROUTER,
        MCS_MASTER21,
        MCS_PRODIGY,
        QUARTZ_ROUTER,
        MCS_SONY_DVS_M1000,
        MCS_TEKTRONIX_M2100,
        TELECT_ROUTER,
        ADCOM_ROUTER,
        NVISION_1308_ROUTER,
        NVISION_3064_ROUTER,
        NVISION_3128_ROUTER,
        NVISION_3512_ROUTER,
        BTS_MARS_ROUTER,
        NVISION_1055_ROUTER,
        LMS_BASED_ROUTER,
        PROBEL_TX220,
        PROBEL_SYS2,
        PROBEL_SYS3,
        THOMSON_9920,
        VIDEOTEK_RS12,
        SIERRA1616,
        EASYKEY_31,
        DEMO_MCSWITCHER,
        SONY256_ROUTER,
        NETWORK_PROLINE_ROUTER
        };

typedef Byte etswitchdevices;

enum tothermediadevices {
        DIGI_360,
        SONY_SSCU1000,
        LEITCH_SS,
        CHYRON_MAX,
        CHYRON_CODI,
        PRESTO_CG,
        BTS_VIDIFONT,
        QUANTEL_SS,
        VIDDISK,
        DEMOVIDDISK,
        PROFILEVIDDISK,
        CBVIDDISK,
        DCBVIDDISK,
        SCCOM,
        QUANTA_DELTA,
        CHYRON_SCRIBE,
        SONY_MINIDISK,
        D2PORTVIDDISK,
        BRONTO_SS,
        CHYRON_MAXINE,
        MEDIAGPIDEV,
        BFETIMER,
        D3PORTVIDDISK,
        D4PORTVIDDISK,
        D5PORTVIDDISK,
        Demo5PORTVIDDISK,
        D8PORTVIDDISK,
        CAVENA_4CH,
        ESS3_SS,
        FLASHFILE_SS,
        DPM700,
        CLIPBOXVIDDISK,
        IRDETO,
        WGNER,
        IBT_VPS9601,
        SERCOM10H,
        ESS10CHDEV,
        LEITCH_LG,
        D10PORTVIDDISK,
        OXTELKEYER,
        ENCO_DAD,
        IBT_VPS9701,
        TEXT_CODI,
        COLLAGE_CG,
        INFINITE_CG,
        TYPEDEKO_CG,
        SCREENST_SRV2,
        PICTUREBOX2C_SS,
        ASTON_CG,
        IBT_VPS9702,
        IMAGESTORE_SS,
        HPBufferVidDisk,
        OrbanAirtime_AC,
        FLASHFILE_SS2,
        CHYRON_MAX2,
        LEITCH_SS2,
        THOM_MPEG,
        VLIDVIDDISK,
        MEDIAREQUESTER,
        MEDIADISTRIBUTOR,
        AVALON_ARCHIVE,
        MEDIARDRO,
        CLIPBOXCACHEDISK,
        CPTTIMER,
        STAS10DISK,
        MAV70DISK,
        DATABASEDISTRIBUTOR,
        MULTIREQUESTER,
        DISK_ARCHIVE,
        CHARISMA_DVE,
        LGA100LOGO,
        EVERTZ_8074_ENCODER,
        SGIVIDEODISK,
        BUGBURNER_SS,
        SONY_DME7000_DVE,
        NORPAK_TES5_ENCODER,
        SONY_MD_B3
        };

typedef Byte etothermediadevices;

enum tgpidevices {
        GPISWITCHDEV };

typedef Byte etgpidevices;

enum tannodevices {
        LANNOTATOR,
        DEMOANNOTATOR,
        GPIANNOTATOR,
        GPIAANNOTATOR };

typedef Byte etannodevices;

enum typeoflist {
        NOLISTTYPE,
        RANDOMLIST,
        COMPILERLIST,
        SEQPLAYLIST,
        ANOTATELIST,
        MEDIALIST,
        RDROLIST
      };

typedef Byte etypeoflist;

enum listactions {
        TIMEDLIST,
        THREADONBREAKLIST,
        SKIPBADEVENTS,
        PLAYHARDHITS,
        SWITCHBLACK,
        TENSIONOFF,
        LOGOONSKIP,
        UPDATEONAIR,
        RIPPLELIST,
        KEEPCARTTHREADED,
        DOABROUTING,
        TENSIONAFTERUC,
        CONTACTSTART,
        PLAYIDTITLE,
        EXTRA15,
        LETROLLBIT };

typedef Byte elistactions;
typedef Word  listactionset;

typedef Byte tlistnumber;

enum tprotocol {
        NOPROTOCOL,
        ampexservtr,
        sonyservtr,
        serbcr,
        serlms,
        serswitcher,
        sermedia,
        serannotator
        };

typedef Byte etprotocol;

enum mediastatetype {           // if you wanted to display these
        nomediachange,          // NOCHANGE
        mediainwithid,          // INJECTED
        mediainwithoutid,       // INJECTED
        mediaininternalid,      // INJECTED
        mediaout,               // UNTHREADED
        mediaouttostorage,      // UNTHREADED
        mediacued,              // CUED
        mediacueing,            // CUEING
        mediaidle,              // IDLE
        mediathreading,         // THREADING
        mediaunthreading,       // UNTHREADING
        mediatensionedon,       // TENSIONED ON
        mediatensionedoff,      // TENSIONED OFF
        mediaplaying,           // PLAYING
        mediarecording,         // RECORDING
        mediareadyforstorage }; // UNTHREADING

typedef Byte emediastatetype;

enum tstatus {          // below are display strings
        vlocal,         // LOCAL              
        unthreaded,     // UNTHRD
        eject,          // EJECT
        cued,           // CUED
        stop,           // STOP
        stopping,       // STOPPNG
        rewd,           // REWD
        ffwd,           // FFWD
        rec,            // RECORD
        ubplay,         // UB PLAY
        play,           // PLAY
        fwdshuttle,     // FWDSHTL
        revshuttle,     // REVSHTL
        fwdvar,         // FWDVAR
        revvar,         // REVVAR
        fwdjog,         // FWDJOG
        revjog,         // REVJOG
        slow,           // SLOW
        cueing,         // CUEING
        cue,            // CUE
        tenrel,         // TENREL
        tensionon,      // READY
        still,          // STILL
        reject,
        thread,
        inject,
        ubreaddone,
        standbyon,      // STANDBY
        unknown,        // UNKNOWN
        rwdeject,
        unknown1,
        novtr,          // NODEV
        // the following are internal only, no display values
        transitionit,
        previewit,
        threadvtr,
        lmsthread,
        sendvtrtobin,
        extake,
        markejectbin,
        ejectbin,
        lmssyspreset,
        lmssysreset,
        lmstimeset,
        lmseleinit,
        lmsbcread,
        lmsidread,
        lmsbinstatus,
        lmsvtrread,
        lmsdothread,
        lmsdoplay,
        lmsonair,
        lmsbinlight,
        lmsbcreadmove,
        lmsswitchinit,
        lmssysstat,
        lmssysvtrreset,
        vtrnocom,       // NO COM
        vtrpulled,      // PULLED
        threaderror,
        extakecolor,
        editon,
        editoff,
        tmselect,
        presetselect,
        extakeinit,
        lmsdisplay,
        lmscancelcue,
        gpisetreset,
        presetit,
        lmsbinreset,
        lmsthreadreturn,
        cuewithdata,
        playnothread,
        slownothread,
        stopnothread,
        stillnothread,
        lmsmoveelevator,
        tso,            // TSO
        purgehiddenbins,
        removeallstorage,
        adcneterror,
        tcgen2tctape,
        presettcgen,
        loadubid,
        reccue,
        previewplay,
        extakename,
        // the next two are display again.
        Busy,           // BUSY
        OffLine,        // OFFLINE
        // the following are internal only, no display values
        DeleteDStorage,
        ProtectDStorage,
        UnProtectDStorage,
        CacheNextStorage,
        // for video disks when initializing
        DeviceInitializing, // INITIAL
        // the following are internal only, no display values
        cSpaceRemainingCommand,
        cSizeOfIDCommand,
        cDeleteProtectIDCommand,
        cUnDeleteProtectIDCommand,
        cIDRequestCommand,
        cRenameIDCommand,
        cNewCopyOfIDCommand,
        cDeleteVideoFileCommand,
        cCopyVideoFileCommand,
        cDeleteVideoFileFromDiskCommand,
        cAbortCopyVideoFileCommand,
        cSetSOMCommand,
        cSetDurationCommand,
        cGetFromArchiveCommand,
        cSentToArchiveCommand,
        cDeleteFromArchiveCommand,
        READUSERBITS,
        cuewithIDdata
        };

typedef Byte etstatus;

enum tbcrstatus {
        bcrerror,
        bcrok,
        bcrread,
        bcrread1,
        bcrunknown,
        bcrunknown1 };

typedef Byte etbcrstatus;

enum tlmsstatus {
        lmsok,
        lmsnocom,
        lmsunknown,
        lmsnotcommunicating };

typedef Byte etlmsstatus;

enum tlmsstates {
        lmsdooropen,
        lmselevatorjam,
        lmsportfull,
        lmsvtrjam,
        lmsbinverify,
        lmsininitial,
        lmsshutdown,
        lmsmanual };

typedef Byte etlmsstates;

typedef Byte tlmsstateset;

enum tswitcherstatus {
        switcherok,
        switchernocom };

typedef Byte etswitcherstatus;

enum trunstatus {
        doneplay,
        doswitch,
        doplayissued,
        dopreroll,
        donerec,
        dopostroll,
        doreserved,
        doediton,
        doeditoff,
        doneskipped,
        dores11,
        dores12,
        dores13,
        dores14,
        dores15,
        dores16 };

typedef Byte etrunstatus;
typedef Word trunstatusset;

enum tvtrtypes {
        NORMALVTR,
        COMPILEVTR };

typedef Byte etvtrtypes;

enum tbinnotice {
        nonotice,
        binshouldeject,
        externalload,
        binnotvalid,
        bintosecondary };

typedef Byte etbinnotice;

enum TStorageFlag {
        MaterialInCacheQueue,
        MaterialNotRecorded,
        MaterialInArchive,
        MaterialDeleteProtected,
        MaterialDurration,
        MaterialAutoDubbed };

typedef Byte eTStorageFlag;

typedef Byte TStorageSet;

enum tclocks {
        tc,
        sync,
        pc,
        noclock };

typedef Byte etclocks;

enum durtype {
        bytetype,
        linttype };

typedef Byte edurtype;

enum tlistcmd {
        natput,
        natget,
        natinsert,
        natendinsert,
        natdelete,
        natdeleteall,
        npack,
        nlistchanged,
        nlookahead,
        nEjectStorageID,
        nDeleteDStorage,
        nProtectDStorage,
        nmoveevents,
        listdothread,
        listdoplay,
        listthreadevent,
        listfreeze,
        listrecue,
        listmakeready,
        nUnProtectDStorage,
        nactions,
        listunthreadevent,
        nlistname,
        nloadbarcode,
        nswitchevent,
        neventdone,
        neventundone,
        nmovekey,
        natputkey,
        liststop,
        listdoplaykey,
        listthreadeventkey,
        listunthreadeventkey,
        nswitcheventkey,
        liststopkey,
        listrecuekey,
        listabortevent,
        listabortkey,
        listdoplayone,
        listswapevent,
        listskipevent,
        listunthreadall,
        listlocklist,
        listunlocklist,
        listcheckcompile,
        listreadycompiler,
        listdoplaycompiler,
        threadcompilevtrs,
        threadonespot,
        playcompilevtrs,
        listpackcount,
        listthreadspecific,
        ncompileundone,
        listthreadtime,
        listswitchmaster,
        liststandbykey,
        listjamnewid,
        listhold,
        liststartupdate,
        listlengthen,
        logdiscrepency,
        listcutnext,
        listtensionrel,
        ntogglelookahead,
        nmediamade,
        npulldata,
        listgangplay,
        listplaysecondary,
        listplayinstant,
        natgetasrun,
        listpreviewplay,
        ndevicespecific,
        listkeepbreak,
        nCacheNextDStorage,
        nlistgangcommand };

typedef Byte etlistcmd;

#pragma pack(push, 1)
struct nreventhdr
{
        etlistcmd nlistcmd;
        short nlistid;
        short nlistindex;
        short numberofevents;
} ;

struct nrevent
{
        etlistcmd nlistcmd;
        Word nlistid;
        short nlistindex;
        short numberofevents;
        union
        {
                struct
                {
                        long fromkey;
                        long tokey;

                };
                char stringvalue[256];  // first byte is length
                short nothervalue;
                revent aevent[44];      // array of revent records
                Byte eventbuffer[4576];

        };
} ;

struct BinData
{
        Word binnumber;
        char bineid[8];
        char bintitle[16];
        Byte binSOM[4];
        Byte binDUR[4];
        Word ownercount;
        Byte currentowner;
        etbinnotice binnotice;
        TStorageSet StorageSet;
} ;

typedef BinData bindataarray[44];

struct nBinData
{
        etlistcmd nbincmd;
        Byte nbindevice;
        union
        {
                Byte devicedata[1716];
                struct
                {
                        short nbinindex;
                        short numberofbins;
                        BinData abin[44];

                };

        };
} ;

struct nSystemData
{
        Byte protocoldata[4096];
} ;
#pragma pack(pop)

struct messageRec;
typedef messageRec *messagePtr;

enum msgType { reply,
        login,
        logout,
        response,
        ncomcmd,
        ncomevent,
        ncombin,
        nsystemreq,
        nlouthobj,
        nnetcmd,    // not used by api applications
        nnetbin };  // not used by api applications

typedef Byte emsgType;

typedef Word logindata[8];

typedef Byte tcmddata[33];

#pragma pack(push, 1)
struct rcmdrec
{
        Byte msdeviceindex;
        Byte lsdeviceindex;
        etstatus thecmd;
        Byte thecmddata[33];
} ;

struct messageRec
{
        emsgType MessageID;
        union
        {
                nSystemData thesystemdata;
                nBinData thenBinData;
                nrevent thenrevent;
                rcmdrec ninputcmds;
                struct
                {
                        short ResponseID;
                        Boolean Accepted;

                };
                struct
                {
                        Byte whattype;
                        Word loginqualifier[8];

                };

        };
} ;
#pragma pack(pop)

enum liststatevalues {
        LISTISPLAYING,
        LISTTHREADING,
        LISTINFREEZE,
        LISTINHOLD,
        LISTUPCOUNTING,
        LISTRECUEING,
        LISTWILLEND,
        LISTWILLENDBREAK };

typedef Byte eliststatevalues;

typedef Byte liststateset;

#pragma pack(push, 1)
struct tlistdata
{
        etypeoflist listtype;
        Byte listchanged;				// incremented whenever list size changes
        Byte listdisplay;				// incremented whenever an event changes
        Byte listsyschange;			// incremented when list related values are changed. 
        short listcount;				// number of events in list 
        char lockname[9];				// First byte is the length
        Word lookahead;
        Word maxlistevents;			// largest list that can be loaded
        listactionset actions;
        char listname[13];      // First Byte is the length, contains dos filename XXXXXXXX.XXX
        Word packcount;					// number of events to leave on list
        Byte threadtime[4];			// HH:MM:SS:FF four bytes BCD encoded
        Byte listlogged;				// incremented when logged event changes
        liststateset liststate;
        eTCCompareType difference; // whether running short, or long to next hard start
        Byte offtime[4];        // HH:MM:SS:FF in BCD
        Byte timetonext[4];     // HH:MM:SS:FF in BCD time remaining till next hard start
} ;

struct tslmsvtr;
typedef tslmsvtr *ptslmsvtr;

struct tslmsvtr
{
        etdevices sthedev;
        Byte sthedevnumber;
        etvtrdevices ssubdevtype;
        char sdevname[5];
        trunstatusset slmsvtrrunstatus;
        etstatus slmsvtrstatus;
        char slmsvtrid[8];
        Word slmslistnum;
        Byte slistowner;
        etvtrtypes svtrtype;
        Byte tapeh;
        Byte tapem;
        Byte tapes;
        Byte tapef;
        emediastatetype devmediastate;
        union
        {
                long slmsrunduration;
                struct
                {
                        Byte slmsrundurh;
                        Byte slmsrundurm;
                        Byte slmsrundurs;
                        Byte slmsrundurf;

                };

        };
} ;

typedef tslmsvtr aslmsvtrs[1];

struct slms;
typedef slms *PSlms;

struct slms
{
        etdevices thedev;
        Byte thedevnumber;
        Byte maxvtrs;
        etlmsdevices subdevtype;
        char devname[5];
        etlmsstatus slmsstatus;
        tlmsstateset ssetstate;
        Byte binchanged;
        Byte bindisplay;
        short bincount;
        short emptybins;
        Word switchpointin;
        Word switchpointout;
        tslmsvtr theslmsvtr[1]; // max in LMS may be 6
} ;

struct svtr;
typedef svtr *PSvtr;

struct svtr
{
        etdevices thedev;
        Byte thedevnumber;
        etvtrdevices subdevtype;
        char devname[5];
        trunstatusset svtrrunstatus;
        etstatus svtrstatus;
        char svtrid[8];
        Word slistnum;
        Byte listowner;
        etvtrtypes svtrtype;
        Byte tapeh;
        Byte tapem;
        Byte tapes;
        Byte tapef;
        emediastatetype devmediastate;
        union
        {
                long srunduration;
                struct
                {
                        Byte srundurh;
                        Byte srundurm;
                        Byte srundurs;
                        Byte srundurf;

                };

        };
} ;
#pragma pack(pop)

#pragma pack(push, 1)
struct sbcr
{
        etdevices thedev;
        Byte thedevnumber;
        etbcrdevices subdevtype;
        char devname[5];
        etbcrstatus sbcrstatus;
        char sbcrid[8];
        char sbcrtitle[16];
        Byte sbcrdurh;
        Byte sbcrdurm;
        Byte sbcrdurs;
        Byte sbcrdurf;
        Byte sbcrsomh;
        Byte sbcrsomm;
        Byte sbcrsoms;
        Byte sbcrsomf;
} ;

typedef sbcr *PSbcr;

struct sswitcher
{
        etdevices thedev;
        Byte thedevnumber;
        etswitchdevices subdevtype;
        char devname[5];
        etswitcherstatus switchstatus;
        Word switchpointin;
        Word switchpointout;
} ;

typedef sswitcher *psswitcher;

enum tgpistatus {
        gpiisok,
        gpiunknown };

typedef Byte etgpistatus;

struct sgpichannel
{
        trunstatusset sgpirunstatus;
        etstatus sgpistatus;
        char sgpiid[8];
        Byte sgpiidchange;
        Word sgpilistnum;
        Byte slistowner;
        emediastatetype devmediastate;
        union
        {
                long sgpirunduration;
                struct
                {
                        Byte sgpirundurh;
                        Byte sgpirundurm;
                        Byte sgpirundurs;
                        Byte sgpirundurf;

                };

        };
} ;

typedef sgpichannel asgpichannel[6];

struct sgpidev
{
        etdevices thedev;
        Byte thedevnumber;
        etgpidevices subdevtype;
        char devname[5];
        Byte maxgpis;
        etgpistatus sgpidevstatus;
        sgpichannel thesgpidev[6];
} ;

typedef sgpidev *psgpidev;

struct smediaplayer
{
        trunstatusset srunstatus;
        etstatus sstatus;
        char sid[8];
        Word slistnum;
        Byte slistowner;
        Byte tapeh;
        Byte tapem;
        Byte tapes;
        Byte tapef;
        emediastatetype sdevmediastate;
        union
        {
                long srunduration;
                struct
                {
                        Byte srundurh;
                        Byte srundurm;
                        Byte srundurs;
                        Byte srundurf;

                };

        };
} ;

typedef smediaplayer amediaplayer[1];

struct smediadevice
{
        etdevices thedev;
        Byte thedevnumber;
        etothermediadevices subdevtype;
        char devname[5];
        Byte maxplayers;
        Byte smediastatus;
        Word MStorageCount;
        Word MFreeStorageCount;
        Byte StorageChanged;
        smediaplayer thesplayers[1];  // worst case is current 12
} ;

typedef smediadevice *psmediadevice;

enum tanostatus {
        anonull,
        anonoserial,
        anofault,
        noano,
        anoblack,
        anocb,
        anoprogram,
        anoblackaudio,
        anocbaudio,
        anoprogramaudio };

typedef Byte etanostatus;

struct sano
{
        etdevices thedev;
        Byte thedevnumber;
        etannodevices subdevtype;
        char devname[5];
        etanostatus sanostatus;
} ;
#pragma pack(pop)

typedef sano *PSano;

enum ERRORTYPE {
        NOERRORS,
        SYSTEM_ERROR,
        LIST_ERROR,
        DEVICE_ERROR,
        USERNOTIFICATIONS,
        DIAGNOSTIC_ERROR
        };

typedef Byte tERRORTYPE;

#pragma pack(push, 1)
struct ERROR_PACKET
{
        tERRORTYPE error_kind;
        Word error_number;
        Byte minor_error;
        Byte list_number;
        Byte major_device;
        Byte minor_device;
        Word event_number;
        char id[8];
        char title[16];
        Byte som[4];
        Byte dur[4];
        char bin_number[4];
        Byte timeofday[4];
} ;
#pragma pack(pop)


enum  tsystemprotocols  { tprotocol1,
                      tprotocol2,
                      tprotocol3,
                      tprotocol4,
                      tprotocol5,
                      tprotocol6,
                      tprotocol7,
                      tprotocol8,
                      tsetheadowner,
                      tgetgpistatus,
                      tsetgpibit,
                      tgetdevicenames,
                      tgetlistnames
                      };

typedef Byte etsystemprotocols;

#pragma pack(push, 1)
struct TDEVICENAMERECORD
{
  Byte majorprotocol;
  Byte minorprotocol;
  char devicename [68] [17]; // First byte is the length;
                             // 68 such devices
};
#pragma pack(pop)

#pragma pack(push, 1)
struct TLISTNAMERECORD
{
  Byte majorprotocol;
  Byte minorprotocol;
  char listname [16] [33];   // First byte is the length;
                             // Sixteen such lists.
};
#pragma pack(pop)

//#pragma pack(push, 1)
//struct ERROR_PACKET
//{
//        tERRORTYPE error_kind;
//        Word error_number;
//        Byte minor_error;
//        Byte list_number;
//        Byte major_device;
//        Byte minor_device;
//        Word event_number;
//        char id[8];
//        char title[16];
//        Byte som[4];
//        Byte dur[4];
//        char bin_number[4];
//        Byte timeofday[4];
//} ;
//#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define MAXDEVSIZE (Word)(475)
#define MAXNETEVENTS (Byte)(44)
#define MAXNETBLOCK (Word)(2048)
#define MAXSYSLISTS (Byte)(16)
#define FULLCHANNELS (Byte)(68)

#define MAXSUBDEVICES (Byte)(20)
#define STARTDEVICEOWNERS (Byte)(128)
#define RecordCacheOffSet (Byte)(32)
#define STARTREMOTEOWNERS (Byte)(196)

#define BLACKINDEX (Byte)(1)
#define COLORBARINDEX (Byte)(2)
#define STATIONIDINDEX (Byte)(3)
#define INITINDEX (Byte)(4)

#define MAXSWITCHERINPUTS (Word)(32767)
#define idlen (Byte)(8)
#define titlelen (Byte)(16)
#define eventnumlen (Byte)(4)
#define sonyeventnumlen (Byte)(8)
#define listnamelen (Byte)(12)
#define cmddatalen (Byte)(32)
#define cmdlen (Byte)(32)
#define DEVICENAMELEN (Byte)(5)

#define NOLIST (Byte)(0)
#define LIST1 (Byte)(1)
#define LIST2 (Byte)(2)
#define LIST3 (Byte)(3)
#define LIST4 (Byte)(4)
#define LIST5 (Byte)(5)
#define LIST6 (Byte)(6)
#define LIST7 (Byte)(7)
#define LIST8 (Byte)(8)
#define LIST9 (Byte)(9)
#define LIST10 (Byte)(10)
#define LIST11 (Byte)(11)
#define LIST12 (Byte)(12)
#define LIST13 (Byte)(13)
#define LIST14 (Byte)(14)
#define LIST15 (Byte)(15)
#define LIST16 (Byte)(16)

#define SYSTEM_STATUS (Shortint)(-1)
#define LIST_STATUS (Byte)(0)

// SYSTEM ERRORS
#define NO_CONNECTION (Byte)(0)
#define SUCCESS (Byte)(0)
#define INVALID_BLOCK (Byte)(1)
#define REF_SHIFT_VID (Byte)(1)
#define REF_SHIFT_PC (Byte)(2)
#define BACKGROUND_GONE (Byte)(3)
#define NO_DATAGRAM_NCBS (Byte)(200)
#define NO_RESPONSE_NCBS (Byte)(201)
#define NCB_TIMEOUT (Byte)(202)

// LIST ERRORS
#define NOTPLAYED_NOTCUED (Byte)(1)
#define NOTPLAYED_MISSING (Byte)(2)
#define NOTPLAYED_NOTTHREADED (Byte)(3)
#define NOTPLAYED_SKIPPED (Byte)(4)
#define NOTPLAYED_TIMED_EVENT (Byte)(5)
#define NOTPLAYED_TIME_ERROR (Byte)(6)
#define BACKUP_SWITCHED_IN (Byte)(7)
#define MASTER_FAILURE (Byte)(8)
#define EVENT_STOPPED_SKIPPED (Byte)(9)
#define SEC_PLAYED_NOT_CUED (Byte)(10)
#define NOTHING_TO_SKIP_TO (Byte)(11)
#define EVENT_HAS_INVALID_SOM (Byte)(12)
#define CURRENT_EVENTWAS_CUT (Byte)(13)
#define VTR_IN_RECINHIBIT (Byte)(14)
#define RECORD_EVENT_STOPPED (Byte)(15)
#define CURRENT_EVENTWAS_PACKED (Byte)(16)
#define CURRENT_EVENTWAS_RECUED (Byte)(17)
#define MEDIA_WAS_REMOVED (Byte)(20)
#define LIST_ROLLOVER_ERROR (Byte)(21)
#define MEDIA_DIDNOT_RESPOND_FOR_PLAY (Byte)(22)
#define MEDIA_DIDNOT_RESPOND_FOR_STANDBY (Byte)(23)
#define HEAD_REMOVED_NOSTORAGE (Byte)(25)
#define HEAD_REMOVED_EJECTED (Byte)(26)
#define HEAD_REMOVED_INVALSTOR (Byte)(27)
#define BACKUP_NOT_READY (Byte)(30)
#define NO_RECORD_RESOURCE (Byte)(31)
#define RECORD_EVENT_WASCUT (Byte)(32)
#define PROTECT_COPY_NOT_VALID (Byte)(33)
#define PROTECT_COPY_CHANGED (Byte)(34)
#define UNABLE_TO_ASSIGN_DEVICE (Byte)(40)
#define UNABLE_TO_RELEASE_DEVICE (Byte)(41)
#define INVALID_SYSTEM_COMMAND (Byte)(42)
#define UNABLE_TO_FIND_DEVICE (Byte)(43)
#define UNABLE_TO_FIND_PROTECT_DEVICE (Byte)(44)
#define INVALID_MINORDEVICE (Byte)(45)
#define UNABLE_TO_RELEASE_INPLAY (Byte)(46)
#define ANOTATION_MEDIA_REMOVED (Byte)(50)
#define NO_PROGRAM_EVENTS (Byte)(51)
#define PLAYOUT_TIMES_SAME (Byte)(52)
#define RECORD_EVENT_MISSED (Byte)(53)
#define ANOTATION_MEDIA_SHORT (Byte)(54)

// DEVICE ERRORS
#define UNABLE_TO_CUE (Byte)(1)
#define TAPE_RAN_OUT (Byte)(2)
#define VTR_FAILURE (Byte)(3)
#define SYNC_PLAY_FAILURE (Byte)(4)
#define NO_MORE_BIN_SPACE (Byte)(5)
#define DUPLICATE_TAPE_ID (Byte)(6)
#define BIN_REMOVED_DURING_THREAD (Byte)(7)
#define THREAD_FAILURE (Byte)(8)
#define THREAD_REISSUED (Byte)(9)
#define DISTURBED_MACRO (Byte)(10)
#define THREAD_ERROR_UNKNOWN (Byte)(11)
#define READ_BARCODE_UNKNOWN (Byte)(12)
#define LOST_BARCODE_READ (Byte)(13)
#define LOST_THREAD_VTR (Byte)(14)
#define LOST_CUE (Byte)(15)
#define LOST_VTR_TO_BIN (Byte)(16)
#define LOST_RWDEJECT (Byte)(17)
#define LOST_ELEV_INIT (Byte)(18)
#define LOST_EJECT_BIN (Byte)(19)
#define LOST_SYNC_PLAY (Byte)(20)
#define BAD_BARCODE_READ (Byte)(21)
#define MACRO_BUSY_ERROR (Byte)(22)
#define STUCK_CASSETTE_FAILURE (Byte)(23)
#define DOOR_IS_OPENED (Byte)(24)
#define OUTPORT_IS_FULL (Byte)(25)
#define OUTPORT_IS_FAILED (Byte)(26)
#define VTR_EJECT_FAILURE (Byte)(27)
#define LMS_COMM_FAILURE (Byte)(28)
#define UNUSED_ERROR_CODE (Byte)(29)
#define DEVICE_COMM_FAILURE (Byte)(30)
#define SERVO_REF_MISSING (Byte)(31)
#define TAPE_TROUBLE (Byte)(32)
#define HARD_ERROR (Byte)(33)
#define DEVICE_WENT_INTO_LOCAL (Byte)(34)
#define DEVICE_NOT_IN_EDITON (Byte)(35)
#define COMMAND_ISSUED_IN_LOCAL (Byte)(36)
#define INTERNALID_READFAILED (Byte)(37)
#define ELEVATOR_OFFLINE (Byte)(40)
#define DUPLICATE_IN_VTR (Byte)(41)
#define VTR_BUZZER                (Byte)(42)
#define VTR_SERVO_ALARM           (Byte)(43)
#define VTR_SYSTEM_ALARM          (Byte)(44)
#define VTR_DIAGNOSTIC_STATUS     (Byte)(45)    { needs minor value }
#define VTR_AUDIO_RAW_VERIFY_ERROR(Byte)(46)
#define VTR_VIDEO_RAW_VERIFY_ERROR(Byte)(47)
#define VTR_AUDIO_DATA_ERROR_LEVEL(Byte)(48)
#define VTR_VIDEO_DATA_ERROR_LEVEL(Byte)(49)
#define AUTO_ENABLE_OFF (Byte)(50)
#define SWITCHER_LOST_COMM (Byte)(51)
#define SWITCHER_NAK (Byte)(52)
#define AUTO_ENABLE_ON (Byte)(53)

#define DEVICE_NAK                (Byte) (60)
#define TIMEOUT_RETRY             (Byte) (61)

// archive errors
#define DEVICE_ACK                (Byte) (70)
#define DEVICE_NAK_UNEXP          (Byte) (71)
#define ID_MISMATCH               (Byte) (72)
#define MACRO_NOT_FOUND           (Byte) (73)
#define COMMAND_TIMEOUT           (Byte) (74)
#define INVALID_STATE             (Byte) (75)
#define ID_NOT_IN_DISK            (Byte) (76)
#define ID_NOT_IN_ARCHIVE         (Byte) (77)
#define ID_ALREADY_IN_ARCHIVE     (Byte) (78)

// GMT errors
#define UNKNOWN_MACRO_RESPONSE    (Byte) (80)
#define REQUEST_FAILED            (Byte) (81)
#define NOT_IMPLEMENTED           (Byte) (82)
#define MACRO_TIMEOUT             (Byte) (83)

#define AXIS_OFF_LINE (Byte)(100)
#define ARM_OFF_LINE (Byte)(101)
#define ARM_NOT_INDEXED (Byte)(102)
#define UNUSABLE_LOCATION (Byte)(103)
#define NO_CASSETTE_IN_GRIPPER (Byte)(104)
#define TARGETING_OR_DOOR_OPEN (Byte)(105)
#define RC_SOFTWARE_ERROR (Byte)(106)
#define EMERGENCY_STOP_ACTIVE (Byte)(107)
#define MOVE_WITH_LIGHT_CURTAIN_ACTIVE (Byte)(108)
#define LOAD_PORT_NOT_INDEXED (Byte)(109)
#define CHC_NOT_RESPONDING_TO_RC (Byte)(110)
#define BARCODE_READER_ERROR (Byte)(111)
#define LOAD_PORT_STALL_NOT_CLEARED (Byte)(112)
#define FETCH_OR_INSERT_INVALID (Byte)(113)
#define NO_VTR_AT_LOCATION (Byte)(114)
#define CHC_SLIDER_NOT_RETRACTED (Byte)(115)
#define INCORRECT_SIZE (Byte)(116)
#define DISABLE_DOOR_MODE (Byte)(117)
#define LOAD_PORT_INDEX_PROBLEM (Byte)(118)
#define MALFUNCTION_ERROR (Byte)(119)
#define CART_TRANSPORT_ERROR (Byte)(120)
#define LOADPORT_FULL_ERROR (Byte)(121)
#define VTR_TAPE_SIZE (Byte)(122)
#define HUB_OFF_LINE (Byte)(123)
#define GRIPPER_NOT_CLEARED (Byte)(124)
#define INVALID_PARM_ERROR (Byte)(125)
#define CART_JARRED (Byte)(126)
#define MECHANICAL_ERROR (Byte)(127)
#define SLIDER_NOT_HOME (Byte)(128)
#define BIN_IN_USE_ERROR (Byte)(129)
#define TAPE_IN_VTR (Byte)(130)
#define REMOVE_TAPES (Byte)(131)
#define MARC_COMMAND_ERR (Byte)(132)
#define MARC_VTR_ERR (Byte)(133)
#define MARC_ROBOT_ERR (Byte)(134)
#define MARC_OTHER_ERR (Byte)(135)
#define MARC_UNKNOWN (Byte)(136)
#define ROBOT_WARNING (Byte)(137)
#define ROBOT_ERROR (Byte)(138)
#define CASSETTE_PROTRUSION (Byte)(139)
#define MARC_HARD_VTRERR (Byte)(140)
#define MARC_VTRCMD_ERR (Byte)(141)
#define MARC_ROBOTCMD_ERR (Byte)(142)
#define MARC_BCRCMD_ERR (Byte)(143)
#define MARC_MISCCMD_ERR (Byte)(144)
#define MARC_DOOR_TIMEOUT (Byte)(145)
#define MARC_ELEVATOR_TIMEOUT (Byte)(146)
#define MARC_VTRCMD_TIMEOUT (Byte)(147)
#define DEVICE_MISINITIALIZED (Byte)(150)
#define CACHING_FAILURE (Byte)(151)
#define DISK_WARNING (Byte)(152)
#define DISK_ERROR (Byte)(153)
#define DISK_NOT_CUED_IN_TIME (Byte)(154)
#define NO_VIDEO_INPUT (Byte)(155)
#define NO_REF_INPUT (Byte)(156)
#define DISK_NOT_SUPPORTED (Byte)(157)
#define DISK_CMD_WHILE_BUSY (Byte)(158)
#define DISK_DISK_FULL (Byte)(159)
#define DISK_WRONG_PORT_TYPE (Byte)(160)
#define DISK_INVALID_PORT (Byte)(161)
#define DISK_ILLEGAL_VALUE (Byte)(162)
#define DISK_SYSTEM_ERROR (Byte)(163)
#define DISK_ID_DELETE_PROTECTED (Byte)(164)
#define DISK_ID_TRANSFERRED (Byte)(165)
#define DISK_ID_NOT_TRANSFERRED (Byte)(166)
#define DISK_ID_STILL_PLAYING (Byte)(167)
#define DISK_ID_STILL_RECORDING (Byte)(168)
#define DISK_ID_ALREADY_EXISTS (Byte)(169)
#define DISK_ID_NOT_FOUND (Byte)(170)
#define DISK_INVALID_ID (Byte)(171)
#define DISK_SYSTEM_REBOOTED (Byte)(172)
#define DISK_CUE_OR_OPERATION_FAILED (Byte)(173)
#define DISK_PORT_NOT_ACTIVE (Byte)(174)
#define DISK_PORT_PLAYING_OR_ACTIVE (Byte)(175)
#define DISK_PORT_NOT_IDLE (Byte)(176)
#define DISK_CUE_NOT_DONE (Byte)(177)
#define DISK_NOT_IN_CUE_STATE (Byte)(178)
#define DISK_NO_OUTPUT_SELECTED (Byte)(179)
#define DISK_AUDIO_OVERLOAD (Byte)(180)
#define DISK_NO_AUDIO_INPUT (Byte)(181)
#define DISK_PORT_NOT_OPENED (Byte)(182)
#define DISK_MATERIAL_IS_SHORT (Byte)(183)
#define DISK_PORT_DOWN (Byte)(184)
#define DISK_PORT_ZERO (Byte)(185)
#define COMM_RESTORED (Byte)(186)
#define COMM_LOST (Byte)(187)
#define DISK_NAK_TIME_OUT (Byte)(188)
#define DISK_NAK_FRAMING_ERROR (Byte)(189)
#define DISK_NAK_OVER_FLOW (Byte)(190)
#define DISK_NAK_PARITY_ERROR (Byte)(191)
#define DISK_NAK_CHECKSUM_ERROR (Byte)(192)
#define DISK_NAK_UNDEFINED_ERROR (Byte)(193)
#define DISK_LOST_DISK_REPLY (Byte)(194)
#define DISK_DELETED_THREADED_ID (Byte)(195)
#define DISK_CACHE_RECORDER_FAILED (Byte)(196)
#define DISK_FAILED_PLAY (Byte)(197)
#define DISK_FAILED_RECORD (Byte)(198)
#define VTR_STATUS_ERROR (Byte)(200)
#define LMS_UNKNOWN_COMM (Byte)(201)

// USERNOTIFICATIONS
#define LOAD_MANUAL_TAPE (Byte)(1)
#define UNLOAD_MANUAL_TAPE (Byte)(2)
#define NOTIFY_BREAKEVENT (Byte)(3)

// DIAGNOSTIC ERRORS
#define DISK_DELETED_SPOT (Byte)(155)
#define DISK_RECORDED_SPOT (Byte)(156)
#define DISK_ISSUED_PLAY (Byte)(157)
#define DISK_ISSUED_STOP (Byte)(158)
#define DISK_ISSUED_REC (Byte)(159)
#define PLAY_RECORD_STATUS_LATE (Byte)(160)
#define GOT_PLAY_RECORD_STATUS (Byte)(161)
#define GOT_DISK_IDLE_STATUS (Byte)(162)
#define GOT_DISK_CUED_STATUS (Byte)(163)
#define DISK_ISSUED_CUE (Byte)(164)
#define DISK_ISSUED_UnPROT (Byte)(165)
#define DISK_ISSUED_PROTECT (Byte)(166)
#define DIAG_BACKGROUND_GONE (Byte)(171)
#define DIAG_TRANSITION (Byte)(53)
#define DIAG_SWITCH_PRESET (Byte)(54)
#define DIAG_AUDIO_OVER (Byte)(55)
#define DIAG_KEY_BUS (Byte)(56)
#define DIAG_PRESET_EFFECT_BEGIN (Byte)(57)
#define DIAG_TAKE_SENT (Byte)(58)
#define DIAG_SWITCH (Byte)(180)
#define LMS_LOG_SWITCHER (Byte)(170)

//-- template instantiations -------------------------------------------------

}       /* namespace Newsincd */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace apiinc;
#endif
//-- end unit ----------------------------------------------------------------
#endif  // newsincd
