//----------------------------------------------------------------------------
// apilib32.hpp - bcbdcc32 generated hdr (DO NOT EDIT) rev: 0
// From: apilib32.pas
//
// 06/06/1997       First version
//
//----------------------------------------------------------------------------
#ifndef apilib32HPP
#define apilib32HPP
//----------------------------------------------------------------------------
#include <windows.h>
#include "netdefs.hpp"
#include "sysdefs.h"
namespace apilib32
{
//-- type declarations -------------------------------------------------------
typedef Word *TWORDPTR;

typedef int *TIntPtr;

//-- var, const, procedure ---------------------------------------------------
extern "C" Byte  connect(char * PServerName, char * PClientName, char * PErrorString);
extern "C" Byte SendData(Byte Handle, void * MessagePointer, int length);
extern  "C" Byte RecData(Byte Handle, void * DataPointer, TIntPtr LengthPtr);
extern  "C" Byte DisConnect(Byte Handle);
extern  "C" Byte  SetupStatusPtr(Byte Handle, int StatusBlock, void * StatusPointer);
extern  "C" Netdefs::TConnectionStatus  GetStatus(Byte Handle);

}       /* namespace apilib32 */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace apilib32;
#endif
//-- end unit ----------------------------------------------------------------
#endif  // apilib32
