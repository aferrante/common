// ADC.h: interface for the CADC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADC_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
#define AFX_ADC_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// the following line forces a link with the harris api lib
#pragma comment(lib, "APILIB32.lib")
#include "../../MSG/MessagingObject.h"
#include "ADCDefs.h"

class CADC : public CMessagingObject
{
public:

	CAConnection**	m_ppConn;  // pointer to an array of pointers to connection types
	int							m_nNumConn;
  messageRec			m_SendData;
  messageRec			m_ReceiveData;

	unsigned char*	m_pucEventStreamBuffer;  //a buffer for loading up a bunch of events
	unsigned short	m_usStreamBufferLength;  //length of the stream events buffer

  revent					m_Event;  // one event, used for modifications and as a swap buffer event.
	unsigned char*	m_pucExtendedEventBuffer;  //extended data swap buffer
	unsigned short	m_usExtendedBufferLength;  // length of the buffer and the data. (max 4096)

  revent					m_VerifyEvent;  // one event, used for verifications 
	unsigned char*	m_pucVerifyExtendedEventBuffer;  //extended data swap buffer
	unsigned short	m_usVerifyExtendedBufferLength;  // length of the buffer and the data. (max 4096)

	CRITICAL_SECTION m_crit;  //  critical section.for API calls
	CRITICAL_SECTION m_critGetEvents;  //  critical section.for getting events calls

  char* m_pchDebugFilename;

public:
	CADC();
	virtual ~CADC();

// override
//	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);

// NOTE: passed in Lists are 1-based, but events are zero-based

// core - main
	CAConnection* ConnectServer(char* pszServerName, char* pszClientName=NULL); // returns pointer to connection
	CAConnection* ConnectionExists(char* pszServerName); // returns pointer to connection
	int DisconnectServer(CAConnection* pConn);
	int DisconnectServer(unsigned char ucHandle);
	int DisconnectServer(char* pszServerName); // searches for handle
	int SendListChange(CAConnection* pConn, int nList);
	int LockList(CAConnection* pConn, int nList);
	int UnlockList(CAConnection* pConn, int nList);
	int CountEventsInStream(unsigned char* pchMessageRec, unsigned long ulMessageLen, unsigned char*** pppchEventMarkers=NULL);
	int StreamToArray(CAList* pList, int nStartIndex, unsigned char* pchMessageRec, unsigned char ucFrameBasis=0x29, int nNumLimitEvents=-1, int nSafeBufferLength=-1);
	int EventToStream(CAEvent* pEvent, unsigned long ulFlags=ADC_TARGET_SWAP_EVENT, unsigned char ucFrameBasis=0x29);
	int InitEvent(unsigned long ulFlags=ADC_SWAP_EVENT);


// core - read only and low overhead
	int GetStatusData(CAConnection* pConn);
	int GetNextPrimaryIndex(CAConnection* pConn, int nList, int nIndex, unsigned long ulFlags=ADC_GETEVENTS_LIST);
	int GetPreviousPrimaryIndex(CAConnection* pConn, int nList, int nIndex, unsigned long ulFlags=ADC_GETEVENTS_LIST);
	int GetNumSecondaries(CAConnection* pConn, int nList, int nIndex, unsigned long ulFlags=ADC_GETEVENTS_LIST);

// core - read-write and higher overhead
	int GetEvents(CAConnection* pConn, int nList, int nIndex, int nNumEvents, unsigned long ulFlags = ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET, unsigned long ulTimeoutMS=ADC_DEFAULT_TIMEOUT, bool* pbStatusChange=NULL, tlistdata* pinitlistdata=NULL);
	int GetEventsUntilPlaying(CAConnection* pConn, int nList, int nStartIndex, int nMaxEvents, unsigned long ulFlags = ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_EVENTLOOK|ADC_GETEVENTS_INCLUDEDONE, unsigned long ulTimeoutMS=ADC_DEFAULT_TIMEOUT, bool* pbStatusChange=NULL, tlistdata* pinitlistdata=NULL);
	int GetEventsUntilIDs(CAConnection* pConn, int nList, int nStartIndex, int nMaxEvents, char* pchIDs[], int* pnIndex[], int nNumIDs, unsigned long ulFlags = ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_INCLUDEDONE|ADC_GETEVENTS_ALL_IDS, unsigned long ulTimeoutMS=ADC_DEFAULT_TIMEOUT, bool* pbStatusChange=NULL, tlistdata* pinitlistdata=NULL);
	int GetEventsUntilEvent(CAConnection* pConn, int nList, int nStartIndex, int nMaxEvents, unsigned long ulFlags = ADC_VERIFY_ID|ADC_VERIFY_TYPE|ADC_GETEVENTS_LIST|ADC_GETEVENTS_DONTGET|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_INCLUDEDONE, unsigned long ulTimeoutMS=ADC_DEFAULT_TIMEOUT, bool* pbStatusChange=NULL, tlistdata* pinitlistdata=NULL);
	int MoveEventsBlockToIndex(CAConnection* pConn, int nList, int nStartIndex, int nNumEvents, int nTargetIndex, unsigned long ulFlags=ADC_PROTECT_PLAY|ADC_PROTECT_DONE);
	int SwapEventBlocks(CAConnection* pConn, int nList, int nBlockOneStartIndex, int nBlockOneNumEvents, int nBlockTwoStartIndex, int nBlockTwoNumEvents, unsigned long ulFlags=ADC_PROTECT_PLAY|ADC_PROTECT_DONE);
	int	InsertEventAt(CAConnection* pConn, int nList, int nIndex, /*int nNumEvents=1,*/ unsigned long ulFlags=ADC_PROTECT_PLAY);
	int	DeleteEventAt(CAConnection* pConn, int nList, int nIndex, int nNumEvents=1, unsigned long ulFlags=ADC_VERIFY_ID|ADC_VERIFY_TYPE|ADC_PROTECT_PLAY|ADC_PROTECT_DONE);
	int	ModifyEventAt(CAConnection* pConn, int nList, int nIndex, /*int nNumEvents=1,*/ unsigned long ulFlags=ADC_VERIFY_ID|ADC_VERIFY_TYPE|ADC_PROTECT_PLAY|ADC_PROTECT_DONE);

// utility
	int	FieldLength(char* pchArray, int nArrayLength, char* pchNulls, int nNumNulls);
	unsigned long	ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned char ucFrameBasis=0x29, unsigned long ulFlags=ADC_NORMAL);  // pass in the systemfrx from the system data
	void	ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned char ucFrameBasis=0x29, unsigned long ulFlags=ADC_NORMAL);// pass in the systemfrx from the system data
	bool	IsIdenticalEvent(unsigned long ulFlags=ADC_VERIFY_ALL);
};

#endif // !defined(AFX_ADC_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
