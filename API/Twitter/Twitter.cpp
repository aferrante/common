// Twitter.cpp: implementation of the CTwitter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Twitter.h"
#include <process.h>
#include "../../TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTwitter::CTwitter()
{
	m_bKillThread = true;
	m_bThreadStarted = false;

	InitializeCriticalSection(&m_crit);
	InitializeCriticalSection(&m_critGlobal);
}

CTwitter::~CTwitter()
{
	m_bKillThread = true;
	while(m_bThreadStarted)
	{
		Sleep(10);
	}
	DeleteCriticalSection(&m_crit);
	DeleteCriticalSection(&m_critGlobal);
}

