// Twitter.h: interface for the CTwitter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TWITTER_H__7E5F8936_A9C5_4E4A_875E_616811CC0D0A__INCLUDED_)
#define AFX_TWITTER_H__7E5F8936_A9C5_4E4A_875E_616811CC0D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//#include "../../TTY/Serial.h"
#include "../../MSG/MessagingObject.h"


#define TWITTER_SUCCESS			 0
#define TWITTER_ERROR			-1



class CTwitter : public CMessagingObject
{
public:
	CTwitter();
	virtual ~CTwitter();


public:
	CRITICAL_SECTION m_crit;
	CRITICAL_SECTION m_critGlobal;
	bool m_bKillThread;
	bool m_bThreadStarted;
};

#endif // !defined(AFX_TWITTER_H__7E5F8936_A9C5_4E4A_875E_616811CC0D0A__INCLUDED_)
