// TwitterTestDlg.h : header file
//

#if !defined(AFX_TWITTERTESTDLG_H__CA2EEB69_51E1_4FC9_9E23_E35F0AE61AEF__INCLUDED_)
#define AFX_TWITTERTESTDLG_H__CA2EEB69_51E1_4FC9_9E23_E35F0AE61AEF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CTwitterTestDlg dialog

class CTwitterTestDlg : public CDialog
{
// Construction
public:
	CTwitterTestDlg(CWnd* pParent = NULL);	// standard constructor

	int m_nPage;
// Dialog Data
	//{{AFX_DATA(CTwitterTestDlg)
	enum { IDD = IDD_TWITTERTEST_DIALOG };
	CListCtrl	m_list;
	CString	m_szSearch;
	CString	m_szEncode;
	CString	m_szDecode;
	CString	m_szHashtag;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTwitterTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL


	void SetBackNext();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTwitterTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonSearch();
	afx_msg void OnButtonEncode();
	afx_msg void OnButtonDecode();
	afx_msg void OnKillfocusEditHashtag();
	afx_msg void OnButtonBack();
	afx_msg void OnButtonNext();
	afx_msg void OnButtonMakeurl();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TWITTERTESTDLG_H__CA2EEB69_51E1_4FC9_9E23_E35F0AE61AEF__INCLUDED_)
