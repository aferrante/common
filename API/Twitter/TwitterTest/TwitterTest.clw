; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTwitterTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "TwitterTest.h"

ClassCount=3
Class1=CTwitterTestApp
Class2=CTwitterTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_TWITTERTEST_DIALOG

[CLS:CTwitterTestApp]
Type=0
HeaderFile=TwitterTest.h
ImplementationFile=TwitterTest.cpp
Filter=N

[CLS:CTwitterTestDlg]
Type=0
HeaderFile=TwitterTestDlg.h
ImplementationFile=TwitterTestDlg.cpp
Filter=D
LastObject=IDC_BUTTON_MAKEURL
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=TwitterTestDlg.h
ImplementationFile=TwitterTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_TWITTERTEST_DIALOG]
Type=1
Class=CTwitterTestDlg
ControlCount=16
Control1=IDOK,button,1208025089
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_SEARCH,edit,1350631552
Control4=IDC_BUTTON_SEARCH,button,1342242816
Control5=IDC_BUTTON_ENCODE,button,1342242816
Control6=IDC_EDIT_ENCODE,edit,1350631552
Control7=IDC_EDIT_DECODE,edit,1350631552
Control8=IDC_BUTTON_DECODE,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT_HASHTAG,edit,1350631552
Control11=IDC_LIST1,SysListView32,1350664201
Control12=IDC_STATIC,static,1342308352
Control13=IDC_BUTTON_NEXT,button,1476460544
Control14=IDC_BUTTON_BACK,button,1476460544
Control15=IDC_STATIC,static,1342308352
Control16=IDC_BUTTON_MAKEURL,button,1342242816

