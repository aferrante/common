// TwitterTest.h : main header file for the TWITTERTEST application
//

#if !defined(AFX_TWITTERTEST_H__80E07D87_B07D_4D9A_BBEA_D79F3F048F13__INCLUDED_)
#define AFX_TWITTERTEST_H__80E07D87_B07D_4D9A_BBEA_D79F3F048F13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTwitterTestApp:
// See TwitterTest.cpp for the implementation of this class
//

class CTwitterTestApp : public CWinApp
{
public:
	CTwitterTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTwitterTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTwitterTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TWITTERTEST_H__80E07D87_B07D_4D9A_BBEA_D79F3F048F13__INCLUDED_)
