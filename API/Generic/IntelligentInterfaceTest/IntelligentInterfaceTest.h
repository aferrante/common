// IntelligentInterfaceTest.h : main header file for the INTELLIGENTINTERFACETEST application
//

#if !defined(AFX_INTELLIGENTINTERFACETEST_H__F74038B1_152E_40B7_ABA7_24DA9DE0DE10__INCLUDED_)
#define AFX_INTELLIGENTINTERFACETEST_H__F74038B1_152E_40B7_ABA7_24DA9DE0DE10__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CIntelligentInterfaceTestApp:
// See IntelligentInterfaceTest.cpp for the implementation of this class
//

class CIntelligentInterfaceTestApp : public CWinApp
{
public:
	CIntelligentInterfaceTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIntelligentInterfaceTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CIntelligentInterfaceTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTELLIGENTINTERFACETEST_H__F74038B1_152E_40B7_ABA7_24DA9DE0DE10__INCLUDED_)
