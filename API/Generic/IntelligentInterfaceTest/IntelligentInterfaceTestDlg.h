// IntelligentInterfaceTestDlg.h : header file
//

#if !defined(AFX_INTELLIGENTINTERFACETESTDLG_H__08A07F6C_3A46_4D49_B61F_41EA50061800__INCLUDED_)
#define AFX_INTELLIGENTINTERFACETESTDLG_H__08A07F6C_3A46_4D49_B61F_41EA50061800__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "../II.h"

/////////////////////////////////////////////////////////////////////////////
// CIntelligentInterfaceTestDlg dialog

class CIntelligentInterfaceTestDlg : public CDialog
{
// Construction
public:
	CIntelligentInterfaceTestDlg(CWnd* pParent = NULL);	// standard constructor

	CIICore m_ii;

// Dialog Data
	//{{AFX_DATA(CIntelligentInterfaceTestDlg)
	enum { IDD = IDD_INTELLIGENTINTERFACETEST_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIntelligentInterfaceTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CIntelligentInterfaceTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTELLIGENTINTERFACETESTDLG_H__08A07F6C_3A46_4D49_B61F_41EA50061800__INCLUDED_)
