// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.

// II.h

// Intelligent Interface

#ifndef II_CORE_INCLUDED
#define II_CORE_INCLUDED

#include "../../MSG/MessagingObject.h"
#include "../../TTY/Serial.h"
#include "../../LAN/NetUtil.h"

#define II_ERRORSTRING_LEN 512

#define II_CONN_EXISTING 0  // use existing connection, create new if none
#define II_CONN_SEPARATE	1  // close and make new each time


#define IICORE_ERROR -1
#define IICORE_SUCCESS 0
#define IICORE_CORRECTED 1


// types:

#define IICORE_LYRIC				0x0000
#define IICORE_CHANNELBOX		0x0001
#define IICORE_ICONSTATION	0x0002
#define IICORE_CRUST				0x0003


// commands
#define II_LYR_CMD_OPENSCENE		0xb0	  // Opening a Lyric Message as an alias using W command. 
#define II_LYR_CMD_LOADSCENE		0xb1	  // Loading a Lyric Message to buffer using V\5\3 command. 
#define II_LYR_CMD_PLAY					0xb2    // Take to air with Y\<D5><F3> command, which returns immediately. dont use V\6.
#define II_LYR_CMD_RELEASE			0xb3    // Resume play out with V\5\15
#define II_LYR_CMD_UNLOAD				0xb4    // load a special blank template to "clear" the existing frame - can use animation effect so it's not a cut. 
#define II_LYR_CMD_CLEAR				0xb5    // clear frame buffer with Y\<D5><CF><FE><Frame Buffer>\\<CR><LF>

#define II_CB_CMD_OPENALIAS			0xc0	  // Opening a Channel Box scene as an alias using Lyric W command. 
#define II_CB_CMD_OPENSCENE			0xc1	  // Opening a Channel Box scene. 
#define II_CB_CMD_LOADSCENE			0xc2    //Loading a Channel Box scene. 
#define II_CB_CMD_PLAY					0xc3    //Starting play out. 
#define II_CB_CMD_STOP					0xc4    //Stopping play out. 
#define II_CB_CMD_UPDATE				0xc5    //Updating the value of a field on the control panel. 
#define II_CB_CMD_TRIGGER 			0xc6    //Triggering a dynamic field (such as a button) on the control panel. 
#define II_CB_CMD_CLOSE					0xc7    //Closing a Channel Box scene. 

#define II_ICON_CMD_LOAD				0xd0	  // load a layout
#define II_ICON_CMD_LOADFIRE		0xd1	  // load a layout and fire a Salvo. T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
#define II_ICON_CMD_FIRE				0xd2	  // fire a Salvo. 
#define II_ICON_CMD_UPDATE			0xd3    // update your item source text at any time. I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
#define II_ICON_CMD_CLEAR				0xd4    // clear preview and clear output. T\14\Channel\\   - Clear Preview and Clear Output
#define II_ICON_CMD_POLL				0xd5    // poll the IconStation system to confirm IconStation is still running and responding to commands. *\\   - Poll IconStation (returns *<CRLF>)

#define II_PIE_CMD_LOAD					0xe0	  // load a project V\5\13
#define II_PIE_CMD_PLAY					0xe1	  // play V\6
#define II_PIE_CMD_UPDATE				0xe3    // V\5\13 ???
#define II_PIE_CMD_CLEAR				0xe4    // Y\  
#define II_PIE_CMD_FIRE					0xe5    // E  // fire off user defined events



class CIICore : public CMessagingObject
{
public:
  CIICore();   // standard constructor
  ~CIICore();   // standard destructor
  
	int m_nTxRxDwellMS;
	int m_nTxTxPauseMS;
	int m_nTxRetries;
	int m_nMode;

  bool m_bUseEthernet;
  bool m_bCommInit;

//private:  // allow outside manipulation of these items.
	CNetUtil  m_net;
	CSerial		m_serial;	

  int   m_nCOMport;
  DWORD m_dwBaudRate;
  
  int    m_nEthernetPort;
  SOCKET m_socket;
  char   m_szEthernetAddress[256];

	unsigned long m_ulType;

// if lyric, unload needs either to load a blank template or send a keyboard cmd
	int m_nBlankMessage;

	// these are the messages in play
	char** m_pszMessages;  
	int    m_nMessages;

public:
  // comms
  int SetupEthernetConnection(char* pszIP, int nPort=23);  //telnet for II
  int SetupSerialConnection(int nCOMport, DWORD nBaud);
  int OpenConnection(char* pszError);
  int CloseConnection();
  int GetComPort();
  int SetComPort(int nPort);
  int RelinquishCOMPort();
  int SetBaudRate(DWORD nBaud);
  DWORD GetBaudRate();

// main interface:
	int InitializeConnection();  // different from open connection, this sends (on the open conn) the initialization string if any.
	int PollDevice();  // status check
	int OpenScene( char* pszAlias);  // open a scene.  if alias NULL, just opens without aliasing
	int LoadScene( char* pszScene);  // load a scene.
	int PlayScene( char* pszScene);  // displays a scene.
	int ResumeScene( char* pszScene);  // plays after a hold a scene.
	int UpdateScene( char** ppchFields, char** ppchValues, int* nNumPairs );  // updates fields within a scene.
	int Trigger( char* pszTrigger, char* pszValue );  // triggers anims
	int CloseScene( char* pszScene );  // unload,



#define II_LYR_CMD_OPENSCENE		0xb0	  // Opening a Lyric Message as an alias using W command. 
#define II_LYR_CMD_LOADSCENE		0xb1	  // Loading a Lyric Message to buffer using V\5\3 command. 
#define II_LYR_CMD_PLAY					0xb2    // Take to air with Y\<D5><F3> command, which returns immediately. dont use V\6.
#define II_LYR_CMD_RELEASE			0xb3    // Resume play out with V\5\15
#define II_LYR_CMD_UNLOAD				0xb4    // load a special blank template to "clear" the existing frame - can use animation effect so it's not a cut. 
#define II_LYR_CMD_CLEAR				0xb5    // clear frame buffer with Y\<D5><CF><FE><Frame Buffer>\\<CR><LF>

#define II_CB_CMD_OPENALIAS			0xc0	  // Opening a Channel Box scene as an alias using Lyric W command. 
#define II_CB_CMD_OPENSCENE			0xc1	  // Opening a Channel Box scene. 
#define II_CB_CMD_LOADSCENE			0xc2    //Loading a Channel Box scene. 
#define II_CB_CMD_PLAY					0xc3    //Starting play out. 
#define II_CB_CMD_STOP					0xc4    //Stopping play out. 
#define II_CB_CMD_UPDATE				0xc5    //Updating the value of a field on the control panel. 
#define II_CB_CMD_TRIGGER 			0xc6    //Triggering a dynamic field (such as a button) on the control panel. 
#define II_CB_CMD_CLOSE					0xc7    //Closing a Channel Box scene. 

#define II_ICON_CMD_LOAD				0xd0	  // load a layout
#define II_ICON_CMD_LOADFIRE		0xd1	  // load a layout and fire a Salvo. T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
#define II_ICON_CMD_FIRE				0xd2	  // fire a Salvo. 
#define II_ICON_CMD_UPDATE			0xd3    // update your item source text at any time. I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
#define II_ICON_CMD_CLEAR				0xd4    // clear preview and clear output. T\14\Channel\\   - Clear Preview and Clear Output
#define II_ICON_CMD_POLL				0xd5    // poll the IconStation system to confirm IconStation is still running and responding to commands. *\\   - Poll IconStation (returns *<CRLF>)

#define II_PIE_CMD_LOAD					0xe0	  // load a project V\5\13
#define II_PIE_CMD_PLAY					0xe1	  // play V\6
#define II_PIE_CMD_UPDATE				0xe3    // V\5\13 ???
#define II_PIE_CMD_CLEAR				0xe4    // Y\  
#define II_PIE_CMD_FIRE					0xe5    // E  // fire off user defined events



private:
  // sending commands
  // int TriggerSqueezeback(int duration, int command, char* pszError=NULL);
  int SendCommand(char* pszError, char** ppszReturn, int nMode, char* pszCommand, ... );
  int ReceiveAck(char* pchBuffer, unsigned long* pulBufferLen);
//  int rem_send_char(unsigned char ch);
//  int remote_send(char* pszFormat, ...);
//  void do_crc( unsigned char ch, unsigned short* crcptr );
};


#endif //II_CORE_INCLUDED

#ifdef INSTEAD_OF_COMMENTING_OUT_THE_FOLLOWING

// Lyric II
C Change color of specified field.
E Define Macro to read message.
F Change font of specified field.
M Set disk drive and message directory.
Q Resend previous response.
U Update one field of a Tab Data message.
V Special Effects and Control.
W Write to Tab Data message.
X and R Requests External Update and responds to Update Request.
Y Assorted.
Embedded Commands that are embedded in R, U or W commands to change the Font, Color, or Background of an Intelligent Interface-enabled Template.


// Channel Box II
B\OP - Opening a Channel Box scene.
B\LO - Loading a Channel Box scene.
B\PL - Starting play out. 
B\ST - Stopping play out.
B\CL - Closing a Channel Box scene.
B\UP - Updating a field in the Channel Box control panel.
B\AN - Query


Querying the Addressable Control Panel Fields
A list of addressable control panel fields as well as their data type can be retrieved through the following command:

B\AN\<SceneId>\\<checksum><CR><LF>

Querying the Current Value and Acceptable Values of a Field
Once we've retrieved the acceptable field names through the AN command, we can utilize the following the command to obtain the current value for a particular field and, if needed, a restricted list of allowable values (particularly useful for combo boxes).

B\AN\<SceneId>\<FieldName>\\<checksum><CR><LF>


// IconStation II
IconStation supports four CII commands for greater output flexibility.
The first CII command allows you to load a layout and fire a Salvo.
The second CII command allows you to clear preview and clear output.
The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
The fourth CII command allows you to update your item source text at any time.

Channel Entry
The Channel entry equals A or B.
	A = program channel
	B = preview channel

T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
T\14\Channel\\   - Clear Preview and Clear Output
*\\   - Poll IconStation (returns *<CRLF>)
I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
Example Command:
I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

//stuff has this structure:
<LayoutTags>
	<LayoutName>InfoChannel</LayoutName>
	<Region> // region 1
		<Name>Crawl</Name> // the name of the region
		<Tag> // a "tag" within a region
			<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
			<Text>blarg</Text>  // text to put in the tag (field) in the region
		</Tag>
	</Region>
	<Region> // an additional region in the layout - one with multiple "tags"
		<Name>Title_area</Name>
		<Tag>
			<Name>Field1</Name>
			<Text>coming up next</Text>
		</Tag>
		<Tag>
			<Name>Field2</Name>
			<Text>more text</Text>
		</Tag>
	</Region>
</LayoutTags>



// Crust

The protocol is supported in the following limited way:

V\5\13
V\6
Y
E

======== V\5\13 ===========

Here are two examples of the V\5\13 command that loads the project and populates the fields:

          V\5\13\1\1\gnext\1\Coming Up Next: Legends\\
          V\5\13\1\2\gtime\1\10:00 PM\SUNNY\<$:project.folder>/images/sunny.tga\\

The first three args are required to identify the command: V\5\13
Set the 4th arg  to 1
The 5th arg is used by Crust to index the project for other II commands
The 6th arg is the name of the Crust project located in Crust's default project folder
Set the 7th arg to 1
The remaining arguments are used to populate the data fields in the referenced project


======== V\6 ============

Here are two examples of the V\6 command

           V\6\1\\
           V\6\2\\

The first two args are required to identify the command
The 3rd arg will play the indexed project given the following considerations:

     1) Crust relies on user defined events to activate behaviors
     2) The events are identified by simple strings
     3) Events can be posted into Crust in the following ways: Telnet, HTTP, and the "event pit" (app console widget.)
     4) By default, a project includes a "Play" event that, when posted, activates the project for display.

     So the V\5\13 command above isn't enough to display the project according to default behaviors. Invoking V\6 with the project index will activate the project for display

There are a number of additional conditions that can trigger events, but they need to be defined by the user for the given project.

use consecutive delimiters \\ to skip a field, use space \ \ to clear it.

optional checksum is not supported.


========= Y ==============

Here's an example of the Y command

       Y\2\\

This command will unload the project indexed by the second arg which is 2 in the above example


=========  E =============

Here's an example of the E command

    E\gtime.FadeDown\\

Crust uses the E command to fire user defined events such as:   gtime.FadeDown
----------------------------------------------------------------------------------------------------------------------------------


For the general case, you should be able to fire V\5\13 commands followed by V\6 commands.  
Keep in mind that a project can be set up to auto-play when loaded which removes the need to issue a V\6 command.

Response codes:
Here's a trace of something that went bad:
V\5\13\1\1\promo2\1\Fresh\<$:project.folder>/movies/Fresh.avi\\
*0xa005f101


Here's a trace of something that worked:
E\Scene Stop\\
*0x0




.///////////////////

standardActions(0) = "insert into [curMainDB].dbo.Graphics_Actions (graphicid, graphic_action_typeid, graphic_typeid, name, description, statusid) values ([sys_graphicidVALUE], 0, NULL, '', 'Load', 1)"  
  
standardActions(1) = "insert into [curMainDB].dbo.Graphics_Actions (graphicid, graphic_action_typeid, graphic_typeid, name, description, statusid) values ([sys_graphicidVALUE], 0, NULL, '', 'Play', 1)"  
  
standardActions(2) = "insert into [curMainDB].dbo.Graphics_Actions (graphicid, graphic_action_typeid, graphic_typeid, name, description, statusid) values ([sys_graphicidVALUE], 0, NULL, '', 'Release', 1)"   

standardActions(3) = "insert into [curMainDB].dbo.Graphics_Actions (graphicid, graphic_action_typeid, graphic_typeid, name, description, statusid) values ([sys_graphicidVALUE], 0, NULL, '', 'Unload', 1)"  
  
standardActions(4) = "insert into [curMainDB].dbo.Graphics_Actions (graphicid, graphic_action_typeid, graphic_typeid, name, description, statusid) values ([sys_graphicidVALUE], 0, NULL, '', 'Clear', 1)"   

#endif //INSTEAD_OF_COMMENTING_OUT_THE_FOLLOWING


