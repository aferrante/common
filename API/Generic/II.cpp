// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.


#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <direct.h>  // directory routines
#include <sys/timeb.h>
#include <time.h>

#include "II.h"

CIICore::CIICore()
{
  m_dwBaudRate = CBR_19200;
  m_nCOMport = 1;
  m_bUseEthernet = true;
	m_bCommInit = false;
  m_nEthernetPort = 23;
  strcpy(m_szEthernetAddress,"");

	m_nTxRxDwellMS=0;
	m_nTxTxPauseMS=50;
	m_nTxRetries=4;
	m_nMode=II_CONN_EXISTING;

	m_ulType = IICORE_LYRIC;
	
	m_nBlankMessage=-1;

	m_net.Initialize();
}

CIICore::~CIICore()
{
}


/* for crc, remove later
const unsigned int lstab[] =
{
  0x0000, 0xc0c1, 0xc181, 0x0140,
    0xc301, 0x03c0, 0x0280, 0xc241,
    0xc601, 0x06c0, 0x0780, 0xc741,
    0x0500, 0xc5c1, 0xc481, 0x0440
};

const unsigned int mstab[] =
{
  0x0000, 0xcc01, 0xd801, 0x1400,
    0xf001, 0x3c00, 0x2800, 0xe401,
    0xa001, 0x6c00, 0x7800, 0xb401,
    0x5000, 0x9c01, 0x8801, 0x4400
};
*/



/////////////////////////////////////////////////////////////////////////////
// Public Methods Implementation



/////////////////////////////////////////////////////////////////////////////
// Connection settings

int CIICore::SetupEthernetConnection(char* pszIP, int nPort)
{
	if((pszIP)&&(strlen(pszIP)))
	{
		if(m_serial.m_bConnected) m_serial.CloseConnection();
		m_nEthernetPort = nPort;
		strcpy(m_szEthernetAddress, pszIP);
	  m_bUseEthernet = true;
		m_bCommInit = true;
		return IICORE_SUCCESS;
	}
	m_bCommInit = false;
  return IICORE_ERROR;
}

int CIICore::SetupSerialConnection(int nCOMport, DWORD nBaud)
{
	if(SetBaudRate(nBaud)>=IICORE_SUCCESS)
	{
		if(m_socket) { m_net.CloseConnection(m_socket); m_socket=NULL; }
		m_bUseEthernet = false;
		m_bCommInit = true;
		return SetComPort(nCOMport);
	}
	m_bCommInit = false;
  return IICORE_ERROR;
}

int CIICore::SetBaudRate(DWORD nBaud)
{
  // verify baud rate
  if(
			(nBaud == CBR_9600)
		||(nBaud == CBR_19200)
		||(nBaud == CBR_38400)
		||(nBaud == CBR_57600)
		||(nBaud == CBR_115200)
		)
  {
		m_dwBaudRate = nBaud;
		return IICORE_SUCCESS;
  }
  return IICORE_ERROR;
}

DWORD CIICore::GetBaudRate()
{
  return m_dwBaudRate;
}

int CIICore::SetComPort(int nPort)
{
	int nRV = IICORE_SUCCESS;
  if (nPort < 1) { nPort = 1; nRV = IICORE_CORRECTED; }
  if (nPort > 4) {nPort = 4; nRV = IICORE_CORRECTED; }
  m_nCOMport = nPort;
	return nRV;
}

int CIICore::GetComPort()
{
  return m_nCOMport;
}

int CIICore::RelinquishCOMPort()
{
  m_serial.CloseConnection();
	return IICORE_SUCCESS;
}


int CIICore::OpenConnection(char* pszError)
{
  if(pszError) strcpy(pszError, "");
  if (m_bUseEthernet)
  {
    if(m_net.OpenConnection(m_szEthernetAddress, m_nEthernetPort, &m_socket, 5000, 5000, pszError)<NET_SUCCESS)
		{
			return IICORE_ERROR;
		}
  }
  else
  {
    if(m_serial.OpenConnection(m_nCOMport, m_dwBaudRate, 8, ONESTOPBIT, NOPARITY)<SERIAL_SUCCESS)
    {
      if(pszError) _snprintf(pszError, II_ERRORSTRING_LEN-1, "Error connecting to Imagestore on COM %d at 5d.", m_nCOMport, m_dwBaudRate);
      return IICORE_ERROR;
    }
  }
	return IICORE_SUCCESS;
}

int CIICore::CloseConnection()
{
  if(m_bUseEthernet)
  {
    m_net.CloseConnection(m_socket);  // never fails
		m_socket=NULL; 
		return IICORE_SUCCESS;
  }
  else
  {
    return RelinquishCOMPort();
  }
}

/////////////////////////////////////////////////////////////////////////////
//
//  Generic command retry  
//

int CIICore::SendCommand(char* pszError, char** ppszReturn, int nMode, char* pszCommand, ... )
{
  int nSucceeded=IICORE_ERROR; 
  int nCount;
  
	if(nMode == II_CONN_EXISTING)
	{
		if(m_bUseEthernet)
		{
			if(m_socket==NULL)
			{
				if (!OpenConnection(pszError)) return IICORE_ERROR;
			}
		}
		else
		{
			if(!m_serial.m_bConnected)
			{
				if (!OpenConnection(pszError)) return IICORE_ERROR;
			}
		}
	}
	else
	if(nMode == II_CONN_SEPARATE)
	{
		CloseConnection();
		if (!OpenConnection(pszError)) return IICORE_ERROR;
	}
	else return IICORE_ERROR;

  nCount=0;

  char pszMessage[8192],pszResponse[8192], crlf[6];
	unsigned long ulBufLen=0;
  
	va_list marker;
	// create the formatted output string
	va_start(marker, pszCommand); // Initialize variable arguments.
	_vsnprintf(pszMessage, 8191, pszCommand, (va_list) marker);
	va_end( marker );             // Reset variable arguments.

	sprintf(crlf, "%c%c", 13,10);  

	strcat(pszMessage, crlf); // all II commands are terminated with CRLF.

  while ((nSucceeded<IICORE_SUCCESS) && (nCount<m_nTxRetries))
  {
		if (m_bUseEthernet)
		{
			return send(m_socket, pszMessage, strlen(pszMessage), 0);
		}
		else
		{
			return m_serial.SendData(pszMessage, strlen(pszMessage));
		}

    // Wait a moment...
    Sleep(m_nTxRxDwellMS);

    nSucceeded = ReceiveAck(pszResponse, &ulBufLen);

    if (nSucceeded<IICORE_SUCCESS)
    {
			Sleep(m_nTxTxPauseMS); //wait again, unit might just be busy, 
			// dont pepper it with rapid commands
    }
		else
		{
			// copy response buffer
			if(ppszReturn)
			{
				char* pch = (char*)malloc(ulBufLen+1);
				if(pch)
				{
					strcpy(pch, pszResponse);
					*(ppszReturn) = pch;
				}
				else 
				{
					*(ppszReturn) = NULL;
				}
			}
		}
    nCount++;
  }

  if ((nSucceeded<IICORE_SUCCESS)&&(pszError)) 
	{
		_snprintf(pszError, II_ERRORSTRING_LEN-1, "SendCommand failed after %d retries.\nCommand: %s\nReturn code: %s.", nCount, pszCommand, ((ulBufLen>0)?pszResponse:""));
  }

	if(nMode == II_CONN_SEPARATE)
	{
		CloseConnection();
	}

  return nSucceeded;
}


// receive response
int  CIICore::ReceiveAck(char* pchBuffer, unsigned long* pulBufferLen)
{ 
	if((pchBuffer==NULL)||(pulBufferLen==NULL)) return IICORE_ERROR;
  if (m_bUseEthernet)
  {
		if(m_net.GetLine((unsigned char**)(&pchBuffer), pulBufferLen, m_socket)<NET_SUCCESS) *pulBufferLen = 0;
  }
  else
  {
		if(m_serial.ReceiveData(&pchBuffer, pulBufferLen)<SERIAL_SUCCESS) *pulBufferLen = 0;
  }

  if (*pulBufferLen == 0)  return IICORE_ERROR;

  return IICORE_SUCCESS;
}

