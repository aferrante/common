// Serial.cpp: implementation of the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // need for overlapped structs, etc
#include "Serial.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSerial::CSerial()
{
	m_hDevice = INVALID_HANDLE_VALUE;
	m_bConnected = false;
	m_ulBaud     = CBR_9600;
	m_chByteSize = 8;
	m_chStopBits = ONESTOPBIT;
	m_chParity   = NOPARITY;
	m_chComPort  = 1;
	m_chFlowCtrl = SERIAL_XONXOFF;

	m_bEnableMessaging = false; // turn off module messaging by default
}

CSerial::CSerial(char chComPort, unsigned long ulBaud, char chByteSize, char chStopBits, char chParity, char chFlowCtrl)
{
	m_hDevice = INVALID_HANDLE_VALUE;
	m_bConnected = false;
	m_ulBaud     = ulBaud;
	m_chByteSize = chByteSize;
	m_chStopBits = chStopBits;
	m_chParity   = chParity;
	m_chComPort  = chComPort;
	m_chFlowCtrl = chFlowCtrl;

	m_bEnableMessaging = false; // turn off module messaging by default
}

CSerial::~CSerial()
{
	if(m_bConnected) CloseConnection();
}

int CSerial::SendData(char* pchData, unsigned long ulDataLength, unsigned long ulFlags)
{
//char foo[1023]; sprintf(foo, "pchData=%d, m_bConnected=%d, m_hDevice=%d",pchData,m_bConnected,(m_hDevice)); AfxMessageBox(foo);
	if((pchData==NULL)||(!m_bConnected)||(m_hDevice==INVALID_HANDLE_VALUE)) return SERIAL_ERROR;

	bool bSuccess = false;
	unsigned long ulBytesWritten=0;
//AfxMessageBox("WriteFile");
	if(
			WriteFile(
								m_hDevice, 
								pchData, 
								ulDataLength,
								&ulBytesWritten, 
								&m_ovlpTX
								)
		) 
		bSuccess = true;

	if (!bSuccess)
	{
		DWORD dwLastError = GetLastError();

		COMSTAT comstat;
		switch(dwLastError)
		{
		case ERROR_IO_PENDING:
			{
				unsigned long ulBytesSent=0;
				while(
							!GetOverlappedResult(
																		m_hDevice,
																		&m_ovlpTX, 
																		&ulBytesSent,
																		TRUE
																	)
							)
				{
					dwLastError = GetLastError();
					if(dwLastError == ERROR_IO_INCOMPLETE)
					{
						// not finished yet
						ulBytesWritten += ulBytesSent;
						continue;
					}
					else
					{
						unsigned long ulError=0;
						ClearCommError(m_hDevice, &ulError, &comstat);
						if (ulError>0)
						{
							// error, report
						}
						break;
					}
				}
    
				ulBytesWritten += ulBytesSent;
    
				if(ulBytesWritten != ulDataLength)
				{
					// error, report
				}
//char foo[1023]; 
//sprintf(foo, "Error in WriteFile: %d",dwLastError);
//sprintf(foo, "wrote %d bytes",ulBytesWritten);
//AfxMessageBox(foo);
				return ulBytesWritten;  // return number of bytes written!
			} break;
		case ERROR_BROKEN_PIPE:
			{
			} // break;  //let this do the default case
		default:
			{
// sprintf(foo, "error %d",dwLastError); AfxMessageBox(foo);
				unsigned long ulError=0;
				ClearCommError(m_hDevice, &ulError, &comstat);
				if (ulError>0)
				{
// sprintf(foo, "ClearCommError %d",ulError); AfxMessageBox(foo);

					// error, report
				}
				return SERIAL_ERROR;
			} break;
		}
	}
//char foo[1023];
// sprintf(foo, "wrote %d bytes not pending",ulBytesWritten); AfxMessageBox(foo);
	return ulBytesWritten;
}

int CSerial::ReceiveData(char** ppchData, unsigned long* pulDataLength, unsigned long ulFlags)
{
	if((ppchData==NULL)||(pulDataLength==NULL)||(!m_bConnected)||(m_hDevice==INVALID_HANDLE_VALUE)) return SERIAL_ERROR;

//	AfxMessageBox("receiving");

  COMSTAT	comstat;
	unsigned long ulError=0;

	ClearCommError(m_hDevice, &ulError, &comstat);
	if (ulError>0)
	{
		// error, report
	}

	unsigned long ulLength = comstat.cbInQue;
	if(ulLength==0) ulLength = 4096;

	if(ulFlags&SERIAL_USEBUFFER)
	{
//	AfxMessageBox("use buffer");
		if(*pulDataLength>0)
		{
			if(comstat.cbInQue > 0)
				ulLength = min(*pulDataLength, comstat.cbInQue);
			else
				ulLength = *pulDataLength;
		}
		else return SERIAL_ERROR;  // cant have a zero length buffer to receive into
	}
	else
	if (ulLength > 0)
	{
//	AfxMessageBox("creating buffer");
		char* pch = (char*) malloc(ulLength+1);
		if(pch == NULL)  return SERIAL_ERROR;  // need a buffer to receive into
		*ppchData = pch;
	}
	else
	{
//	AfxMessageBox("no length");
		*pulDataLength = 0;
		return SERIAL_ERROR;
	}

	unsigned long ulBytesReceived=0;
  if (ulLength > 0)
  {
		bool	bSuccess = false;

		if(
				ReadFile(
									m_hDevice, 
									*ppchData,
									ulLength,
									&ulBytesReceived, 
									&m_ovlpRX
								)
			)
			bSuccess = true;

    if (!bSuccess)
    {
			DWORD dwLastError = GetLastError();
			switch(dwLastError)
      {
			case ERROR_IO_PENDING:
				{
					unsigned long ulBytesRead=0;
					while(
						!GetOverlappedResult(
																	m_hDevice,
																	&m_ovlpRX, 
																	&ulBytesRead, 
																	TRUE
																)
								)
					{
						dwLastError = GetLastError();
						if(dwLastError == ERROR_IO_INCOMPLETE)
						{
							// not finished yet
							ulBytesReceived += ulBytesRead;
							continue;
						}
						else
						{

							unsigned long ulError=0;
							ClearCommError(m_hDevice, &ulError, &comstat);
							if (ulError>0)
							{
								// error, report
							}
							break;
						}
					}
				
					ulBytesReceived += ulBytesRead;
// 			char foo[64]; sprintf(foo, "ulBytesReceived was %d", ulBytesReceived);	AfxMessageBox(foo);
    
					if(ulBytesReceived != ulLength)
					{
						// error, report
					}
					if(!(ulFlags&SERIAL_USEBUFFER))
						*((*ppchData)+ulBytesReceived) = 0; //null term;
					*pulDataLength = ulBytesReceived;
					return ulBytesReceived;  // return number of bytes received!
				} break;
			default:
				{
					unsigned long ulError=0;
					ClearCommError(m_hDevice, &ulError, &comstat);
					if (ulError>0)
					{
						// error, report
					}

					if(!(ulFlags&SERIAL_USEBUFFER))
					{
						if(*ppchData) free (*ppchData);
						*ppchData = NULL;
					}
					*pulDataLength = 0;

					return SERIAL_ERROR;
				} break;
			}
    }
  }
	if(!(ulFlags&SERIAL_USEBUFFER))
		*((*ppchData)+ulBytesReceived) = 0; //null term;
	*pulDataLength = ulBytesReceived;
	return ulBytesReceived;  // return number of bytes received!
}

int CSerial::CloseConnection()
{
	if(m_hDevice!=INVALID_HANDLE_VALUE)
	{
		SetCommMask(m_hDevice, 0);
		EscapeCommFunction(m_hDevice, CLRDTR);
		PurgeComm(m_hDevice, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);
		CloseHandle(m_hDevice);
	}

	m_hDevice = INVALID_HANDLE_VALUE;
	m_bConnected = false;
	return SERIAL_SUCCESS;
}

int CSerial::OpenConnection(char chComPort, unsigned long ulBaud, char chByteSize, char chStopBits, char chParity, char chFlowCtrl, COMMTIMEOUTS* pcto)
{
	if(m_bConnected) CloseConnection();  // open new.

  BOOL	bSuccess = false;

	// process overrides:
	if(chComPort!=SERIAL_INVALID)		m_chComPort  = chComPort;
	if(ulBaud!=NULL)								m_ulBaud		 = ulBaud;
	if(chByteSize!=SERIAL_INVALID)	m_chByteSize = chByteSize;
	if(chStopBits!=SERIAL_INVALID)	m_chStopBits = chStopBits;
	if(chParity!=SERIAL_INVALID)		m_chParity   = chParity;
	if(chFlowCtrl!=SERIAL_INVALID)	m_chFlowCtrl = chFlowCtrl;
  
  char pszCom[32];
	sprintf(pszCom, "COM%d",	m_chComPort);
   
  // open COM device
  m_hDevice = CreateFile(
													pszCom, 
													GENERIC_READ|GENERIC_WRITE,
													0,                    // exclusive access
													NULL,                 // no security attrs
													OPEN_EXISTING,
													FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED, // overlapped I/O
													NULL
												);

	if(m_hDevice==INVALID_HANDLE_VALUE)
  {
					// error, report
		return SERIAL_ERROR;
  }
  else
  {

//char foo[1023]; sprintf(foo, "OpenConnection handle %d",m_hDevice); AfxMessageBox(foo);
		SetCommMask(m_hDevice, EV_RXCHAR);
		SetupComm(m_hDevice, SERIAL_BLOCKSIZE, SERIAL_BLOCKSIZE);
		PurgeComm(m_hDevice, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);
		
		if(pcto)
		{
			SetCommTimeouts(m_hDevice, pcto);
		}
		else
		{
			COMMTIMEOUTS	ctoTimeouts;
 
			// A value of MAXDWORD, combined with zero values for both the 
			// ReadTotalTimeoutConstant and ReadTotalTimeoutMultiplier members, 
			// specifies that the read operation is to return immediately with 
			// the characters that have already been received, even if no characters have been received. 

			ctoTimeouts.ReadIntervalTimeout = 0xFFFFFFFF;
			ctoTimeouts.ReadTotalTimeoutMultiplier = 0;
			ctoTimeouts.ReadTotalTimeoutConstant = 0;
//			ctoTimeouts.ReadIntervalTimeout=10000;
			ctoTimeouts.WriteTotalTimeoutMultiplier = 20000/m_ulBaud;
			ctoTimeouts.WriteTotalTimeoutConstant = 1000;
			SetCommTimeouts(m_hDevice, &ctoTimeouts);
		}
  }
  
  DCB	dcb;
  dcb.DCBlength = sizeof(DCB);
  
  if (!GetCommState(m_hDevice, &dcb))
  {
					// error, report
		return SERIAL_ERROR;
  }
  
  dcb.BaudRate	= m_ulBaud;
  dcb.ByteSize	= m_chByteSize;
  dcb.Parity		= m_chParity;
  dcb.StopBits	= m_chStopBits;
  
  // hardware flow control
  dcb.fOutxDsrFlow = ((m_chFlowCtrl&SERIAL_DTRDSR) != 0);
  if (dcb.fOutxDsrFlow) 
		dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;
  else
		dcb.fDtrControl = DTR_CONTROL_ENABLE;
  
  dcb.fOutxCtsFlow = ((m_chFlowCtrl&SERIAL_RTSCTS) != 0);
  if (dcb.fOutxCtsFlow)
		dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
  else
		dcb.fRtsControl = RTS_CONTROL_ENABLE;
  
  // software flow control
  dcb.fInX = ((m_chFlowCtrl&SERIAL_XONXOFF) != 0);
	dcb.fOutX = dcb.fInX;
  dcb.XonChar		= 0x11;
  dcb.XoffChar	= 0x13;
  dcb.XonLim		= 100;
  dcb.XoffLim		= 100;
  dcb.fBinary = TRUE;
  dcb.fParity = TRUE;
  
  if (!SetCommState(m_hDevice, &dcb))
  {
					// error, report
		return SERIAL_ERROR;
  }

	m_bConnected = true;
	return SERIAL_SUCCESS;
}

