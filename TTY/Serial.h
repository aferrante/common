// Serial.h: interface for the CSerial class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIAL_H__E51C6D7B_BAA4_413E_BB2D_0E5A9C7B4A0F__INCLUDED_)
#define AFX_SERIAL_H__E51C6D7B_BAA4_413E_BB2D_0E5A9C7B4A0F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../MSG/MessagingObject.h"

#define SERIAL_DTRDSR       0x01
#define SERIAL_RTSCTS       0x02
#define SERIAL_XONXOFF      0x04
#define SERIAL_INVALID      0x7f

#define SERIAL_BLOCKSIZE    4096
#define SERIAL_USEBUFFER    0x000001

#define SERIAL_SUCCESS			0
#define SERIAL_ERROR		    -1  // generic


class CSerial : public CMessagingObject 
{
public:
	CSerial();
	CSerial(char chComPort, unsigned long ulBaud, char chByteSize, char chStopBits, char chParity, char chFlowCtrl=SERIAL_XONXOFF);
	virtual ~CSerial();

  int SendData(char* pchData, unsigned long ulDataLength, unsigned long ulFlags=0);
  int ReceiveData(char** ppchData, unsigned long* pulDataLength, unsigned long ulFlags=0);
  int CloseConnection( );
  int OpenConnection(char chComPort=SERIAL_INVALID, unsigned long ulBaud=NULL, char chByteSize=SERIAL_INVALID, char chStopBits=SERIAL_INVALID, char chParity=SERIAL_INVALID, char chFlowCtrl=SERIAL_XONXOFF, COMMTIMEOUTS* pcto=NULL);

public:

	unsigned long m_ulBaud;
	char m_chByteSize;
	char m_chStopBits;
	char m_chParity;
	char m_chComPort;
	char m_chFlowCtrl;

	HANDLE			m_hDevice;
	bool				m_bConnected;
	OVERLAPPED	m_ovlpRX;
	OVERLAPPED	m_ovlpTX;

};

#endif // !defined(AFX_SERIAL_H__E51C6D7B_BAA4_413E_BB2D_0E5A9C7B4A0F__INCLUDED_)
