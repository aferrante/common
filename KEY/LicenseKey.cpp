// LicenseKey.cpp: implementation of the CLicenseKey class.
//
//////////////////////////////////////////////////////////////////////
#include <stdafx.h>
#include <snmp.h>
#include "LicenseKey.h"
#include "..\TXT\BufferUtil.h"
#include <time.h>


SNMPAPI
SNMP_FUNC_TYPE
SnmpUtilOidCpy(
    OUT AsnObjectIdentifier *DstObjId,
    IN  AsnObjectIdentifier *SrcObjId
    )
{
  DstObjId->ids = (UINT *)GlobalAlloc(GMEM_ZEROINIT,SrcObjId->idLength * 
          sizeof(UINT));
  if(!DstObjId->ids){
    SetLastError(1);
    return 0;
  }

  memcpy(DstObjId->ids,SrcObjId->ids,SrcObjId->idLength*sizeof(UINT));
  DstObjId->idLength = SrcObjId->idLength;

  return 1;
}


VOID
SNMP_FUNC_TYPE
SnmpUtilOidFree(
    IN OUT AsnObjectIdentifier *ObjId
    )
{
  GlobalFree(ObjId->ids);
  ObjId->ids = 0;
  ObjId->idLength = 0;
}

SNMPAPI
SNMP_FUNC_TYPE
SnmpUtilOidNCmp(
    IN AsnObjectIdentifier *ObjIdA,
    IN AsnObjectIdentifier *ObjIdB,
    IN UINT                 Len
    )
{
  UINT CmpLen;
  UINT i;
  int  res;

  CmpLen = Len;
  if(ObjIdA->idLength < CmpLen)
    CmpLen = ObjIdA->idLength;
  if(ObjIdB->idLength < CmpLen)
    CmpLen = ObjIdB->idLength;

  for(i=0;i<CmpLen;i++){
    res = ObjIdA->ids[i] - ObjIdB->ids[i];
    if(res!=0)
      return res;
  }
  return 0;
}

VOID
SNMP_FUNC_TYPE
SnmpUtilVarBindFree(
    IN OUT RFC1157VarBind *VarBind
    )
{
  BYTE asnType;
  // free object name
  SnmpUtilOidFree(&VarBind->name);

  asnType = VarBind->value.asnType;

  if(asnType==ASN_OBJECTIDENTIFIER){
    SnmpUtilOidFree(&VarBind->value.asnValue.object);
  }
  else if(
        (asnType==ASN_OCTETSTRING) ||
        (asnType==ASN_RFC1155_IPADDRESS) ||
        (asnType==ASN_RFC1155_OPAQUE) ||
        (asnType==ASN_SEQUENCE)){
    if(VarBind->value.asnValue.string.dynamic){
      GlobalFree(VarBind->value.asnValue.string.stream);
    }
  }

  VarBind->value.asnType = ASN_NULL;

}


typedef BOOL(WINAPI * pSnmpExtensionInit) (
        IN DWORD dwTimeZeroReference,
        OUT HANDLE * hPollForTrapEvent,
        OUT AsnObjectIdentifier * supportedView);

typedef BOOL(WINAPI * pSnmpExtensionTrap) (
        OUT AsnObjectIdentifier * enterprise,
        OUT AsnInteger * genericTrap,
        OUT AsnInteger * specificTrap,
        OUT AsnTimeticks * timeStamp,
        OUT RFC1157VarBindList * variableBindings);

typedef BOOL(WINAPI * pSnmpExtensionQuery) (
        IN BYTE requestType,
        IN OUT RFC1157VarBindList * variableBindings,
        OUT AsnInteger * errorStatus,
        OUT AsnInteger * errorIndex);

typedef BOOL(WINAPI * pSnmpExtensionInitEx) (
        OUT AsnObjectIdentifier * supportedView);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLicenseKey::CLicenseKey()
{
	m_pszLicenseString = NULL;

	m_bValid = false;
	m_bMachineSpecific = false; // checks against MAC address
	m_bValidMAC = false;
	m_bExpires = false;   // checks against clock
	m_bExpired = false;   // is past the date.
	m_bExpireForgiveness = false;   // forgive past the expiry date.

	m_ucMAC[0] = 0;      // MAC address
	m_ucMAC[1] = 0;      // MAC address
	m_ucMAC[2] = 0;      // MAC address
	m_ucMAC[3] = 0;      // MAC address
	m_ucMAC[4] = 0;      // MAC address
	m_ucMAC[5] = 0;      // MAC address
	m_ulExpiryDate = 0;  // unixtime
	m_ulExpiryForgiveness=0; //seconds

	m_ppszParams=NULL;
	m_ppszValues=NULL;
	m_ulNumParams=0;
}

CLicenseKey::~CLicenseKey()
{
	if(m_pszLicenseString) free(m_pszLicenseString);
	if(m_ppszParams!=NULL)
	{
		unsigned long i=0;
		while(i<m_ulNumParams)
		{
			if(m_ppszParams[i]) free(m_ppszParams[i]);
			i++;
		}
		delete [] m_ppszParams;
	}
	if(m_ppszValues!=NULL)
	{
		unsigned long i=0;
		while(i<m_ulNumParams)
		{
			if(m_ppszValues[i]) free(m_ppszValues[i]);
			i++;
		}
		delete [] m_ppszValues;
	}
}

void CLicenseKey::SetParams(char* pszParams)
{
	if(m_ppszParams!=NULL)
	{
		unsigned long i=0;
		while(i<m_ulNumParams)
		{
			if(m_ppszParams[i]) free(m_ppszParams[i]);
			i++;
		}
		delete [] m_ppszParams;
	}
	if(m_ppszValues!=NULL)
	{
		unsigned long i=0;
		while(i<m_ulNumParams)
		{
			if(m_ppszValues[i]) free(m_ppszValues[i]);
			i++;
		}
		delete [] m_ppszValues;
	}

	m_ppszParams=NULL;
	m_ppszValues=NULL;
	
	if((pszParams)&&(strlen(pszParams)>0))
	{
// an encoded path is passed in in the ppszPath pointer, that buffer is destroyed, on success only.
// The value is then reset to point to a new buffer with the decoded path.

		char* pszPath = (char*)malloc(strlen(pszParams)+strlen("license?+1"));
		if(pszPath)
		{
			char* pszPathOrig = pszPath;
			sprintf(pszPath, "license?%s", pszParams);
			if(URLDecode(&pszPath, &m_ppszParams, &m_ppszValues, &m_ulNumParams)>=0)
			{
				free(pszPath);

/*
				unsigned long i=0;
				while(i<m_ulNumParams)
				{
					AfxMessageBox(m_ppszParams[i]);AfxMessageBox(m_ppszValues[i]);
					i++;
				}
*/
			}
			free(pszPathOrig);
		}
	}
}

void CLicenseKey::AddParam(char* pszParam, char* pszValue)
{
	if((pszParam)&&(strlen(pszParam))&&(pszValue)&&(strlen(pszValue)))
	{
		char* pchP = (char*)malloc(strlen(pszParam)+1);
		if(pchP)
		{
			char* pchV = (char*)malloc(strlen(pszValue)+1);
			if(pchV)
			{
				char** ppP = new char*[m_ulNumParams+1];
				if(ppP)
				{
					char** ppV = new char*[m_ulNumParams+1];
					if(ppV)
					{
						unsigned long i=0;
						while(i<m_ulNumParams)
						{
							if((m_ppszParams)&&(m_ppszParams[i]))
							{
								ppP[i] = m_ppszParams[i];
							}
							if((m_ppszValues)&&(m_ppszValues[i]))
							{
								ppV[i] = m_ppszValues[i];
							}
							i++;
						}
						if(m_ppszParams) delete [] m_ppszParams;
						if(m_ppszValues) delete [] m_ppszValues;
						ppP[i] = pchP;
						ppV[i] = pchV;
						m_ppszParams = ppP;
						m_ppszValues = ppV;
						m_ulNumParams=i;
					}
				}
			}
			else free(pchP);
		}
	}
}

void CLicenseKey::DeleteParam(char* pszParam)
{
}



void CLicenseKey::GenerateKey()  // looks at params and creates m_pszLicenseString
{
	// have to generate a valid key, so 
	m_bValid = true;
	m_bValidMAC = true;

	srand( clock() );

	char pchString[8192];  // thats a long enough key!
	memset( pchString, 0, 8192);

	pchString[0] = m_bMachineSpecific?m_ucMAC[3]:(char)(rand()%255);
	pchString[1] = 0x00;
	pchString[1] |= m_bMachineSpecific?0x20:0x00;
	pchString[1] |= m_bExpireForgiveness?0x01:0x00;
	pchString[1] |= m_bExpires?0x08:0x04;
	pchString[2] = m_bMachineSpecific?m_ucMAC[5]:(char)(rand()%255);
	pchString[3] = 'V';
	pchString[4] = m_bExpires?(char)(m_ulExpiryDate&0x000000ff):(char)(rand()%255);
	pchString[5] = m_bMachineSpecific?m_ucMAC[0]:(char)(rand()%255);
	pchString[6] = 'D';
	pchString[7] = (char)m_ulNumParams;  // obviously, max num params is 255
	pchString[8] = m_bExpires?(char)((m_ulExpiryDate&0x0000ff00)>>8):(char)(rand()%255);
	pchString[9] = 'S';
	pchString[10] = m_bMachineSpecific?m_ucMAC[4]:(char)(rand()%255);
	pchString[11] = m_bExpireForgiveness?(char)((m_ulExpiryForgiveness&0x00ff0000)>>16):(char)(rand()%255);
	pchString[12] = m_bExpires?(char)((m_ulExpiryDate&0xff000000)>>24):(char)(rand()%255);
	pchString[13] = m_bMachineSpecific?m_ucMAC[2]:(char)(rand()%255);
	pchString[14] = m_bExpireForgiveness?(char)((m_ulExpiryForgiveness&0x0000ff00)>>8):(char)(rand()%255);
	pchString[15] = m_bExpireForgiveness?(char)((m_ulExpiryForgiveness&0xff000000)>>24):(char)(rand()%255);
	pchString[16] = m_bMachineSpecific?m_ucMAC[1]:(char)(rand()%255);
	pchString[17] = m_bExpires?(char)((m_ulExpiryDate&0x00ff0000)>>16):(char)(rand()%255);
	pchString[18] = m_bExpireForgiveness?(char)(m_ulExpiryForgiveness&0x000000ff):(char)(rand()%255);

	m_bExpired = false;   // is past the date.

	int i=19;
	unsigned long n=0;
	if((m_ppszParams)&&(m_ppszValues)&&(m_ulNumParams>0))
	{	
		while(n<m_ulNumParams)
		{
			if((m_ppszParams[n])&&(m_ppszValues[n]))
			{
//				AfxMessageBox(m_ppszParams[n]);AfxMessageBox(m_ppszValues[n]);
				int j=0;
				char* pch = m_ppszParams[n];
				while((i<8192)&&(pch[j]!=0))
				{
					if(pch[j]=='+')
					{
						pchString[i] = 0;
					}
					else
					if(pch[j]=='Q')
					{
						pchString[i] = 13;
					}
					else
						pchString[i] = pch[j];
					j++;
					i++;
				}
				pchString[i] = '+'; i++;
				j=0;
				pch = m_ppszValues[n];
				while((i<8192)&&(pch[j]!=0))
				{
					if(pch[j]=='+')
					{
						pchString[i] = 0;
					}
					else
					if(pch[j]=='Q')
					{
						pchString[i] = 13;
					}
					else
						pchString[i] = pch[j];
					j++;
					i++;
				}
				pchString[i] = 'Q';	i++;

				n++;
			}
			else	n++;
		}
	}

//	AfxMessageBox(pchString);
	unsigned long ulBufLen = i;
	unsigned long ulEncodedBufLen = ulBufLen;
//CString foo; foo.Format("original %d",ulBufLen);		AfxMessageBox(foo);

  CBufferUtil bu;
	char* pchEncoded = pchString;
	bu.Base64Encode(&pchEncoded, &ulEncodedBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);
//	AfxMessageBox(pchEncoded);
//foo.Format("encoded %d",ulEncodedBufLen);		AfxMessageBox(foo);

	int nPads = 0;
	if(pchEncoded[ulEncodedBufLen-1] == LICENSE_B64PADCH) { nPads++; pchEncoded[ulEncodedBufLen-1] = 0; ulEncodedBufLen--; }
	if(pchEncoded[ulEncodedBufLen-1] == LICENSE_B64PADCH) { nPads++; pchEncoded[ulEncodedBufLen-1] = 0; ulEncodedBufLen--; }

	pchString[0] = 'K';

	switch(nPads)
	{
	default:
	case 0: pchString[1] = 'K'; break;
	case 1: pchString[1] = 'V'; break;
	case 2: pchString[1] = 'X'; break;
	}

//	CString foo; foo.Format("%d", ulEncodedBufLen); AfxMessageBox(foo);
	i=0;
	n=2;
	while((i<(int)ulEncodedBufLen)&&(n<8192))
	{
		if(pchEncoded[i]=='+')
		{
			pchString[n]='K'; n++;
			if((n==5)||(n==11))	{pchString[n]='-'; n++;}
			pchString[n]='p'; n++;
		}
		else
		if(pchEncoded[i]=='%')
		{
			pchString[n]='K'; n++;
			if((n==5)||(n==11))	{pchString[n]='-'; n++;}
			pchString[n]='r'; n++;
		}
		else
		if(pchEncoded[i]=='-')
		{
			pchString[n]='K'; n++;
			if((n==5)||(n==11))	{pchString[n]='-'; n++;}
			pchString[n]='D'; n++;
		}
		else
		{
			pchString[n]=pchEncoded[i]; n++;
		}
		if((n==5)||(n==11))	{pchString[n]='-'; n++;}

		i++;
	}


	if(m_pszLicenseString) free(m_pszLicenseString);
	m_pszLicenseString = (char*)malloc(strlen(pchString)+1);
	if(m_pszLicenseString) strcpy(m_pszLicenseString, pchString);

	if(pchEncoded) free(pchEncoded);

}

void CLicenseKey::InterpretKey() // looks at m_pszLicenseString and creates params
{
	m_bValid=false;
	if((m_pszLicenseString)&&(strlen(m_pszLicenseString)>0))
	{
		if((m_pszLicenseString[0]=='K')&&(m_pszLicenseString[5]=='-')&&(m_pszLicenseString[11]=='-'))
		{
			if(
				  (m_pszLicenseString[1]=='K')
				||(m_pszLicenseString[1]=='V')
				||(m_pszLicenseString[1]=='X')
				)
			{
				int nPads = 0; 
				if(m_pszLicenseString[1]=='V') nPads=1;
				if(m_pszLicenseString[1]=='X') nPads=2;

				char pchString[8192];  // thats a long enough key!
				memset( pchString, 0, 8192);

				unsigned long nLen = strlen(m_pszLicenseString);
				unsigned long n=0;
				unsigned long i=2;
				while(i<nLen)
				{
					if(m_pszLicenseString[i]!='-')
					{
						if(m_pszLicenseString[i]=='K')
						{
							i++;
							if(m_pszLicenseString[i]=='-') i++;

							if(m_pszLicenseString[i]=='p')	pchString[n] = '+';
							else if(m_pszLicenseString[i]=='r')	pchString[n] = '%';
							else if(m_pszLicenseString[i]=='D')	pchString[n] = '-';
						}
						else
						{
							pchString[n] = m_pszLicenseString[i];
						}
						n++;
					}
					i++;
				}
				if(nPads>=1) { pchString[n] = 'K'; n++; }
				if(nPads>=2) { pchString[n] = 'K'; n++; }
				pchString[n] = 0;

				unsigned long ulBufLen = n;
//CString foo; foo.Format("encoded %d: %s",ulBufLen, pchString);		AfxMessageBox(foo);
				CBufferUtil bu;
				char* pchDecoded = pchString;
				if(bu.Base64Decode(&pchDecoded, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH)>=RETURN_SUCCESS)
				{
//			AfxMessageBox(pchDecoded);
//CString foo; foo.Format("decoded %d: %s",ulBufLen, pchDecoded);		AfxMessageBox(foo);

					if(
							(pchDecoded[3]=='V')
						&&(pchDecoded[6]=='D')
						&&(pchDecoded[9]=='S')
						&&(pchDecoded[1]&(0x08|0x04))
						)
					{
						// we are valid.
						if(pchDecoded[1]&0x20) m_bMachineSpecific = true; else m_bMachineSpecific = false; 
						if(pchDecoded[1]&0x01) m_bExpireForgiveness = true; else m_bExpireForgiveness = false; 
						if(pchDecoded[1]&0x08) m_bExpires = true; else m_bExpires = false; 

						if(m_ppszParams!=NULL)
						{
							unsigned long i=0;
							while(i<m_ulNumParams)
							{
								if(m_ppszParams[i]) free(m_ppszParams[i]);
								i++;
							}
							delete [] m_ppszParams;
						}
						if(m_ppszValues!=NULL)
						{
							unsigned long i=0;
							while(i<m_ulNumParams)
							{
								if(m_ppszValues[i]) free(m_ppszValues[i]);
								i++;
							}
							delete [] m_ppszValues;
						}

						m_ppszParams=NULL;
						m_ppszValues=NULL;
						m_ulNumParams = 0;

						if(m_bMachineSpecific)
						{
							m_ucMAC[0]=pchDecoded[5];
							m_ucMAC[1]=pchDecoded[16];
							m_ucMAC[2]=pchDecoded[13];
							m_ucMAC[3]=pchDecoded[0];
							m_ucMAC[4]=pchDecoded[10];
							m_ucMAC[5]=pchDecoded[2];
						}
						if(m_bExpires)
						{
							m_ulExpiryDate = 0x00000000;
							m_ulExpiryDate |= (pchDecoded[4]&0x000000ff);
							m_ulExpiryDate |= ((pchDecoded[8]<<8)&0x0000ff00);
							m_ulExpiryDate |= ((pchDecoded[17]<<16)&0x00ff0000);
							m_ulExpiryDate |= ((pchDecoded[12]<<24)&0xff000000);
						}
						if(m_bExpireForgiveness)
						{
							m_ulExpiryForgiveness = 0x00000000;
							m_ulExpiryForgiveness |= (pchDecoded[18]&0x000000ff);
							m_ulExpiryForgiveness |= ((pchDecoded[14]<<8)&0x0000ff00);
							m_ulExpiryForgiveness |= ((pchDecoded[11]<<16)&0x00ff0000);
							m_ulExpiryForgiveness |= ((pchDecoded[15]<<24)&0xff000000);
						}
					}

					//OK, from here, we decode params.  first decrypt
					char pszParam[8192]; 
					memset(pszParam, 0, 8192);
					strcpy(pszParam, "license?");
//				AfxMessageBox(pchDecoded);

					n=19;
					if(n>=ulBufLen) m_bValid = true;

					i=strlen("license?");
					while(n<ulBufLen)
					{

						if(pchDecoded[n]=='Q')
						{
							pszParam[i] = '&';
						}
						else
						if(pchDecoded[n]=='+')
						{
							pszParam[i] = '=';
						}
						else
						if(pchDecoded[n]==0)
						{
							pszParam[i] = '+';
						}
						else
						if(pchDecoded[n]==13)
						{
							pszParam[i] = 'Q';
						}
						else
						{
							pszParam[i] = pchDecoded[n];
						}

						n++;
						i++;
					}

					free(pchDecoded);
					pchDecoded = pszParam;
					
//					AfxMessageBox(pchDecoded);
					if(URLDecode(&pchDecoded, &m_ppszParams, &m_ppszValues, &m_ulNumParams)>=0)
					{
						free(pchDecoded);
						m_bValid = true;
					}
					else m_bValid = false;


					if(m_bValid)
					{
						if(m_bExpires) 
						{
							if((unsigned long)time(NULL)>m_ulExpiryDate) m_bExpired=true; else m_bExpired=false;
						}

						if(m_bMachineSpecific) 
						{
							if(CheckMAC(m_ucMAC)==0) m_bValidMAC = true; else m_bValidMAC=false;
						}
					}
				}
			}
		}
	}
}

int CLicenseKey::CheckMAC(unsigned char* pucMAC)
{
	if(pucMAC)
	{
		bool bSuccess = false;
    HINSTANCE m_hInst;
    pSnmpExtensionInit m_Init;
    pSnmpExtensionInitEx m_InitEx;
    pSnmpExtensionQuery m_Query;
    pSnmpExtensionTrap m_Trap;
    HANDLE PollForTrapEvent;
    AsnObjectIdentifier SupportedView;
    UINT OID_ifEntryType[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 3 };
    UINT OID_ifEntryNum[]  = { 1, 3, 6, 1, 2, 1, 2, 1 };
    UINT OID_ipMACEntAddr[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 6 };  //, 1 ,6 };
    AsnObjectIdentifier MIB_ifMACEntAddr =
        { sizeof(OID_ipMACEntAddr) / sizeof(UINT), OID_ipMACEntAddr };
    AsnObjectIdentifier MIB_ifEntryType = { sizeof(OID_ifEntryType) / sizeof(UINT), OID_ifEntryType };
    AsnObjectIdentifier MIB_ifEntryNum = { sizeof(OID_ifEntryNum) / sizeof(UINT), OID_ifEntryNum };
    RFC1157VarBindList varBindList;
    RFC1157VarBind varBind[2];
    AsnInteger errorStatus;
    AsnInteger errorIndex;
    AsnObjectIdentifier MIB_NULL = { 0, 0 };
    int ret;
    int dtmp;
    int i = 0, j = 0;
    BOOL found = FALSE;
//    char TempEthernet[13];
    m_Init = NULL;
    m_InitEx = NULL;
    m_Query = NULL;
    m_Trap = NULL;

    /* Load the SNMP dll and get the addresses of the functions necessary */
    m_hInst = LoadLibrary(_T("inetmib1.dll"));  // use _T for use with COM
    if (m_hInst < (HINSTANCE) HINSTANCE_ERROR) 
		{
      m_hInst = NULL;
      return -1;
    }
    m_Init   = (pSnmpExtensionInit)    GetProcAddress(m_hInst, "SnmpExtensionInit");
    m_InitEx = (pSnmpExtensionInitEx)  GetProcAddress(m_hInst, "SnmpExtensionInitEx");
    m_Query  = (pSnmpExtensionQuery)   GetProcAddress(m_hInst, "SnmpExtensionQuery");
    m_Trap   = (pSnmpExtensionTrap)    GetProcAddress(m_hInst, "SnmpExtensionTrap");
    m_Init(GetTickCount(), &PollForTrapEvent, &SupportedView);

    /* Initialize the variable list to be retrieved by m_Query */
    varBindList.list = varBind;
    varBind[0].name = MIB_NULL;
    varBind[1].name = MIB_NULL;

    /* Copy in the OID to find the number of entries in the
       Inteface table */
    varBindList.len = 1;        /* Only retrieving one item */
    SNMP_oidcpy(&varBind[0].name, &MIB_ifEntryNum);
    ret = m_Query(ASN_RFC1157_GETNEXTREQUEST, &varBindList, &errorStatus, &errorIndex);
//CString foo; foo.Format("# of adapters in this system : %i\n", varBind[0].value.asnValue.number);AfxMessageBox(foo); 
		varBindList.len = 2;

    /* Copy in the OID of ifType, the type of interface */
    SNMP_oidcpy(&varBind[0].name, &MIB_ifEntryType);

    /* Copy in the OID of ifPhysAddress, the address */
    SNMP_oidcpy(&varBind[1].name, &MIB_ifMACEntAddr);

    do 
		{

      /* Submit the query.  Responses will be loaded into varBindList.
         We can expect this call to succeed a # of times corresponding
         to the # of adapters reported to be in the system */
      ret = m_Query(ASN_RFC1157_GETNEXTREQUEST, &varBindList, &errorStatus, &errorIndex); 
			if (!ret)
			{
				ret = 1;
			}
      else
			{  /* Confirm that the proper type has been returned */
          ret = SNMP_oidncmp(&varBind[0].name, &MIB_ifEntryType, MIB_ifEntryType.idLength); 
			}
			
			if (!ret) 
			{
        j++;
        dtmp = varBind[0].value.asnValue.number;
//CString foo; 
//foo.Format("Interface #%i type : %i\n", j, dtmp); AfxMessageBox(foo); 

        /* Type 6 describes ethernet interfaces */
        if (dtmp == 6) 
				{

          /* Confirm that we have an address here */
          ret = SNMP_oidncmp(&varBind[1].name, &MIB_ifMACEntAddr, MIB_ifMACEntAddr.idLength);
          if ((!ret)&&(varBind[1].value.asnValue.address.stream != NULL)) 
					{
            if (
                (varBind[1].value.asnValue.address.stream[0] ==
                 0x44)
                && (varBind[1].value.asnValue.address.stream[1] ==
                    0x45)
                && (varBind[1].value.asnValue.address.stream[2] ==
                    0x53)
                && (varBind[1].value.asnValue.address.stream[3] ==
                    0x54)
                && (varBind[1].value.asnValue.address.stream[4] ==
                    0x00)) 
						{

              /* Ignore all dial-up networking adapters */              
//foo.Format("Interface #%i is a DUN adapter\n", j); AfxMessageBox(foo); 
              continue;
						}
            if (
                (varBind[1].value.asnValue.address.stream[0] ==
                 0x00)
                && (varBind[1].value.asnValue.address.stream[1] ==
                    0x00)
                && (varBind[1].value.asnValue.address.stream[2] ==
                    0x00)
                && (varBind[1].value.asnValue.address.stream[3] ==
                    0x00)
                && (varBind[1].value.asnValue.address.stream[4] ==
                    0x00)
                && (varBind[1].value.asnValue.address.stream[5] ==
                    0x00)) 
						{

              /* Ignore NULL addresses returned by other network interfaces */
//foo.Format("Interface #%i is a NULL address\n", j); AfxMessageBox(foo); 
              continue;
            }

						if(
								(pucMAC[0] == varBind[1].value.asnValue.address.stream[0])
							&&(pucMAC[1] == varBind[1].value.asnValue.address.stream[1])
							&&(pucMAC[2] == varBind[1].value.asnValue.address.stream[2])
							&&(pucMAC[3] == varBind[1].value.asnValue.address.stream[3])
							&&(pucMAC[4] == varBind[1].value.asnValue.address.stream[4])
							&&(pucMAC[5] == varBind[1].value.asnValue.address.stream[5])
							)	bSuccess=true;

						
/*foo.Format("MAC Address of interface #%i: %02x%02x%02x%02x%02x%02x", j,
                    varBind[1].value.asnValue.address.stream[0],
                    varBind[1].value.asnValue.address.stream[1],
                    varBind[1].value.asnValue.address.stream[2],
                    varBind[1].value.asnValue.address.stream[3],
                    varBind[1].value.asnValue.address.stream[4],
                    varBind[1].value.asnValue.address.stream[5]); AfxMessageBox(foo); 
            /*
						sprintf(TempEthernet, "%02x%02x%02x%02x%02x%02x",
                    varBind[1].value.asnValue.address.stream[0],
                    varBind[1].value.asnValue.address.stream[1],
                    varBind[1].value.asnValue.address.stream[2],
                    varBind[1].value.asnValue.address.stream[3],
                    varBind[1].value.asnValue.address.stream[4],
                    varBind[1].value.asnValue.address.stream[5]);
            printf("MAC Address of interface #%i: %s\n", j,
                   TempEthernet);
									 */
					}
        }
      }
    } while ((!ret)&(!bSuccess));         /* Stop only on an error.  An error will occur
                               when we go exhaust the list of interfaces to
                               be examined */
//    getch();

    /* Free the bindings */
    SNMP_FreeVarBind(&varBind[0]);
    SNMP_FreeVarBind(&varBind[1]);

		if(bSuccess) return 0;
	}

	return -1;
}

// an encoded path is passed in in the ppszPath pointer, that buffer is destroyed, on success only.
// The value is then reset to point to a new buffer with the decoded path.
int CLicenseKey::URLDecode(char** ppszPath, char*** pppszNames, char*** pppszValues, unsigned long* pulNumPairs)
{
	if((ppszPath==NULL)||(*ppszPath==NULL)||(strlen(*ppszPath)==0)
		||(pppszNames==NULL)||(pppszValues==NULL)||(pulNumPairs==NULL)) return -1;
	char* pch;
	char* pszBuffer = *ppszPath;
	char* pszTemp = NULL;
	char* pszLast;// should be a pointer to the term zero. safety pointer, do not exceed this.
	unsigned long ulNumPairs = 0;
	CBufferUtil bu;
	// first determine if there are arguments.
	pch = strchr(pszBuffer, '?');
	if((pch!=NULL)&&(strlen(pch+1)))
	{
		ulNumPairs = bu.CountChar(pch+1, strlen(pch+1), '=');  // this tells us how many pairs.
		pszTemp = (char*)malloc((pch-pszBuffer)+1);
		if(pszTemp==NULL) return -1;
		memcpy(pszTemp, pszBuffer, (pch-pszBuffer));
		*(pszTemp+(pch-pszBuffer))=0;  // zero term
	}
	else
	{
		pszTemp = (char*)malloc(strlen(pszBuffer)+1);
		if(pszTemp==NULL) return -1;
		strcpy(pszTemp, pszBuffer);
	}

	//decode the path first
	//replace pluses with spaces;
	unsigned long ulLen = strlen(pszTemp);
	bu.ReplaceChar(pszTemp, ulLen, '+', ' ');
	// now, decode hex codes.
	char* pchMark = pszTemp;
	unsigned long i=0; 
	pszLast = pszTemp+strlen(pszTemp);
	while((i<ulLen)&&(pchMark<pszLast))
	{
		if((*(pszTemp+i))=='%')
		{// hex code to decode
			i++;
			if(i<ulLen)
			{
				switch(*(pszTemp+i))
				{
				case '0':	*pchMark = (char)0; i++; break;
				case '1':	*pchMark = (char)16; i++; break;
				case '2':	*pchMark = (char)32; i++; break;
				case '3':	*pchMark = (char)48; i++; break;
				case '4':	*pchMark = (char)64; i++; break;
				case '5':	*pchMark = (char)80; i++; break;
				case '6':	*pchMark = (char)96; i++; break;
				case '7':	*pchMark = (char)112; i++; break;
				case '8':	*pchMark = (char)128; i++; break;
				case '9':	*pchMark = (char)144; i++; break;
				case 'a':
				case 'A':	*pchMark = (char)160; i++; break;
				case 'b':
				case 'B':	*pchMark = (char)176; i++; break;
				case 'c':
				case 'C':	*pchMark = (char)192; i++; break;
				case 'd':
				case 'D':	*pchMark = (char)208; i++; break;
				case 'e':
				case 'E':	*pchMark = (char)224; i++; break;
				case 'f':
				case 'F':	*pchMark = (char)240; i++; break;
				default:   break; //not a hex char, error, but continue
				}
			}
			if(i<ulLen)
			{
				switch(*(pszTemp+i))
				{
				case '0': i++; break;
				case '1':	*pchMark += 1; i++; break;
				case '2':	*pchMark += 2; i++; break;
				case '3':	*pchMark += 3; i++; break;
				case '4':	*pchMark += 4; i++; break;
				case '5':	*pchMark += 5; i++; break;
				case '6':	*pchMark += 6; i++; break;
				case '7':	*pchMark += 7; i++; break;
				case '8':	*pchMark += 8; i++; break;
				case '9':	*pchMark += 9; i++; break;
				case 'a':
				case 'A':	*pchMark += 10; i++; break;
				case 'b':
				case 'B':	*pchMark += 11; i++; break;
				case 'c':
				case 'C':	*pchMark += 12; i++; break;
				case 'd':
				case 'D':	*pchMark += 13; i++; break;
				case 'e':
				case 'E':	*pchMark += 14; i++; break;
				case 'f':
				case 'F':	*pchMark += 15; i++; break;
				default:	break;//not a hex char, error, but continue
				}
			}
		}
		else
		{
			*pchMark = (*(pszTemp+i));
			i++;
		}
		pchMark++;
	}
	*pchMark=0;  // zero term the path

	*ppszPath = pszTemp;  // we still have the original buffer in pszBuffer.
	if(ulNumPairs>0)
	{
		char** ppszNames = (char**)malloc(sizeof(char*)*(ulNumPairs+1));
		if(ppszNames==NULL) return -1;
		char** ppszValues = (char**)malloc(sizeof(char*)*(ulNumPairs+1));
		if(ppszValues==NULL) return -1;

		CSafeBufferUtil sbu;

		unsigned long j=0; 
		
		char* pchPair = sbu.Token(pch+1, strlen(pch+1), "=");
		while((j<ulNumPairs)&&(pchPair))
		{
			ppszNames[j] = NULL;
			ppszValues[j] = NULL;

			if(pchPair)
			{
				ulLen = strlen(pchPair);
				pszTemp = (char*)malloc(strlen(pchPair)+1);
				if(pszTemp)
				{
					strcpy(pszTemp, pchPair);
					bu.ReplaceChar(pszTemp, ulLen, '+', ' ');
					// now, decode hex codes.
					pchMark = pszTemp;
					i=0; 
					while(i<ulLen)
					{
						if((*(pszTemp+i))=='%')
						{// hex code to decode
							i++;
							if(i<ulLen)
							{
								switch(*(pszTemp+i))
								{
								case '0':	*pchMark = (char)0; i++; break;
								case '1':	*pchMark = (char)16; i++; break;
								case '2':	*pchMark = (char)32; i++; break;
								case '3':	*pchMark = (char)48; i++; break;
								case '4':	*pchMark = (char)64; i++; break;
								case '5':	*pchMark = (char)80; i++; break;
								case '6':	*pchMark = (char)96; i++; break;
								case '7':	*pchMark = (char)112; i++; break;
								case '8':	*pchMark = (char)128; i++; break;
								case '9':	*pchMark = (char)144; i++; break;
								case 'a':
								case 'A':	*pchMark = (char)160; i++; break;
								case 'b':
								case 'B':	*pchMark = (char)176; i++; break;
								case 'c':
								case 'C':	*pchMark = (char)192; i++; break;
								case 'd':
								case 'D':	*pchMark = (char)208; i++; break;
								case 'e':
								case 'E':	*pchMark = (char)224; i++; break;
								case 'f':
								case 'F':	*pchMark = (char)240; i++; break;
								default:   break; //not a hex char, error, but continue
								}
							}
							if(i<ulLen)
							{
								switch(*(pszTemp+i))
								{
								case '0': i++; break;
								case '1':	*pchMark += 1; i++; break;
								case '2':	*pchMark += 2; i++; break;
								case '3':	*pchMark += 3; i++; break;
								case '4':	*pchMark += 4; i++; break;
								case '5':	*pchMark += 5; i++; break;
								case '6':	*pchMark += 6; i++; break;
								case '7':	*pchMark += 7; i++; break;
								case '8':	*pchMark += 8; i++; break;
								case '9':	*pchMark += 9; i++; break;
								case 'a':
								case 'A':	*pchMark += 10; i++; break;
								case 'b':
								case 'B':	*pchMark += 11; i++; break;
								case 'c':
								case 'C':	*pchMark += 12; i++; break;
								case 'd':
								case 'D':	*pchMark += 13; i++; break;
								case 'e':
								case 'E':	*pchMark += 14; i++; break;
								case 'f':
								case 'F':	*pchMark += 15; i++; break;
								default:	break;//not a hex char, error, but continue
								}
							}
						}
						else
						{
							*pchMark = (*(pszTemp+i));
							i++;
						}
						pchMark++;
					}
					*pchMark=0;  // zero term the path

				}
				ppszNames[j] = pszTemp;
	
				pchPair = sbu.Token(NULL, 0, "&");

				if(pchPair)
				{
					ulLen = strlen(pchPair);
					pszTemp = (char*)malloc(strlen(pchPair)+1);
					if(pszTemp)
					{
						strcpy(pszTemp, pchPair);
						bu.ReplaceChar(pszTemp, ulLen, '+', ' ');
						// now, decode hex codes.
						pchMark = pszTemp;
						i=0; 
						while(i<ulLen)
						{
							if((*(pszTemp+i))=='%')
							{// hex code to decode
								i++;
								if(i<ulLen)
								{
									switch(*(pszTemp+i))
									{
									case '0':	*pchMark = (char)0; i++; break;
									case '1':	*pchMark = (char)16; i++; break;
									case '2':	*pchMark = (char)32; i++; break;
									case '3':	*pchMark = (char)48; i++; break;
									case '4':	*pchMark = (char)64; i++; break;
									case '5':	*pchMark = (char)80; i++; break;
									case '6':	*pchMark = (char)96; i++; break;
									case '7':	*pchMark = (char)112; i++; break;
									case '8':	*pchMark = (char)128; i++; break;
									case '9':	*pchMark = (char)144; i++; break;
									case 'a':
									case 'A':	*pchMark = (char)160; i++; break;
									case 'b':
									case 'B':	*pchMark = (char)176; i++; break;
									case 'c':
									case 'C':	*pchMark = (char)192; i++; break;
									case 'd':
									case 'D':	*pchMark = (char)208; i++; break;
									case 'e':
									case 'E':	*pchMark = (char)224; i++; break;
									case 'f':
									case 'F':	*pchMark = (char)240; i++; break;
									default:   break; //not a hex char, error, but continue
									}
								}
								if(i<ulLen)
								{
									switch(*(pszTemp+i))
									{
									case '0': i++; break;
									case '1':	*pchMark += 1; i++; break;
									case '2':	*pchMark += 2; i++; break;
									case '3':	*pchMark += 3; i++; break;
									case '4':	*pchMark += 4; i++; break;
									case '5':	*pchMark += 5; i++; break;
									case '6':	*pchMark += 6; i++; break;
									case '7':	*pchMark += 7; i++; break;
									case '8':	*pchMark += 8; i++; break;
									case '9':	*pchMark += 9; i++; break;
									case 'a':
									case 'A':	*pchMark += 10; i++; break;
									case 'b':
									case 'B':	*pchMark += 11; i++; break;
									case 'c':
									case 'C':	*pchMark += 12; i++; break;
									case 'd':
									case 'D':	*pchMark += 13; i++; break;
									case 'e':
									case 'E':	*pchMark += 14; i++; break;
									case 'f':
									case 'F':	*pchMark += 15; i++; break;
									default:	break;//not a hex char, error, but continue
									}
								}
							}
							else
							{
								*pchMark = (*(pszTemp+i));
								i++;
							}
							pchMark++;
						}
						*pchMark=0;  // zero term the path
					}
					ppszValues[j] = pszTemp;
				}
			}
			j++;
			pchPair = sbu.Token(NULL, 0, "=");
		}

		ppszNames[j] = NULL;
		ppszValues[j] = NULL;


		*pppszNames = ppszNames;
		*pppszValues = ppszValues;

	}
	else
	{
		*pppszNames = NULL;
		*pppszValues = NULL;
	}

	// dont free, do it maually outside, in case more to be checked on buffer.
	//free(pszBuffer);  // frees the original incoming buffer
	*pulNumPairs = ulNumPairs;

	return 0;
}

