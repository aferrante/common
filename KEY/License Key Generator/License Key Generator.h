// License Key Generator.h : main header file for the LICENSE KEY GENERATOR application
//

#if !defined(AFX_LICENSEKEYGENERATOR_H__B7137802_8F56_439D_B302_6195FDF37E82__INCLUDED_)
#define AFX_LICENSEKEYGENERATOR_H__B7137802_8F56_439D_B302_6195FDF37E82__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CLicenseKeyGeneratorApp:
// See License Key Generator.cpp for the implementation of this class
//

class CLicenseKeyGeneratorApp : public CWinApp
{
public:
	CLicenseKeyGeneratorApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicenseKeyGeneratorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLicenseKeyGeneratorApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LICENSEKEYGENERATOR_H__B7137802_8F56_439D_B302_6195FDF37E82__INCLUDED_)
