// License Key GeneratorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "License Key Generator.h"
#include "License Key GeneratorDlg.h"
#include "C:\Source\Common\TXT\BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicenseKeyGeneratorDlg dialog

CLicenseKeyGeneratorDlg::CLicenseKeyGeneratorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseKeyGeneratorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLicenseKeyGeneratorDlg)
	m_szExpires = _T("");
	m_nForgive = 0;
	m_szKey = _T("");
	m_szMAC = _T("");
	m_szParams = _T("");
	m_bExpires = FALSE;
	m_bExpForgive = FALSE;
	m_bMAC = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLicenseKeyGeneratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLicenseKeyGeneratorDlg)
	DDX_Text(pDX, IDC_EDIT_EXPIRES, m_szExpires);
	DDX_Text(pDX, IDC_EDIT_FORGIVE, m_nForgive);
	DDV_MinMaxInt(pDX, m_nForgive, 0, 2147483647);
	DDX_Text(pDX, IDC_EDIT_KEY, m_szKey);
	DDX_Text(pDX, IDC_EDIT_MAC, m_szMAC);
	DDX_Text(pDX, IDC_EDIT_PARAMS, m_szParams);
	DDX_Check(pDX, IDC_CHECK_EXP, m_bExpires);
	DDX_Check(pDX, IDC_CHECK_EXPFORGIVE, m_bExpForgive);
	DDX_Check(pDX, IDC_CHECK_MAC, m_bMAC);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLicenseKeyGeneratorDlg, CDialog)
	//{{AFX_MSG_MAP(CLicenseKeyGeneratorDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_ANALYZE, OnButtonAnalyze)
	ON_BN_CLICKED(IDC_BUTTON_GEN, OnButtonGen)
	ON_BN_CLICKED(IDC_CHECK_EXP, OnCheckExp)
	ON_BN_CLICKED(IDC_CHECK_EXPFORGIVE, OnCheckExpforgive)
	ON_BN_CLICKED(IDC_CHECK_MAC, OnCheckMac)
	ON_EN_CHANGE(IDC_EDIT_FORGIVE, OnChangeEditForgive)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicenseKeyGeneratorDlg message handlers

BOOL CLicenseKeyGeneratorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

		CTime exptm( time(NULL) );

		m_szExpires.Format("%04d-%02d-%02d %02d:%02d:%02d", 
			exptm.GetYear(), 
			exptm.GetMonth(), 
			exptm.GetDay(), 
			exptm.GetHour(), 
			exptm.GetMinute(), 
			exptm.GetSecond()
			);
		UpdateData(FALSE);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLicenseKeyGeneratorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLicenseKeyGeneratorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLicenseKeyGeneratorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CLicenseKeyGeneratorDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
}

void CLicenseKeyGeneratorDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CLicenseKeyGeneratorDlg::OnButtonAnalyze() 
{
	UpdateData(TRUE);
	if(m_szKey.GetLength()>0)
	{
		if(m_cKey.m_pszLicenseString) free(m_cKey.m_pszLicenseString);
		m_cKey.m_pszLicenseString = (char*)malloc(m_szKey.GetLength()+1);
		if(m_cKey.m_pszLicenseString)
		{
			sprintf(m_cKey.m_pszLicenseString, "%s", m_szKey);
			m_cKey.InterpretKey();
			if(m_cKey.m_bValid)
			{
				char status[8192];
				strcpy(status, "valid key. ");
				if(m_cKey.m_bMachineSpecific)
				{
					m_bMAC=TRUE;
					if(m_cKey.m_bValidMAC)
					{
						strcat(status, "valid MAC. ");
						m_szMAC.Format("%02x-%02x-%02x-%02x-%02x-%02x", m_cKey.m_ucMAC[0],m_cKey.m_ucMAC[1],m_cKey.m_ucMAC[2],m_cKey.m_ucMAC[3],m_cKey.m_ucMAC[4],m_cKey.m_ucMAC[5]);
					}
					else
					{
						strcat(status, "non-valid MAC. ");
						m_szMAC.Format("No match: %02x-%02x-%02x-%02x-%02x-%02x", m_cKey.m_ucMAC[0],m_cKey.m_ucMAC[1],m_cKey.m_ucMAC[2],m_cKey.m_ucMAC[3],m_cKey.m_ucMAC[4],m_cKey.m_ucMAC[5]);
					}
				} else m_bMAC=FALSE;
				if(m_cKey.m_bExpires)
				{
					if(m_cKey.m_bExpired)
						strcat(status, "expired. ");
					else
						strcat(status, "not expired. ");
					m_bExpires=TRUE;

					CTime exptm( m_cKey.m_ulExpiryDate );
					m_szExpires.Format("%04d-%02d-%02d %02d:%02d:%02d", 
						exptm.GetYear(), 
						exptm.GetMonth(), 
						exptm.GetDay(), 
						exptm.GetHour(), 
						exptm.GetMinute(), 
						exptm.GetSecond()
						);


				} else{ m_bExpires=FALSE; m_szExpires="No expiry date";}

				if(m_cKey.m_bExpireForgiveness)
				{
					m_bExpForgive=TRUE;
					if(m_cKey.m_bExpires)
					{
						if((unsigned long)time(NULL)<m_cKey.m_ulExpiryDate+m_cKey.m_ulExpiryForgiveness)
							strcat(status, "within forgiveness period. ");
						else
							strcat(status, "past forgiveness. ");
					}
					else
						strcat(status, "forgiveness does not apply. ");

					m_nForgive = m_cKey.m_ulExpiryForgiveness;
				} else m_bExpForgive=FALSE;
				GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(status);


				// update the edit fields...

				m_szParams="";
				unsigned long i=0;
				while(i<m_cKey.m_ulNumParams)
				{
					if(i!=0)m_szParams+="&";
					m_szParams+=m_cKey.m_ppszParams[i];
					m_szParams+="=";
					m_szParams+=m_cKey.m_ppszValues[i];

					i++;
				}
				UpdateData(FALSE);

			}
			else
			{
				GetDlgItem(IDC_STATIC_STATUS)->SetWindowText("not a valid key");
			}
		}
		else
		{
			GetDlgItem(IDC_STATIC_STATUS)->SetWindowText("memory error");
		}
	}
	else 
	{
		GetDlgItem(IDC_STATIC_STATUS)->SetWindowText("invalid key");
	}
}

void CLicenseKeyGeneratorDlg::OnButtonGen() 
{
//	AfxMessageBox("Q");
	if(!UpdateData(TRUE)) return;//{AfxMessageBox("X"); return;}

	m_cKey.m_bMachineSpecific = m_bMAC?true:false; // checks against MAC address
	m_cKey.m_bExpires = m_bExpires?true:false; // checks against MAC address
	m_cKey.m_bExpireForgiveness = m_bExpForgive?true:false; // checks against MAC address

	m_cKey.SetParams(m_szParams.GetBuffer(1));
	m_szParams.ReleaseBuffer();

	if(m_cKey.m_bMachineSpecific)
	{
		CSafeBufferUtil sbu;
		CBufferUtil bu;
		char szMAC[256];
		sprintf(szMAC, "%s", m_szMAC);
		char* pch = sbu.Token(szMAC, strlen(szMAC), "-:.");
		int n=0;
		while((pch)&&(n<6))
		{
			m_cKey.m_ucMAC[n] = (char) bu.xtol(pch, 2); n++;
			pch = sbu.Token(NULL, NULL, "-:.");
		}
	}

	if(m_cKey.m_bExpires)
	{
		CSafeBufferUtil sbu;

		char szDate[256];
		int nDate[6];
		sprintf(szDate, "%s", m_szExpires);
		char* pch = sbu.Token(szDate, strlen(szDate), "-:. ");
		int n=0;
		while((pch)&&(n<6))
		{
			nDate[n] = atoi(pch); n++;
			pch = sbu.Token(NULL, NULL, "-:. ");
		}

		CTime ctime( nDate[0], nDate[1], nDate[2], nDate[3], nDate[4], nDate[5] );

		m_cKey.m_ulExpiryDate = ctime.GetTime( );

	}

	if(m_cKey.m_bExpireForgiveness)
	{
		m_cKey.m_ulExpiryForgiveness = m_nForgive;
	}

	m_cKey.GenerateKey();

	if(m_cKey.m_pszLicenseString) m_szKey = m_cKey.m_pszLicenseString;
	else m_szKey="no key";

	UpdateData(FALSE);


/*
	unsigned char m_ucMAC[6];      // MAC address
	unsigned long m_ulExpiryDate;  // unixtime
	unsigned long m_ulExpiryForgiveness;  //seconds
*/	
}

void CLicenseKeyGeneratorDlg::OnCheckExp() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CLicenseKeyGeneratorDlg::OnCheckExpforgive() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CLicenseKeyGeneratorDlg::OnCheckMac() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CLicenseKeyGeneratorDlg::OnChangeEditForgive() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_CHANGE flag ORed into the lParam mask.
	
	// TODO: Add your control notification handler code here
	if(m_bExpires)
	{
		UpdateData(TRUE);
		CString foo;
		CSafeBufferUtil sbu;

		char szDate[256];
		int nDate[6];
		sprintf(szDate, "%s", m_szExpires);
		char* pch = sbu.Token(szDate, strlen(szDate), "-:. ");
		int n=0;
		while((pch)&&(n<6))
		{
			nDate[n] = atoi(pch); n++;
			pch = sbu.Token(NULL, NULL, "-:. ");
		}

		CTime exptm( nDate[0], nDate[1], nDate[2], nDate[3], nDate[4], nDate[5] );

		exptm += CTimeSpan(m_nForgive);

		foo.Format("seconds. (%.02f days) - limit %04d-%02d-%02d %02d:%02d:%02d", 
			((float)m_nForgive)/86400.0,
			exptm.GetYear(), 
			exptm.GetMonth(), 
			exptm.GetDay(), 
			exptm.GetHour(), 
			exptm.GetMinute(), 
			exptm.GetSecond()
			);
		GetDlgItem(IDC_STATIC_SECCALC)->SetWindowText(foo);
	}
	else
	{
		UpdateData(TRUE);
		CString foo;
		foo.Format("seconds. (%.02f days)", ((float)m_nForgive)/86400.0);
		GetDlgItem(IDC_STATIC_SECCALC)->SetWindowText(foo);
	}
}
