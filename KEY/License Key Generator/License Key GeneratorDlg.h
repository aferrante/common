// License Key GeneratorDlg.h : header file
//

#if !defined(AFX_LICENSEKEYGENERATORDLG_H__03C67B85_1496_4714_84C4_2E694C8DE7FD__INCLUDED_)
#define AFX_LICENSEKEYGENERATORDLG_H__03C67B85_1496_4714_84C4_2E694C8DE7FD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../LicenseKey.h"


/////////////////////////////////////////////////////////////////////////////
// CLicenseKeyGeneratorDlg dialog

class CLicenseKeyGeneratorDlg : public CDialog
{
// Construction
public:
	CLicenseKeyGeneratorDlg(CWnd* pParent = NULL);	// standard constructor

	CLicenseKey m_cKey;

// Dialog Data
	//{{AFX_DATA(CLicenseKeyGeneratorDlg)
	enum { IDD = IDD_LICENSEKEYGENERATOR_DIALOG };
	CString	m_szExpires;
	int		m_nForgive;
	CString	m_szKey;
	CString	m_szMAC;
	CString	m_szParams;
	BOOL	m_bExpires;
	BOOL	m_bExpForgive;
	BOOL	m_bMAC;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicenseKeyGeneratorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CLicenseKeyGeneratorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonAnalyze();
	afx_msg void OnButtonGen();
	afx_msg void OnCheckExp();
	afx_msg void OnCheckExpforgive();
	afx_msg void OnCheckMac();
	afx_msg void OnChangeEditForgive();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LICENSEKEYGENERATORDLG_H__03C67B85_1496_4714_84C4_2E694C8DE7FD__INCLUDED_)
