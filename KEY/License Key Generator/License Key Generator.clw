; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CLicenseKeyGeneratorDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "License Key Generator.h"

ClassCount=3
Class1=CLicenseKeyGeneratorApp
Class2=CLicenseKeyGeneratorDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_LICENSEKEYGENERATOR_DIALOG

[CLS:CLicenseKeyGeneratorApp]
Type=0
HeaderFile=License Key Generator.h
ImplementationFile=License Key Generator.cpp
Filter=N

[CLS:CLicenseKeyGeneratorDlg]
Type=0
HeaderFile=License Key GeneratorDlg.h
ImplementationFile=License Key GeneratorDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_EDIT_FORGIVE

[CLS:CAboutDlg]
Type=0
HeaderFile=License Key GeneratorDlg.h
ImplementationFile=License Key GeneratorDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=3
Control1=IDC_STATIC,static,1342308480
Control2=IDC_STATIC,static,1342308352
Control3=IDOK,button,1342373889

[DLG:IDD_LICENSEKEYGENERATOR_DIALOG]
Type=1
Class=CLicenseKeyGeneratorDlg
ControlCount=19
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_KEY,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_BUTTON_GEN,button,1342242816
Control6=IDC_BUTTON_ANALYZE,button,1342242816
Control7=IDC_CHECK_EXP,button,1342242819
Control8=IDC_CHECK_EXPFORGIVE,button,1342242819
Control9=IDC_EDIT_FORGIVE,edit,1350631552
Control10=IDC_STATIC_SECCALC,static,1342308352
Control11=IDC_EDIT_EXPIRES,edit,1350631552
Control12=IDC_STATIC,static,1342308352
Control13=IDC_CHECK_MAC,button,1342242819
Control14=IDC_EDIT_MAC,edit,1350631552
Control15=IDC_STATIC_STATUS,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT_PARAMS,edit,1350631552
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,static,1342308354

