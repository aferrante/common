// FileUtil.h: interface for the CFileUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LICENSEKEY_H__729886C1_8345_46F2_978D_FD663E4DAF01__INCLUDED_)
#define AFX_LICENSEKEY_H__729886C1_8345_46F2_978D_FD663E4DAF01__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
#define LICENSE_B64PADCH		'K'


class CLicenseKey
{
public:
	char* m_pszLicenseString;

	bool m_bValid;
	bool m_bMachineSpecific; // checks against MAC address
	bool m_bValidMAC;
	bool m_bExpires;   // checks against clock
	bool m_bExpired;   // is past the date.
	bool m_bExpireForgiveness;   // forgive past the expiry date.

	unsigned char m_ucMAC[6];      // MAC address
	unsigned long m_ulExpiryDate;  // unixtime
	unsigned long m_ulExpiryForgiveness;  //seconds

	char** m_ppszParams;
	char** m_ppszValues;
	unsigned long m_ulNumParams;

public:
	CLicenseKey();
	virtual ~CLicenseKey();

	void AddParam(char* pszParam, char* pszValue);  
	void DeleteParam(char* pszParam);  
	void GenerateKey();  // looks at params and creates m_pszLicenseString
	void InterpretKey(); // looks at m_pszLicenseString and creates params
	void SetParams(char* pszParams);  
	int URLDecode(char** ppszPath, char*** pppszNames, char*** pppszValues, unsigned long* pulNumPairs);
	int CheckMAC(unsigned char* pucMAC);
};

#endif // !defined(AFX_LICENSEKEY_H__729886C1_8345_46F2_978D_FD663E4DAF01__INCLUDED_)
