// FileUtil.h: interface for the CFileUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILEUTIL_H__729886C1_8345_46F2_978D_FD663E4DAF01__INCLUDED_)
#define AFX_FILEUTIL_H__729886C1_8345_46F2_978D_FD663E4DAF01__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <iostream.h>
#include <stdio.h>
#include "../MSG/MessagingObject.h"

#define FILEUTIL_FILE_OPEN						0x00000001
#define FILEUTIL_MALLOC_OK						0x00000002
#define FILEUTIL_MALLOC_FAILED				0x00000004
#define FILEUTIL_ENCRYPTED						0x00000008
#define FILEUTIL_FILE_EXISTS					0x00000010
#define FILEUTIL_FILE_ERROR						0x00000020

#define FILEUTIL_WRITE_OK						  0x00000000
#define FILEUTIL_WRITE_NOSECTION		  0x00000001
#define FILEUTIL_WRITE_NOENTRY			  0x00000002
#define FILEUTIL_WRITE_NOVALUE			  0x00000003
#define FILEUTIL_WRITE_BADBUFFER		  0x00000004
#define FILEUTIL_WRITE_NOMALLOC			  0x00000005
#define FILEUTIL_WRITE_INSERTERROR	  0x00000006

class CFileUtil : public CMessagingObject
{
public:
	unsigned long m_ulStatus;
	FILE* m_pfileSettings;
	char* m_pchBuffer;
	unsigned long m_ulBufferLength;


public:
	CFileUtil();
	virtual ~CFileUtil();

	int GetSettings(char* pszFilename, bool bEncrypted=false);
	int SetSettings(char* pszFilename, bool bEncrypted=false);
	char* GetIniString(char* pszSection, char* pszEntry, char* pszDefaultValue, char* pszOldValue=NULL);
	int SetIniString(char* pszSection, char* pszEntry, char* pszValue, char* pszComment=NULL);
	int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);
	int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

	void Encrypt();
	void Decrypt();

};

#endif // !defined(AFX_FILEUTIL_H__729886C1_8345_46F2_978D_FD663E4DAF01__INCLUDED_)
