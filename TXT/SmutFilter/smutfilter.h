// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual
// property of Video Design Interactive and may not be distributed.

#ifndef SMUTFILTER_INCLUDED
#define SMUTFILTER_INCLUDED

#define MAX_SMUTLIST_SIZE  1000
#define MAX_WORDFORMS_SIZE 20
#define MAX_CATEGORIES 20


class CSmutFilter // : public CObject
{
public:
	CSmutFilter();   // standard constructor
	~CSmutFilter();   // standard destructor

public:
  // public member variables:
  BOOL m_bStub;
  CString m_szDictionaryName;
  BOOL m_bHighlightWholeWord;
  // change notification
  CString m_szChanges; BOOL m_bChanged;

  // public methods

  // main interface to checker
  int languageFilter(CString *text, int **startEndPairs, int *listSize,
                     BOOL bReplaceCurses, unsigned long *wordsChecked);

  // interfaces for modifying dictionary.

  // working with the dictionary:
  BOOL loadDictionary();
  BOOL saveDictionary();
  int getDictionarySize();
  BOOL validateDictionary(CString *szErrorMsg);
  void sortDictionary(BOOL bSeparateSubstrings=TRUE);

  // working with categories:
  BOOL enableCategory(CString szCategory);
  BOOL disableCategory(CString szCategory);
  BOOL isDisabledCategory(CString szCategory);
  //int getCategoryList(CString szCategoryList[MAX_CATEGORIES]);
 // int getDisabledCategoryList(CString szCategoryList[MAX_CATEGORIES]);
  BOOL categoryExists(CString szCategory);
  BOOL addCategory(CString szCategory, BOOL bActive);
  int getCategorySize();
  BOOL getCategory(int n, CString *szCategory, BOOL *bActive);
  int countWordsInCategory(CString szCategory);

  // working with words:
  BOOL getWord(int nPosition, CString *szWord, BOOL *bActive, BOOL *bWhole, CString *szCategory, CString *szComment);
  int addWord(CString szWord, BOOL bActive, BOOL bWhole, CString szCategory, CString szComment);
  BOOL editWord(int nPosition, CString szWord, BOOL bActive, BOOL bWhole, CString szCategory, CString szComment);
  BOOL deleteWord(int nItem);
  BOOL setActive(int nPosition, BOOL bActive);
  BOOL setWholeWordOnly(int nPosition, BOOL bActive);
  BOOL setCategory(int nPosition, CString szCategory);

  // utility:
  CString generateWordForms(CString szWord);
  int generateWordFormsList(CString szWord, CString szWordList[]);

private:
  // private member variables:
  FILE *m_logfp;
  int m_nMaxSuffixLength;  // max subtractable suffix length: y-ies = 1 for 'y'

  // storage of the smut list
  int m_nSmutListSize;  // number of dictionary entries
  struct smut_t
  {
    CString word, category, comment;
    int active, wholeWordOnly;
  } m_smutList[MAX_SMUTLIST_SIZE];
  int m_smutMinLength; // minimum length of a curse, used for speedup

  // storage of the language-specific word forms, for example:
  // wordforms=s,er,ed,ers,ing,y-ies
  CString m_wordForms[MAX_WORDFORMS_SIZE];  
  int m_nWordFormListSize;// number of word form entries
  
  // storage for category list
  struct cat_t
  {
    CString category;
    BOOL active;
  } m_categoryList[MAX_CATEGORIES];
  int m_nCategoryListSize; // number of disabled categories

  struct prev_t
  {
    CString word;
    int nOrigIndices[256];
    int nOrigIndex, nOrigLength;
  } m_Prevwords[25];
  int m_nPrevIndex;

  //CString m_DisabledCategoryList[MAX_CATEGORIES];
  //int m_nDisabledCategoryListSize; // number of disabled categories

  // private methods used by language filter:
//  BOOL checkWord(CString prevword, CString word, 
//                 CString *match, int *start, int *end);

#ifdef NEW_F_U_C_K_Y_O_U
  BOOL checkWord(CString word, CString *match, int *start, int *end);
#else
  BOOL checkWord(CString prevword, int prevwordlen, CString word, CString *match, int *start, int *end);
#endif
  BOOL compareWords(CString wordToCheck, CString dictWord, 
                    BOOL bCheckForms, BOOL wholeWordOnly, 
                    int *start, CString *match);
  CString stringFilter(CString in);

  // change notification
  void addChanges(CString change);
};


#endif
