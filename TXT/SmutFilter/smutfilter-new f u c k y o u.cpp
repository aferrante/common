/*
 * Smut filter class
 *  Gregg, Stated 5/31/00
 *  Gregg, 01/24/01 - made into class
 *  Gregg, 01/25/01 - major revamp of the engine
 *  Gregg, 02/28-29/01 - major revamp of the engine again.
 *  Gregg, 12-17-01  - added filtering of numerals from the incoming string, e.g. "fu333ck"
 *
 *
 *  Calling:
 *  void setMailServer(CString mailserver);  // for mailing change feedback
 *    If you call this first with a valid mail server name in
 *    the SmutEditor, then at the end of editing the system will
 *    email all the changes made back to VDI - insta-feedback!
 *
 *  int CSmutFilter::languageFilter(CString *text, int **startEndPairs, int *listSize,
 *                                 BOOL bReplaceCurses, unsigned long *wordsChecked)
 *
 *    int *startEndPairs=NULL, listSize=0, numCurses;
 *    unsigned long wordsChecked; // returns number of words in the buffer
 *    CString bufferToCheck;
 *    BOOL bReplaceCurses = TRUE/FALSE (replace curses with punctuation)
 *    numCurses = languageFilter(&bufferToCheck, startEndPairs, &listSize, 
 *                               bReplaceCurses, &wordsChecked);
 *
 *  NOTE: A sample "OnButtonCheck()" is provided at the bottom of this file.
 *
 *  BEWARE:  Every time you instantiate a CSmutFilter object, the smut list
 *           is loaded again.  Make the smutFilter a member variable to 
 *           avoid this.
 *
 *  ALSO BEWARE:  The constructor calls preloadSmut("curses.txt"), which
 *            means that the smutfile must be in the local directory where
 *            this is called!!!
 *
 *  Returns:
 *    number of curses found total,
 *
 *    startEndPairs contains 2*(number of dirty words found) integers
 *    that are the start and end indices of dirty words, starting
 *    with index 0.  Thus, the string "eat-F--U--C--K-me" will return
 *    two integers, 4 and 13, marking the beginning and end indices
 *    of the bad word.  listSize is the maximum list size, in case
 *    the buffer is too small - it is NOT the total number of integers
 *    returned.  The list will be realloc'd if necessary.
 *
 *  Curse list from:  http://www.jps.net/scritch/swear.html
 *  Carlin extended list from:  http://pages.map.com/~mrpbody/humour/dirtwrd.txt
 *  Other words from the meta tags at porn sites!
 * 
 *  Performance:  On my PIII-500 laptop, approx. 150 words/second.
 *  01/26/2001   english.words dictionary (from file) 49168 words, 392s, 125.4 words/sec
 *  01/26/2001   LilithFair-FE.com.txt (from edit box) 2165 words, 14s, 154.7 words/sec
 *  01/26/2001   Kenji's Ch.16 (from file) 11,370 words, 75s, 151.6 words/sec
 */

#include "stdafx.h"
#include "SmutFilter.h"
#include "../vdifile/vdifile.h"

#define GSTR(X) (char *)((LPCTSTR) X)


CSmutFilter::CSmutFilter()
{
  CTime theTime = CTime::GetCurrentTime();
  m_szChanges.Format("Changes performed on %s.\n\n", theTime.Format("%m/%d/%Y")); // internal changelog
  m_bChanged = FALSE;
  
  // override this if you like from outside...
  m_bHighlightWholeWord = TRUE;//FALSE; // highlight fuckwit as ****wit or *******?

  m_szDictionaryName = "curses.vdi";
  if (!loadDictionary())
  {
    AfxMessageBox("Error loading smut filter dictionary!");
  }
  m_logfp = NULL; //fopen("smutfilter.log", "wt");
  m_nMaxSuffixLength = 1;

  m_nPrevIndex = 0;
}

CSmutFilter::~CSmutFilter()
{
  if (m_logfp) fclose(m_logfp);
}




/*
 *  Finds and flags a bad word in a buffer contained in 
 *  a single CString.  Returns TRUE if any questionable words are
 *  found.
 */
BOOL CSmutFilter::checkWord(CString word, CString *match, int *start, int *end)
{
  CString wordForm, temp, checking;
  int i, l, n, oIndex, found, s, starpos, startPos;
  BOOL wholeWordOnly;
  CString dictWordList[50];
  int dictWordIndex=0;
  CString debug;
  

  // f�ck yo�


      // add word to prevwords array.  Someday, this should be a circular (ring) buffer
      if (m_nPrevIndex <24) 
      {
        m_Prevwords[m_nPrevIndex].word = stringFilter(word);
        m_Prevwords[m_nPrevIndex].word.MakeLower();
        m_nPrevIndex++;
      }
      else
      {
        for (int z=0; z<24; z++) m_Prevwords[z] = m_Prevwords[z+1];
        m_Prevwords[24].word = stringFilter(word);
        (m_Prevwords[24].word).MakeLower();
      }

  
  word = m_Prevwords[m_nPrevIndex - 1].word;
  l = word.GetLength();   
  if (l < m_smutMinLength)  return FALSE;
  
  for (found = FALSE, i=0, oIndex = 0; i<l; i++, oIndex++)
  {
    if (ispunct(word[i]))
    {
      found = TRUE;
      temp.Format("%s%s", word.Left(i), word.Right((l-i)-1));
      word = temp;
      l--; i--;
    }
    else if (isdigit(word[i]))  // 0-9
    {
      found = TRUE;
      temp.Format("%s%s", word.Left(i), word.Right((l-i)-1));
      word = temp;
      l--; i--;
    }
    else
    {
      if (i<256) 
      {
        m_Prevwords[m_nPrevIndex - 1].nOrigIndices[i] = oIndex;
        m_Prevwords[m_nPrevIndex - 1].nOrigLength++;
      }
    }
  }
  //m_szPrevwords[m_nPrevIndex - 1]=word;
  

  for (s=0; s<m_nSmutListSize; s++)  // for each word in the dictionary
  {
    if (!isDisabledCategory(m_smutList[s].category))
    {
      if (m_smutList[s].active)
      {
        checking = m_smutList[s].word;
        
        dictWordIndex = 0;

        starpos = checking.Find("*");
        if (starpos >= 0)
        {
          CString szLeft, szRight;
          dictWordList[dictWordIndex++] =  (m_smutList[s].word).Left(starpos);
          dictWordList[dictWordIndex++] = (m_smutList[s].word).Right((m_smutList[s].word).GetLength() - starpos - 1);
          starpos = dictWordList[dictWordIndex-1].Find("*");
          while (starpos >= 0)
          {
            CString szTemp; szTemp = dictWordList[dictWordIndex-1];
            dictWordList[dictWordIndex-1] =  (szTemp).Left(starpos);
            dictWordList[dictWordIndex++] = (szTemp).Right(szTemp.GetLength() - starpos - 1);
            starpos = dictWordList[dictWordIndex-1].Find("*");
          }
        }
        else dictWordList[dictWordIndex++] = m_smutList[s].word;

        int z;
        BOOL bFound = TRUE;
        CString out[26];

        // now dictWordList contains the broken down words
        // e.g. fuck*you*harder --> [0]=fuck,[1]=you,[2]=harder, with
        // number of items = dictWordIndex.  If there are no 
        // asterisks, then the list is just that word.

        // now compare the dictWordList with the prevwords array.
        int offset = m_nPrevIndex-dictWordIndex;
        debug = "";
        for (z=0; z<dictWordIndex; z++)
        {
          CString s;
          s.Format("z=%d m_szPrevwords[%d]=%s  dictWordList[%d]=%s\n",
            z, z+offset, m_Prevwords[z+offset].word,
            z, dictWordList[z]); debug += s;
          
          if (!compareWords(m_Prevwords[z+offset].word, dictWordList[z], 
            TRUE, TRUE, &startPos, &(out[z])))
          { bFound = FALSE; break; }
        }

        //if (debug != "") AfxMessageBox(debug);
        
        //why don't you eat my shorts, you camel jockey?

        if (bFound)
        {
          for (*match="", z=0; z<dictWordIndex; z++)
          {
            *match += out[z];
            if (z < (dictWordIndex-1)) *match += " ";
          }
          
          if (m_bHighlightWholeWord)
          {
            // I think God is ���t� you dunecoon -f-u-c-k-h-e-a-d-
            *start = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[0] - ((*match).GetLength() + 1);
            *end = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[m_Prevwords[m_nPrevIndex - 1].nOrigLength - 1];
          }
          else
          {
            *start = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[0] - ((*match).GetLength() + 1);
            *end = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[word.GetLength() - 1];
          }            
          if (m_logfp) 
            fprintf(m_logfp, "Matched %s to dictionary words: %s\n", word,*match);
          return TRUE; 
        }

        // else try the concatenation;
        checking = "";
        for (z=0; z<dictWordIndex; z++)
          checking += dictWordList[z];
              
        
        
#ifdef OLD
        if (starpos >= 0) // if this is a multi-word phrase
        {
          CString first, second, m1, m2;
          
          // break dictionary entry into two words at the star
          first =  (m_smutList[s].word).Left(starpos);
          second = (m_smutList[s].word).Right((m_smutList[s].word).GetLength() - starpos - 1);
          
          if ((compareWords(prevword, first, TRUE, TRUE, &startPos, &m1)) && 
            (compareWords(word, second, TRUE, TRUE, &startPos, &m2)))
          {
            //*match = first + " " + second;
            *match = m1 + " " + m2;
            if (m_bHighlightWholeWord)
            {
              // I think God is ���t� you dunecoon -f-u-c-k-h-e-a-d-
              *start = origIndices[0] - (prevword.GetLength() + 1);
              *end = origIndices[oLength - 1];
            }
            else
            {
              *start = origIndices[0] - (prevword.GetLength() + 1);
              *end = origIndices[word.GetLength() - 1];
            }            
            if (m_logfp) 
              fprintf(m_logfp, "Matched %s to dictionary words: %s\n", word, first + " " + second);
            return TRUE; 
          }
          checking = first + second;
        }
#endif
        
        // beware - the left() should be the total length - the
        // max length of a subtractable suffix; for example,
        // if there is a rule y-ies that converts pussy --> pussies,
        // then the "y" is the subtractable suffix, and its length is one.
         
        n = word.Find(checking.Left(checking.GetLength()-m_nMaxSuffixLength));
        if (n >=0)
        {
          wholeWordOnly=m_smutList[s].wholeWordOnly;  
          
          if (compareWords(word, checking, TRUE, wholeWordOnly, &startPos, match))
          {
            //*match = checking;
            if (m_bHighlightWholeWord)
            {
              //*start = 0; *end = word.GetLength() - 1;
              *start = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[0];
              *end = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[m_Prevwords[m_nPrevIndex - 1].nOrigLength - 1];
              //AfxMessageBox(word);
            }
            else
            {
              *start = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[startPos];
              *end = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[startPos + match->GetLength() - 1];
            }
            if (m_logfp) 
              fprintf(m_logfp, "Matched %s to dictionary word: %s\n", word, checking);
            return TRUE; 
          }
          
          if ((!wholeWordOnly) && ((n = word.Find(checking)) >= 0))
          {
            *match = checking;
            if (m_bHighlightWholeWord)
            {
              //*start = 0; *end = word.GetLength() - 1;
              *start = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[0];
              *end = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[m_Prevwords[m_nPrevIndex - 1].nOrigLength - 1];

            }
            else
            {
              *start = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[n];
              *end = m_Prevwords[m_nPrevIndex - 1].nOrigIndices[n + match->GetLength() - 1];
            }
            if (m_logfp) 
              fprintf(m_logfp, "Matched %s to dictionary word: %s\n", word, checking);
            return TRUE; 
          }
        }
      }
    }
  }
  return FALSE;
}




BOOL CSmutFilter::compareWords(CString wordToCheck, CString dictWord, 
                               BOOL bCheckForms, BOOL wholeWordOnly, 
                               int *start, CString *match)
{
  int form;
  CString wordForm, szWF;
  int m;

  *start = 0;
  *match = dictWord;

  if (wordToCheck.Compare(dictWord) == 0)
    return TRUE;
  
  if (bCheckForms)
  {
    for (form=0; form<m_nWordFormListSize; form++)
    {
      // handle pussy --> pussies
      if ((m_wordForms[form]).Find("-") >= 0) 
      {
        int plusPos = (m_wordForms[form]).Find("-"); 
        CString oldSuffix = m_wordForms[form].Left(plusPos);
        CString newSuffix = m_wordForms[form].Right(((m_wordForms[form]).GetLength()-plusPos) - 1);

        if ( (dictWord).Right(plusPos) ==  oldSuffix)
        {
          wordForm = (dictWord).Left((dictWord).GetLength()-plusPos) + newSuffix;
 
          //CString szFoo; szFoo.Format("newSuffix=%s oldSuffix=%s word=%s rule=%s new=%s",             newSuffix, oldSuffix, dictWord, m_wordForms[form], wordForm); AfxMessageBox(szFoo);

          if (wholeWordOnly)
          {
            if (wordToCheck.Compare(wordForm) == 0)
            {
              *match = wordForm;
              return TRUE; 
            }
          }
          else
          {
            m = wordToCheck.Find(wordForm);
            if (m >= 0)
            { 
              *match = wordForm;
              *start = m;
              return TRUE; 
            }
          }
        }
      }
      else if ((m_wordForms[form]).Find("+") >= 0) // s+s
      {
        // X+Y 
        // if it doesn't end in X, add s
        
        int plusPos = (m_wordForms[form]).Find("+"); 
        CString oldSuffix = m_wordForms[form].Left(plusPos);
        CString newSuffix = m_wordForms[form].Right(((m_wordForms[form]).GetLength()-plusPos) - 1);

        if ( (dictWord).Right(plusPos) !=  oldSuffix)
        {
          wordForm = dictWord + newSuffix;

          if (wholeWordOnly)
          {
            if (wordToCheck.Compare(wordForm) == 0)
            {
              *match = wordForm;
              return TRUE; 
            }
          }
          else
          {
            m = wordToCheck.Find(wordForm);
            if (m >= 0)
            { 
              *match = wordForm;
              *start = m;
              return TRUE; 
            }
          }
        }    
      }
      else //if (n < 0) // standard append of word form
      {
        wordForm = dictWord + m_wordForms[form];   
        //fprintf(m_fp,"Checking '%s' against '%s'; wordForm='%s'\n",wordToCheck,  dictWord, wordForm);
        
        if (wholeWordOnly)
        {
          if (wordToCheck.Compare(wordForm) == 0)
          {
            *match = wordForm;
            return TRUE; 
          }
        }
        else
        {
          int m = wordToCheck.Find(wordForm);
          if (m >= 0)
          {
            *start = m;
            *match = wordForm;
            return TRUE; 
          }
        }
      }
    }
  }
  return FALSE;
}


// 04-02-2001 added sorting on wholeWordOnly
BOOL CSmutFilter::loadDictionary()
{
  char curse[512], buffer[512], temp2[100];
  CString wordForm, temp, curse2;
  //FILE *fp;
  int i, t2i, length;
  CVDIfile vFP;
  BOOL bAtEOF = FALSE;

  m_nSmutListSize=0; 
  m_smutMinLength=999;
  m_nWordFormListSize = 0;
  m_nCategoryListSize = 0;

  //fp = fopen(GSTR(m_szDictionaryName), "rt");
  //if (fp == NULL) return FALSE;

  if (!vFP.OpenForRead(m_szDictionaryName))
    return FALSE;


  while (!bAtEOF) // (!feof(fp))
  {
    //fgets(curse, 512, fp);
    //if (feof(fp))  goto bottom;
    // wordForm.Format("%s", curse);  
    
    wordForm = vFP.ReadLine(&bAtEOF);
    if (bAtEOF) goto bottom;

    wordForm.TrimLeft();  wordForm.TrimRight(); //wordForm.MakeLower();
    strcpy(curse, (LPCTSTR) wordForm);

    if ((wordForm == "") || (curse[0] == '#'))
    {
      // skip this line
      ;
    }
    else if (wordForm[0] == '!')
    {
      if (strncmp(curse, "!wordforms=", 11) == 0)
      {
        // !m_wordForms=s,er,ed,ers,ing
        strcpy(buffer,((LPCTSTR) wordForm)+11);  strcpy(temp2, "");  t2i=0;
        for (i=0;i< (int)strlen(buffer); i++)
        {
          if (buffer[i] == ',')
          {
            m_wordForms[m_nWordFormListSize] = temp2;
            
            // for compareWords() algorithm
            length = m_wordForms[m_nWordFormListSize].Find("-"); // returns -1 if not found
            if (length > m_nMaxSuffixLength) m_nMaxSuffixLength = length;
            length = m_wordForms[m_nWordFormListSize].Find("+"); // returns -1 if not found
            if (length > m_nMaxSuffixLength) m_nMaxSuffixLength = length;

            m_nWordFormListSize++;
            strcpy(temp2, "");  t2i=0;
          }
          else { temp2[t2i++] = buffer[i]; temp2[t2i] =0; }
        }
        if (t2i > 0) m_wordForms[m_nWordFormListSize++] = temp2;
      }
      /*
      else if (strncmp(curse, "!disable=", 9) == 0)
      {
        // !disable=main,children
        strcpy(buffer,((LPCTSTR) wordForm)+9);  strcpy(temp2, "");  t2i=0;
        for (i=0;i< (int)strlen(buffer); i++)
        {
          if (buffer[i] == ',')
          {
            m_DisabledCategoryList[m_nDisabledCategoryListSize++] = temp2;
            strcpy(temp2, "");  t2i=0;
          }
          else { temp2[t2i++] = buffer[i]; temp2[t2i] =0; }
        }
        if (t2i > 0) m_DisabledCategoryList[m_nDisabledCategoryListSize++] = temp2;
      }
      */
      else if (strncmp(curse, "!categories=", 12) == 0)
      {
        // !categories=categoryname,active,main,1,children,0
        BOOL which=FALSE;
        CString szCat;

        strcpy(buffer,((LPCTSTR) wordForm)+12);  strcpy(temp2, "");  t2i=0;
        for (i=0;i< (int)strlen(buffer); i++)
        {
          if (buffer[i] == ',')
          {
            if (!which) { szCat.Format("%s", temp2); which=TRUE; }
            else
            {
              m_categoryList[m_nCategoryListSize].category = szCat;
              m_categoryList[m_nCategoryListSize].active = (BOOL) atol(temp2);
              which=FALSE; m_nCategoryListSize++;
            }
            strcpy(temp2, "");  t2i=0;
          }
          else { temp2[t2i++] = buffer[i]; temp2[t2i] =0; }
        }
        if (t2i > 0) 
        {
          //m_DisabledCategoryList[m_nDisabledCategoryListSize++] = temp2;
          if (!which) { szCat.Format("%s", temp2); which=TRUE; }
          else
          {
            m_categoryList[m_nCategoryListSize].category = szCat;
            m_categoryList[m_nCategoryListSize].active = (BOOL) atol(temp2);
            m_nCategoryListSize++; which=FALSE;
          }
        }
      }
    }
    else // just a dictionary entry
    {
      CString szComment="", tokens[10]; int tokenCount=0, commentPos;
      tokens[tokenCount] = "";

      // strip out comments starting with #
      commentPos = wordForm.Find("#");
      if (commentPos >= 0)
      {
        szComment = wordForm.Right((wordForm.GetLength() - commentPos) - 1);
        szComment.TrimLeft(); szComment.TrimRight();

        wordForm = wordForm.Left(commentPos); wordForm.TrimRight();
      }

      for (i=0; i<wordForm.GetLength(); i++)
      {
        if (wordForm[i] == ',') 
        {
          tokenCount++;
          tokens[tokenCount] = "";
        }
        else tokens[tokenCount] += wordForm[i];
      }
      if (tokens[tokenCount] != "") tokenCount++;

      m_smutList[m_nSmutListSize].word = tokens[0];
      m_smutList[m_nSmutListSize].active =  atol(tokens[1]);
      m_smutList[m_nSmutListSize].wholeWordOnly =  atol(tokens[2]);
      m_smutList[m_nSmutListSize].category = tokens[3];
      m_smutList[m_nSmutListSize].comment = szComment;

      if ((m_smutList[m_nSmutListSize].word).GetLength() < m_smutMinLength)
        m_smutMinLength = (m_smutList[m_nSmutListSize].word).GetLength();
      m_nSmutListSize++;
    }
  }
bottom:
  //temp.Format("loaded %d smut entries, %d word forms", m_smutIndex, m_numWordForms); AfxMessageBox(temp);
  
  //fclose(fp);
  vFP.Close();

  // pick up any extra categories...
  for (i=0; i<m_nSmutListSize; i++)
  {
    if (!categoryExists(m_smutList[i].category))
    {
      m_categoryList[m_nCategoryListSize].category = m_smutList[i].category;
      m_categoryList[m_nCategoryListSize].active = TRUE;
      m_nCategoryListSize++;
    }
  }

  sortDictionary();

  return TRUE;
}

void CSmutFilter::sortDictionary()
{
  int i, j;
  BOOL active, whole;
  CString sortkey_i, sortkey_j, comment, category, word;
  // sort on whole word only...
  for (i=0; i<m_nSmutListSize; i++)
  {
    sortkey_i.Format("%d%s", !(m_smutList[i].wholeWordOnly), m_smutList[i].word);
    for (j=0; j<i; j++)
    {
      sortkey_j.Format("%d%s", !(m_smutList[j].wholeWordOnly), m_smutList[j].word);

      if (sortkey_i.Compare(sortkey_j) < 0)
      {
        word =     m_smutList[i].word;
        active =   m_smutList[i].active;
        whole =    m_smutList[i].wholeWordOnly;
        category = m_smutList[i].category;
        comment =  m_smutList[i].comment;

        m_smutList[i].word = m_smutList[j].word;
        m_smutList[i].active = m_smutList[j].active;
        m_smutList[i].wholeWordOnly = m_smutList[j].wholeWordOnly;
        m_smutList[i].category = m_smutList[j].category;
        m_smutList[i].comment = m_smutList[j].comment;

        m_smutList[j].word = word;
        m_smutList[j].active = active;
        m_smutList[j].wholeWordOnly = whole;
        m_smutList[j].category = category;
        m_smutList[j].comment = comment;
      }
    }    
  }
}


int CSmutFilter::languageFilter(CString *text, int **startEndPairs, int *listSize,
                                BOOL bReplaceCurses, unsigned long *wordsChecked)
{
  int i, start, end, listIndex=0, numCurses=0, m_nPrevIndex=0;
  CString word, onechar, match, m_szPrevwords[26], prevword;
  unsigned char ch;
  int length, last=-1, wordlen, num_punct;
  char replacements[25]; 
  BOOL error = FALSE;
  unsigned long checked=0;

  length = (*text).GetLength();
  //float flength = (float) length;  // used for progress bar
  strcpy(replacements, "!$%&*?");  num_punct=strlen(replacements);

  srand( (unsigned)time( NULL ) );


  word = ""; m_szPrevwords[m_nPrevIndex]=""; prevword="";
  for (i=0; i<=length; i++)
  {
    if (i == length) ch = ' ';
    else ch = (*text)[i];

    /* set the percentage
    gnProgressPercent=(int) ((i*100.0)/flength);
    if (gnProgressPercent != last)
    {
      Status(gzProgressString,ST_PROGRESS|gnProgressFlags,gnProgressPercent);
      last = gnProgressPercent;
    }
    */

    if (((isspace(ch)) ||(ch == '.') ||(ch == ',') || (i==length)) && 
      (word.GetLength() > 0))
    {
      //word.TrimLeft(); word.TrimRight();

      /* strip quotes...
      for (i=0; i<word.GetLength(); i++)
      {
        if ((word[i] == '\'') || (word[i] == '\"'))
        {
          word = word.Left(i) + word.Right((word.GetLength()-i)-1);
          //i--;
        }
      }

      //while ((wordlen>0) && ((word.Left(1) == "\'")  || (word.Left(1) == "\"")))      { CString szTemp = word; word = word.Right(word.GetLength()-1); wordlen = word.GetLength(); AfxMessageBox("left-was '"+szTemp+"' is '"+word);}
      //while ((wordlen>0) && ((word.Right(1) == "\'") || (word.Right(1) == "\"")))      { CString szTemp = word; word = word.Left(word.GetLength()-1); wordlen = word.GetLength(); AfxMessageBox("rightwas '"+szTemp+"' is '"+word);}
      */
      wordlen = word.GetLength();

      checked++;

      if (checkWord(word, &match, &start, &end))
      {
        //fprintf(m_fp, "%s (matched word: %s, [%d, %d]\n", word, match, start, end); 

        // add indices to list
        if (listIndex+2 >= (*listSize))
        {
          int *newm;
          // realloc the list
          newm = (int *) realloc(*startEndPairs, ((*listSize) + 100) * sizeof(int));
          if (newm != NULL)
          {
            *startEndPairs = newm;
            (*listSize) += 100;
            error=FALSE;
          }
          else 
          {
            error = TRUE;
            AfxMessageBox("Out of memory reallocating list of start-end pairs.");
          }
        }
        if (!error)
        {
          (*startEndPairs)[listIndex] = start + (i-wordlen);
          if ((*startEndPairs)[listIndex] < 0) (*startEndPairs)[listIndex] = 0;
          if ((*startEndPairs)[listIndex] >= length) (*startEndPairs)[listIndex] = length-1;
          listIndex++;
          
          (*startEndPairs)[listIndex] = end + (i-wordlen);
          if ((*startEndPairs)[listIndex] < 0) (*startEndPairs)[listIndex] = 0;
          if ((*startEndPairs)[listIndex] >= length) (*startEndPairs)[listIndex] = length-1;
          listIndex++;

          numCurses++;
        }

        if (0)//(bReplaceCurses)  // replace curses with punctuation
        {
          int q, rn, rnlast=-2;
          rn = (int)((rand()/(double)RAND_MAX)*num_punct + 0.5);
          if (rn <0) rn=0; if (rn > (num_punct-1)) rn=(num_punct-1);

          for (q=(*startEndPairs)[listIndex-2]; q<=(*startEndPairs)[listIndex-1]; q++)
          {
            while (rn == rnlast)  
            {
              rn = (int)((rand()/(double)RAND_MAX)*num_punct + 0.5);
              if (rn <0) rn=0; if (rn > (num_punct-1)) rn=(num_punct-1);
            }
            (*text).SetAt(q, replacements[rn]);
            rnlast = rn;
          }
        }

        // playing around now, try replacing only vowels with asterisks
        // so we can see what the word really is...
        if (bReplaceCurses)
        {
          int q;
          for (q=(*startEndPairs)[listIndex-2]; q<=(*startEndPairs)[listIndex-1]; q++)
          {
            (*text).SetAt(q, '*');
          }
        }


        // playing around now, try replacing only vowels with asterisks
        // so we can see what the word really is...
        if (0)//(bReplaceCurses)
        {
          int q;
          for (q=(*startEndPairs)[listIndex-2]; q<=(*startEndPairs)[listIndex-1]; q++)
          {
            if ( ((*text)[q] == 'a') || ((*text)[q] == 'e') || ((*text)[q] == 'i') || ((*text)[q] == 'o') || ((*text)[q] == 'u'))
              (*text).SetAt(q, '*');
          }
        }
      }

      word = "";

      //prevword=word;  
      // the following fixes "rim job" - since prevword was set to 
      // ("rim) the phrase ("rim job") never matched because ("rim) != (rim)
      /*for (prevword="", q=0; q<word.GetLength(); q++)
      {
        if (!ispunct(word[q])) 
          prevword += (char) (word[q]);
      }
      if (m_nPrevIndex <24) 
      {
        m_szPrevwords[m_nPrevIndex] = prevword;
        m_szPrevwords[m_nPrevIndex].MakeLower();
        m_szPrevwords[m_nPrevIndex] = stringFilter(m_szPrevwords[m_nPrevIndex]);
        m_nPrevIndex++;
      }
      else
      {
        for (int z=0; z<24; z++) m_szPrevwords[z] = m_szPrevwords[z+1];
        m_szPrevwords[24] = prevword;
        m_szPrevwords[24].MakeLower();
        m_szPrevwords[24] = stringFilter(m_szPrevwords[24]);
      }
      */
    }
    else if (i < length)
    {
      if (!isspace(ch))
      {
        onechar.Format("%c", ch);
        word += onechar;
      }
    }
  }
  
  // gzProgressString=_T("");  
  *wordsChecked = checked;
  return numCurses;
}

int CSmutFilter::getDictionarySize()
{
  return m_nSmutListSize;
}

int CSmutFilter::getCategorySize()
{
  return m_nCategoryListSize;
}

int CSmutFilter::addWord(CString szWord, BOOL bActive, BOOL bWhole, CString szCategory, CString szComment)
{
  int i=0, j;
  CString szTemp;

  szWord.MakeLower();
  
  szTemp.Format("Added word: %s, active=%d whole=%d cat=%s comment=%s", szWord, bActive, bWhole, szCategory, szComment);
  addChanges(szTemp);


  // add it
  for (i=0; i<m_nSmutListSize; i++)
  {
    if (m_smutList[i].word > szWord)
      break;
  }
  // insert at i;
  for (j=m_nSmutListSize; j>i; j--)
  {
    m_smutList[j].word = m_smutList[j-1].word;
    m_smutList[j].active = m_smutList[j-1].active;
    m_smutList[j].wholeWordOnly = m_smutList[j-1].wholeWordOnly;
    m_smutList[j].category = m_smutList[j-1].category;
    m_smutList[j].comment = m_smutList[j-1].comment;
  }
  
  m_smutList[i].word = szWord;
  m_smutList[i].active = bActive;
  m_smutList[i].wholeWordOnly = bWhole;
  m_smutList[i].category = szCategory;
  m_smutList[i].comment = szComment;

  addCategory(szCategory, TRUE);
  
  m_nSmutListSize++;

  sortDictionary();
  return i;
}


BOOL CSmutFilter::editWord(int nPosition, CString szWord, BOOL bActive, 
                           BOOL bWhole, CString szCategory, CString szComment)
{
  if ((nPosition >= m_nSmutListSize) || (nPosition < 0)) return FALSE;

  CString szTemp;
  szTemp.Format("Edited word: %s to (%s, active=%d whole=%d cat=%s comment=%s)" ,
    m_smutList[nPosition].word, szWord, bActive, bWhole, szCategory,szComment);
  addChanges(szTemp);

  m_smutList[nPosition].word = szWord;
  m_smutList[nPosition].active = bActive;
  m_smutList[nPosition].wholeWordOnly = bWhole;
  m_smutList[nPosition].category = szCategory;
  m_smutList[nPosition].comment = szComment;
  return TRUE;
}


BOOL CSmutFilter::getWord(int nPosition, CString *szWord, BOOL *bActive, 
                          BOOL *bWhole, CString *szCategory, CString *szComment)
{
  if ((nPosition >= m_nSmutListSize) || (nPosition < 0)) return FALSE;

  *szWord = m_smutList[nPosition].word;
  *bActive = m_smutList[nPosition].active;
  *bWhole = m_smutList[nPosition].wholeWordOnly;
  *szCategory = m_smutList[nPosition].category;
  *szComment = m_smutList[nPosition].comment;
  return TRUE;
}



BOOL CSmutFilter::deleteWord(int nItem)
{
  int i;
  if ((nItem >= m_nSmutListSize) || (nItem < 0)) return FALSE;

  addChanges("Deleted word: " + m_smutList[nItem].word);

  for (i=nItem; i<m_nSmutListSize; i++)
  {
    m_smutList[i].word = m_smutList[i+1].word;
    m_smutList[i].active = m_smutList[i+1].active;
    m_smutList[i].wholeWordOnly = m_smutList[i+1].wholeWordOnly;
    m_smutList[i].category = m_smutList[i+1].category;
    m_smutList[i].comment = m_smutList[i+1].comment;
  }
  m_nSmutListSize--;
  return TRUE;
}  



BOOL CSmutFilter::isDisabledCategory(CString szCategory)
{
  int i;
  for (i=0; i<m_nCategoryListSize; i++)
    if (szCategory == m_categoryList[i].category)
      return !(m_categoryList[i].active);
  return FALSE;
}


BOOL CSmutFilter::categoryExists(CString szCategory)
{
  int i;
  for (i=0; i<m_nCategoryListSize; i++)
    if (szCategory == m_categoryList[i].category)
      return TRUE;
  return FALSE;
}

BOOL CSmutFilter::getCategory(int n, CString *szCategory, BOOL *bActive)
{ 
  if (n >= m_nCategoryListSize) return FALSE;
  *szCategory = m_categoryList[n].category;
  *bActive = m_categoryList[n].active;
  return TRUE;
}

/*
// Gets a list of categories, and returns the number of categories.
int CSmutFilter::getCategoryList(CString szCategoryList[MAX_CATEGORIES])
{ 
  int i, j, nCount=0;
  BOOL bFound;

  szCategoryList[nCount] = "";
  for (i=0; i<m_nSmutListSize; i++)
  {
    bFound = FALSE;
    for (j=0; j<nCount; j++)
    {
      if (m_smutList[i].category == szCategoryList[j])
      {
        bFound = TRUE;
        break;
      }
    }
    if (!bFound)
    {
      szCategoryList[nCount] = m_smutList[i].category;
      nCount++;
    }
  }
  return nCount;
}

int CSmutFilter::getDisabledCategoryList(CString szCategoryList[MAX_CATEGORIES])
{ 
  int i, j, nCount=0;
  BOOL bFound;

  szCategoryList[nCount] = "";
  for (i=0; i<m_nSmutListSize; i++)
  {
    bFound = FALSE;
    for (j=0; j<nCount; j++)
    {
      if (m_smutList[i].category == szCategoryList[j])
      {
        bFound = TRUE;
        break;
      }
    }
    if (!bFound)
    {
      if (isDisabledCategory(m_smutList[i].category))
      {
        szCategoryList[nCount] = m_smutList[i].category;
        nCount++;
      }
    }
  }
  return nCount;
}
*/

BOOL CSmutFilter::enableCategory(CString szCategory)
{
  int i;
  for (i=0; i<m_nCategoryListSize; i++)
  {
    if (m_categoryList[i].category == szCategory)
    {
      m_categoryList[i].active = TRUE;
      addChanges("Enabled category: " + szCategory);
      break;
    }
  }
  return TRUE;
}


BOOL CSmutFilter::setActive(int nPosition, BOOL bActive)
{
  if ((nPosition >= m_nSmutListSize) || (nPosition < 0)) return FALSE;
  m_smutList[nPosition].active = bActive;
  return TRUE;
}


BOOL CSmutFilter::setCategory(int nPosition, CString szCategory)
{
  if ((nPosition >= m_nSmutListSize) || (nPosition < 0)) return FALSE;
  m_smutList[nPosition].category = szCategory;
  return TRUE;
}


BOOL CSmutFilter::setWholeWordOnly(int nPosition, BOOL bWholeWordOnly)
{
  if ((nPosition >= m_nSmutListSize) || (nPosition < 0)) return FALSE;
  m_smutList[nPosition].wholeWordOnly = bWholeWordOnly;
  return TRUE;
}


BOOL CSmutFilter::disableCategory(CString szCategory)
{
  BOOL bFound = FALSE;
  int i;

  for (i=0; i<m_nCategoryListSize; i++)
  {
    if (m_categoryList[i].category == szCategory)
    {
      m_categoryList[i].active = FALSE;
      addChanges("Disabled category: " + szCategory);
      break;
    }
  }
  return TRUE;
}


BOOL CSmutFilter::addCategory(CString szCategory, BOOL bActive)
{
  BOOL bFound = FALSE;
  CString szCategoryList[MAX_CATEGORIES];
  int i;

  for (i=0; i<m_nCategoryListSize; i++)
  {
    if (m_categoryList[i].category == szCategory)
    {
      bFound = TRUE;
      break;
    }
  }
  if (!bFound)
  {
    m_categoryList[m_nCategoryListSize].category = szCategory;
    m_categoryList[m_nCategoryListSize].active = bActive;
    m_nCategoryListSize++;
    addChanges("Added category: " + szCategory);
    return TRUE;
  }
  //else AfxMessageBox("Category '"+szCategory+"' already exists!");
  return FALSE;
}


int CSmutFilter::countWordsInCategory(CString szCategory)
{
  int i, count=0;
  for (i=0; i<m_nSmutListSize; i++)
  {
    if (m_smutList[i].category == szCategory) count++;
  }
  return count;
}



// validate the dictionary.  Return error messages if problem found
BOOL CSmutFilter::validateDictionary(CString *szErrorMsg)
{
  int i,j, starcount;

  for (i=0; i<m_nSmutListSize; i++)
  {
    for (j=0, starcount=0; j<m_smutList[i].word.GetLength(); j++)
    {
      if (m_smutList[i].word[j] == '*') starcount++;
    }
    if (starcount > 24)
    {
      szErrorMsg->Format("Cannot save dictionary: too many asterisks in %s", m_smutList[i].word);
      return FALSE;
    }
  }
  return TRUE;
}


BOOL CSmutFilter::saveDictionary()
{
  CVDIfile vFP;
  //FILE *fp;
  int i, nMaxLength=0;
  CTime theTime = CTime::GetCurrentTime();
  CString szTemp, szList;

  sortDictionary();
  
  //fp = fopen(m_szDictionaryName, "wt");
  //if (fp == NULL) return FALSE;
    
  if (!vFP.OpenForWrite(m_szDictionaryName)) return FALSE;

  
  vFP.WriteString("# This is the curse file for the VDI Smut Filter.\n");
  vFP.WriteString("#\n");
  vFP.WriteString("# Format:  word, active flag (0=not active, 1=active), whole-word-only\n");
  vFP.WriteString("# flag (0=substring search, 1=no substring search).  For example,\n");
  vFP.WriteString("#    \"fuck,0,0\" won't match anything (not active)  \n");
  vFP.WriteString("#    \"fuck,0,1\" won't match anything (not active)  \n");
  vFP.WriteString("#    \"fuck,1,0\" matches \"fuck\",\"apefucker\", \"xxxFUCKxxx\" etc. (not whole word only)  \n");
  vFP.WriteString("#    \"fuck,1,1\" matches \"fuck\" (whole word only)  \n");
  vFP.WriteString("#\n");
  vFP.WriteString("# Lines beginning with ! are special directives; for example:\n");
  vFP.WriteString("#\n");
  vFP.WriteString("# !wordforms tells the parser all the possible language-local wordforms\n");
  vFP.WriteString("#      a word form XX-YYY  says replace a trailing XX with YYY\n");
  vFP.WriteString("#   For example: the word \"bag\" in the dictionary below will be tested as:\n");
  vFP.WriteString("#      bags, bager, baged, baging, bagged (g-gged), bagging (g-gging)\n");
  vFP.WriteString("#   \"pussy\" would be tested against \"pussies\" (y-ies rule).\n");
  vFP.WriteString("# Obviously, some of these are not real words, but tests on real world\n");
  vFP.WriteString("# files have turned up few mismatches...\n");
  vFP.WriteString("#\n");
  vFP.WriteString("# !categories=main,1,childrens,1\n");
  vFP.WriteString("# Is used to store the categories.\n");
  vFP.WriteString("#\n");
  vFP.WriteString("# A single '*' character can be used to break up a dictionary entry\n");
  vFP.WriteString("# into 2 words.  For example, \"rim*job\" will match \"rimjob\" or \"rim job\"\n");
  vFP.WriteString("# or \"rim-job\" or \"rim@job\" etc.  Only one asterisk is allowed per line!\n");
  vFP.WriteString("#\n");
  vFP.WriteString("# The smut filter and its assocated data files are the intellectual property\n");
  vFP.WriteString("# of Video Design Interactive.  Disassembly, dissemination, or copying of\n");
  vFP.WriteString("# these files or their derivatives  without the written consent of Video\n");
  vFP.WriteString("# Design are forbidden.  (c) 2000,2001  VDI.  All Rights Reserved.\n");
  vFP.WriteString("#\n");
  vFP.WriteString("# Version history:\n");
  vFP.WriteString("#   Version 1.0    May 31, 2000       GWR     (157 words)\n");
  vFP.WriteString("#   Version 3.1    July 12, 2000      GWR     (170 words)\n");
  vFP.WriteString("#   Version 4.0    January 24, 2001   Noggin  (230 words)\n");
  vFP.WriteString("#   Version 4.1    January 25, 2001   GWR     (312 words)\n");
  vFP.WriteString("#   Version 4.2    January 28, 2001   GWR     (307 words, added categories)\n");
  vFP.WriteString("#   Version 4.5    January 29, 2001 (15:55:48) (From dictionary editor, 309 words)\n");
  vFP.WriteString("#   Version 4.6    February 27, 2001  GWR      (Added category storage)\n");
  vFP.WriteString("#   Version 4.7    April 02, 2001 (16:07:52) (From dictionary editor, 317 words)\n");
  vFP.WriteString("#   Version 4.8    November 13, 2001  GWR     Added some words from peoplelink (334 words)\n");
  vFP.WriteString("#   Version 4.9    April 29, 2002 GWR  (Added some words from Tom Dillof, 348 words)\n");
  vFP.WriteString("#   Version 5.0    %s (From dictionary editor, %d words)\n\n", 
    theTime.Format("%B %d, %Y (%H:%M:%S)"), m_nSmutListSize);

  /*
  if (m_nDisabledCategoryListSize > 0)
  {
    szList = "";
    for (i=0; i<m_nDisabledCategoryListSize; i++)
    {
      if (i>0) szList +=",";
      szList += m_DisabledCategoryList[i];
    }
    vFP.WriteString("\n!disabled=%s\n", szList);
  }
  */

  if (m_nCategoryListSize > 0)
  {
    CString szTemp;
    szList = "";
    for (i=0; i<m_nCategoryListSize; i++)
    {
      szTemp.Format("%s,%d", m_categoryList[i].category, m_categoryList[i].active);
      if (i>0) szList +=",";
      szList += szTemp;
    }
    vFP.WriteString("!categories=%s\n", szList);
  }

  if (m_nWordFormListSize > 0)
  {
    szList = "";
    for (i=0; i<m_nWordFormListSize; i++)
    {
      if (i>0) szList +=",";
      szList += m_wordForms[i];
    }
    vFP.WriteString("!wordforms=%s\n", szList);
  }

  vFP.WriteString("\n");

  // find maximum spaces....
  for (i=0; i<m_nSmutListSize; i++)
  {
    szTemp.Format("%s,%d,%d,%s", m_smutList[i].word, m_smutList[i].active, m_smutList[i].wholeWordOnly, m_smutList[i].category);
    if (szTemp.GetLength() > nMaxLength)
       nMaxLength = szTemp.GetLength();
  }
  nMaxLength+=10;

  for (i=0; i<m_nSmutListSize; i++)
  {
    if (m_smutList[i].comment == "")
    {
      vFP.WriteString("%s,%d,%d,%s\n", m_smutList[i].word,
        m_smutList[i].active,
        m_smutList[i].wholeWordOnly,
        m_smutList[i].category);
    }
    else
    {
      CString szSpaces="";
      int j;
      szTemp.Format("%s,%d,%d,%s", m_smutList[i].word, m_smutList[i].active, m_smutList[i].wholeWordOnly, m_smutList[i].category);
      for (j=0; j<nMaxLength - szTemp.GetLength(); j++) szSpaces+=" ";
      vFP.WriteString("%s%s# %s\n", szTemp,szSpaces,m_smutList[i].comment);
    }
  }
  //fclose(fp);
  vFP.Close();
  return TRUE;
}



// generate a list of word forms...
CString CSmutFilter::generateWordForms(CString szWord)
{
  int n, form;
  CString szTemp, szList = "";
  BOOL bFirst=TRUE;

  for (form=0; form<m_nWordFormListSize; form++)
  {
    n = (m_wordForms[form]).Find("-");  // handle pussy --> pussies
    if ((n >= 0) && ((szWord).Right(n) == (m_wordForms[form]).Left(n)))
    {
      szTemp = (szWord).Left((szWord).GetLength()-(n)) + 
        (m_wordForms[form]).Right((m_wordForms[form]).GetLength()-(n+1));
      if (bFirst) bFirst=FALSE; else szList+=", ";
      szList+=szTemp;
    }
    else if ((m_wordForms[form]).Find("+") >= 0) // s+s
    {
      // X+Y 
      // if it doesn't end in X, add s
      int plusPos = (m_wordForms[form]).Find("+"); 
      CString oldSuffix = m_wordForms[form].Left(plusPos);
      CString newSuffix = m_wordForms[form].Right(((m_wordForms[form]).GetLength()-plusPos) - 1);

      if ( (szWord).Right(plusPos) !=  oldSuffix)
      {
        szTemp = szWord + newSuffix;
        if (bFirst) bFirst=FALSE; else szList+=", ";
        szList+=szTemp;
        //CString szFoo; szFoo.Format("newSuffix=%s oldSuffix=%s word=%s rule=%s new=%s", newSuffix, oldSuffix, szWord, m_wordForms[form], szTemp); AfxMessageBox(szFoo);
      }
    }
    else if (n < 0) // standard append of word form
    {
      szTemp = szWord + m_wordForms[form];   
      if (bFirst) bFirst=FALSE; else szList+=", ";
      szList+=szTemp;
    }
  }
  return szList;
}



// generate a list of word forms...
int CSmutFilter::generateWordFormsList(CString szWord, CString szWordList[])
{
  int form, n, count;
  CString szTemp, szList = "";

  count = 0;

  szWordList[count++]=szWord;

  for (form=0; form<m_nWordFormListSize; form++)
  {
    n = (m_wordForms[form]).Find("-");  // handle pussy --> pussies
    if ((n >= 0) && ((szWord).Right(n) == (m_wordForms[form]).Left(n)))
    {
      szTemp = (szWord).Left((szWord).GetLength()-(n)) + 
        (m_wordForms[form]).Right((m_wordForms[form]).GetLength()-(n+1));
      szWordList[count++]=szTemp;
    }
    else if ((m_wordForms[form]).Find("+") >= 0) // s+s
    {
      // X+Y 
      // if it doesn't end in X, add s
      int plusPos = (m_wordForms[form]).Find("+"); 
      CString oldSuffix = m_wordForms[form].Left(plusPos);
      CString newSuffix = m_wordForms[form].Right(((m_wordForms[form]).GetLength()-plusPos) - 1);

      if ( (szWord).Right(plusPos) !=  oldSuffix)
      {
        szTemp = szWord + newSuffix;
        szWordList[count++]=szTemp;
        //CString szFoo; szFoo.Format("word=%s rule=%s new=%s", szWord, m_wordForms[form], szTemp); AfxMessageBox(szFoo);
      }
    }
    else if (n < 0) // standard append of word form
    {
      szTemp = szWord + m_wordForms[form];   
      szWordList[count++]=szTemp;
    }
  }
  return count;
}



// removes accented characters, such as:
// f�ck, f�ck, f�ck, f�ck, sh�t, sh�t, sh�t, sh�t, d�mn, d�mn, d�mn, d�mn,
// d�mn, d�mn, etc? and for instance: ���t�.  and what about the ever-present
// "fuhque"?
// Also, I noticed that Jesus gets found, but not god, God, or Christ.  What
// about that, buddy. "I think God is ���t�" just sails right through!
// (And I noticed, "She gave him head" sails through too - though those are
// all normal words by themselves. what to do what to do...)
CString CSmutFilter::stringFilter(CString in) 
{
  int i, nc=in.GetLength(), nf=0;
  unsigned char ch;
  CString out = in;

  for (i=0; i<in.GetLength(); i++)
  {
    ch = (unsigned char) in[i];

    switch (ch)
    {
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'A'); break;
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'C'); break;
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'E'); break;
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'Y'); break;
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'I'); break;
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'D'); break;
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'N'); break;
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'N'); break;
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'U'); break;
    case ((unsigned char) '�'): 
      nf++; out.SetAt(i, 'p'); break;
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'B'); break;
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'a'); break;
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'c'); break;
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'e'); break;
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'i'); break;
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'd'); break;
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'n'); break;
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'o'); break;
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'u'); break;
    case ((unsigned char) '�'):
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'y'); break;
    case ((unsigned char) '�'):
      nf++; out.SetAt(i, 'p'); break;
    }
  }

  if (nf)
  {
    out.MakeLower();
  
    CString foo;
    foo.Format("In: %s Out: %s  total=%d fixed=%d", in, out, nc, nf);
    //AfxMessageBox(foo);
  }
  return out;
}


void CSmutFilter::addChanges(CString change)
{
  m_szChanges += change;
  m_szChanges += "\n";
  m_bChanged = TRUE;
}





/*****************************************************************/
/* Sample calling routine:

void CSmutTestDlg::OnButtonCheck() 
{
  int i, *startEndPairs=NULL, listSize=0, numCurses;
  CString bufferToCheck, results;
  CSmutFilter cVar;
  BOOL bReplaceCurses;

  GetDlgItem(IDC_EDIT_BOX)->GetWindowText(bufferToCheck);
  bReplaceCurses = ((CButton *)GetDlgItem(IDC_CHECK_REPLACE))->GetCheck();

  numCurses = cVar.languageFilter(&bufferToCheck, &startEndPairs, &listSize, bReplaceCurses);

  if (numCurses == 0) results.Format("0 Curses Found.");
  else
  {
    results.Format("%d Curses Found, at:\n", numCurses);
    for (i=0; i<numCurses*2; i+=2)
    {
      CString szTemp;
      szTemp.Format("[%d,%d], ", startEndPairs[i], startEndPairs[i+1]); 
      results += szTemp;
    }
  }
  GetDlgItem(IDC_STATIC_RESULTS)->SetWindowText(results);

  GetDlgItem(IDC_EDIT_BOX)->SetWindowText(bufferToCheck);

}
*/