# This is the curse file for the VDI Smut Filter.
#
# Format:  word, active flag (0=not active, 1=active), whole-word-only
# flag (0=substring search, 1=no substring search).  For example,
#    "fuck,0,0" won't match anything (not active)  
#    "fuck,0,1" won't match anything (not active)  
#    "fuck,1,0" matches "fuck","apefucker", "xxxFUCKxxx" etc. (not whole word only)  
#    "fuck,1,1" matches "fuck" (whole word only)  
#
# Lines beginning with ! are special directives; for example:
#
# !wordforms tells the parser all the possible language-local wordforms
#      a word form XX-YYY  says replace a trailing XX with YYY
#   For example: the word "bag" in the dictionary below will be tested as:
#      bags, bager, baged, baging, bagged (g-gged), bagging (g-gging)
#   "pussy" would be tested against "pussies" (y-ies rule).
# Obviously, some of these are not real words, but tests on real world
# files have turned up few mismatches...
#
# !categories=main,1,childrens,1
# Is used to store the categories.
#
# A single '*' character can be used to break up a dictionary entry
# into 2 words.  For example, "rim*job" will match "rimjob" or "rim job"
# or "rim-job" or "rim@job" etc.  Only one asterisk is allowed per line!
#
# The smut filter and its assocated data files are the intellectual property
# of Video Design Interactive.  Disassembly, dissemination, or copying of
# these files or their derivatives  without the written consent of Video
# Design are forbidden.  (c) 2000,2001  VDI.  All Rights Reserved.
#
# Version history:
#   Version 1.0    May 31, 2000       GWR     (157 words)
#   Version 3.1    July 12, 2000      GWR     (170 words)
#   Version 4.0    January 24, 2001   Noggin  (230 words)
#   Version 4.1    January 25, 2001   GWR     (312 words)
#   Version 4.2    January 28, 2001   GWR     (307 words, added categories)
#   Version 4.5    January 29, 2001 (15:55:48) (From dictionary editor, 309 words)
#   Version 4.6    February 27, 2001  GWR      (Added category storage)
#   Version 4.7    April 02, 2001 (16:07:52) (From dictionary editor, 317 words)
#   Version 4.8    November 13, 2001  GWR     Added some words from peoplelink (334 words)
#   Version 4.9    November 13, 2001 (20:05:30) (From dictionary editor, 334 words)
#   Version 5.0    April 29, 2002  (15:19:00) Added some words from Tom Dillof (348 words)
#   Version 5.1      December 11, 2002

!categories=main,1,children,1,medical,1,slur,1
!wordforms=s+s,e+ers,e+ed,e+er,e+ing,e-ing,y-ies,e-ers,e-er,e-ed,g-gged,g-gging,s-ses

anal,1,1,main
anally,1,1,main
anus,1,1,main
arabian*goggle,1,1,main          # putting your hairy nut sack over a girl's eyes so it is like goggles 
arse,1,1,main                     # British-ass
ass,1,1,main
ass*hole,1,1,main
ass*lick,1,1,main
ass*master,1,1,main
ass*munch,1,1,main
asswipe,1,1,main
ball,1,1,main
ballsy,1,1,main
bang,1,1,main
bare,1,1,main
bastard,1,1,main
beaver,1,1,main
beef*curtain,1,1,main             # British - labia
bell*end,1,1,main                 # British - tip of the penis
bestiality,1,1,main
biatch,1,1,main                   # slang for bitch
bimbo,1,1,main
bitchin,1,1,main
bite*me,1,1,main
blow,1,1,main
blow*job,1,1,main
bollicks,1,1,main                 # British - stupid,silly
bollocks,1,1,main                 # British - stupid,silly
bondage,1,1,main
boner,1,1,main
bong,1,1,main
boob,1,1,main
booger,1,1,main
breast,1,1,main
broad,1,1,main
bugger,1,1,main                   # British - to have anal sex
bull,1,1,main
bull*shit,1,1,main
bush,1,1,main
butt,0,1,main
camel*jockey,1,1,slur             # slur against Arab people
camel*toe,1,1,main                # "camel toes" - a vagina as seen through a tight pair of jeans or pants.
carpet*munch,1,1,main             # a carpet muncher is a lesbian
cat*house,1,1,main                # whorehouse
chicken*head,1,1,main             # inner-city slang for a promiscuous girl
chicken*shit,1,1,main
chink,1,1,slur                    # slur against Chinese people
choad,1,1,main                    # penis
choada,1,1,main                   # penis
chode,1,1,main                    # a penis that is bigger in circumference than it is in length
christ,1,1,main                   # Religious
circle*jerk,1,1,main              # Mutual masturbation in a circle
clit,1,1,main                     # Clitoris
clitoris,1,1,main
cock,1,1,main                     # Penis
cock*smoker,1,1,main              # Homosexual
cock*suck,1,1,main
cojones,1,1,main                  # balls
condom,1,1,main                   # If you don't know...
coon,1,1,slur                     # slur against black people
cooze,1,1,main                    # Vagina
corn*hole,1,1,main                # to have anal sex with
cotton*picker,1,1,slur            # slur against black people
crabs,1,1,main
crap,1,1,main
crapper,1,1,main
crotch,1,1,main
cum,1,1,main
cunnilingus,1,1,main
damn,1,1,main
darkie,1,1,slur                   # slur against black people
darky,1,1,slur                    # slur against black people
diarhhea,1,1,medical
dick,1,1,main
dick*lick,1,1,main
dick*wad,1,1,main
dick*weed,1,1,main
diddle,1,1,main
dike,1,1,main                     # slur agains lesbians, misspelled
dildo,1,1,main
dingleberry,1,1,main
dink,1,1,slur                     # slur against Asian people
dong,1,1,main
doo*doo,1,1,children
doodie,1,1,children
doody,1,1,children
dork,1,1,main
dot*head,1,1,slur                 # slur against Indian people
douche,1,1,medical
dumb*ass,1,1,main
dune*coon,1,1,slur                # slur against Arab people
dyke,1,1,slur                     # lesbian
erect,1,1,main
erotic,1,1,main
fag,1,1,slur
faggit,1,1,slur
faggot,1,1,slur
faggy,1,1,slur
fagy,1,1,slur
fart,1,1,main
fart*knocker,1,1,main
fat*ass,1,1,main
felch,1,1,main                    # If you don't know, I'm not telling...
fellatio,1,1,main
feltch,1,1,main                   # If you don't know, I'm not telling...
fetish,1,1,main
fingered,1,1,main
fingering,1,1,main
fist,1,1,main
fisting,1,1,main
flesh,1,1,main
foreskin,1,1,main
freak,1,1,main
french*tickler,1,1,main           # marital aid
fro*nip,1,1,main		  # hairy nipples (fro nips)
frottage,1,1,main
fuckhead,1,1,main
fuckwad,1,1,main
fuckwit,1,1,main
fugly,1,1,main
fur*burger,1,1,main               # vagina
fur*trader,1,1,main               # lesbian
gay,0,1,slur
genitalia,1,1,main
giz,1,1,main                      # slang for semen, misspelled
gizz,1,1,main                     # slang for semen, misspelled
gland,1,1,main
go*down,1,1,main
god,1,1,main                      # Religious
god*damn,1,1,main
golden*shower,1,1,main
gook,1,1,slur                     # slur against Asian (Vietnamese?) people
goy,1,1,slur                      # Jewish slur against non-Jews
greaser,1,1,slur                  # slur against Italian people
groe,1,1,main                     # A pejorative term for African-Americans, or Negroes. Used extensively in New England (USA). Example: "You drive like a Groe."
grostulation,1,1,main             # the contractions of the rectum during anal sex from the English "gross" meaning disgusting or vile!
grundle,1,1,main                   # it's the taint.
guinea,1,1,slur                   # slur against Italian people
gum*job,1,1,main
gummer,1,1,main
gyz,1,1,main                      # slang for semen, misspelled
gyzm,1,1,main                     # slang for semen, misspelled
hair*pie,1,1,main                 # vagina
half*assed,1,1,main
hand*job,1,1,main
hard*on,1,1,main
hardcore,1,1,main
heeb,1,1,slur                     # slur against Jewish people
hell,1,1,main
hershey*highway,1,1,main
homo,1,1,slur
honkey,1,1,slur                   # black slang for white people
honky,1,1,slur                    # black slang for white people
hooker,1,1,main
hooters,1,1,main
horny,1,1,main
hum*job,1,1,main
hummer,1,1,main
hump,1,1,main
idiot,1,1,main
incest,1,1,main
jack*ass,1,1,main
jack*off,1,1,main
jack*shit,1,1,main
jacking,1,1,main                  # Masturbation
jap,1,1,slur
jerk,1,1,main
jerk*off,1,1,main
jesus,1,1,main                    # Religious
jigaboo,1,1,slur                  # slur against black people
jism,1,1,main
jiz,1,1,main
jizm,1,1,main
jugs,1,1,main
jungle*bunny,1,1,slur             # slur against black people
jungle*fever,1,1,main
kaffir,1,1,slur                   # South African slur against black people
kike,1,1,slur                     # slur against Jewish people
knob,1,1,main			  # penis
knock*up,1,1,main                 # Pregnant
knocked*up,1,1,main               # Pregnant
knockers,1,1,main
lesbian,1,1,main                  # Lesbian
lesbo,1,1,slur                    # Lesbian
lez,1,1,slur                      # Lesbian
lezbo,1,1,slur                    # Lesbian
lezzie,1,1,slur                   # Lesbian
lezzy,1,1,slur                    # Lesbian
lick,1,1,main
limey,1,1,slur                    # slur against British people?
love*juice,1,1,main               # semen
masterbate,1,1,main
masturbate,1,1,main
masturbation,1,1,main
masturbator,1,1,main
meat*wallet,1,1,main              # vagina
mick,1,1,slur                     # slur against Irish people
mister*charlie,1,1,slur           # slur against Asian (Chinese) people
mockie,1,1,slur                   # slur against Jewish people
mofo,1,1,main                     # motherfucker
morning*wood,1,1,main
mother*fuck,1,1,main
muff,1,1,main
muff*diver,1,1,main
muff*diving,1,1,main
muther*fuck,1,1,main
naked,1,1,main
needle*dick,1,1,main
nifkin,1,1,main                   # it's the taint.
nigga,1,1,slur                    # slur against black people, misspelled
niggaz,1,1,slur                   # slur against black people, misspelled
nigger,1,1,slur                   # slur against black people
niggus,1,1,slur                   # slur against black people, misspelled
nigguz,1,1,slur                   # slur against black people, misspelled
nip,1,1,slur                      # slur against Japanese people (from Nippon)
nipple,1,1,main
nookey,1,1,main
nookie,1,1,main
nooky,1,1,main
nude,1,1,main
nuts,1,1,main
nymph,1,1,main                    # short for nymphomaniac
ofay,1,1,main
oral,0,1,main
orgasm,1,1,main
orgasmic,1,1,main
orgy,1,1,main
paddy,1,1,slur                    # slur against Irish people
pansy,1,1,main
pearl*necklace,1,1,main           # sperm surrounding one's neck after ejaculation e.g., 'Mary is not fond of pearl necklaces.'
pecker,1,1,main
pee,1,1,main
pee*pee,1,1,main
penetrate,1,1,main
penetration,1,1,main
penile,1,1,main
penis,1,1,medical
phuck,1,1,main                    # fuck, misspelled
phuk,1,1,main                     # fuck, misspelled
phuq,1,1,main                     # fuck, misspelled
pillow*bite,1,1,main              # "pillow biter" = gay male
pimp,1,1,main
piss,1,1,main
piss*flap,1,1,main                # piss flaps = labia
poo*poo,1,1,children
poon,1,1,main                     # vagina
poontang,1,1,main                 # vagina
poop,1,1,children
poop*chute,1,1,main               # rectum
porcelain*altar,1,1,main          # the toilet
porch*monkey,1,1,slur             # slur against black people
porn,1,1,main
prick,1,1,main
prostitute,1,1,main
pube,1,1,main
pubic,1,1,medical
pud,1,1,main                      # penis
puke,1,1,main
punk*ass,1,1,main
pussy,1,1,main
putz,1,1,main
queef,1,1,main                    # a pussy fart  i.e. an unpleasant-smelling encapsulation of gas excreted through the vagina, like a fart
queeny,1,1,slur                   # homosexual male
queer,1,1,slur
quiff,1,1,main
quim,1,1,main                     # pussy
quim*nuts,1,1,main                # used to refer to a particularly hanging vagina.  E.g.: John said,"Look at the quim nuts on her" whilst browsing through a crotch mag.
rape,1,1,main
raping,1,1,main
rapist,1,1,main
rectum,1,1,medical
redneck,1,1,slur                  # slur against southern people, especially rural and prejudiced
rim*job,1,1,main                  # to lick someone's anus
rimming,1,1,main                  # to lick someone's anus
rump,1,1,main
s&m,1,1,main
sand*nigger,1,1,slur              # slur against Arab people
scat,1,1,main                     # indulgence in feces
schlong,1,1,main
scrotum,1,1,medical
scum,1,1,main
sex,1,1,main
shag,1,1,main                     # British - to have sex
shitty,1,1,main
shiznit,1,1,main
sixty*nine,1,1,main
skull*fuck,1,1,main
slash,0,1,main
slit,0,1,main
slut,1,1,main
smegma,1,1,medical
smut,1,1,main
snatch,1,1,main
snot,1,1,children
sonofabitch,1,1,main
sonuvabitch,1,1,main
spade,1,1,slur                    # slur against black people
spear*chucker,1,1,slur            # slur against black people
sperm,1,1,main
spic,1,1,slur                     # slur against hispanic people
spick,1,1,slur                    # slur against hispanic people
spooge,1,1,main
spook,1,1,slur                    # slur against black people
strip,1,1,main
stripper,1,1,main
suck,1,1,main
swallow,1,1,main
sweater*puppet,1,1,main           # tits - that girl has nice sweater puppets!
tar*baby,1,1,slur                 # slur against black people
tea*bag,1,1,main                  # type of fellatio, the act of sucking the testicles.  "Mary tea bags real nice". "John enjoys it when he gets tea bagged".
testicle,1,1,medical
threesome,1,1,main
tinkle,1,1,children               # urinate
tit,1,1,main                      # breast
titty,1,1,main                    # breast
tongue,1,1,main                   # not bad as a noun, but suggestive as a verb...
towel*head,1,1,slur               # slur against Arab people
trailer*trash,1,1,slur            # slur against poor white people
transvestite,1,1,main
turd,1,1,main                     # feces
turd*burglar,1,1,slur             # slur against homosexual males
twat,1,1,main                     # vagina
up*yours,1,1,main
upskirt,1,1,main
vagina,1,1,medical
vibrator,1,1,main                 # marital aid
virgin,0,1,main
wank,1,1,main                     # British - to masturbate.  A wanker is a stupid person.
water*sport,1,1,main
wee*wee,1,1,main
wet*back,1,1,slur
whack,1,1,main
white*trash,1,1,slur              # slur against poor white people
whitey,1,1,slur                   # black slur against white people
whore,1,1,main
whorebag,1,1,main
whore*house,1,1,main
wigger,1,1,slur                   # slur against poor white people
wise*ass,1,1,main
woodie,1,1,main
woody,1,1,main
wop,1,1,slur                      # slur against Italian people
yid,1,1,slur                      # slur against Jewish people
# Partial matches (not whole word only)
bitch,1,0,main
cunt,1,0,main
fuck,1,0,main
jizz,1,0,main
shit,1,0,main
