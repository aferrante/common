// SmutCheckDlg.h : header file
//

#if !defined(AFX_SMUTCHECKDLG_H__65FC43ED_6655_4757_8344_AC2F6E33C913__INCLUDED_)
#define AFX_SMUTCHECKDLG_H__65FC43ED_6655_4757_8344_AC2F6E33C913__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CSmutCheckDlg dialog

class CSmutCheckDlg : public CDialog
{
// Construction
public:
	CSmutCheckDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSmutCheckDlg)
	enum { IDD = IDD_SMUTCHECK_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSmutCheckDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

public:
 	CEditEx m_EditEx;
  CString m_szCurrentText;
  CSmutFilter m_smut;
  BOOL m_bReplaceCurses;

  CString GetCodedText();


	// Generated message map functions
	//{{AFX_MSG(CSmutCheckDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnChangeRicheditChattext();
	afx_msg void OnButtonSmutcheck();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMUTCHECKDLG_H__65FC43ED_6655_4757_8344_AC2F6E33C913__INCLUDED_)
