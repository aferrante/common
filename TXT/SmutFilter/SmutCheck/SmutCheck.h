// SmutCheck.h : main header file for the SMUTCHECK application
//

#if !defined(AFX_SMUTCHECK_H__36C21618_F051_446A_B559_AA39605B1DDF__INCLUDED_)
#define AFX_SMUTCHECK_H__36C21618_F051_446A_B559_AA39605B1DDF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSmutCheckApp:
// See SmutCheck.cpp for the implementation of this class
//

class CSmutCheckApp : public CWinApp
{
public:
	CSmutCheckApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSmutCheckApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSmutCheckApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMUTCHECK_H__36C21618_F051_446A_B559_AA39605B1DDF__INCLUDED_)
