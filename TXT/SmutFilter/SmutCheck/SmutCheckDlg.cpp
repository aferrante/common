// SmutCheckDlg.cpp : implementation file
//

#include "stdafx.h"

#include "../../EditEx/EditEx.h"
#include "../SmutFilter.h"

#include "SmutCheck.h"
#include "SmutCheckDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmutCheckDlg dialog

CSmutCheckDlg::CSmutCheckDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSmutCheckDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSmutCheckDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSmutCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSmutCheckDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSmutCheckDlg, CDialog)
	//{{AFX_MSG_MAP(CSmutCheckDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_RICHEDIT_CHATTEXT, OnChangeRicheditChattext)
	ON_BN_CLICKED(IDC_BUTTON_SMUTCHECK, OnButtonSmutcheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmutCheckDlg message handlers

BOOL CSmutCheckDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
  m_EditEx.SetRichEdit((CRichEditCtrl*)GetDlgItem(IDC_RICHEDIT_CHATTEXT));
	m_EditEx.SetDisallowedChars(_T(""), _T(""), FALSE, EESD_CLEAR );

  m_bReplaceCurses = FALSE;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmutCheckDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSmutCheckDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSmutCheckDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSmutCheckDlg::OnChangeRicheditChattext() 
{
		CString szText;
		m_EditEx.GetWnd()->GetWindowText(szText);
		if(m_szCurrentText.CompareNoCase(szText)!=0)
		{
			m_szCurrentText=szText;
			OnButtonSmutcheck();
		}
}



void CSmutCheckDlg::OnButtonSmutcheck() 
{
	CHARRANGE chrgInit;
	m_EditEx.GetWnd()->GetSel(chrgInit);
  int numCurses, *startEndPairs=NULL, listSize=0;
  unsigned long wordsChecked;
  //  BOOL bReplaceCurses;
  
  CString szTemp, szCodedText;
	FontSettings_t fsFont;
  m_EditEx.GetWnd()->GetWindowText(szTemp);
  
  m_smut.m_bHighlightWholeWord=FALSE;

  numCurses = m_smut.languageFilter(&szTemp, &startEndPairs, &listSize, m_bReplaceCurses, &wordsChecked);

  if(m_bReplaceCurses) m_EditEx.GetWnd()->SetWindowText(szTemp); //  dont need the if, but may make it micro-faster without the set.

	fsFont=m_EditEx.GetFont();

	// set everything to default font, but also include |0s.
//  szCodedText=m_EditEx.FontDataToCode(fsFont)+szTemp;//testing
  szCodedText=GetCodedText(); // needs font codes



	m_EditEx.SetCodedTextVoidRV(szCodedText);
//  m_EditEx.GetWnd()->GetWindowText(szTemp);
//	AfxMessageBox(szCodedText);
	// now find the |0s and replace with chippies 3.
	FINDTEXTEX ft;
	int nfound = m_EditEx.GetWnd()->GetTextLength();

	ft.chrg.cpMin=0;      // range to search    
	ft.chrg.cpMax=nfound;      // range to search    
	ft.lpstrText="|";     // null-terminated string to find    
//	ft.chrgText;  // range in which text is found
	nfound = 0;
	while(nfound>=0)
	{
		nfound=m_EditEx.GetWnd()->FindText(0,&ft);
		if (nfound>=0) 
		{
			ft.chrgText.cpMin++;ft.chrgText.cpMax++;
			m_EditEx.GetWnd()->SetSel(ft.chrgText);
			if((m_EditEx.GetWnd()->GetSelText()).CompareNoCase("|")==0)
			{
				ft.chrgText.cpMin--;
				m_EditEx.GetWnd()->SetSel(ft.chrgText);
				m_EditEx.GetWnd()->ReplaceSel("|");
				ft.chrg.cpMin=ft.chrgText.cpMax-1;
			}
			else
			if((m_EditEx.GetWnd()->GetSelText()).CompareNoCase("0")==0)
			{
				ft.chrgText.cpMin--;
				m_EditEx.GetWnd()->SetSel(ft.chrgText);
//				m_EditEx.GetWnd()->SetSelectionCharFormat(m_EditEx.FontDataToCharFormat(m_ding[0].fsFont));
				m_EditEx.GetWnd()->ReplaceSel("3");
				ft.chrg.cpMin=ft.chrgText.cpMax-1;
			}
			
		}
	}
//AfxMessageBox(szTemp);
  //  GetDlgItem(IDC_EDIT_CHATTEXT)->SetWindowText(szTemp);
  for(int i=0;i<numCurses;i++)
  {
//		CString foo; foo.Format("%s\n%d,%d",szTemp,startEndPairs[i+i],startEndPairs[i+i+1]+1);
//		AfxMessageBox(foo);
		fsFont =	m_EditEx.GetFont();
		fsFont.crText=RGB(255,0,0);
		fsFont.nStyle|=FSS_BOLD;
		m_EditEx.SetFont(fsFont, startEndPairs[i+i],startEndPairs[i+i+1]+1);
  }
	m_EditEx.GetWnd()->SetSel(chrgInit);

//	m_EditEx.GetWnd()->Invalidate();
}




CString CSmutCheckDlg::GetCodedText()
{
  CString szTemp, szText=_T("");

	szTemp=m_EditEx.GetCodedText();
  //    GetDlgItem(IDC_EDIT_CHATTEXT)->GetWindowText(szTemp);

	//now we have all font codes in the string.
	// we only care about chippies by wassco font right now, so we will strip
	// all font codes, and then insert a special char in front of any
	// chippies by wassco font char.  

	//AfxMessageBox(szTemp);
	int nLen = szTemp.GetLength();
	for (int i=0; i<nLen; i++)
	{
		if ((szTemp.GetAt(i)=='<')&&(szTemp.GetAt(i+1)=='!')&&(szTemp.GetAt(i+2)=='F')&&(szTemp.GetAt(i+3)=='S')&&(szTemp.GetAt(i+4)=='"'))
		{ //we've found a font code.
			CString szFont=_T("");  
			i+=5;
			BOOL bGotFont=FALSE;
			szText+=_T("<!FS"); szText+=CString('"');
			while (szTemp.GetAt(i)!='>') 
			{
				
				while(!bGotFont)
				{
//					CString foox; foox.Format("bing! %d, %c",i,szTemp.GetAt(i) );
//					AfxMessageBox(foox);
					if(szTemp.GetAt(i)=='"') 	
						bGotFont=TRUE;
					else
					{
						szFont+=CString(szTemp.GetAt(i));
						szText+=szTemp.GetAt(i);i++;
					}
				}
				if(szFont.CompareNoCase(m_EditEx.GetFont().szFace)==0)
				{
					//if the font is the deafult font, make it have default properties
					// this removes the red bold change made by smut editor.
					CString szFontCode;
					szFontCode=m_EditEx.FontDataToCode(m_EditEx.GetFont());
					szFontCode=szFontCode.Right(18);
					szFontCode=szFontCode.Left(17); //just numerical font properties
					szText+=CString('"');
					szText+=szFontCode;
					i+=17;
				}
				else
					// just copy the font properties
					szText+=szTemp.GetAt(i);i++;
				

			}

			szText+=szTemp.GetAt(i);i++;
			if(szFont.CompareNoCase("ChippiesByWassco")==0)
			{
				while(szTemp.GetAt(i)=='3')
				{
					szText+="|0";
					i++;
				}
			}
			i--;

		}
		else  //it's just a char
		{
			char ch = szTemp.GetAt(i);
			if(ch=='|') //then we found a pipe that's not a ding bat
				szText+="||";  //must add || to get |
			else
				szText+=ch;
		}
	}
//	AfxMessageBox(szText);
	return szText;
}
