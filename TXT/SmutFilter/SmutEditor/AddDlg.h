#if !defined(AFX_ADDDLG_H__BFA73750_F60B_11D4_AF88_0010A4B277C0__INCLUDED_)
#define AFX_ADDDLG_H__BFA73750_F60B_11D4_AF88_0010A4B277C0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AddDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddDlg dialog

class CAddDlg : public CDialog
{
// Construction
public:
	CAddDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAddDlg)
	enum { IDD = IDD_DIALOG_ADD };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

public:
  CString m_szWord, m_szCategory, m_szCategoryList[20], m_szComment;
  BOOL m_bActive, m_bWhole;
  int m_nCatCount;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDDLG_H__BFA73750_F60B_11D4_AF88_0010A4B277C0__INCLUDED_)
