// SmutEditorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "../SmutFilter.h"

#include "SmutEditor.h"
#include "SmutEditorDlg.h"
#include "AddDlg.h"
#include "TestFilterDlg.h"
#include "MailServerDlg.h"
#include "../../vdifile/vdifile.h"
#include "../../networking/networking.h"
#include "../../Email/sendmail/sendmail.h"
#include "../../VersionControl/VersionControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
  CString szTemp;
  szTemp.Format("Build date: %s %s", __DATE__, __TIME__);
  GetDlgItem(IDC_STATIC_BUILD)->SetWindowText(szTemp);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmutEditorDlg dialog

CSmutEditorDlg::CSmutEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSmutEditorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSmutEditorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSmutEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSmutEditorDlg)
	DDX_Control(pDX, IDC_LIST_CATS, m_lvCats);
	DDX_Control(pDX, IDC_LIST_WORDS, m_lvWords);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSmutEditorDlg, CDialog)
	//{{AFX_MSG_MAP(CSmutEditorDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(NM_CLICK, IDC_LIST_WORDS, OnClickListWords)
	ON_BN_CLICKED(IDC_CHECK_ACTIVE, OnCheckActive)
	ON_BN_CLICKED(IDC_CHECK_WHOLE, OnCheckWhole)
	ON_EN_KILLFOCUS(IDC_EDIT_CAT, OnKillfocusEditCat)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CATS, OnClickListCats)
	ON_BN_CLICKED(IDC_BUTTON_CAT, OnButtonCat)
	ON_BN_CLICKED(IDC_BUTTON_ADDCAT, OnButtonAddcat)
	ON_BN_CLICKED(IDC_BUTTON_TEST, OnButtonTest)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_WORDS, OnDblclkListWords)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_WORDS, OnKeydownListWords)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmutEditorDlg message handlers

BOOL CSmutEditorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
  int i;

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
 

  //CVDIfile v;
  //v.EncodeFile("curses.txt", "curses.vdi");
  //v.DecodeFile("curses.vdi", "curses.txt");

  // word list
  CRect rcList;
  m_iLSCurrentItem = -1;
	m_lvWords.GetClientRect(&rcList);
	m_lvWords.InsertColumn(0, "Word", LVCFMT_LEFT, (int) (rcList.right*30/100),-1);
	m_lvWords.InsertColumn(1, "Active", LVCFMT_LEFT, (int) (rcList.right*16/100),-1);
	m_lvWords.InsertColumn(2, "Whole", LVCFMT_LEFT, (int) (rcList.right*16/100),-1);
	m_lvWords.InsertColumn(3, "Category", LVCFMT_LEFT, (int) (rcList.right*22/100),-1);
	m_lvWords.InsertColumn(4, "Comments", LVCFMT_LEFT, (int) (rcList.right*100/100),-1);

  BOOL bActive, bWhole;
  CString szWord, szCategory, szComment;

  csmut.sortDictionary(FALSE);

  for (i=0; i<csmut.getDictionarySize(); i++)
  {
    csmut.getWord(i, &szWord, &bActive, &bWhole, &szCategory, &szComment);

    m_lvWords.InsertItem(i,szWord);
    if (bActive) m_lvWords.SetItemText(i,1,"Yes");
    else m_lvWords.SetItemText(i,1,"No");
    if (bWhole) m_lvWords.SetItemText(i,2,"Yes");
    else m_lvWords.SetItemText(i,2,"No");
    m_lvWords.SetItemText(i,3,szCategory);
    m_lvWords.SetItemText(i,4,szComment);
  }


  // cat list
  m_iLSCCurrentItem = -1;
	m_lvCats.GetClientRect(&rcList);
	m_lvCats.InsertColumn(0, "Category", LVCFMT_LEFT, (int) (rcList.right*50/100),-1);
	m_lvCats.InsertColumn(1, "Status", LVCFMT_LEFT, (int) (rcList.right*25/100),-1);
	m_lvCats.InsertColumn(2, "Contains", LVCFMT_LEFT, (int) (rcList.right*25/100),-1);

  FillOutScreen(-1);
  Stats();

  // read hidden registry settings
  m_bChanged = FALSE;
	CWinApp *pWinApp = AfxGetApp();
  m_szMailHost = pWinApp->GetProfileString("Settings", "MailServer", "");
  m_szEMailAddress = pWinApp->GetProfileString("Settings", "EMailAddress", "");

  if ((m_szMailHost == "") || (m_szEMailAddress == ""))
  {
    CMailServerDlg d;
    d.m_szMailServer = m_szMailHost;
    d.m_szEMailAddress = m_szEMailAddress;
    d.DoModal();
    m_szMailHost = d.m_szMailServer;
    m_szEMailAddress = d.m_szEMailAddress;

    pWinApp->WriteProfileString("Settings", "MailServer", m_szMailHost);
    pWinApp->WriteProfileString("Settings", "EMailAddress", m_szEMailAddress);
  }


  CVersionControl cvc;
  cvc.CheckVersion("SmutEditor", "1.0.0.0");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmutEditorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSmutEditorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSmutEditorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSmutEditorDlg::OnClickListWords(NMHDR* pNMHDR, LRESULT* pResult) 
{
  // TODO: Add your control notification handler code here
  NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  int nHitItem;
  BOOL bSel=FALSE;
  CRect myRect;
  
  int iMaxCount = m_lvWords.GetItemCount( );
  
  for (int nItem=0; nItem < iMaxCount; nItem++)
  {
    m_lvWords.GetItemRect( nItem, myRect, LVIR_BOUNDS) ;
    if (pNMListView->ptAction.y >= myRect.top && pNMListView->ptAction.y < myRect.bottom)
    {
      nHitItem = nItem;
      m_iLSCurrentItem = nItem;
      bSel |= TRUE;
      m_lvWords.SetItem(nHitItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
      m_lvWords.SetItemState(nHitItem,INDEXTOSTATEIMAGEMASK(2), LVIS_STATEIMAGEMASK);

      //AfxMessageBox(csmut.m_smutList[m_iLSCurrentItem].word);
      FillOutScreen(m_iLSCurrentItem);
    }
    else
    {
      bSel |= FALSE;
      m_lvWords.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~LVIS_SELECTED, LVIS_SELECTED, 0);
      m_lvWords.SetItemState(nItem,INDEXTOSTATEIMAGEMASK(1), LVIS_STATEIMAGEMASK);
    }
  }
  
  *pResult = 0;
}



void CSmutEditorDlg::OnClickListCats(NMHDR* pNMHDR, LRESULT* pResult) 
{
  // TODO: Add your control notification handler code here
  NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  int nHitItem;
  BOOL bSel=FALSE;
  CRect myRect;
  BOOL bActive;
  CString szCategory;
  
  int iMaxCount = m_lvCats.GetItemCount( );
  
  for (int nItem=0; nItem < iMaxCount; nItem++)
  {
    m_lvCats.GetItemRect( nItem, myRect, LVIR_BOUNDS) ;
    if (pNMListView->ptAction.y >= myRect.top && pNMListView->ptAction.y < myRect.bottom)
    {
      nHitItem = nItem;
      m_iLSCCurrentItem = nItem;
      bSel |= TRUE;
      m_lvCats.SetItem(nHitItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
      m_lvCats.SetItemState(nHitItem,INDEXTOSTATEIMAGEMASK(2), LVIS_STATEIMAGEMASK);

      //AfxMessageBox(csmut.m_smutList[m_iLSCurrentItem].word);
      //FillOutScreen(m_iLSCurrentItem);

      csmut.getCategory(m_iLSCCurrentItem, &szCategory, &bActive);
      if (!bActive) GetDlgItem(IDC_BUTTON_CAT)->SetWindowText("Enable Category");
      else GetDlgItem(IDC_BUTTON_CAT)->SetWindowText("Disable Category");
    }
    else
    {
      bSel |= FALSE;
      m_lvCats.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~LVIS_SELECTED, LVIS_SELECTED, 0);
      m_lvCats.SetItemState(nItem,INDEXTOSTATEIMAGEMASK(1), LVIS_STATEIMAGEMASK);
    }
  }
  
  *pResult = 0;
}

void CSmutEditorDlg::OnCheckActive() 
{
  BOOL bActive;

  if (m_iLSCurrentItem <0) return;

  bActive = ((CButton *) GetDlgItem(IDC_CHECK_ACTIVE))->GetCheck();
  csmut.setActive(m_iLSCurrentItem, bActive);

  if (bActive) m_lvWords.SetItemText(m_iLSCurrentItem,1,"Yes");
  else m_lvWords.SetItemText(m_iLSCurrentItem,1,"No");

  FillOutScreen(m_iLSCurrentItem);
  m_bChanged = TRUE;
}


void CSmutEditorDlg::OnCheckWhole() 
{
  BOOL bWhole;

  if (m_iLSCurrentItem <0) return;

  bWhole = ((CButton *) GetDlgItem(IDC_CHECK_WHOLE))->GetCheck();
  csmut.setWholeWordOnly(m_iLSCurrentItem, bWhole);

  if (bWhole) m_lvWords.SetItemText(m_iLSCurrentItem,2,"Yes");
  else m_lvWords.SetItemText(m_iLSCurrentItem,2,"No");

  FillOutScreen(m_iLSCurrentItem);
  m_bChanged = TRUE;
}



void CSmutEditorDlg::OnKillfocusEditCat() 
{
  CString szCategory;

  if (m_iLSCurrentItem <0) return;

  GetDlgItem(IDC_EDIT_CAT)->GetWindowText(szCategory);
  csmut.setCategory(m_iLSCurrentItem, szCategory);
  m_lvWords.SetItemText(m_iLSCurrentItem,3,szCategory);

  m_bChanged = TRUE;
  FillOutScreen(m_iLSCurrentItem);
}




void CSmutEditorDlg::Stats() 
{
  CString szTemp, szCategoryList[20], szWord, szCategory, szComment;
  int nCats, i, nNotActive=0;
  BOOL bActive, bWhole;
  
  nCats = csmut.getCategorySize();

  for (i=0; i<csmut.getDictionarySize(); i++)
  {
    csmut.getWord(i, &szWord, &bActive, &bWhole, &szCategory, &szComment);
    if ((csmut.isDisabledCategory(szCategory)) || (!bActive))
      nNotActive++;
  }

  if (nCats == 1)
    szTemp.Format("%d words in %d category (%d inactive words)", csmut.getDictionarySize(), nCats, nNotActive);
  else szTemp.Format("%d words in %d categories (%d inactive words)", csmut.getDictionarySize(), nCats, nNotActive);
  GetDlgItem(IDC_STATIC_STATS)->SetWindowText(szTemp);
}


void CSmutEditorDlg::OnOK() 
{
  CString szErrorMsg;

  if (!m_bChanged) { CDialog::OnCancel(); return; }

  if (!csmut.validateDictionary(&szErrorMsg))
  { AfxMessageBox(szErrorMsg); return; }
  csmut.saveDictionary();
      if ((csmut.m_bChanged) && (m_szMailHost != ""))
      {
        CNetworking cnet;
        CSendMail csm;
        CString errors;
        cnet.startNetworking();

        if (!csm.sendMail("gregg@VideoDesignInteractive.com", 
          GSTR(m_szEMailAddress), 
          "VDI SmutFilter Feedback",
          GSTR(m_szMailHost), csmut.m_szChanges, &errors))
          AfxMessageBox(errors);

        cnet.shutdownNetworking();
      }
	CDialog::OnOK();
}


void CSmutEditorDlg::OnButtonAdd() 
{
  CAddDlg dlg;
  int insertPosition, i;
  BOOL bActive;

  dlg.m_szWord = "";
  dlg.m_nCatCount = 0;
  dlg.m_bActive = TRUE;
  dlg.m_bWhole = TRUE;

  dlg.m_nCatCount = csmut.getCategorySize(); //csmut.getCategoryList(dlg.m_szCategoryList);
  for (i=0; i<dlg.m_nCatCount; i++)
    csmut.getCategory(i, &(dlg.m_szCategoryList[i]), &bActive);

  dlg.m_szCategory = dlg.m_szCategoryList[0];
  dlg.m_szComment = "";


  if (dlg.DoModal() == IDOK)
  {
    // add it

    (dlg.m_szWord).MakeLower();

    insertPosition = csmut.addWord(dlg.m_szWord, dlg.m_bActive, dlg.m_bWhole, 
      dlg.m_szCategory, dlg.m_szComment);

    // fix the list box
    m_lvWords.InsertItem(insertPosition,dlg.m_szWord);
    if (dlg.m_bActive) m_lvWords.SetItemText(insertPosition,1,"Yes");
    else m_lvWords.SetItemText(insertPosition,1,"No");
    if (dlg.m_bWhole) m_lvWords.SetItemText(insertPosition,2,"Yes");
    else m_lvWords.SetItemText(insertPosition,2,"No");
    m_lvWords.SetItemText(insertPosition,3,dlg.m_szCategory);
    m_lvWords.SetItemText(insertPosition,4,dlg.m_szComment);

    m_iLSCurrentItem = insertPosition;
    
    FillOutScreen(insertPosition);
    m_bChanged = TRUE;
  }
}



void CSmutEditorDlg::OnButtonDel() 
{
  CString szTemp, szWord, szCategory, szComment;
  BOOL bActive, bWhole;

  if (m_iLSCurrentItem <0) return;
  
  csmut.getWord(m_iLSCurrentItem, &szWord, &bActive, &bWhole, &szCategory, &szComment);

  szTemp.Format("Are you sure you want to delete word %s (and not just disable it?)", szWord);

  if (MessageBox(szTemp, "Confirm Delete...", MB_YESNO) == IDYES)
  {
    csmut.deleteWord(m_iLSCurrentItem);

    // fix the list box
    m_lvWords.DeleteItem(m_iLSCurrentItem);
    
    m_iLSCurrentItem = -1;
    FillOutScreen(-1);
    m_bChanged = TRUE;
  }
}



void CSmutEditorDlg::OnCancel() 
{
  CString szErrorMsg;
  
  if (!m_bChanged) CDialog::OnCancel();
  else
  {
    // notify VDI of smutfile changes...
    if (MessageBox("Save changes?", 
      "Confirm Save", MB_YESNO) == IDYES)
    {
      if ((csmut.m_bChanged) && (m_szMailHost != ""))
      {
        CNetworking cnet;
        CSendMail csm;
        CString errors;
        cnet.startNetworking();

        if (!csm.sendMail("gregg@VideoDesignInteractive.com", 
          GSTR(m_szEMailAddress), 
          "VDI SmutFilter Feedback",
          GSTR(m_szMailHost), csmut.m_szChanges, &errors))
          AfxMessageBox(errors);

        cnet.shutdownNetworking();
      }

      if (!csmut.validateDictionary(&szErrorMsg))
      { AfxMessageBox(szErrorMsg); return; }
      csmut.saveDictionary();
    }
  }
  CDialog::OnCancel();
}




void CSmutEditorDlg::OnButtonEdit() 
{
  CAddDlg dlg;
  CString szCatList[MAX_CATEGORIES];
  int i;
  BOOL bActive;
  
  if (m_iLSCurrentItem <0) return;

  csmut.getWord(m_iLSCurrentItem, &dlg.m_szWord, &dlg.m_bActive, 
                &dlg.m_bWhole, &dlg.m_szCategory, &dlg.m_szComment);

  dlg.m_nCatCount = csmut.getCategorySize(); //csmut.getCategoryList(dlg.m_szCategoryList);
  for (i=0; i<dlg.m_nCatCount; i++)
    csmut.getCategory(i, &(dlg.m_szCategoryList[i]), &bActive);


  if (dlg.DoModal() == IDOK)
  {
    csmut.editWord(m_iLSCurrentItem, dlg.m_szWord, dlg.m_bActive, 
                   dlg.m_bWhole, dlg.m_szCategory, dlg.m_szComment);

    FillOutScreen(m_iLSCurrentItem);

    // fix the list box
    m_lvWords.SetItemText(m_iLSCurrentItem,0,dlg.m_szWord);
    if (dlg.m_bActive) m_lvWords.SetItemText(m_iLSCurrentItem,1,"Yes");
    else m_lvWords.SetItemText(m_iLSCurrentItem,1,"No");
    if (dlg.m_bWhole) m_lvWords.SetItemText(m_iLSCurrentItem,2,"Yes");
    else m_lvWords.SetItemText(m_iLSCurrentItem,2,"No");
    m_lvWords.SetItemText(m_iLSCurrentItem,3,dlg.m_szCategory);
    m_lvWords.SetItemText(m_iLSCurrentItem,4,dlg.m_szComment);
    
    m_bChanged = TRUE;
  }
}


void CSmutEditorDlg::FillOutScreen(int nItem)
{
  BOOL bActive, bWhole, bCatDisabled, bFirst;
  CString szWord, szCategory, szComment, szTemp, szList="", szResult="", szDisabled[20];

  if (nItem < 0)
  {
    ((CButton *)GetDlgItem(IDC_CHECK_ACTIVE))->SetCheck(FALSE);
    ((CButton *)GetDlgItem(IDC_CHECK_WHOLE))->SetCheck(FALSE);
    //((CButton *)GetDlgItem(IDC_CHECK_CATDIS))->SetCheck(FALSE);
    //GetDlgItem(IDC_EDIT_CAT)->SetWindowText("");
    GetDlgItem(IDC_EDIT_WORD)->SetWindowText("");
    GetDlgItem(IDC_STATIC_STATS)->SetWindowText("");
    GetDlgItem(IDC_STATIC_INFO)->SetWindowText("");
  }
  else
  {
    csmut.getWord(nItem, &szWord, &bActive,&bWhole,&szCategory, &szComment);
    bCatDisabled = csmut.isDisabledCategory(szCategory);

    ((CButton *)GetDlgItem(IDC_CHECK_ACTIVE))->SetCheck(bActive);
    ((CButton *)GetDlgItem(IDC_CHECK_WHOLE))->SetCheck(bWhole);
    //((CButton *)GetDlgItem(IDC_CHECK_CATDIS))->SetCheck(bCatDisabled);
     
    //GetDlgItem(IDC_EDIT_CAT)->SetWindowText(szCategory);
      
    szTemp = szWord;
    GetDlgItem(IDC_EDIT_WORD)->SetWindowText(szWord);
    if (bActive && (!bCatDisabled)) szTemp += " is active.";
    else
    {
      szTemp += " is not active ";
      if (!bActive) 
        szTemp += "(word disabled)";
      if (bCatDisabled)
        szTemp += " (category disabled)";
    }
    szResult += szTemp; szResult += "\n\n";
  
    int asterisk = szWord.Find("*");
    if (asterisk >=0)
    {
      CString form1, form2, list1[25], list2[25];
      int count1, count2, i, j;
      form1 = szWord.Left(asterisk);
      form2 = szWord.Right((szWord.GetLength() - asterisk) -1);

      count1 = csmut.generateWordFormsList(form1, list1);
      count2 = csmut.generateWordFormsList(form2, list2);
      

      szList = "Word forms: "; bFirst=TRUE;
      for (i=0; i<count1; i++)
      {
        for (j=0; j<count2; j++)
        {
          if (!bFirst) szList += ", "; else bFirst=FALSE;
          szList += (list1[i] + " " + list2[j]);
        }
      }
    }
    else
    {
      szList = "Word forms: " + csmut.generateWordForms(szWord);
    }
    szResult += szList; szResult += "\n\n";
  }
  
  // category list
  m_lvCats.DeleteAllItems();
  for (int i=0; i<csmut.getCategorySize(); i++)
  {
    csmut.getCategory(i, &szCategory, &bActive);
    m_lvCats.InsertItem(i,szCategory);
    if (!bActive) m_lvCats.SetItemText(i,1,"Disabled");
    else m_lvCats.SetItemText(i,1,"Active");

    szTemp.Format("%d words", csmut.countWordsInCategory(szCategory));
    m_lvCats.SetItemText(i,2,szTemp);
  }



  GetDlgItem(IDC_STATIC_INFO)->SetWindowText(szResult);

  Stats();
}

// enable / disable category
void CSmutEditorDlg::OnButtonCat() 
{
  BOOL bActive;
  CString szCategory;

  if (m_iLSCCurrentItem <0) return;

  csmut.getCategory(m_iLSCCurrentItem, &szCategory, &bActive);
  if (!bActive)
  {
    csmut.enableCategory(szCategory);
    GetDlgItem(IDC_BUTTON_CAT)->SetWindowText("Disable Category");
    m_lvCats.SetItemText(m_iLSCCurrentItem, 1, "Active");
  }
  else
  {
    csmut.disableCategory(szCategory);
    GetDlgItem(IDC_BUTTON_CAT)->SetWindowText("Enable Category");
    m_lvCats.SetItemText(m_iLSCCurrentItem, 1, "Disabled");
  }

  FillOutScreen(m_iLSCurrentItem);
  m_bChanged = TRUE;
}

void CSmutEditorDlg::OnButtonAddcat() 
{
  CString szCategory;

  GetDlgItem(IDC_EDIT_ADDCAT)->GetWindowText(szCategory);

  if (szCategory != "")
  {
    csmut.addCategory(szCategory, TRUE);
    m_iLSCCurrentItem = csmut.getCategorySize()-1;
    m_lvCats.InsertItem( m_iLSCCurrentItem, szCategory);
    m_lvCats.SetItemText(m_iLSCCurrentItem, 1, "Active");
  }
  FillOutScreen(m_iLSCurrentItem);
  m_bChanged = TRUE;
}


// This will test text with the current dictionary
void CSmutEditorDlg::OnButtonTest() 
{
  CTestFilterDlg dlg;

  // save changes?
  if (m_bChanged)
  {
    if (MessageBox("You have made changes to the smut dictionary.\nSave changes now before testing?", 
          "Confirm", MB_YESNO) == IDYES)
    {
      // save it first
      csmut.saveDictionary();
      m_bChanged = FALSE;
    }
  }

  dlg.DoModal();
  
}

void CSmutEditorDlg::OnDblclkListWords(NMHDR* pNMHDR, LRESULT* pResult) 
{
  // TODO: Add your control notification handler code here
  NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  int nHitItem;
  BOOL bSel=FALSE;
  CRect myRect;
  BOOL bActive;
  CString szCategory;
  
  int iMaxCount = m_lvWords.GetItemCount( );
  
  for (int nItem=0; nItem < iMaxCount; nItem++)
  {
    m_lvWords.GetItemRect( nItem, myRect, LVIR_BOUNDS) ;
    if (pNMListView->ptAction.y >= myRect.top && pNMListView->ptAction.y < myRect.bottom)
    {
      nHitItem = nItem;
      m_iLSCurrentItem = nItem;
      bSel |= TRUE;
      m_lvWords.SetItem(nHitItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
      m_lvWords.SetItemState(nHitItem,INDEXTOSTATEIMAGEMASK(2), LVIS_STATEIMAGEMASK);

      //AfxMessageBox(csmut.m_smutList[m_iLSCurrentItem].word);
      //FillOutScreen(m_iLSCurrentItem);

      csmut.getCategory(m_iLSCCurrentItem, &szCategory, &bActive);
      if (!bActive) GetDlgItem(IDC_BUTTON_CAT)->SetWindowText("Enable Category");
      else GetDlgItem(IDC_BUTTON_CAT)->SetWindowText("Disable Category");

      OnButtonEdit();
      break;
    }
    else
    {
      bSel |= FALSE;
      m_lvWords.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~LVIS_SELECTED, LVIS_SELECTED, 0);
      m_lvWords.SetItemState(nItem,INDEXTOSTATEIMAGEMASK(1), LVIS_STATEIMAGEMASK);
    }
  }
  
  *pResult = 0;
}


void CSmutEditorDlg::OnKeydownListWords(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
  if (  (pLVKeyDow->wVKey == 8) || // backspace
      ( pLVKeyDow->wVKey == 46)) //del
  {
    OnButtonDel();
  }
  
	*pResult = 0;
}
