//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SmutEditor.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SMUTEDITOR_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_ADD                  129
#define IDD_DIALOG_TEST                 130
#define IDI_ICON1                       131
#define IDB_BITMAP1                     132
#define IDD_DIALOG_SMTP                 133
#define IDC_LIST_WORDS                  1000
#define IDC_STATIC_FORMS                1001
#define IDC_STATIC_INFO                 1001
#define IDC_STATIC_ACTIVE               1002
#define IDC_CHECK_ACTIVE                1003
#define IDC_CHECK_WHOLE                 1004
#define IDC_EDIT_WORD                   1005
#define IDC_EDIT_CAT                    1006
#define IDC_CHECK_CATDIS                1007
#define IDC_STATIC_DISCAT               1008
#define IDC_BUTTON_ADD                  1009
#define IDC_BUTTON_DEL                  1010
#define IDC_BUTTON_EDIT                 1011
#define IDC_COMBO_CATS                  1012
#define IDC_STATIC_STATS                1013
#define IDC_LIST_CATS                   1014
#define IDC_BUTTON_CAT                  1015
#define IDC_BUTTON_ADDCAT               1016
#define IDC_EDIT_ADDCAT                 1017
#define IDC_BUTTON_TEST                 1018
#define IDC_EDIT_BOX                    1019
#define IDC_CHECK_REPLACE               1020
#define IDC_BUTTON_GO                   1021
#define IDC_STATIC_RESULTS              1022
#define IDC_BUTTON_GO2                  1023
#define IDC_EDIT_COMMENT                1023
#define IDC_BUTTON_HELP                 1024
#define IDC_STATIC_BUILD                1025
#define IDC_EDIT_SMTP                   1028
#define IDC_EDIT_ADDR                   1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
