// MailServerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmutEditor.h"
#include "MailServerDlg.h"
#include "../../networking/networking.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMailServerDlg dialog


CMailServerDlg::CMailServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMailServerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMailServerDlg)
	m_szMailServer = _T("");
	m_szEMailAddress = _T("");
	//}}AFX_DATA_INIT
}


void CMailServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMailServerDlg)
	DDX_Text(pDX, IDC_EDIT_SMTP, m_szMailServer);
	DDX_Text(pDX, IDC_EDIT_ADDR, m_szEMailAddress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMailServerDlg, CDialog)
	//{{AFX_MSG_MAP(CMailServerDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMailServerDlg message handlers

BOOL CMailServerDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	

  


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CMailServerDlg::VerifyMailServer() 
{
  SOCKET s;
  UpdateData(TRUE);

  CNetworking cnet;
  
  cnet.startNetworking();
  BOOL bRV = cnet.openConnection(GSTR(m_szMailServer), 25, &s, FALSE);
  cnet.shutdownNetworking();

  return bRV;
}


void CMailServerDlg::OnOK() 
{
  if (!VerifyMailServer()) AfxMessageBox("Error contacting mail server.\nPlease enter a valid mail server.");
  else
  {
    if (m_szEMailAddress.Find("@") < 0)
    AfxMessageBox("Please enter a valid email address.");
    else CDialog::OnOK();
  }
}

void CMailServerDlg::OnCancel() 
{
	CDialog::OnCancel();
}
