// SmutEditorDlg.h : header file
//

#if !defined(AFX_SMUTEDITORDLG_H__BFA73746_F60B_11D4_AF88_0010A4B277C0__INCLUDED_)
#define AFX_SMUTEDITORDLG_H__BFA73746_F60B_11D4_AF88_0010A4B277C0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CSmutEditorDlg dialog

class CSmutEditorDlg : public CDialog
{
// Construction
public:
	CSmutEditorDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSmutEditorDlg)
	enum { IDD = IDD_SMUTEDITOR_DIALOG };
	CListCtrl	m_lvCats;
	CListCtrl	m_lvWords;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSmutEditorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSmutEditorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClickListWords(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCheckActive();
	afx_msg void OnCheckWhole();
	afx_msg void OnKillfocusEditCat();
	virtual void OnOK();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDel();
	virtual void OnCancel();
	afx_msg void OnButtonEdit();
	afx_msg void OnClickListCats(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonCat();
	afx_msg void OnButtonAddcat();
	afx_msg void OnButtonTest();
	afx_msg void OnDblclkListWords(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydownListWords(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

public:
  int m_iLSCurrentItem, m_iLSCCurrentItem;
  CSmutFilter csmut;
  BOOL m_bChanged;
  CString m_szMailHost, m_szEMailAddress;
  void Stats();
  void FillOutScreen(int nItem);


	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMUTEDITORDLG_H__BFA73746_F60B_11D4_AF88_0010A4B277C0__INCLUDED_)
