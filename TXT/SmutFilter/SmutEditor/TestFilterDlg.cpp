// TestFilterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmutEditor.h"
#include "TestFilterDlg.h"
#include "../SmutFilter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestFilterDlg dialog


CTestFilterDlg::CTestFilterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestFilterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestFilterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CTestFilterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestFilterDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestFilterDlg, CDialog)
	//{{AFX_MSG_MAP(CTestFilterDlg)
	ON_BN_CLICKED(IDC_BUTTON_GO, OnButtonGo)
	ON_BN_CLICKED(IDC_BUTTON_GO2, OnButtonGo2)
	ON_BN_CLICKED(IDC_BUTTON_HELP, OnButtonHelp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestFilterDlg message handlers

BOOL CTestFilterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	((CButton *)GetDlgItem(IDC_CHECK_REPLACE))->SetCheck(1);

  return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CTestFilterDlg::OnButtonGo() 
{
  TestSmutFilter(FALSE);
}
void CTestFilterDlg::OnButtonGo2() 
{
  TestSmutFilter(TRUE);
}
  

void CTestFilterDlg::TestSmutFilter(BOOL bFromFile) 
{
  int i, n, *startEndPairs=NULL, listSize=0, numCurses;
  CString filename, bufferToCheck="", ext, crlf, results, szTemp, szTime;
  CSmutFilter cVar;
  unsigned long wordsChecked;
  BOOL bReplaceCurses, bHighlightWholeWord;
  CTime theTime;
  CTimeSpan span;
  clock_t start, finish;
  double duration;
  char line[1024], dir[256];

  GetDlgItem(IDC_STATIC_RESULTS)->SetWindowText("Processing...");
  
  if (bFromFile)
  {
    GetCurrentDirectory(256, dir);
    
    crlf.Format("%c%c", 13, 10);
    
    filename="";   ext="";
    static char BASED_CODE szFilter[] = 
      "All Files (*.*)|*.*||";
    CFileDialog zorg(TRUE, NULL, NULL, 0, szFilter, NULL);
    
    if (zorg.DoModal() == IDOK)
    {
      filename = zorg.GetPathName();
      ext = zorg.GetFileExt();
      
      FILE *fp = fopen(filename, "rt");
      if (fp == NULL)
      { AfxMessageBox("Error opening file."); return; }

      while (!feof(fp))
      {
        n=fread(line, 1, 1020, fp); 
        line[n]=0;
        szTemp.Format("%s", line);
        bufferToCheck += szTemp;
      }
      fclose(fp);
      
      // fix newlines
      for (i=0; i<bufferToCheck.GetLength(); i++)
      {
        if ((bufferToCheck[i] == 13) && (bufferToCheck[i+1] != 10))
        {
          // insert 10 after
          bufferToCheck = bufferToCheck.Left(i+1) + ((char) 10) +
            bufferToCheck.Right(bufferToCheck.GetLength() - i);
          i++;
        }
        else if ((i > 0) && (bufferToCheck[i] == 10) && (bufferToCheck[i-1] != 13))
        {
          // insert 13 before
          bufferToCheck = bufferToCheck.Left(i) + ((char) 13) +
            bufferToCheck.Right((bufferToCheck.GetLength() - i));
          i++;
        }
      }
    }
    SetCurrentDirectory(dir);
  }
  else GetDlgItem(IDC_EDIT_BOX)->GetWindowText(bufferToCheck);
  
  bReplaceCurses = ((CButton *)GetDlgItem(IDC_CHECK_REPLACE))->GetCheck();
  
  bHighlightWholeWord = ((CButton *)GetDlgItem(IDC_CHECK_WHOLE))->GetCheck();
  cVar.m_bHighlightWholeWord = bHighlightWholeWord;

  start = clock(); theTime = CTime::GetCurrentTime();
  
  numCurses = cVar.languageFilter(&bufferToCheck, &startEndPairs, &listSize, 
    bReplaceCurses, &wordsChecked);
  
  finish = clock(); span = CTime::GetCurrentTime() - theTime;
  duration = ((double)(finish - start)) / (double) CLOCKS_PER_SEC;
  if (duration == 0.0)  duration = 0.001;
  
  if (numCurses == 0) results = "0 Curses Found.\n";
  else
  {
    BOOL bFirst = TRUE;
    //szTemp.Format("%d Curses Found, at: ", numCurses);
    if (numCurses == 1) results.Format("%d Curse Found.\n", numCurses);
    else results.Format("%d Curses Found.\n", numCurses);
    /*
    //for (i=0; i<(numCurses<10?numCurses:10)*2; i+=2)
    for (i=0; i<numCurses*2; i+=2)
    {
    CString szTemp;
    szTemp.Format("[%d,%d], ", startEndPairs[i], startEndPairs[i+1]); 
    if (!bFirst) results += ","; else bFirst = FALSE;
    results += szTemp;
    }
    //if (numCurses>=10) results += "...";
    */
  }

  szTemp.Format("\n%d words (%d characters) checked in %s.\nThroughput=%0.2lf words/second (%0.2lf cps)\n", 
    wordsChecked, bufferToCheck.GetLength(), span.Format("%H:%M:%S"),
    (double) wordsChecked / (double) duration,
    (double) bufferToCheck.GetLength() / (double) duration);
  results += szTemp;
  
  GetDlgItem(IDC_STATIC_RESULTS)->SetWindowText(results);
  GetDlgItem(IDC_EDIT_BOX)->SetWindowText(bufferToCheck);

}
 

void CTestFilterDlg::OnButtonHelp() 
{
  
  MessageBox("Type (or paste) text into the edit box and click\nCheck Edit Box to test the smut filter's recognition\nof words and phrases.  To check a file, use the\nCheck File button.\n\nChecking Replace Curses will replace all curses found\nwith asterisks to make identifying problems easier.", 
    "SmutEditor Test Help", MB_OK);

}
