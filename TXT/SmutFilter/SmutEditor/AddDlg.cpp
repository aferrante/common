// AddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmutEditor.h"
#include "AddDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddDlg dialog


CAddDlg::CAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddDlg, CDialog)
	//{{AFX_MSG_MAP(CAddDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddDlg message handlers

void CAddDlg::OnOK() 
{
  int i, numstars = 0;

  GetDlgItem(IDC_EDIT_WORD)->GetWindowText(m_szWord);
  GetDlgItem(IDC_EDIT_COMMENT)->GetWindowText(m_szComment);
  GetDlgItem(IDC_COMBO_CATS)->GetWindowText(m_szCategory);
  m_bActive = ((CButton *)GetDlgItem(IDC_CHECK_ACTIVE))->GetCheck();
  m_bWhole = ((CButton *)GetDlgItem(IDC_CHECK_WHOLE))->GetCheck();


  for (i=0; i<m_szWord.GetLength(); i++)
  {
    if (m_szWord[i] == '*') numstars++;
  }
  if (numstars > 24)
  {
    AfxMessageBox("Error: words can be broken by at\nmost 24 asterisks (*).\n");
    return;
  }


	
	CDialog::OnOK();
}

BOOL CAddDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  int i;
  for (i=0; i<m_nCatCount; i++)
  {
    ((CComboBox *)GetDlgItem(IDC_COMBO_CATS))->AddString(m_szCategoryList[i]);
  }
  
  GetDlgItem(IDC_EDIT_WORD)->SetWindowText(m_szWord);
  GetDlgItem(IDC_EDIT_COMMENT)->SetWindowText(m_szComment);
  ((CComboBox *)GetDlgItem(IDC_COMBO_CATS))->SetCurSel(0);
  GetDlgItem(IDC_COMBO_CATS)->SetWindowText(m_szCategory);
  ((CButton *)GetDlgItem(IDC_CHECK_ACTIVE))->SetCheck(m_bActive);
  ((CButton *)GetDlgItem(IDC_CHECK_WHOLE))->SetCheck(m_bWhole);
  

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
