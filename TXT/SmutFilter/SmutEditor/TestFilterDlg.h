#if !defined(AFX_TESTFILTERDLG_H__3BD24152_0CCF_11D5_AFB4_0010A4B277C0__INCLUDED_)
#define AFX_TESTFILTERDLG_H__3BD24152_0CCF_11D5_AFB4_0010A4B277C0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TestFilterDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestFilterDlg dialog

class CTestFilterDlg : public CDialog
{
// Construction
public:
	CTestFilterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestFilterDlg)
	enum { IDD = IDD_DIALOG_TEST };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestFilterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestFilterDlg)
	afx_msg void OnButtonGo();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonGo2();
	afx_msg void OnButtonHelp();
	//}}AFX_MSG

public:
  void TestSmutFilter(BOOL bFromFile);


	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTFILTERDLG_H__3BD24152_0CCF_11D5_AFB4_0010A4B277C0__INCLUDED_)
