#if !defined(AFX_MAILSERVERDLG_H__ED7736FE_DF78_40C2_8D00_4868A244154C__INCLUDED_)
#define AFX_MAILSERVERDLG_H__ED7736FE_DF78_40C2_8D00_4868A244154C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MailServerDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMailServerDlg dialog

class CMailServerDlg : public CDialog
{
// Construction
public:
	CMailServerDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMailServerDlg)
	enum { IDD = IDD_DIALOG_SMTP };
	CString	m_szMailServer;
	CString	m_szEMailAddress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMailServerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
   BOOL VerifyMailServer();

	// Generated message map functions
	//{{AFX_MSG(CMailServerDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAILSERVERDLG_H__ED7736FE_DF78_40C2_8D00_4868A244154C__INCLUDED_)
