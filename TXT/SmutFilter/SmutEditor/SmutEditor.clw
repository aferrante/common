; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMailServerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SmutEditor.h"

ClassCount=6
Class1=CSmutEditorApp
Class2=CSmutEditorDlg
Class3=CAboutDlg

ResourceCount=6
Resource1=IDD_DIALOG_TEST
Resource2=IDR_MAINFRAME
Resource3=IDD_DIALOG_ADD
Class4=CAddDlg
Resource4=IDD_DIALOG_SMTP
Class5=CTestFilterDlg
Resource5=IDD_ABOUTBOX
Class6=CMailServerDlg
Resource6=IDD_SMUTEDITOR_DIALOG

[CLS:CSmutEditorApp]
Type=0
HeaderFile=SmutEditor.h
ImplementationFile=SmutEditor.cpp
Filter=N

[CLS:CSmutEditorDlg]
Type=0
HeaderFile=SmutEditorDlg.h
ImplementationFile=SmutEditorDlg.cpp
Filter=D
LastObject=IDC_LIST_WORDS
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=SmutEditorDlg.h
ImplementationFile=SmutEditorDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC_BUILD,static,1342308352

[DLG:IDD_SMUTEDITOR_DIALOG]
Type=1
Class=CSmutEditorDlg
ControlCount=22
Control1=IDC_LIST_WORDS,SysListView32,1350631433
Control2=IDC_EDIT_WORD,edit,1350633600
Control3=IDC_BUTTON_ADD,button,1342242816
Control4=IDC_BUTTON_EDIT,button,1342242816
Control5=IDC_BUTTON_DEL,button,1342242816
Control6=IDC_CHECK_ACTIVE,button,1342242819
Control7=IDC_CHECK_WHOLE,button,1342242819
Control8=IDCANCEL,button,1342242816
Control9=IDOK,button,1208025089
Control10=IDC_STATIC_INFO,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC_STATS,static,1342308353
Control13=IDC_LIST_CATS,SysListView32,1350631433
Control14=IDC_STATIC,button,1342177287
Control15=IDC_STATIC,button,1342177287
Control16=IDC_BUTTON_CAT,button,1342242816
Control17=IDC_BUTTON_ADDCAT,button,1342242816
Control18=IDC_EDIT_ADDCAT,edit,1350631552
Control19=IDC_STATIC,button,1342177287
Control20=IDC_BUTTON_TEST,button,1342242816
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342177294

[DLG:IDD_DIALOG_ADD]
Type=1
Class=CAddDlg
ControlCount=12
Control1=IDC_EDIT_WORD,edit,1350631552
Control2=IDC_CHECK_ACTIVE,button,1342242819
Control3=IDC_CHECK_WHOLE,button,1342242819
Control4=IDC_COMBO_CATS,combobox,1344339970
Control5=IDC_EDIT_COMMENT,edit,1350631552
Control6=IDOK,button,1342242817
Control7=IDCANCEL,button,1342242816
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352

[CLS:CAddDlg]
Type=0
HeaderFile=AddDlg.h
ImplementationFile=AddDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_EDIT_WORD
VirtualFilter=dWC

[DLG:IDD_DIALOG_TEST]
Type=1
Class=CTestFilterDlg
ControlCount=11
Control1=IDOK,button,1342242816
Control2=IDCANCEL,button,1208025088
Control3=IDC_EDIT_BOX,edit,1352732676
Control4=IDC_CHECK_REPLACE,button,1342242819
Control5=IDC_BUTTON_GO,button,1342242817
Control6=IDC_STATIC_RESULTS,static,1342308352
Control7=IDC_STATIC,button,1342177287
Control8=IDC_BUTTON_GO2,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_BUTTON_HELP,button,1342242816
Control11=IDC_CHECK_WHOLE,button,1342242819

[CLS:CTestFilterDlg]
Type=0
HeaderFile=TestFilterDlg.h
ImplementationFile=TestFilterDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CTestFilterDlg

[DLG:IDD_DIALOG_SMTP]
Type=1
Class=CMailServerDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_SMTP,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_ADDR,edit,1350631552

[CLS:CMailServerDlg]
Type=0
HeaderFile=MailServerDlg.h
ImplementationFile=MailServerDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_EDIT_ADDR

