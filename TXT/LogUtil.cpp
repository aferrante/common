// LogUtil.cpp: implementation of the CLogUtil class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "LogUtil.h"
#include "../TXT/BufferUtil.h"
//#include <time.h>
#include <direct.h>
#include <process.h>
//#include <winnt.h>
//#include <windef.h>
//#include <winbase.h> // needed to halt thread execution with Sleep. // now using stdafx.


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//thread process;
void FileHandlerThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLogUtil::CLogUtil()
{
	m_bThreadStarted = false;
	m_ucType = MSG_DESTTYPE_LOG;
	m_bFileInUse = false;
//	m_bBufferInUse = false;
	m_bAllowRepeat=false;
	m_bKillThread = true;
	m_bVerboseFile = false;
	m_pchSendBuffer=NULL;
	m_pszLastMessage=NULL;
	m_pchNewFileBuffer=NULL;
	m_ulBufferLength=0;
	m_ulNewFileBufferLength=0;
	m_nAbsoluteTimeOffsetMS=0;
	m_ulLastUnixTime=0;
	m_usLastMillitime=0;
	m_ulLastMessageCount=0;
	m_pfile=NULL;
	m_pszFileBaseName=NULL;
	m_pszDestinationTable=NULL;
	m_pszFunctionTable=NULL;
	m_pszFilenameFormatSpec = NULL;
	m_pszLastRotateKey = (char*)malloc(13);
	if(m_pszLastRotateKey) sprintf(m_pszLastRotateKey, "000000000000");

	m_chOverwritePeriod='Y';
	m_chRotatePeriod='M';
	m_nRotateOffsetMonth=1;
	m_nRotateOffsetDay=1;
	m_nRotateOffsetHours=0;
	m_nRotateOffsetMinutes=0;

	m_ulReqCount=0;
	m_ulSvcCount=0;

	InitializeCriticalSection(&m_critBuffer);

}

CLogUtil::~CLogUtil()
{
	if(m_pszLastRotateKey) free(m_pszLastRotateKey);
	m_pszLastRotateKey = NULL;
	CloseLog();
	DeleteCriticalSection(&m_critBuffer);
}

//////////////////////////////////////////////////////////////////////
// Design Philosophy
//////////////////////////////////////////////////////////////////////
// Log Files are typically time stamped ASCII text, that get large.
// To conserve space, files are written in a binary format.
// Log files have had the following information per record:
// Timestamp | Message | Severity | Calling Function | Icon | Destinations
// Timestamp has usually been the absolute machine timestamp, to the millisecond
// Message is the information to be logged.
// Severity is how severe the error (if it is an error) is.
// Calling function is the name of the function that sends the information
// Icon is a graphical icon associated with the message if any is desired
// Destinations are other places (such as email or a GUI) that this message has been sent.
// To conserve space, the following format is adopted:
// There are two types of records: table records and item records.
// A table record is reference information required to parse the records in the file.
// It has the following form:
// [1 byte - the ref data type - the first bit is 1][information, variable bytes (char 10 encoded as "|~", "|" encoded as "||")][char 10 (newline)]

// there are 3 reference data types:
// 1. reference time.  the absolute time from which the entries are timestamped with an offset.
//    The byte value is 0x80.  The reference time is recorded as 4 bytes unixtime, 2 bytes millisecond offset.
// 2. calling function name record.
//    The byte value is 0x81.  The information is one byte for 255 functions tabulated, 0x00 is not valid. followed by ascii function name, no char 10 allowed.
// 3. destination name record.
//    The byte value is 0x82.  The information is one byte for 255 destinations tabulated, 0x00 is not valid. followed by ascii destination name, no char 10 allowed.
// ...
// files should start with the reference time (or appended upon opening), and the reference time should be updated every 8388607 milliseconds.
// An item record is a logged item, and has the following form:
// [3 bytes - time in milliseconds elapsed since last table entry for reference time - the first bit is zero][1 byte, calling function][1 byte, severity and icon][1 byte, destinations][information, variable bytes (char 10 encoded as "|~", "|" encoded as "||")][char 10 (newline)]
// 
// a viewer will be required to view these files, unless they are decompiled to text.
// utilities for such decompilation should be in this code.
// one file per object.

int CLogUtil::OpenLog(char* pszFileParams)
{
	// pszParams holds the following parameters:
	// file base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs

// to keep time offset as close as possible, calculate it first
	_timeb timebuffer;
  _ftime( &timebuffer );

	char* pszParams = NULL;
	// copy the params, dont want to diddle the passed in buffer.
	if(pszFileParams)
	{
		pszParams = (char*)malloc(strlen(pszFileParams)+1);
		if(pszParams) strcpy(pszParams, pszFileParams);
	}
	char* pszParamsOrig = pszParams;

	char* pszFileBaseName = pszParams;
	char* pszParamList = strchr(pszParams, '|');

	if(pszParamList!=NULL)
	{
		*pszParamList = 0; //Null Term
		pszParams = pszParamList+1;  // advance to param list.
	}
	else
	{ 
		pszParams=NULL;
	}

	unsigned long ulTime=0;
	if(pszFileBaseName==NULL)
	{
		if(pszParams) free(pszParams);
		return LOG_BADBUFFER;
	}
	CloseLog();  // loads default values as well.

	m_pszFileBaseName = (char*)malloc(strlen(pszFileBaseName)+1);
	if(m_pszFileBaseName) strcpy(m_pszFileBaseName, pszFileBaseName);



	if(pszParams!=NULL)// parse params
	{
		if (strlen(pszParams)>0) 
		{
			bool bFinalParam = false;
//			char* pch = strtok( pszParams, "|" );
			char* pch = strchr( pszParams, '|' );
			if(pch!=NULL)
			{
				*pch = 0;
			}
			else bFinalParam = true;
			pch = pszParams;

			int j=0;
			CBufferUtil bu;
			while(pch!=NULL)
			{
				switch(j)
				{
				case 0:	// file rotate spec
					{
						int nLen = strlen(pch);
						if(nLen)
						{
							char szValidChars[7];
							char pair[3];
							memset(pair, 0, 3);  // zero terminator

							int min=0, hour=0, day=1, month=1;
							if((nLen>2)&&(bu.IsDecimalString(pch+2))) // there must be some numbers!
							{
								if((nLen-2)>=2) //minutes valid
								{
									strncpy(pair, pch+strlen(pch)-2, 2);
									min=atoi(pair);
									if (min<0) min=0;
									if (min>59) min=59;
								}
								if((nLen-2)>=4) // hours are valid
								{
									strncpy(pair, pch+strlen(pch)-4, 2);
									hour=atoi(pair);
									if (hour<0) hour=0;
									if (hour>23) hour=23;
								}
								if((nLen-2)>=6) // days are valid
								{
									strncpy(pair, pch+strlen(pch)-6, 2);
									day=atoi(pair);
									if (day<1) day=1;
									if (day>31) day=31;  
								}
								if((nLen-2)>=8) // months are valid
								{
									strncpy(pair, pch+strlen(pch)-8, 2);
									month=atoi(pair);
									if (month<1) month=1;
									if (month>12) month=12;  
								}
							}
							sprintf(szValidChars, "HDWMYF");
							strupr(pch);
							char ch = *(pch);
							if(strchr(szValidChars, ch)!=NULL)	m_chOverwritePeriod=ch;

							if(nLen>1) // rotate period is there
							{
								sprintf(szValidChars, "HDMY");
								ch = *(pch+1);
								if(strchr(szValidChars, ch)!=NULL)	m_chRotatePeriod=ch;
							}
							m_nRotateOffsetMonth = month;
							m_nRotateOffsetDay = day;
							m_nRotateOffsetHours= hour;
							m_nRotateOffsetMinutes = min;

							switch(m_chOverwritePeriod)
							{
							case 'F': break;
								{
									sprintf(szValidChars, "HDMYF");// currently not supporting break on week, only keep week.
									if (strchr(szValidChars, m_chRotatePeriod)==NULL) m_chRotatePeriod='M'; break;  // month default
								}
								break;
							case 'Y': break;
								{
									sprintf(szValidChars,"HDM");// currently not supporting break on week, only keep week.
									if (strchr(szValidChars, m_chRotatePeriod)==NULL) m_chRotatePeriod='M'; break; // month default
								}
								break;
							case 'M': 
								{
									sprintf(szValidChars,"HD"); // currently not supporting break on week, only keep week.
									if (strchr(szValidChars, m_chRotatePeriod)==NULL) m_chRotatePeriod='D'; break; //day
								}
							case 'W': 
								{
									sprintf(szValidChars,"HD");
									if (strchr(szValidChars, m_chRotatePeriod)==NULL) m_chRotatePeriod='D'; if (day>7) day=7; break;//day
								}
								break;
							case 'D': // has to be H
								{
									m_chRotatePeriod='H';
								}
								break; 
							case 'H': // error - there is no "keep hour" - go with default
								{
									m_chOverwritePeriod='Y'; m_chRotatePeriod='M'; 
								}
								break; 
							}

						}

					}
					break;
				case 1: //custom filename format
					{
						if(strlen(pch))
						{
							m_pszFilenameFormatSpec = (char*)malloc(strlen(pch)+1);
							if(m_pszFilenameFormatSpec) strcpy(m_pszFilenameFormatSpec, pch);
						}
					}
					break;
				case 2: // human readable
					{
						if((strlen(pch))&&(atoi(pch)!=0)) m_bVerboseFile=true;
					}
					break;
				case 3: // allow repeat text
					{
						if((strlen(pch))&&(atoi(pch)!=0)) m_bAllowRepeat=true;
					}
					break;
				case 4: // unixtime offset
					{
						if((strlen(pch))&&(atoi(pch)!=0))
						{
							ulTime=atoi(pch);
							m_nAbsoluteTimeOffsetMS=(timebuffer.time-ulTime)*1000;
						}
					}
					break; 
				case 5: // millitime offset
					{
						if((strlen(pch))&&(atoi(pch)!=0))
						{
							m_nAbsoluteTimeOffsetMS+=(timebuffer.millitm-(atoi(pch)));
						}
					}
					break; 
				}
				j++;
				//pch = strtok( NULL, "|" );

				pszParams += (strlen(pch)+1);  // alters the params buffer pointer

				if(bFinalParam)
				{
					// detected no more delimiters last time.
					pch = NULL;
				}
				else
				if(*pszParams == 0)
				{
					// end of param string
					pch = NULL;
				}
				else
				{
					pch = strchr( pszParams, '|' );
					if(pch!=NULL)
					{
						*pch = 0;
					}
					else bFinalParam = true;
					pch = pszParams;
				}
			}
		}
	}

	m_bKillThread = false;
	_beginthread( FileHandlerThread, 0, (void*)(this) );  // do the writes to the buffer
		
	if(pszParamsOrig) free(pszParamsOrig);

	return RotateLog(ulTime, true);
}

int CLogUtil::CloseLog()
{
	m_bKillThread = true;
	while(m_bThreadStarted) Sleep(1);

	if((m_ulLastMessageCount>0)&&(m_pfile))
	{
		// have to write out repeat line
		if(m_pszLastMessage)  // only do it if there is a message.
		{
			if(m_bVerboseFile)
			{
				// full text description, dont need to check anything.
				char chRpt[256];

				memcpy(chRpt, m_pszLastMessage, 30); // last repeat time

				sprintf(chRpt+30, "...repeated %d times...%c%c%c", m_ulLastMessageCount, 13, 10, 0);

				fwrite(chRpt, 1, strlen(chRpt), m_pfile );
				fflush(m_pfile);
			}
			else
			{
				// have to write out repeat line, which we encode
				// [6 bytes time and flags][char 0x13 (rpt signifier)][1 byte, num bytes for len][1 to 4 bytes repeat#][char 10]
				unsigned char buffer[16]; // need 13
				unsigned char ucCount, ucBuf; 

				if(m_ulLastMessageCount&0xff000000) ucCount = 4;
				else if(m_ulLastMessageCount&0xff0000) ucCount = 3;
				else if(m_ulLastMessageCount&0xff00) ucCount = 2;
				else ucCount = 1;

				memcpy(buffer, m_pszLastMessage, 6); // last repeat time

				*(buffer+6) = 0x13;
				*(buffer+7) = ucCount; ucBuf=8;
				*(buffer+ucBuf++) = (unsigned char)(m_ulLastMessageCount&0xff);
				if(ucCount>1) *(buffer+ucBuf++) = (unsigned char)((m_ulLastMessageCount>>8)&0xff);
				if(ucCount>2) *(buffer+ucBuf++) = (unsigned char)((m_ulLastMessageCount>>16)&0xff);
				if(ucCount>3) *(buffer+ucBuf++) = (unsigned char)((m_ulLastMessageCount>>24)&0xff);
				*(buffer+ucBuf++) = 10;

				fwrite( buffer, 1, ucBuf, m_pfile );
				fflush(m_pfile);
			}

		}
	}

	if(m_pfile!=NULL)	fclose(m_pfile);
	m_pfile=NULL;

	if(m_pchSendBuffer) free(m_pchSendBuffer);
	m_pchSendBuffer=NULL;
	if(m_pchNewFileBuffer) free(m_pchNewFileBuffer);
	m_pchNewFileBuffer=NULL;
	if(m_pszLastMessage) free(m_pszLastMessage);
	m_pszLastMessage=NULL;
	m_ulBufferLength=0;
	m_ulNewFileBufferLength=0;
	m_nAbsoluteTimeOffsetMS=0;
	m_ulLastUnixTime=0;
	m_usLastMillitime=0;
	m_ulLastMessageCount=0;
	if(m_pszFileBaseName) free(m_pszFileBaseName);
	m_pszFileBaseName=NULL;
	if(m_pszDestinationTable) free(m_pszDestinationTable);
	m_pszDestinationTable=NULL;
	if(m_pszFunctionTable)		free(m_pszFunctionTable);
	m_pszFunctionTable=NULL;
	if(m_pszFilenameFormatSpec) free(m_pszFilenameFormatSpec);
	m_pszFilenameFormatSpec = NULL;

//AfxMessageBox("here");
	m_chOverwritePeriod='Y';
	m_chRotatePeriod='M';
	m_nRotateOffsetMonth=1;
	m_nRotateOffsetDay=1;
	m_nRotateOffsetHours=0;
	m_nRotateOffsetMinutes=0;
	if(m_pszLastRotateKey) _snprintf(m_pszLastRotateKey, sizeof(m_pszLastRotateKey)-1, "000000000000");

	m_bFileInUse = false;
//	m_bBufferInUse = false;
	m_bAllowRepeat=false;
	m_bKillThread = true;
	m_bVerboseFile = false;

	m_ulReqCount=0;
	m_ulSvcCount=0;

	return LOG_SUCCESS;
}

int CLogUtil::RotateLog(unsigned long unixtime, bool bOpen)
{
// rotate spec is the file rotation specification: like this:
// 'H' = hour, 'D' = day, 'W' = week, 'M' = month, 'Y' = year, 'F' = Forever.
// the first char indicates the period of overwrite, the second char indicates period of rotation.  characters after that specify rotation offset, with spec MMddhhmm
// we are currently not supporting break on week, only keep week.  so, no 'W' in second position
// this means number of days, hours, and minutes (ddd:hh:mm after the default rotation period (01010000 -  zero days, 00:00 o'clock)
// F.ex. "YM01010000" would start with "Module-Jan.log" and create a new file, "Module-Feb.log" on Feb 1st.  it would overwrite "Module-Jan.log" on Jan 1st of next year. (month 01 ignored)
// "YM01020000" would create new files such as "Module-Feb.log" on Feb 2nd. etc.  Things on Feb 1st would get logged to the Jan file.(month 01 ignored)
// "YM01010200" would create new files such as "Module-Feb.log" on the second HOUR of Feb.  everything up until Feb 1st, 01:59 AM gets logged to the January file.
// "FD01000530" would create new files at 5:30 AM, every day, never overwriting a file.  "Module-21Feb2002.log" etc. - the "MMdd" part of the offset is ignored, since it can't make any sense
// "FW01030600" would create new files at 6:00 AM, every week on Tuesday (Sun=1, 0 = use default), never overwriting a file.  
// defaults for each char's rotation offsets are DH01010000, WD01010000, MD01010000, YM01010000. FM01010000 
// So, you could just put "F" and it will be interpreted as "FM01010000"
// if there is an error, the default "YM01010000" will be used.  if the error is just with the numbers, the numbers 01010000 will be used with whatever letters.
// minimum is "DH00000000" (only looks at minutes of time for offset).  Max is "FY" = "FY12312359" (uses 365 if not a leap year)
// custom format override must include basename,
//   e.g.:  "DEServer-CustomLogName-%b%a%Y.log"
//   e.g.:  "t%m%d%y.log" for Fox


	// figure out the format...
	char* pszFormatString=NULL;
	char* pszRotateSpec = (char*)malloc(12); 
	if(pszRotateSpec) sprintf(pszRotateSpec, "YM00100000");

//					unsigned long ulBufLen = m_ulBufferLength;	CBufferUtil bxu;  char* px = bxu.ReadableHex(m_pchSendBuffer, &ulBufLen);
//					AfxMessageBox(px); free(px);
	

	if((m_pszFilenameFormatSpec != NULL)&&(strlen(m_pszFilenameFormatSpec)>0))
	{
		// was simple assignment, so got freed incorrectly, corrected 2012-12-15.
		// pszFormatString = m_pszFilenameFormatSpec;
		
//		AfxMessageBox(m_pszFilenameFormatSpec);
		pszFormatString = (char*)malloc(strlen(m_pszFilenameFormatSpec)+1); // have to malloc, gets freed later
		if(pszFormatString)
		{
			strcpy(pszFormatString, m_pszFilenameFormatSpec);
		}
	}
	else
	{
		pszFormatString = (char*)malloc(strlen(m_pszFileBaseName)+32); // 32 is more than enough to take format specifiers

//		char droh[256];
//		sprintf(droh, "before %08x, %08x", pszFormatString, m_pchSendBuffer);
//		AfxMessageBox(droh);
		
		if(pszFormatString)
		{
			strcpy(pszFormatString, m_pszFileBaseName);

			switch(m_chOverwritePeriod)
			{ 
			case 'F':
				{
					switch (m_chRotatePeriod)
					{
					case 'F': strcat(pszFormatString, ".log"); break; // one logfile!
					case 'Y':	strcat(pszFormatString, "-%Y.log"); break; // break on year
					case 'M':	strcat(pszFormatString, "-%b-%Y.log"); break; // break on Month
					case 'D':	strcat(pszFormatString, "-%d-%b-%Y.log"); break; // break on day
					case 'H':	strcat(pszFormatString, "-%H%M-%d-%b-%Y.log"); break; // break on hour.
					}
				}
				break;
			case 'Y': 
				{
					switch (m_chRotatePeriod)
					{
					case 'M':	strcat(pszFormatString, "-%b.log"); break; // break on Month
					case 'D':	strcat(pszFormatString, "-%d-%b.log"); break; // break on day
					case 'H':	strcat(pszFormatString, "-%H%M-%d-%b.log"); break; // break on hour.
					}
				}
				break;
			case 'M': 
				{
					switch (m_chRotatePeriod)
					{
					case 'D':	strcat(pszFormatString, "-%d.log"); break; // break on day
					case 'H':	strcat(pszFormatString, "-%H%M-%d.log"); break; // break on hour.
					}
				}
				break;
			case 'W':
				{
					switch (m_chRotatePeriod)
					{
					case 'D':	strcat(pszFormatString, "-%a.log"); break; // break on day
					case 'H':	strcat(pszFormatString, "-%H%M-%a.log"); break; // break on hour.
					}
				}
				break;
			case 'D':
				{
					strcat(pszFormatString, "-%H%M.log"); break; // break on hour. (has to be)
				}
				break;
			default:
				{
					strcat(pszFormatString, "-%b.log"); break;  //"xxxx-Jan.log" is default.
				}
				break;
			}


			if(pszRotateSpec) sprintf(pszRotateSpec,"%c%c%02d%02d%02d%02d",
							m_chOverwritePeriod,m_chRotatePeriod,
							m_nRotateOffsetMonth,
							m_nRotateOffsetDay,
							m_nRotateOffsetHours,
							m_nRotateOffsetMinutes
							);

		}
	} 

//					unsigned long ulBufLen = m_ulBufferLength;	CBufferUtil bxu;  char* px = bxu.ReadableHex(m_pchSendBuffer, &ulBufLen);
//					AfxMessageBox(px); free(px);
//					AfxMessageBox(pszFormatString);
//					char doh[256];
//					sprintf(doh, "after %08x, %08x", pszFormatString, m_pchSendBuffer);
//					AfxMessageBox(doh);

	// use current time or override time, passed in
	time_t currentTime;

	if(unixtime==0)
	{
		time(&currentTime); 

		//adjust for offset
		currentTime -=(m_nAbsoluteTimeOffsetMS/1000);
	}
	else
	{
		currentTime = unixtime;
	}

	tm* theTime = localtime( &currentTime	);
	currentTime=mktime(theTime); 


	unsigned short nCurrentYear = theTime->tm_year+1900; //theTime.GetYear();
	unsigned char  nCurrentMonth = theTime->tm_mon+1; //theTime.GetMonth();
	unsigned char  nCurrentDay = theTime->tm_mday; //theTime.GetDay();
	unsigned char  nCurrentHours= theTime->tm_hour; //theTime.GetHour();
	unsigned char  nCurrentMinutes = theTime->tm_min; //theTime.GetMinute();
//	BYTE  nCurrentDayOfWeek = theTime.GetDayOfWeek();


	// figure out next rotate threshold...

	unsigned short nThresholdYear = 0;
	unsigned char  nThresholdMonth = 0;
	unsigned char  nThresholdDay = 0;
	unsigned char  nThresholdHours= 0;
	unsigned char  nThresholdMinutes = 0;

	char buffer[5];
	memset(buffer, 0, 5);
	if((!bOpen)&&(m_pszLastRotateKey))
	{
		strncpy(buffer, m_pszLastRotateKey, 4);  
		nThresholdYear = atoi(buffer); 	memset(buffer, 0, 5);
		strncpy(buffer, m_pszLastRotateKey+4, 2);  
		nThresholdMonth = atoi(buffer);
		strncpy(buffer, m_pszLastRotateKey+6, 2);  
		nThresholdDay = atoi(buffer);
		strncpy(buffer, m_pszLastRotateKey+8, 2);  
		nThresholdHours= atoi(buffer);
		strncpy(buffer, m_pszLastRotateKey+10, 2);  
		nThresholdMinutes = atoi(buffer);
	//	BYTE  nThresholdDayOfWeek = atoi(g_pmd->m_file.m_fReg[nIndex].szLastRotateKey.Right(1));
	}
	if (nThresholdYear)  // force new file if 0..  later.
	{
		switch (m_chRotatePeriod)
		{
		case 'F': break; // one logfile!
		case 'Y':	nThresholdYear++; break;
		case 'M':	nThresholdMonth++; break; // break on Month
		case 'D':	nThresholdDay++; break; 
		case 'H':	nThresholdHours++; break; 
		case 'W':	nThresholdDay++; break; 
		}
	}

	// lets make key numbers
	int nThreshold;
	int nCurrent;
	nThreshold= nThresholdMinutes+nThresholdHours*100+nThresholdDay*10000
		+nThresholdMonth*1000000+(nThresholdYear%10)*100000000;
	nCurrent= nCurrentMinutes+nCurrentHours*100+nCurrentDay*10000
		+nCurrentMonth*1000000+(nCurrentYear%10)*100000000;
	// here's the logic:
	// the threshold might be calculated as January 32nd for instance, but the way the key is assembled, the
	// number would be 0132.  But if the day were Feb 1, the current # would be 0201, which is >= 0132.
	// therefore there is no need to make the calcs to turn 1/31+ 1 into 2/1.

// rotate!
	char pszNextRotateKey[13];

	time_t timeRotate;
	tm		 RT = *theTime;
	RT.tm_year = 0; 
	RT.tm_mon = 0;
	RT.tm_mday = 0;
	RT.tm_hour = 0;
	RT.tm_min = 0;
	RT.tm_sec =0;


	char* pszFilename=NULL;
  
	if (nCurrent>=nThreshold)
  {
		// must assemble correct rotate key
		switch (m_chRotatePeriod)
		{
		case 'F': timeRotate=0;  break; // one logfile! time will never exceed yr 2039 with CTime
		case 'Y': 
			{
				RT.tm_year = nCurrentYear-1900; 
				RT.tm_mon = m_nRotateOffsetMonth-1;
				RT.tm_mday = m_nRotateOffsetDay;
				RT.tm_hour = m_nRotateOffsetHours;
				RT.tm_min = m_nRotateOffsetMinutes;
				timeRotate=mktime(&RT); // break on year
			} break;
		case 'M':
			{
				RT.tm_year = nCurrentYear-1900; 
				RT.tm_mon = nCurrentMonth-1;
				RT.tm_mday = m_nRotateOffsetDay;
				RT.tm_hour = m_nRotateOffsetHours;
				RT.tm_min = m_nRotateOffsetMinutes;
				timeRotate=mktime(&RT); // break on year
			} break;
		case 'D':
			{
				RT.tm_year = nCurrentYear-1900; 
				RT.tm_mon = nCurrentMonth-1;
				RT.tm_mday = nCurrentDay;
				RT.tm_hour = m_nRotateOffsetHours;
				RT.tm_min = m_nRotateOffsetMinutes;
				timeRotate=mktime(&RT); // break on year
			} break;
		case 'H': 
			{
				RT.tm_year = nCurrentYear-1900; 
				RT.tm_mon = nCurrentMonth-1;
				RT.tm_mday = nCurrentDay;
				RT.tm_hour = nCurrentHours;
				RT.tm_min = m_nRotateOffsetMinutes;
				timeRotate=mktime(&RT); // break on year
			} break;
		}

		if(currentTime<timeRotate) // on open, must create the LAST file name and the correct open key.
		{
			int d=0,h=0;
			switch (m_chRotatePeriod)
			{
			case 'Y':
				{
					nCurrentYear--; 
					theTime->tm_year = nCurrentYear-1900; 
					theTime->tm_mon = nCurrentMonth-1;
					theTime->tm_mday = nCurrentDay;
					theTime->tm_hour = nCurrentHours;
					theTime->tm_min = nCurrentMinutes;
					theTime->tm_sec = 0;
					currentTime = mktime(theTime); 
				} break; // break on year
			case 'M':	
				{
					nCurrentMonth--; if (nCurrentMonth<1) {nCurrentMonth=12;nCurrentYear--;} 
					theTime->tm_year = nCurrentYear-1900; 
					theTime->tm_mon = nCurrentMonth-1;
					theTime->tm_mday = nCurrentDay;
					theTime->tm_hour = nCurrentHours;
					theTime->tm_min = nCurrentMinutes;
					theTime->tm_sec = 0;
					currentTime = mktime(theTime); 
				} break; // break on Month
			case 'D':	d=1; break; // break on day
			case 'H':	h=1; break; // break on hour
			}


			timeRotate -= ((d*86400)+(h*3600)); // -= zero if Y or M

			RT = *localtime( &timeRotate	);

	    nCurrentYear		= RT.tm_year+1900;
			nCurrentMonth		= RT.tm_mon+1;
			nCurrentDay			= RT.tm_mday;
			nCurrentHours		= RT.tm_hour;
			nCurrentMinutes	= RT.tm_min;

		}

		switch (m_chRotatePeriod)
		{
		case 'F': sprintf(pszNextRotateKey, "203900000000"); break; // one logfile! time will never exceed yr 2039 with CTime
		case 'Y': sprintf(pszNextRotateKey,"%04d%02d%02d%02d%02d",nCurrentYear, m_nRotateOffsetMonth,	m_nRotateOffsetDay,	m_nRotateOffsetHours,	m_nRotateOffsetMinutes ); break; // break on year
		case 'M':	sprintf(pszNextRotateKey,"%04d%02d%02d%02d%02d",nCurrentYear, nCurrentMonth,	m_nRotateOffsetDay,	m_nRotateOffsetHours,	m_nRotateOffsetMinutes );break; // break on month
		case 'D':	sprintf(pszNextRotateKey,"%04d%02d%02d%02d%02d",nCurrentYear, nCurrentMonth,	nCurrentDay,	m_nRotateOffsetHours,	m_nRotateOffsetMinutes ); break; // break on day
		case 'H':	sprintf(pszNextRotateKey,"%04d%02d%02d%02d%02d",nCurrentYear, nCurrentMonth,	nCurrentDay,	nCurrentHours,	m_nRotateOffsetMinutes );	break; // break on hour
		}
	
	
		if(m_pszLastRotateKey) strcpy(m_pszLastRotateKey, pszNextRotateKey);


		// ok here, need to create the filename from the format string
		if(pszFormatString) 
		{
			pszFilename = (char*)malloc(_MAX_PATH+1); // cant be more than this anyway.
			if(pszFilename)
			{
				strftime(pszFilename, _MAX_PATH, pszFormatString, &RT );
			}
		}

    if (!bOpen) 
		{ 
			if (m_pfile) 
			{ 
				fclose(m_pfile); 
				m_pfile=NULL; 
			} 
			unlink(pszFilename); 
//			AfxMessageBox("unlink");
		}
  }
  else if (m_pfile)
	{
		if(pszFormatString) { try{ free(pszFormatString);} catch(...){} }
		if(pszFilename)			{ try{ free(pszFilename);} catch(...){} }
		if(pszRotateSpec)		{ try{ free(pszRotateSpec);} catch(...){} }
		return LOG_ROTATED;
	}

  if (m_pfile!=NULL) fclose(m_pfile);

	//make dirs if nec.
	char path_buffer[_MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	char final_buffer[_MAX_PATH];

	strcpy(path_buffer, pszFilename);

	_splitpath( path_buffer, drive, dir, fname, ext );	

	// have to make dir.
	// if dir already exists, no problem, _mkdir just returns and we continue
	// so, we can always just call mkdir

	if(strlen(drive)>0)
	{
		strcpy(final_buffer, drive);
		if((dir[0]!='\\')&&(dir[0]!='/'))
		{
			// need to ensure a dir delimiter between drive and directory
			// bu.MakeFileString below does the same thing.
			strcat(final_buffer, "\\");
		}
		strcat(final_buffer, dir);
	}
	else
	{
		// want to remove any leading directory delimiters in the case of no drive specified (using current directory)
		// actually, we can leave it, it goes to the root dir
		strcpy(final_buffer, dir);
	}

	int nLen = (int)strlen(final_buffer);
	for (int i=0; i<nLen; i++)
	{
		if((final_buffer[i]=='/')||(final_buffer[i]=='\\'))
		{
			path_buffer[i] = 0;
			if(strlen(path_buffer)>0)
				_mkdir(path_buffer);
			path_buffer[i] = '\\';
		}
		else
			path_buffer[i] = final_buffer[i];
	}

	// the fopen will fail if there are illegal characters, so let's strip them
	CBufferUtil bu;
	pszFilename = bu.MakeFileString(pszFilename);

	if(pszFilename == NULL)
	{
		if(pszFormatString) { try{ free(pszFormatString); } catch(...){} }
//		if(pszFilename)			{ try{ free(pszFilename); } catch(...){} }  // no need here, we just checked it
		if(pszRotateSpec)		{ try{ free(pszRotateSpec); } catch(...){} }
		return LOG_ERROR;
	}

	m_pfile = fopen(pszFilename, "a+b");  //open it as binary, append with read
	if (m_pfile == NULL) 
	{
		if(pszFormatString) { try{ free(pszFormatString); } catch(...){} }
		if(pszFilename)			{ try{ free(pszFilename); } catch(...){} }
		if(pszRotateSpec)		{ try{ free(pszRotateSpec); } catch(...){} }
		return LOG_ERROR;
	}
	//setvbuf(m_pfile, NULL, _IONBF, NULL);


	//have to check the first character to set proper encryption mode.
	// determine file size
	fseek( m_pfile, 0, SEEK_END );
	if(ftell(m_pfile)>0)
	{
		// not a new file, lets set proper encyption mode
		char ch;
		fseek( m_pfile, 0, SEEK_SET );
		int nChars = fread( &ch, 1, 1, m_pfile );

		if((nChars>0)&&((ch&0xff)!=0x80)) m_bVerboseFile = true;  // the first byte shall be a time offset if an encrypted file
		else m_bVerboseFile = false;  // default to readable if error.
	}

	fseek(m_pfile,0,SEEK_END);  // append is at end.

	if(!m_bVerboseFile)
	{
//		AfxMessageBox("encrypt");
		// OK we opened a new file, and it has a compressed format.
		// This means we need to dump the offset time and any 
		// existing tables into the top of the new file.
		_timeb timebuffer;
		_ftime( &timebuffer );

		// this time is computer time, not offset.
		m_ulLastUnixTime = timebuffer.time;
		m_usLastMillitime = timebuffer.millitm;

		// add a timeoffset record to the file, but correct with offset.
		WriteTimeOffsetRecord();
		if((m_pszFunctionTable)&&(strlen(m_pszFunctionTable)))	
			fwrite( m_pszFunctionTable, 1, strlen(m_pszFunctionTable), m_pfile );
		if((m_pszDestinationTable)&&(strlen(m_pszDestinationTable)))	
			fwrite( m_pszDestinationTable, 1, strlen(m_pszDestinationTable), m_pfile );
		fflush(m_pfile);
	}
	if(pszFormatString) { try{ free(pszFormatString); } catch(...){} }
	if(pszFilename)			{ try{ free(pszFilename); } catch(...){} }
	if(pszRotateSpec)		{ try{ free(pszRotateSpec); } catch(...){} }
	return LOG_SUCCESS;
}

int CLogUtil::WriteTimeOffsetRecord()
{
	if(m_pfile)
	{
		char buffer[10];

		unsigned long ulCorrectedTime = m_ulLastUnixTime;
		short ssCorrectedMillitime = m_usLastMillitime;

		ulCorrectedTime -=(m_nAbsoluteTimeOffsetMS/1000);
		ssCorrectedMillitime -=(m_nAbsoluteTimeOffsetMS%1000);

		while(ssCorrectedMillitime<0){ulCorrectedTime-=1; ssCorrectedMillitime+=1000;}
		while(ssCorrectedMillitime>999){ulCorrectedTime+=1; ssCorrectedMillitime-=1000;}

		sprintf(buffer, "%c%c%c%c%c%c%c%c%c",
			0x80,
			(unsigned char)((ulCorrectedTime>>24)&(0xff)),
			(unsigned char)((ulCorrectedTime>>16)&(0xff)),
			(unsigned char)((ulCorrectedTime>>8)&(0xff)),
			(unsigned char)((ulCorrectedTime)&(0xff)),
			(unsigned char)((ssCorrectedMillitime>>8)&(0xff)),
			(unsigned char)((ssCorrectedMillitime)&(0xff)),
			(unsigned char)(10),
			(unsigned char)(0) // just in case.  should get another zero after it anyway.
			);

			fwrite( buffer, 1, 8, m_pfile ); // 8 char record, fixed.
			fflush(m_pfile);
		
		return LOG_SUCCESS;
	}
	return LOG_ERROR;
}


int CLogUtil::Synchronize(_timeb timeSynch)
{
	if(timeSynch.time==0) m_nAbsoluteTimeOffsetMS=0;
	else
	{
		_timeb timebuffer;
		_ftime( &timebuffer );

		m_nAbsoluteTimeOffsetMS=(timebuffer.time-timeSynch.time)*1000;
		m_nAbsoluteTimeOffsetMS+=(timebuffer.millitm-timeSynch.millitm);
	}
	return LOG_SUCCESS;
}

int CLogUtil::HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	if(!m_bThreadStarted) // can't handle the messages...
		return LOG_ERROR;

	unsigned long ulReqCount = m_ulReqCount; // the "ID" of this function call.
	m_ulReqCount++; // increment the request for the next possible call.

	EnterCriticalSection(&m_critBuffer);
/*
	while((m_ulSvcCount<ulReqCount)||(m_bBufferInUse))
	{
		// wait.
		Sleep(1);
	}

	m_bBufferInUse = true; // prevent access from file write thread
*/
	
	// check tabulations.
	// if destination is present, check destination table for name, if its not there, add it
	// if caller is present, check caller table for name, if its not there, add it
	// then format the message and add it to the message buffer.
	// when finished with buffer, m_ulSvcCount++;
	
	//buffer record is as follows:
	//[4 bytes unix time][2 bytes millitime][4 bytes flags][variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10]

	unsigned long ulRecordLength = 13;  // 4+2+4+1+1+1
	unsigned long ulMsgLength = 0;
	unsigned long ulCallerLength = 0;
	unsigned long ulDestLength = 0;

	ulMsgLength = strlen(pszMessage);
	char* pszEncMsg = EncodeTen(pszMessage, &ulMsgLength);
	if(!pszEncMsg)
	{
		LeaveCriticalSection(&m_critBuffer);
		return LOG_BADBUFFER;
	}
	ulRecordLength+=ulMsgLength; // encoded length

	char* pszEncCaller = NULL;
	char* pszEncDestination = NULL;

	if(pszCaller)
	{
		ulCallerLength = strlen(pszCaller);
		if(ulCallerLength>0)
		{
			pszEncCaller = EncodeTen(pszCaller, &ulCallerLength);
			if(!pszEncCaller)
			{
	//			free(pszEncMsg);
	//			return LOG_BADBUFFER;  // lets just continue, this is less important than the msg itself
			}
			else  ulRecordLength+=ulCallerLength; // encoded length
		}
	}

	if(pszDestinations)
	{
		ulDestLength = strlen(pszDestinations);
		if(ulDestLength>0)
		{
			pszEncDestination = EncodeTen(pszDestinations, &ulDestLength);
			if(!pszEncDestination)
			{
	//			free(pszEncMsg);
	//			return LOG_BADBUFFER;  // lets just continue, this is less important than the msg itself
			}
			else	ulRecordLength+=ulDestLength; // encoded length
		}
	}

	//expand the buffer.

	int nReturnValue = LOG_SUCCESS;
	char* pch = (char*) malloc(m_ulBufferLength+ulRecordLength+1);
	if(pch)
	{	
		if(m_pchSendBuffer)
		{
			memcpy(pch, m_pchSendBuffer, m_ulBufferLength);
			free(m_pchSendBuffer);
		}
		m_pchSendBuffer = pch;

		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time>>24)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time>>16)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time>>8)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.millitm>>8)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.millitm)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags>>24)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags>>16)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags>>8)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags)&(0xff));

		memcpy((pch+m_ulBufferLength), pszEncMsg, ulMsgLength); m_ulBufferLength+=ulMsgLength;
		*(pch+(m_ulBufferLength++))= (unsigned char)(10); //delim

		if(pszEncCaller) memcpy((pch+m_ulBufferLength), pszEncCaller, ulCallerLength); m_ulBufferLength+=ulCallerLength;
		*(pch+(m_ulBufferLength++))= (unsigned char)(10); //delim

		if(pszEncDestination) memcpy((pch+m_ulBufferLength), pszEncDestination, ulDestLength);  m_ulBufferLength+=ulDestLength;
		*(pch+(m_ulBufferLength++))= (unsigned char)(10); //delim
		memset((pch+m_ulBufferLength), (unsigned char)(0), 1); //zero term the whole thing
/*
///
		unsigned long ulBufL = m_ulBufferLength;
		CBufferUtil bu;
		char* pbuf = bu.ReadableHex(pch, &ulBufL);
		AfxMessageBox(pbuf);
		free(pbuf);
///
*/
	}
	else nReturnValue = LOG_BADBUFFER;

	if(pszEncMsg) free(pszEncMsg);
	if(pszEncCaller) free(pszEncCaller);
	if(pszEncDestination) free(pszEncDestination);

//	m_bBufferInUse = false; // release buffer access
	m_ulSvcCount++;
	if(m_ulSvcCount == m_ulReqCount) // all requests serviced, can reset counter
		m_ulSvcCount = m_ulReqCount = 0;
	LeaveCriticalSection(&m_critBuffer);

	return nReturnValue;
}

int CLogUtil::Modify(char* pchParams, unsigned long* pulBufferLen)
{
	// this function MUST be overridden by objects
	return 0;
}


void FileHandlerThread(void* pvArgs)
{
	CLogUtil* plog = (CLogUtil*)pvArgs;
	if(plog)
	{
		plog->m_bThreadStarted = true;
		while (!plog->m_bKillThread)
		{
			// check buffer for new messages, and ability to write them.
			if((plog->m_ulBufferLength>0)&&(!plog->m_bFileInUse))//&&(!plog->m_bBufferInUse))
			{
				EnterCriticalSection(&plog->m_critBuffer);
//				plog->m_bBufferInUse=true;
				plog->m_bFileInUse=true;

				// if they exist, lock the buffer, process record by record into file.
				if((plog->m_pchSendBuffer)&&(plog->m_pfile))
				{
					char* pch = plog->m_pchSendBuffer;

//					unsigned long ulBufLen = plog->m_ulBufferLength;	CBufferUtil bu;  char* px = bu.ReadableHex(plog->m_pchSendBuffer, &ulBufLen);
//					AfxMessageBox(px); free(px);

					while((pch)&&(plog)&&(pch<plog->m_pchSendBuffer+plog->m_ulBufferLength)) // in other words, while there are still records left
					{
						//first, check the time, then rotate;
// buffer record:
//[4 bytes unix time][2 bytes millitime][4 bytes flags][variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10]
						_timeb timestamp;
						timestamp.time  = (0xff000000&((*pch&0xff)<<24)); pch++;
						timestamp.time |= (0x00ff0000&((*pch&0xff)<<16)); pch++;
						timestamp.time |= (0x0000ff00&((*pch&0xff)<<8));  pch++;
						timestamp.time |= (0x000000ff&(*pch&0xff));				pch++;

						timestamp.millitm  = (0xff00&((*pch&0xff)<<8)); pch++;
						timestamp.millitm |= (0x00ff&(*pch&0xff));			pch++;

						plog->RotateLog(timestamp.time);

						unsigned long ulFlags = 0;
						ulFlags  = (0xff000000&((*pch&0xff)<<24));	pch++;
						ulFlags |= (0x00ff0000&((*pch&0xff)<<16));	pch++;
						ulFlags |= (0x0000ff00&((*pch&0xff)<<8));		pch++;
						ulFlags |= (0x000000ff&(*pch&0xff));				pch++;


						if(plog->m_bVerboseFile)
						{
							// full text description, dont need to check anything.
							// decompile the buffer record
							// write out timestamp, icon, message, calling func, destination.
							if(plog->m_pfile)
							{
								//first, write the timestamp and icon
								//timestamp is 2004-Jan-31 23:59:59.999 (24 chars)
								//icon and severity are 4 chars: "[!1]"
								// separators are 3 chars: " - "
								//"2004-Jan-31 23:59:59.999 [!1] "

								unsigned long ulTempBufferLen = 30;

								char buffer[48]; // need 33;
								tm* theTime = localtime( &timestamp.time	);
								strftime( buffer, 40, "%Y-%b-%d %H:%M:%S.", theTime );

								char ch = ' ';
								if((ulFlags&0x000000f0)!=MSG_PRI_NORMAL)
									ch = (char)((ulFlags>>4)&0x0000000f)+48;

								switch(ulFlags&0x0000000f)
								{
								case MSG_ICONQUESTION://						0x00000001  // ? icon
									{
										sprintf(buffer+21, "%03d [?%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONEXCLAMATION://					0x00000002  // ! icon
									{
										sprintf(buffer+21, "%03d [!%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONERROR://								0x00000003  // X icon
									{
										sprintf(buffer+21, "%03d [X%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONSTOP://								0x00000004  // stop sign icon
									{
										sprintf(buffer+21, "%03d [S%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONHAND://								0x00000005  // hand icon
									{
										sprintf(buffer+21, "%03d [H%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONINFO://								0x00000006  // (i) icon 
									{
										sprintf(buffer+21, "%03d [i%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONUSER1://								0x00000007  
									{
										sprintf(buffer+21, "%03d [1%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONUSER2://								0x00000008  
									{
										sprintf(buffer+21, "%03d [2%c] ", timestamp.millitm, ch);
									} break;
								case MSG_ICONNONE://								0x00000000  // default, nothing
								default:
									{
										if(ch==' ')
											sprintf(buffer+21, "%03d      ", timestamp.millitm);
										else
											sprintf(buffer+21, "%03d [ %c] ", timestamp.millitm, ch);
									} break;
								}

								unsigned char ucDelim=(char)10;

								char* pchMsg = pch; //strtok( pch, &ucDelim );
								while((((*pchMsg)&0xff)!=ucDelim)&&(((*pchMsg)&0xff)!=0))	pchMsg++; // go to next delim.

								unsigned long ulCallerBufferLen=0;
								char* pchCaller = NULL;

								unsigned long ulDestBufferLen=0;
								char* pchDest = NULL;

								unsigned long ulBufferLen = (pchMsg-pch);
								char* pchDecMsg = plog->DecodeTen(pch, &ulBufferLen, TEN_DECODE_CRLF);  // text mode, make it crlf
								if(pchDecMsg)
								{
									// free(pch);  // do not want to free pch, its in the middle somewhere!
									// need to calculate the length of the buffer we need
									// if there are CRLFs, we want to put each line on its own line, but also
									// put leading spaces to justify the text.

									pch = strstr(pchDecMsg, "\r\n");
									while(pch!=NULL)
									{
										ulBufferLen+=30; // the length of the indent.
										pch = strstr(pch+2, "\r\n");
									}

								}
								ulTempBufferLen+=ulBufferLen;

								// put a delimiter
								ulTempBufferLen+=3;

								pch = ++pchMsg;

								if(*pch!=ucDelim)
								{
									while(((*pchMsg&0xff)!=ucDelim)&&((*pchMsg&0xff)!=0))	pchMsg++; // go to next delim.
									ulCallerBufferLen = (pchMsg-pch);
									pchCaller = pch; //plog->DecodeTen(pch, &ulCallerBufferLen);
									ulTempBufferLen+=ulCallerBufferLen;

									pch = ++pchMsg;
								}
								else
								{
									pch++;
								}

								// put a delimiter
								ulTempBufferLen+=3;

								if(*pch!=ucDelim)
								{
									while(((*pchMsg&0xff)!=ucDelim)&&((*pchMsg&0xff)!=0))	pchMsg++; // go to next delim.
									ulDestBufferLen = (pchMsg-pch);
									pchDest = pch; //plog->DecodeTen(pch, &ulDestBufferLen);
									ulTempBufferLen+=ulDestBufferLen;

									pch = ++pchMsg;
								}
								else
								{
									pch++;
								}


								ulTempBufferLen+=2;
								char* pchTempBuffer = (char*) malloc(ulTempBufferLen+1);
								if(pchTempBuffer)
								{
									unsigned long ulBufPtr = 30;
									memcpy(pchTempBuffer, buffer, 30);

									char* pchBegin	= pchDecMsg;
									char* pchEnd		= strstr(pchDecMsg, "\r\n");

									if(pchEnd==NULL)
									{
										// its a one liner.
										memcpy(pchTempBuffer+ulBufPtr, pchDecMsg, ulBufferLen);
										sprintf(buffer, " - ");
										memcpy(pchTempBuffer+30+ulBufferLen, buffer, 3);
										if(pchCaller) memcpy(pchTempBuffer+33+ulBufferLen, pchCaller, ulCallerBufferLen);
										memcpy(pchTempBuffer+33+ulBufferLen+ulCallerBufferLen, buffer, 3);
										if(pchDest) memcpy(pchTempBuffer+36+ulBufferLen+ulCallerBufferLen, pchDest, ulDestBufferLen);
										*(pchTempBuffer+36+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 13;
										*(pchTempBuffer+37+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 10;
										*(pchTempBuffer+38+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 0;
									}
									else
									{
										memcpy(pchTempBuffer+ulBufPtr, pchBegin, pchEnd-pchBegin); ulBufPtr+=(pchEnd-pchBegin);
										sprintf(buffer, " - ");
										memcpy(pchTempBuffer+ulBufPtr, buffer, 3);  ulBufPtr+=3;
										if(pchCaller) {memcpy(pchTempBuffer+ulBufPtr, pchCaller, ulCallerBufferLen); ulBufPtr+=ulCallerBufferLen;}
										memcpy(pchTempBuffer+ulBufPtr, buffer, 3); ulBufPtr+=3;
										if(pchDest) {memcpy(pchTempBuffer+ulBufPtr, pchDest, ulDestBufferLen); ulBufPtr+=ulDestBufferLen;}
										*(pchTempBuffer+ulBufPtr) = 13; ulBufPtr++;
										*(pchTempBuffer+ulBufPtr) = 10; ulBufPtr++;
										while(pchEnd!=NULL)
										{
											memset(pchTempBuffer+ulBufPtr, ' ', 30); ulBufPtr+=30;
											pchBegin = pchEnd+2; // skip the \r\n
											pchEnd = strstr(pchBegin, "\r\n");
											if(pchEnd==NULL)
											{
												// last line
												memcpy(pchTempBuffer+ulBufPtr, pchBegin, strlen(pchBegin)); ulBufPtr+=strlen(pchBegin);
											}
											else
											{
												// intermediate
												memcpy(pchTempBuffer+ulBufPtr, pchBegin, (pchEnd-pchBegin)); ulBufPtr+=(pchEnd-pchBegin);
											}
											*(pchTempBuffer+ulBufPtr) = 13; ulBufPtr++;
											*(pchTempBuffer+ulBufPtr) = 10; ulBufPtr++;
										}
										*(pchTempBuffer+ulBufPtr) = 0;
									}
/*
									memcpy(pchTempBuffer+ulBufPtr, pchDecMsg, ulBufferLen);
									sprintf(buffer, " - ");
									memcpy(pchTempBuffer+30+ulBufferLen, buffer, 3);
									if(pchCaller) memcpy(pchTempBuffer+33+ulBufferLen, pchCaller, ulCallerBufferLen);
									memcpy(pchTempBuffer+33+ulBufferLen+ulCallerBufferLen, buffer, 3);
									if(pchDest) memcpy(pchTempBuffer+36+ulBufferLen+ulCallerBufferLen, pchDest, ulDestBufferLen);
									*(pchTempBuffer+36+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 13;
									*(pchTempBuffer+37+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 10;
									*(pchTempBuffer+38+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 0;
//									sprintf(buffer, "%c%c",13,10);
*/
//									AfxMessageBox(pchTempBuffer);

									bool bRepeat=false;
									if(plog->m_pszLastMessage)
									{
										if(strcmp(plog->m_pszLastMessage+30, pchTempBuffer+30)==0) //match
										{
											plog->m_ulLastMessageCount++;
											bRepeat=true;
										}
									}

									if(bRepeat)
									{
										if(plog->m_bAllowRepeat)
										{
											//write it anyway
											fwrite( pchTempBuffer, 1, ulTempBufferLen, plog->m_pfile );
											fflush(plog->m_pfile);
										}
									}
									else
									{
										if(plog->m_ulLastMessageCount>0)
										{
											char chRpt[256];

											if(plog->m_pszLastMessage)
												memcpy(chRpt, plog->m_pszLastMessage, 30); // last repeat time
											else
												memcpy(chRpt, pchDecMsg, 30); // current values

											sprintf(chRpt+30, "...repeated %d times...%c%c%c", plog->m_ulLastMessageCount, 13, 10, 0);

											fwrite( chRpt, 1, strlen(chRpt), plog->m_pfile );
											fflush(plog->m_pfile);
										}

//					unsigned long ulBufLen = ulBufferLen+6+ulCallerLen+ulDestLen;	CBufferUtil bu;  char* px = bu.ReadableHex(pchDecMsg, &ulBufLen);
//					AfxMessageBox(px); free(px);

										fwrite( pchTempBuffer, 1, ulTempBufferLen, plog->m_pfile );
										fflush(plog->m_pfile);

										plog->m_ulLastMessageCount=0;
									}
									if(plog->m_pszLastMessage) free(plog->m_pszLastMessage);
									plog->m_pszLastMessage=pchTempBuffer; // updates repeat with new time.

								}
								else
								{
									// go to file directly.
									fwrite( buffer, 1, 30, plog->m_pfile );
									fwrite( pchDecMsg, 1, ulBufferLen, plog->m_pfile );
									sprintf(buffer, " - ");
									fwrite(buffer, 1, 3, plog->m_pfile);
									if(pchCaller) fwrite( pchCaller, 1, ulCallerBufferLen, plog->m_pfile ); 
									fwrite(buffer, 1, 3, plog->m_pfile);
									if(pchDest) fwrite( pchDest, 1, ulDestBufferLen, plog->m_pfile ); 
									sprintf(buffer, "%c%c",13,10);
									fwrite(buffer, 1, 2, plog->m_pfile);
									fflush(plog->m_pfile);
								}

 // must not free these, they are pointers into an existing buffer
		//						if(pchCaller) { free(pchCaller);  ulCallerBufferLen=0;	} 
		//						if(pchDest)   { free(pchDest);		ulDestBufferLen=0;		}
  // must free the allocated buffer for decodede messages
								if(pchDecMsg) { free(pchDecMsg);  ulBufferLen=0;				}

							}
						}
						else
						{
//					unsigned long ulBufLen = plog->m_ulBufferLength;	CBufferUtil bu;  char* px = bu.ReadableHex(plog->m_pchSendBuffer, &ulBufLen);
//					AfxMessageBox(px); free(px);
							// check elapsed time for new offset
							// check callers, if not in table, add to table and write record.
							// check destinations, if not in table, add to table and write record(s).
							// compile file record and write to file.
// [3 bytes - time in milliseconds elapsed since last table entry for reference time - the first bit is zero][1 byte, calling function][1 byte, severity and icon][1 byte, destinations][information, variable bytes (char 10 encoded as "|~", "|" encoded as "||")][char 10 (newline)]

							unsigned long ulOffset = (timestamp.time-plog->m_ulLastUnixTime)*1000
								+ (timestamp.millitm-plog->m_usLastMillitime)
								- plog->m_nAbsoluteTimeOffsetMS;

							if(ulOffset>0x7FFFFF)
							{
								// reset time synch.
								plog->m_ulLastUnixTime = timestamp.time;
								plog->m_usLastMillitime = timestamp.millitm;
								// absolute offset stays the same.

								ulOffset = 0; // this record has zero offset!
								//write out a time record
								plog->WriteTimeOffsetRecord();
							}
							// before we write the record, we need to decompile it to check the callers and destinations tables.

							if(plog->m_pfile)
							{
								unsigned char ucDelim=(char)10;
								unsigned char ucCallerIdx=0x00;
								unsigned char ucDestIdx=0x00;
								unsigned char ucSevIcon=(char)(ulFlags&(0xff));

								char* pchMsg = pch;
								
								while(*pchMsg!=ucDelim) pchMsg++; // go to first delim.
								pchMsg++; // go past first delim.

								unsigned long ulBufferLen=0;
								unsigned long ulCallerLen=0;
								unsigned long ulDestLen=0;
								char* pchDecMsg = pchMsg;
								char* pchDecCaller = pchMsg;
								char* pchDecDest = pchMsg;
								char* pchTable = NULL;

								if(((*pchMsg)&0xff)!=ucDelim)
								{
									while((((*pchDecCaller)&0xff)!=ucDelim)&&(((*pchDecCaller)&0xff)!=0))	pchDecCaller++; // go to next delim.

									ulBufferLen = pchDecCaller - pchMsg;
									if(ulBufferLen>0)
									{
										pchDecCaller = (char*)malloc(ulBufferLen+2); // record term plus zero term 
										bool bExists = false;
										if(pchDecCaller)
										{
											ulCallerLen = ulBufferLen;
											memcpy(pchDecCaller, pchMsg, ulBufferLen);
											memset(pchDecCaller+ulBufferLen, ucDelim, 1);
											memset(pchDecCaller+ulBufferLen+1, 0, 1);
											//check table for this.
											if((plog->m_pszFunctionTable)&&(strlen(plog->m_pszFunctionTable)>0))
											{
												char* pszMarker = plog->m_pszFunctionTable;
												do
												{
													pchTable = strstr(pszMarker, pchDecCaller );

													if((pchTable!=NULL)&&(pchTable>plog->m_pszFunctionTable+1)&&(((*(pchTable-2))&0xff)==0x81)
														&&((*(pchTable+strlen(pchDecCaller)-1)&0xff)==ucDelim))
													{ //its there
														bExists = true;
														ucCallerIdx = ((*(pchTable-1))&0xff);
													}
													else
													{
														pszMarker = pchTable+1;
													}
												}	while((pchTable!=NULL)&&(!bExists));

											}

											if(!bExists) // have to add it.
											{
												unsigned long ulTableLength = 0;
												pchTable = NULL;
												if(plog->m_pszFunctionTable) ulTableLength = strlen(plog->m_pszFunctionTable);
												pchTable = (char*) malloc(ulTableLength+strlen(pchDecCaller)+3); //3= record type byte, caller idx byte, (final char 10 is included), term zero
												if(pchTable)
												{
													if(plog->m_pszFunctionTable)
													{
														memcpy(pchTable, plog->m_pszFunctionTable, ulTableLength);
														free(plog->m_pszFunctionTable);
														//find last value of caller index.
														char* pchTmp = pchTable+strlen(pchTable);

														while((pchTmp>pchTable)&&(!((((*pchTmp)&0xff)==0x81)&&((*(pchTmp-1)&0xff)==ucDelim)) ) ) 
															pchTmp--;

														if(pchTmp+1<pchTable+strlen(pchTable))
														{
															ucCallerIdx = ((*(pchTmp+1))&0xff);
														}
													}
													plog->m_pszFunctionTable = pchTable;

													if(ucCallerIdx<0xff)
													{
														ucCallerIdx++;

														pchTable+=ulTableLength;
														memset(pchTable, 0x81, 1 ); 
														memset(pchTable+1, ucCallerIdx, 1 );
														memcpy(pchTable+2, pchDecCaller, strlen(pchDecCaller)); // delim
				//										memset(pchTable+2+strlen(pchDecCaller), ucDelim, 1); //record end
														memset(pchTable+2+strlen(pchDecCaller), 0, 1); //term zero


//					unsigned long ulBufLen = strlen(plog->m_pszFunctionTable);	CBufferUtil bu;  char* px = bu.ReadableHex(plog->m_pszFunctionTable, &ulBufLen);
//					AfxMessageBox(px); free(px);

														// have to write the record to the file as well.
														if((plog->m_pszFunctionTable)&&(strlen(plog->m_pszFunctionTable)))	
															fwrite( pchTable, 1, strlen(pchDecCaller)+2, plog->m_pfile );
														fflush(plog->m_pfile);
													}
													else // just have to skip it.
													{
														memset(pchTable+ulTableLength, 0, 1);
													}
												}
											}
//											free(pchDecMsg);
										}
										//else //skip it.
										pchMsg+=ulBufferLen+1;
									} 
									else pchMsg++;
								}
								else // no caller, skip
									pchMsg++;

								pchDecDest = pchMsg;
								pchTable = NULL;
								ulBufferLen=0;

								if(((*pchMsg)&0xff)!=ucDelim)
								{
									while((((*pchDecDest)&0xff)!=ucDelim)&&(((*pchDecDest)&0xff)!=0))	pchDecDest++; // go to next delim.
									ulBufferLen = pchDecDest - pchMsg;
									if(ulBufferLen>0)
									{
										pchDecDest = (char*)malloc(ulBufferLen+2); // record term plus zero term 
										// the buffer has a comma-delimited set of destination names.
										// we dont parse them out individually.  
										// instead we have 255 possible combinations.
										bool bExists = false;
										if(pchDecDest)
										{
											ulDestLen = ulBufferLen;
											memcpy(pchDecDest, pchMsg, ulBufferLen);
											memset(pchDecDest+ulBufferLen, ucDelim, 1);
											memset(pchDecDest+ulBufferLen+1, 0, 1);
											//check table for this.
											if((plog->m_pszDestinationTable)&&(strlen(plog->m_pszDestinationTable)>0))
											{
												char* pszMarker = plog->m_pszDestinationTable;
												do
												{
													pchTable = strstr(pszMarker, pchDecDest );

													if((pchTable!=NULL)&&(pchTable>plog->m_pszDestinationTable+1)&&(((*(pchTable-2))&0xff)==0x82)
														&&((*(pchTable+strlen(pchDecDest)-1)&0xff)==ucDelim))
													{ //its there
														bExists = true;
														ucDestIdx = ((*(pchTable-1))&0xff);
													}
													else
													{
														pszMarker = pchTable+1;
													}
												}	while((pchTable!=NULL)&&(!bExists));

											}

											if(!bExists) // have to add it.
											{
												unsigned long ulTableLength = 0;
												pchTable = NULL;
												if(plog->m_pszDestinationTable) ulTableLength = strlen(plog->m_pszDestinationTable);
												pchTable = (char*) malloc(ulTableLength+strlen(pchDecDest)+3); //3= record type byte, dest idx byte, (final char 10 is included), term zero
												if(pchTable)
												{
													if(plog->m_pszDestinationTable)
													{
														memcpy(pchTable, plog->m_pszDestinationTable, ulTableLength);
														free(plog->m_pszDestinationTable);
														//find last value of dest index.
														char* pchTmp = pchTable+strlen(pchTable);

														while((pchTmp>pchTable)&&(!((((*pchTmp)&0xff)==0x82)&&((*(pchTmp-1)&0xff)==ucDelim)) ) ) 
															pchTmp--;

														if(pchTmp+1<pchTable+strlen(pchTable))
														{
															ucDestIdx = ((*(pchTmp+1))&0xff);
														}
													}
													plog->m_pszDestinationTable = pchTable;

													if(ucDestIdx<0xff)
													{
														ucDestIdx++;

														pchTable+=ulTableLength;
														memset(pchTable, 0x82, 1 ); 
														memset(pchTable+1, ucDestIdx, 1 );
														memcpy(pchTable+2, pchDecDest, strlen(pchDecDest));
				//										memset(pchTable+2+strlen(pchDecDest), ucDelim, 1); //record end
														memset(pchTable+2+strlen(pchDecDest), 0, 1); //term zero

//					unsigned long ulBufLen = strlen(plog->m_pszDestinationTable);	CBufferUtil bu;  char* px = bu.ReadableHex(plog->m_pszDestinationTable, &ulBufLen);
//					AfxMessageBox(px); free(px);

														// have to write the record to the file as well.
														if((plog->m_pszDestinationTable)&&(strlen(plog->m_pszDestinationTable)))	
															fwrite( pchTable, 1, strlen(pchDecDest)+2, plog->m_pfile );
														fflush(plog->m_pfile);
													}
													else // just have to skip it.
													{
														memset(pchTable+ulTableLength, 0, 1);
													}
												}
											}
//											free(pchDecDest);
										}
										//else //skip it.
									} 
								}

								// ok so now we can write the actual record.
								pchMsg = pch;
								
								while(*pchMsg!=ucDelim) pchMsg++; // go to first delim, which is the end of msg marker

								ulBufferLen = (pchMsg-pch)+1; 

								pchDecMsg = (char*)malloc(7+ulBufferLen+ulCallerLen+ulDestLen); //time offset(6) + caler and dest term 0;
								if(pchDecMsg)
								{

// [3 bytes - time in milliseconds elapsed since last table entry for reference time - the first bit is zero][1 byte, calling function][1 byte, severity and icon][1 byte, destinations][information, variable bytes (char 10 encoded as "|~", "|" encoded as "||")][char 10 (newline)]
/*
									sprintf( pchDecMsg, "%c%c%c%c%c%c",
										((ulOffset&(0x007fffff))>>16)&0xff,
										((ulOffset&(0x007fffff))>>8)&0xff,
										(ulOffset&(0x007fffff))&0xff,
										ucCallerIdx,
										ucSevIcon,
										ucDestIdx
										);
*/
									memset(pchDecMsg,   ((ulOffset&(0x007fffff))>>16)&0xff, 1 );
									memset(pchDecMsg+1, ((ulOffset&(0x007fffff))>>8)&0xff, 1 );
									memset(pchDecMsg+2, (ulOffset&(0x007fffff))&0xff, 1 );
									memset(pchDecMsg+3, ucCallerIdx, 1 );
									memset(pchDecMsg+4, ucSevIcon, 1 );
									memset(pchDecMsg+5, ucDestIdx, 1 );
									memcpy(pchDecMsg+6, pch, ulBufferLen );
									if((pchDecCaller)&&(ulCallerLen)) memcpy(pchDecMsg+6+ulBufferLen, pchDecCaller, ulCallerLen );
									if((pchDecDest)&&(ulDestLen)) memcpy(pchDecMsg+6+ulBufferLen+ulCallerLen, pchDecDest, ulDestLen );
									memset(pchDecMsg+6+ulBufferLen+ulCallerLen+ulDestLen, 0, 1 );

									bool bRepeat=false;
									if(plog->m_pszLastMessage)
									{
										if(strcmp(plog->m_pszLastMessage+6, pchDecMsg+6)==0) //match
										{
											plog->m_ulLastMessageCount++;
											bRepeat=true;
										}
									}

									if(bRepeat)
									{
										if(plog->m_bAllowRepeat)
										{
											//write it anyway
											fwrite( pchDecMsg, 1, ulBufferLen+6, plog->m_pfile );
											fflush(plog->m_pfile);
										}
									}
									else
									{
										if(plog->m_ulLastMessageCount>0)
										{
											// have to write out repeat line, which we encode
											// [6 bytes time and flags][char 0x13 (rpt signifier)][1 byte, num bytes for len][1 to 4 bytes repeat#][char 10]
											unsigned char buffer[16]; // need 13
											unsigned char ucCount, ucBuf; 

											if(plog->m_ulLastMessageCount&0xff000000) ucCount = 4;
											else if(plog->m_ulLastMessageCount&0xff0000) ucCount = 3;
											else if(plog->m_ulLastMessageCount&0xff00) ucCount = 2;
											else ucCount = 1;

											if(plog->m_pszLastMessage)
												memcpy(buffer, plog->m_pszLastMessage, 6); // last repeat time
											else
												memcpy(buffer, pchDecMsg, 6); // current values

											*(buffer+6) = 0x13;
											*(buffer+7) = ucCount; ucBuf=8;
											*(buffer+ucBuf++) = (unsigned char)(plog->m_ulLastMessageCount&0xff);
											if(ucCount>1) *(buffer+ucBuf++) = (unsigned char)((plog->m_ulLastMessageCount>>8)&0xff);
											if(ucCount>2) *(buffer+ucBuf++) = (unsigned char)((plog->m_ulLastMessageCount>>16)&0xff);
											if(ucCount>3) *(buffer+ucBuf++) = (unsigned char)((plog->m_ulLastMessageCount>>24)&0xff);
											*(buffer+ucBuf++) = 10;

											fwrite( buffer, 1, ucBuf, plog->m_pfile );
											fflush(plog->m_pfile);
										}

//					unsigned long ulBufLen = ulBufferLen+6+ulCallerLen+ulDestLen;	CBufferUtil bu;  char* px = bu.ReadableHex(pchDecMsg, &ulBufLen);
//					AfxMessageBox(px); free(px);

										fwrite( pchDecMsg, 1, (ulBufferLen+6), plog->m_pfile );
										fflush(plog->m_pfile);

										plog->m_ulLastMessageCount=0;
									}
									if(plog->m_pszLastMessage) free(plog->m_pszLastMessage);
									plog->m_pszLastMessage=pchDecMsg; // updates repeat with new time.

								}

								if(pchDecCaller){ free(pchDecCaller); ulCallerLen=0; }
								if(pchDecDest)  { free(pchDecDest);   ulDestLen=0;   }

								pch+=ulBufferLen+1; //should be past first delim;
								while((*pch!=ucDelim)&&(*pch!=0)) pch++; // go to second delim, which is the end of record
								pch++;
								while((*pch!=ucDelim)&&(*pch!=0)) pch++; // go to last (third) delim, which is the end of record
								pch++; // go past end of record, should be 0 or next record

							} //if(plog->m_pfile)
						} // end else not verbose
					} //end while

					if(plog->m_pchSendBuffer) free(plog->m_pchSendBuffer);
					plog->m_pchSendBuffer=NULL;
					plog->m_ulBufferLength=0;
				}
				// release file and buffer
				plog->m_bFileInUse=false;
//				plog->m_bBufferInUse=false;
				LeaveCriticalSection(&plog->m_critBuffer);
			} // else theres nothing to do
			else
			{
				// give the processor a break for a few flops 
				Sleep(1);
			}

		}  // end while dont kill
		plog->m_bThreadStarted = false;
	} // if log
}

char* CLogUtil::ReturnRecords(unsigned long* pulNumRecords, char nMode)
{
//THIS FUNCTION HAS SEVERE PROBLEMS.  NEEDS DEBUGGING !

	// takes in a pointer with the value of number of records requested.
	// returns buffer length in same pointer

	char* pch = NULL;
	unsigned long ulBufferLen=0;
	if(!m_bVerboseFile)
	{
		//first check if returning tables
		if((nMode&LOG_GETTABLES)&&(!(nMode&(LOG_GETALL|LOG_GETFROMTOP)))) // if all, we xmit everything in the file, so dont repeat it.
		{
/*
			while(m_bBufferInUse)
			{
				Sleep(1); //wait
			}
			m_bBufferInUse = true;
*/
			if(m_pszFunctionTable) ulBufferLen+=strlen(m_pszFunctionTable);
			if(m_pszDestinationTable) ulBufferLen+=strlen(m_pszDestinationTable);
			pch = (char*) malloc(ulBufferLen+1);
			if(pch)
			{
					*pch = 0;
					strncat(pch, m_pszFunctionTable, strlen(m_pszFunctionTable));
					strncat(pch, m_pszDestinationTable, strlen(m_pszDestinationTable));
			}
//			m_bBufferInUse = false;
		}

		if(m_pfile)
		{
			while(m_bFileInUse)
			{
				Sleep(1); //wait
			}
			m_bFileInUse=true;

			if(nMode&LOG_GETALL)
			{
							// determine file size
				fseek(m_pfile,0,SEEK_END);
				ulBufferLen = ftell(m_pfile);
				fseek(m_pfile,0,SEEK_SET);

				pch = (char*) malloc(ulBufferLen+1);
				if(pch)
				{
					fread(pch,sizeof(char),ulBufferLen,m_pfile);
				}
				fseek(m_pfile,0,SEEK_END);
				m_bFileInUse=false;

			}
			else if(nMode&LOG_GETFROMTOP)
			{
				// in order to reduce file access, we load the file into memory and parse from there.
							// determine file size
				fseek(m_pfile,0,SEEK_END);
				unsigned long ulBufferSize = ftell(m_pfile);
				fseek(m_pfile,0,SEEK_SET);

				char* pchBuffer = (char*) malloc(ulBufferSize+1);
				if(pchBuffer)
				{
					fread(pchBuffer,sizeof(char),ulBufferSize,m_pfile);
				}
				fseek(m_pfile,0,SEEK_END);
				m_bFileInUse=false;

				if(pchBuffer)
				{
					unsigned long ulAllocLen=0;
					unsigned long ulRecordsFound=0;
					char chRecordType=0x00; //undetermined
					unsigned long ulCharCount=0;
					unsigned long ulBufferPointer=0;

					char* pchTemp = NULL;
					while (
								  (ulBufferPointer<ulBufferSize)
								&&(ulRecordsFound<*pulNumRecords)
								)
					{
						if((ulBufferLen>=ulAllocLen)||(pchTemp == NULL)) 
						{
							pchTemp = (char*)malloc(ulAllocLen+1024); // expand buffer size by 1K

							if(pchTemp)
							{
								if(pch)
								{
									memcpy(pchTemp, pch, ulBufferLen);
									free(pch);
								}
								pch = pchTemp;
								ulAllocLen+=1024;
							}
						}
						if(pch)
						{
							memcpy(pch+ulBufferLen, pchBuffer+ulBufferPointer, 1);  ulBufferPointer++;
							switch(chRecordType)
							{
							case 0x80:
								{
									// we need to count off 7 characters
									ulCharCount++;
									if(ulCharCount>=7) //seventh char is end of record;
									{
										chRecordType = (char)0x00;
										ulCharCount=0;
									}
									ulBufferLen++;
								} break;
							case 0x81: 
								{
									ulCharCount++;
									if(ulCharCount>1) //must pass index, then look for rec end;
									{
										if(*(pch+ulBufferLen)==10)
										{
											chRecordType = (char)0x00;
											ulCharCount=0;
										}
									}
									ulBufferLen++;
								} break;
							case 0x82:
								{
									ulCharCount++;
									if(ulCharCount>1) //must pass index, then look for rec end;
									{
										if(*(pch+ulBufferLen)==10)
										{
											chRecordType = (char)0x00;
											ulCharCount=0;
										}
									}
									ulBufferLen++;
								} break;
							case 0xff: 
								{
									ulCharCount++;
									if(ulCharCount>5) //must pass flags, then look for rec end;
									{
										if(*(pch+ulBufferLen)==10)
										{
											ulRecordsFound++;
											chRecordType = (char)0x00;
											ulCharCount=0;
										}
									}
									ulBufferLen++;
								} break;
							case 0x00: // undetermined
							default: 
								{
									ulBufferLen++;
									ulCharCount=0;
									if(*(pch+ulBufferLen)==0x80)
									{ //time record
										chRecordType = (char)0x80;
									}
									else if(*(pch+ulBufferLen)==0x81)
									{ //caller record
										chRecordType = (char)0x81;
									}
									else if(*(pch+ulBufferLen)==0x82)
									{ //destination record
										chRecordType = (char)0x82;
									}
									else 
									{ //log record
										chRecordType = (char)0xff;
									}

								} break;
							}
						}
					} // end while
					free(pchBuffer);
				}
			}
			else // get from bottom
			{

/*  NOT SUPPORTED YET - do later when time. (20112004)
				
				// in order to reduce file access, we load the file into memory and parse from there.
							// determine file size
				fseek(m_pfile,0,SEEK_END);
				unsigned long ulBufferSize = ftell(m_pfile);
				fseek(m_pfile,0,SEEK_SET);

				char* pchBuffer = (char*) malloc(ulBufferSize+1);
				if(pchBuffer)
				{
					fread(pchBuffer,sizeof(char),ulBufferSize,m_pfile);
				}
				fseek(m_pfile,0,SEEK_END);
				m_bFileInUse=false;

				if(pchBuffer)
				{
					// parse records

					// parsing backwards is harder than parsing forwards, because all records end the same - with char 10.
					// so, we start at the top, and count down, adding up to *pulNumRecords records to the buffer.
					// if we still have file left over, we remove the top record from the buffer and add the next to the bottom.
					// eventually what is returned is the last *pulNumRecords records.

					unsigned long ulAllocLen=0;
					unsigned long ulRecordsFound=0;
					char chRecordType=0x00; //undetermined
					unsigned long ulCharCount=0;
					unsigned long ulBufferPointer=0;
					unsigned long ulRecordBufferLen=0;
xxx
					char* pchTemp = NULL;  // buffer to temp expand, 
					char* pchRecord = NULL; // buffer to take record, before deciding what to do with it.
					char* pchRecords = NULL; // buffer to add records to, adding to pch eventually

					while (
								  (ulBufferPointer<ulBufferSize)
								&&(ulRecordsFound<*pulNumRecords)
								)
					{
						if((ulRecordBufferLen>=ulAllocLen)||(pchTemp == NULL)) 
						{
							pchTemp = (char*)malloc(ulAllocLen+1024); // expand buffer size by 1K

							if(pchTemp)
							{
								if(pchRecords)
								{
									memcpy(pchTemp, pchRecords, ulRecordBufferLen);
									free(pchRecords);
								}
								pszRecords = pchTemp;
								ulAllocLen+=1024;
							}
						}
						if(pchRecords)
						{
							memcpy(pchRecords+ulRecordBufferLen, pchBuffer+ulBufferPointer, 1);  ulBufferPointer++;
							switch(chRecordType)
							{
							case 0x80:
								{

xxxx edited to here
								// we need to count off 7 characters
									ulCharCount++;
									if(ulCharCount>=7) //seventh char is end of record;
									{
										chRecordType = (char)0x00;
										ulCharCount=0;
									}
									ulRecordBufferLen++;
								} break;
							case 0x81: 
								{
									ulCharCount++;
									if(ulCharCount>1) //must pass index, then look for rec end;
									{
										if(*(pchRecords+ulRecordBufferLen)==10)
										{
											chRecordType = (char)0x00;
											ulCharCount=0;
										}
									}
									ulRecordBufferLen++;
								} break;
							case 0x82:
								{
									ulCharCount++;
									if(ulCharCount>1) //must pass index, then look for rec end;
									{
										if(*(pchRecords+ulRecordBufferLen)==10)
										{
											chRecordType = (char)0x00;
											ulCharCount=0;
										}
									}
									ulRecordBufferLen++;
								} break;
							case 0xff: 
								{
									ulCharCount++;
									if(ulCharCount>5) //must pass flags, then look for rec end;
									{
										if(*(pchRecords+ulRecordBufferLen)==10)
										{
											ulRecordsFound++;
											chRecordType = (char)0x00;
											ulCharCount=0;
										}
									}
									ulRecordBufferLen++;
								} break;
							case 0x00: // undetermined
							default: 
								{
									ulRecordBufferLen++;
									ulCharCount=0;
									if(*(pchRecords+ulRecordBufferLen)==0x80)
									{ //time record
										chRecordType = (char)0x80;
									}
									else if(*(pchRecords+ulRecordBufferLen)==0x81)
									{ //caller record
										chRecordType = (char)0x81;
									}
									else if(*(pchRecords+ulRecordBufferLen)==0x82)
									{ //destination record
										chRecordType = (char)0x82;
									}
									else 
									{ //log record
										chRecordType = (char)0xff;
									}

								} break;
							}
						}
					} // end while
					

					free(pchBuffer);
				}
*/   // NOT SUPPORTED YET - do later when time. (20112004)

			}
		}
	}
	else // just count line by line
	{
		if(m_pfile)
		{
			while(m_bFileInUse)
			{
				Sleep(1); //wait
			}
			m_bFileInUse=true;

			if(nMode&LOG_GETALL)
			{
							// determine file size
				fseek(m_pfile,0,SEEK_END);
				ulBufferLen = ftell(m_pfile);
				fseek(m_pfile,0,SEEK_SET);

				pch = (char*) malloc(ulBufferLen+1);
				if(pch)
				{
					fread(pch,sizeof(char),ulBufferLen,m_pfile);
				}
				fseek(m_pfile,0,SEEK_END);
				m_bFileInUse=false;

			}
			else if(nMode&LOG_GETFROMTOP)
			{
				// in order to reduce file access, we load the file into memory and parse from there.
							// determine file size
				fseek(m_pfile,0,SEEK_END);
				unsigned long ulBufferSize = ftell(m_pfile);
				fseek(m_pfile,0,SEEK_SET);

				char* pchBuffer = (char*) malloc(ulBufferSize+1);
				if(pchBuffer)
				{
					fread(pchBuffer,sizeof(char),ulBufferSize,m_pfile);
				}
				fseek(m_pfile,0,SEEK_END);
				m_bFileInUse=false;

				if(pchBuffer)
				{
					// parse records


					free(pchBuffer);
				}
			}
			else // get from bottom
			{
				// in order to reduce file access, we load the file into memory and parse from there.
							// determine file size
				fseek(m_pfile,0,SEEK_END);
				unsigned long ulBufferSize = ftell(m_pfile);
				fseek(m_pfile,0,SEEK_SET);

				char* pchBuffer = (char*) malloc(ulBufferSize+1);
				if(pchBuffer)
				{
					fread(pchBuffer,sizeof(char),ulBufferSize,m_pfile);
				}
				fseek(m_pfile,0,SEEK_END);
				m_bFileInUse=false;

				if(pchBuffer)
				{
					// parse records


					free(pchBuffer);
				}
			}
		}
	}

	*pulNumRecords = ulBufferLen;
	return pch;
}

// only takes an encrypted file
int  CLogUtil::HumanReadableFile(char* pszInputFileName, char* pszOutputFileName)
{
	// do this later - the idea was to not load the entire file into mem,
	// but to use a smaller buffer and do incremental file reads and file writes
	return 0;
}

// only takes an encrypted buffer
char* CLogUtil::HumanReadableStream(char* pchInputBuffer, unsigned long* pulBufferLength )
{
	if(pchInputBuffer == NULL) return NULL;

	_timeb timeAbsolute;
	char* pch = NULL;
	char* pchMarker = pchInputBuffer;
	char* pchBegin = NULL;
	char* pchEnd = pchInputBuffer+*pulBufferLength;
	char* pchTemp = NULL;
	char* pchFunctionTable = NULL;
	char* pchDestTable = NULL;
	bool bError = false;
	timeAbsolute.time = 0;
	timeAbsolute.millitm = 0;

	unsigned long ulBufferLength=0;

	while((pchMarker<pchEnd)&&(!bError))
	{
//		char foo[256]; sprintf(foo, "[0x%02x]", (*pchMarker&0xff)); AfxMessageBox(foo);
		switch(*pchMarker&0xff)
		{
		case 0x80:  // time record
			{
				if(pchMarker<pchEnd) pchMarker++;
				timeAbsolute.time  = (0xff000000&((*pchMarker&0xff)<<24));	if(pchMarker<pchEnd) pchMarker++;
				timeAbsolute.time |= (0x00ff0000&((*pchMarker&0xff)<<16));	if(pchMarker<pchEnd) pchMarker++;
				timeAbsolute.time |= (0x0000ff00&((*pchMarker&0xff)<<8));		if(pchMarker<pchEnd) pchMarker++;
				timeAbsolute.time |= (0x000000ff&(*pchMarker&0xff));				if(pchMarker<pchEnd) pchMarker++;

				timeAbsolute.millitm  = (0xff00&((*pchMarker&0xff)<<8));		if(pchMarker<pchEnd) pchMarker++;
				timeAbsolute.millitm |= (0x00ff&(*pchMarker&0xff));					if(pchMarker<pchEnd) pchMarker++;

				while(
					     ((unsigned char)(*pchMarker&0xff) != 0x0a)
						 &&(pchMarker<pchEnd)
						 )	pchMarker++; // end of record

			} break;
		case 0x81:   // calling function record
			{
				unsigned long ulLen=0;
				unsigned long ulBufLen=0;

				pchBegin = pchMarker;  // mark beginning of record.
				if(pchMarker<pchEnd) pchMarker++; // now at index.
				if(pchMarker<pchEnd) pchMarker++; // now at begining of function name.

				while(
					     ((unsigned char)(*pchMarker&0xff) != 0x0a)
						 &&(pchMarker<pchEnd)
						 )	pchMarker++; // end of record

				ulLen = pchMarker-pchBegin+1; // want the end of record as well.

				bool bExists = false;
				pchTemp = NULL;
				if(pchFunctionTable==NULL)  // then its the first one.
				{
					pchTemp = (char*) malloc(ulLen+1);
					ulBufLen = 0;
				}
				else
				{
					//we have to check if the function record overwrites another index record.
					unsigned char ucBuf[3];
					ucBuf[0] = 0x81;
					ucBuf[1] = *(pchBegin+1);
					ucBuf[2] = 0;
					char* pszFunction=NULL;
					unsigned long ulFunctionLen=0;

					//check table for this.
					if((pchFunctionTable)&&(strlen(pchFunctionTable)>0)&&(ucBuf[1]!=0))
					{
						char* pszTable = pchFunctionTable;
						do
						{
							pszFunction = strstr(pszTable, (char*)ucBuf );

							if(
								  (pszFunction!=NULL)
								&&(
										(pszFunction==pchFunctionTable)
									||(
											(pszFunction>pchFunctionTable)
											&&(((*(pszFunction-1))&0xff) == 0x0a)
										)
									)
								)
							{ //its there
								bExists = true;
								pszFunction+=2;  // for now, have to skip index byte.

								while(((*(pszFunction+ulFunctionLen)&0xff)!=0x0a)&&(pszFunction+ulFunctionLen<pchFunctionTable+strlen(pchFunctionTable))) ulFunctionLen++;
							}
							else
							{
								pszTable = pszFunction+1;
							}
						}	while((pszFunction!=NULL)&&(!bExists));
					}
					if(bExists)
					{ // have to replace.  (unless its the same)
						if((ulLen!=ulFunctionLen+3)||(memcmp(pchBegin+2, pszFunction, ulFunctionLen)!=0))
						{
							ulBufLen = strlen(pchFunctionTable);
							pchTemp = (char*) malloc(ulBufLen-ulFunctionLen+3+ulLen+1);
							if(pchTemp)
							{
								memcpy(pchTemp, pchFunctionTable, (pszFunction-2) - pchFunctionTable);
								memcpy(pchTemp+((pszFunction-2)-pchFunctionTable), pchBegin, ulLen);
								memcpy(pchTemp+((pszFunction-2)-pchFunctionTable)+ulLen, pszFunction+ulFunctionLen+2, ulBufLen-(ulFunctionLen+3)-((pszFunction-2) - pchFunctionTable));
								*(pchTemp+ulBufLen-ulFunctionLen+3+ulLen) = 0;
								free(pchFunctionTable);
								pchFunctionTable = pchTemp;
							}
						}
					}
					else
					{
						//have to add it.
						ulBufLen = strlen(pchFunctionTable);
						pchTemp = (char*) malloc(ulLen+ulBufLen+1);
						if(pchTemp)
						{
							memcpy(pchTemp, pchFunctionTable, ulBufLen);
							free(pchFunctionTable);
						}
					}
				}

				if((pchTemp)&&(!bExists)) // then add it.
				{
					memcpy(pchTemp+ulBufLen, pchBegin, ulLen);
					*(pchTemp+ulBufLen+ulLen) = 0;
					pchFunctionTable = pchTemp;
				}
				
// AfxMessageBox(pchFunctionTable);

			} break;
		case 0x82:  // destination record
			{
				unsigned long ulLen=0;
				unsigned long ulBufLen=0;

				pchBegin = pchMarker;  // mark beginning of record.
				if(pchMarker<pchEnd) pchMarker++; // now at index.
				if(pchMarker<pchEnd) pchMarker++; // now at begining of function name.
				while(
					     ((unsigned char)(*pchMarker&0xff) != 0x0a)
						 &&(pchMarker<pchEnd)
						 )	pchMarker++; // end of record

				ulLen = pchMarker-pchBegin+1; // want the end of record as well.

				bool bExists = false;
				pchTemp = NULL;
				if(pchDestTable==NULL)  // then its the first one.
				{
					pchTemp = (char*) malloc(ulLen+1);
					ulBufLen = 0;
				}
				else
				{
					//we have to check if the function record overwrites another index record.
					unsigned char ucBuf[3];
					ucBuf[0] = 0x82;
					ucBuf[1] = *(pchBegin+1);
					ucBuf[2] = 0;
					char* pszDest=NULL;
					unsigned long ulDestLen=0;

					//check table for this.
					if((pchDestTable)&&(strlen(pchDestTable)>0)&&(ucBuf[1]!=0))
					{
						char* pszTable = pchDestTable;
						do
						{
							pszDest = strstr(pszTable, (char*)ucBuf );

							if(
								  (pszDest!=NULL)
								&&(
										(pszDest==pchDestTable)
									||(
											(pszDest>pchDestTable)
											&&(((*(pszDest-1))&0xff) == 0x0a)
										)
									)
								)
							{ //its there
								bExists = true;
								pszDest+=2;  // for now, have to skip index byte.

								while(((*(pszDest+ulDestLen)&0xff)!=0x0a)&&(pszDest+ulDestLen<pchDestTable+strlen(pchDestTable))) ulDestLen++;
							}
							else
							{
								pszTable = pszDest+1;
							}
						}	while((pszDest!=NULL)&&(!bExists));
					}
					if(bExists)
					{ // have to replace.  (unless its the same)
						if((ulLen!=ulDestLen+3)||(memcmp(pchBegin+2, pszDest, ulDestLen)!=0))
						{
							ulBufLen = strlen(pchDestTable);
							pchTemp = (char*) malloc(ulBufLen-ulDestLen+3+ulLen+1);
							if(pchTemp)
							{
								memcpy(pchTemp, pchDestTable, (pszDest-2) - pchDestTable);
								memcpy(pchTemp+((pszDest-2)-pchDestTable), pchBegin, ulLen);
								memcpy(pchTemp+((pszDest-2)-pchDestTable)+ulLen, pszDest+ulDestLen+2, ulBufLen-(ulDestLen+3)-((pszDest-2) - pchDestTable));
								*(pchTemp+ulBufLen-ulDestLen+3+ulLen) = 0;
								free(pchDestTable);
								pchDestTable = pchTemp;
							}
						}
					}
					else
					{
						//have to add it.
						ulBufLen = strlen(pchDestTable);
						pchTemp = (char*) malloc(ulLen+ulBufLen+1);
						if(pchTemp)
						{
							memcpy(pchTemp, pchDestTable, ulBufLen);
							free(pchDestTable);
						}
					}
				}

				if((pchTemp)&&(!bExists)) // then add it.
				{
					memcpy(pchTemp+ulBufLen, pchBegin, ulLen);
					*(pchTemp+ulBufLen+ulLen) = 0;
					pchDestTable = pchTemp;
				}

//		AfxMessageBox(pchDestTable);

			} break;
		default:  // message record.
			{
				unsigned long ulLen=0;
				unsigned long ulOffset=0;
				pchBegin = pchMarker;  // mark beginning of record.
				if(pchMarker<pchEnd-6) pchMarker+=6; // now at message.

//AfxMessageBox(pchMarker);

				unsigned char ucCount = 0;
				unsigned long ulNum = 0;
				if((unsigned char)(*pchMarker&0xff)==0x13)  // this is a repeat record.
				{
					if(pchMarker<pchEnd) pchMarker++;
					ucCount = (*pchMarker&0xff); if(pchMarker<pchEnd) pchMarker++;
					ulNum = 0;
					if(ucCount>0){ ulNum	 = (0x000000ff&(*pchMarker&0xff));	 if(pchMarker<pchEnd) pchMarker++;}
					if(ucCount>1){ ulNum	|= (0x0000ff00&(*pchMarker&0xff));	 if(pchMarker<pchEnd) pchMarker++;}
					if(ucCount>2){ ulNum	|= (0x00ff0000&(*pchMarker&0xff));	 if(pchMarker<pchEnd) pchMarker++;}
					if(ucCount>3){ ulNum	|= (0xff000000&(*pchMarker&0xff));	 if(pchMarker<pchEnd) pchMarker++;}  // end of record
				}
				else
				{
					while(
								 ((unsigned char)(*pchMarker&0xff) != 0x0a)
							 &&(pchMarker<pchEnd)
							 )	pchMarker++; // end of record

					ulLen = pchMarker-pchBegin; // do not want the end of record included.
				}

				unsigned char ucCaller=0, ucIconSev=0, ucDest=0;
				if(pchBegin<pchEnd-6) 
				{
					// first is the time offset
					ulOffset  = (0x00ff0000&((*pchBegin&0xff)<<16));	pchBegin++;
					ulOffset |= (0x0000ff00&((*pchBegin&0xff)<<8));		pchBegin++;
					ulOffset |= (0x000000ff&(*pchBegin&0xff));				pchBegin++;

					ucCaller  = *pchBegin;  pchBegin++;
					ucIconSev = *pchBegin;  pchBegin++;
					ucDest    = *pchBegin;  pchBegin++;

					char* pchMessage = NULL;

					if((unsigned char)(*pchBegin&0xff)==0x13)  // this is a repeat record.
					{
					
						char message[36]; //"... repeated 4294967295 times ..."				

						sprintf(message, "... repeated %ld times ...", ulNum);

						ucCaller  = 0;
						ucIconSev = 0;
						ucDest    = 0;

						pchMessage = message;
						ulLen = strlen(message);
					}
					else
					{
						ulLen -= 6;
						pchMessage = DecodeTen(pchBegin, &ulLen, TEN_DECODE_CRLF);  // text mode, make it crlf
						if(pchMessage)
						{
							// need to calculate the length of the buffer we need
							// if there are CRLFs, we want to put each line on its own line, but also
							// put leading spaces to justify the text.

							char* pchLine = strstr(pchMessage, "\r\n");
							while(pchLine!=NULL)
							{
								ulLen+=30; // the length of the indent.
								pchLine = strstr(pchLine+2, "\r\n");
							}
						}
					}
					// find the other data.
					char* pszFunction=NULL;
					char* pszDest=NULL;
					unsigned long ulFunctionLen=0;
					unsigned long ulDestLen=0;

					bool bExists = false;
					unsigned char ucBuf[36];
					ucBuf[0] = 0x81;
					ucBuf[1] = ucCaller;
					ucBuf[2] = 0;

					//check table for this.
					if((pchFunctionTable)&&(strlen(pchFunctionTable)>0)&&(ucCaller))
					{
						char* pszTable = pchFunctionTable;
						do
						{
							pszFunction = strstr(pszTable, (char*)ucBuf );

							if(
								  (pszFunction!=NULL)
								&&(
										(pszFunction==pchFunctionTable)
									||(
											(pszFunction>pchFunctionTable)
											&&(((*(pszFunction-1))&0xff) == 0x0a)
										)
									)
								)
							{ //its there
								bExists = true;
								pszFunction+=2;

								while(((*(pszFunction+ulFunctionLen)&0xff)!=0x0a)&&(pszFunction+ulFunctionLen<pchFunctionTable+strlen(pchFunctionTable))) ulFunctionLen++;
							}
							else
							{
								pszTable = pszFunction+1;
							}
						}	while((pszFunction!=NULL)&&(!bExists));
					}

					bExists = false;
					ucBuf[0] = 0x82;
					ucBuf[1] = ucDest;
					ucBuf[2] = 0;

					//check table for this.
					if((pchDestTable)&&(strlen(pchDestTable)>0)&&(ucDest))
					{
						char* pszTable = pchDestTable;
						do
						{
							pszDest = strstr(pszTable, (char*)ucBuf );

							if(
								  (pszDest!=NULL)
								&&(
										(pszDest==pchDestTable)
									||(
											(pszDest>pchDestTable)
											&&(((*(pszDest-1))&0xff) == 0x0a)
										)
									)
								)
							{ //its there
								bExists = true;
								pszDest+=2;

								while(((*(pszDest+ulDestLen)&0xff)!=0x0a)&&(pszDest+ulDestLen<pchDestTable+strlen(pchDestTable))) ulDestLen++;
							}
							else
							{
								pszTable = pszDest+1;
							}
						}	while((pszDest!=NULL)&&(!bExists));
					}


					// ok now we have enough to write the record to a buffer.

//2004-Dec-08 10:47:00.263 [! ] message text - Function - Dest1
					//30 + 3 + 3 + 2; datetime, delims, crlf
					pchTemp = NULL;
					if((pszFunction)||(pszDest)||(ulFunctionLen)||(ulDestLen))
					{
						pchTemp = (char*)malloc(ulBufferLength + 38 + ulLen + ulFunctionLen + ulDestLen + 1); // lets zero term it.
					}
					else
					{  // no extra delimiters needed
						pchTemp = (char*)malloc(ulBufferLength + 32 + ulLen + ulFunctionLen + ulDestLen + 1); // lets zero term it.
					}

					if(pchTemp)
					{
						if(pch) // first check that there is a buffer!
						{
							if(ulBufferLength>0) memcpy(pchTemp, pch, ulBufferLength);
							free(pch);
						}

						pch = pchTemp;

						_timeb timestamp;

						timestamp.time    = timeAbsolute.time + ulOffset/1000;
						timestamp.millitm = timeAbsolute.millitm + (unsigned short)(ulOffset%1000);

						if(timestamp.millitm>999)
						{
							timestamp.time++;
							timestamp.millitm -= 1000;
						}

						tm* theTime = localtime( &timestamp.time	);

						strftime( (char*)&ucBuf, 30, "%Y-%b-%d %H:%M:%S.", theTime);

						unsigned long ulBufOffset = ulBufferLength;
						memcpy(pch+ulBufOffset, ucBuf, 21); ulBufOffset+=21;

						char ch = ' ';
						if((ucIconSev&0xf0)!=MSG_PRI_NORMAL)
							ch = ((ucIconSev&0xf0)>>4)+48;

						switch(ucIconSev&0x0f)
						{
						case MSG_ICONQUESTION://						0x00000001  // ? icon
							{
								sprintf((char*)&ucBuf, "%03d [?%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONEXCLAMATION://					0x00000002  // ! icon
							{
								sprintf((char*)&ucBuf, "%03d [!%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONERROR://								0x00000003  // X icon
							{
								sprintf((char*)&ucBuf, "%03d [X%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONSTOP://								0x00000004  // stop sign icon
							{
								sprintf((char*)&ucBuf, "%03d [S%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONHAND://								0x00000005  // hand icon
							{
								sprintf((char*)&ucBuf, "%03d [H%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONINFO://								0x00000006  // (i) icon 
							{
								sprintf((char*)&ucBuf, "%03d [i%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONUSER1://								0x00000007  
							{
								sprintf((char*)&ucBuf, "%03d [1%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONUSER2://								0x00000008  
							{
								sprintf((char*)&ucBuf, "%03d [2%c] ", timestamp.millitm, ch);
							} break;
						case MSG_ICONNONE://								0x00000000  // default, nothing
						default:
							{
								if(ch==' ')
									sprintf((char*)&ucBuf, "%03d       ", timestamp.millitm);
								else
									sprintf((char*)&ucBuf, "%03d [ %c] ", timestamp.millitm, ch);
							} break;
						}

						memcpy(pch+ulBufOffset, ucBuf, 9); ulBufOffset+=9;

//////////
						if(pchMessage)
						{
							char* pchSubBegin	= pchMessage;
							char* pchSubEnd		= strstr(pchMessage, "\r\n");

							if(pchSubEnd==NULL)
							{
								// its a one liner.
								memcpy(pch+ulBufOffset, pchMessage, ulLen);
								ulBufOffset+=ulLen;

								if((pszFunction)||(pszDest)||(ulFunctionLen)||(ulDestLen))
								{
									sprintf((char*)&ucBuf, " - ");
									memcpy(pch+ulBufOffset, ucBuf, 3);
									ulBufOffset+=3;

									if((pszFunction)&&(ulFunctionLen))
									{
										memcpy(pch+ulBufOffset, pszFunction, ulFunctionLen);
										ulBufOffset+=ulFunctionLen;
									}

									memcpy(pch+ulBufOffset, ucBuf, 3);
									ulBufOffset+=3;

									if((pszDest)&&(ulDestLen))
									{
										memcpy(pch+ulBufOffset, pszDest, ulDestLen);
										ulBufOffset+=ulDestLen;
									}
								}

								sprintf((char*)&ucBuf, "%c%c%c", 13, 10, 0);
								memcpy(pch+ulBufOffset, ucBuf, 3);

								ulBufferLength = ulBufOffset+2;
							}
							else
							{
								memcpy(pch+ulBufOffset, pchSubBegin, pchSubEnd-pchSubBegin); 
								ulBufOffset+=(pchSubEnd-pchSubBegin);
								if((pszFunction)||(pszDest)||(ulFunctionLen)||(ulDestLen))
								{
									sprintf((char*)&ucBuf, " - ");
									memcpy(pch+ulBufOffset, ucBuf, 3);
									ulBufOffset+=3;

									if((pszFunction)&&(ulFunctionLen))
									{
										memcpy(pch+ulBufOffset, pszFunction, ulFunctionLen);
										ulBufOffset+=ulFunctionLen;
									}

									memcpy(pch+ulBufOffset, ucBuf, 3);
									ulBufOffset+=3;

									if((pszDest)&&(ulDestLen))
									{
										memcpy(pch+ulBufOffset, pszDest, ulDestLen);
										ulBufOffset+=ulDestLen;
									}
								}
								*(pch+ulBufOffset) = 13; ulBufOffset++;
								*(pch+ulBufOffset) = 10; ulBufOffset++;
								while(pchSubEnd!=NULL)
								{

									memset(pch+ulBufOffset, ' ', 30); ulBufOffset+=30;
									pchSubBegin = pchSubEnd+2; // skip the \r\n
									pchSubEnd = strstr(pchSubBegin, "\r\n");
									if(pchSubEnd==NULL)
									{
//							AfxMessageBox("null");
									// last line
										memcpy(pch+ulBufOffset, pchSubBegin, strlen(pchSubBegin)); ulBufOffset+=strlen(pchSubBegin);
									}
									else
									{
//							AfxMessageBox("not null");
									// intermediate
										memcpy(pch+ulBufOffset, pchSubBegin, (pchSubEnd-pchSubBegin)); ulBufOffset+=(pchSubEnd-pchSubBegin);
									}

									*(pch+ulBufOffset) = 13; ulBufOffset++;
									*(pch+ulBufOffset) = 10; ulBufOffset++;
								}
								*(pch+ulBufOffset) = 0;
//						AfxMessageBox("x");
								ulBufferLength = ulBufOffset;
							}
							free(pchMessage);  // free the decoded buffer.
						}
////////////
/*
						if(pchMessage)
						{
							memcpy(pch+ulBufOffset, pchMessage, ulLen);
							ulBufOffset+=ulLen;
						}

						if((pszFunction)||(pszDest)||(ulFunctionLen)||(ulDestLen))
						{
							sprintf((char*)&ucBuf, " - ");
							memcpy(pch+ulBufOffset, ucBuf, 3);
							ulBufOffset+=3;

							if((pszFunction)&&(ulFunctionLen))
							{
								memcpy(pch+ulBufOffset, pszFunction, ulFunctionLen);
								ulBufOffset+=ulFunctionLen;
							}

							memcpy(pch+ulBufOffset, ucBuf, 3);
							ulBufOffset+=3;

							if((pszDest)&&(ulDestLen))
							{
								memcpy(pch+ulBufOffset, pszDest, ulDestLen);
								ulBufOffset+=ulDestLen;
							}
						}

						sprintf((char*)&ucBuf, "%c%c%c", 13, 10, 0);
						memcpy(pch+ulBufOffset, ucBuf, 3);

						ulBufferLength =ulBufOffset+2;
*/
//						AfxMessageBox(pch);
					}
					else
					{
						// have to use a temp file instead of a memory buffer.
						// TODO.
					}
				}
			}
		}

		pchMarker++;
	}
	
	return pch;
}

