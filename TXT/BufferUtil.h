// BufferUtil.h: interface for the CBufferUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BUFFERUTIL_H__8B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_)
#define AFX_BUFFERUTIL_H__8B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// return codes
#define RETURN_SUCCESS 0
#define SOFTERROR_B64_ALPHA 1 // soft because uses default alphabet
#define SOFTERROR_B64_PADCH 2 // soft because uses default pad char
#define SOFTERROR_B64_PAD_IN_ALPHA   4 // pad character exists in alpha string, soft because uses default pad char and alpha
#define ERROR_B64_NULLPTR   -1 // null pointer to output string 
#define ERROR_B64_INVALID_FILENAME -2
#define ERROR_B64_FILEERROR -3
#define ERROR_B64_NULLMALLOC -4

#define MODE_DEFAULT					0x00
#define MODE_FULLHEX					0x00
#define MODE_ALLOWPRINTABLES	0x01
#define MODE_ALLOWLINEFEED		0x02
#define MODE_MAKECRLF					0x04
#define MODE_DELIMPRINTABLES	0x08
#define MODE_DELIMMASK				0x30
#define MODE_DELIM32BIT				0x10
#define MODE_DELIM64BIT				0x20
#define MODE_DELIM128BIT			0x30

#define MODE_MULTIDELIM				0  // for CSafeBufferUtil::Token.  consecutive delimiters treated as one, the default behavior, same as strtok
#define MODE_SINGLEDELIM			1  // for CSafeBufferUtil::Token.  consecutive delimiters treated separately, a pointer to a strlen 0 string is returned. does not skip leading delims

class CSafeBufferUtil  
{
public:
	CSafeBufferUtil();
	virtual ~CSafeBufferUtil();

	// have to do the following because strtok not threadsafe
	char*	Token(char* pchBuffer, unsigned long ulBufferLength, char* pszTokens, unsigned long ulMode=MODE_MULTIDELIM);

private:
	// have to do the following because strtok not threadsafe
	char*	m_pszLastToken;			// the pointer to the last token
	char*	m_pszTokenBuffer;		// pointer to the entire buffer
	unsigned long m_ulTokenBufferLength;
	unsigned long m_ulLastTokenLength;

};

class CBufferUtil  
{
public:
	CBufferUtil();
	virtual ~CBufferUtil();

  int		Base64Encode(char** ppchBuffer, unsigned long* pulBufLen, bool bFreeBuffer=false, char* pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=');
  int		Base64Decode(char** ppchBuffer, unsigned long* pulBufLen, bool bFreeBuffer=false, char* pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=');
  int		Base64EncodeFromFile(char** ppchBuffer, unsigned long* pulBufLen, bool bFreeBuffer=false, char* pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=');
  int		Base64DecodeToFile(char* pszFilename, char* pchBuffer, unsigned long* pulBufLen, bool bFreeBuffer=false, char* pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=');
  char*	Base64Boundary(char* pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=');
	char* ReturnBase64Number(__int64 i64Value, char* pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"); 
	bool	IsFileString(char* pszText, bool bAllowDirPaths=true, bool bAllowDrive=true); 
	bool	IsHexString(char* pszText);
	bool	IsDTMFString(char* pszText);
	unsigned char IsDecimalString(char* pszText);
	bool	BreakFileString(char* pszText, char** ppszPath, char** ppszFile, char** ppszExt);
	char*	MakeFileString(char* pszText, char chReplaceChar=0, bool bAllowDirPaths=true, bool bAllowDrive=true, bool bUseSameBuffer=true);
	unsigned long CountChar(char* pchBuffer, unsigned long ulBufLen, char chFind);
	char*	StripChar(char* pchBuffer, unsigned long* pulBufLen, char chStripChar);
	void	ReplaceChar(char* pchBuffer, unsigned long ulBufLen, char chRemoveChar, char chReplaceChar);
	char*	ReadableHex(char* pchBuffer, unsigned long* pulBufLen, unsigned long ulMode=MODE_ALLOWPRINTABLES);
	char*	DecodeReadableHex(char* pchBuffer, unsigned long* pulBufLen, unsigned long ulMode=MODE_FULLHEX);
	char* XMLEncode(char* pszText);
	unsigned char* memrncmp(unsigned char* pucBuffer, unsigned long ulBufLen, unsigned char* pucCmpBuffer, unsigned long ulCmpBufLen);
	unsigned long xtol(char* pucBuffer, unsigned long ulBufLen);
	int   ParseCSV(char* data, char**** ppppchBuffer, unsigned long* pulRows, unsigned long* pulColumns );
	char* EscapeChar(char* pszText, char ch);
	char* UnEscapeChar(char* pszText, char ch);
	char* FormatXML(char* pszText, char* pszIndent="  ");
	char* EscapeCharsWithChar(char* pszText, char* pszTgt, char chEsc);
};

#endif // !defined(AFX_BUFFERUTIL_H__8B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_)
