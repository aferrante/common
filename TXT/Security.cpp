// Security.cpp: implementation of the CSecurity and support classes.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdlib.h>
#include "BufferUtil.h"
#include "Security.h"
#include <time.h>


/*  .. if no stdafx, no DEBUG_NEW - have to exclude this
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/

//////////////////////////////////////////////////////////////////////
// CSecureAsset Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSecureAsset::CSecureAsset(char* pszName, unsigned long ulType, char* pszParentName, char* pszData)
{
	m_pszName = (char*) malloc(strlen(pszName)+1);
	if(m_pszName) strcpy(m_pszName, pszName);
	m_ulType = ulType;
	if(pszParentName)
	{
		m_pszParentName = (char*) malloc(strlen(pszParentName)+1);
		if(m_pszParentName) strcpy(m_pszParentName, pszParentName);
	}
	if(pszData)
	{
		m_pszData = (char*) malloc(strlen(pszData)+1);
		if(m_pszData) strcpy(m_pszData, pszData);
	}
}

CSecureAsset::~CSecureAsset()
{
	if(m_pszName) free(m_pszName);
	if(m_pszParentName) free(m_pszParentName);
	if(m_pszData) free(m_pszData);
}


//////////////////////////////////////////////////////////////////////
// CSecureAssetList Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSecureAssetList::CSecureAssetList()
{
	m_ppAssets = NULL;
	m_ulNumAssets = 0;
}

CSecureAssetList::~CSecureAssetList()
{
	if(m_ppAssets)
	{
		for(unsigned long i=0; i<m_ulNumAssets; i++)
		if(m_ppAssets[i]) delete m_ppAssets[i];
		delete [] m_ppAssets;
	}
}

int CSecureAssetList::AddAsset(CSecureAsset* pAsset)
{
	if(pAsset==NULL) return SECURE_ERROR;
	if(GetAssetIndex(pAsset->m_pszName)>=0) // theres already an asset of this name
		return SECURE_ALREADYEXISTS;
	CSecureAsset** ppAssets;		// a pointer to an array of pointers to existing member assets
	ppAssets = new CSecureAsset*[m_ulNumAssets+1];
	if(ppAssets==NULL) return SECURE_ERROR;

	if(m_ppAssets)
	{
		memcpy(ppAssets, m_ppAssets, sizeof(CSecureAsset*)*(m_ulNumAssets));
		delete [] m_ppAssets;
	}

	m_ppAssets = ppAssets;
	m_ppAssets[m_ulNumAssets] = pAsset;
	m_ulNumAssets++;

	return SECURE_SUCCESS;
}

int CSecureAssetList::GetAssetIndex(char* pszAssetName)
{
	unsigned int ulIndex=0;
	if((m_ppAssets)&&(pszAssetName)&&(strlen(pszAssetName)>0))
	{
		while(ulIndex<m_ulNumAssets)
		{
			if((m_ppAssets[ulIndex])&&(m_ppAssets[ulIndex]->m_pszName)&&(strlen(m_ppAssets[ulIndex]->m_pszName)>0))
			{
				if(strcmp(m_ppAssets[ulIndex]->m_pszName, pszAssetName)==0) return ulIndex;
			}
			ulIndex++;
		}
	}
	return -1;
}

int CSecureAssetList::RemoveAsset(char* pszAssetName)   // removes asset and deletes obj
{
	if((m_ppAssets)&&(pszAssetName)&&(strlen(pszAssetName)>0)&&(m_ulNumAssets>0))
	{
		int nIndex = GetAssetIndex(pszAssetName);
		if(nIndex>=0)
		{
			//delete the object
			delete m_ppAssets[nIndex];  // the pointer is valid, checked in GetAssetIndex

			// truncate the list.
			m_ulNumAssets--;
			while(nIndex<(int)(m_ulNumAssets))
			{
				m_ppAssets[nIndex] = m_ppAssets[nIndex+1];
				nIndex++;
			}

			// allocate a smaller list.
			CSecureAsset** ppAssets;		// a pointer to an array of pointers to existing member assets
			ppAssets = new CSecureAsset*[m_ulNumAssets];

			if(ppAssets)
			{
				memcpy(ppAssets, m_ppAssets, sizeof(CSecureAsset*)*(m_ulNumAssets));
				delete [] m_ppAssets;
				m_ppAssets = ppAssets;
			}
			// else its ok, just let the original buffer exist with an extra entry; m_ulNumAssets is decremented so it doesnt mattter.
		}
	} else return SECURE_ERROR;
	return SECURE_SUCCESS;
}

int CSecureAssetList::CheckSubAsset(char* pszSubAssetName, char* pszAssetName)  // recursively checks an asset to see if it is a subasset of an asset
{
	if((pszSubAssetName)&&(pszAssetName)&&(strlen(pszSubAssetName)>0)&&(strlen(pszAssetName)>0))
	{
		int nIndex = GetAssetIndex(pszSubAssetName);
		if(nIndex>=0)
		{
			if((m_ppAssets[nIndex]->m_pszParentName)&&(strlen(m_ppAssets[nIndex]->m_pszParentName)>0))
			{
				if(strcmp(pszAssetName, m_ppAssets[nIndex]->m_pszParentName))
				{
					// different
					return CheckSubAsset(m_ppAssets[nIndex]->m_pszParentName, pszAssetName);  
					// this should recursive check until a match or until there are no more parents.
				}
				else
				{
					//same!  
					return SECURE_SUCCESS;
				}
			}
		}
	}
	return SECURE_ERROR;
}


//////////////////////////////////////////////////////////////////////
// CSecureLocus Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSecureLocus::CSecureLocus(char* pszName, unsigned long ulType)
{
	m_pszName = (char*) malloc(strlen(pszName)+1);
	if(m_pszName) strcpy(m_pszName, pszName);
	m_ulType = ulType;
	m_ppAssets = NULL;		// a pointer to an array of pointers to existing member assets
	m_ulNumAssets = 0;
}

CSecureLocus::~CSecureLocus()
{
	if(m_pszName) free(m_pszName);
	if(m_ppAssets) delete [] m_ppAssets;  // delete the array of pointers, but not the objects they point to
}

int CSecureLocus::ChangeName(char* pszName)
{
	if((pszName==NULL)||(strlen(pszName)<=0)) return SECURE_ERROR;
	if(m_pszName) free(m_pszName);
	m_pszName = (char*) malloc(strlen(pszName)+1);
	if(m_pszName) strcpy(m_pszName, pszName);
	else return SECURE_ERROR;
	return SECURE_SUCCESS;
}

int CSecureLocus::AddAsset(CSecureAsset* pAsset)
{
	if(pAsset==NULL) return SECURE_ERROR;
	if(GetAssetIndex(pAsset->m_pszName)>=0) // theres already an asset of this name
		return SECURE_ALREADYEXISTS;
	CSecureAsset** ppAssets;		// a pointer to an array of pointers to existing member assets
	ppAssets = new CSecureAsset*[m_ulNumAssets+1];
	if(ppAssets==NULL) return SECURE_ERROR;

	if(m_ppAssets)
	{
		memcpy(ppAssets, m_ppAssets, sizeof(CSecureAsset*)*(m_ulNumAssets));
		delete [] m_ppAssets;
	}

	m_ppAssets = ppAssets;
	m_ppAssets[m_ulNumAssets] = pAsset;
	m_ulNumAssets++;

	return SECURE_SUCCESS;
}

int CSecureLocus::GetAssetIndex(char* pszAssetName)
{
	unsigned int ulIndex=0;
	if((m_ppAssets)&&(pszAssetName)&&(strlen(pszAssetName)>0))
	{
		while(ulIndex<m_ulNumAssets)
		{
			if((m_ppAssets[ulIndex])&&(m_ppAssets[ulIndex]->m_pszName)&&(strlen(m_ppAssets[ulIndex]->m_pszName)>0))
			{
				if(strcmp(m_ppAssets[ulIndex]->m_pszName, pszAssetName)==0) return ulIndex;
			}
			ulIndex++;
		}
	}
	return -1;
}

int CSecureLocus::RemoveAsset(char* pszAssetName)  // removes asset and deletes obj
{
	if((m_ppAssets)&&(pszAssetName)&&(strlen(pszAssetName)>0)&&(m_ulNumAssets>0))
	{
		int nIndex = GetAssetIndex(pszAssetName);
		if(nIndex>=0)
		{
			//delete the object
			delete m_ppAssets[nIndex];  // the pointer is valid, checked in GetAssetIndex

			// truncate the list.
			m_ulNumAssets--;
			while(nIndex<(int)(m_ulNumAssets))
			{
				m_ppAssets[nIndex] = m_ppAssets[nIndex+1];
				nIndex++;
			}

			// allocate a smaller list.
			CSecureAsset** ppAssets;		// a pointer to an array of pointers to existing member assets
			ppAssets = new CSecureAsset*[m_ulNumAssets];

			if(ppAssets)
			{
				memcpy(ppAssets, m_ppAssets, sizeof(CSecureAsset*)*(m_ulNumAssets));
				delete [] m_ppAssets;
				m_ppAssets = ppAssets;
			}
			// else its ok, just let the original buffer exist with an extra entry; m_ulNumAssets is decremented so it doesnt mattter.
		}
	} else return SECURE_ERROR;
	return SECURE_SUCCESS;
}


int CSecureLocus::AddAttributes(unsigned long ulFlags)
{
	m_ulType |= ulFlags;
	return m_ulType;
}

int CSecureLocus::RemoveAttributes(unsigned long ulFlags)
{
	m_ulType &= ~ulFlags;
	return m_ulType;
}



//////////////////////////////////////////////////////////////////////
// CSecureUser Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSecureUser::CSecureUser(char* pszUser, char* pszPassword)
{
	m_pszUser = (char*) malloc(strlen(pszUser)+1);
	if(m_pszUser) strcpy(m_pszUser, pszUser);
	m_pszPassword = (char*) malloc(strlen(pszPassword)+1);
	if(m_pszPassword) strcpy(m_pszPassword, pszPassword);
	m_pszGroups = NULL;
	m_ulFlags = 0;  // settings such as ability to change pw etc
	m_ppLoci = NULL;					// a pointer to an array of pointers to existing loci
	m_ulNumLoci = 0;
	m_ulMaxAttempts=0;	// max number of login attempts allowed for user. 0 = infinity
	m_ulNumAttempts=0;	// number of unsuccessful login attempts since last success
	m_ulPwExpiry=0;			// time when password will expire (unixtime) - set to zero if unused
}

CSecureUser::~CSecureUser()
{
	if(m_pszUser) free(m_pszUser);
	if(m_pszPassword) free(m_pszPassword);
	if(m_pszGroups) free(m_pszGroups);
	if(m_ppLoci) delete [] m_ppLoci;  // delete the array of pointers, but not the objects they point to
}

int CSecureUser::ChangePassword(char* pszPw)
{
	if((pszPw==NULL)||(strlen(pszPw)<=0)) return SECURE_ERROR;
	if(m_pszPassword) free(m_pszPassword);
	m_pszPassword = (char*) malloc(strlen(pszPw)+1);
	if(m_pszPassword) strcpy(m_pszPassword, pszPw);
	else return SECURE_ERROR;
	return SECURE_SUCCESS;
}

int CSecureUser::AddGroup(char* pszGroup)
{
	if((pszGroup==NULL)||(strlen(pszGroup)<=0)) return SECURE_ERROR;
	// have to check for group already exists
	int nReturn = SECURE_ERROR;

	char* pszGr = (char*)malloc(strlen(pszGroup)+2);
	if(pszGr==NULL) return SECURE_ERROR;
	sprintf(pszGr, "%s%c", pszGroup, SECURE_USER_GROUPDELIM); //char 31(US) delim.
	unsigned long ulBufferLen = 1;  //term 0
	if(m_pszGroups)
	{
		char* pch = strstr(m_pszGroups, pszGr);
		if(pch!=NULL)
		{
			if((pch==m_pszGroups)||((pch>m_pszGroups)&&(*(pch-1)==SECURE_USER_GROUPDELIM)))
			{
				// exists! 
				free(pszGr);
				return SECURE_SUCCESS;
			}
		}
		ulBufferLen = strlen(m_pszGroups);
	}
	ulBufferLen += strlen(pszGr);

	char* pchBuffer = (char*)malloc(ulBufferLen);
	if(pchBuffer)
	{
		ulBufferLen = 0;
		if(m_pszGroups)
		{
			ulBufferLen = strlen(m_pszGroups);
			memcpy(pchBuffer, m_pszGroups, ulBufferLen);
			free(m_pszGroups);
		}
		m_pszGroups = pchBuffer;
		memcpy(m_pszGroups+ulBufferLen, pszGr, strlen(pszGr)+1);  // include term 0

		nReturn = SECURE_SUCCESS;
	}
	free(pszGr);
	return nReturn;
}

int CSecureUser::RemoveGroup(char* pszGroup)  // only removes it from the permission set, doesn't delete the user group obj
{
	if((pszGroup==NULL)||(strlen(pszGroup)<=0)) return SECURE_ERROR;
	// have to check for group already exists
	int nReturn = SECURE_ERROR;

	char* pszGr = (char*)malloc(strlen(pszGroup)+2);
	if(pszGr==NULL) return SECURE_ERROR;
	sprintf(pszGr, "%s%c", pszGroup, SECURE_USER_GROUPDELIM); //char 31(US) delim.
	unsigned long ulBufferPtr = 0;
	if(m_pszGroups)
	{
		char* pch = strstr(m_pszGroups, pszGr);
		if(pch!=NULL)
		{
			if((pch==m_pszGroups)||((pch>m_pszGroups)&&(*(pch-1)==SECURE_USER_GROUPDELIM)))
			{
				// exists! let's remove.
				if(strlen(m_pszGroups)==strlen(pszGr)) // only one thing, so just remove it.
				{
					free(m_pszGroups); m_pszGroups=NULL;
				}
				else // have to excise just the one record
				{
					char* pchBuffer = (char*)malloc(strlen(m_pszGroups)-strlen(pszGr)+1);
					if(pchBuffer)
					{
						if(pch!=m_pszGroups) // not the first one
						{
							ulBufferPtr = pch-m_pszGroups;
							memcpy(pchBuffer, m_pszGroups, ulBufferPtr);
							memset(pchBuffer+(pch-m_pszGroups)+1, 0, 1);  // create a term zero (may get overwritten by next step, but not necessarily)
						}
						if(pch+strlen(pszGr) < m_pszGroups+strlen(m_pszGroups)) // wasnt the last one
						{
							memcpy(pchBuffer+ulBufferPtr, pch+strlen(pszGr), strlen(m_pszGroups)-(pch+strlen(pszGr)-m_pszGroups)+1);  // copy term zero
						}

						free(m_pszGroups);
						m_pszGroups = pchBuffer;
					} 
					else
					{
						free(pszGr);
						return SECURE_ERROR;
					}
				}
				free(pszGr);
				return SECURE_SUCCESS;
			}
		}
	}
	free(pszGr);
	return nReturn;
}

int CSecureUser::AddLocus(CSecureLocus* pLocus)
{
	if(pLocus==NULL) return SECURE_ERROR;
	if(GetLocusIndex(pLocus->m_pszName)>=0) // theres already a locus of this name
		return SECURE_ALREADYEXISTS;

	CSecureLocus** ppLoci;		// a pointer to an array of pointers to existing member assets
	ppLoci = new CSecureLocus*[m_ulNumLoci+1];
	if(ppLoci==NULL) return SECURE_ERROR;

	if(m_ppLoci)
	{
		memcpy(ppLoci, m_ppLoci, sizeof(CSecureLocus*)*(m_ulNumLoci));
		delete [] m_ppLoci;
	}

	m_ppLoci = ppLoci;
	m_ppLoci[m_ulNumLoci] = pLocus;
	m_ulNumLoci++;

	return SECURE_SUCCESS;

}

int CSecureUser::GetLocusIndex(char* pszLocusName)
{
	unsigned int ulIndex=0;
	if((m_ppLoci)&&(pszLocusName)&&(strlen(pszLocusName)>0))
	{
		while(ulIndex<m_ulNumLoci)
		{
			if((m_ppLoci[ulIndex])&&(m_ppLoci[ulIndex]->m_pszName)&&(strlen(m_ppLoci[ulIndex]->m_pszName)>0))
			{
				if(strcmp(m_ppLoci[ulIndex]->m_pszName, pszLocusName)==0) return ulIndex;
			}
			ulIndex++;
		}
	}
	return -1;
}

int CSecureUser::RemoveLocus(char* pszLocusName)   // only removes it from the permission set, doesn't delete the locus obj
{
	if((m_ppLoci)&&(pszLocusName)&&(strlen(pszLocusName)>0)&&(m_ulNumLoci>0))
	{
		int nIndex = GetLocusIndex(pszLocusName);
		if(nIndex>=0)
		{
			//dont delete the locus object, just remove the pointer from list.
			// delete m_ppLoci[nIndex];  // the pointer is valid, checked in GetLocusIndex
			// truncate the list.
			m_ulNumLoci--;
			while(nIndex<(int)(m_ulNumLoci))
			{
				m_ppLoci[nIndex] = m_ppLoci[nIndex+1];
				nIndex++;
			}

			// allocate a smaller list.
			CSecureLocus** ppLoci;		// a pointer to an array of pointers to existing member assets
			ppLoci = new CSecureLocus*[m_ulNumLoci];

			if(ppLoci)
			{
				memcpy(ppLoci, m_ppLoci, sizeof(CSecureLocus*)*(m_ulNumLoci));
				delete [] m_ppLoci;
				m_ppLoci = ppLoci;
			}
			// else its ok, just let the original buffer exist with an extra entry; m_ulNumAssets is decremented so it doesnt mattter.
		}
	} else return SECURE_ERROR;
	return SECURE_SUCCESS;
}

int CSecureUser::AddAttributes(unsigned long ulFlags)
{
	m_ulFlags |= ulFlags;
	return m_ulFlags;
}

int CSecureUser::RemoveAttributes(unsigned long ulFlags)
{
	m_ulFlags &= ~ulFlags;
	return m_ulFlags;
}




//////////////////////////////////////////////////////////////////////
// CSecurity Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSecurity::CSecurity()
{
	unsigned int i=0;
	for(i=0; i<SECURE_FILE_NUMFILES;i++)
		m_pszFilename[i] = NULL;

	m_ppUsers = NULL;
	m_ulNumUsers = 0;

	m_ppLoci = NULL;
	m_ulNumLoci = 0;

	m_ulMaxAttempts = SECURE_DEFAULT_NUMATTEMPTS;	// default max number of login attempts allowed for users. 0 = infinity
	m_ulPwExpireInt = SECURE_DEFAULT_PWINTERVAL;	// default password expiry interval.  0 = infinity
}

CSecurity::~CSecurity()
{
	unsigned int i=0;
	for(i=0; i<SECURE_FILE_NUMFILES;i++)
		if(m_pszFilename[i]) free(m_pszFilename[i]);

	if(m_ppUsers)
	{
		for(i=0; i<m_ulNumUsers;i++)
		if(m_ppUsers[i]) delete m_ppUsers[i];
		delete [] m_ppUsers;
	}

	if(m_ppLoci)
	{
		for(i=0; i<m_ulNumLoci;i++)
		if(m_ppLoci[i]) delete m_ppLoci[i];
		delete [] m_ppLoci;
	}
}

// core
int 	CSecurity::InitSecurity(char* pszFilename, char* pszBackupFilename, char* pszMagicKeyUser, char* pszMagicKeyPw, unsigned long ulFlags) // can override the magic key
{
	// this function sets up the filenames.  If the files exist, it loads the data into the data members, otherwise it just creates the magic key user.
	if(pszFilename == NULL) return SECURE_ERROR;

	// free older data if any.
	if(m_pszFilename[SECURE_FILE_PRIMARY])	{ free(m_pszFilename[SECURE_FILE_PRIMARY]);		m_pszFilename[SECURE_FILE_PRIMARY]=NULL;}
	if(m_pszFilename[SECURE_FILE_SECONDARY]){ free(m_pszFilename[SECURE_FILE_SECONDARY]); m_pszFilename[SECURE_FILE_SECONDARY]=NULL;}

	m_pszFilename[SECURE_FILE_PRIMARY] = (char*) malloc(strlen(pszFilename)+1);
	if(m_pszFilename[SECURE_FILE_PRIMARY]) strcpy(m_pszFilename[SECURE_FILE_PRIMARY], pszFilename);
	else return SECURE_ERROR;

	
	// remove old data if any
	unsigned int i=0;

	if(m_pAssetList)
	{
		delete m_pAssetList;
		m_pAssetList = NULL;
	}

	if(m_ppUsers)
	{
		for(i=0; i<m_ulNumUsers; i++)
		{
			if(m_ppUsers[i]){  delete m_ppUsers[i]; m_ppUsers[i]=NULL; }
		}
		delete [] m_ppUsers;
		m_ppUsers = NULL;
	}

	if(m_ppLoci)
	{
		for(i=0; i<m_ulNumLoci; i++)
		{
			if(m_ppLoci[i]){  delete m_ppLoci[i]; m_ppLoci[i]=NULL; }
		}
		delete [] m_ppLoci;
		m_ppLoci = NULL;
	}

	m_ulNumUsers = 0;
	m_ulNumLoci = 0;

	if(pszBackupFilename)
	{
		m_pszFilename[SECURE_FILE_SECONDARY] = (char*) malloc(strlen(pszBackupFilename)+1);
		if(m_pszFilename[SECURE_FILE_SECONDARY]) strcpy(m_pszFilename[SECURE_FILE_SECONDARY], pszBackupFilename);
	}

	//lets get the data.
	FILE* fp = fopen(m_pszFilename[SECURE_FILE_PRIMARY], "rb");
	if(fp)  // the file is there.
	{
		fclose(fp); // lets close it and open it in a FileUtil object
		m_file.GetSettings(m_pszFilename[SECURE_FILE_PRIMARY], true);  //encrypted
	}
	else  // try the backup
	{
		if(m_pszFilename[SECURE_FILE_SECONDARY])
		{
			fp = fopen(m_pszFilename[SECURE_FILE_SECONDARY], "rb");
		}
		if(fp)  // the file is there.
		{
			fclose(fp); // lets close it and open it in a FileUtil object
			m_file.GetSettings(m_pszFilename[SECURE_FILE_SECONDARY], true);  //encrypted
		}
	}

	if(m_file.m_ulStatus&FILEUTIL_MALLOC_OK)  // this only gets set if we have a file already.
	{
		unsigned long ulNumUsers		= m_file.GetIniInt("Users", "Num", 0);
		unsigned long ulNumLoci			= m_file.GetIniInt("Loci",  "Num", 0);
		unsigned long ulNumAssets		= m_file.GetIniInt("Assets",  "Num", 0);

		m_ulMaxAttempts = m_file.GetIniInt("Defaults", "Max", SECURE_DEFAULT_NUMATTEMPTS);	// default max number of login attempts allowed for users. 0 = infinity
		m_ulPwExpireInt = m_file.GetIniInt("Defaults", "Expire", SECURE_DEFAULT_PWINTERVAL);	// default password expiry interval.  0 = infinity

		char* pchUser =  m_file.GetIniString("Mx", "U", SECURE_MAGIC_KEY_ID);
		char* pchPw		=  m_file.GetIniString("Mx", "P", SECURE_MAGIC_KEY_PW);

		if((pszMagicKeyUser)&&(strlen(pszMagicKeyUser)))  // override.
		{
			if(pchUser) free(pchUser);
			pchUser = (char*) malloc(strlen(pszMagicKeyUser)+1);
			strcpy(pchUser, pszMagicKeyUser);
		}
		if((pszMagicKeyPw)&&(strlen(pszMagicKeyPw)))  // override
		{
			if(pchPw) free(pchPw);
			pchUser = (char*) malloc(strlen(pszMagicKeyPw)+1);
			strcpy(pchPw, pszMagicKeyPw);
		}
		CSecureUser* pUser= new CSecureUser(pchUser, pchPw);
		m_ulFlags = SECURE_USER_MAGIC;  // all access...

		AddUser(pUser, SECURE_USER_MAGIC);  // add the magic key user, always item [0].

		free(pchUser);
		free(pchPw);

		m_pAssetList = new CSecureAssetList;

		if(m_pAssetList)
		{
			for(i=0; i<ulNumAssets; i++)
			{
				// need to add assets.
				char pchKey[_MAX_PATH];
				_snprintf(pchKey, _MAX_PATH-1, "A%05dName", i);
				char* pchName		=  m_file.GetIniString("Assets", pchKey, "");
				if(strlen(pchName)>0)
				{
					_snprintf(pchKey, _MAX_PATH-1, "A%05dPName", i);
					char* pszParentName =  m_file.GetIniString("Assets", pchKey, "");
					_snprintf(pchKey, _MAX_PATH-1, "A%05dData", i);
					char* pszData =  m_file.GetIniString("Assets", pchKey, "");
					_snprintf(pchKey, _MAX_PATH-1, "A%05dType", i);
					unsigned long ulType = m_file.GetIniInt("Assets", pchKey, SECURE_ASSET_UNDEF);

					CSecureAsset* pAsset = new CSecureAsset(pchName, ulType, pszParentName, pszData);

					m_pAssetList->AddAsset(pAsset);
					if(pchName) free(pchName);
					if(pszParentName) free(pszParentName);
					if(pszData) free(pszData);
				}
				else free(pchName);
			}
		}

		for(i=0; i<ulNumLoci; i++)
		{
			// need to add loci.
			char pchKey[_MAX_PATH];
			_snprintf(pchKey, _MAX_PATH-1, "L%03dName", i);
			char* pchName		=  m_file.GetIniString("Loci", pchKey, "");
			if(strlen(pchName)>0)
			{
				_snprintf(pchKey, _MAX_PATH-1, "L%03dType", i);
				unsigned long ulType = m_file.GetIniInt("Loci", pchKey, SECURE_LOCUS_VIRT);

				CSecureLocus* pLocus = new CSecureLocus(pchName, ulType);

				_snprintf(pchKey, _MAX_PATH-1, "L%03dNumAssets", i);
				unsigned long ulAssets = m_file.GetIniInt("Loci", pchKey, 0);
				free(pchName);

				unsigned long x=0;
				for(x=0; x<ulAssets; x++)
				{
					_snprintf(pchKey, _MAX_PATH-1, "L%03dA%03dName", i, x);
					pchName		=  m_file.GetIniString("Loci", pchKey, "");
					if(strlen(pchName)>0)
					{
						_snprintf(pchKey, _MAX_PATH-1, "L%03dA%03dType", i, x);
						ulType = m_file.GetIniInt("Loci", pchKey, SECURE_ASSET_UNDEF);

						CSecureAsset* pAsset = new CSecureAsset(pchName, ulType);		// a pointer to an array of pointers to existing member assets
						pLocus->AddAsset(pAsset);
					}
					free(pchName);
				}
				AddLocus(pLocus, 0);
			}
			else free(pchName);
		}

		for(i=0; i<ulNumUsers; i++)
		{
			// need to add users.
			char pchKey[_MAX_PATH];
			_snprintf(pchKey, _MAX_PATH-1, "U%03dName", i);
			char* pchUser		=  m_file.GetIniString("Users", pchKey, "");

			_snprintf(pchKey, _MAX_PATH-1, "U%03dPw", i);
			char* pchPw		=  m_file.GetIniString("Users", pchKey, "");

			if(strlen(pchUser))
			{
				CSecureUser* pUser = new CSecureUser(pchUser, pchPw);

				_snprintf(pchKey, _MAX_PATH-1, "U%03dGroups", i);
				pUser->m_pszGroups	=  m_file.GetIniString("Users", pchKey, "");
				if(strlen(pUser->m_pszGroups)<=0) { free(pUser->m_pszGroups); pUser->m_pszGroups=NULL; }
				_snprintf(pchKey, _MAX_PATH-1, "U%03dFlags", i);
				pUser->m_ulFlags = m_file.GetIniInt("Users", pchKey, SECURE_USER_INDIV|SECURE_USER_NOEXP);
				_snprintf(pchKey, _MAX_PATH-1, "U%03dMaxAttempts", i);
				pUser->m_ulMaxAttempts = m_file.GetIniInt("Users", pchKey, m_ulMaxAttempts);	// max number of login attempts allowed for user. 0 = infinity
				_snprintf(pchKey, _MAX_PATH-1, "U%03dPwExpiry", i);
				pUser->m_ulPwExpiry = m_file.GetIniInt("Users", pchKey, m_ulPwExpireInt);				// time when password will expire (unixtime) - set to zero if unused

				free(pchUser);
				free(pchPw);

				_snprintf(pchKey, _MAX_PATH-1, "U%03dNumLoci", i);
				pUser->m_ulNumLoci = m_file.GetIniInt("Users", pchKey, 0);
				pUser->m_ppLoci = (CSecureLocus**)malloc(sizeof(CSecureLocus*)*(pUser->m_ulNumLoci));

				if((pUser->m_ppLoci)&&(m_ppLoci))
				{
					unsigned long x=0;
					for(x=0; x<pUser->m_ulNumLoci; x++)
					{
						_snprintf(pchKey, _MAX_PATH-1, "U%03dL%03dName", i, x);
						pchUser	=  m_file.GetIniString("Users", pchKey, "");

						if(strlen(pchUser)>0)
						{
							int nIndex = GetLocusIndex(pchUser);
							if(nIndex>=0)
							{
								pUser->m_ppLoci[x] = m_ppLoci[nIndex];
							}
							else pUser->m_ppLoci[x] = NULL; // the locus doesnt exist in the master list...
						}
						free(pchUser);
					}
				}
				AddUser(pUser, 0);
			}
			else
			{
				free(pchUser);
				free(pchPw);
			}
		}
	}
	else  // this is new, set it up.
	{
		m_file.GetSettings(m_pszFilename[SECURE_FILE_PRIMARY], true);  //encrypted, create the file object - leave memory blank though.

		m_ulNumUsers = 0;
		m_ulNumLoci  = 0;
		m_ulMaxAttempts = SECURE_DEFAULT_NUMATTEMPTS;	// default max number of login attempts allowed for users. 0 = infinity
		m_ulPwExpireInt = SECURE_DEFAULT_PWINTERVAL;	// default password expiry interval.  0 = infinity

		CSecureUser* pUser= new CSecureUser(pszMagicKeyUser!=NULL?pszMagicKeyUser:SECURE_MAGIC_KEY_ID, pszMagicKeyPw!=NULL?pszMagicKeyPw:SECURE_MAGIC_KEY_PW);
		m_ulFlags = SECURE_USER_MAGIC;  // all access...
		AddUser(pUser, SECURE_USER_MAGIC);  // add the magic key user, always item [0].
	}

	return SECURE_SUCCESS;
}

int 	CSecurity::SaveSecurity(unsigned long ulFlags, char* pszOverrideFilename)
{
	char pszFile[_MAX_PATH];
	int nFiles = 2;
	if((pszOverrideFilename)&&(strlen(pszOverrideFilename)>0))
	{
		nFiles = 1;
		strcpy(pszFile, pszOverrideFilename);
	}
	else strcpy(pszFile, m_pszFilename[SECURE_FILE_PRIMARY]);

	// write the primary (or override file)
	while(nFiles>0)
	{
		if(!(m_file.m_ulStatus&FILEUTIL_MALLOC_OK))
		{
			m_file.GetSettings(pszFile, true);  //encrypted, create the file object - leave memory blank though.
		}

		if(m_file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			m_file.SetIniInt("Defaults", "Max", m_ulMaxAttempts);	// default max number of login attempts allowed for users. 0 = infinity
			m_file.SetIniInt("Defaults", "Expire", m_ulPwExpireInt);	// default password expiry interval.  0 = infinity
			m_file.SetIniInt("Loci",  "Num", m_ulNumLoci);

			if((m_ppUsers)&&(m_ulNumUsers>0)&&(m_ppUsers[0]))
			{
				m_file.SetIniInt("Users", "Num", m_ulNumUsers-1);  // -1 because the first is the magic user.
				m_file.SetIniString("Mx", "U", m_ppUsers[0]->m_pszUser?m_ppUsers[0]->m_pszUser:"", "mx");
				m_file.SetIniString("Mx", "P", m_ppUsers[0]->m_pszPassword?m_ppUsers[0]->m_pszPassword:"", "mx");
			}
			if(m_pAssetList)
			{
				m_file.SetIniInt("Assets", "Num", m_pAssetList->m_ulNumAssets); 
			}

// loci
			unsigned long i;
			for(i=0; i<m_ulNumLoci;i++)
			{
				// need to add loci.
				if((m_ppLoci)&&(m_ppLoci[i]))
				{
					char pchKey[_MAX_PATH];
					_snprintf(pchKey, _MAX_PATH-1, "L%03dName", i);
					if((m_ppLoci[i]->m_pszName)&&(strlen(m_ppLoci[i]->m_pszName))) 
						m_file.SetIniString("Loci", pchKey, m_ppLoci[i]->m_pszName);
					else
						m_file.SetIniString("Loci", pchKey, "", "n");
					_snprintf(pchKey, _MAX_PATH-1, "L%03dType", i);
					m_file.SetIniInt("Loci", pchKey, m_ppLoci[i]->m_ulType);
					_snprintf(pchKey, _MAX_PATH-1, "L%03dNumAssets", i);
					m_file.SetIniInt("Loci", pchKey, m_ppLoci[i]->m_ulNumAssets);

					unsigned long x=0;
					for(x=0; x<m_ppLoci[i]->m_ulNumAssets; x++)
					{
						if((m_ppLoci[i]->m_ppAssets)&&(m_ppLoci[i]->m_ppAssets[x]))
						{
							_snprintf(pchKey, _MAX_PATH-1, "L%03dA%03dName", i, x);
							if((m_ppLoci[i]->m_ppAssets[x]->m_pszName)&&(strlen(m_ppLoci[i]->m_ppAssets[x]->m_pszName))) 
								m_file.SetIniString("Loci", pchKey, m_ppLoci[i]->m_ppAssets[x]->m_pszName);
							else
								m_file.SetIniString("Loci", pchKey, "", "a");
							_snprintf(pchKey, _MAX_PATH-1, "L%03dA%03dType", i, x);
							m_file.SetIniInt("Loci", pchKey, m_ppLoci[i]->m_ppAssets[x]->m_ulType);
						}
					}
				}
			}

// users
			unsigned long j=0;
			for(j=0; j<m_ulNumUsers-1; j++)
			{
				i=j+1;  // skip user[0], which is the magic user
				// need to add users.
				if((m_ppUsers)&&(m_ppUsers[i]))
				{
					char pchKey[_MAX_PATH];
					_snprintf(pchKey, _MAX_PATH-1, "U%03dName", j);
					if((m_ppUsers[i]->m_pszUser)&&(strlen(m_ppUsers[i]->m_pszUser))) 
						m_file.SetIniString("Users", pchKey, m_ppUsers[i]->m_pszUser);
					else
						m_file.SetIniString("Users", pchKey, "", "u");
					_snprintf(pchKey, _MAX_PATH-1, "U%03dPw", j);
					if((m_ppUsers[i]->m_pszPassword)&&(strlen(m_ppUsers[i]->m_pszPassword))) 
						m_file.SetIniString("Users", pchKey, m_ppUsers[i]->m_pszPassword);
					else
						m_file.SetIniString("Users", pchKey, "", "p");
					_snprintf(pchKey, _MAX_PATH-1, "U%03dGroups", j);
					if((m_ppUsers[i]->m_pszGroups)&&(strlen(m_ppUsers[i]->m_pszGroups))) 
						m_file.SetIniString("Users", pchKey, m_ppUsers[i]->m_pszGroups);
					else
						m_file.SetIniString("Users", pchKey, "", "g");
					_snprintf(pchKey, _MAX_PATH-1, "U%03dFlags", j);
					m_file.SetIniInt("Users", pchKey, m_ppUsers[i]->m_ulFlags);
					_snprintf(pchKey, _MAX_PATH-1, "U%03dMaxAttempts", j);
					m_file.SetIniInt("Users", pchKey, m_ppUsers[i]->m_ulMaxAttempts);	// max number of login attempts allowed for user. 0 = infinity
					_snprintf(pchKey, _MAX_PATH-1, "U%03dPwExpiry", j);
					m_file.SetIniInt("Users", pchKey, m_ppUsers[i]->m_ulPwExpiry);				// time when password will expire (unixtime) - set to zero if unused
					_snprintf(pchKey, _MAX_PATH-1, "U%03dNumLoci", j);
					m_file.SetIniInt("Users", pchKey, m_ppUsers[i]->m_ulNumLoci);

					unsigned long x=0;
					for(x=0; x<m_ppUsers[i]->m_ulNumLoci; x++)
					{
						if((m_ppUsers[i]->m_ppLoci)&&(m_ppUsers[i]->m_ppLoci[x]))
						{
							_snprintf(pchKey, _MAX_PATH-1, "U%03dL%03dName", j, x);
							if((m_ppUsers[i]->m_ppLoci[x]->m_pszName)&&(strlen(m_ppUsers[i]->m_ppLoci[x]->m_pszName))) 
								m_file.SetIniString("Users", pchKey, m_ppUsers[i]->m_ppLoci[x]->m_pszName);
							else
								m_file.SetIniString("Users", pchKey, "", "l");
						}
					}
				}
			}

// asset list
			if(m_pAssetList)
			{
				for(i=0; i<m_pAssetList->m_ulNumAssets;i++)
				{
					// need to add assets.
					if((m_pAssetList->m_ppAssets)&&(m_pAssetList->m_ppAssets[i]))
					{
						char pchKey[_MAX_PATH];
						_snprintf(pchKey, _MAX_PATH-1, "A%05dName", i);
						if((m_pAssetList->m_ppAssets[i]->m_pszName)&&(strlen(m_pAssetList->m_ppAssets[i]->m_pszName))) 
							m_file.SetIniString("Assets", pchKey, m_pAssetList->m_ppAssets[i]->m_pszName);
						else
							m_file.SetIniString("Assets", pchKey, "", "n");
						_snprintf(pchKey, _MAX_PATH-1, "A%05dPName", i);
						if((m_pAssetList->m_ppAssets[i]->m_pszName)&&(strlen(m_pAssetList->m_ppAssets[i]->m_pszParentName))) 
							m_file.SetIniString("Assets", pchKey, m_pAssetList->m_ppAssets[i]->m_pszParentName);
						else
							m_file.SetIniString("Assets", pchKey, "", "n");
						_snprintf(pchKey, _MAX_PATH-1, "A%05dData", i);
						if((m_pAssetList->m_ppAssets[i]->m_pszName)&&(strlen(m_pAssetList->m_ppAssets[i]->m_pszData))) 
							m_file.SetIniString("Assets", pchKey, m_pAssetList->m_ppAssets[i]->m_pszData);
						else
							m_file.SetIniString("Assets", pchKey, "", "n");
						_snprintf(pchKey, _MAX_PATH-1, "A%05dType", i);
						m_file.SetIniInt("Assets", pchKey, m_pAssetList->m_ppAssets[i]->m_ulType);
					}
				}
			}
		}


		//////  this section is paranoia - when we overwrite the file, we save the last set so we can undo.
		char pszBackupFile[_MAX_PATH];
		strcpy(pszBackupFile, pszFile);
		// change the last letter of the filename to @, unless it's @ already, in which case chage it to $
		if(pszBackupFile[strlen(pszBackupFile)-1] == '@') pszBackupFile[strlen(pszBackupFile)-1] = '$';
		else pszBackupFile[strlen(pszBackupFile)-1] = '@';

		_unlink(pszBackupFile); // delete the old backup file (if exists).
		rename(pszFile, pszBackupFile ); // write new backup file
		//////  end paranoia section

		m_file.SetSettings(pszFile, true);  //encrypted, create the file object - leave memory blank though.

		nFiles--;
		if(nFiles>0) strcpy(pszFile, m_pszFilename[SECURE_FILE_SECONDARY]);  // go again with backup
	}
		
	return SECURE_SUCCESS;
}

int 	CSecurity::CheckSecure(char* pszUser, char* pszPassword, char* pszLocus, char* pszAsset )  // checks security on asset or locus. if both supplied, further checks asset is in locus
{
	if((pszUser)&&(strlen(pszUser)>0))
	{
		int nIndex = GetUserIndex(pszUser);
		if(nIndex>=0)
		{
			if(m_ppUsers[nIndex]->m_pszPassword)
			{
				if(strcmp(m_ppUsers[nIndex]->m_pszPassword, pszPassword)!=0) // password mismatch
				{
					m_ppUsers[nIndex]->m_ulNumAttempts++;
					if(m_ppUsers[nIndex]->m_ulNumAttempts<m_ppUsers[nIndex]->m_ulMaxAttempts)
					{
						if((m_ppUsers[nIndex]->m_pszGroups)&&(strlen(m_ppUsers[nIndex]->m_pszGroups)))
						{
							// have to check groups.
							CSafeBufferUtil sbu;
							char delims[2] = { SECURE_USER_GROUPDELIM, 0 };
							char* pchGroup = sbu.Token(m_ppUsers[nIndex]->m_pszGroups, strlen(m_ppUsers[nIndex]->m_pszGroups), delims);
							while(pchGroup)
							{
								if(CheckSecure(pchGroup, pszPassword, pszLocus, pszAsset) == SECURE_SUCCESS) return SECURE_SUCCESS;
								pchGroup = sbu.Token(NULL, 0, delims);
							}
						}
					} else return SECURE_MAXATTEMPTS;
					return SECURE_ERROR;  // couldnt find any group that matched.
				}
			}
			// at this point, we have the user and checked the password.
			// now we need to check the asset/locus if necessary
			// except if this is the magic key, which has ALL access.
			// by definition, magic key is user[0].

			if(nIndex == 0)  // if magic key.
			{
				m_ppUsers[nIndex]->m_ulNumAttempts=0;
				return SECURE_SUCCESS;
			}

			if((pszLocus)&&(strlen(pszLocus)>0))
			{
				int nLocus = m_ppUsers[nIndex]->GetLocusIndex(pszLocus);
				if(nLocus<0)
				{
					return SECURE_EXCLUDED;  // couldnt find a locus that matched.
				}
				else
				{
					if((pszAsset)&&(strlen(pszAsset)>0))
					{
						int nAsset = m_ppUsers[nIndex]->m_ppLoci[nLocus]->GetAssetIndex(pszAsset);
						if(nAsset<0)
						{
							return SECURE_EXCLUDED;  // couldnt find an asset that matched in the specified locus.
						}
					}
				}
			}
			else // if no locus specified, BUT an asset specified, need to check all Loci for the asset.
			{
				if((pszAsset)&&(strlen(pszAsset)>0))
				{
					unsigned long ulLocus = 0;
					while(ulLocus<m_ppUsers[nIndex]->m_ulNumLoci)
					{
						int nAsset = m_ppUsers[nIndex]->m_ppLoci[ulLocus]->GetAssetIndex(pszAsset);
						if(nAsset>=0)
						{	
							if(m_ppUsers[nIndex]->m_ulPwExpiry < (unsigned long)time(NULL))
							{
								m_ppUsers[nIndex]->m_ulNumAttempts=0;
								return SECURE_SUCCESS; // it's in a locus.
							}
							else //expired
							{
								return SECURE_EXPIRED;
							}
						}
						ulLocus++;
					}
					return SECURE_EXCLUDED;  // couldnt find an asset that matched in any locus.
				}

			}
			if(m_ppUsers[nIndex]->m_ulPwExpiry < (unsigned long)time(NULL))
			{
				m_ppUsers[nIndex]->m_ulNumAttempts=0;
				return SECURE_SUCCESS; // it's in a locus.
			}
			else //expired
			{
				return SECURE_EXPIRED;
			}
		}
		else return SECURE_NOTEXISTS;
	}
	return SECURE_ERROR;
}

// users
int 	CSecurity::AddUser(CSecureUser* pUser, unsigned long ulFlags)
{
	if(pUser==NULL) return SECURE_ERROR;
	if(GetUserIndex(pUser->m_pszUser)>=0) // theres already a user of this name
		return SECURE_ALREADYEXISTS;

	CSecureUser** ppUsers;		// a pointer to an array of pointers to existing member assets
	ppUsers = new CSecureUser*[m_ulNumUsers+1];
	if(ppUsers==NULL) return SECURE_ERROR;

	if(m_ppUsers)
	{
		memcpy(ppUsers, m_ppUsers, sizeof(CSecureUser*)*(m_ulNumUsers));
		delete [] m_ppUsers;
	}

	m_ppUsers = ppUsers;
	m_ppUsers[m_ulNumUsers] = pUser;
	m_ulNumUsers++;

	SaveSecurity();  //update the file
	return SECURE_SUCCESS;
}

int 	CSecurity::DeleteUser(char* pszUser, unsigned long ulFlags)
{
	if((m_ppUsers)&&(pszUser)&&(strlen(pszUser)>0)&&(m_ulNumUsers>0))
	{
		int nIndex = GetUserIndex(pszUser);
		if(nIndex>=0)
		{
			//delete the user object
			delete m_ppUsers[nIndex];  // the pointer is valid, checked in GetUserIndex

			// truncate the list.
			m_ulNumUsers--;
			while(nIndex<(int)(m_ulNumUsers))
			{
				m_ppUsers[nIndex] = m_ppUsers[nIndex+1];
				nIndex++;
			}

			// allocate a smaller list.
			CSecureUser** ppUsers;		// a pointer to an array of pointers to existing member assets
			ppUsers = new CSecureUser*[m_ulNumUsers];

			if(ppUsers)
			{
				memcpy(ppUsers, m_ppUsers, sizeof(CSecureUser*)*(m_ulNumUsers));
				delete [] m_ppUsers;
				m_ppUsers = ppUsers;
			}
			// else its ok, just let the original buffer exist with an extra entry; m_ulNumUsers is decremented so it doesnt mattter.
		}
	} else return SECURE_ERROR;
	SaveSecurity();  //update the file
	return SECURE_SUCCESS;
}

int 	CSecurity::GetUserIndex(char* pszUser)
{
	unsigned int ulIndex=0;
	if((m_ppUsers)&&(pszUser)&&(strlen(pszUser)>0))
	{
		while(ulIndex<m_ulNumUsers)
		{
			if((m_ppUsers[ulIndex])&&(m_ppUsers[ulIndex]->m_pszUser)&&(strlen(m_ppUsers[ulIndex]->m_pszUser)>0))
			{
				if(strcmp(m_ppUsers[ulIndex]->m_pszUser, pszUser)==0) return ulIndex;
			}
			ulIndex++;
		}
	}
	return -1;
}


int 	CSecurity::AddLocus(CSecureLocus* pLocus, unsigned long ulFlags)
{
	if(pLocus==NULL) return SECURE_ERROR;
	if(GetLocusIndex(pLocus->m_pszName)>=0) // theres already a locus of this name
		return SECURE_ALREADYEXISTS;

	CSecureLocus** ppLoci;		// a pointer to an array of pointers to existing member assets
	ppLoci = new CSecureLocus*[m_ulNumLoci+1];
	if(ppLoci==NULL) return SECURE_ERROR;

	if(m_ppLoci)
	{
		memcpy(ppLoci, m_ppLoci, sizeof(CSecureLocus*)*(m_ulNumLoci));
		delete [] m_ppLoci;
	}

	m_ppLoci = ppLoci;
	m_ppLoci[m_ulNumLoci] = pLocus;
	m_ulNumLoci++;

	SaveSecurity();  //update the file
	return SECURE_SUCCESS;
}

int 	CSecurity::DeleteLocus(char* pszName, unsigned long ulFlags)
{
	if((m_ppLoci)&&(pszName)&&(strlen(pszName)>0)&&(m_ulNumLoci>0))
	{
		int nIndex = GetLocusIndex(pszName);
		if(nIndex>=0)
		{
			//delete the locus object
			delete m_ppLoci[nIndex];  // the pointer is valid, checked in GetLocusIndex
			// truncate the list.
			m_ulNumLoci--;
			while(nIndex<(int)(m_ulNumLoci))
			{
				m_ppLoci[nIndex] = m_ppLoci[nIndex+1];
				nIndex++;
			}
			// allocate a smaller list.
			CSecureLocus** ppLoci;		// a pointer to an array of pointers to existing member assets
			ppLoci = new CSecureLocus*[m_ulNumLoci];

			if(ppLoci)
			{
				memcpy(ppLoci, m_ppLoci, sizeof(CSecureLocus*)*(m_ulNumLoci));
				delete [] m_ppLoci;
				m_ppLoci = ppLoci;
			}
			// else its ok, just let the original buffer exist with an extra entry; m_ulNumLoci is decremented so it doesnt mattter.
		}
	} else return SECURE_ERROR;
	SaveSecurity();  //update the file
	return SECURE_SUCCESS;
}

int 	CSecurity::GetLocusIndex(char* pszName)
{
	unsigned int ulIndex=0;
	if((m_ppLoci)&&(pszName)&&(strlen(pszName)>0))
	{
		while(ulIndex<m_ulNumLoci)
		{
			if((m_ppLoci[ulIndex])&&(m_ppLoci[ulIndex]->m_pszName)&&(strlen(m_ppLoci[ulIndex]->m_pszName)>0))
			{
				if(strcmp(m_ppLoci[ulIndex]->m_pszName, pszName)==0) return ulIndex;
			}
			ulIndex++;
		}
	}
	return -1;
}

int 	CSecurity::ClearAssetList()
{
	if(m_pAssetList)
	{
		if(m_pAssetList->m_ppAssets)
		{
			for(unsigned long i=0; i<m_pAssetList->m_ulNumAssets; i++)
			if(m_pAssetList->m_ppAssets[i]) delete m_pAssetList->m_ppAssets[i];
			delete [] m_pAssetList->m_ppAssets;
		}
	}
	SaveSecurity();  //update the file
	return SECURE_SUCCESS;
}
