// Security.h: interface for the CSecurity and support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SECURITY_H__BD7685ED_3AC0_425A_A010_9AF7F04B3A68__INCLUDED_)
#define AFX_SECURITY_H__BD7685ED_3AC0_425A_A010_9AF7F04B3A68__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "SecurityDefines.h"
#include "FileUtil.h"


//////////////////////
// SECURITY MANAGEMENT
//
// assets are base level items.
class CSecureAsset  
{
public:
	CSecureAsset(char* pszName, unsigned long ulType=SECURE_ASSET_UNDEF, char* pszParentName=NULL, char* pszData=NULL);
	virtual ~CSecureAsset();
	int ModifyAsset(char* pszName, unsigned long ulType, char* pszParentName=NULL, char* pszData=NULL);

public:
	char* m_pszName;
	char* m_pszParentName;
	char* m_pszData;
	unsigned long m_ulType;
};


class CSecureAssetList  
{
public:
	CSecureAssetList();
	virtual ~CSecureAssetList();

	int AddAsset(CSecureAsset* pAsset);
	int GetAssetIndex(char* pszAssetName);
	int RemoveAsset(char* pszAssetName);   // removes asset and deletes obj
	int CheckSubAsset(char* pszSubAssetName, char* pszAssetName);  // recursively checks an asset to see if it is a subasset of an asset

public:
	CSecureAsset** m_ppAssets;
	unsigned long m_ulNumAssets;
};


// loci are virtual areas with member assets to be secured.
// loci can be members of other loci
class CSecureLocus  
{
public:
	CSecureLocus(char* pszName, unsigned long ulType=SECURE_LOCUS_VIRT);
	virtual ~CSecureLocus();

	int ChangeName(char* pszName);

	int AddAsset(CSecureAsset* pAsset);
	int GetAssetIndex(char* pszAssetName);
	int RemoveAsset(char* pszAssetName);   // removes asset and deletes obj

	int AddAttributes(unsigned long ulFlags);
	int RemoveAttributes(unsigned long ulFlags);

public:
	char* m_pszName;
	unsigned long m_ulType;
	CSecureAsset** m_ppAssets;		// a pointer to an array of pointers to existing member assets
	unsigned long m_ulNumAssets;
};


// users are entities wishing to access members of secure loci
// a group, internally, is a user.
// groups can be part of other groups
class CSecureUser  
{
public:
	CSecureUser(char* pszUser, char* pszPassword);
	virtual ~CSecureUser();

	int ChangePassword(char* pszPw);

	int AddGroup(char* pszGroup);
	int RemoveGroup(char* pszGroup);  // only removes it from the permission set, doesn't delete the user group obj

	int AddLocus(CSecureLocus* pLocus);
	int GetLocusIndex(char* pszLocusName);
	int RemoveLocus(char* pszLocusName);   // only removes it from the permission set, doesn't delete the locus obj

	int AddAttributes(unsigned long ulFlags);
	int RemoveAttributes(unsigned long ulFlags);

public:
	char* m_pszUser;
	char* m_pszPassword;
	char* m_pszGroups;							// group names are stored in a formatted string.
	unsigned long m_ulFlags;				// settings such as ability to change pw etc
	CSecureLocus** m_ppLoci;				// a pointer to an array of pointers to existing loci
	unsigned long m_ulNumLoci;			// number of pointers in the array
	unsigned long m_ulMaxAttempts;	// max number of login attempts allowed for user. 0 = infinity
	unsigned long m_ulNumAttempts;	// number of unsuccessful login attempts since last success
	unsigned long m_ulPwExpiry;			// time when password will expire (unixtime) - set to zero if unused
};


class CSecurity  
{
public:
	CSecurity();
	virtual ~CSecurity();

public:
	unsigned long m_ulFlags;  // general settings
	unsigned long m_ulStatus; // general status

	char* m_pszFilename[SECURE_FILE_NUMFILES];

	CFileUtil m_file; // the security data itself - hold in memory rather than parsing files all the time

	CSecureUser**		m_ppUsers;
	unsigned long		m_ulNumUsers;

	CSecureLocus**	m_ppLoci;
	unsigned long		m_ulNumLoci;

	unsigned long m_ulMaxAttempts;	// default max number of login attempts allowed for users. 0 = infinity
	unsigned long m_ulPwExpireInt;	// default password expiry interval.  0 = infinity

	CSecureAssetList* m_pAssetList;

	// core
	int 	InitSecurity(char* pszFilename, char* pszBackupFilename=NULL, char* pszMagicKeyUser=NULL, char* pszMagicKeyPw=NULL, unsigned long ulFlags=0); // can override the magic key
	int 	SaveSecurity(unsigned long ulFlags=0, char* pszOverrideFilename=NULL);
	int 	CheckSecure(char* pszUser, char* pszPassword, char* pszLocus=NULL, char* pszAsset=NULL);  // checks security on asset or locus. if both supplied, further checks asset is in locus

	// users
	int 	AddUser(CSecureUser* pUser, unsigned long ulFlags);
	int 	DeleteUser(char* pszUser, unsigned long ulFlags);
	int 	GetUserIndex(char* pszUser);

	//loci
	int 	AddLocus(CSecureLocus* pLocus, unsigned long ulFlags);
	int 	DeleteLocus(char* pszName, unsigned long ulFlags);
	int 	GetLocusIndex(char* pszName);

	// asset list
	int		ClearAssetList();
};

#endif // !defined(AFX_SECURITY_H__BD7685ED_3AC0_425A_A010_9AF7F04B3A68__INCLUDED_)


/*
//////////////////////////////////////////////
// TO DO LIST
//////////////////////////////////////////////

	- initialize and save asset list in Init/SaveSecurity

*/