// SecurityDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(SECURITYDEFINES_H_INCLUDED)
#define SECURITYDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

#define SECURE_MAGIC_KEY_ID	  "master"
#define SECURE_MAGIC_KEY_PW	  "slave"


#define SECURE_ASSET_UNDEF	0x00000000		// default asset type, top level
#define SECURE_ASSET_FILE		0x00000001		// file
#define SECURE_ASSET_DIR		0x00000002		// directory
#define SECURE_ASSET_USER		0x00000003		// user object
#define SECURE_ASSET_EXCL		0x00000100		// exclusive - do not allow descending perm.  i.e. if an asset is a dir, do not consider sub-dirs part of asset
#define SECURE_ASSET_CHILD	0x40000000		// child asset
#define SECURE_ASSET_PARENT	0x80000000		// parent asset


#define SECURE_LOCUS_VIRT		0x00000000		// default asset type, virtual
#define SECURE_LOCUS_USER		0x00000003		// user type


#define SECURE_SUCCESS					0
#define SECURE_ERROR						-1  //generic
#define SECURE_ALREADYEXISTS		-2  
#define SECURE_NOTEXISTS				-3  //user doesnt exist
#define SECURE_MISMATCH					-4  //pw/user mismatch
#define SECURE_EXCLUDED					-5  //pw match but excluded asset
#define SECURE_EXPIRED					-6  //pw match but expired
#define SECURE_MAXATTEMPTS			-7  //maximum attempts exceeded

#define SECURE_USER_INDIV				0x00000000		// individual user
#define SECURE_USER_GROUP				0x00000001		// user group

#define SECURE_USER_CHGNEXT			0x00000010		// user must change pw at next login
#define SECURE_USER_NOCHG				0x00000020		// user cannot change password
#define SECURE_USER_NOEXP				0x00000040		// password never expires
#define SECURE_USER_DISABLED		0x00000080		// user account is disabled
#define SECURE_USER_LOCKED			0x00000100		// user account is locked out

#define SECURE_USER_LOGFAIL			0x00001000		// log all failed attempts by user
#define SECURE_USER_LOGLOGIN		0x00002000		// log all logins
#define SECURE_USER_LOGACT			0x00004000		// log all activity
#define SECURE_USER_LOGALL			0x00007000		// log all attempts and activity

#define SECURE_USER_MAGIC				0x80000000		// all access no matter what.  (needed for recovery)

#define SECURE_USER_GROUPDELIM	31  // same as ascii US

#define SECURE_FILE_ENCRYPT			0x00000000		// default
#define SECURE_FILE_BACKUP			0x00000000		// default
#define SECURE_FILE_NOENCRYPT		0x00000001		// do not use encrypted files
#define SECURE_FILE_NOBKUPENC		0x00000002		// do not use encryption on backup files
#define SECURE_FILE_NOBACKUP		0x00000004		// do not use backup files
#define SECURE_FILE_FAILBKUP		0x00000008		// only write to backup file on failure. (this means, dont write to the backup file except when the primary file can not be written to)


#define SECURE_FILE_PRIMARY				0
#define SECURE_FILE_SECONDARY			1
#define SECURE_FILE_NUMFILES			2

#define SECURE_DEFAULT_NUMATTEMPTS			10
#define SECURE_DEFAULT_PWINTERVAL				0  // = infinity

#endif // !defined(SECURITYDEFINES_H_INCLUDED)
