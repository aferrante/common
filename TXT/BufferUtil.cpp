// BufferUtil.cpp: implementation of the CBufferUtil class.
//
//////////////////////////////////////////////////////////////////////

//#include <stdafx.h>  //remove this

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <io.h>
#include <string.h>
#include "BufferUtil.h"


#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif


//////////////////////////////////////////////////////////////////////
// CSafeBufferUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSafeBufferUtil::CSafeBufferUtil()
{
	m_pszLastToken = NULL; // the pointer to the last token
	m_pszTokenBuffer = NULL;		// pointer to the entire buffer
	m_ulTokenBufferLength = 0;
	m_ulLastTokenLength=0;
}

CSafeBufferUtil::~CSafeBufferUtil()
{
	if(m_pszTokenBuffer) free(m_pszTokenBuffer);	// must use malloc to allocate
}

// this function is like strtok, except that you can put in a non-zero terminated buffer if you want
// it still delimits on zero, and if pchBuffer is NULL, ulBufferLength is ignored that call...
// it returns a pointer to the place in the buffer where each field starts, but does NOT allocate
// a new buffer to hold the field contents.  Therefore, do not free or delete the returned pointer.
char*	CSafeBufferUtil::Token(char* pchBuffer, unsigned long ulBufferLength, char* pszTokens, unsigned long ulMode)
{
	if((pchBuffer==NULL)&&(m_pszLastToken==NULL)) // we're all done here...
	{
		m_pszLastToken = NULL;			// the pointer to the last token
		if(m_pszTokenBuffer) free(m_pszTokenBuffer);
		m_pszTokenBuffer = NULL;		// pointer to the entire buffer
		m_ulTokenBufferLength = 0;
		m_ulLastTokenLength = 0;
		return NULL;
	}

	if((pszTokens==NULL)||(strlen(pszTokens)<=0))  // no tokens
	{
		if(pchBuffer)
		{
			if(m_pszTokenBuffer) free(m_pszTokenBuffer);
			m_pszTokenBuffer = (char*) malloc(ulBufferLength+1); // we will put a term zero for safety
			if(m_pszTokenBuffer)
			{
				memcpy(m_pszTokenBuffer, pchBuffer, ulBufferLength);
				*(m_pszTokenBuffer+ulBufferLength)=0;
				m_pszLastToken = m_pszTokenBuffer;			// the pointer to the last token
				m_ulTokenBufferLength = ulBufferLength;
				m_ulLastTokenLength = strlen(m_pszLastToken);
				return m_pszLastToken;
			}
			else
			{
				m_pszLastToken = NULL;			// the pointer to the last token
				m_pszTokenBuffer = NULL;		// pointer to the entire buffer
				m_ulTokenBufferLength = 0;
				m_ulLastTokenLength = 0;
				return NULL;
			}
		}
		else
		{
			m_pszLastToken = NULL;			// the pointer to the last token
			if(m_pszTokenBuffer) free(m_pszTokenBuffer);
			m_pszTokenBuffer = NULL;		// pointer to the entire buffer
			m_ulTokenBufferLength = 0;
			m_ulLastTokenLength = 0;
			return NULL;
		}
	}

	if(pchBuffer)
	{
		if(m_pszTokenBuffer) free(m_pszTokenBuffer);
		m_pszTokenBuffer = (char*) malloc(ulBufferLength+1);
		if(m_pszTokenBuffer)
		{
			memcpy(m_pszTokenBuffer, pchBuffer, ulBufferLength);
			*(m_pszTokenBuffer+ulBufferLength)=0;
			m_pszLastToken = NULL;			// the pointer to the first token
			if(ulMode==MODE_SINGLEDELIM)	m_pszLastToken = m_pszTokenBuffer;			// the pointer to the first token
			m_ulTokenBufferLength = ulBufferLength;
			
			// have to add zeros where we have delims
			char* pch = m_pszTokenBuffer;
			bool bSkippedLeading = false; if(ulMode==MODE_SINGLEDELIM) bSkippedLeading=true;  // defeat skip leading for single delim mode
			bool bFoundDelim = false;
			bool bFoundNonDelim = false;
			unsigned long ulLen = strlen(pszTokens);

			while((pch<(m_pszTokenBuffer+m_ulTokenBufferLength))&&(!bFoundNonDelim))
			{
				//skip any leading delims
				unsigned long i = 0;
				while((!bSkippedLeading)&&(i<ulLen))
				{
					if(*pch==pszTokens[i])
					{
						*pch=0; 
						break;
					}
					else i++;
					if(i>=ulLen)
					{
						m_pszLastToken = pch;
						bSkippedLeading = true; // didn't find a delimiter.
					}
				}
				if(bSkippedLeading)
				{
					i = 0;
					while((!bFoundDelim)&&(i<ulLen))
					{
						if(*pch==pszTokens[i])
						{
							bFoundDelim = true;
						}
						i++;
					}
					i = 0;
					while((bFoundDelim)&&(!bFoundNonDelim)&&(i<ulLen))
					{
						if(*pch==pszTokens[i])
						{
							*pch=0;
							if(ulMode==MODE_SINGLEDELIM) bFoundNonDelim=true;  // defeat eating up more delims for single delim mode
							break;
						}
						else i++;
						if(i>=ulLen) bFoundNonDelim = true; // didn't find a delimiter.
					}
				}
				pch++;
			}
			if(m_pszLastToken)
				m_ulLastTokenLength = strlen(m_pszLastToken);
			else
				m_ulLastTokenLength = 0;
			return m_pszLastToken;
		}
		else
		{
			m_pszLastToken = NULL;			// the pointer to the last token
			m_pszTokenBuffer = NULL;		// pointer to the entire buffer
			m_ulTokenBufferLength = 0;
			m_ulLastTokenLength = 0;
			return NULL;
		}

	}
	else
	{
		// NULL was passed in, so we have to use the existing buffer.
		char* pch = m_pszLastToken+m_ulLastTokenLength;
		bool bFoundDelim = false;
		bool bFoundNonDelim = false;
		unsigned long ulLen = strlen(pszTokens);
		char* pchStart = NULL;
		while((pch<(m_pszTokenBuffer+m_ulTokenBufferLength))&&(!bFoundNonDelim))
		{
			if((*pch)!=0)  //skip delims
			{
				if(pchStart == NULL) pchStart = pch;
				unsigned long i = 0;
				while((!bFoundDelim)&&(i<ulLen))
				{
					if(*pch==pszTokens[i])
					{
						bFoundDelim = true;
					}
					i++;
				}
				i = 0;
				while((bFoundDelim)&&(!bFoundNonDelim)&&(i<ulLen))
				{
					if(*pch==pszTokens[i])
					{
						// have to add zeros where we have delims
						*pch=0; 
						if(ulMode==MODE_SINGLEDELIM) bFoundNonDelim=true;  // defeat eating up more delims for single delim mode
						break;
					}
					else i++;
					if(i>=ulLen) bFoundNonDelim = true; // didn't find a delimiter.
				}
			}
			pch++;
		}

		if(pch>=(m_pszTokenBuffer+m_ulTokenBufferLength)) //we are at the end.
		{
			if(pchStart==NULL)
			{
				m_pszLastToken = NULL;
				if(m_pszTokenBuffer) free(m_pszTokenBuffer);
				m_pszTokenBuffer = NULL;
				m_ulTokenBufferLength = 0;
			}
			else
			{
				m_pszLastToken = pchStart;
			}
		}
		else
		{
			m_pszLastToken = pchStart;
		}
		if(m_pszLastToken)
			m_ulLastTokenLength = strlen(m_pszLastToken);
		else
			m_ulLastTokenLength = 0;
		return m_pszLastToken;
	}

}






//////////////////////////////////////////////////////////////////////
// CBufferUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBufferUtil::CBufferUtil()
{

}

CBufferUtil::~CBufferUtil()
{

}

int CBufferUtil::Base64Encode(char** ppchBuffer, unsigned long* pulBufLen, bool bFreeBuffer, char* pszAlphabet, char chPad)
{
	// takes in a buffer that may contain chars of value 0 that do not terminate the "string".
	// encodes the buffer up to the passed in length.
	// creates a new buffer with the encoded chars, and a terminating NULL.
	// possibly frees the incoming buffer.
	// resets the buffer pointer to the new buffer, and reassignes the length pointer to the new buffer length

	char* pchBuffer = *ppchBuffer;
	if(pchBuffer==NULL) return ERROR_B64_NULLPTR;

	int nReturnCode = RETURN_SUCCESS;

	if(strlen(pszAlphabet)<64)
	{
		pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
	}
	if((chPad<=0)||(strchr(pszAlphabet, chPad)!=NULL))
	{
		if(strchr(pszAlphabet, '=')!=NULL)
		{
			pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
	}

  // pad is = char
  unsigned char igroup[3], ogroup[4];
  unsigned long i, nLen, n=0;

  nLen = *pulBufLen;

  unsigned long ulDataLength = (nLen/3+((nLen%3)?1:0))*4;

	char* pch = (char*)malloc(ulDataLength+1); //+1 for terminating null
	char* pchOut = pch;

	if(pchOut==NULL)  return ERROR_B64_NULLMALLOC;

  for (i=0; i<nLen; i+=3)
  {
    // take three bytes in
    if (i<nLen)     { igroup[0] = pchBuffer[i];   n=1; } else igroup[0]=0;
    if ((i+1)<nLen) { igroup[1] = pchBuffer[i+1]; n=2; } else igroup[1]=0;
    if ((i+2)<nLen) { igroup[2] = pchBuffer[i+2]; n=3; } else igroup[2]=0;

    ogroup[0] = pszAlphabet[igroup[0] >> 2];
    ogroup[1] = pszAlphabet[((igroup[0] & 0x03) << 4) | (igroup[1] >> 4)];
    ogroup[2] = pszAlphabet[((igroup[1] & 0x0f) << 2) | (igroup[2] >> 6)];
    ogroup[3] = pszAlphabet[igroup[2] & 0x3f];
    
    if (n < 3) 
    {
      ogroup[3] = chPad;
      if (n < 2) 
      {
        ogroup[2] = chPad;
      }
    }

	  *(pch++) = ogroup[0];
	  *(pch++) = ogroup[1];
	  *(pch++) = ogroup[2];
	  *(pch++) = ogroup[3];
  }

	*pch = 0; //terminating null
	if(bFreeBuffer) free(pchBuffer);
	*ppchBuffer = pchOut;
  *pulBufLen = ulDataLength;

  return nReturnCode;
}

int CBufferUtil::Base64Decode(char** ppchBuffer, unsigned long* pulBufLen, bool bFreeBuffer, char* pszAlphabet, char chPad)
{
	// takes in a base 64 encoded buffer.
	// decodes the buffer up to the passed in length.
	// creates a new buffer with the encoded chars that may contain chars of value 0 that do not terminate the "string".
	// however, we do add a terminating null character, not included in the buffer length, so it can be discarded.
	// possibly frees the incoming buffer.
	// resets the buffer pointer to the new buffer, and reassignes the length pointer to the new buffer length

	char* pchBuffer = *ppchBuffer;
	if(pchBuffer==NULL) return ERROR_B64_NULLPTR;

	int nReturnCode = RETURN_SUCCESS;

  // even though we don't use the pad char, we need to take it in to make sure it
	// doesn't exist in the alphabet.
	if(strlen(pszAlphabet)<64)
	{
		pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
	}
	if((chPad<=0)||(strchr(pszAlphabet, chPad)!=NULL))
	{
		if(strchr(pszAlphabet, '=')!=NULL)
		{
			pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
	}

  // pad is = char
  unsigned char igroup[4], ogroup[3];
  unsigned long i, nLen, n=0, nOut=0;
	char* index[2] = {0,0};

  nLen = *pulBufLen;

	unsigned long ulDataLength;

	if(pchBuffer[nLen-2]==chPad) // two pad char
		ulDataLength = (nLen*3/4)-2 + 1;   //+1 for terminating zero
	else
	if(pchBuffer[nLen-1]==chPad) // one pad char
		ulDataLength = (nLen*3/4)-1 + 1;   //+1 for terminating zero
	else		// no pad char
		ulDataLength = nLen*3/4 + 1;   //+1 for terminating zero

	char* pch = (char*)malloc(ulDataLength);
	char* pchOut = pch;

	if(pchOut==NULL)  return ERROR_B64_NULLMALLOC;

  for (i=0; i<nLen; i+=4)
  {
    // take 4 bytes in
    if (i<nLen)     { igroup[0] = pchBuffer[i];   n=1; } else igroup[0]=0;
    if ((i+1)<nLen) { igroup[1] = pchBuffer[i+1]; n=2; } else igroup[1]=0;
    if ((i+2)<nLen) { igroup[2] = pchBuffer[i+2]; n=3; } else igroup[2]=0;
    if ((i+3)<nLen) { igroup[3] = pchBuffer[i+3]; n=4; } else igroup[3]=0;

		nOut=0;
		if(n>1)
		{
			index[0] = strchr(pszAlphabet, igroup[0]);
			if(index[0]!=NULL)
			{
				index[1] = strchr(pszAlphabet, igroup[1]);
				if(index[1]!=NULL)
				{
					ogroup[0] = ((((index[0]-pszAlphabet)<<2) & 0xfc)|(((index[1]-pszAlphabet)>>4) & 0x03));
					nOut++;
				}
			}
		}
		if(n>2)
		{
			index[0] = strchr(pszAlphabet, igroup[1]);
			if(index[0]!=NULL)
			{
				index[1] = strchr(pszAlphabet, igroup[2]); // if this is a pad char, it won't be found
				if(index[1]!=NULL)
				{
					ogroup[1] = ((((index[0]-pszAlphabet)<<4) & 0xf0)|(((index[1]-pszAlphabet)>>2) & 0x0f));
					nOut++;
				}
			}
		}
		if(n>3)
		{
			index[0] = strchr(pszAlphabet, igroup[2]);// if this is a pad char, it won't be found
			if(index[0]!=NULL)
			{
				index[1] = strchr(pszAlphabet, igroup[3]);// if this is a pad char, it won't be found
				if(index[1]!=NULL)
				{
					ogroup[2] = ((((index[0]-pszAlphabet)<<6) & 0xc0)|((index[1]-pszAlphabet) & 0x3f));
					nOut++;
				}
			}
		}

	  if(nOut>0) *(pch++) = ogroup[0];
	  if(nOut>1) *(pch++) = ogroup[1];
	  if(nOut>2) *(pch++) = ogroup[2];
  }

	*(pch)=0;  // teriminating zero
	if(bFreeBuffer) free(pchBuffer);

	*ppchBuffer = pchOut;
	*pulBufLen = ulDataLength-1;  // dont include term zero in count
  return nReturnCode;
}

int CBufferUtil::Base64EncodeFromFile(char** ppchBuffer, unsigned long* pulBufLen, bool bFreeBuffer, char* pszAlphabet, char chPad)
{
	// takes in a filebuffer
	// encodes the buffer up to the file length
	// creates a new buffer with the encoded chars, and a terminating NULL.
	// possibly frees the buffer with the filename.
	// resets the buffer pointer to the new buffer, and reassignes the length pointer to the new buffer length

	if((ppchBuffer==NULL)||(*ppchBuffer==NULL)) return ERROR_B64_NULLPTR;
	if(!IsFileString(*ppchBuffer, true, true)) return ERROR_B64_INVALID_FILENAME;

	char* pchBuffer = *ppchBuffer;

	int nReturnCode = RETURN_SUCCESS;

	if(strlen(pszAlphabet)<64)
	{
		pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
	}
	if((chPad<=0)||(strchr(pszAlphabet, chPad)!=NULL))
	{
		if(strchr(pszAlphabet, '=')!=NULL)
		{
			pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
	}

	FILE* fp;
	char* pch=NULL;
	unsigned long ulTotalDataLength;

	fp = fopen(pchBuffer,"rb");
  if (fp != NULL)
  {
		ulTotalDataLength = _filelength( _fileno( fp));
		pch = (char*)malloc(ulTotalDataLength);
		fread(pch, sizeof(char), ulTotalDataLength, fp);
		fclose(fp);
	  nReturnCode = Base64Encode(&pch, &ulTotalDataLength, true, pszAlphabet, chPad); // frees the input buffer, returns output buffer in pch
  }
	else nReturnCode = ERROR_B64_FILEERROR;

	if(bFreeBuffer) free(*ppchBuffer);
	*ppchBuffer = pch;
	*pulBufLen = ulTotalDataLength;

  return nReturnCode;
}

int CBufferUtil::Base64DecodeToFile(char* pszFilename, char* pchBuffer, unsigned long* pulBufLen, bool bFreeBuffer, char* pszAlphabet, char chPad)
{
	// takes in a base 64 encoded buffer.
	// decodes the buffer up to the passed in length.
	// creates a binary file with the encoded chars.
	// frees the encoded buffer.
	// possibly frees the decoded buffer.

	if(!IsFileString(pszFilename, true, true)) return ERROR_B64_INVALID_FILENAME;

	int nReturnCode = RETURN_SUCCESS;

	if(strlen(pszAlphabet)<64)
	{
		pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
	}
	if((chPad<=0)||(strchr(pszAlphabet, chPad)!=NULL))
	{
		if(strchr(pszAlphabet, '=')!=NULL)
		{
			pszAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
	}

	nReturnCode = Base64Decode(&pchBuffer, pulBufLen, true, pszAlphabet, chPad);
	if(nReturnCode>=RETURN_SUCCESS)
	{
		FILE* fp;
		fp = fopen(pszFilename,"wb");
		if (fp != NULL)
		{
			unsigned long ulTotalDataLength = *pulBufLen;
			unsigned long ulDataLength=0; // so far
			bool bDone=false;
			int i = 0;
			unsigned long nLen=0;
			while (!bDone)
			{
				nLen = fwrite(pchBuffer+ulDataLength, sizeof(char), ulTotalDataLength-ulDataLength, fp);
				ulDataLength+=nLen;
				if(ulTotalDataLength==ulDataLength) bDone=true;
				if(nLen=0)
				{
					i++; // errors
				}
				if(i>5) { bDone=true; nReturnCode = ERROR_B64_FILEERROR; } // retry a few times.
			}
			fclose(fp);
		}
		else nReturnCode = ERROR_B64_FILEERROR;

		if(bFreeBuffer) free(pchBuffer); //frees output buffer
	}
  return nReturnCode;
}

char* CBufferUtil::Base64Boundary(char* pszAlphabet, char chPad)
{
	unsigned long ulTime = time(NULL);
	unsigned long ulBufLen=8;
	char* pbuffer = (char*)malloc(9); // 8 chars+plus terminating 0
	char* pReturn = pbuffer; 
	if(pReturn==NULL)  return NULL;

	sprintf(pbuffer, "%08x", time);

	ulTime = Base64Encode(&pbuffer, &ulBufLen, false, pszAlphabet, chPad);
	if (ulTime >= RETURN_SUCCESS)
	{
		free(pReturn);
		pReturn = pbuffer;
	}

	// buffer pointers are equal, lets allocate a new one for the final return.
	pReturn = (char*)malloc(strlen("-----part__-----")+ulBufLen+2); //+2 is +1 for the pad char, +1 for terminating zero
	if(pReturn==NULL)
	{
		free(pbuffer);
		return NULL;
	}

// RFC 2045
//		(A good strategy is to choose a boundary that includes a character sequence such as "=_" 
//		which can never appear in a quoted-printable body.  See the definition of multipart messages in RFC 2046.)
	sprintf(pReturn, "-----part%c_%s_-----", chPad, pbuffer);
	free(pbuffer);
	return pReturn;
}

// this function takes in an integer number and returns an 8 digit string with characters representing a number in base 64
// 281474976710655 is the largest number representable in this function.  leading zeros are included.
// negative values are represented as -00000000, error returns NULL.
char* CBufferUtil::ReturnBase64Number(__int64 i64Value, char* pszAlphabet)
{
	if((pszAlphabet)&&(strlen(pszAlphabet)>63)&&(i64Value < 281474976710655)&&(i64Value > -281474976710655))
	{
		bool bNeg = false;
		if(i64Value<0)
		{
			i64Value = -i64Value;
			bNeg = true;
		}

		__int64 nDigit[8] = { 4398046511104, 68719476736, 1073741824, 16777216, 262144, 4096, 64, 0 };
		__int64 nOutDigit[8] = { 0,0,0,0,0,0,0,0 };

		int i=0;
		while(i<8)
		{
			if(i64Value >= nDigit[i])
			{
				if(i<7)  // because nDigit[i] =0, do not divide by.
				{
					nOutDigit[i] = i64Value/nDigit[i];
					i64Value %= nDigit[i];
				}
				else
				{
					nOutDigit[i] = i64Value;
				}
			}

			i++;
		}
		char* pch = (char*)malloc(10);
		if(pch)
		{
			char* pchPut=pch;
			if(bNeg) 
			{
				*pchPut = '-';
				pchPut++;
			}

			i=0;
			while(i<8)
			{
				*pchPut = pszAlphabet[nOutDigit[i]];
				pchPut++;
				i++;
			}
			*pchPut = 0;//zero terminate
			return pch;
		}
	}

	return NULL;
}

// checks to see that all chars are not any of /\:*?<">| (MS requirement)
// a blank string (zero length) returns false
bool CBufferUtil::IsFileString(char* pszText, bool bAllowDirPaths, bool bAllowDrive) 
{
  int i=0, len = strlen(pszText);
	if (len<=0) return false;
  char ch;
	bool bFile=true;
  while ((i<len)&&(bFile))
  {
    ch = pszText[i];
		if(
				 ((ch=='/')&&(!bAllowDirPaths))
			 ||((ch=='\\')&&(!bAllowDirPaths))
			 ||((ch==':')&&(!bAllowDrive))
			 ||(ch=='*')
			 ||(ch=='?')
			 ||(ch=='"')
			 ||(ch=='<')
			 ||(ch=='>')
			 ||(ch=='|')
			)
		{
			bFile=false;
			break;
		}
		i++;
  }
  return bFile;
}

// checks to see that all chars are in the range 0-9, a-f, A-F.
// a blank string (zero length) returns false
bool CBufferUtil::IsHexString(char* pszText) 
{
  int i=0, len = strlen(pszText);
	if (len<=0) return false;
  char ch;
	bool bHex=true;
  while ((i<len)&&(bHex))
  {
    ch = pszText[i];
		if(!(
				((ch>47)&&(ch<58))
			||((ch>64)&&(ch<71))
			||((ch>96)&&(ch<103))
			))
			bHex=false;
		i++;
  }
  return bHex;
}

// checks to see that all chars are in the range 0-9, *,#, A-D.
// a blank string (zero length) returns false
bool CBufferUtil::IsDTMFString(char* pszText) 
{
  int i=0, len = strlen(pszText);
	if (len<=0) return false;
  char ch;
	bool bHex=true;
  while ((i<len)&&(bHex))
  {
    ch = pszText[i];
		if(!(
				((ch>47)&&(ch<58))
			||((ch>64)&&(ch<69))
			||(ch==42)
			||(ch==35)
			))
			bHex=false;
		i++;
  }
  return bHex;
}

// checks to see that all chars are in the range 0-9, '-' and '.' for negative and decimal #s.
// a blank string (zero length) returns false
unsigned char CBufferUtil::IsDecimalString(char* pszText) 
{
  int i=0, len = strlen(pszText);
	if (len<=0) return 0x00;
  char ch;
	bool bDecimal=true;
	char nNumNeg=0;
	char nNumPoint=0;
  while ((i<len)&&(bDecimal))
  {
    ch = pszText[i];
		if(!(
				((ch>47)&&(ch<58))
			||(ch=='-')
			||(ch=='.')
			))
		{
			bDecimal=false;
		}
		else
		{
			if(ch=='-') 
			{
				if(i!=0) return 0x00; // if it's not in the first position, it;s not a valid minus sign
				else
				{
					if(len==1) return 0x00; // if it is only a minus sign with no numbers after it, it's not a number.
				}
				nNumNeg++; if (nNumNeg>1) return 0x00; // multiple negative signs
 			}
			if(ch=='.') 
			{
				nNumPoint++;if (nNumPoint>1) return 0x00; // multiple decimal points
			}
		}
		i++;
  }

	//return values:
  if (bDecimal) // if decimal number, returns flags for negative and non-integer, like "98.73")
	{
		unsigned char nReturn = 0x01;// decimal
		if (nNumNeg>0) nReturn|=0x02; // negative
		if (nNumPoint>0) nReturn|=0x04; // non-integer
		return nReturn;
		// so a negative integer would return 3, a positive integer returns 1, negative non-integer returns 7, etc.
		// return value may be BOOL cast
	}
  return 0x00;
}


bool CBufferUtil::BreakFileString(char* pszInputString, char** ppszPath, char** ppszFile, char** ppszExt)
{
	if(pszInputString==NULL) return false;
	if(!IsFileString(pszInputString, true, true)) return false;
	char* pszText = (char*)malloc(strlen(pszInputString)+1);
	if(pszText==NULL) return false;
	strcpy(pszText, pszInputString);
	char* pchExtSep=NULL;
	char* pchFileSep=NULL;
	char* pchFileSep2=NULL;
	pchExtSep   = strrchr(pszText, '.');
	pchFileSep  = strrchr(pszText, '\\');
	pchFileSep2 = strrchr(pszText, '/');

	unsigned long nLen=0;
	if((pchExtSep>pszText)&&(max(pchFileSep,pchFileSep2)<pchExtSep)) // have to avoid c:\dir.ext\FilenameNoExt
	{ // there is an extention on the file
		nLen = strlen(pszText)-(pchExtSep-pszText);
		if(ppszExt)
		{
			*ppszExt = (char*)malloc(nLen);
			if(*ppszExt)
			{
				memcpy(*ppszExt, pchExtSep+1, nLen-1);
				(*ppszExt)[nLen-1] = 0; //null term
			}
		}
		*pchExtSep = 0; //sever the string at the dot.
	}

	if(max(pchFileSep,pchFileSep2)>0) 
	{ // there is a path
		nLen = (max(pchFileSep,pchFileSep2))-pszText+1;
		if(ppszPath)
		{
			*ppszPath = (char*)malloc(nLen+1); //include the trailing sep character
			if(*ppszPath)
			{
				memcpy(*ppszPath, pszText, nLen);
				(*ppszPath)[nLen] = 0; //null term
			}
		}
		nLen = strlen(pszText)-(max(pchFileSep,pchFileSep2)-pszText);
		if(ppszFile)
		{
			*ppszFile = (char*)malloc(nLen);
			if(*ppszFile)
			{
				memcpy(*ppszFile, max(pchFileSep,pchFileSep2)+1, nLen-1);
				(*ppszFile)[nLen-1] = 0; //null term
			}
		}
		free(pszText);
	}
	else
	{ // there is no path
		if(ppszFile) *ppszFile = pszText; //ext was stripped already
		if(ppszPath) *ppszPath = NULL;
	}
	return true;
}


// checks to see that all chars are not any of /\:*?<">| (MS requirement)
char* CBufferUtil::MakeFileString(char* pszText, char chReplaceChar, bool bAllowDirPaths, bool bAllowDrive, bool bUseSameBuffer)
{
	if(pszText==NULL) return NULL;
  char ch = chReplaceChar; // can't use illegal chars!
	char prch;
	if(
			 ((ch=='/')&&(!bAllowDirPaths))
		 ||((ch=='\\')&&(!bAllowDirPaths))
		 ||(ch==':')
		 ||(ch=='*')
		 ||(ch=='?')
		 ||(ch=='"')
		 ||(ch=='<')
		 ||(ch=='>')
		 ||(ch=='|')
		)
		chReplaceChar=0; // strip offending chars.

  unsigned long i=0, j=0, len = strlen(pszText);
	if (len<=0)	return NULL;

	char* pchOut = (char*)malloc(len+2);//for term null and possible path delim after drive spec
 	if (pchOut==NULL)	return NULL;

	while (i<len)
  {
    ch = pszText[i];
		if(
				 ((ch=='/')&&(!bAllowDirPaths))
			 ||((ch=='\\')&&(!bAllowDirPaths))
//			 ||((ch==':')&&(!((bAllowDrive)&&(i==1)&&((pszText[2]=='\\')||(pszText[2]=='/')||(pszText[2]==0)))))
			 ||((ch==':')&&(((i==1)&&(!bAllowDrive))||(i>1)))
			 ||(ch=='*')
			 ||(ch=='?')
			 ||(ch=='"')
			 ||(ch=='<')
			 ||(ch=='>')
			 ||(ch=='|')
			)
		{
			if((!bAllowDirPaths)||((bAllowDirPaths)&&(i==0)))
			{
				if(chReplaceChar!=0) pchOut[j++]=chReplaceChar;
			}
			else
			if((bAllowDirPaths)&&(i>0))
			{
				// prevents repeat of path delimiter
			  prch = pszText[i-1];
				if(!(
					  ((prch=='/')||(prch =='\\'))
					&&((chReplaceChar=='/')||(chReplaceChar =='\\'))
					))
					if(chReplaceChar!=0) pchOut[j++]=chReplaceChar;
			}
		}
		else
		if((i==1)&&(bAllowDrive)&&(ch==':')&&(pszText[2]!='\\')&&(pszText[2]!='/')&&(pszText[2]!=0))  // not a delim, and not the end, let's insert a delim
		{
			pchOut[j++]=ch;
			if(bAllowDirPaths) pchOut[j++]='\\';
		}
		else
		{
			if((!bAllowDirPaths)||((bAllowDirPaths)&&(i==0)))
			{
				pchOut[j++]=ch;
			}
			else
			if((bAllowDirPaths)&&(i>0))
			{
				// prevents repeat of path delimiter
			  prch = pszText[i-1];
				if(!(
					  ((prch=='/')||(prch =='\\'))
					&&((ch=='/')||(ch =='\\'))
					))
					pchOut[j++]=ch;
			}
		}

		i++;
  }

	pchOut[j]=0;//null term
	if(bUseSameBuffer) 
	{
		memcpy(pszText, pchOut, strlen(pchOut)+1 ); //include null term
		free(pchOut); // get rid of the temp buffer.
		pchOut = pszText;
	}
	return pchOut;
}

unsigned long CBufferUtil::CountChar(char* pchBuffer, unsigned long ulBufLen, char chFind)
{
	unsigned long nReturn=0; 
	char ch;
	for (unsigned long i=0; i<ulBufLen; i++)
	{
		ch=pchBuffer[i];
    if (ch == chFind)
    {
      nReturn++;
    }
	}
	return nReturn;
}

//returns a new buffer with chars stripped, doesnt free existing buffer.  does reset buffer length passed in to size of output buffer
char* CBufferUtil::StripChar(char* pchBuffer, unsigned long* pulBufLen, char chStripChar)
{
	char* pch = (char*)malloc(*pulBufLen); 
	char ch;
	unsigned long j=0;
	for (unsigned long i=0; i<*pulBufLen; i++)
	{
		ch=pchBuffer[i];
    if (ch != chStripChar)
    {
      pch[j++] = ch;
    }
	}
	*pulBufLen =j;
	return pch;
}

//actually replaces in the buffer
void CBufferUtil::ReplaceChar(char* pchBuffer, unsigned long ulBufLen, char chRemoveChar, char chReplaceChar)
{
	for (unsigned long i=0; i<ulBufLen; i++)
	{
    if (pchBuffer[i] == chRemoveChar) pchBuffer[i] = chReplaceChar;
	}
}


char*	CBufferUtil::DecodeReadableHex(char* pchBuffer, unsigned long* pulBufLen, unsigned long ulMode)
{
	// just use this one for a full readable hex string like [00][00][00] with no printable chars etc to confuse things.
	char* pch = (char*)malloc((*pulBufLen)+1); // +1 for zero term.
	if(pch==NULL) return NULL;

	unsigned long x=0;
	unsigned char ch=0;
	unsigned char digit=0;

	for (unsigned long f=0; f<*pulBufLen; f++)
	{
		if(*(pchBuffer+f)=='[')
		{
			ch=0;
			digit=0;
		}
		else
		if(*(pchBuffer+f)==']')
		{
			pch[x++]=ch;
		}
		else
		{
			// in a numba
			ch = (unsigned char)xtol((pchBuffer+f), 2);
			f++; // 2 bytes, so incrememnt it
		}
	}
	pch[x]=0; //zero terminate for safety.
	*pulBufLen = x;
	return pch;
}

char* CBufferUtil::ReadableHex(char* pchBuffer, unsigned long* pulBufLen, unsigned long ulMode)
{
	char* pch = (char*)malloc((*pulBufLen)*6+1); // +1 for zero term. 6 = possible brackets and crlf for every char
	if(pch==NULL) return NULL;
	int nDelimLimit = 4;
	if((ulMode&MODE_DELIMMASK) == MODE_DELIM64BIT) nDelimLimit = 8;
	else if((ulMode&MODE_DELIMMASK) == MODE_DELIM128BIT) nDelimLimit = 16;
	

	unsigned long x=0;
	int nRunLength = 0;
	bool bInPrintable = false;
	for (unsigned long f=0; f<*pulBufLen; f++)
	{
		if(
			  ((*(pchBuffer+f)>31)&&(*(pchBuffer+f)<127)&&(ulMode&MODE_ALLOWPRINTABLES))
			||((*(pchBuffer+f)==10)&&(ulMode&MODE_ALLOWLINEFEED))
			)
		{
			if((*(pchBuffer+f)==10)&&(ulMode&MODE_ALLOWLINEFEED)&&(ulMode&MODE_MAKECRLF))
			{
				pch[x++]=13;
				pch[x]=10;
			}
			else
			{
				if((ulMode&MODE_DELIMPRINTABLES)&&(x>0)&&(!bInPrintable))
				{
					pch[x++]=13;
					pch[x++]=10;
				}
				pch[x]=*(pchBuffer+f);
			}
			bInPrintable = true;
		}
		else
		{
			if(bInPrintable)
			{
				if(ulMode&MODE_DELIMPRINTABLES)
				{
					pch[x++]=13;
					pch[x++]=10;
				}
			}

			bInPrintable = false;
			char hex[5];
			sprintf(hex, "[%02x]", (*(pchBuffer+f))&0xff);
			pch[x++] = hex[0];
			pch[x++] = hex[1];
			pch[x++] = hex[2];
			pch[x] = hex[3];

			nRunLength++;
			if(ulMode&MODE_DELIMMASK) // anything, setting is already stored in nDelimLimit
			{
				if(nRunLength>=nDelimLimit)
				{
					x++;
					pch[x++]=13;
					pch[x]=10;

					nRunLength = 0;
				}
			}

		}
		x++;
	}
	pch[x]=0;
	*pulBufLen = x;
	return pch;
}

char* CBufferUtil::XMLEncode(char* pszText)
{
	if(pszText==NULL) return NULL;
	unsigned long ulBufferLength=strlen(pszText)+1;// +1 for zero term.
	//now we have to count the encoded characters;
	unsigned long ulChars = CountChar(pszText, strlen(pszText), '>');
	ulBufferLength += ulChars*3; // (gets "&gt;")
	ulChars = CountChar(pszText, strlen(pszText), '<');
	ulBufferLength += ulChars*3; // (gets "&lt;")
	ulChars = CountChar(pszText, strlen(pszText), '"');
	ulBufferLength += ulChars*5; // (gets "&quot;")
	ulChars = CountChar(pszText, strlen(pszText), '&');
	ulBufferLength += ulChars*4; // (gets "&amp;")
	ulChars = CountChar(pszText, strlen(pszText), '\'');
	ulBufferLength += ulChars*5; // (gets "&apos;")

	char* pch = (char*)malloc(ulBufferLength); 
	if(pch==NULL) return NULL;

	int x=0;
	for (unsigned long i=0; i<strlen(pszText); i++)
	{
		switch(pszText[i])
		{
    default: pch[x++] = pszText[i]; break;
		case '>': memcpy(&(pch[x]), "&gt;", 4);		x+=4; break;
		case '<': memcpy(&(pch[x]), "&lt;", 4);		x+=4; break;
		case '"': memcpy(&(pch[x]), "&quot;", 6);	x+=6; break;
		case '&': memcpy(&(pch[x]), "&amp;", 5);	x+=5; break;
		case '\'': memcpy(&(pch[x]), "&apos;", 6);	x+=6; break;
		}
	}
	pch[x] = 0;
	return pch;
}


// escapes a target set of chars (in a zero terminated string) with a special char
char* CBufferUtil::EscapeCharsWithChar(char* pszText, char* pszTgt, char chEsc)
{
	char* pch = NULL;
	if((pszText)&&(pszTgt))
	{
		unsigned long ulLen=strlen(pszText);
		unsigned long ulChars = 0;
		
		int q=0;
		while(q<(int)strlen(pszTgt))
		{
			ulChars+=CountChar(pszText, ulLen, pszTgt[q]);
			q++;
		}
		if(ulChars>0)
		{
			pch = (char*)malloc(ulLen+ulChars+1);
			int n=0;
			if(pch)
			{
				for (unsigned long i=0; i<ulLen; i++)
				{
					q=0;
					while(q<(int)strlen(pszTgt))
					{
						if(*(pszText+i) == pszTgt[q])
						{
							*(pch+n) = chEsc;
							n++;
							break; // no need to continue
						}
						q++;
					}
					*(pch+n) = *(pszText+i); n++;
				}
				*(pch+n)=0; //null term
			}
		}
		else
		{
			pch = (char*)malloc(ulLen+1);
			if(pch)
			{
				strcpy(pch, pszText);
			}
		}
	}
	return pch;
}


char* CBufferUtil::EscapeChar(char* pszText, char ch)
{
	char* pch = NULL;
	if(pszText)
	{
		unsigned long ulLen=strlen(pszText);
		unsigned long ulChars = CountChar(pszText, ulLen, ch);
		if(ulChars>0)
		{
			pch = (char*)malloc(ulLen+ulChars+1);
			int n=0;
			if(pch)
			{
				for (unsigned long i=0; i<ulLen; i++)
				{
					*(pch+n) = *(pszText+i); n++;
					if(*(pszText+i) == ch)
					{
						*(pch+n) = *(pszText+i); n++;
					}
				}
				*(pch+n)=0; //null term
			}
		}
		else
		{
			pch = (char*)malloc(ulLen+1);
			if(pch)
			{
				strcpy(pch, pszText);
			}
		}
	}
	return pch;
}

char* CBufferUtil::UnEscapeChar(char* pszText, char ch)
{
	char* pch = NULL;
	if(pszText)
	{
		unsigned long ulLen=strlen(pszText);
		unsigned long ulChars = CountChar(pszText, ulLen, ch);
		if(ulChars>0)
		{
			pch = (char*)malloc(ulLen+1);
			int n=0;
			if(pch)
			{
				for (unsigned long i=0; i<ulLen; i++)
				{
					if(*(pszText+i) == ch)
					{
						if(*(pszText+i+1) == ch)
						{
							*(pch+n) = *(pszText+i); n++;
						}
					}
					else
					{
						*(pch+n) = *(pszText+i); n++;
					}
				}
				*(pch+n)=0; //null term
			}
		}
		else
		{
			pch = (char*)malloc(ulLen+1);
			if(pch)
			{
				strcpy(pch, pszText);
			}
		}
	}
	return pch;
}



// this function reverse searches a buffer for a pattern contained in a different buffer.  
// it returns a pointer to the position at which the pattern is found, NULL if not found or if params incorrect
unsigned char* CBufferUtil::memrncmp(unsigned char* pucBuffer, unsigned long ulBufLen, unsigned char* pucCmpBuffer, unsigned long ulCmpBufLen)
{
	if((pucBuffer==NULL)||(pucCmpBuffer==NULL)||(ulBufLen<=0)||(ulCmpBufLen<=0)||(ulBufLen<ulCmpBufLen)) return NULL;

	int nIndex = ulBufLen-ulCmpBufLen;  // has to be int, unsigned long does not have negative values for valid index check
	while(nIndex>=0)
	{
		if(memcmp(pucBuffer+nIndex, pucCmpBuffer, ulCmpBufLen)==0) return (pucBuffer+nIndex);
		nIndex--;
	}
	return NULL;
}


// takes a character buffer with a hex number, and returns an unsigned integer.
unsigned long CBufferUtil::xtol(char* pucBuffer, unsigned long ulBufLen)
{
	if(pucBuffer==NULL) return 0;
	if(ulBufLen>8) ulBufLen=8; // can only return max 32bit number.

	unsigned long ulReturn=0, i=0;
	bool bDone=false;
	while((i<ulBufLen)&&(!bDone))
	{
	  char ch;
    ch = *(pucBuffer+i);
		if(!(
				((ch>47)&&(ch<58))
			||((ch>64)&&(ch<71))
			||((ch>96)&&(ch<103))
			))
		{
			bDone=true;
		}
		else
		{
			ulReturn<<=4;
			if((ch>47)&&(ch<58))
			{
				ch-=48;
			}
			else
			if((ch>64)&&(ch<71))
			{
				ch-=55;
			}
			else
			if((ch>96)&&(ch<103))
			{
				ch-=87;
			}
			ulReturn|=(ch&0x0f);
		}
		i++;
	}
	return ulReturn;
}

// following must be zero terminated.
int CBufferUtil::ParseCSV(char* data, char**** ppppchBuffer, unsigned long* pulRows, unsigned long* pulColumns)
{
	if((data)&&(ppppchBuffer)&&(pulRows)&&(pulColumns))
	{
		unsigned long ulRows=0;
		unsigned long ulColumns=0;
		unsigned long ulLen=strlen(data);
		bool bInField = false;
		char* pch = NULL;
		char* pchEnd = NULL;
		char* pchField = NULL;
		int nBufLen = 0;
		char*** pppbuf=NULL;  // array of records
		char**  ppbuf=NULL;   // record, array of char*
		int nCols=0;
		unsigned long i=0;
		unsigned long j=0;

		CSafeBufferUtil sbu;
		pch = sbu.Token(data, ulLen, ",", MODE_SINGLEDELIM);
		pchEnd = pch+ulLen;
		while(pch)
		{
//AfxMessageBox(pch);
			int nLen = strlen(pch);
			int nIndex = 0;
			char* pchf=(char*)malloc(nBufLen+nLen+2);
fieldtypselect:
//AfxMessageBox("field type select");
			if(pchf)
			{
				if((*pch == '"')||(bInField)) // we have a quoted field
				{
					if(bInField) // delim'd on a quoted comma
					{
//CString foo; foo.Format("continuing quoted [%s] %d bytes so far.. adding [%s]", pchField, nBufLen, pch); AfxMessageBox(foo);
//AfxMessageBox("continuing quoted");
						if((pchField)&&(nBufLen))
						{
							memcpy(pchf, pchField, nBufLen);
						}
						memset(pchf+nBufLen, ',', 1);
						nIndex = nBufLen+1;
//AfxMessageBox(pchf);
					}
					else
					{
//AfxMessageBox("starting quoted");
						bInField = true;
						pch++;
					}
					while(*pch != 0)
					{
						switch(*pch)
						{
						case '"':
							{
								switch(	*(pch+1) )
								{
								case '"':
									{
										// double quote, add one.
										*(pchf+nIndex) = *pch;
										pch+=2;
										nIndex++;
									} break;
								case 13:
									{
										if(*(pch+2) == 10)
										{
											pch++;  // then flow thru
										}
										else
										{
											*(pchf+nIndex) = 0;
											bInField = false;
											// terminate buffer, then fast fwd to end 
											while(*pch != 0) pch++;
											break;
										}
									}
								case 10:
									{
										bInField = false;
										// add field and make row;  then make new buffer
//////////////////////////////////////////////////////////
// add field.
//AfxMessageBox("quoted case 10");
//AfxMessageBox(pchf?pchf:"null");
										*(pchf+nIndex) = 0; nIndex = 0;

										if(nCols>=(int)ulColumns)
										{
											char**  pptmp = new char*[nCols+1];   // record, array of char*
											if(pptmp)
											{
												i=0;
												if(ppbuf)
												{
													while(i<ulColumns)
													{
														pptmp[i]=ppbuf[i];
														i++;
													}
													delete [] ppbuf;
												}
												if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
												pptmp[i] = pchf;
												ppbuf = pptmp;
												nCols++;

												//now have to go back and fix the previous rows.
												if(pppbuf)
												{
													j=0;
													while(j<ulRows)
													{
														pptmp = new char*[nCols];   // record, array of char*
														if(pptmp)
														{
															i=0;
															char**  ppSource = pppbuf[j];
															if(ppSource)
															{
																while(i<ulColumns)
																{
																	pptmp[i]=ppSource[i];
																	i++;
																}
																delete [] ppSource;
															}
															pptmp[i] = NULL;
															pppbuf[j] = pptmp;
														}
														j++;
													}
												}

												ulColumns = nCols;
											}
										}
										else // we have enough;
										{
											if(ppbuf == NULL)
											{
												ppbuf	= new char*[ulColumns];   // record, array of char*
												if(ppbuf)
												{
													i=0;
													while(i<ulColumns)
													{
														ppbuf[i]=NULL; //init
														i++;
													}
												}
											}
											if(ppbuf)
											{
//AfxMessageBox(pchf?pchf:"null");
												if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
												ppbuf[nCols] = pchf;
												nCols++;
											}
										}
// add field.
//////////////////////////////////////////////////////////
// add row
//AfxMessageBox("adding row inside quoted");
										char*** ppptmp= new char**[ulRows+1];
										if(ppptmp)
										{
											j=0;
											if(pppbuf)
											{
												while(j<ulRows)
												{
													ppptmp[j]=pppbuf[j];
													j++;
												}
												delete [] pppbuf;
											}
										
											ppptmp[j] = ppbuf;
										}
										pppbuf = ppptmp;

										ulRows++;
										nCols=0;
										ppbuf = NULL;


/*
j=0;
while(j<ulRows)
{
	i=0;
	while(i<ulColumns)
	{
		CString foo; foo.Format("col%d, row%d: %s", i,j,(ppptmp[j][i])?(ppptmp[j][i]):"null");
		AfxMessageBox(foo);
		i++;
	}
	j++;
}
*/
// add row
//////////////////////////////////////////////////////////

										pch+=2;
										pchf=(char*)malloc(nBufLen+nLen+2-nIndex);
										goto fieldtypselect;
									} break;
								case 0:
								default:
									{
										*(pchf+nIndex) = 0;
										bInField = false;
										// terminate buffer, then fast fwd to end 
										while(*pch != 0) pch++;
									} break;
								}
							} break;
						default:
							{
								*(pchf+nIndex) = *pch;
								nIndex++;
								pch++;
							} break;
						}
					}
					if(bInField) // me must have delimited on a comma
					{
						nBufLen =	nLen-1; // remove the one for the quote
						pchField = pchf;
					}
					else
					{
						nBufLen =	0;
						pchField = NULL;

//////////////////////////////////////////////////////////
// add field.
						*(pchf+nIndex) = 0; nIndex = 0;
//AfxMessageBox("quoted end of buffer");
//AfxMessageBox(pchf?pchf:"null");

						if(nCols>=(int)ulColumns)
						{
							char**  pptmp = new char*[nCols+1];   // record, array of char*
							if(pptmp)
							{
								i=0;
								if(ppbuf)
								{
									while(i<ulColumns)
									{
										pptmp[i]=ppbuf[i];
										i++;
									}
									delete [] ppbuf;
								}
								if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
								pptmp[i] = pchf;
								ppbuf = pptmp;
								nCols++;

								//now have to go back and fix the previous rows.
								if(pppbuf)
								{
									j=0;
									while(j<ulRows)
									{
										pptmp = new char*[nCols];   // record, array of char*
										if(pptmp)
										{
											i=0;
											char**  ppSource = pppbuf[j];
											if(ppSource)
											{
												while(i<ulColumns)
												{
													pptmp[i]=ppSource[i];
													i++;
												}
												delete [] ppSource;
											}
											pptmp[i] = NULL;
											pppbuf[j] = pptmp;
										}
										j++;
									}
								}

								ulColumns = nCols;
							}
						}
						else // we have enough;
						{
							if(ppbuf == NULL)
							{
								ppbuf	= new char*[ulColumns];   // record, array of char*
								if(ppbuf)
								{
									i=0;
									while(i<ulColumns)
									{
										ppbuf[i]=NULL; //init
										i++;
									}
								}
							}
							if(ppbuf)
							{
//AfxMessageBox(pchf?pchf:"null");
								if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
								ppbuf[nCols] = pchf;
								nCols++;
							}
						}

// add field.
//////////////////////////////////////////////////////////

					}
				}
				else
				{// it's a non-quoted field, have to see if there's a LF
//AfxMessageBox("starting non-quoted");
					if(*pch != 0)
					{
						while(*pch != 0)
						{
							switch(*pch)
							{
							case 13: 
								{
									if(*(pch+1) == 10)
									{
										pch++;  // then flow thru
									}
									else
									{
										*(pchf+nIndex) = *pch;
										nIndex++;
										pch++; 
										break;
									}
								}
							case 10:
								{
//AfxMessageBox(pchf);

									bInField = false;
									// add field and make row;  then make new buffer
//////////////////////////////////////////////////////////
// add field.
									*(pchf+nIndex) = 0; nIndex = 0;

									unsigned long i=0;
									unsigned long j=0;

									if(nCols>=(int)ulColumns)
									{
										char**  pptmp = new char*[nCols+1];   // record, array of char*
										if(pptmp)
										{
											i=0;
											if(ppbuf)
											{
												while(i<ulColumns)
												{
													pptmp[i]=ppbuf[i];
													i++;
												}
												delete [] ppbuf;
											}
											if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
											pptmp[i] = pchf;
											ppbuf = pptmp;
											nCols++;

											//now have to go back and fix the previous rows.
											if(pppbuf)
											{
												j=0;
												while(j<ulRows)
												{
													pptmp = new char*[nCols];   // record, array of char*
													if(pptmp)
													{
														i=0;
														char**  ppSource = pppbuf[j];
														if(ppSource)
														{
															while(i<ulColumns)
															{
																pptmp[i]=ppSource[i];
																i++;
															}
															delete [] ppSource;
														}
														pptmp[i] = NULL;
														pppbuf[j] = pptmp;
													}
													j++;
												}
											}

											ulColumns = nCols;
										}
									}
									else // we have enough;
									{
										if(ppbuf == NULL)
										{
											ppbuf	= new char*[ulColumns];   // record, array of char*
											if(ppbuf)
											{
												i=0;
												while(i<ulColumns)
												{
													ppbuf[i]=NULL; //init
													i++;
												}
											}
										}
										if(ppbuf)
										{
//AfxMessageBox(pchf?pchf:"null");
											if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
											ppbuf[nCols] = pchf;
											nCols++;
										}
									}
// add field.
//////////////////////////////////////////////////////////
// add row
//AfxMessageBox("adding row inside non-quoted");

									char*** ppptmp = new char**[ulRows+1];
									if(ppptmp)
									{
										j=0;
										if(pppbuf)
										{
											while(j<ulRows)
											{
												ppptmp[j]=pppbuf[j];
												j++;
											}
											delete [] pppbuf;
										}
										ppptmp[j] = ppbuf;
									}
									pppbuf = ppptmp;

									ulRows++;
									nCols=0;
									ppbuf = NULL;

/*
j=0;
while(j<ulRows)
{
	i=0;
	while(i<ulColumns)
	{
		CString foo; foo.Format("col%d, row%d: %s", i,j,(ppptmp[j][i])?(ppptmp[j][i]):"null");
		AfxMessageBox(foo);
		i++;
	}
	j++;
}
*/
// add row
//////////////////////////////////////////////////////////


									pch++;
//CString foo; foo.Format("%d, %d", *pch, nLen+2-nIndex); AfxMessageBox(foo);
									pchf=(char*)malloc(nLen+2-nIndex);
									goto fieldtypselect;
								} break;
							default:
								{
									*(pchf+nIndex) = *pch;
//AfxMessageBox(pchf);
									nIndex++;
									pch++;
								} break;
							}
						}
						nBufLen =	0;
						pchField = NULL;

//////////////////////////////////////////////////////////
// add field.
						*(pchf+nIndex) = 0;  nIndex = 0;

						unsigned long i=0;
						unsigned long j=0;

						if(nCols>=(int)ulColumns)
						{
							char**  pptmp = new char*[nCols+1];   // record, array of char*
							if(pptmp)
							{
								i=0;
								if(ppbuf)
								{
									while(i<ulColumns)
									{
										pptmp[i]=ppbuf[i];
										i++;
									}
									delete [] ppbuf;
								}

								if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}

								pptmp[i] = pchf;
								ppbuf = pptmp;
								nCols++;

								//now have to go back and fix the previous rows.
								if(pppbuf)
								{
									j=0;
									while(j<ulRows)
									{
										pptmp = new char*[nCols];   // record, array of char*
										if(pptmp)
										{
											i=0;
											char**  ppSource = pppbuf[j];
											if(ppSource)
											{
												while(i<ulColumns)
												{
													pptmp[i]=ppSource[i];
													i++;
												}
												delete [] ppSource;
											}
											pptmp[i] = NULL;
											pppbuf[j] = pptmp;
										}
										j++;
									}
								}

								ulColumns = nCols;
							}
						}
						else // we have enough;
						{
							if(ppbuf == NULL)
							{
								ppbuf	= new char*[ulColumns];   // record, array of char*
								if(ppbuf)
								{
									i=0;
									while(i<ulColumns)
									{
										ppbuf[i]=NULL; //init
										i++;
									}
								}
							}
							if(ppbuf)
							{
//AfxMessageBox(pchf?pchf:"null");
								if((pchf)&&(strlen(pchf)<=0)) {free(pchf); pchf=NULL;}
								ppbuf[nCols] = pchf;
								nCols++;
							}
						}
// add field.
//////////////////////////////////////////////////////////
					}
					else // it was a zero length field, have to add it.
					{
						if(pchf) free(pchf);
						pchf = NULL;

						// but only add a field if it is not the end of all data.
//////////////////////////////////////////////////////////
// add null field.

						if(pch<pchEnd)
						{
							nIndex = 0;

							unsigned long i=0;
							unsigned long j=0;

							if(nCols>=(int)ulColumns)
							{
								char**  pptmp = new char*[nCols+1];   // record, array of char*
								if(pptmp)
								{
									i=0;
									if(ppbuf)
									{
										while(i<ulColumns)
										{
											pptmp[i]=ppbuf[i];
											i++;
										}
										delete [] ppbuf;
									}
									pptmp[i] = NULL;
									ppbuf = pptmp;
									nCols++;

									//now have to go back and fix the previous rows.
									if(pppbuf)
									{
										j=0;
										while(j<ulRows)
										{
											pptmp = new char*[nCols];   // record, array of char*
											if(pptmp)
											{
												i=0;
												char**  ppSource = pppbuf[j];
												if(ppSource)
												{
													while(i<ulColumns)
													{
														pptmp[i]=ppSource[i];
														i++;
													}
													delete [] ppSource;
												}
												pptmp[i] = NULL;
												pppbuf[j] = pptmp;
											}
											j++;
										}
									}

									ulColumns = nCols;
								}
							}
							else // we have enough;
							{
								if(ppbuf == NULL)
								{
									ppbuf	= new char*[ulColumns];   // record, array of char*
									if(ppbuf)
									{
										i=0;
										while(i<ulColumns)
										{
											ppbuf[i]=NULL; //init
											i++;
										}
									}
								}
								if(ppbuf)
								{
	//AfxMessageBox(pchf?pchf:"null");
									ppbuf[nCols] = NULL;
									nCols++;
								}
							}
						}
// add null field.
//////////////////////////////////////////////////////////

					}
				}
			}
			pch = sbu.Token(NULL, NULL, ",", MODE_SINGLEDELIM);
		}
//////////////////////////////////////////////////////////
// add row if nec
		if(ppbuf)
		{
//AfxMessageBox("adding row at end");
			char*** ppptmp= new char**[ulRows+1];
			if(ppptmp)
			{
				j=0;
				if(pppbuf)
				{
					while(j<ulRows)
					{
						ppptmp[j]=pppbuf[j];
						j++;
					}
					delete [] pppbuf;
				}
			
				ppptmp[j] = ppbuf;
			}
			pppbuf = ppptmp;

			ulRows++;
			nCols=0;
			ppbuf = NULL;

/*
j=0;
while(j<ulRows)
{
	i=0;
	while(i<ulColumns)
	{
		CString foo; foo.Format("col%d, row%d: %s", i,j,(ppptmp[j][i])?(ppptmp[j][i]):"null");
		AfxMessageBox(foo);
		i++;
	}
	j++;
}
*/
		}
// add row if nec
//////////////////////////////////////////////////////////

		*pulRows = ulRows;
		*pulColumns = ulColumns;
		*ppppchBuffer = pppbuf;
		return 0;
	}
	return -1;
}


// Format XML is not an XML parser.
// It expects well formed XML, and may have problems with strings that are not well formed XML.
// Given well-formed XML, it will simply insert carriage returns and indents appropriately before tags 
// to provide a more human-readable, hierachical view of the XML.
// It is for display purposes only.  The returned buffer is allocated with malloc and must
// be de-allocated using free();
char* CBufferUtil::FormatXML(char* pszText, char* pszIndent)
{
	char* pchR = NULL;
	if((pszText!=NULL)&&(pszIndent!=NULL))
	{
		unsigned long ulBufLen = strlen(pszText);
		int max=0;
		int count=0;
		int level = 0;
		unsigned long q=0;
		bool bEndTag=true;
		while(q<ulBufLen)
		{
			if(*(pszText+q)=='>')
			{
				if((*(pszText+q-1)=='/'))
				{
					//level--;
					count++;
				}
				else
				{
					if(!bEndTag)
					{
						level++;
						count++;
					}
				}
				bEndTag = false;

			}
			else
			if((*(pszText+q)=='<')&&(*(pszText+q+1)=='/'))
			{
				bEndTag = true;
				level--;
				count++;
			}			

			if(level>max) max=level;
			q++;
		}
		// here we have a count of how many tags, and the max level of indent
		// worst case, if every tag were on a line, and they were all at max indent level, we can estimate the largest possible buffer needed
		// this is probably not quite true but close enough.


			int nLen =ulBufLen+(count*max*(strlen(pszIndent)+2)); //+2 for crlf
/*
			FILE* fp=fopen("XML.txt", "ab");
			if(fp)
			{
				fprintf(fp,"max=%d,count=%d, buflen=%d\r\n", max, count,nLen);
				fclose(fp);
			}
*/	

		char* inbuf = pszText;

		pchR = (char*)malloc(nLen+1);
		if(pchR==NULL) return pchR;

		max = strlen(pszIndent);
		char* outbuf = pchR;
		char* outbuf0 = outbuf;
		char buffer[256]; strcpy(buffer,"");
		char buffer2[256];
		count=0;
		int indent = -1;
		int nCondition=0; //0 = init, 1 = in begin tag including <>, 2 in content (X in <tag>X</tag>), 
		// 3 in end tag including </>, 4 in between tags including any whitespace

		char** pptags=NULL;
		int nNumtags=0;

		bool content = false;
	//	bool contentreturn = false;
	//	bool tagonline = false;
		bool indented = false;
		bool suppressed = false;
	//	bool returned = false;

		int nLastCondition=0;
		int nLastTag=-1;

	//	FILE* fp;

		while(count<nLen)
		{
			if(*inbuf == 0)
			{
				break;
			}

			if(*inbuf =='<')
			{
				if(*(inbuf+1) =='/')
				{
					//end tag, can't be anything else
					nCondition = 3;
					content = false;
					//if we are in an end tag and this is good XML, we have to remove the last tag from the list
					nNumtags--;
					if((nNumtags>=0)&&(pptags)&&(pptags[nNumtags])) free(pptags[nNumtags]);
				}
				else
				{
					// in a tag
					nCondition = 1;
					content = false;

					// get the tag name.
					int q=0;
					while((*(inbuf+q+1) != ' ')&&(*(inbuf+q+1) != '\t')&&(*(inbuf+q+1) != '>')&&(*(inbuf+q+1) != '/')&&(*(inbuf+q+1) != 0))
					{
						buffer2[q] = *(inbuf+q+1);
						q++;
					}
					buffer2[q] = 0;

					char** ppnew = new char*[nNumtags+1];
					if(ppnew)
					{
						char* pch = (char*)malloc(strlen(buffer2)+1);
						if(pch)
						{
							strcpy(pch, buffer2);
						}
						if(pptags)
						{
							q=0;
							while(q<nNumtags)
							{
								ppnew[q]=pptags[q];
								q++;
							}
							delete [] pptags;
						}
						ppnew[nNumtags] = pch;
						nNumtags++;
						pptags = ppnew;

					}
					else
					{
						if(pptags)
						{
							if(nNumtags>0)
							{
								int q=0; 
								while(q<nNumtags)
								{
									free (pptags[q]);
									q++;
								}
							}
							delete [] pptags;
						}

						if(pchR) free(pchR);
						return NULL;
						
					}

				}

			}
			else if(*inbuf =='>')
			{
				if(nCondition == 1)
				{
					if(*(inbuf-1)=='/')
					{
						nNumtags--;
						if((nNumtags>=0)&&(pptags)&&(pptags[nNumtags])) free(pptags[nNumtags]);
						nCondition = 5;
						content = false;
						if(*(inbuf+1)!='<') { content = true;}
					}
					else
					{
						if(*(inbuf+1)!='<') { content = true;}
						else{ content = false; nCondition=0;}
					}
				}
				else
				if(nCondition == 3)
				{
					if(*(inbuf+1)!='<') { content = true;}
					else{ content = false; nCondition=4;}
				}
			}
			else
			{
				if(content)
				{
					if((nCondition==5)||(nCondition==3)) nCondition=6;
					else if(nCondition == 1)	nCondition=2; 
				}
			}

			if(nLastCondition!=nCondition)
			{
				// something has changed so do something with indents or newlines
				switch(nCondition)
				{
				case 0:  // init, or after a begin tag has just ended.
					{
						// put the > there
						*outbuf = *inbuf;
						outbuf++;

						// then decide...

						// only do this if the next thing is not an end tag for the same begin tag
						sprintf(buffer, "</>");
						if((pptags)&&(nNumtags>0)&&(pptags[nNumtags-1]))
						{
							sprintf(buffer, "</%s>", pptags[nNumtags-1]);
						}
	/*
			fp=fopen("XML.txt", "ab");
			if(fp)
			{
				*outbuf = 0;
				fprintf(fp,"checking %s vs %s\r\n", buffer, inbuf+1);
				fclose(fp);
			}
	*/
						if(strncmp(inbuf+1, buffer, strlen(buffer)))
						{
	/*
			fp=fopen("XML.txt", "ab");
			if(fp)
			{
				*outbuf = 0;
				fprintf(fp,"tags diff return\r\n");
				fclose(fp);
			}
	*/
							*outbuf = 13;
							outbuf++;
							*outbuf = 10;
							outbuf++;
	//						tagonline = false;
							indented = false;
							suppressed = false;
	//						returned = true;
						}
						else suppressed = true;

					} break;
				case 1:  // in a tag
					{
						nLastTag = nNumtags-1;
						indent++;

	/*


			fp=fopen("XML.txt", "ab");
			if(fp)
			{
				*outbuf = 0;
				fprintf(fp,"INDENT1+: %d\r\n", indent);
				fclose(fp);
			}
	*/
						if((nLastCondition==2)||(nLastCondition==6)||(nLastCondition==4)||(nLastCondition==5))
						{

							*outbuf = 13;
							outbuf++;
							*outbuf = 10;
							outbuf++;
	//						tagonline = false;
							indented = false;
							suppressed = false;

						}

						if(!indented)
						{
							int q=0;
/*
							while(q<indent)
							{
								*outbuf = ' ';
								outbuf++;
								*outbuf = ' ';
								outbuf++;
								q++;
							}
*/
							while(q<indent)
							{
								int d=0;
								while(d<max)
								{
									*outbuf = *(pszIndent+d);
									outbuf++;
									d++;
								}
								q++;
							}
						
							indented = true;
						}

	//					tagonline = true;

						// and the begin tag marker
						*outbuf = *inbuf;
						outbuf++;

					} break;
				case 2:  // in content
					{
						//if the content has a simple bracketed set of tags, leave it, otherwise return and indent 
						bool DoIt=true;
						int q=1;
						while(*(inbuf+q)!=0)
						{
							if(*(inbuf+q)=='<')
							{
								sprintf(buffer, "</>");
								if((pptags)&&(nNumtags>0)&&(pptags[nNumtags-1]))
								{
									sprintf(buffer, "</%s>", pptags[nNumtags-1]);
								}
	/*
					fp=fopen("XML.txt", "ab");
					if(fp)
					{
						*outbuf = 0;
						fprintf(fp,"checking %s vs %s\r\n", buffer, inbuf+1);
						fclose(fp);
					}

	*/
								if(strncmp(inbuf+q, buffer, strlen(buffer))==0)
								{
									DoIt=false;
								}


								break;
							}
							q++;
						}
						if(DoIt)
						{
							*outbuf = 13;
							outbuf++;
							*outbuf = 10;
							outbuf++;
	//						tagonline = false;
							indented = false;

							if(!indented)
							{
								int q=0;
								/*
								while(q<indent+1)
								{
									*outbuf = ' ';
									outbuf++;
									*outbuf = ' ';
									outbuf++;
									q++;
								}
								*/
								while(q<indent+1)
								{
									int d=0;
									while(d<max)
									{
										*outbuf = *(pszIndent+d);
										outbuf++;
										d++;
									}
									q++;
								}

								indented = true;
							}
						}

						*outbuf = *inbuf;
						outbuf++;
					} break;
				case 3:  // in an end tag
					{

						if((!suppressed)&&(nLastCondition!=2))
						{
							*outbuf = 13;
							outbuf++;
							*outbuf = 10;
							outbuf++;
	//						tagonline = false;
							indented = false;

							if(!indented)
							{
								int q=0;
								/*
								while(q<indent)
								{
									*outbuf = ' ';
									outbuf++;
									*outbuf = ' ';
									outbuf++;
									q++;
								}
								*/
								while(q<indent)
								{
									int d=0;
									while(d<max)
									{
										*outbuf = *(pszIndent+d);
										outbuf++;
										d++;
									}
									q++;
								}

								indented = true;
							}
						}
						suppressed = false;

						*outbuf = *inbuf;
						outbuf++;
						indent--; //back off one
	/*
			fp=fopen("XML.txt", "ab");
			if(fp)
			{
				*outbuf = 0;
				fprintf(fp,"INDENT2-: %d\r\n", indent);
				fclose(fp);
			}
	*/

					} break;
				case 4:  // in between tags
					{
						// put the > there
						*outbuf = *inbuf;
						outbuf++;

					} break;
				case 5:  // in between tags but came out of a <single/> tag
					{
						// put the > there
						*outbuf = *inbuf;
						outbuf++;

						indent--; //back off one
	/*
			fp=fopen("XML.txt", "ab");
			if(fp)
			{
				*outbuf = 0;
				fprintf(fp,"INDENT3-: %d\r\n", indent);
				fclose(fp);
			}
	*/

					} break;
				case 6:  // in betwewen tags content
					{


						if((nLastCondition==3)||(nLastCondition==5))
						{

							*outbuf = 13;
							outbuf++;
							*outbuf = 10;
							outbuf++;
	//						tagonline = false;
							indented = false;
							suppressed = false;


							if(!indented)
							{
								int q=0;
								/*
								while(q<indent+1)
								{
									*outbuf = ' ';
									outbuf++;
									*outbuf = ' ';
									outbuf++;
									q++;
								}
								*/
								while(q<indent+1)
								{
									int d=0;
									while(d<max)
									{
										*outbuf = *(pszIndent+d);
										outbuf++;
										d++;
									}
									q++;
								}

								indented = true;
							}
						}



						*outbuf = *inbuf;
						outbuf++;
					} break;
				}

			}
			else
			{
				*outbuf = *inbuf;
				outbuf++;
			}
			nLastCondition = nCondition;

	///					*outbuf = 0;
	//					AfxMessageBox(outbuf0);
	/*
			fp=fopen("XML.txt", "ab");
			if(fp)
			{
				*outbuf = 0;
				if((pptags)&&(nNumtags>0)&&(pptags[nNumtags-1]))
					fprintf(fp,"now condition %d, current tag [%s]\r\n%s\r\n\r\n", nCondition, pptags[nNumtags-1], outbuf0);
				else
					fprintf(fp,"now condition %d, no current tag\r\n%s\r\n\r\n", nCondition, outbuf0);
				fclose(fp);
			}
	*/
			inbuf++;
			count++;
		}
		*outbuf=0;

		if(pptags)
		{
			if(nNumtags>0)
			{
				int q=0; 
				while(q<nNumtags)
				{
					free (pptags[q]);
					q++;
				}
			}
			delete [] pptags;
		}


	}
	return pchR;
}

