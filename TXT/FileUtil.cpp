// FileUtil.cpp: implementation of the CFileUtil class.
//
//////////////////////////////////////////////////////////////////////

#include "FileUtil.h"
#include <ctype.h>
#include <io.h>
#include <direct.h>



#define ENCRYPT_OFFSET 81
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileUtil::CFileUtil()
{
	m_ulStatus=0;
	m_pfileSettings=NULL;
	m_pchBuffer=NULL;
	m_ulBufferLength=0;
	m_ucType = MSG_DESTTYPE_INI;
}

CFileUtil::~CFileUtil()
{
	if(m_pchBuffer) free(m_pchBuffer);
	if(m_pfileSettings!=NULL)
	{
		fflush(m_pfileSettings);
		fclose(m_pfileSettings);
	}
}

//////////////////////////////////////////////////////////////////////
// Design Philosophy
//////////////////////////////////////////////////////////////////////
// Multiple settings are typically accessed at one time.
// These settings are stored in a file, however since the order
// of particular settings is not deterministic, it makes sense to
// load the entire file into memory (if possible) and seek for
// particular settings in memory.
// Writing or over-writing settings have a simlar paradigm.  The entire
// file should be written at once, and closed.
// Settings files have structure similar to standard .ini files.
// the structure is as in this sample:
/*

[Section Name]
Entry1 Name=value //comment
Entry2 Name=value //comment

[Section2 Name]
Entry3 Name=value //comment
Entry4 Name=value //comment
//comment


[END]

*/
// Section names are ASCII only in brackets, stand-alone on one line, 
// followed by CRLF. no comments allowed on line
// Entry names can be any ASCII string not including '=', immediately 
// followed by '=' then the value.  If there is a comment, the value has 
// one space after it, and then any comments follow, all on one line.
// Comments on their own line may be placed anywhere in the file.
// Files may be encrypted, or they may be human-readable ASCII.
//////////////////////////////////////////////////////////////////////

int CFileUtil::GetSettings(char* pszFilename, bool bEncrypted) 
{
	if(m_pfileSettings) fclose (m_pfileSettings);
	m_pfileSettings = fopen(pszFilename, "rb");
	if(m_pfileSettings)
	{
		m_ulStatus |= (FILEUTIL_FILE_OPEN|FILEUTIL_FILE_EXISTS);
		// determine file size
		fseek(m_pfileSettings,0,SEEK_END);
		unsigned long ulLen = ftell(m_pfileSettings);
		fseek(m_pfileSettings,0,SEEK_SET);

		// allocate buffer and read in file contents
		char* pch = (char*) malloc(ulLen+1); // for 0
		if(pch!=NULL)
		{
			m_ulStatus |= FILEUTIL_MALLOC_OK;
			m_ulBufferLength = ulLen;
			fread(pch,sizeof(char),m_ulBufferLength,m_pfileSettings);
			memset(pch+m_ulBufferLength, 0, 1);

			if(m_pchBuffer) free(m_pchBuffer);
			m_pchBuffer = pch;

			if(bEncrypted)
			{
				Decrypt();
			}

			// enforce buffer ending.
			if(strstr( m_pchBuffer, "\r\n\r\n[END]\r\n" )==NULL) // end every file with this
			{
				pch = (char*)malloc(m_ulBufferLength+strlen("\r\n\r\n[END]\r\n")+1); //+1 for final 0
				if(pch)
				{
					memcpy(pch, m_pchBuffer, m_ulBufferLength);
					memcpy(pch+m_ulBufferLength, "\r\n\r\n[END]\r\n", strlen("\r\n\r\n[END]\r\n"));
					memset(pch+m_ulBufferLength+strlen("\r\n\r\n[END]\r\n"), 0, 1); // allows str manip.
					m_ulBufferLength+=strlen("\r\n\r\n[END]\r\n"); // dont include final zero in buflen
					free(m_pchBuffer);
					m_pchBuffer = pch;
				}
			}
		}
		else 	m_ulStatus |= FILEUTIL_MALLOC_FAILED;

		fclose(m_pfileSettings);
		m_pfileSettings=NULL;
		m_ulStatus &=~FILEUTIL_FILE_OPEN;

	}
	else //open failed.
	{ 
		//we need to check if the file is there already, 
		//if not, its new, we can just create a new buffer.

		if(_access( pszFilename, 0 )<0) // we bet that the file doesnt exist.
		{
			unsigned long ulLen = strlen("\r\n\r\n[END]\r\n");
			char* pch = (char*) malloc(ulLen+1);
			if(pch!=NULL)
			{
				m_ulStatus |= FILEUTIL_MALLOC_OK;
				memcpy(pch,"\r\n\r\n[END]\r\n", ulLen);
				memset(pch+ulLen, 0, 1);

				if(m_pchBuffer) free(m_pchBuffer);
				m_pchBuffer = pch;
				m_ulBufferLength = ulLen;
			}
			else 	m_ulStatus |= FILEUTIL_MALLOC_FAILED;
		}
		else  // some other error has occurred.
		{
			// but, we dont overwrite the last buffer if it is there...
			m_ulStatus |= FILEUTIL_FILE_ERROR;
			m_pfileSettings=NULL;
		}
	}

	return m_ulStatus;
}

int CFileUtil::SetSettings(char* pszFilename, bool bEncrypted) 
{
	if(pszFilename==NULL) return FILEUTIL_WRITE_BADBUFFER;
	if(m_pfileSettings) fclose (m_pfileSettings);

	char path_buffer[_MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];

	strcpy(path_buffer, pszFilename);

	_splitpath( pszFilename, drive, dir, fname, ext );	

	// have to make dir if there is a dir path.
	// if dir already exists, no problem, _mkdir just returns and we continue
	// so, we can always just call mkdir

	for (int i=0; i<(int)strlen(dir); i++)
	{
		if((dir[i]=='/')||(dir[i]=='\\'))
		{
			path_buffer[i] = 0;
			if(strlen(path_buffer)>0)
				_mkdir(path_buffer);
			path_buffer[i] = '\\';
		}
		else
			path_buffer[i] = dir[i];
	}

	m_pfileSettings = fopen(pszFilename, "wb");
	if(m_pfileSettings)
	{
		m_ulStatus |= FILEUTIL_FILE_OPEN;
		if(m_pchBuffer!=NULL)
		{

			if(strstr( m_pchBuffer, "\r\n\r\n[END]\r\n" )==NULL) // end every file with this
			{
				char* pch = (char*)malloc(m_ulBufferLength+strlen("\r\n\r\n[END]\r\n")+1); //+1 for final 0
				if(pch)
				{
					memcpy(pch, m_pchBuffer, m_ulBufferLength);
					memcpy(pch+m_ulBufferLength, "\r\n\r\n[END]\r\n", strlen("\r\n\r\n[END]\r\n"));
					memset(pch+m_ulBufferLength+strlen("\r\n\r\n[END]\r\n"), 0, 1); // allows str manip.
					m_ulBufferLength+=strlen("\r\n\r\n[END]\r\n"); // dont include final zero in buflen
					free(m_pchBuffer);
					m_pchBuffer = pch;
				}
			}
			
			if(bEncrypted)
			{
				Encrypt();
			}
			fwrite( m_pchBuffer, sizeof(char),m_ulBufferLength, m_pfileSettings );
			if(bEncrypted)
			{
				Decrypt(); // don't keep the buffer in an encrypted state.
			}

		}
		else 	
		{
			m_ulStatus &= ~FILEUTIL_MALLOC_OK;
			m_ulStatus |= FILEUTIL_MALLOC_FAILED;
		}

		fflush(m_pfileSettings);
		fclose(m_pfileSettings);
		m_pfileSettings=NULL;

		m_ulStatus &=~FILEUTIL_FILE_OPEN;

	}
	else //open failed.
	{ // but, we don't overwrite the last buffer if it is there...
		m_pfileSettings=NULL;
	}

	return m_ulStatus;
}

char* CFileUtil::GetIniString(char* pszSection, char* pszEntry, char* pszDefaultValue, char* pszOldValue)
{
	// if successful, allocates a buffer and puts a null term string in it
	if((pszSection)&&(pszEntry)&&(m_pchBuffer))
	{

		char* pSec = new char[strlen(pszSection)+5]; // term zero
		sprintf(pSec, "[%s]%c%c", pszSection, 13, 10);

		char* pPointer = NULL;
		pPointer = strstr( m_pchBuffer, pSec );


		if(pPointer!=NULL)
		{
			// section exists.
			pPointer+=strlen(pSec); // move to next line.

			while(pPointer<(m_pchBuffer+m_ulBufferLength))
			{

				if (
						(pPointer<(m_pchBuffer+m_ulBufferLength-strlen(pszEntry)))
					&&(strncmp(pPointer, pszEntry, strlen(pszEntry))==0)
					 )
				{ 

					// first make sure this is the first thing on a line
					int i=1;
					bool bFirstOnLine = false;
					while(isspace(*(pPointer-i)))
					{
						if(*(pPointer-i)==10){ bFirstOnLine = true; break;}
						i++;
					}

					if(bFirstOnLine) // then continue, otherwise it could be in a comment, so proceed to end of line then continue
					{
						// possible match
						pPointer+=strlen(pszEntry);
						//eat up whitespace
						while((pPointer<(m_pchBuffer+m_ulBufferLength))&&(isspace(*pPointer)))
						{
							pPointer++;
						}
						if(pPointer<(m_pchBuffer+m_ulBufferLength))
						{
							if(*pPointer=='=') // fits the spec.
							{
								pPointer++; // go to next char
								char* pchWhite = pPointer;
								//eat up whitespace, but only up to a newline
								while(
										   (pPointer<(m_pchBuffer+m_ulBufferLength))
										 &&(isspace(*pPointer))
										 &&(strncmp(pPointer, "\r\n", 2))
									 	 &&(strncmp(pPointer, "\n", 1))
										 )
								{
									pPointer++;
								}
								// pPointer is now either the beginning of the value, or the end of the line (or beginning of a comment)
								if(pPointer<(m_pchBuffer+m_ulBufferLength))
								{
									//capture the value
									char* pch = pPointer;

									// if it's the end of the line already, this while will not execute once.
									while
										(
											(pPointer<(m_pchBuffer+m_ulBufferLength-2))
										&&(
												(!((strncmp(pPointer, "//", 2)==0)&&(isspace(*(pPointer-1)))))
											&&(strncmp(pPointer, "\r\n", 2))
											&&(strncmp(pPointer, "\n", 1))
											)
										) pPointer++;  // advance until comment or newline

									int nBuflen = 1;

									if(pPointer>pch)  // not blank value
									{
										if(strncmp(pPointer, "//allow_whitespace", 18 )==0)
										{
											pch=pchWhite;
											pPointer--; //back up to a non '/' or EOLN char
										}
										else
										{
											pPointer--; //back up to a non '/' or EOLN char
											while((pPointer>pch)&&(isspace(*pPointer))) pPointer--;
										}
										//eat up trailing whitespace
										nBuflen = pPointer-pch+2; //extra for terminating zero
									}

									pPointer = pch;

									pch = (char*)malloc(nBuflen);
									if(pch)
									{
										if(pszOldValue) free(pszOldValue);

										if(nBuflen>1) memcpy(pch, pPointer, nBuflen-1);
										memset(pch+nBuflen-1, 0, 1);

										// lets run in thru char 10 decode.
										unsigned long ulBufferLen = strlen(pch);
										char* pchDecode = DecodeTen(pch, &ulBufferLen);
										if(pchDecode)
										{
											free(pch);
											pch = pchDecode;
										}

										if(pSec) delete [] pSec;
										return pch;
									}
								}
							}
						}
					}
					else // not first on line, so just continue
					{
						while
							(
								(pPointer<(m_pchBuffer+m_ulBufferLength-2))
							&&(
									(strncmp(pPointer, "\r\n", 2))
								&&(strncmp(pPointer, "\n", 1))
								)
							) pPointer++;  // advance until comment or newline
						continue;  //to big while loop
					}
				}

				if(
						(
							(pPointer<(m_pchBuffer+m_ulBufferLength-3))
						&&(
								(strncmp(pPointer, "\r\n[", 3)==0) // new section (thats the specification).
							||(strncmp(pPointer, "\n[", 2)==0) // new section.  (thats the specification).
							)
						)
						||(pPointer>=(m_pchBuffer+m_ulBufferLength-1)) // should never happen
					) break; // end of section, so get out of big while loop;
				pPointer++;
			}
		}
		if(pSec) delete [] pSec;
	}

	if(pszDefaultValue)
	{
		// allocate a buffer and fill it with default value;
		char* pch = (char*)malloc(strlen(pszDefaultValue)+1);
		if(pch)
		{
			if(pszOldValue) free(pszOldValue);
			memcpy(pch, pszDefaultValue, strlen(pszDefaultValue));
			memset(pch+strlen(pszDefaultValue), 0, 1);
			return pch;
		}
	}
	return pszDefaultValue; // just in case
}

int CFileUtil::GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue)
{
	char* pch = GetIniString(pszSection, pszEntry, NULL);
	if(pch!=NULL)
	{
		int nReturn = atoi(pch);
		free(pch); // malloc was used
		return nReturn;
	}
	return nDefaultValue; 
}

int CFileUtil::SetIniString(char* pszSection, char* pszEntry, char* pszValue, char* pszComment)
{ 
	//only writes it to memory buffer, must call SetSettings to output to file.

	if((pszSection == NULL)||(strlen(pszSection)<=0)) return FILEUTIL_WRITE_NOSECTION;
	if((pszEntry == NULL)||(strlen(pszEntry)<=0)) return FILEUTIL_WRITE_NOENTRY;
	if(pszValue == NULL) return FILEUTIL_WRITE_NOVALUE;  // value can be "";
	if(m_pchBuffer == NULL) return FILEUTIL_WRITE_BADBUFFER;


	bool bValueAlloc = false;
	bool bCommentAlloc = false;
	if((strlen(pszValue))&&((strchr( pszValue, 10))||(strchr( pszValue, TEN_ENCODE_CTRLCHAR))) ) // there is a char 10 inside "value", must encode
	{
		char* pchEncode = NULL;
		unsigned long ulBufferLen = strlen(pszValue);
		pchEncode = EncodeTen(pszValue, &ulBufferLen);
		if(pchEncode)
		{
			pszValue = pchEncode;  //replace the pointer.
			bValueAlloc = true;
		}
	}

	if((pszComment)&&(strlen(pszComment))&&((strchr( pszComment, 10))||(strchr( pszComment, TEN_ENCODE_CTRLCHAR))) ) // there is a char 10 inside "comment", must encode
	{
		char* pchEncode = NULL;
		unsigned long ulBufferLen = strlen(pszComment);
		pchEncode = EncodeTen(pszComment, &ulBufferLen);
		if(pchEncode) 
		{
			pszComment = pchEncode;  //replace the pointer.
			bCommentAlloc = true;
		}
	}

	// first step is to determine if there is already a setting in the buffer.
	char* pSec = new char[strlen(pszSection)+5]; // one extra for final 0
	sprintf(pSec, "[%s]%c%c", pszSection, 13, 10);

	char* pPointer = NULL;
	pPointer = strstr( m_pchBuffer, pSec );


	if(pPointer!=NULL)
	{
		// section exists.
		pPointer+=strlen(pSec); // move to next line.

		while(pPointer<(m_pchBuffer+m_ulBufferLength))
		{

			if (
					(pPointer<(m_pchBuffer+m_ulBufferLength-strlen(pszEntry)))
				&&(strncmp(pPointer, pszEntry, strlen(pszEntry))==0)
				 )
			{ 

				// first make sure this is the first thing on a line
				int i=1;
				bool bFirstOnLine = false;
				while(isspace(*(pPointer-i)))
				{
					if(*(pPointer-i)==10){ bFirstOnLine = true; break;}
					i++;
				}

				if(bFirstOnLine) // then continue, otherwise it could be in a comment, so proceed to end of line then continue
				{
					// possible match
					pPointer+=strlen(pszEntry);
					//eat up whitespace
					while((pPointer<(m_pchBuffer+m_ulBufferLength))&&(isspace(*pPointer)))
					{
						pPointer++;
					}
					if(pPointer<(m_pchBuffer+m_ulBufferLength))
					{
						if(*pPointer=='=') // fits the spec.
						{
							//eat up whitespace, but only up to a newline.
							pPointer++; // go to next char
							char* pchWhite = pPointer;
							while (
											(pPointer<(m_pchBuffer+m_ulBufferLength))
										&&(isspace(*pPointer))
										&&(
												(strncmp(pPointer, "\r\n", 2))
											&&(strncmp(pPointer, "\n", 1))
											)
										)
							{
								pPointer++;
							}
							if(pPointer<(m_pchBuffer+m_ulBufferLength))
							{
								//capture the value
								char* pch = pPointer;  // end of first part of buffer to be preserved.

								if((pszComment)&&(strlen(pszComment)>0)) // we will replace the comment too.
								{
									bool bAllowWhite = false;
									if(strncmp(pszComment, "allow_whitespace", 16 )==0)
									{
										pch = pchWhite;
										bAllowWhite = true;
									}
									while
										(
											(pPointer<(m_pchBuffer+m_ulBufferLength-2))
										&&(
												(strncmp(pPointer, "\r\n", 2))
											&&(strncmp(pPointer, "\n", 1))
											)
										)
									{
										pPointer++;  // advance until newline
									}
									 //pPointer is end of buffer to be replaced.
									bool bEnsureSpace = false;
									// if we allow whitespace but there isn't any, we must ensure a space to preserve the comment.  
									// this trailing space becomes part of the value on the next get.
			//						if(!bAllowWhite)
			//						{
										if((pszValue)&&(strlen(pszValue)))
										{
											if(*(pszValue+strlen(pszValue)-1) != ' ') bEnsureSpace = true;
										}
										else  // the value is blank.  lets make sure that the insert point has a space already
										{
											if(*(pch-1) != ' ') bEnsureSpace = true;
										}
				//					}
									if(bEnsureSpace)
										i = m_ulBufferLength-(pPointer-pch)+strlen(pszValue)+strlen(pszComment)+3; //bufferlength, +3 for " //"
									else
										i = m_ulBufferLength-(pPointer-pch)+strlen(pszValue)+strlen(pszComment)+2; //bufferlength, +2 for "//"

									char* pNewBuffer = (char*) malloc(i+1); // for final 0

									if(pNewBuffer==NULL)
									{
										if( bValueAlloc ) free(pszValue); // free encoded buffer
										if( bCommentAlloc ) free(pszComment); // free encoded buffer

										if(pSec) delete [] pSec;

										return FILEUTIL_WRITE_NOMALLOC;
									}
									memcpy(pNewBuffer, m_pchBuffer, (pch-m_pchBuffer));
									pch = pNewBuffer+(pch-m_pchBuffer);

									if (strlen(pszValue)) memcpy(pch, pszValue, strlen(pszValue));
									if (bEnsureSpace)
										memcpy(pch+strlen(pszValue), " //", 3);
									else
										memcpy(pch+strlen(pszValue), "//", 2);

									memcpy(pch+strlen(pszValue)+(bEnsureSpace?3:2), pszComment, strlen(pszComment));
									memcpy(pch+strlen(pszValue)+(bEnsureSpace?3:2)+strlen(pszComment), pPointer, (m_pchBuffer+m_ulBufferLength-pPointer));

									free(m_pchBuffer);
									m_pchBuffer = pNewBuffer;
									m_ulBufferLength = i; 
									memset(m_pchBuffer+m_ulBufferLength, 0, 1);
	
									if( bValueAlloc ) free(pszValue); // free encoded buffer
									if( bCommentAlloc ) free(pszComment); // free encoded buffer

									if(pSec) delete [] pSec;
									return FILEUTIL_WRITE_OK;

								}
								else // just replace the value.
								{
									while
										(
											(pPointer<(m_pchBuffer+m_ulBufferLength-2))
										&&(
												(!((strncmp(pPointer, "//", 2)==0)&&(isspace(*(pPointer-1)))))
											&&(strncmp(pPointer, "\r\n", 2))
											&&(strncmp(pPointer, "\n", 1))
											)
										) pPointer++;  // advance until comment or newline

									//pPointer is end of second part of buffer to be preserved.


									bool bEnsureSpace = false;
									if( (strncmp(pPointer, "//", 2)==0)&&(isspace(*(pPointer-1)) ))// there was a comment there already, do not overwrite.
									{
										if(strncmp(pPointer, "//allow_whitespace", 18 )==0)
										{
											pch=pchWhite;
										}
								//		else
									// if we allow whitespace but there isn't any, we must ensure a space to preserve the comment.  
									// this trailing space becomes part of the value on the next get.
										{
											if((pszValue)&&(strlen(pszValue)))
											{
												if(!(isspace(*(pszValue+strlen(pszValue)-1)))) bEnsureSpace = true;
											}
											else  // the value is blank.  lets make sure that the insert point has a space already
											{
												if(!(isspace(*(pch-1)))) bEnsureSpace = true;
											}
										}
									}

									if(bEnsureSpace) 
										i = m_ulBufferLength-(pPointer-pch)+strlen(pszValue)+1; //bufferlength
									else
										i = m_ulBufferLength-(pPointer-pch)+strlen(pszValue); //bufferlength
									char* pNewBuffer = (char*) malloc(i+1); // for final 0 

									if(pNewBuffer==NULL)
									{
										if( bValueAlloc ) free(pszValue); // free encoded buffer
										if( bCommentAlloc ) free(pszComment); // free encoded buffer

										if(pSec) delete [] pSec;

										return FILEUTIL_WRITE_NOMALLOC;
									}
									memcpy(pNewBuffer, m_pchBuffer, (pch-m_pchBuffer));
									pch = pNewBuffer+(pch-m_pchBuffer);
									if(strlen(pszValue)) memcpy(pch, pszValue, strlen(pszValue));
									if(bEnsureSpace) memset(pch+strlen(pszValue), ' ', 1);
									memcpy(pch+strlen(pszValue)+(bEnsureSpace?1:0), pPointer, (m_pchBuffer+m_ulBufferLength-pPointer));

									free(m_pchBuffer);
									m_pchBuffer = pNewBuffer;
									m_ulBufferLength = i;
									memset(m_pchBuffer+m_ulBufferLength, 0, 1);
									if( bValueAlloc ) free(pszValue); // free encoded buffer
									if( bCommentAlloc ) free(pszComment); // free encoded buffer
									if(pSec) delete [] pSec;

									return FILEUTIL_WRITE_OK;

								}
								break;
							}
						}
					}
				}
				else // not first on line, so just continue
				{
					while
						(
							(pPointer<(m_pchBuffer+m_ulBufferLength-2))
						&&(
								(strncmp(pPointer, "\r\n", 2))
							&&(strncmp(pPointer, "\n", 1))
							)
						) pPointer++;  // advance until comment or newline
					continue;  //to big while loop
				}
			}
			else
			if(
					(
						(pPointer<(m_pchBuffer+m_ulBufferLength-3))
					&&(
							(strncmp(pPointer, "\r\n[", 3)==0) // new section (thats the specification).
						||(strncmp(pPointer, "\n[", 2)==0) // new section.  (thats the specification).
						)
					)
					||(pPointer>=(m_pchBuffer+m_ulBufferLength-1)) // should never happen
				)
			{
				// we didnt find the entry in the section, so it must be new!

				// back up to last add area of previous section.
				while((pPointer>m_pchBuffer)&&(isspace(*pPointer))) pPointer--;
				while((*pPointer!='\n')) pPointer++;
				pPointer++; //pPointer is splitpoint

				//Add entry

				if((pszComment)&&(strlen(pszComment)>0))
				{
					int nBufLen = m_ulBufferLength+strlen(pszEntry)+strlen(pszValue)+strlen(pszComment)+6; //bufferlength, +6 for "=" + " //" +"\r\n"
					char* pNewBuffer = (char*) malloc(nBufLen+1); // for final 0 

					if(pNewBuffer==NULL) 
					{
						if( bValueAlloc ) free(pszValue); // free encoded buffer
						if( bCommentAlloc ) free(pszComment); // free encoded buffer
						if(pSec) delete [] pSec;

						return FILEUTIL_WRITE_NOMALLOC;
					}		
					memcpy(pNewBuffer, m_pchBuffer, (pPointer-m_pchBuffer));
					char* pch = pNewBuffer+(pPointer-m_pchBuffer);
					memcpy(pch, pszEntry, strlen(pszEntry));			pch+=strlen(pszEntry);
					memcpy(pch, "=", 1);													pch+=1;
					if(strlen(pszValue)) memcpy(pch, pszValue, strlen(pszValue));			pch+=strlen(pszValue);
					if((strncmp(pszComment, "allow_whitespace", 16 )==0)&&(isspace(*(pch-1))))
					{ memcpy(pch, "//", 2);												pch+=2;}
					else
					{	memcpy(pch, " //", 3);											pch+=3;}
					memcpy(pch, pszComment, strlen(pszComment));	pch+=strlen(pszComment);
					memcpy(pch, "\r\n", 2);												pch+=2;
					memcpy(pch, pPointer, (m_pchBuffer+m_ulBufferLength-pPointer));

					free(m_pchBuffer);
					m_pchBuffer = pNewBuffer;
					m_ulBufferLength = nBufLen; 
					memset(m_pchBuffer+m_ulBufferLength, 0, 1);
					if( bValueAlloc ) free(pszValue); // free encoded buffer
					if( bCommentAlloc ) free(pszComment); // free encoded buffer
					if(pSec) delete [] pSec;

					return FILEUTIL_WRITE_OK;
				}
				else // just add the value.without a comment
				{
					int nBufLen = m_ulBufferLength+strlen(pszEntry)+strlen(pszValue)+3; //bufferlength, +3 for "=" +"\r\n"
					char* pNewBuffer = (char*) malloc(nBufLen+1); // for final 0 

					if(pNewBuffer==NULL) 
					{
						if( bValueAlloc ) free(pszValue); // free encoded buffer
						if( bCommentAlloc ) free(pszComment); // free encoded buffer
						if(pSec) delete [] pSec;

						return FILEUTIL_WRITE_NOMALLOC;
					}		
					memcpy(pNewBuffer, m_pchBuffer, (pPointer-m_pchBuffer));
					char* pch = pNewBuffer+(pPointer-m_pchBuffer);
					memcpy(pch, pszEntry, strlen(pszEntry));			pch+=strlen(pszEntry);
					memcpy(pch, "=", 1);													pch+=1;
					if(strlen(pszValue)) memcpy(pch, pszValue, strlen(pszValue));			pch+=strlen(pszValue);
					memcpy(pch, "\r\n", 2);												pch+=2;
					memcpy(pch, pPointer, (m_pchBuffer+m_ulBufferLength-pPointer));

					free(m_pchBuffer);
					m_pchBuffer = pNewBuffer;
					m_ulBufferLength = nBufLen; 
					memset(m_pchBuffer+m_ulBufferLength, 0, 1);
					if( bValueAlloc ) free(pszValue); // free encoded buffer
					if( bCommentAlloc ) free(pszComment); // free encoded buffer
					if(pSec) delete [] pSec;

					return FILEUTIL_WRITE_OK;

				}

				break; // end of section, so get out of big while loop;
			}
			pPointer++;
		}// big while

		// if it made it here, it means that something went wrong, couldnt find an appropriate insert place.
		if( bValueAlloc ) free(pszValue); // free encoded buffer
		if( bCommentAlloc ) free(pszComment); // free encoded buffer
		if(pSec) delete [] pSec;

		return FILEUTIL_WRITE_INSERTERROR;

	}
	else // its a new section, we can add at end.
	{
		pPointer = strstr( m_pchBuffer, "\r\n\r\n[END]\r\n" );
		if(pPointer==NULL) // end every file with this
		{
			pPointer = m_pchBuffer+m_ulBufferLength;
		}
		
				//Add section and entry

		if((pszComment)&&(strlen(pszComment)>0)) 
		{
			int nBufLen = (pPointer-m_pchBuffer)+strlen(pSec)+strlen(pszEntry)+strlen(pszValue)+strlen(pszComment)
				+ strlen("\r\n\r\n[END]\r\n")+10; //bufferlength, +10 for "\r\n\r\n"+"=" + " //" +"\r\n"
			char* pNewBuffer = (char*) malloc(nBufLen+1); // for final 0 

			if(pNewBuffer==NULL) 
			{
				if( bValueAlloc ) free(pszValue); // free encoded buffer
				if( bCommentAlloc ) free(pszComment); // free encoded buffer
				if(pSec) delete [] pSec;

				return FILEUTIL_WRITE_NOMALLOC;
			}
			memcpy(pNewBuffer, m_pchBuffer, (pPointer-m_pchBuffer));
			char* pch = pNewBuffer+(pPointer-m_pchBuffer);
			memcpy(pch, "\r\n\r\n", 4);										pch+=4;
			memcpy(pch, pSec, strlen(pSec));							pch+=strlen(pSec);
			memcpy(pch, pszEntry, strlen(pszEntry));			pch+=strlen(pszEntry);
			memcpy(pch, "=", 1);													pch+=1;
			if(strlen(pszValue)) memcpy(pch, pszValue, strlen(pszValue));			pch+=strlen(pszValue);
			memcpy(pch, " //", 3);												pch+=3;
			memcpy(pch, pszComment, strlen(pszComment));	pch+=strlen(pszComment);
			memcpy(pch, "\r\n\r\n\r\n[END]\r\n", 2+strlen("\r\n\r\n[END]\r\n"));

			free(m_pchBuffer);
			m_pchBuffer = pNewBuffer;
			m_ulBufferLength = nBufLen; 
			memset(m_pchBuffer+m_ulBufferLength, 0, 1);
			if( bValueAlloc ) free(pszValue); // free encoded buffer
			if( bCommentAlloc ) free(pszComment); // free encoded buffer
			if(pSec) delete [] pSec;

			return FILEUTIL_WRITE_OK;
		}
		else // just write value with no comment
		{
			int nBufLen = (pPointer-m_pchBuffer)+strlen(pSec)+strlen(pszEntry)+strlen(pszValue)
				+ strlen("\r\n\r\n[END]\r\n")+7; //bufferlength, +7 for "\r\n\r\n"+"=" +"\r\n"
			char* pNewBuffer = (char*) malloc(nBufLen+1); // for final 0 

			if(pNewBuffer==NULL) 
			{
				if( bValueAlloc ) free(pszValue); // free encoded buffer
				if( bCommentAlloc ) free(pszComment); // free encoded buffer
				if(pSec) delete [] pSec;

				return FILEUTIL_WRITE_NOMALLOC;
			}
			memcpy(pNewBuffer, m_pchBuffer, (pPointer-m_pchBuffer));
			char* pch = pNewBuffer+(pPointer-m_pchBuffer);
			memcpy(pch, "\r\n\r\n", 4);										pch+=4;
			memcpy(pch, pSec, strlen(pSec));							pch+=strlen(pSec);
			memcpy(pch, pszEntry, strlen(pszEntry));			pch+=strlen(pszEntry);
			memcpy(pch, "=", 1);													pch+=1;
			memcpy(pch, pszValue, strlen(pszValue));			pch+=strlen(pszValue);
			memcpy(pch, "\r\n\r\n\r\n[END]\r\n", 2+strlen("\r\n\r\n[END]\r\n"));

			free(m_pchBuffer);
			m_pchBuffer = pNewBuffer;
			m_ulBufferLength = nBufLen; 
			memset(m_pchBuffer+m_ulBufferLength, 0, 1);
			if( bValueAlloc ) free(pszValue); // free encoded buffer
			if( bCommentAlloc ) free(pszComment); // free encoded buffer
			if(pSec) delete [] pSec;

			return FILEUTIL_WRITE_OK;
		}
	}
}


int CFileUtil::SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment)
{
	//only writes it to memory buffer, must call SetSettings to output to file.
	char buffer[32];// max num of decimal digits will be to represent "-4294967295", so this should be fine.
	sprintf(buffer, "%ld", nValue);
	return SetIniString(pszSection, pszEntry, buffer, pszComment);
}


void CFileUtil::Encrypt()
{
	// very simple encryption scheme, just offset by byte
	if(m_pchBuffer)
	{
		for(unsigned long i=0; i<m_ulBufferLength; i++)
		{
			*(m_pchBuffer+i) = (char) ((*(m_pchBuffer+i)-ENCRYPT_OFFSET)&0xff); 
		}
		m_ulStatus|=FILEUTIL_ENCRYPTED;
	}
}

void CFileUtil::Decrypt()
{
	if(m_pchBuffer)
	{
		for(unsigned long i=0; i<m_ulBufferLength; i++)
		{
			*(m_pchBuffer+i) = (char) ((*(m_pchBuffer+i)+ENCRYPT_OFFSET)&0xff);
		}
		m_ulStatus&=~FILEUTIL_ENCRYPTED;
	}
}

