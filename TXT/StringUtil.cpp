// StringUtil.cpp: definition of the CStringUtil class.
//
//////////////////////////////////////////////////////////////////////

#include "StringUtil.h"


CStringUtil::CStringUtil()
{
}

CStringUtil::~CStringUtil()
{
}


std::string CStringUtil::format(const char* fmt, ...)
{
	if (!fmt) return "";
	va_list args;
	va_start(args, fmt);

	int   n = -1, len = 2048;
	char* buffer = 0;
	while (n == -1)
	{
		if (buffer){ try { delete [] buffer;} catch(...){} }
		buffer = new char [len + 1];
		memset(buffer, 0, len + 1);
		n = _vsnprintf_s(buffer, len, len, fmt, args);
		len += 2048;
	}
	std::string s(buffer);
	if (buffer){ try { delete [] buffer;} catch(...){} }

	va_end(args);
	return s;
}

std::wstring CStringUtil::format(const wchar_t* fmt, ...)
{
	if (!fmt) return L"";
	va_list args;
	va_start(args, fmt);

	int   n = -1, len = 2048;
	wchar_t* buffer = 0;
	while (n == -1)
	{
		if (buffer){ try { delete [] buffer;} catch(...){} }
		buffer = new wchar_t [len + 1];
		memset(buffer, 0, len + 1);
		n = _vsnwprintf_s(buffer, len, len, fmt, args);
		len += 2048;
	}
	std::wstring s(buffer);
	if (buffer){ try { delete [] buffer;} catch(...){} }

	va_end(args);
	return s;
}