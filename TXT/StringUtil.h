// StringUtil.h: interface for the CStringUtil class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_STRINGUTIL_H__9B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_)
#define AFX_STRINGUTIL_H__9B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <string>
#include <stdarg.h>

// return codes
#define RETURN_SUCCESS 0

class CStringUtil  
{
public:
	CStringUtil();
	virtual ~CStringUtil();

	std::string format(const char* fmt, ...);
	std::wstring format(const wchar_t* fmt, ...);
};

#endif // !defined(AFX_STRINGUTIL_H__9B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_)
