// JSONUtil.cpp: definition of the CJSONUtil class and object classes
//
//////////////////////////////////////////////////////////////////////

#include "JSONUtil.h"


CJSONString::CJSONString()
{
	m_p=NULL;
}

CJSONString::~CJSONString()
{
	if(m_p) free(m_p);
}



CJSONValue::CJSONValue()
{
	m_p=NULL;
	m_nType = JSON_TYPE_UNDEF;
}

CJSONValue::~CJSONValue()
{
	if(m_p) free(m_p);
}


CJSONNameValPair::CJSONNameValPair()
{
	m_pName=NULL;
	m_pValue=NULL;
}

CJSONNameValPair::~CJSONNameValPair()
{
	if(m_pName) delete m_pName;
	if(m_pValue) delete m_pValue;
}


CJSONArray::CJSONArray()
{
	m_ppValues=NULL;
	m_nNumValues=0;
}

CJSONArray::~CJSONArray()
{
	if(m_ppValues) 
	{
		int i=0;
		while(i<m_nNumValues)
		{
			if(m_ppValues[i]) delete m_ppValues[i];
			i++;
		}

		delete [] m_ppValues;
	}
}


CJSONObj::CJSONObj()
{
	m_ppNameValPairs=NULL;
	m_nNumPairs=0;
}

CJSONObj::~CJSONObj()
{
	if(m_ppNameValPairs) 
	{
		int i=0;
		while(i<m_nNumPairs)
		{
			if(m_ppNameValPairs[i]) delete m_ppNameValPairs[i];
			i++;
		}

		delete [] m_ppNameValPairs;
	}
}




CJSONUtil::CJSONUtil()
{
}

CJSONUtil::~CJSONUtil()
{
}


CJSONObj* CJSONUtil::ConvertBufferToObject(char* pchBuffer)
{

	return NULL;
}


CJSONArray* CJSONUtil::ConvertBufferToArray(char* pchBuffer)
{

	return NULL;
}


CJSONValue* CJSONUtil::ConvertBufferToValue(char* pchBuffer)
{

	return NULL;
}
