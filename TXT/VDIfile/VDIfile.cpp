/*  VDI Files
 *  Start: 01/26/2001 GWR
 *  Finish:03/29/2001 GWR
 *
 *  Public methods:
 *  // opening and closing files:
 *  BOOL OpenForRead(CString szFilename);
 *  BOOL OpenForWrite(CString szFilename);
 *  BOOL Close();
 *  int DecodeMode(CString szMode); // unused at this time
 *
 *  // File operations:
 *  BYTE ReadByte(BOOL *bAtEOF);
 *  BOOL WriteByte(BYTE ch);
 *  CString ReadLine();
 *  void UnReadByte(BYTE ch);
 *  BOOL WriteString(CString szString);
 *  BOOL WriteString(char fmt_string[], ... );
 *  
 *  BOOL IsEOF();
 *  long FilePos();
 *
 *  // helper functions - I use EncodeFile to make the first
 *  // conversion of plaintext to encrypted...
 *  BOOL EncodeFile(CString szFilename, CString szOutputFilename);
 *  BOOL DecodeFile(CString szFilename, CString szOutputFilename);
 */

#include "stdafx.h"

#include "VDIfile.h"

#define GSTR(X) (char *)((LPCTSTR) X)

#define MODE_READ   1
#define MODE_WRITE  2
#define MODE_APPEND 4
#define MODE_TEXT   8
#define MODE_BINARY 16


CVDIfile::CVDIfile()
{
  m_bStub = FALSE;
  m_fp = NULL;
  m_nBufferSize = 0;
}



CVDIfile::~CVDIfile()
{
//  AfxMessageBox("vdi1");
  Close();
//  AfxMessageBox("vdi2");
}

BOOL CVDIfile::OpenForRead(CString szFilename)
{
  return Open(szFilename, "rb");
}

BOOL CVDIfile::OpenForWrite(CString szFilename)
{
  return Open(szFilename, "wb");
}


//To create a poll in existing polling place
// Source: Polling Place Current Polls page - dfspot.html
BOOL CVDIfile::Open(CString szFilename, CString szMode)
{
  int nModes;

  nModes = DecodeMode(szMode);

  m_fp = fopen(GSTR(szFilename), GSTR(szMode));
  if (m_fp == NULL)
    return FALSE;

  // Initialize data structures for enc/decryption

  
  return TRUE;
}



BOOL CVDIfile::Close()
{
  if (m_fp) { fclose(m_fp); m_fp = NULL; }
  return TRUE;
}



CString CVDIfile::ReadLine(BOOL *bAtEOF)
{
  *bAtEOF=FALSE;

  if (m_fp == NULL) { *bAtEOF=TRUE; return ""; }

  CString szLine = "";
  BYTE ch=0;

  while (!(*bAtEOF))
  {
    ch = ReadByte(bAtEOF);
    if (*bAtEOF) break;
    if (ch != '\n') 
      szLine += (BYTE) ch; 
    else break;
  }

  //szLine.TrimLeft();  szLine.TrimRight();
  //AfxMessageBox(szLine);
  return szLine;
}



void CVDIfile::UnReadByte(BYTE ch)
{
    m_szBuffer += (BYTE) ch;
    m_nBufferSize++;
}



BYTE CVDIfile::ReadByte(BOOL *bAtEOF)
{
  BYTE ch;
  *bAtEOF = FALSE;

  if (m_fp == NULL) { *bAtEOF = TRUE; return 0; }

  if (m_nBufferSize == 0)
  {
    char buffer[1026];
    int i,n;
    // fill buffer
    n = fread(buffer, 1, 1024, m_fp);
    if (n == 0) { *bAtEOF = TRUE; return 0; }

    for (i=0; i<n; i++)
    {
      m_szBuffer += (char) Decrypt(buffer[i], 0);
    }
    //AfxMessageBox("Read new block: "+m_szBuffer);
    m_nBufferSize=n;
  }

  ch = m_szBuffer[0];
  m_nBufferSize--;
  m_szBuffer = m_szBuffer.Right(m_nBufferSize);

  return ch;
}

BOOL CVDIfile::EncodeFile(CString szFilename, CString szOutputFilename)
{
  CVDIfile u;
  FILE *fp;

  fp = fopen((LPCTSTR)szFilename, "rb");
  if (fp == NULL) return FALSE;
  if (!u.OpenForWrite(szOutputFilename))
  { fclose(fp); return FALSE; }

  while (!feof(fp))
    u.WriteByte((BYTE) fgetc(fp));
  fclose(fp); u.Close();
  return TRUE;
}


BOOL CVDIfile::DecodeFile(CString szFilename, CString szOutputFilename)
{
  CVDIfile v;
  FILE *fp;
  BOOL bAtEOF=FALSE;
  BYTE ch;

  if (!v.OpenForRead(szFilename)) return FALSE;
  fp = fopen((LPCTSTR)szOutputFilename, "wb");
  if (fp == NULL) { v.Close(); return FALSE; }

  while (!bAtEOF)
  {
    ch = v.ReadByte(&bAtEOF);
    if (bAtEOF) break;

    fputc(ch, fp);
  }
  v.Close();
  fclose(fp);
  return TRUE;
}


long CVDIfile::FilePos()
{
  return ftell(m_fp);
}


BOOL CVDIfile::WriteString(CString szString)
{
  return WriteStringMain(szString);
}


BOOL CVDIfile::WriteString(char fmt_string[], ... )
{
  char buffer[1024];
  va_list marker;

  // create the formatted output string
  va_start(marker, fmt_string); // Initialize variable arguments.
  vsprintf((char *) buffer, fmt_string, (va_list) marker);
  va_end( marker );             // Reset variable arguments.

  return WriteStringMain(buffer);
}



BOOL CVDIfile::WriteStringMain(CString szString)
{
  BOOL rv = TRUE;
  int i;

  for (i=0; i<szString.GetLength(); i++)
  {
    if (!WriteByte(szString[i]))
      rv = FALSE;
  }
  return rv;
}



BOOL CVDIfile::WriteByte(BYTE ch)
{
  int rv;
  if (m_fp == NULL) return 0;
  rv = fputc(Encrypt(ch, FilePos()), m_fp);
  if (rv == EOF) return FALSE;
  return TRUE;
}



// "r" or "w" or "a"
// "b" or "t"
int CVDIfile::DecodeMode(CString szMode)
{
  int i, modes = 0;

  for (i=0; i<szMode.GetLength(); i++)
  {
    if (szMode[i] == 'r')      { modes |= MODE_READ; }
    else if (szMode[i] == 'w') { modes |= MODE_WRITE; }
    else if (szMode[i] == 'a') { modes |= MODE_APPEND; }
    else if (szMode[i] == 'b') { modes |= MODE_BINARY; }
    else if (szMode[i] == 't') { modes |= MODE_TEXT; }
  }
  return modes;
}



BYTE CVDIfile::Encrypt(BYTE ch, unsigned long fpos)
{
  BYTE t;
  if (ch <128) t = ch + 128;
  else t = ch-128;
  return t;
}



BYTE CVDIfile::Decrypt(BYTE ch, unsigned long fpos)
{
  BYTE t;
  if (ch <128) t = ch + 128;
  else t = ch-128;
  return t;
}
