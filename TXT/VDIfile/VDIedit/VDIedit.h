// VDIedit.h : main header file for the VDIEDIT application
//

#if !defined(AFX_VDIEDIT_H__511644E4_2533_11D5_AFF8_0010A4B277C0__INCLUDED_)
#define AFX_VDIEDIT_H__511644E4_2533_11D5_AFF8_0010A4B277C0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CVDIeditApp:
// See VDIedit.cpp for the implementation of this class
//

class CVDIeditApp : public CWinApp
{
public:
	CVDIeditApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVDIeditApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CVDIeditApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VDIEDIT_H__511644E4_2533_11D5_AFF8_0010A4B277C0__INCLUDED_)
