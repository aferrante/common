// VDIeditDlg.h : header file
//

#if !defined(AFX_VDIEDITDLG_H__511644E6_2533_11D5_AFF8_0010A4B277C0__INCLUDED_)
#define AFX_VDIEDITDLG_H__511644E6_2533_11D5_AFF8_0010A4B277C0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CVDIeditDlg dialog

class CVDIeditDlg : public CDialog
{
// Construction
public:
	CVDIeditDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CVDIeditDlg)
	enum { IDD = IDD_VDIEDIT_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVDIeditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

public:
  CString m_szFilename, m_szEditBuffer;
  CVDIfile m_vdifile;
  BOOL getFilename(CString *filename, CString *ext);
  BOOL getFilenameSave(CString *filename, CString *ext);
  BOOL loadFile(CString filename);
  BOOL saveFile(CString filename);

	// Generated message map functions
	//{{AFX_MSG(CVDIeditDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonOpen();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonConvert();
	afx_msg void OnButtonBrowse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VDIEDITDLG_H__511644E6_2533_11D5_AFF8_0010A4B277C0__INCLUDED_)
