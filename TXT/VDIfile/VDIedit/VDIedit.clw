; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CVDIeditDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "VDIedit.h"

ClassCount=3
Class1=CVDIeditApp
Class2=CVDIeditDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_VDIEDIT_DIALOG

[CLS:CVDIeditApp]
Type=0
HeaderFile=VDIedit.h
ImplementationFile=VDIedit.cpp
Filter=N

[CLS:CVDIeditDlg]
Type=0
HeaderFile=VDIeditDlg.h
ImplementationFile=VDIeditDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_BUTTON_BROWSE

[CLS:CAboutDlg]
Type=0
HeaderFile=VDIeditDlg.h
ImplementationFile=VDIeditDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_VDIEDIT_DIALOG]
Type=1
Class=CVDIeditDlg
ControlCount=9
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_BOX,edit,1353781444
Control4=IDC_STATIC_FN,static,1342308352
Control5=IDC_BUTTON_OPEN,button,1342242816
Control6=IDC_BUTTON_SAVE,button,1342242816
Control7=IDC_EDIT_CONVFN,edit,1350631552
Control8=IDC_BUTTON_BROWSE,button,1342242816
Control9=IDC_BUTTON_CONVERT,button,1342242816

