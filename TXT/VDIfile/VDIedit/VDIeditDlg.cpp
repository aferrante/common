// VDIeditDlg.cpp : implementation file
//

#include "stdafx.h"

#include "../VDIfile.h"

#include "VDIedit.h"
#include "VDIeditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVDIeditDlg dialog

CVDIeditDlg::CVDIeditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVDIeditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVDIeditDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVDIeditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVDIeditDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CVDIeditDlg, CDialog)
	//{{AFX_MSG_MAP(CVDIeditDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_OPEN, OnButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CONVERT, OnButtonConvert)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVDIeditDlg message handlers

BOOL CVDIeditDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
  loadFile(m_szFilename);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CVDIeditDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CVDIeditDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CVDIeditDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CVDIeditDlg::OnButtonOpen() 
{
	CString filename, ext;
  if (getFilename(&filename, &ext))
  {
    m_szFilename = filename;
    loadFile(m_szFilename);
  }
  	
}

void CVDIeditDlg::OnButtonSave() 
{
	CString filename, ext;
  if (getFilenameSave(&filename, &ext))
  {
    m_szFilename = filename;
    saveFile(m_szFilename);
  }
}


/// gregg's image file dialog
BOOL CVDIeditDlg::getFilename(CString *filename, CString *ext)
{
  *filename="";   *ext="";
  static char BASED_CODE szFilter[] = 
    "VDI (*.vdi)|*.vdi|All Files (*.*)|*.*||";
  CFileDialog zorg(TRUE, NULL, NULL, 0, szFilter, NULL);
  if (zorg.DoModal() == IDOK)
  {
    *filename = zorg.GetPathName();
    *ext = zorg.GetFileExt();
    return TRUE;
  }
  return FALSE;
}

/// gregg's image file dialog
BOOL CVDIeditDlg::getFilenameSave(CString *filename, CString *ext)
{
  *filename="";   *ext="";
  static char BASED_CODE szFilter[] = 
    "VDI (*.vdi)|*.vdi|All Files (*.*)|*.*||";
  CFileDialog zorg(FALSE, NULL, NULL, 0, szFilter, NULL);
  if (zorg.DoModal() == IDOK)
  {
    *filename = zorg.GetPathName();
    *ext = zorg.GetFileExt();
    return TRUE;
  }
  return FALSE;
}


BOOL CVDIeditDlg::loadFile(CString filename)
{
  m_szEditBuffer  = "";
  if (filename != "")
  {
    BOOL atEOF = FALSE;
    CString szTemp;
    if (m_vdifile.OpenForRead(filename))
    {
      while (!atEOF)
      {
        szTemp  = m_vdifile.ReadLine(&atEOF);
        szTemp += "\r\n";
        if (!atEOF)
        {
          m_szEditBuffer += szTemp;
        }
      }
      m_vdifile.Close();
      GetDlgItem(IDC_EDIT_BOX)->SetWindowText(m_szEditBuffer);
      GetDlgItem(IDC_STATIC_FN)->SetWindowText("Filename: " + m_szFilename);
    }
    else 
    {
      GetDlgItem(IDC_STATIC_FN)->SetWindowText("Error opening file: " + m_szFilename);
      return FALSE;
    }
  }
  return TRUE;
}



BOOL CVDIeditDlg::saveFile(CString filename)
{
  m_szEditBuffer  = "";
  if (filename != "")
  {
    BOOL atEOF = FALSE;
    CString szTemp;
    if (m_vdifile.OpenForWrite(filename))
    {
      GetDlgItem(IDC_EDIT_BOX)->GetWindowText(m_szEditBuffer);
      m_vdifile.WriteString(m_szEditBuffer);
      m_vdifile.Close();
      GetDlgItem(IDC_STATIC_FN)->SetWindowText("Filename: " + m_szFilename);
    }
    else 
    {
      GetDlgItem(IDC_STATIC_FN)->SetWindowText("Error opening file: " + m_szFilename);
      return FALSE;
    }
  }
  return TRUE;
}

void CVDIeditDlg::OnButtonConvert() 
{
  CString szFilename, szOutput, szTemp;
  GetDlgItem(IDC_EDIT_CONVFN)->GetWindowText(szFilename);

  if (szFilename.Right(4) == ".vdi")
  {
    // decode mode
    szOutput = szFilename.Left(szFilename.GetLength() - 4) + ".txt";
    if (m_vdifile.DecodeFile(szFilename, szOutput))
    {
      szTemp.Format("Decoded VDI file '%s' to\n'%s'", szFilename, szOutput);
      AfxMessageBox(szTemp);

      m_szFilename = szFilename;
      loadFile(szFilename);
    }
    else AfxMessageBox("Decoding VDI file failed!");
  }
  else
  {
    szOutput = szFilename;
    if (szOutput.Mid(szOutput.GetLength() - 4, 1) == ".")
      szOutput = szOutput.Left(szOutput.GetLength() - 4) + ".vdi";
    else if (szOutput.Mid(szOutput.GetLength() - 3, 1) == ".")
      szOutput = szOutput.Left(szOutput.GetLength() - 3) + ".vdi";
    else if (szOutput.Mid(szOutput.GetLength() - 2, 1) == ".")
      szOutput = szOutput.Left(szOutput.GetLength() - 2) + ".vdi";
    else 
      szOutput += ".vdi";

    if (m_vdifile.EncodeFile(szFilename, szOutput))
    {
      szTemp.Format("Encoded file '%s' to\nVDI file '%s'", szFilename, szOutput);
      AfxMessageBox(szTemp);

      m_szFilename = szOutput;
      loadFile(szOutput);
    }
    else AfxMessageBox("Decoding VDI file failed!");
  }
}

void CVDIeditDlg::OnButtonBrowse() 
{
  CString filename,ext;
  if (getFilename(&filename, &ext))
    GetDlgItem(IDC_EDIT_CONVFN)->SetWindowText(filename);
}
