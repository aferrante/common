/*  VDI Files
 *  Start: 01/26/2001 GWR
 */

#ifndef VDIFILE_INCLUDED
#define VDIFILE_INCLUDED

class CVDIfile 
{
public:
	CVDIfile();   // standard constructor
	~CVDIfile();   // standard destructor

  BOOL  m_bStub;


  BOOL OpenForRead(CString szFilename);
  BOOL OpenForWrite(CString szFilename);
  BOOL Close();
  int DecodeMode(CString szMode);

  BYTE ReadByte(BOOL *bAtEOF);
  BOOL WriteByte(BYTE ch);
  CString ReadLine(BOOL *bAtEOF);
  void UnReadByte(BYTE ch);
  BOOL WriteString(CString szString);
  BOOL WriteString(char fmt_string[], ... );
  long FilePos();

  BOOL EncodeFile(CString szFilename, CString szOutputFilename);
  BOOL DecodeFile(CString szFilename, CString szOutputFilename);

protected:
  FILE *m_fp;
  // internal buffer for efficient reads
  CString m_szBuffer;
  int m_nBufferSize;

  BOOL Open(CString szFilename, CString szMode);
  BOOL WriteStringMain(CString szString);
  BYTE Encrypt(BYTE ch, unsigned long fpos);
  BYTE Decrypt(BYTE ch, unsigned long fpos);
};

#endif
