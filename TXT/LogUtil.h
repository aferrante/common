// LogUtil.h: interface for the CLogUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
#define AFX_LOGUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <iostream.h>
#include "../MSG/MessagingObject.h"

#define LOG_SUCCESS			0
#define LOG_BADBUFFER		1
#define LOG_ROTATED			2
#define LOG_ERROR				-1

#define LOG_GETALL				0x01
#define LOG_GETTABLES			0x02
#define LOG_GETFROMTOP		0x04
#define LOG_GETASVERBOSE	0x08

class CLogUtil : public CMessagingObject
{
public:
	bool m_bThreadStarted;
	bool m_bFileInUse;
//	bool m_bBufferInUse;
	bool m_bAllowRepeat;
	bool m_bKillThread;
	bool m_bVerboseFile;
	char* m_pchSendBuffer;
	char* m_pchNewFileBuffer;
	char* m_pszLastMessage;
	long m_nAbsoluteTimeOffsetMS;
	unsigned long m_ulBufferLength;
	unsigned long m_ulNewFileBufferLength;
	unsigned long m_ulLastUnixTime;
	unsigned short m_usLastMillitime;
	unsigned long m_ulLastMessageCount;
	FILE* m_pfile;
	char* m_pszFileBaseName;
	char* m_pszDestinationTable;
	char* m_pszFunctionTable;
	char* m_pszFilenameFormatSpec;
	char* m_pszLastRotateKey; //forces new file.

	unsigned long m_ulReqCount;
	unsigned long m_ulSvcCount;

	CRITICAL_SECTION m_critBuffer;

protected:
	char m_chOverwritePeriod;
	char m_chRotatePeriod;
	unsigned char m_nRotateOffsetMonth;
	unsigned char m_nRotateOffsetDay;
	unsigned char m_nRotateOffsetHours;
	unsigned char m_nRotateOffsetMinutes;

public:
	CLogUtil();
	virtual ~CLogUtil();

	int OpenLog(char* pszFileParams);
	int CloseLog();
	int RotateLog(unsigned long unixtime=0, bool bOpen=false);
	int Synchronize(_timeb timeSynch); 
	int WriteTimeOffsetRecord();

	char* ReturnRecords(unsigned long* nulNumRecords, char nMode=LOG_GETTABLES);

// override
	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);
	int Modify(char* pchParams, unsigned long* pulBufferLen);
	
// utility
	int   HumanReadableFile(char* pszInputFileName, char* pszOutputFileName);
	char* HumanReadableStream(char* pchInputBuffer, unsigned long* pulBufferLength );

};

#endif // !defined(AFX_LOGUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
