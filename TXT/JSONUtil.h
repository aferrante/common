// JSONUtil.h: interface for the CJSONUtil class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_JSONUTIL_H__9B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_)
#define AFX_JSONUTIL_H__9B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <stdlib.h>

//  http://www.json.org/
// although it is supposed to be "Unicode", this was written for UTF8, so we use char instead of wchar_t

#ifndef NULL
#define NULL 0
#endif

// return codes
#define JSON_SUCCESS 0
#define JSON_ERROR -1


#define JSON_TYPE_UNDEF	 -1
#define JSON_TYPE_STRING	0
#define JSON_TYPE_NUMBER	1
#define JSON_TYPE_TRUE		2
#define JSON_TYPE_FALSE		3
#define JSON_TYPE_NULL		4
#define JSON_TYPE_OBJECT	5
#define JSON_TYPE_ARRAY		6

class CJSONString
{
public:
	CJSONString();
	virtual ~CJSONString();

	char* m_p;
};


class CJSONValue
{
public:
	CJSONValue();
	virtual ~CJSONValue();
//A value can be a string in double quotes, or a number, or true or false or null, or an object or an array. These structures can be nested.

	int m_nType;
	void* m_p;
};


class CJSONNameValPair
{
public:
	CJSONNameValPair();
	virtual ~CJSONNameValPair();

	CJSONString* m_pName;
	CJSONValue* m_pValue;
};

class CJSONArray
{
public:
	CJSONArray();
	virtual ~CJSONArray();

	CJSONValue** m_ppValues;
	int m_nNumValues;
};

class CJSONObj
{
public:
	CJSONObj();
	virtual ~CJSONObj();

	CJSONNameValPair** m_ppNameValPairs;
	int m_nNumPairs;
};


class CJSONUtil
{
public:
	CJSONUtil();
	virtual ~CJSONUtil();

	CJSONObj* ConvertBufferToObject(char* pchBuffer=NULL);
	CJSONArray* ConvertBufferToArray(char* pchBuffer=NULL);
	CJSONValue* ConvertBufferToValue(char* pchBuffer=NULL);

};

#endif // !defined(AFX_JSONUTIL_H__9B0ABB9A_7D37_4C7A_9F79_2F97C59FE7D7__INCLUDED_)
