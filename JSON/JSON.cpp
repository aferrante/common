// JSON.cpp: implementation of the CJSON class.
//
//////////////////////////////////////////////////////////////////////

#include <StdAfx.h>
#include <stdlib.h>
#include <string.h>
#include "JSON.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CJSON::CJSON()
{
	m_pszJSONString = NULL;
}

CJSON::~CJSON()
{
	if(m_pszJSONString) free(m_pszJSONString);
}

int CJSON::SetObject(char* pszJSONString)
{
	if((pszJSONString)&&(strlen(pszJSONString)))
	{
		if(m_pszJSONString) free(m_pszJSONString);
		m_pszJSONString = (char*)malloc(strlen(pszJSONString)+1);
		if(m_pszJSONString)
		{
			strcpy(m_pszJSONString, pszJSONString);
			return JSON_SUCCESS;
		}
	}
	return JSON_ERROR;
}

//GetObjectLength expects the first char to be a {.  it will count until it reaches the matching }
//or it will just count until it finds a 
int CJSON::GetObjectLength(char* pszJSONString)
{
	if(pszJSONString == NULL)	// if NULL, use internal
	{
		pszJSONString = m_pszJSONString;
	}

	if(pszJSONString)
	{
		unsigned int i=0;
		int ref=0;
		bool bInString = false;
		while(i < strlen(pszJSONString))
		{

/*
#define JSON_TOKEN_OPENBRACE			'{'
#define JSON_TOKEN_CLOSEBRACE			'}'
#define JSON_TOKEN_OPENBRACKET		'['
#define JSON_TOKEN_CLOSEBRACKET		']'
#define JSON_TOKEN_COMMA					','
#define JSON_TOKEN_DBLQUOTE				'\"'
#define JSON_TOKEN_COLON					':'
#define JSON_TOKEN_ESC						'\\'

*/
			switch(*(pszJSONString+i))
			{
			case JSON_TOKEN_OPENBRACE:
				{
					if(!bInString) ref++; 
				} break;
			case JSON_TOKEN_CLOSEBRACE: 
				{
					if(!bInString) ref--; 
					if(ref <= 0) // ref count back to zero
					{
						if(ref<0) return JSON_BADARGS; // got more closebraces than open.

						return (i+1);
					}
				} break;
			case JSON_TOKEN_DBLQUOTE:	
				{
					if(bInString) 
					{
						if((*(pszJSONString+i-1)) != JSON_TOKEN_ESC) 
						{
							bInString = false;
						}
						//else // escaped dbl quote inside string, do not count.
					}
					else bInString = true;
				} break;
			case 0: // null term!
				{
					return JSON_BADARGS; // not full JSON
				} break;
			}

			i++;
		}
	}

	return JSON_ERROR;
}


// in GetObjectValue, pszJSONString is the string identifier of the object of which we want the value returned
// this just returns a buffer with the value of the first item with a string match.
char* CJSON::GetObjectValue(char* pszJSONString, unsigned long ulFlags)
{
	char* pch=NULL;
	if((pszJSONString)&&(strlen(pszJSONString)>0)&&(m_pszJSONString)&&(strlen(m_pszJSONString)>strlen(pszJSONString)))
	{
		unsigned int i=0;
		unsigned int j=0;
		int ref=0;
		int valref=0;
		bool bInString = false;
		bool bInValue = false;
		bool bIDfound = false;

		char* pchBuffer=(char*)malloc(strlen(m_pszJSONString)+1); // can contain the entire thing, so def can contain the value.  we'll realloc after.

		if(pchBuffer)
		{
			while((i < strlen(m_pszJSONString))&&(pch==NULL))
			{

	/*
	#define JSON_TOKEN_OPENBRACE			'{'
	#define JSON_TOKEN_CLOSEBRACE			'}'
	#define JSON_TOKEN_OPENBRACKET		'['
	#define JSON_TOKEN_CLOSEBRACKET		']'
	#define JSON_TOKEN_COMMA					','
	#define JSON_TOKEN_DBLQUOTE				'\"'
	#define JSON_TOKEN_COLON					':'
	#define JSON_TOKEN_ESC						'\\'
	*/




				if((bIDfound)&&(bInValue)) // otherwise no reason to start saving it down.
				{
					*(pchBuffer+j) = *(m_pszJSONString+i);
					j++;
				}
//				if(bInValue) AfxMessageBox("Inval");

				switch(*(m_pszJSONString+i))
				{
				case JSON_TOKEN_OPENBRACE:
					{
						if(!bInString)
						{
							ref++; 
//							if(bInValue)
//							{
//								CString foo; foo.Format("ref++ %d, valref %d", ref, valref);
//								AfxMessageBox(foo);
//							}
						}
					} break;
				case JSON_TOKEN_COMMA:
					{
						if((!bInString)&&(bIDfound)&&(bInValue)&&(ref <= valref))
						{
							bInValue = false;
							*(pchBuffer+j) = 0;
							pch = (char*)realloc(pchBuffer, strlen(pchBuffer)+1);
						}
					} break;
				case JSON_TOKEN_CLOSEBRACE: 
					{
						if(!bInString)
						{
							ref--;
//							if(bInValue)
//							{
//								CString foo; foo.Format("ref-- %d, valref %d", ref, valref);
//								AfxMessageBox(foo);
//							}
							if((bInValue)&&(bIDfound)&&(ref <= valref))
							{
								bInValue = false;
								*(pchBuffer+j) = 0;
								pch = (char*)realloc(pchBuffer, strlen(pchBuffer)+1);
							}
						}
					} break;
				case JSON_TOKEN_DBLQUOTE:	
					{
						if(bInString) 
						{
							if((*(pszJSONString+i-1)) != JSON_TOKEN_ESC) 
							{
								bInString = false;
								
								if(!bIDfound)
								{
									*(pchBuffer+j) = 0; // terminate the string so we can compare it.
									if(ulFlags&JSON_CASE_SENSITIVE)
									{
										if(strcmp(pchBuffer, pszJSONString)==0)
										{
											// found the string ID.
											bIDfound = true;
											valref = ref;
											j=0; // reset the buffer for the value.
										}
									}
									else
									{
										if(stricmp(pchBuffer, pszJSONString)==0)
										{
											// found the string ID.
											bIDfound = true;
											valref = ref;
											j=0;// reset the buffer for the value.
										}
									}			
								}
								else // we are in a string that might be a value, or part of a value.
								{
									if((bInValue)&&(ref <= valref)) // if the string is the entire value, then ref = valref.  otherwise we jsut keep adding it to the full value
									{
										bInValue = false;
										*(pchBuffer+j) = 0;

										pch = (char*)realloc(pchBuffer, strlen(pchBuffer)+1);
									}
								}
							}
							//else // escaped dbl quote inside string, do not count, just add to buffer as value, already done above if in a value.
						}
						else 
						{
							bInString = true;
							if((!bIDfound)&&(!bInValue)) j=0; // searching for the string identifier!
						}
					} break;
				case JSON_TOKEN_COLON:
					{
						if((!bInString)&&(bIDfound)) // let's only start saving a value if we've found the identifier
						{
							if(!bInValue) // only set this once - there may be a colon in a value, if a value is an array of objects.
							{
								bInValue = true;
								valref= ref;
							}
						}
					} break;
				case 0: // null term!
					{
						i = strlen(m_pszJSONString); // not full JSON, this breaks us out (shouldnt happen)
					} break;
				default:
					{
						if(
							   ((bInString)&&(!bIDfound)&&(!bInValue))
							)
						{
							*(pchBuffer+j) = *(m_pszJSONString+i);

//							AfxMessageBox(pchBuffer);
							j++;
						}

					} break;
				}

				i++;
			}
		}
	}
	return pch;
}



#ifdef OLD_LETS_TRY_AGAIN
// in GetObjectValue, pszJSONString is the string identifier of the object of which we want the value returned
// this just returns a buffer with the value of the first item with a string match.
char* CJSON::GetObjectValue(char* pszJSONString, unsigned long ulFlags)
{
	char* pch=NULL;
	if((pszJSONString)&&(strlen(pszJSONString)>0)&&(m_pszJSONString)&&(strlen(m_pszJSONString)>strlen(pszJSONString)))
	{
		unsigned int i=0;
		unsigned int j=0;
		int ref=0;
		int valref=0;
		bool bInString = false;
		bool bInValue = false;
		bool bIDfound = false;

		char* pchBuffer=(char*)malloc(strlen(m_pszJSONString)+1); // can contain the entire thing, so def can contain the value.  we'll realloc after.

		if(pchBuffer)
		{
			while((i < strlen(m_pszJSONString))&&(pch==NULL))
			{

	/*
	#define JSON_TOKEN_OPENBRACE			'{'
	#define JSON_TOKEN_CLOSEBRACE			'}'
	#define JSON_TOKEN_OPENBRACKET		'['
	#define JSON_TOKEN_CLOSEBRACKET		']'
	#define JSON_TOKEN_COMMA					','
	#define JSON_TOKEN_DBLQUOTE				'\"'
	#define JSON_TOKEN_COLON					':'
	#define JSON_TOKEN_ESC						'\\'
	*/




				if(bInValue)
				{
					*(pchBuffer+j) = *(m_pszJSONString+i);
					j++;
				}


				switch(*(m_pszJSONString+i))
				{
				case JSON_TOKEN_OPENBRACE:
					{
						if(!bInString) ref++; 
					} break;
				case JSON_TOKEN_COMMA:
					{
						if((!bInString)&&(bIDfound)&&(bInValue)&&(ref <= valref))
						{
							bInValue = false;
							*(pchBuffer+j) = 0;
							
							pch = (char*)realloc(pchBuffer, strlen(pchBuffer)+1);
							i = strlen(m_pszJSONString); // this breaks us out 
						}
					} break;
				case JSON_TOKEN_CLOSEBRACE: 
					{
						if(!bInString)
						{
							ref--;
							if((bInValue)&&(bIDfound)&&(ref <= valref))
							{
								bInValue = false;
								*(pchBuffer+j) = 0;

								pch = (char*)realloc(pchBuffer, strlen(pchBuffer)+1);
								i = strlen(m_pszJSONString); // this breaks us out 
							}
						}
					} break;
				case JSON_TOKEN_DBLQUOTE:	
					{
						if(bInString) 
						{
							if((*(pszJSONString+i-1)) != JSON_TOKEN_ESC) 
							{
								bInString = false;								
								*(pchBuffer+j) = 0;

								if(!bIDfound)
								{
									if(ulFlags&JSON_CASE_SENSITIVE)
									{

										if(strcmp(pchBuffer, pszJSONString)==0)
										{
											// found the string ID.
											bIDfound = true;
											valref = ref;
											j=0;
										}
									}
									else
									{
										if(stricmp(pchBuffer, pszJSONString)==0)
										{
											// found the string ID.
											bIDfound = true;
											valref = ref;
											j=0;
										}
									}			
								}
								else
								{
									if((bInValue)&&(ref <= valref))
									{
										bInValue = false;
										*(pchBuffer+j) = 0;

										pch = (char*)realloc(pchBuffer, strlen(pchBuffer)+1);
										i = strlen(m_pszJSONString); // this breaks us out 
									}
								}
							}
							//else // escaped dbl quote inside string, do not count, just add to buffer as value
							else
							{
								*(pchBuffer+j) = *(m_pszJSONString+i);
								j++;
							}

						}
						else 
						{
							bInString = true;
							if((!bIDfound)&&(!bInValue)) j=0; // searching for the string identifier!
						}
					} break;
				case JSON_TOKEN_COLON:
					{
						if(!bInString)
						{
							bInValue = true;
							valref= ref;
						}
					} break;
				case 0: // null term!
					{
						i = strlen(m_pszJSONString); // not full JSON, this breaks us out (shouldnt happen)
					} break;
				default:
					{
						if(
							   ((bInString)&&(!bIDfound)&&(!bInValue))
							)
						{
							*(pchBuffer+j) = *(m_pszJSONString+i);
							j++;
						}

					} break;
				}

				i++;
			}
		}
	}
	return pch;
}

#endif