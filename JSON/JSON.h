// JSON.h: interface for the CJSON class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_JSON_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_)
#define AFX_JSON_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define JSON_SUCCESS				0
#define JSON_ERROR					-1
#define JSON_INPROGRESS 		-2
#define JSON_BADARGS				-3
#define JSON_MEMEX					-4
#define JSON_UNSUPPORTED		-256

#define JSON_TOKEN_OPENBRACE			'{'
#define JSON_TOKEN_CLOSEBRACE			'}'
#define JSON_TOKEN_OPENBRACKET		'['
#define JSON_TOKEN_CLOSEBRACKET		']'
#define JSON_TOKEN_COMMA					','
#define JSON_TOKEN_DBLQUOTE				'\"'
#define JSON_TOKEN_COLON					':'
#define JSON_TOKEN_ESC						'\\'


#define JSON_CASE_SENSITIVE			0x00000001


class CJSON  
{
public:
	char* m_pszJSONString; 

public:
	CJSON();
	virtual ~CJSON();

	int SetObject(char* pszJSONString);
	int GetObjectLength(char* pszJSONString);
	char* GetObjectValue(char* pszJSONString, unsigned long ulFlags=0);
};

#endif // !defined(AFX_JSON_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_)
