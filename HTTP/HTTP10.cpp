// HTTP10.cpp: implementation of the CHTTP10 class.
//
//////////////////////////////////////////////////////////////////////

//#include <stdafx.h>  //remove this
#include "HTTP10.h"
#include <process.h>
#include "../TXT/BufferUtil.h"

/*  .. if no stdafx, no DEBUG_NEW - have to exclude this
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/
//#include "../TXT/BufferUtil.h"

//////////////////////////////////////////////////////////////////////
// CHTTPHeader Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHTTPHeader::CHTTPHeader()
{
	m_ucMethod			= HTTP_UNDEF;  // enumerated values
	m_pszVersion		= NULL;
	m_pszURL				= NULL;
	m_pszPath				= NULL;
	m_pszUser				= NULL;
	m_pszPassword		= NULL;
	m_pszHeaders		= NULL;  // other headers
	m_pszMimeType		= NULL;
	m_ppszArgNames	= NULL;
	m_ppszArgValues	= NULL;
	m_ulNumArgs			= 0;
}

CHTTPHeader::~CHTTPHeader()
{
	if(m_pszVersion)	free(m_pszVersion);   // must use malloc to allocate
	if(m_pszURL)			free(m_pszURL);				// must use malloc to allocate
	if(m_pszPath)			free(m_pszPath);			// must use malloc to allocate
	if(m_pszUser)			free(m_pszUser);			// must use malloc to allocate
	if(m_pszPassword)	free(m_pszPassword);  // must use malloc to allocate
	if(m_pszHeaders)	free(m_pszHeaders);   // must use malloc to allocate
	// DONT free the mime type pointer
	if(m_ulNumArgs>0)
	{
		if(m_ppszArgNames)
		{
			unsigned long i=0; 
			while(i<m_ulNumArgs)
			{
				if(m_ppszArgNames[i]) free(m_ppszArgNames[i]);
				i++;
			}
			free(m_ppszArgNames);
		}
		if(m_ppszArgValues)
		{
			unsigned long i=0; 
			while(i<m_ulNumArgs)
			{
				if(m_ppszArgValues[i]) free(m_ppszArgValues[i]);
				i++;
			}
			free(m_ppszArgValues);
		}
	}
}


//////////////////////////////////////////////////////////////////////
// CHTTP10 Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHTTP10::CHTTP10(char* pszRootPath)
{
	m_pSecure = NULL;			// pointer to a security object, if desired.  if this is null, no security is used
	m_pszHost = NULL;			// the name of the host, needed for redirects
	m_ppszErrors = NULL;		// a pointer to a zero terminated string with formatted filenames of template error pages.
	m_ppszMime = NULL;			// a pointer to an array of zero terminated strings with mime associations (last char* is NULL)
	m_ulStatus = HTTP_UNINIT;  //unintialized
	if(pszRootPath)
	{
		m_pszRoot = (char*)malloc(strlen(pszRootPath)+1);			// the path to the www root dir
	} else m_pszRoot=NULL;
	if(m_pszRoot)
	{ 
		strcpy(m_pszRoot, pszRootPath);			// the path to the www root dir
		m_ulStatus = HTTP_GOTROOT;
	}
}

CHTTP10::~CHTTP10()
{
	if(m_pSecure) delete m_pSecure; // must use new to allocate.
	if(m_pszRoot) free(m_pszRoot);  // must use malloc to allocate
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_ppszErrors)
	{
		unsigned long i=0;
		while(m_ppszErrors[i]!=NULL)
		{
			free(m_ppszErrors[i]);  // must use malloc to allocate
			i++;
		}
	}
	if(m_ppszMime)
	{
		unsigned long i=0;
		while(m_ppszMime[i]!=NULL)
		{
			free(m_ppszMime[i]);  // must use malloc to allocate
			i++;
		}
	}
}

int  CHTTP10::InitializeMessaging(void* pmsgr)
{
	m_net.InitializeMessaging(pmsgr);
	return CMessagingObject::InitializeMessaging(pmsgr);  // call the base class
}


int		CHTTP10::SetRoot(char* pszRootPath)
{
	if(m_pszRoot)
	{
		free(m_pszRoot);  // wipe the old.
		m_ulStatus &= ~HTTP_GOTROOT;
	}
	if(pszRootPath)
	{
		m_pszRoot = (char*)malloc(strlen(pszRootPath)+1);			// the path to the www root dir
	}
	else 
	{
		m_pszRoot=NULL;
		return HTTP_ERROR;
	}
	if(m_pszRoot)
	{ 
		strcpy(m_pszRoot, pszRootPath);			// the path to the www root dir
		m_ulStatus |= HTTP_GOTROOT;
		return HTTP_SUCCESS;
	}
	return HTTP_ERROR;
}

// the messaging object can be any that has had its m_lpfnDM member initialized, or it can be NULL if no messages are desired
int		CHTTP10::InitServer(unsigned short usPort, void* pHandlerThread, void* pParserObject, void* pMessagingObject, char* pszInfo)  // init the server to listen on a port - can be called repeatedly for mutiple port listening
{
	if(!(	m_ulStatus & (HTTP_GOTHOST|HTTP_GOTROOT))) 
	{
		if(pszInfo) strcpy(pszInfo, "InitServer: Both host name and root path must be initialized before starting server.");
		return HTTP_ERROR;
	}

	CNetServer* pServer = new CNetServer;
	if(pServer)
	{
		pServer->m_usPort = usPort;  // port on which the server listens
		pServer->m_ucType = NET_TYPE_PROTOCOL_HTTP;						// defined type - indicates which protocol to use, structure of data
		pServer->m_pszName = NULL;  //(dont need)					// name of the server, for human readability
		pServer->m_pszStatus = (char*)malloc(NET_ERRORSTRING_LEN);				// status buffer with error messages from thread
		pServer->m_lpfnHandler = (pHandlerThread==NULL)?HTTP10HandlerThread:pHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = (pParserObject==NULL)?this:pParserObject;					// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = pMessagingObject;					// pointer to the object with the Message function.

		int nReturn = m_net.StartServer(pServer, pMessagingObject, 5000, pszInfo);
		if(nReturn>=NET_SUCCESS) m_ulStatus|=HTTP_LISTEN;
		return nReturn;
	}
	return HTTP_ERROR;
}

int		CHTTP10::EndServer()   // stop all listeners
{
	if(m_net.m_ppNetServers!=NULL)
	{
		unsigned short usIndex=0;
		while(usIndex<m_net.m_usNumServers)
		{
			if(m_net.m_ppNetServers[usIndex])
			{
				m_net.StopServer(m_net.m_ppNetServers[usIndex]->m_usPort);
			}
			usIndex++;
		}
	}
	m_ulStatus &= ~HTTP_LISTEN;
	return HTTP_SUCCESS;  // unconditional success...
}


int		CHTTP10::ParseHeader(unsigned char* pucBuffer, unsigned long ulBufferLength, CHTTPHeader* pHeader)
{
	if((pucBuffer==NULL)||(pHeader==NULL)||(!(m_ulStatus&HTTP_GOTROOT))) 	return HTTP_ERROR;
	char* pch;

	// expecting requests of this format:
	// GET /path/file.html HTTP/1.0<CRLF>													(mandatory)
	// From: someuser@domain.com<CRLF>													(optional header)
	// User-Agent: HTTPTool/1.0<CRLF>															(optional header)
	// Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==<CRLF>		(optional header, required for basic auth)
	// <CRLF>

	CSafeBufferUtil sbu;

	// first do the first line.
	pch = sbu.Token((char*)pucBuffer, ulBufferLength, " \t");  //whitespace
	if (pch==NULL) 	return HTTP_ERROR; 

	if(strnicmp(pch, "GET", 3)==0) pHeader->m_ucMethod = HTTP_GET;
	else
	if(strnicmp(pch, "HEAD", 4)==0) pHeader->m_ucMethod = HTTP_HEAD;
	else
	if(strnicmp(pch, "POST", 4)==0) pHeader->m_ucMethod = HTTP_POST;
	else
	if(strnicmp(pch, "PUT", 3)==0) pHeader->m_ucMethod = HTTP_PUT;
	else
	if(strnicmp(pch, "DELETE", 6)==0) pHeader->m_ucMethod = HTTP_DELETE;
	else
	if(strnicmp(pch, "OPTIONS", 7)==0) pHeader->m_ucMethod = HTTP_OPTIONS;
	else
	if(strnicmp(pch, "TRACE", 5)==0) pHeader->m_ucMethod = HTTP_TRACE;
	else
		pHeader->m_ucMethod = HTTP_UNDEF;


	pch = sbu.Token(NULL, 0, " \t");
	if (pch==NULL) 	return HTTP_ERROR; 

	// have to split the URL in case there's cgi args.
//	char* pchBuffer = pch; // have to save it to free it (actually dont need to free it here, happens outside)
	URLDecode(&pch, &(pHeader->m_ppszArgNames), &(pHeader->m_ppszArgValues), &(pHeader->m_ulNumArgs));

	if(pHeader->m_pszURL) free(pHeader->m_pszURL);
	pHeader->m_pszURL = (char*)malloc(strlen(pch)+1);
	if(pHeader->m_pszURL) strcpy(pHeader->m_pszURL, pch);

	if(pHeader->m_pszPath) free(pHeader->m_pszPath);
	pHeader->m_pszPath = (char*)malloc(MAX_PATH+1);
	if(pHeader->m_pszPath)
	{
		_snprintf(pHeader->m_pszPath, MAX_PATH, "%s", m_pszRoot );
		strncat(pHeader->m_pszPath, pch, MAX_PATH);
		_fullpath(pHeader->m_pszPath, pHeader->m_pszPath, MAX_PATH);  // in case of relative references
		char ext[_MAX_EXT];
		_splitpath(pHeader->m_pszPath, NULL, NULL, NULL, ext);
		pHeader->m_pszMimeType =  MimeType(ext);  // get the mime type.
	}


	pch = sbu.Token(NULL, 0, "\r\n");  //crlf
	if (pch==NULL) 	return HTTP_ERROR; 

	if(pHeader->m_pszVersion) free(pHeader->m_pszVersion);
	pHeader->m_pszVersion = (char*)malloc(strlen(pch)+1);
	if(pHeader->m_pszVersion) strcpy(pHeader->m_pszVersion, pch);



	// after this point, we require only non essentials, except possibly for authorization.
	// on to other headers.

	if(pHeader->m_pszHeaders) free(pHeader->m_pszHeaders);
	pHeader->m_pszHeaders = NULL;

	
	pch = sbu.Token(NULL, 0, "\r\n");  //crlf

	bool bFoundAuth = false;  //only search for one set
	while(pch!=NULL)
	{
		bool bAuthNow = false;  //dont add to headers if its auth
		if(!bFoundAuth)
		{
			if(strnicmp(pch, "authorization", 13)==0)
			{
				// this is an auth field, have to check for insensitive case
				char* pchMark=strchr(pch, 'b');
				if(pchMark)
				{
					if((strnicmp(pchMark, "basic", 5)==0)&&(strlen(pchMark)>6))
					{
						bFoundAuth = true;
						bAuthNow = true;
					}
				}
				if(!bFoundAuth)
				{
					pchMark=strchr(pch, 'B');
					if(pchMark)
					{
						if((strnicmp(pchMark, "basic", 5)==0)&&(strlen(pchMark)>6))
						{
							bFoundAuth = true;
							bAuthNow = true;
						}
					}
				}
				if(bFoundAuth)
				{

					pchMark+=5;
					unsigned long ulLen=0;
					while( isspace(*pchMark)) pchMark++; // eat up whitespace
					char* pchStart = pchMark;
					while(!(isspace(*pchMark))) { pchMark++; ulLen++;}
					// parse the base 64 stuff
					CBufferUtil bu;
					if (bu.Base64Decode(&pchStart, &ulLen, false)==RETURN_SUCCESS)
					{
						//pchStart should now be a buffer full of something of the format
						// "Aladdin:open sesame"  where userid = "Aladdin" and password = "open sesame"
						char* pchTemp = strchr(pchStart, ':');
						if(pchTemp!=NULL)
						{
							int nNewLen = (pchTemp-pchStart);
							if(pHeader->m_pszUser) free(pHeader->m_pszUser);
							pHeader->m_pszUser = (char*)malloc(nNewLen+1);
							if(pHeader->m_pszUser)
							{
								strncpy(pHeader->m_pszUser, pchStart, nNewLen);
								*((pHeader->m_pszUser)+nNewLen) = 0; // zero terminate
							}

							if(pHeader->m_pszPassword) free(pHeader->m_pszPassword);
							pHeader->m_pszPassword = (char*)malloc(ulLen-(nNewLen));
							if(pHeader->m_pszPassword)
							{
								strncpy(pHeader->m_pszPassword, pchTemp+1, ulLen-nNewLen-1);
								*((pHeader->m_pszPassword)+ulLen-nNewLen-1) = 0; // zero terminate
							}
						}
						else
						{
							bFoundAuth = false;  // because of error
							bAuthNow = false;  // because of error
						}
					}
					else
					{
						bFoundAuth = false;  // because of error
						bAuthNow = false;  // because of error
					}
				}
			}
		}

		if(!bAuthNow)  // add to general headers.
		{
			char* pszHeader = NULL;
			unsigned long ulMarker = 0;

			if(pHeader->m_pszHeaders)
			{
				ulMarker = strlen(pHeader->m_pszHeaders);
			}

			pszHeader = (char*)malloc(ulMarker+strlen(pch)+3);
			if(pszHeader)
			{
				*pszHeader = 0;
				if(pHeader->m_pszHeaders)
				{
					memcpy(pszHeader, pHeader->m_pszHeaders, ulMarker);
					free(pHeader->m_pszHeaders);
				}
				(pHeader->m_pszHeaders) = pszHeader;
				memcpy((pHeader->m_pszHeaders)+ulMarker, pch, strlen(pch));
				char crlf[3] = { 13,10,0 };
				memcpy((pHeader->m_pszHeaders)+ulMarker+strlen(pch), crlf, 2);
				*((pHeader->m_pszHeaders)+ulMarker+strlen(pch)+2) = 0;//term
			}
		}

		pch = sbu.Token(NULL, 0, "\r\n");  //crlf
	}

	return HTTP_SUCCESS;
}

int		CHTTP10::RegisterMimeType(char* pszExt, char* pszType)  //adds mime types, error checks each one
{
	return HTTP_ERROR;
}

int		CHTTP10::LoadMimeBuffer(char* pszBuffer)  // overwrites the whole buffer
{
	return HTTP_ERROR;
}

//returns a string such as "text/html" if a file extension of "htm" is passed in (for instance)
char*	CHTTP10::MimeType(char* pszExt)
{
	if((!(m_ulStatus&HTTP_GOTMIME))||(pszExt==NULL)||(strlen(pszExt)<=0)) return NULL;
	char* pch = NULL;
	return pch;
}

int		CHTTP10::RegisterError(unsigned short ucCode, char* pszReason, char* pszFileName) //adds template files, error checks filenames
{
	return HTTP_ERROR;
}

int		CHTTP10::LoadErrorBuffer(char* pszBuffer)  // overwrites the whole buffer
{
	return HTTP_ERROR;
}

int		CHTTP10::GetError(unsigned short ucCode, char** ppszReason, char** ppszFileName) //adds template files, error checks filenames
{
	return HTTP_ERROR;
}

char*	CHTTP10::ErrorBuffer(CHTTPHeader* pHeader, unsigned short ucCode)
{
	if((pHeader==NULL)||(m_pszHost==NULL)||(!(m_ulStatus&HTTP_GOTHOST))) return NULL;

	char* pch = NULL;
	if(m_ppszErrors==NULL)
	{
		//just give a standard and simple error page.

		unsigned long ulFileLen = 
			strlen("<html><body>Error: </body></html>");
		unsigned long ulBufferLen = 
			strlen("HTTP/1.0 \r\nContent-Type: text/html\r\nContent-Length: %ld\r\n\r\n<html><body>Error: </body></html>");


		// for 401, the "WWW-Authenticate: Basic" is necessary to get a dialog box. 
		// one could add a 'realm="RealmName"' as well, however it isnt necessary.
		// at any rate, this can be passed in with one of the pages or external error text strings
		switch(ucCode)
		{
		case 400: ulFileLen += strlen("400 Bad request."); ulBufferLen += 2*strlen("400 Bad request."); break;
		case 401: ulFileLen += strlen("401 Unauthorized.WWW-Authenticate: Basic\r\n"); ulBufferLen += 2*strlen("401 Unauthorized.WWW-Authenticate: Basic\r\n"); break;
		case 403: ulFileLen += strlen("403 Forbidden."); ulBufferLen += 2*strlen("403 Forbidden."); break;
		case 404: ulFileLen += strlen("404 Object not found."); ulBufferLen += 2*strlen("404 Object not found."); break;
		case 500: ulFileLen += strlen("500 Internal server error."); ulBufferLen += 2*strlen("500 Internal server error."); break;
		case 501: ulFileLen += strlen("501 Not implemented."); ulBufferLen += 2*strlen("501 Not implemented."); break;
		case 505: ulFileLen += strlen("505 HTTP version not supported."); ulBufferLen += 2*strlen("505 HTTP version not supported."); break;
		default: ulFileLen += 3; ulBufferLen += 6; break;
		}

		char length[32]; 
		sprintf(length, "%ld", ulFileLen);
		ulBufferLen += (strlen(length));

		pch = (char*)malloc(ulBufferLen+1);  //term zero

		if(pch)
		{
			strcpy(pch, "HTTP/1.0 ");
			switch(ucCode)
			{
			case 400: strcat(pch, "400 Bad request."); break;
//			case 401: strcat(pch, "401 Unauthorized.\r\nWWW-Authenticate: Basic realm=\"Global\""); break;
			case 401: strcat(pch, "401 Unauthorized.\r\nWWW-Authenticate: Basic"); break;
			case 403: strcat(pch, "403 Forbidden."); break;
			case 404: strcat(pch, "404 Object not found."); break;
			case 500: strcat(pch, "500 Internal server error."); break;
			case 501: strcat(pch, "501 Not implemented."); break;
			case 505: strcat(pch, "505 HTTP version not supported."); break;
			default: char code[8]; sprintf(code,"%d", ucCode); code[3]=0; strcat(pch, code); break;
			}

				
			strcat(pch, "\r\nContent-Type: text/html\r\nContent-Length: ");
			strcat(pch, length);
			strcat(pch, "\r\n\r\n<html><body>Error: ");
			switch(ucCode)
			{
			case 400: strcat(pch, "400 Bad request."); break;
			case 401: strcat(pch, "401 Unauthorized."); break;
			case 403: strcat(pch, "403 Forbidden."); break;
			case 404: strcat(pch, "404 Object not found."); break;
			case 500: strcat(pch, "500 Internal server error."); break;
			case 501: strcat(pch, "501 Not implemented."); break;
			case 505: strcat(pch, "505 HTTP version not supported."); break;
			default: char code[8]; sprintf(code,"%d", ucCode);  code[3]=0; strcat(pch, code); break;
			}
			strcat(pch, "</body></html>");
			*(pch+ulBufferLen) = 0; //zero term
		}
	}
	else
	{
		// either send out a different message, 
		// or send a template file out, if desired.
	}
	return pch;
}

char*	CHTTP10::RedirectBuffer(CHTTPHeader* pHeader)
{
	if((pHeader==NULL)||(m_pszHost==NULL)||(!(m_ulStatus&HTTP_GOTHOST))) return NULL;

	char* pch = NULL;
	if(m_ppszErrors==NULL)
	{
		//just give a standard and simple redirect.
		unsigned long ulFileLen = 
			strlen("<html><body><a href=\"http://index.htm\">http://index.htm</a></body></html>")
			+ strlen(pHeader->m_pszURL)*2
			+ strlen(m_pszHost)*2;

		unsigned long ulBufferLen = 
			strlen("HTTP/1.0 301\r\nContent-Type: text/html\r\nContent-Length: \r\nLocation: http://index.htm\r\n\r\n<html><body><a href=\"http://index.htm\">http://index.htm</a></body></html>")
			+ strlen(pHeader->m_pszURL)*3
			+ strlen(m_pszHost)*3;

		bool bAppend = false;
		if( *((pHeader->m_pszURL)+(strlen(pHeader->m_pszURL)-1)) != '/' )
		{
			ulBufferLen+=3;
			ulFileLen+=2;
			bAppend = true;
		}

		char length[32]; 
		sprintf(length, "%ld", ulFileLen);
		ulBufferLen += (strlen(length));

		pch = (char*)malloc(ulBufferLen+1);  //term zero
		if(pch)
		{
			strcpy(pch, "HTTP/1.0 301\r\nContent-Type: text/html\r\nContent-Length: ");
			strcat(pch, length);
			strcat(pch, "\r\nLocation: http://");
			strcat(pch, m_pszHost);
			strcat(pch, pHeader->m_pszURL);
			if(bAppend) strcat(pch, "/");
			strcat(pch, "index.htm\r\n\r\n<html><body><a href=\"http://");
			strcat(pch, m_pszHost);
			strcat(pch, pHeader->m_pszURL);
			if(bAppend) strcat(pch, "/");
			strcat(pch, "index.htm\">http://");
			strcat(pch, m_pszHost);
			strcat(pch, pHeader->m_pszURL);
			if(bAppend) strcat(pch, "/");
			strcat(pch, "index.htm</a></body></html>");
			*(pch+ulBufferLen) = 0; //zero term
		}
	}
	else
	{
		// either send out a different message, 
		// or send a template file out, if desired.
	}
	return pch;
}


// utility
int	CHTTP10::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						m_ulStatus |= HTTP_SUSPEND;
						if(m_pszHost) free(m_pszHost);
						m_pszHost=pch;
						m_ulStatus |= HTTP_GOTHOST;
						m_ulStatus &= ~HTTP_SUSPEND;
						return HTTP_SUCCESS;
					}
				}
			}
		}
	}
	return HTTP_ERROR;
}

int	CHTTP10::CheckSafe(char ch)
{
	if((ch=='.')||(ch=='/')||(ch=='\\')) return HTTP_PATHSAFE;
	if(ch<46) return HTTP_UNSAFE;
	if((ch>57)&&(ch<65)) return HTTP_UNSAFE;
	if((ch>90)&&(ch<97)) return HTTP_UNSAFE;
	if(ch>122) return HTTP_UNSAFE;
	return HTTP_SAFE|HTTP_PATHSAFE;
}

// this URL encoder encodes the space char (0x20) instead of replacing with pluses.
char*	CHTTP10::URLEncode(char* pszPath, char** ppszNames, char** ppszValues, unsigned long ulNumPairs)
{
	if((pszPath==NULL)||(((ppszNames==NULL)||(ppszValues==NULL))&&(ulNumPairs>0))) return NULL;
	unsigned long ulBufferSize = (strlen(pszPath))*3 + 1;  // *3 means we are able to encode every single character in path if nec + trailing ?
	char* pch = (char*) malloc(ulBufferSize+1);  // term zero
	char encoded[3];
	if(pch)
	{
		char* pchMark = pch;
		// first encode the path.
		int nLen = strlen(pszPath);
		for(int i=0; i<nLen; i++)
		{
			if(CheckSafe(*(pszPath+i)) & HTTP_PATHSAFE)
				*pchMark++ = *(pszPath+i);
			else // have to encode
			{
				sprintf(encoded, "%02x", (unsigned char)(*(pszPath+i)) );  // convert to unsigned to avoid negative numbers
				*pchMark++ = '%';
				*pchMark++ = encoded[0];
				*pchMark++ = encoded[1];
			}
		}

		if(ulNumPairs>0)
		{
			*pchMark++ = '?';

			bool bInitial = true;
			for(unsigned long q=0; q<ulNumPairs; q++)
			{
				if((ppszNames[q])&&(strlen(ppszNames[q]))&&(ppszValues[q]))
				{
					char* pchPair = (char*)malloc((strlen(ppszNames[q])+strlen(ppszNames[q]))*3+2);
					if(pchPair)
					{
						if(bInitial) *pchMark++ = '&';  // not q=0 because maybe a null pointer caused skipped q=0
						bInitial = false;
						char* pchTempMark = pchPair;
						nLen = strlen(ppszNames[q]);
						for(int i=0; i<nLen; i++)
						{
							if(CheckSafe(*(ppszNames[q]+i)) & HTTP_SAFE)
								*pchTempMark++ = *(ppszNames[q]+i);
							else // have to encode
							{
								sprintf(encoded, "%02x", (unsigned char)(*(ppszNames[q]+i)) );  // convert to unsigned to avoid negative numbers
								*pchTempMark++ = '%';
								*pchTempMark++ = encoded[0];
								*pchTempMark++ = encoded[1];
							}
						}
						*pchTempMark++ = '=';

						nLen = strlen(ppszValues[q]);
						for(i=0; i<nLen; i++)
						{
							if(CheckSafe(*(ppszValues[q]+i)) & HTTP_SAFE)
								*pchTempMark++ = *(ppszValues[q]+i);
							else // have to encode
							{
								sprintf(encoded, "%02x", (unsigned char)(*(ppszValues[q]+i)) );  // convert to unsigned to avoid negative numbers
								*pchTempMark++ = '%';
								*pchTempMark++ = encoded[0];
								*pchTempMark++ = encoded[1];
							}
						}
						*pchTempMark = 0;  // zero terminate

						// now add it
						if( (strlen(pchPair)+(pchMark-pch)) > (ulBufferSize) )
						{
							//have to allocate a bigger buffer;
							char* pchTemp = (char*) malloc(ulBufferSize+strlen(pchPair)+1);  // term zero
							if(pchTemp)
							{

								unsigned long ulMarker = pchMark-pch;
								memcpy(pchTemp, pch, ulMarker);
								free(pch);
								pch = pchTemp;
								pchMark = pch+ulMarker;

								memcpy(pchMark, pchPair, strlen(pchPair)); 
								pchMark += strlen(pchPair);
								ulBufferSize += strlen(pchPair);
							}
						}
						free(pchPair);
					}
				}
			}
		}
		*pchMark = 0;

		// lets free any unused memory by returning a buffer exactly the right size
		char* pchReturn = (char*)malloc(strlen(pch)+1);
		if(pchReturn)
		{
			strcpy(pchReturn, pch);
			free(pch);
			pch = pchReturn;
		}
	}
	return pch;

}

// an encoded path is passed in in the ppszPath pointer, that buffer is destroyed, on success only.
// The value is then reset to point to a new buffer with the decoded path.
int CHTTP10::URLDecode(char** ppszPath, char*** pppszNames, char*** pppszValues, unsigned long* pulNumPairs)
{
	if((ppszPath==NULL)||(*ppszPath==NULL)||(strlen(*ppszPath)==0)
		||(pppszNames==NULL)||(pppszValues==NULL)||(pulNumPairs==NULL)) return HTTP_ERROR;
	char* pch;
	char* pszBuffer = *ppszPath;
	char* pszTemp = NULL;
	char* pszLast;// should be a pointer to the term zero. safety pointer, do not exceed this.
	unsigned long ulNumPairs = 0;
	CBufferUtil bu;
	// first determine if there are arguments.
	pch = strchr(pszBuffer, '?');
	if((pch!=NULL)&&(strlen(pch+1)))
	{
		ulNumPairs = bu.CountChar(pch+1, strlen(pch+1), '=');  // this tells us how many pairs.
		pszTemp = (char*)malloc((pch-pszBuffer)+1);
		if(pszTemp==NULL) return HTTP_ERROR;
		memcpy(pszTemp, pszBuffer, (pch-pszBuffer));
		*(pszTemp+(pch-pszBuffer))=0;  // zero term
	}
	else
	{
		pszTemp = (char*)malloc(strlen(pszBuffer)+1);
		if(pszTemp==NULL) return HTTP_ERROR;
		strcpy(pszTemp, pszBuffer);
	}

	//decode the path first
	//replace pluses with spaces;
	unsigned long ulLen = strlen(pszTemp);
	bu.ReplaceChar(pszTemp, ulLen, '+', ' ');
	// now, decode hex codes.
	char* pchMark = pszTemp;
	unsigned long i=0; 
	pszLast = pszTemp+strlen(pszTemp);
	while((i<ulLen)&&(pchMark<pszLast))
	{
		if((*(pszTemp+i))=='%')
		{// hex code to decode
			i++;
			if(i<ulLen)
			{
				switch(*(pszTemp+i))
				{
				case '0':	*pchMark = (char)0; i++; break;
				case '1':	*pchMark = (char)16; i++; break;
				case '2':	*pchMark = (char)32; i++; break;
				case '3':	*pchMark = (char)48; i++; break;
				case '4':	*pchMark = (char)64; i++; break;
				case '5':	*pchMark = (char)80; i++; break;
				case '6':	*pchMark = (char)96; i++; break;
				case '7':	*pchMark = (char)112; i++; break;
				case '8':	*pchMark = (char)128; i++; break;
				case '9':	*pchMark = (char)144; i++; break;
				case 'a':
				case 'A':	*pchMark = (char)160; i++; break;
				case 'b':
				case 'B':	*pchMark = (char)176; i++; break;
				case 'c':
				case 'C':	*pchMark = (char)192; i++; break;
				case 'd':
				case 'D':	*pchMark = (char)208; i++; break;
				case 'e':
				case 'E':	*pchMark = (char)224; i++; break;
				case 'f':
				case 'F':	*pchMark = (char)240; i++; break;
				default:   break; //not a hex char, error, but continue
				}
			}
			if(i<ulLen)
			{
				switch(*(pszTemp+i))
				{
				case '0': i++; break;
				case '1':	*pchMark += 1; i++; break;
				case '2':	*pchMark += 2; i++; break;
				case '3':	*pchMark += 3; i++; break;
				case '4':	*pchMark += 4; i++; break;
				case '5':	*pchMark += 5; i++; break;
				case '6':	*pchMark += 6; i++; break;
				case '7':	*pchMark += 7; i++; break;
				case '8':	*pchMark += 8; i++; break;
				case '9':	*pchMark += 9; i++; break;
				case 'a':
				case 'A':	*pchMark += 10; i++; break;
				case 'b':
				case 'B':	*pchMark += 11; i++; break;
				case 'c':
				case 'C':	*pchMark += 12; i++; break;
				case 'd':
				case 'D':	*pchMark += 13; i++; break;
				case 'e':
				case 'E':	*pchMark += 14; i++; break;
				case 'f':
				case 'F':	*pchMark += 15; i++; break;
				default:	break;//not a hex char, error, but continue
				}
			}
		}
		else
		{
			*pchMark = (*(pszTemp+i));
			i++;
		}
		pchMark++;
	}
	*pchMark=0;  // zero term the path

	*ppszPath = pszTemp;  // we still have the original buffer in pszBuffer.
	if(ulNumPairs>0)
	{
		char** ppszNames = (char**)malloc(sizeof(char*)*(ulNumPairs+1));
		if(ppszNames==NULL) return HTTP_ERROR;
		char** ppszValues = (char**)malloc(sizeof(char*)*(ulNumPairs+1));
		if(ppszValues==NULL) return HTTP_ERROR;

		CSafeBufferUtil sbu;

		unsigned long j=0; 
		
		char* pchPair = sbu.Token(pch+1, strlen(pch+1), "=");
		while((j<ulNumPairs)&&(pchPair))
		{
			ppszNames[j] = NULL;
			ppszValues[j] = NULL;

			if(pchPair)
			{
				ulLen = strlen(pchPair);
				pszTemp = (char*)malloc(strlen(pchPair)+1);
				if(pszTemp)
				{
					strcpy(pszTemp, pchPair);
					bu.ReplaceChar(pszTemp, ulLen, '+', ' ');
					// now, decode hex codes.
					pchMark = pszTemp;
					i=0; 
					while(i<ulLen)
					{
						if((*(pszTemp+i))=='%')
						{// hex code to decode
							i++;
							if(i<ulLen)
							{
								switch(*(pszTemp+i))
								{
								case '0':	*pchMark = (char)0; i++; break;
								case '1':	*pchMark = (char)16; i++; break;
								case '2':	*pchMark = (char)32; i++; break;
								case '3':	*pchMark = (char)48; i++; break;
								case '4':	*pchMark = (char)64; i++; break;
								case '5':	*pchMark = (char)80; i++; break;
								case '6':	*pchMark = (char)96; i++; break;
								case '7':	*pchMark = (char)112; i++; break;
								case '8':	*pchMark = (char)128; i++; break;
								case '9':	*pchMark = (char)144; i++; break;
								case 'a':
								case 'A':	*pchMark = (char)160; i++; break;
								case 'b':
								case 'B':	*pchMark = (char)176; i++; break;
								case 'c':
								case 'C':	*pchMark = (char)192; i++; break;
								case 'd':
								case 'D':	*pchMark = (char)208; i++; break;
								case 'e':
								case 'E':	*pchMark = (char)224; i++; break;
								case 'f':
								case 'F':	*pchMark = (char)240; i++; break;
								default:   break; //not a hex char, error, but continue
								}
							}
							if(i<ulLen)
							{
								switch(*(pszTemp+i))
								{
								case '0': i++; break;
								case '1':	*pchMark += 1; i++; break;
								case '2':	*pchMark += 2; i++; break;
								case '3':	*pchMark += 3; i++; break;
								case '4':	*pchMark += 4; i++; break;
								case '5':	*pchMark += 5; i++; break;
								case '6':	*pchMark += 6; i++; break;
								case '7':	*pchMark += 7; i++; break;
								case '8':	*pchMark += 8; i++; break;
								case '9':	*pchMark += 9; i++; break;
								case 'a':
								case 'A':	*pchMark += 10; i++; break;
								case 'b':
								case 'B':	*pchMark += 11; i++; break;
								case 'c':
								case 'C':	*pchMark += 12; i++; break;
								case 'd':
								case 'D':	*pchMark += 13; i++; break;
								case 'e':
								case 'E':	*pchMark += 14; i++; break;
								case 'f':
								case 'F':	*pchMark += 15; i++; break;
								default:	break;//not a hex char, error, but continue
								}
							}
						}
						else
						{
							*pchMark = (*(pszTemp+i));
							i++;
						}
						pchMark++;
					}
					*pchMark=0;  // zero term the path

				}
				ppszNames[j] = pszTemp;
	
				pchPair = sbu.Token(NULL, 0, "&");

				if(pchPair)
				{
					ulLen = strlen(pchPair);
					pszTemp = (char*)malloc(strlen(pchPair)+1);
					if(pszTemp)
					{
						strcpy(pszTemp, pchPair);
						bu.ReplaceChar(pszTemp, ulLen, '+', ' ');
						// now, decode hex codes.
						pchMark = pszTemp;
						i=0; 
						while(i<ulLen)
						{
							if((*(pszTemp+i))=='%')
							{// hex code to decode
								i++;
								if(i<ulLen)
								{
									switch(*(pszTemp+i))
									{
									case '0':	*pchMark = (char)0; i++; break;
									case '1':	*pchMark = (char)16; i++; break;
									case '2':	*pchMark = (char)32; i++; break;
									case '3':	*pchMark = (char)48; i++; break;
									case '4':	*pchMark = (char)64; i++; break;
									case '5':	*pchMark = (char)80; i++; break;
									case '6':	*pchMark = (char)96; i++; break;
									case '7':	*pchMark = (char)112; i++; break;
									case '8':	*pchMark = (char)128; i++; break;
									case '9':	*pchMark = (char)144; i++; break;
									case 'a':
									case 'A':	*pchMark = (char)160; i++; break;
									case 'b':
									case 'B':	*pchMark = (char)176; i++; break;
									case 'c':
									case 'C':	*pchMark = (char)192; i++; break;
									case 'd':
									case 'D':	*pchMark = (char)208; i++; break;
									case 'e':
									case 'E':	*pchMark = (char)224; i++; break;
									case 'f':
									case 'F':	*pchMark = (char)240; i++; break;
									default:   break; //not a hex char, error, but continue
									}
								}
								if(i<ulLen)
								{
									switch(*(pszTemp+i))
									{
									case '0': i++; break;
									case '1':	*pchMark += 1; i++; break;
									case '2':	*pchMark += 2; i++; break;
									case '3':	*pchMark += 3; i++; break;
									case '4':	*pchMark += 4; i++; break;
									case '5':	*pchMark += 5; i++; break;
									case '6':	*pchMark += 6; i++; break;
									case '7':	*pchMark += 7; i++; break;
									case '8':	*pchMark += 8; i++; break;
									case '9':	*pchMark += 9; i++; break;
									case 'a':
									case 'A':	*pchMark += 10; i++; break;
									case 'b':
									case 'B':	*pchMark += 11; i++; break;
									case 'c':
									case 'C':	*pchMark += 12; i++; break;
									case 'd':
									case 'D':	*pchMark += 13; i++; break;
									case 'e':
									case 'E':	*pchMark += 14; i++; break;
									case 'f':
									case 'F':	*pchMark += 15; i++; break;
									default:	break;//not a hex char, error, but continue
									}
								}
							}
							else
							{
								*pchMark = (*(pszTemp+i));
								i++;
							}
							pchMark++;
						}
						*pchMark=0;  // zero term the path
					}
					ppszValues[j] = pszTemp;
				}
			}
			j++;
			pchPair = sbu.Token(NULL, 0, "=");
		}

		ppszNames[j] = NULL;
		ppszValues[j] = NULL;


		*pppszNames = ppszNames;
		*pppszValues = ppszValues;

	}
	else
	{
		*pppszNames = NULL;
		*pppszValues = NULL;
	}

	// dont free, do it maually outside, in case more to be checked on buffer.
	//free(pszBuffer);  // frees the original incoming buffer
	*pulNumPairs = ulNumPairs;

	return HTTP_SUCCESS;
}




// the following thread is provided as a prototype for server handler threads.
// objects of the users choice may be passed in via the pClient->m_lpObject pointer
// this sample thread accepts HTTP1.0 data and acts as a webserver.
// if security is desired, the user can pass in a credentials checking object in
// pClient->m_lpObject.  If dynamic HTML is desired, the object can contain a parser.
void HTTP10HandlerThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}




// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CHTTP10* phttp = (CHTTP10*)(pClient->m_lpObject) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
*/
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
*/
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

*/
				if((header.m_pszUser==NULL)||(header.m_pszPassword==NULL))
//					||(stricmp(header.m_pszUser, "Aladdin"))||(strcmp(header.m_pszPassword, "zoidberg")))  // stupid sample values - this webserver is for only one user!
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if (strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								//  ||(	other security check here, as in credentials. - 401 return)
							{
								DWORD dwAttrib;

								dwAttrib = GetFileAttributes((LPCTSTR)header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // iff error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: Aurora/1.01dev" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: Aurora/1.01dev\r\nContent-Length: \r\n\r\n");

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assmebles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);

										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												strcpy((char*)pch, "HTTP/1.0 200 OK\r\nServer: Aurora/1.01dev");
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												strcpy((char*)pch, "HTTP/1.0 200 OK\r\nServer: Aurora/1.01dev");
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

*/
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

