// HTTP10.h: interface for the CHTTP10 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HTTP10_H__5510A683_01A4_43DB_9A20_58DBBDA52BCF__INCLUDED_)
#define AFX_HTTP10_H__5510A683_01A4_43DB_9A20_58DBBDA52BCF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "../LAN/NetUtil.h"  // includes CMessagingObject
#include "../TXT/Security.h"

#define HTTP_UNDEF		0  // internal
#define HTTP_GET			1  // HTTP/1.0
#define HTTP_HEAD			2  // HTTP/1.0
#define HTTP_POST			3  // HTTP/1.0
#define HTTP_PUT			4  // HTTP/1.1
#define HTTP_DELETE		5  // HTTP/1.1
#define HTTP_OPTIONS  6  // HTTP/1.1
#define HTTP_TRACE	  7  // HTTP/1.1

#define HTTP_SUCCESS				0
#define HTTP_ERROR					-1

#define HTTP_UNINIT				0x00000000
#define HTTP_GOTHOST			0x00000001  // data initialized
#define HTTP_GOTROOT			0x00000002  // data initialized
#define HTTP_GOTMIME			0x00000004  // data initialized
#define HTTP_GOTERRORS		0x00000008  // data initialized
#define HTTP_GOTSECURE		0x00000010  // data initialized
#define HTTP_LISTEN				0x00000020  // listening
#define HTTP_SUSPEND			0x00000040  // service suspended

// char safety.  defined as non alphanumeric
#define HTTP_UNSAFE			0  // unsafe char
#define HTTP_PATHSAFE		1  // safe for path (allows "/" and "\")
#define HTTP_SAFE				2  // safe for names and values


// prototypes
void HTTP10HandlerThread(void* pvArgs);     // sample http/1.0 handler

class CHTTPHeader
{
public:
	CHTTPHeader();
	virtual ~CHTTPHeader();

	unsigned char m_ucMethod;  // enumerated values
	char* m_pszVersion;
	char* m_pszURL;
	char* m_pszPath;
	char* m_pszUser;
	char* m_pszPassword;
	char* m_pszHeaders;  // other headers
	char* m_pszMimeType;  // just a pointer to the canonical list
	char** m_ppszArgNames;
	char** m_ppszArgValues;
	unsigned long m_ulNumArgs;
};


class CHTTP10 : public CMessagingObject
{
public:
	CHTTP10(char* pszRootPath=NULL);
	virtual ~CHTTP10();

public:
	CNetUtil m_net;			// server object.  allows listening on several ports if necessary
	CSecurity* m_pSecure; // pointer to a security object, if desired.  if this is null, no security is used

	unsigned long m_ulStatus;

	char*	m_pszRoot;  // the path to the www root dir
	char*	m_pszHost;	// the name of the host, needed for redirects
	char**	m_ppszErrors; // a pointer to an array of zero terminated strings with formatted resaon strings and filenames of template error pages (optional).
	char**	m_ppszMime;   // a pointer to an array of zero terminated strings with mime associations (last char* is NULL)
	// the strings are loaded with a default on object creation, however can be freed and reloaded with
	// new settings from a settings file or other source, if desired.

	// core
	int		SetRoot(char* pszRootPath);
	int		InitServer(unsigned short usPort=80, void* pHandlerThread=NULL, void* pParserObject=NULL, void* pMessagingObject=NULL, char* pszInfo=NULL);  // init the server to listen on a port - can be called repeatedly for mutiple port listening
	int		EndServer();   // stop all listeners
	int		ParseHeader(unsigned char* pucBuffer, unsigned long ulBufferLength, CHTTPHeader* pHeader);
	int		RegisterMimeType(char* pszExt, char* pszType);  //adds mime types, error checks each one
	int		LoadMimeBuffer(char* pszBuffer);  // overwrites the whole buffer
	char*	MimeType(char* pszExt);
	int		RegisterError(unsigned short ucCode, char* pszReason, char* pszFileName);  //adds template files, error checks filenames
	int		LoadErrorBuffer(char* pszBuffer);  // overwrites the whole buffer
	int		GetError(unsigned short ucCode, char** ppszReason, char** ppszFileName);  //adds template files, error checks filenames
	char*	ErrorBuffer(CHTTPHeader* pHeader, unsigned short ucCode);
	char*	RedirectBuffer(CHTTPHeader* pHeader);

	// utility
	int		CheckSafe(char ch);
	int		GetHost();
	char*	URLEncode(char* pszPath, char** ppszNames, char** ppszValues, unsigned long ulNumPairs);
	int 	URLDecode(char** ppszPath, char*** pppszNames, char*** pppszValues, unsigned long* pulNumPairs);

	//override
	int  InitializeMessaging(void* pmsgr);

};

#endif // !defined(AFX_HTTP10_H__5510A683_01A4_43DB_9A20_58DBBDA52BCF__INCLUDED_)

// GET, HEAD, and POST are 1.0 methods.
// PUT, DELETE, OPTIONS, and TRACE are 1.1

/*
The status code is a three-digit integer, and the first digit identifies the general category of response:

    * 1xx indicates an informational message only
    * 2xx indicates success of some kind
    * 3xx redirects the client to another URL
    * 4xx indicates an error on the client's part
    * 5xx indicates an error on the server's part 
*/