// DirUtil.cpp: implementation of the CDirUtil and support class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DirUtil.h"
#include <process.h>
#include <sys/timeb.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void DirWatchThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirEntry::CDirEntry()
{
	m_szFullPath = NULL;
	m_szFilename = NULL;
	m_szDOSFilename = NULL;
	m_ulEntryTimestamp=0;
	m_ftFileModified.dwLowDateTime =0; 
  m_ftFileModified.dwHighDateTime=0;
	m_ulFlags=0;
	m_ulFileSizeHigh=0;
	m_ulFileSizeLow=0;
	m_ulFileAttribs=0;
}

CDirEntry::~CDirEntry()
{
	if(m_szFullPath) free(m_szFullPath);
	if(m_szFilename) free(m_szFilename);
	if(m_szDOSFilename) free(m_szDOSFilename);
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirUtil::CDirUtil()
{
	m_ulWatchIntervalMS = 30000;
	m_bWatchFolder = false;
	m_bUseSubDirs = true;
	m_pszWatchPath = NULL;
	m_pWatchThread = DirWatchThread;
	InitializeCriticalSection (&m_crit);
	InitializeCriticalSection (&m_critFileOp);

	m_ppDirChange = NULL;
	m_ulNumDirChanges = 0;
//	m_ulMaxNumDirChanges = 128;  //  number of changes we will allow to accumulate without being handled
	m_ppDirCanon = NULL;
	m_ulNumDirEntries = 0;

//	m_bUseSwapFile = true;
}

CDirUtil::~CDirUtil()
{
	if(m_pszWatchPath) free(m_pszWatchPath);
	m_bWatchFolder = false;
	EnterCriticalSection (&m_crit);
	if(m_ppDirChange)
	{
		unsigned long i = 0;
		for(i=0; i<m_ulNumDirChanges; i++)
		{
			delete m_ppDirChange[i];
		}
		delete [] m_ppDirChange;
	}

	if(m_ppDirCanon)
	{
		unsigned long i = 0;
		for(i=0; i<m_ulNumDirEntries; i++)
		{
			delete m_ppDirCanon[i];
		}
		delete [] m_ppDirCanon;
	}
	LeaveCriticalSection (&m_crit);

	DeleteCriticalSection (&m_crit);
	DeleteCriticalSection (&m_critFileOp);
}


int CDirUtil::SetWatchFolder(char* pszFullPath)
{
	if(m_bWatchFolder) return DIRUTIL_INPROGRESS;
	if((pszFullPath)&&(strlen(pszFullPath)))
	{
		// check to see if path exists, and is a dir.
		DWORD dwAttrib = GetFileAttributes(pszFullPath);
		if((dwAttrib == 0xffffffff)||(!(dwAttrib&FILE_ATTRIBUTE_DIRECTORY))) 
			return DIRUTIL_BADARGS;

		char* pch = (char*)malloc(strlen(pszFullPath)+1);
		if(pch)
		{
			sprintf(pch, "%s", pszFullPath);
			if(m_pszWatchPath) free(m_pszWatchPath);
			m_pszWatchPath = pch;

			return DIRUTIL_SUCCESS;
		} 
		return DIRUTIL_ERROR;
	}
	else
		return DIRUTIL_BADARGS;
}

int CDirUtil::BeginWatch(unsigned long ulWatchIntervalMS, bool bSubDirs)
{
	if(m_bWatchFolder) return DIRUTIL_INPROGRESS;
	if((m_pWatchThread)&&(m_pszWatchPath)&&(strlen(m_pszWatchPath)))
	{
		m_bUseSubDirs = bSubDirs;
		m_ulWatchIntervalMS = ulWatchIntervalMS;

		if(_beginthread( (void (__cdecl *)(void *))m_pWatchThread, 0, (void*)(this) ) != -1) 
		{
			return DIRUTIL_SUCCESS;
		}
	}
	return DIRUTIL_ERROR;
}

int CDirUtil::EndWatch()
{
	m_bWatchFolder = false;
	return DIRUTIL_SUCCESS;
}

// returns number of dir changes left in the queue BEFORE processing, 
// negative val if error.
// once a pointer to a CDirEntry is returned, the calling process must
// dispose of it by calling delete.  A successful call to this function
// removes the item from the queue.
int CDirUtil::GetDirChange(CDirEntry** ppDirChange)
{
	EnterCriticalSection (&m_crit);
	if(ppDirChange==NULL) return DIRUTIL_BADARGS;
	if((m_ppDirChange==NULL)||(m_ppDirChange[0]==NULL))
	{
		*ppDirChange = NULL;
		LeaveCriticalSection (&m_crit);
		return 0;
	}
	if(m_ulNumDirChanges>0)
	{
		int nRV = m_ulNumDirChanges;
		*ppDirChange = m_ppDirChange[0];
 
/*

	// debug file
			FILE* fp;
			fp = fopen("_gettinglog.log", "at");
			if(fp)
			{
				fprintf(fp, "0x%08x  0x%08x  0x%08x  removing %s\\%s\r\n", 
					m_ppDirChange[0],
					m_ppDirChange[0]->m_szFullPath,
					m_ppDirChange[0]->m_szFilename,
					m_ppDirChange[0]->m_szFullPath?m_ppDirChange[0]->m_szFullPath:"(null)",
					m_ppDirChange[0]->m_szFilename?m_ppDirChange[0]->m_szFilename:"(null)"
				);
				fflush(fp);
				fclose(fp);
			}
*/
 
		m_ulNumDirChanges--;
		CDirEntry** ppDirChangeArray;
		if(nRV>0)
		{
			ppDirChangeArray = new CDirEntry*[m_ulNumDirChanges];
		}
		else
		{
			// serve up the last one....
			ppDirChangeArray = NULL;
			delete [] m_ppDirChange;
			m_ppDirChange = NULL;
			LeaveCriticalSection (&m_crit);
			return nRV;
		}

		if(ppDirChangeArray)
		{
			for(unsigned long i=0; i<m_ulNumDirChanges; i++)
			{
				ppDirChangeArray[i] = m_ppDirChange[i+1];
			}
			delete [] m_ppDirChange;
			m_ppDirChange = ppDirChangeArray;
		}
		else  // could not alloc, so just shuffle
		{
			for(unsigned long i=0; i<m_ulNumDirChanges; i++)
			{
				m_ppDirChange[i] = m_ppDirChange[i+1];
			}
			m_ppDirChange[i] = NULL;
		}

		LeaveCriticalSection (&m_crit);
		return nRV;
	} 
	else
	{
		*ppDirChange = NULL;
		LeaveCriticalSection (&m_crit);
		return 0;
	}

}

int CDirUtil::AddChange(CDirEntry* pDirChange, bool bDelete)  // adds a change to change list and updates canon.
{
	if(
		  (pDirChange==NULL)
		||(pDirChange->m_szFullPath==NULL)
		||(pDirChange->m_szFilename==NULL)
		||(pDirChange->m_szDOSFilename==NULL)
		)
		return DIRUTIL_BADARGS;

	int nRV = DIRUTIL_SUCCESS;
	EnterCriticalSection (&m_crit);

	CDirEntry** ppDirChangeArray = new CDirEntry*[m_ulNumDirChanges+1];

	if(ppDirChangeArray)
	{
		unsigned long i=0;
		if(m_ppDirChange)
		{
			for(i=0; i<m_ulNumDirChanges; i++)
			{
				ppDirChangeArray[i] = m_ppDirChange[i];
			}
			delete [] m_ppDirChange;
		}
		m_ppDirChange = ppDirChangeArray;

	// add the entry to the changelog.
		CDirEntry* pde = new CDirEntry;

		if(pde)
		{
			pde->m_szFullPath =      (char*)malloc(strlen(pDirChange->m_szFullPath)+1);     // no filename
			if(pde->m_szFullPath)    strcpy(pde->m_szFullPath, pDirChange->m_szFullPath);
			pde->m_szFilename =      (char*)malloc(strlen(pDirChange->m_szFilename)+1);     // long filename
			if(pde->m_szFilename)    strcpy(pde->m_szFilename, pDirChange->m_szFilename);
			pde->m_szDOSFilename =   (char*)malloc(strlen(pDirChange->m_szDOSFilename)+1);  //8.3 format
			if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, pDirChange->m_szDOSFilename);
 
/*

	// debug file
			FILE* fp;
			fp = fopen("_addinglog.log", "at");
			if(fp)
			{
				fprintf(fp, "%d 0x%08x  0x%08x  0x%08x  adding %s\\%s\r\n",
					i, pde,
					pde->m_szFullPath,
					pde->m_szFilename,
					pde->m_szFullPath?pde->m_szFullPath:"(null)",
					pde->m_szFilename?pde->m_szFilename: "(null)"
				);
				fflush(fp);
				fclose(fp);
			}
*/			
			pde->m_ulEntryTimestamp = pDirChange->m_ulEntryTimestamp;			// unixtime, seconds resolution, last modified
			pde->m_ftFileModified   = pDirChange->m_ftFileModified;			  // FILETIME
			pde->m_ulFileSizeHigh   = pDirChange->m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
			pde->m_ulFileSizeLow    = pDirChange->m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
			pde->m_ulFileAttribs    = pDirChange->m_ulFileAttribs;		    // attribs returned by GetFileAttributes
			pde->m_ulFlags          = pDirChange->m_ulFlags;					    // type

			m_ppDirChange[i] = pde;
			m_ulNumDirChanges++;
		} else nRV = DIRUTIL_MEMEX;
	// update the canon.

		switch(pDirChange->m_ulFlags&(~WATCH_CHK)) // ignore the check flag
		{
		case WATCH_INIT://		0x00000000  // was there at beginning	
		case WATCH_ADD: //		0x00000001  // was added	
			{
				ppDirChangeArray = new CDirEntry*[m_ulNumDirEntries+1];
				if(ppDirChangeArray)
				{
					i=0;
					if(m_ppDirCanon)
					{
						for(i=0; i<m_ulNumDirEntries; i++)
						{
							ppDirChangeArray[i] = m_ppDirCanon[i];
						}
						delete [] m_ppDirCanon;
					}
					m_ppDirCanon = ppDirChangeArray;

					pde = new CDirEntry;
					if(pde)
					{
						pde->m_szFullPath =      (char*)malloc(strlen(pDirChange->m_szFullPath)+1);     // no filename
						if(pde->m_szFullPath)    strcpy(pde->m_szFullPath, pDirChange->m_szFullPath);
						pde->m_szFilename =      (char*)malloc(strlen(pDirChange->m_szFilename)+1);     // long filename
						if(pde->m_szFilename)    strcpy(pde->m_szFilename, pDirChange->m_szFilename);
						pde->m_szDOSFilename =   (char*)malloc(strlen(pDirChange->m_szDOSFilename)+1);  //8.3 format
						if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, pDirChange->m_szDOSFilename);
   
						pde->m_ulEntryTimestamp = pDirChange->m_ulEntryTimestamp;			// unixtime, seconds resolution, last modified
						pde->m_ftFileModified   = pDirChange->m_ftFileModified;			  // FILETIME
						pde->m_ulFileSizeHigh   = pDirChange->m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
						pde->m_ulFileSizeLow    = pDirChange->m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
						pde->m_ulFileAttribs    = pDirChange->m_ulFileAttribs;		    // attribs returned by GetFileAttributes
						pde->m_ulFlags          = pDirChange->m_ulFlags;					    // type

						m_ppDirCanon[i] = pde;
						m_ulNumDirEntries++;
					} else nRV = DIRUTIL_MEMEX;
				}
			} break;
		case WATCH_DEL: //		0x00000002  // was deleted	
			{
				int x = FindDirEntryIndex(pDirChange->m_szFullPath, pDirChange->m_szFilename, (pDirChange->m_szFilename==NULL)?pDirChange->m_szDOSFilename:NULL);
				if(x>=0)
				{
					// remove!
					if(m_ppDirCanon)
					{
						if(m_ppDirCanon[x]) delete m_ppDirCanon[x];  // removes from canon

						while((unsigned long)x<m_ulNumDirEntries-1)
						{
							m_ppDirCanon[x] = m_ppDirCanon[x+1];
							x++;
						}
						m_ppDirCanon[x] = NULL;

						m_ulNumDirEntries--;

						// ok now make a smaller array.
						ppDirChangeArray = new CDirEntry*[m_ulNumDirEntries];
						if(ppDirChangeArray)
						{
							x=0;
							while((unsigned long)x<m_ulNumDirEntries)
							{
								ppDirChangeArray[x] = m_ppDirCanon[x];
								x++;
							}
							delete [] m_ppDirCanon;
							m_ppDirCanon = ppDirChangeArray;
						}
					}
				}
			} break;
		case WATCH_CHG: //		0x00000003  // was changed	
			{
				int x = FindDirEntryIndex(pDirChange->m_szFullPath, pDirChange->m_szFilename, (pDirChange->m_szFilename==NULL)?pDirChange->m_szDOSFilename:NULL);
				if(x>=0)
				{
					// change!
					if((m_ppDirCanon)&&(m_ppDirCanon[x]))
					{
						// filename params should remain the same.  (filename changes look like old file deleted, new file added.)
//						m_ppDirCanon[x]->m_szFullPath =     (char*)malloc(strlen(pDirChange->m_szFullPath)+1);   // no filename
//						if(m_ppDirCanon[x]->m_szFullPath)   strcpy(m_ppDirCanon[x]->m_szFullPath, pDirChange->m_szFullPath);
//						m_ppDirCanon[x]->m_szFilename =     (char*)malloc(strlen(pDirChange->m_szFilename)+1);   // long filename
//						if(m_ppDirCanon[x]->m_szFilename)   strcpy(m_ppDirCanon[x]->m_szFilename, pDirChange->m_szFilename);
//						m_ppDirCanon[x]->m_szDOSFilename =  (char*)malloc(strlen(pDirChange->m_szDOSFilename)+1);  //8.3 format
//						if(m_ppDirCanon[x]->m_szDOSFilename) strcpy(m_ppDirCanon[x]->m_szDOSFilename, pDirChange->m_szDOSFilename);
   
						m_ppDirCanon[x]->m_ulEntryTimestamp = pDirChange->m_ulEntryTimestamp;			// unixtime, seconds resolution, last modified
						m_ppDirCanon[x]->m_ftFileModified   = pDirChange->m_ftFileModified;			// FILETIME
						m_ppDirCanon[x]->m_ulFileSizeHigh   = pDirChange->m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
						m_ppDirCanon[x]->m_ulFileSizeLow    = pDirChange->m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
						m_ppDirCanon[x]->m_ulFileAttribs    = pDirChange->m_ulFileAttribs;		// attribs returned by GetFileAttributes
						m_ppDirCanon[x]->m_ulFlags          = pDirChange->m_ulFlags;					// type

					}
				}
			} break;
		default:
			{
				nRV = DIRUTIL_UNSUPPORTED;
			} break;
		}


	}
	else  // could not alloc, so have to error out
		nRV = DIRUTIL_ERROR;

	LeaveCriticalSection (&m_crit);

	// if delete is set, we delete the passed in object
	if((bDelete)&&(nRV == DIRUTIL_SUCCESS))
	{ 
		delete pDirChange; // but only if successful
		pDirChange = NULL; //reset just in case
	}
	return nRV;
}

int CDirUtil::QueryChanges()  // just returns number of changes unprocessed
{
	int nRV = 0;
	EnterCriticalSection (&m_crit);
	if(m_ppDirChange != NULL)
	{
		nRV = m_ulNumDirChanges;
	}
	LeaveCriticalSection (&m_crit);
	return nRV;
}

int CDirUtil::FindDirEntryIndex(char* pszFullPath, char* pszFilename, char* pszDOSFilename) // if dos name ptr not null, ignores long filename
{
	if(pszFullPath==NULL) return DIRUTIL_BADARGS;
	if((pszFilename==NULL)&&(pszDOSFilename==NULL)) return DIRUTIL_BADARGS;  // at least one must exist.

	if((m_ppDirCanon != NULL)&&(m_ulNumDirEntries>0))
	{
		unsigned long i=0;
		while(i<m_ulNumDirEntries)
		{
			if(m_ppDirCanon[i])
			{
				if((pszDOSFilename)&&(strlen(pszDOSFilename)))
				{
					if(
						  (strcmp(pszFullPath, m_ppDirCanon[i]->m_szFullPath)==0)
						&&(strcmp(pszDOSFilename, m_ppDirCanon[i]->m_szDOSFilename)==0)
						) return i;
				}
				else
				{
					if(
						  (strcmp(pszFullPath, m_ppDirCanon[i]->m_szFullPath)==0)
						&&(strcmp(pszFilename, m_ppDirCanon[i]->m_szFilename)==0)
						) return i;
				}
			}

			i++;
		}
	}
	return DIRUTIL_ERROR;
}

int CDirUtil::CheckChanges(char* pszDirPath)  // to be called only by the watch thread or itself
{
	WIN32_FIND_DATA wfd;
	HANDLE hfile = NULL;
	// first, check the folder for all files, if they dont exist in the canon, we must add,
	// if they exist but have different attribs, we must change
	// we deal with everything that is found, but not things that are not found, that is done in watch thread

	if((pszDirPath)&&(strlen(pszDirPath)))
	{
		EnterCriticalSection (&m_critFileOp);

		char pszSearchPath[MAX_PATH+1];
		while(pszDirPath[strlen(pszDirPath)-1] == '\\') pszDirPath[strlen(pszDirPath)-1]=0; // remove trailing '\' if any
		sprintf(pszSearchPath, "%s\\*.*", pszDirPath);

		hfile = FindFirstFile( pszSearchPath,  // pointer to name of file to search for 
			&wfd  // pointer to returned information 
		); 

		// note: have to recurse dirs if set in pdu->m_bUseSubDirs
		if(hfile != INVALID_HANDLE_VALUE)
		{
			_timeb timestamp;
			_ftime( &timestamp );

			CDirEntry* pde;

			if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file or dir
			{
			//	 - timestamp.timezone
			//	timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0); // current local time.

				// do a comparison:

				// first, see if the thing is there at all:
				int x = FindDirEntryIndex(
					pszDirPath, 
					wfd.cFileName, 
					(wfd.cFileName==NULL)?wfd.cAlternateFileName:NULL
					);
				if(x>=0)
				{
					// it's there, check for changes.
					if(
						  (strcmp(m_ppDirCanon[x]->m_szFilename, wfd.cFileName))
						||(strcmp(m_ppDirCanon[x]->m_szDOSFilename, wfd.cAlternateFileName))
						||(m_ppDirCanon[x]->m_ftFileModified.dwHighDateTime != wfd.ftLastWriteTime.dwHighDateTime)
						||(m_ppDirCanon[x]->m_ftFileModified.dwLowDateTime != wfd.ftLastWriteTime.dwLowDateTime)
						||(m_ppDirCanon[x]->m_ulFileSizeHigh != wfd.nFileSizeHigh)
						||(m_ppDirCanon[x]->m_ulFileSizeLow != wfd.nFileSizeLow)
						||(m_ppDirCanon[x]->m_ulFileAttribs != wfd.dwFileAttributes)
						)
					{
						pde = new CDirEntry;
					// add the first file.
						pde->m_szFullPath = (char*)malloc(strlen(pszDirPath)+1);   // no filename
						if(pde->m_szFullPath) strcpy(pde->m_szFullPath, pszDirPath);
						pde->m_szFilename = (char*)malloc(strlen(wfd.cFileName)+1);   // long filename
						if(pde->m_szFilename) strcpy(pde->m_szFilename, wfd.cFileName);
						pde->m_szDOSFilename = (char*)malloc(strlen(wfd.cAlternateFileName)+1);  //8.3 format
						if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, wfd.cAlternateFileName);
     
						pde->m_ulEntryTimestamp = timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0);			// unixtime, seconds resolution, last modified
						pde->m_ftFileModified = wfd.ftLastWriteTime;			// FILETIME, last modified
						pde->m_ulFileSizeHigh = wfd.nFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
						pde->m_ulFileSizeLow = wfd.nFileSizeLow;				// filesize		DWORD    nFileSizeLow;
						pde->m_ulFileAttribs = wfd.dwFileAttributes;		// attribs returned by GetFileAttributes
						pde->m_ulFlags = WATCH_CHK|WATCH_CHG;					// type

						if(AddChange(pde, true)<DIRUTIL_SUCCESS)
						{
							if(pde) delete pde;// must cleanup.
						}
					} // else no changes, so just change the flag
					else
					{
						m_ppDirCanon[x]->m_ulFlags |= WATCH_CHK;
					}
				}
				else   // not there, so have to add it!
				{
					pde = new CDirEntry;
				// add the first file.
					pde->m_szFullPath = (char*)malloc(strlen(pszDirPath)+1);   // no filename
					if(pde->m_szFullPath) strcpy(pde->m_szFullPath, pszDirPath);
					pde->m_szFilename = (char*)malloc(strlen(wfd.cFileName)+1);   // long filename
					if(pde->m_szFilename) strcpy(pde->m_szFilename, wfd.cFileName);
					pde->m_szDOSFilename = (char*)malloc(strlen(wfd.cAlternateFileName)+1);  //8.3 format
					if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, wfd.cAlternateFileName);
   
					pde->m_ulEntryTimestamp = timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0);			// unixtime, seconds resolution, last modified
					pde->m_ftFileModified = wfd.ftLastWriteTime;			// FILETIME, last modified
					pde->m_ulFileSizeHigh = wfd.nFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
					pde->m_ulFileSizeLow = wfd.nFileSizeLow;				// filesize		DWORD    nFileSizeLow;
					pde->m_ulFileAttribs = wfd.dwFileAttributes;		// attribs returned by GetFileAttributes
					pde->m_ulFlags = WATCH_CHK|WATCH_ADD;					// type

					if(AddChange(pde, true)<DIRUTIL_SUCCESS)
					{
						if(pde) delete pde;// must cleanup.
					}
				}

				if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(m_bUseSubDirs))
				{
					char pszSubSearchPath[MAX_PATH+1];
					_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszDirPath, wfd.cFileName);
					CheckChanges(pszSubSearchPath);  // to be called only by the watch thread or itself
				}
			}
			while(FindNextFile( hfile,  // handle to search 
				&wfd  // pointer to structure for data on found file 
				) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
			&&(m_bWatchFolder)
			)
			{

				if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file or dir
				{
					// do a comparison:

					// first, see if the thing is there at all:
					int x = FindDirEntryIndex(
						pszDirPath, 
						wfd.cFileName, 
						(wfd.cFileName==NULL)?wfd.cAlternateFileName:NULL
						);
					if(x>=0)
					{
						// it's there, check for changes.
						if(
								(strcmp(m_ppDirCanon[x]->m_szFilename, wfd.cFileName))
							||(strcmp(m_ppDirCanon[x]->m_szDOSFilename, wfd.cAlternateFileName))
							||(m_ppDirCanon[x]->m_ftFileModified.dwHighDateTime != wfd.ftLastWriteTime.dwHighDateTime)
							||(m_ppDirCanon[x]->m_ftFileModified.dwLowDateTime != wfd.ftLastWriteTime.dwLowDateTime)
							||(m_ppDirCanon[x]->m_ulFileSizeHigh != wfd.nFileSizeHigh)
							||(m_ppDirCanon[x]->m_ulFileSizeLow != wfd.nFileSizeLow)
							||(m_ppDirCanon[x]->m_ulFileAttribs != wfd.dwFileAttributes)
							)
						{
							pde = new CDirEntry;
						// add the first file.
							pde->m_szFullPath = (char*)malloc(strlen(pszDirPath)+1);   // no filename
							if(pde->m_szFullPath) strcpy(pde->m_szFullPath, pszDirPath);
							pde->m_szFilename = (char*)malloc(strlen(wfd.cFileName)+1);   // long filename
							if(pde->m_szFilename) strcpy(pde->m_szFilename, wfd.cFileName);
							pde->m_szDOSFilename = (char*)malloc(strlen(wfd.cAlternateFileName)+1);  //8.3 format
							if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, wfd.cAlternateFileName);
     

							pde->m_ulEntryTimestamp = timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0);			// unixtime, seconds resolution, last modified
							pde->m_ftFileModified = wfd.ftLastWriteTime;			// FILETIME, last modified
							pde->m_ulFileSizeHigh = wfd.nFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
							pde->m_ulFileSizeLow = wfd.nFileSizeLow;				// filesize		DWORD    nFileSizeLow;
							pde->m_ulFileAttribs = wfd.dwFileAttributes;		// attribs returned by GetFileAttributes
							pde->m_ulFlags = WATCH_CHK|WATCH_CHG;					// type

							if(AddChange(pde, true)<DIRUTIL_SUCCESS)
							{
								if(pde) delete pde;// must cleanup.
							}
						} // else no changes, so just change the flag
						else
						{
							m_ppDirCanon[x]->m_ulFlags |= WATCH_CHK;
						}
					}
					else   // not there, so have to add it!
					{
						pde = new CDirEntry;
					// add the first file.
						pde->m_szFullPath = (char*)malloc(strlen(pszDirPath)+1);   // no filename
						if(pde->m_szFullPath) strcpy(pde->m_szFullPath, pszDirPath);
						pde->m_szFilename = (char*)malloc(strlen(wfd.cFileName)+1);   // long filename
						if(pde->m_szFilename) strcpy(pde->m_szFilename, wfd.cFileName);
						pde->m_szDOSFilename = (char*)malloc(strlen(wfd.cAlternateFileName)+1);  //8.3 format
						if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, wfd.cAlternateFileName);
   
						pde->m_ulEntryTimestamp = timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0);			// unixtime, seconds resolution, last modified
						pde->m_ftFileModified = wfd.ftLastWriteTime;			// FILETIME, last modified
						pde->m_ulFileSizeHigh = wfd.nFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
						pde->m_ulFileSizeLow = wfd.nFileSizeLow;				// filesize		DWORD    nFileSizeLow;
						pde->m_ulFileAttribs = wfd.dwFileAttributes;		// attribs returned by GetFileAttributes
						pde->m_ulFlags = WATCH_CHK|WATCH_ADD;					// type

						if(AddChange(pde, true)<DIRUTIL_SUCCESS)
						{
							if(pde) delete pde;// must cleanup.
						}
					}

					if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(m_bUseSubDirs))
					{
						char pszSubSearchPath[MAX_PATH+1];
						_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszDirPath, wfd.cFileName);
						CheckChanges(pszSubSearchPath);  // to be called only by the watch thread or itself
					}
				}
			}
			FindClose( hfile); 
		}
		LeaveCriticalSection(&m_critFileOp);
		return DIRUTIL_SUCCESS;

	}
	return DIRUTIL_ERROR;
}

int CDirUtil::ClearChecks()
{
	for(unsigned long i=0; i<m_ulNumDirEntries; i++)
	{
		if((m_ppDirCanon)&&(m_ppDirCanon[i]))
		{
			m_ppDirCanon[i]->m_ulFlags &= ~WATCH_CHK;  // removes check;
		}
	}
	return DIRUTIL_SUCCESS;
}

int CDirUtil::AddInitDirEntries(char* pszDirPath)  // to be called only by the watch thread or itself
{
	WIN32_FIND_DATA wfd;
	HANDLE hfile = NULL;


	if((pszDirPath)&&(strlen(pszDirPath)))
	{
		char pszSearchPath[MAX_PATH+1];
		while(pszDirPath[strlen(pszDirPath)-1] == '\\') pszDirPath[strlen(pszDirPath)-1]=0; // remove trailing '\' if any
		sprintf(pszSearchPath, "%s\\*.*", pszDirPath);
		EnterCriticalSection(&m_critFileOp);

		hfile = FindFirstFile( pszSearchPath,  // pointer to name of file to search for 
			&wfd  // pointer to returned information 
		); 

		// note: have to recurse dirs if set in pdu->m_bUseSubDirs
		if(hfile != INVALID_HANDLE_VALUE)
		{
			_timeb timestamp;
			_ftime( &timestamp );

			CDirEntry* pde;

			if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file or dir
			{
			//	 - timestamp.timezone
			//	timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0); // current local time.
/*
FILE* fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "adding %s\\%s\r\n",pszDirPath,wfd.cFileName );
	fflush(fp);
	fclose(fp);
}
	*/
				pde = new CDirEntry;
			// add the first file.
				pde->m_szFullPath = (char*)malloc(strlen(pszDirPath)+1);   // no filename
				if(pde->m_szFullPath) strcpy(pde->m_szFullPath, pszDirPath);
				pde->m_szFilename = (char*)malloc(strlen(wfd.cFileName)+1);   // long filename
				if(pde->m_szFilename) strcpy(pde->m_szFilename, wfd.cFileName);
				pde->m_szDOSFilename = (char*)malloc(strlen(wfd.cAlternateFileName)+1);  //8.3 format
				if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, wfd.cAlternateFileName);
     

				pde->m_ulEntryTimestamp = timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0);			// unixtime, seconds resolution, last modified
				pde->m_ftFileModified = wfd.ftLastWriteTime;			// FILETIME, last modified
				pde->m_ulFileSizeHigh = wfd.nFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
				pde->m_ulFileSizeLow = wfd.nFileSizeLow;				// filesize		DWORD    nFileSizeLow;
				pde->m_ulFileAttribs = wfd.dwFileAttributes;		// attribs returned by GetFileAttributes
				pde->m_ulFlags = WATCH_INIT;					// type

				if(AddChange(pde, true)<DIRUTIL_SUCCESS)
				{
					if(pde) delete pde;// must cleanup.
				}

				if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(m_bUseSubDirs))
				{
					char pszSubSearchPath[MAX_PATH+1];
					_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszDirPath, wfd.cFileName);
					AddInitDirEntries(pszSubSearchPath);  // to be called only by the watch thread or itself
				}
			}
			while(FindNextFile( hfile,  // handle to search 
				&wfd  // pointer to structure for data on found file 
				) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
			&&(m_bWatchFolder)
			)
			{

				if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file or dir
				{
/*
FILE* fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "adding %s\\%s\r\n",pszDirPath,wfd.cFileName );
	fflush(fp);
	fclose(fp);
}
*/
					pde = new CDirEntry;
				// add the first file.
					pde->m_szFullPath = (char*)malloc(strlen(pszDirPath)+1);   // no filename
					if(pde->m_szFullPath) strcpy(pde->m_szFullPath, pszDirPath);
					pde->m_szFilename = (char*)malloc(strlen(wfd.cFileName)+1);   // long filename
					if(pde->m_szFilename) strcpy(pde->m_szFilename, wfd.cFileName);
					pde->m_szDOSFilename = (char*)malloc(strlen(wfd.cAlternateFileName)+1);  //8.3 format
					if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, wfd.cAlternateFileName);
     

					pde->m_ulEntryTimestamp = timestamp.time - (timestamp.timezone*60) + (timestamp.dstflag?3600:0);			// unixtime, seconds resolution, last modified
					pde->m_ftFileModified = wfd.ftLastWriteTime;			// FILETIME, last modified
					pde->m_ulFileSizeHigh = wfd.nFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
					pde->m_ulFileSizeLow = wfd.nFileSizeLow;				// filesize		DWORD    nFileSizeLow;
					pde->m_ulFileAttribs = wfd.dwFileAttributes;		// attribs returned by GetFileAttributes
					pde->m_ulFlags = WATCH_INIT;					// type

					if(AddChange(pde, true)<DIRUTIL_SUCCESS)
					{
						if(pde) delete pde;// must cleanup.
					}

					if((wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)&&(m_bUseSubDirs))
					{
						char pszSubSearchPath[MAX_PATH+1];
						_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszDirPath, wfd.cFileName);
						AddInitDirEntries(pszSubSearchPath);  // to be called only by the watch thread or itself
					}
				}
			}
			FindClose( hfile); 
		}
		LeaveCriticalSection(&m_critFileOp);

		return DIRUTIL_SUCCESS;

	}
	return DIRUTIL_ERROR;
}

// this is a fully working sample thread, other threads can be written on this model and
// the m_pWatchThread member can be set to the function pointer.
void DirWatchThread(void* pvArgs)
{
	CDirUtil* pdu = (CDirUtil*) pvArgs;

	if(pdu)
	{
		if(pdu->m_bWatchFolder) return;  //only one thread allowed
		pdu->m_bWatchFolder = true;

		EnterCriticalSection (&pdu->m_crit);

		if(pdu->m_ppDirChange)
		{
			unsigned long i = 0;
			for(i=0; i<pdu->m_ulNumDirChanges; i++)
			{
				if(pdu->m_ppDirChange[i]) delete pdu->m_ppDirChange[i];
				pdu->m_ppDirChange[i] = NULL;
			}
			delete [] pdu->m_ppDirChange;
			pdu->m_ppDirChange = NULL;
		}

		if(pdu->m_ppDirCanon)
		{
			unsigned long i = 0;
			for(i=0; i<pdu->m_ulNumDirEntries; i++)
			{
				if(pdu->m_ppDirCanon[i]) delete pdu->m_ppDirCanon[i];
				pdu->m_ppDirCanon[i] = NULL;
			}
			delete [] pdu->m_ppDirCanon;
			pdu->m_ppDirCanon = NULL;
		}
		pdu->m_ulNumDirChanges = 0;
		pdu->m_ulNumDirEntries = 0;

		LeaveCriticalSection (&pdu->m_crit);

		//start the process up by loading up the canon
		pdu->AddInitDirEntries(pdu->m_pszWatchPath);
		

/*
FILE* fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "added init dir entries\r\nnum changes: %d, num dir entries: %d", pdu->m_ulNumDirChanges, pdu->m_ulNumDirEntries );
	fflush(fp);
	fclose(fp);
}
*/
//		CString foo; foo.Format("%d, %d", pdu->m_ulNumDirChanges, pdu->m_ulNumDirEntries );
//		AfxMessageBox(foo);

		_timeb timestamp;
		_timeb watchtime;
		_ftime( &timestamp );
		watchtime.time = timestamp.time + pdu->m_ulWatchIntervalMS/1000; 
		watchtime.millitm = timestamp.millitm + (unsigned short)(pdu->m_ulWatchIntervalMS%1000); // fractional second updates
		if(watchtime.millitm>999)
		{
			watchtime.time++;
			watchtime.millitm%=1000;
		}

/*
FILE* fp = fopen("dirwatch.log", "at");
if(fp)
{
		char buffer[50];
		tm* theTime = localtime( &timestamp.time	);
		strftime(buffer, 30, "%H:%M:%S.", theTime );

	fprintf(fp, "finshed adding init dir entries at %s%d: \r\nnum changes: %d, num dir entries: %d", buffer,timestamp.millitm, pdu->m_ulNumDirChanges, pdu->m_ulNumDirEntries );
	fflush(fp);
	fclose(fp);
}
*/
//		if(pdu->m_bWatchFolder) AfxMessageBox("pdu->m_bWatchFolder true"); else 		AfxMessageBox("pdu->m_bWatchFolder false");
		while(pdu->m_bWatchFolder)
		{
		// stuff here...
			// check the folder!

			pdu->ClearChecks();  // clears any checks
/*
FILE* fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "checking for changes\r\n" );
	fflush(fp);
	fclose(fp);
}
*/
			pdu->CheckChanges(pdu->m_pszWatchPath);
			// any remaining files in the canon that have not been found, must have been deleted.
			// so, we remove them
/*
fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "checked for changes\r\n" );
	fflush(fp);
	fclose(fp);
}
*/
			unsigned long i=0;
			while(( i<pdu->m_ulNumDirEntries )&&(pdu->m_bWatchFolder))
			{
				if((pdu->m_ppDirCanon)&&(pdu->m_ppDirCanon[i]))
				{
					if(!(pdu->m_ppDirCanon[i]->m_ulFlags&WATCH_CHK))
					{
						// this was not found, so must delete.
						CDirEntry* pde = new CDirEntry;

						if(pde)
						{
							pde->m_szFullPath =      (char*)malloc(strlen(pdu->m_ppDirCanon[i]->m_szFullPath)+1);     // no filename
							if(pde->m_szFullPath)    strcpy(pde->m_szFullPath, pdu->m_ppDirCanon[i]->m_szFullPath);
							pde->m_szFilename =      (char*)malloc(strlen(pdu->m_ppDirCanon[i]->m_szFilename)+1);     // long filename
							if(pde->m_szFilename)    strcpy(pde->m_szFilename, pdu->m_ppDirCanon[i]->m_szFilename);
							pde->m_szDOSFilename =   (char*)malloc(strlen(pdu->m_ppDirCanon[i]->m_szDOSFilename)+1);  //8.3 format
							if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, pdu->m_ppDirCanon[i]->m_szDOSFilename);

							// these dont matter
//							pde->m_ulEntryTimestamp = pDirChange->m_ulEntryTimestamp;			// unixtime, seconds resolution, last modified
//							pde->m_ftFileModified   = pDirChange->m_ftFileModified;			  // FILETIME
//							pde->m_ulFileSizeHigh   = pDirChange->m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
//							pde->m_ulFileSizeLow    = pDirChange->m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
//							pde->m_ulFileAttribs    = pDirChange->m_ulFileAttribs;		    // attribs returned by GetFileAttributes
							pde->m_ulFlags          = WATCH_DEL;					    // type

							if(pdu->AddChange(pde, true)<DIRUTIL_SUCCESS)
							{
								if(pde) delete pde;// must cleanup.
							}
							else
							{
								i=0;  // the AddChange has reordered the canon.  
								//have to keep resetting until no more deletions should be made
							}
						}
					}
				}
				i++;
			}



/*
			EnterCriticalSection (&pdu->m_crit);
	//	AfxMessageBox("checking");
			LeaveCriticalSection (&pdu->m_crit);
*/

/* debug action
			// bogus status thingie
			CDirEntry* pde = new CDirEntry;
			if(pde)
			{
				pde->m_szFullPath =      (char*)malloc(128);   // no filename
				if(pde->m_szFullPath)    strcpy(pde->m_szFullPath, "checking");
				pde->m_szFilename =      (char*)malloc(128);   // long filename
				if(pde->m_szFilename)    strcpy(pde->m_szFilename, "-----");
				pde->m_szDOSFilename =   (char*)malloc(strlen("checking")+1);  //8.3 format
				if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, "checking");
				pde->m_ulFlags = 0;
   
//					AfxMessageBox(pde->m_szFullPath);
				if(pdu->AddChange(pde, true)<DIRUTIL_SUCCESS)
				{
					if(pde) delete pde;// must cleanup.
				}
			}
//			AfxMessageBox("check");
*/
/*
fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "about to wait for %d ms\r\n" ,pdu->m_ulWatchIntervalMS);
	fflush(fp);
	fclose(fp);
}
*/
			while(pdu->m_bWatchFolder)  // this is just a wait loop.
			{
				_ftime( &timestamp );
				if(
						(timestamp.time > watchtime.time)
					||((timestamp.time == watchtime.time)&&(timestamp.millitm >= watchtime.millitm))
					)
				{
					watchtime.time = timestamp.time + pdu->m_ulWatchIntervalMS/1000; 
					watchtime.millitm = timestamp.millitm + (unsigned short)(pdu->m_ulWatchIntervalMS%1000); // fractional second updates
					if(watchtime.millitm>999)
					{
						watchtime.time++;
						watchtime.millitm%=1000;
					}
/* debug action
					CDirEntry* pde = new CDirEntry;

					pde->m_szFullPath =      (char*)malloc(strlen("time")+1);   // no filename
					if(pde->m_szFullPath)    strcpy(pde->m_szFullPath, "time");
					pde->m_szFilename =      (char*)malloc(strlen("time")+1);   // long filename
					if(pde->m_szFilename)    strcpy(pde->m_szFilename, "time");
					pde->m_szDOSFilename =   (char*)malloc(strlen("time")+1);  //8.3 format
					if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, "time");
					pde->m_ulFlags = 0;
   
					if(pdu->AddChange(pde, true)<DIRUTIL_SUCCESS)
					{
						if(pde) delete pde;// must cleanup.
					}
*/
					break;  // breaks out of inner while loop
				}
				Sleep(1);
			} // inner while
/*
fp = fopen("dirwatch.log", "at");
if(fp)
{
	fprintf(fp, "waited for %d ms\r\n" ,pdu->m_ulWatchIntervalMS);
	fflush(fp);
	fclose(fp);
}
*/
		} // outer while
	}  // object pointer exists
	_endthread();
}