// DirUtil.h: interface for the CDirUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRUTIL_H__D16F300A_48FC_4878_A788_8619FB254ECA__INCLUDED_)
#define AFX_DIRUTIL_H__D16F300A_48FC_4878_A788_8619FB254ECA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <afxmt.h>

#define DIRUTIL_SUCCESS				0
#define DIRUTIL_ERROR					-1
#define DIRUTIL_INPROGRESS		-2
#define DIRUTIL_BADARGS				-3
#define DIRUTIL_MEMEX					-4
#define DIRUTIL_UNSUPPORTED		-256

#define WATCH_INIT		0x00000000  // was there at beginning	
#define WATCH_ADD			0x00000001  // was added	
#define WATCH_DEL			0x00000002  // was deleted	
#define WATCH_CHG			0x00000003  // was changed	
#define WATCH_CHK			0x00000010  // was checked	

class CDirEntry
{
public:
	CDirEntry();
	virtual ~CDirEntry();

public:
	char* m_szFullPath;  // with no filename
	char* m_szFilename;   // long filename
	char* m_szDOSFilename;   //8.3 format
     

	unsigned long m_ulFlags;					// type
	unsigned long m_ulEntryTimestamp;			// unixtime, seconds resolution, time of transaction

	unsigned long m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
	unsigned long m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
	unsigned long m_ulFileAttribs;		// attribs returned by GetFileAttributes
	FILETIME m_ftFileModified;			// unixtime, seconds resolution, last modified
};


class CDirUtil  
{
public:
		unsigned long	m_ulWatchIntervalMS;
//		bool	m_bUseSwapFile;
		bool	m_bWatchFolder;
		bool	m_bUseSubDirs;
		char* m_pszWatchPath;
		void* m_pWatchThread;
		CDirEntry** m_ppDirChange;
		unsigned long	m_ulNumDirChanges;
//		unsigned long	m_ulMaxNumDirChanges;  //unimplemented
		CRITICAL_SECTION m_crit;  // critical section to manage dir change array
		CRITICAL_SECTION m_critFileOp;  // critical section to manage denial of access when searching for changes
		CDirEntry** m_ppDirCanon;
		unsigned long	m_ulNumDirEntries;

public:
	CDirUtil();
	virtual ~CDirUtil();

	int SetWatchFolder(char* pszFullPath);
	int BeginWatch( unsigned long ulWatchIntervalMS=30000, bool bSubDirs=true);
	int EndWatch();
	int GetDirChange(CDirEntry** ppDirChange);
	int AddInitDirEntries(char* pszDirPath);  // to be called only by the watch thread
	int CheckChanges(char* pszDirPath);  // to be called only by the watch thread
	int ClearChecks();  // to be called only by the watch thread

	int AddChange(CDirEntry* pDirChange, bool bDelete=false);  // adds a change to both chnge list and updates canon.
	int QueryChanges();  // just returns number of changes unprocessed
	int FindDirEntryIndex(char* pszFullPath, char* pszFilename, char* pszDOSFilename=NULL); // if dos name ptr not null, ignores long filename

};

#endif // !defined(AFX_DIRUTIL_H__D16F300A_48FC_4878_A788_8619FB254ECA__INCLUDED_)
