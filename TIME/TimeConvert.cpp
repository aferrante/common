// TimeConvert.cpp: implementation of the CTimeConvert class.
//
//////////////////////////////////////////////////////////////////////

//#include <stdafx.h>
#include "TimeConvert.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define EG40

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimeConvert::CTimeConvert()
{

}

CTimeConvert::~CTimeConvert()
{

}

int CTimeConvert::HMSFtoMS(int nHours, int nMinutes, int nSeconds, int nFrames, double dblRate)
{
	if(((int)(dblRate))==29)  //NTSC DF
	{
		return ((int)((1001.0/30.0) * ((double)(nFrames + 30*nSeconds + 1798*nMinutes + 2*(nMinutes/10) + 107892*nHours /*+ 0.5*/)) /*+ 0.5*/ + 0.999 ));  
//the 0.5 in the first position gives you half a frame more, putting you squarely in the "midpoint" of the frame.  the 0.5 in the second position gives you rounding on number of milliseconds, however it is not good enough, when it rounds down it truncates and you cannot convert back and forth.  Therefore, adding almost 1 millisecond will always get you just into the frame you want in a re-convertible manner.
// .999999999 is used instead of 1 so that the values that come out to an even number actually experience no millisecond offset.  such as 00:00:30;00 -> 30030 ms. not 30031 ms
// had to make it .999 because .999999999 was tipping the floating point over, treated it like +1.00000
	}
	else
	{
		return ((int)((1000.0/dblRate)*((double)(nFrames + ((int)(dblRate))*(nSeconds + 60*(nMinutes + 60*nHours)) /*+ 0.5*/))  /*+ 0.5*/ + 0.999 ));
//the 0.5 in the first position gives you half a frame more, putting you squarely in the "midpoint" of the frame.  the 0.5 in the second position gives you rounding on number of milliseconds, however it is not good enough, when it rounds down it truncates and you cannot convert back and forth.  Therefore, adding almost 1 millisecond will always get you just into the frame you want in a re-convertible manner.
// .999999999 is used instead of 1 so that the values that come out to an even number actually experience no millisecond offset.  such as 00:00:30;00 -> 30030 ms. not 30031 ms
// had to make it .999 because .999999999 was tipping the floating point over, treated it like +1.00000
	}

	return -1;  // never hits this but leave it, maybe one day we will check validity of incoming values
}

int CTimeConvert::MStoHMSF(int nMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, double dblRate)
{
	if((pucHours)&&(pucMinutes)&&(pucSeconds)&&(pucFrames))
	{
		if(((int)(dblRate))==29)  //NTSC DF
		{

#ifndef EG40
			// fill the data with drop frame
			int rem = (nMilliseconds/1000);
			while(rem<0) rem+=86400;
			while(rem>86399) rem-=86400;
			double ms = (rem%86400)*1000 + (nMilliseconds%1000);
			int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
			int deci = (int)(fr / 17982);
			rem = fr % 17982;
			if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

			*pucHours		= (unsigned char)((deci / 6)&0x000000ff); 
			*pucMinutes	= (unsigned char)((((deci % 6) * 10) + (rem / 1800))&0x000000ff); 
			*pucSeconds	= (unsigned char)(((rem % 1800) / 30)&0x000000ff); 
			*pucFrames	= (unsigned char)((rem % 30)&0x000000ff);

#else

			int fr = ((int)((30.0/1001.0)*((double)(nMilliseconds)))); //total frame count
			int rem = (fr/107892)*107892;  // (for int truncation on division)
			*pucHours		= (unsigned char)((fr/107892)&0x000000ff);
			int min =	(  (fr + (((fr-rem)/1800)*2) - (((fr-rem)/18000)*2) - rem) / 1800); 
			*pucMinutes	= (unsigned char)((min)&0x000000ff); 
			int sec = (fr - ((min)*1798) - ((min/10)*2) -rem ) / 30; 
			*pucSeconds	= (unsigned char)((sec)&0x000000ff);
			*pucFrames	= (unsigned char)((fr - ((sec)*30) - ((min)*1798) - ((min/10)*2) - rem)&0x000000ff);


#endif //#ifndef EG40

		}
		else
		{
//			nMilliseconds %= 86400; //remains of the day!  // don't do this part, if > 1 day's worth comes in, return the right amount of hours
			*pucHours		= (unsigned char)((nMilliseconds/3600000)&0x000000ff);
			*pucMinutes	= (unsigned char)(((nMilliseconds/60000)%60)&0x000000ff);
			*pucSeconds	= (unsigned char)(((nMilliseconds/1000)%60)&0x000000ff);
			*pucFrames	= ((unsigned char)( ((nMilliseconds%1000)*1000) /  ((int)(1000000.0/dblRate)) ));
		}

		return 0;
	}
	return -1;
}

int CTimeConvert::MStoHMSF(int nMilliseconds, int* pnHours, int* pnMinutes, int* pnSeconds, int* pnFrames, double dblRate)
{
	if((pnHours)&&(pnMinutes)&&(pnSeconds)&&(pnFrames))
	{
		if(((int)(dblRate))==29)  //NTSC DF
		{

#ifndef EG40
			// fill the data with drop frame
			int rem = (nMilliseconds/1000);
			while(rem<0) rem+=86400;
			while(rem>86399) rem-=86400;
			double ms = (rem%86400)*1000 + (nMilliseconds%1000);
			int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
			int deci = (int)(fr / 17982);
			rem = fr % 17982;
			if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

			*pnHours		= deci / 6; 
			*pnMinutes	= ((deci % 6) * 10) + (rem / 1800); 
			*pnSeconds	= (rem % 1800) / 30; 
			*pnFrames		= rem % 30;

#else

			int fr = ((int)((30.0/1001.0)*((double)(nMilliseconds)))); //total frame count
			int rem = (fr/107892)*107892;  // (for int truncation on division)
			*pnHours		= (fr/107892);
			*pnMinutes	= (  (fr + (((fr-rem)/1800)*2) - (((fr-rem)/18000)*2) - rem) / 1800); 
			*pnSeconds	= (fr - ((*pnMinutes)*1798) - ((*pnMinutes/10)*2) -rem ) / 30; 
			*pnFrames		= fr - ((*pnSeconds)*30) - ((*pnMinutes)*1798) - ((*pnMinutes/10)*2) - rem;


#endif //#ifndef EG40


		}
		else
		{
//			nMilliseconds %= 86400; //remains of the day!  // don't do this part, if > 1 day's worth comes in, return the right amount of hours
			*pnHours		= (nMilliseconds/3600000);
			*pnMinutes	= (nMilliseconds/60000)%60;
			*pnSeconds	= (nMilliseconds/1000)%60;
			*pnFrames		= ((nMilliseconds%1000)*1000) /  ((int)(1000000.0/dblRate)) ;
		}
		return 0;
	}
	return -1;
}


int CTimeConvert::BCDHMSFtoMS(unsigned long ulTimeCode, double dblRate)
{
	int nTemp = (unsigned char)((ulTimeCode&0xff000000)>>24); 
	int nHours = ((nTemp/16)*10	+ (nTemp%16));
	
	nTemp = (unsigned char)((ulTimeCode&0x00ff0000)>>16); 
	int nMinutes = ((nTemp/16)*10	+ (nTemp%16));

	nTemp = (unsigned char)((ulTimeCode&0x0000ff00)>>8); 
	int nSeconds = ((nTemp/16)*10	+ (nTemp%16));

	nTemp = (unsigned char)(ulTimeCode&0x000000ff);
	int nFrames = ((nTemp/16)*10	+ (nTemp%16));

	return HMSFtoMS(nHours, nMinutes, nSeconds, nFrames, dblRate);
}


int CTimeConvert::MStoBCDHMSF(int nMilliseconds, unsigned long* pulTimeCode, double dblRate)
{
	if(pulTimeCode)
	{
		int nHours;
		int nMinutes;
		int nSeconds;
		int nFrames;

		if(MStoHMSF(nMilliseconds, &nHours, &nMinutes, &nSeconds, &nFrames, dblRate)==0)
		{
			*pulTimeCode = 

			((((((nHours/10)%24)*16) + (nHours%10))			<< 24)&0xff000000) | // have to mod 24 for TC
			((((((nMinutes/10))*16) + (nMinutes%10)) << 16)&0x00ff0000) |
			((((((nSeconds/10))*16) + (nSeconds%10)) << 8)	&0x0000ff00) |
			(((((nFrames/10))*16) + (nFrames%10))					&0x000000ff) ;
			return 0;
		}
	}
	return -1;
}

int CTimeConvert::NDFMStoDFMS(int nNDFMilliseconds)
{
	int nHours;
	int nMinutes;
	int nSeconds;
	int nFrames;

	if(MStoHMSF(nNDFMilliseconds, &nHours, &nMinutes, &nSeconds, &nFrames, 30.0)==0)
	{
		return HMSFtoMS(nHours, nMinutes, nSeconds, nFrames, 29.97);
	}
	return -1;
}


int CTimeConvert::DFMStoNDFMS(int nDFMilliseconds)
{
	int nHours;
	int nMinutes;
	int nSeconds;
	int nFrames;

	if(MStoHMSF(nDFMilliseconds, &nHours, &nMinutes, &nSeconds, &nFrames, 29.97)==0)
	{
		return HMSFtoMS(nHours, nMinutes, nSeconds, nFrames, 30.00);
	}
	return -1;
}


int CTimeConvert::MStoF(int nMilliseconds, double dblRate)
{
	if(((int)(dblRate))==29)  //NTSC DF
	{
		return ((int)((30.0/1001.0)*((double)(nMilliseconds)))); //total frame count
	}
	else
	{
		return ((int)((dblRate/1000.0)*((double)(nMilliseconds)))); //total frame count
	}
	return -1;
}

int CTimeConvert::FtoMS(int nFrames, double dblRate)
{
	if(((int)(dblRate))==29)  //NTSC DF
	{
		return ((int)((1001.0*((double)(nFrames)) )/30.0 + .999)); 
	}
	else
	{
		return ((int)((1000.0*((double)(nFrames)))/dblRate + .999));
	}
	return -1;
}



