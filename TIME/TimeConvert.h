// TimeConvert.h: interface for the CTimeConvert class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TIMECONVERT_H__1805F79E_43BC_4E84_AFC5_ED2D5755B6FE__INCLUDED_)
#define AFX_TIMECONVERT_H__1805F79E_43BC_4E84_AFC5_ED2D5755B6FE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CTimeConvert  
{
public:
	CTimeConvert();
	virtual ~CTimeConvert();

	int HMSFtoMS(int nHours, int nMinutes, int nSeconds=0, int nFrames=0, double dblRate=29.97);
	int MStoHMSF(int nMilliseconds, int* pnHours, int* pnMinutes, int* pnSeconds, int* pnFrames, double dblRate=29.97);
	int MStoHMSF(int nMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, double dblRate=29.97);
	int BCDHMSFtoMS(unsigned long ulTimeCode, double dblRate=29.97);
	int MStoBCDHMSF(int nMilliseconds, unsigned long* pulTimeCode, double dblRate=29.97);
	int NDFMStoDFMS(int nNDFMilliseconds);
	int DFMStoNDFMS(int nDFMilliseconds);
	int MStoF(int nMilliseconds, double dblRate=29.97);
	int FtoMS(int nFrames, double dblRate=29.97);
};

#endif // !defined(AFX_TIMECONVERT_H__1805F79E_43BC_4E84_AFC5_ED2D5755B6FE__INCLUDED_)
