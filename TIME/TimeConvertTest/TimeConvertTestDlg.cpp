// TimeConvertTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TimeConvertTest.h"
#include "TimeConvertTestDlg.h"
#include "..\TimeConvert.h"
#include <sys/timeb.h>
#include "..\..\TXT\BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CTimeConvert tc;
_timeb timestamp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimeConvertTestDlg dialog

CTimeConvertTestDlg::CTimeConvertTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeConvertTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTimeConvertTestDlg)
	m_szValue1 = _T("");
	m_szValue2 = _T("");
	m_szValue3 = _T("");
	m_szValue4 = _T("");
	m_szValue5 = _T("0");
	m_szDFFrames = _T("");
	m_szNDFFrames = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bGoing = false;
	fp = NULL;
	m_nNumSamples=0;
	m_dblDiff=0.0;
	m_nTotal=0;
	m_bDF2NDF = true;
}

void CTimeConvertTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimeConvertTestDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szValue1);
	DDX_Text(pDX, IDC_EDIT2, m_szValue2);
	DDX_Text(pDX, IDC_EDIT3, m_szValue3);
	DDX_Text(pDX, IDC_EDIT4, m_szValue4);
	DDX_Text(pDX, IDC_EDIT5, m_szValue5);
	DDX_Text(pDX, IDC_STATIC_DF_FRAMES, m_szDFFrames);
	DDX_Text(pDX, IDC_STATIC_NDF_FRAMES, m_szNDFFrames);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTimeConvertTestDlg, CDialog)
	//{{AFX_MSG_MAP(CTimeConvertTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_GO, OnButtonGo)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CONVERT, OnButtonConvert)
	ON_BN_CLICKED(IDC_BUTTON_CONVERT2, OnButtonConvert2)
	ON_BN_CLICKED(IDC_BUTTON_CONVERTNDF, OnButtonConvertndf)
	ON_BN_CLICKED(IDC_BUTTON_CONVERTNDF2MS, OnButtonConvertndf2ms)
	ON_BN_CLICKED(IDC_BUTTON_DFMINUS, OnButtonDfminus)
	ON_BN_CLICKED(IDC_BUTTON_DFPLUS, OnButtonDfplus)
	ON_BN_CLICKED(IDC_BUTTON_DFUSETOTAL, OnButtonDfusetotal)
	ON_BN_CLICKED(IDC_BUTTON_NDFMINUS, OnButtonNdfminus)
	ON_BN_CLICKED(IDC_BUTTON_NDFPLUS, OnButtonNdfplus)
	ON_BN_CLICKED(IDC_BUTTON_NDFUSETOTAL, OnButtonNdfusetotal)
	ON_BN_CLICKED(IDC_BUTTON_ZERO, OnButtonZero)
	ON_BN_CLICKED(IDC_BUTTON_DF2NDF, OnButtonDf2ndf)
	ON_EN_SETFOCUS(IDC_EDIT1, OnSetfocusEdit1)
	ON_EN_SETFOCUS(IDC_EDIT3, OnSetfocusEdit3)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN2, OnDeltaposSpin2)
	ON_BN_CLICKED(IDC_STATIC_DF_FRAMES, OnStaticDfFrames)
	ON_BN_CLICKED(IDC_STATIC_NDF_FRAMES, OnStaticNdfFrames)
	ON_BN_CLICKED(IDC_BUTTON_DFFR, OnButtonDffr)
	ON_BN_CLICKED(IDC_BUTTON_NDFFR, OnButtonNdffr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimeConvertTestDlg message handlers

BOOL CTimeConvertTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	_timeb now;
	_ftime(&now);

	int ms =  (((now.time- (now.timezone*60)+(now.dstflag?3600:0))%86400)*1000) + now.millitm;
	m_szValue3.Format("%d", ms);
	m_szValue1.Format("%d", ms);

	UpdateData(FALSE);	
	OnButtonConvert();	
	OnButtonConvertndf();
	OnButtonConvert2();	
	OnButtonConvertndf2ms();


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTimeConvertTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTimeConvertTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTimeConvertTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CTimeConvertTestDlg::OnButtonGo() 
{
	// TODO: Add your control notification handler code here
	if(	m_bGoing )
	{
		m_bGoing = false;

		if(fp)	fclose(fp);
		fp=NULL;

		GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Generate File");

		KillTimer(69);

	}
	else
	{
		fp = fopen("tctest.txt", "ab");

		if(fp)
		{
			m_nNumSamples=0;
			m_dblDiff=0.0;

			m_bGoing = true;
			GetDlgItem(IDC_BUTTON_GO)->SetWindowText("Stop");

			SetTimer(69, 1, NULL);
		}


	}
	
}

void CTimeConvertTestDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent==69)
	{
		if(fp)
		{
			_ftime(&timestamp);

			tm* theTime = localtime( &timestamp.time	);
			char timebuf[MAX_PATH];
			strftime(timebuf, MAX_PATH-1, "%H.%M.%S.", theTime ); // asterisk is a key character.

			int n = ((int)(((double)((timestamp.time- (timestamp.timezone*60)+(timestamp.dstflag?3600:0))%86400))*1000.0 + ((double)(timestamp.millitm))));


			
			int nHours;
			int nMinutes;
			int nSeconds;
			int nFrames;
			unsigned long ulTC;

			tc.MStoHMSF(n, &nHours, &nMinutes, &nSeconds, &nFrames); // default to ntsc df
			int ms = tc.HMSFtoMS(nHours, nMinutes, nSeconds, nFrames); // default to ntsc df
			tc.MStoBCDHMSF(n, &ulTC);


			int diff = (n-ms);

			m_nNumSamples++;

			m_dblDiff = ( ((double)(m_nNumSamples-1))*m_dblDiff + (double)diff)/((double)(m_nNumSamples)); // can never be div 0, we jus tincremented numsamples



			fprintf(fp, "%s%03d   %d ms   %02d:%02d:%02d;%02d (%08x TC) -> ms %d (diff %d ms) (avg diff %.03f ms, %d samples)\r\n", 
				timebuf,
				timestamp.millitm,
				n,
				nHours,
				nMinutes,
				nSeconds,
				nFrames,
				ulTC,
				ms,
				diff,
				m_dblDiff,
				m_nNumSamples
			);

		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CTimeConvertTestDlg::OnButtonConvert() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	int ms = atoi(m_szValue1);
	unsigned long ulTimeCode;

	tc.MStoBCDHMSF(ms, &ulTimeCode, 29.97);

	m_szValue2.Format("%08x", ulTimeCode);

	m_szDFFrames.Format("%d frames", tc.MStoF(ms, 29.97));

	UpdateData(FALSE);	

	
}

void CTimeConvertTestDlg::OnButtonConvert2() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CBufferUtil bu;
	unsigned long ulTimeCode = bu.xtol(m_szValue2.GetBuffer(0),8);
	int ms = tc.BCDHMSFtoMS( ulTimeCode, 29.97);
	m_szValue1.Format("%d", ms);
	m_szDFFrames.Format("%d frames", tc.MStoF(ms, 29.97));

	UpdateData(FALSE);	
}


void CTimeConvertTestDlg::OnButtonConvertndf() 
{
	UpdateData(TRUE);

	int ms = atoi(m_szValue3);
//	unsigned long ulTimeCode;

//	tc.MStoBCDHMSF(ms, &ulTimeCode, 30.00);

		int nHours;
		int nMinutes;
		int nSeconds;
		int nFrames;

	tc.MStoHMSF(ms, &nHours, &nMinutes, &nSeconds, &nFrames, 30.0);

	m_szNDFFrames.Format("%d frames", tc.MStoF(ms, 30.0));



//	m_szValue4.Format("%08x", ulTimeCode);
	m_szValue4.Format("%02d%02d%02d%02d", nHours, nMinutes, nSeconds, nFrames);

	UpdateData(FALSE);	
	
}

void CTimeConvertTestDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}

void CTimeConvertTestDlg::OnButtonConvertndf2ms() 
{
	UpdateData(TRUE);

	CBufferUtil bu;
	unsigned long ulTimeCode = bu.xtol(m_szValue4.GetBuffer(0),8);
	int ms = tc.BCDHMSFtoMS( ulTimeCode, 30.0);
	m_szValue3.Format("%d", ms);
	m_szNDFFrames.Format("%d frames", tc.MStoF(ms, 30.0));

	UpdateData(FALSE);	
	
}

void CTimeConvertTestDlg::OnButtonDfminus() 
{
	UpdateData(TRUE);

	m_nTotal -= atoi(m_szValue1);
	m_szValue5.Format("%d", m_nTotal);
	
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonDfplus() 
{
	UpdateData(TRUE);

	m_nTotal += atoi(m_szValue1);
	m_szValue5.Format("%d", m_nTotal);
	
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonDfusetotal() 
{
	UpdateData(TRUE);
	m_szValue1.Format("%d", m_nTotal);
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonNdfminus() 
{
	UpdateData(TRUE);

	m_nTotal -= atoi(m_szValue3);
	m_szValue5.Format("%d", m_nTotal);
	
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonNdfplus() 
{
	UpdateData(TRUE);

	m_nTotal += atoi(m_szValue3);
	m_szValue5.Format("%d", m_nTotal);
	
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonNdfusetotal() 
{
	UpdateData(TRUE);
	m_szValue3.Format("%d", m_nTotal);
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonZero() 
{
	UpdateData(TRUE);
	m_nTotal = 0;
	m_szValue5.Format("%d", m_nTotal);
	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonDf2ndf() 
{
	UpdateData(TRUE);
	if(m_bDF2NDF)
	{
		int nValue = atoi(m_szValue1);
		m_szValue3.Format("%d", tc.DFMStoNDFMS(nValue));
	}
	else
	{
		int nValue = atoi(m_szValue3);
		m_szValue1.Format("%d", tc.NDFMStoDFMS(nValue));
	}
	UpdateData(FALSE);	
	
}

void CTimeConvertTestDlg::OnSetfocusEdit1() 
{
	m_bDF2NDF = true;
	GetDlgItem(IDC_BUTTON_DF2NDF)->SetWindowText("df->ndf ms");
}

void CTimeConvertTestDlg::OnSetfocusEdit3() 
{
	m_bDF2NDF = false;
	GetDlgItem(IDC_BUTTON_DF2NDF)->SetWindowText("ndf->df ms");
}

void CTimeConvertTestDlg::OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	if(pNMUpDown)
	{
		if(pNMUpDown->iDelta<0)
		{
			m_bDF2NDF = false;
			OnButtonDf2ndf();
		}
		else
		if(pNMUpDown->iDelta>0)
		{
			m_bDF2NDF = true;
			OnButtonDf2ndf();
		}
		// if zero do nothing

	}
	
	*pResult = 0;
}

void CTimeConvertTestDlg::OnStaticDfFrames() 
{
	UpdateData(TRUE);

	int ms = atoi(m_szDFFrames);
	m_szValue1.Format("%d", tc.FtoMS(ms, 29.97));

	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnStaticNdfFrames() 
{
	UpdateData(TRUE);

	int ms = atoi(m_szNDFFrames);
	m_szValue3.Format("%d", tc.FtoMS(ms, 30.0));

	UpdateData(FALSE);	
}

void CTimeConvertTestDlg::OnButtonDffr() 
{
	OnStaticDfFrames(); 
}

void CTimeConvertTestDlg::OnButtonNdffr() 
{
	OnStaticNdfFrames();
}
