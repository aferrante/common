; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTimeConvertTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "TimeConvertTest.h"

ClassCount=3
Class1=CTimeConvertTestApp
Class2=CTimeConvertTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_TIMECONVERTTEST_DIALOG

[CLS:CTimeConvertTestApp]
Type=0
HeaderFile=TimeConvertTest.h
ImplementationFile=TimeConvertTest.cpp
Filter=N

[CLS:CTimeConvertTestDlg]
Type=0
HeaderFile=TimeConvertTestDlg.h
ImplementationFile=TimeConvertTestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_BUTTON_NDFFR

[CLS:CAboutDlg]
Type=0
HeaderFile=TimeConvertTestDlg.h
ImplementationFile=TimeConvertTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_TIMECONVERTTEST_DIALOG]
Type=1
Class=CTimeConvertTestDlg
ControlCount=30
Control1=IDOK,button,1208025089
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_GO,button,1342242816
Control4=IDC_EDIT1,edit,1350631552
Control5=IDC_BUTTON_CONVERT,button,1342242816
Control6=IDC_EDIT2,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_BUTTON_CONVERT2,button,1342242816
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT3,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_BUTTON_CONVERTNDF,button,1342242816
Control13=IDC_EDIT4,edit,1350631552
Control14=IDC_STATIC,static,1342308352
Control15=IDC_BUTTON_CONVERTNDF2MS,button,1342242816
Control16=IDC_EDIT5,edit,1350633600
Control17=IDC_STATIC,static,1342308352
Control18=IDC_BUTTON_DFPLUS,button,1342242816
Control19=IDC_BUTTON_DFMINUS,button,1342242816
Control20=IDC_BUTTON_NDFPLUS,button,1342242816
Control21=IDC_BUTTON_NDFMINUS,button,1342242816
Control22=IDC_BUTTON_DFUSETOTAL,button,1342242816
Control23=IDC_BUTTON_NDFUSETOTAL,button,1342242816
Control24=IDC_BUTTON_ZERO,button,1342242816
Control25=IDC_BUTTON_DF2NDF,button,1073741824
Control26=IDC_SPIN2,msctls_updown32,1342177312
Control27=IDC_STATIC_DF_FRAMES,static,1342308352
Control28=IDC_STATIC_NDF_FRAMES,static,1342308352
Control29=IDC_BUTTON_DFFR,button,1342242816
Control30=IDC_BUTTON_NDFFR,button,1342242816

