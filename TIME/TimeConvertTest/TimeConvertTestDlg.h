// TimeConvertTestDlg.h : header file
//

#if !defined(AFX_TIMECONVERTTESTDLG_H__3E7B5E38_5587_4771_9F14_27E8160B41AD__INCLUDED_)
#define AFX_TIMECONVERTTESTDLG_H__3E7B5E38_5587_4771_9F14_27E8160B41AD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CTimeConvertTestDlg dialog

class CTimeConvertTestDlg : public CDialog
{
// Construction
public:
	CTimeConvertTestDlg(CWnd* pParent = NULL);	// standard constructor

	bool  m_bGoing;
	FILE* fp;
	int m_nNumSamples;
	double m_dblDiff;

	int m_nTotal;
	bool m_bDF2NDF;

// Dialog Data
	//{{AFX_DATA(CTimeConvertTestDlg)
	enum { IDD = IDD_TIMECONVERTTEST_DIALOG };
	CString	m_szValue1;
	CString	m_szValue2;
	CString	m_szValue3;
	CString	m_szValue4;
	CString	m_szValue5;
	CString	m_szDFFrames;
	CString	m_szNDFFrames;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeConvertTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL



// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTimeConvertTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonGo();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonConvert();
	afx_msg void OnButtonConvert2();
	afx_msg void OnButtonConvertndf();
	virtual void OnOK();
	afx_msg void OnButtonConvertndf2ms();
	afx_msg void OnButtonDfminus();
	afx_msg void OnButtonDfplus();
	afx_msg void OnButtonDfusetotal();
	afx_msg void OnButtonNdfminus();
	afx_msg void OnButtonNdfplus();
	afx_msg void OnButtonNdfusetotal();
	afx_msg void OnButtonZero();
	afx_msg void OnButtonDf2ndf();
	afx_msg void OnSetfocusEdit1();
	afx_msg void OnSetfocusEdit3();
	afx_msg void OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnStaticDfFrames();
	afx_msg void OnStaticNdfFrames();
	afx_msg void OnButtonDffr();
	afx_msg void OnButtonNdffr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMECONVERTTESTDLG_H__3E7B5E38_5587_4771_9F14_27E8160B41AD__INCLUDED_)
