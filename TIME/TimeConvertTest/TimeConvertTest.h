// TimeConvertTest.h : main header file for the TIMECONVERTTEST application
//

#if !defined(AFX_TIMECONVERTTEST_H__3F8900F3_7527_48F9_BDAA_CEA57224CBDA__INCLUDED_)
#define AFX_TIMECONVERTTEST_H__3F8900F3_7527_48F9_BDAA_CEA57224CBDA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTimeConvertTestApp:
// See TimeConvertTest.cpp for the implementation of this class
//

class CTimeConvertTestApp : public CWinApp
{
public:
	CTimeConvertTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeConvertTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTimeConvertTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMECONVERTTEST_H__3F8900F3_7527_48F9_BDAA_CEA57224CBDA__INCLUDED_)
