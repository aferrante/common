//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TimeConvertTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TIMECONVERTTEST_DIALOG      102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_GO                   1000
#define IDC_EDIT1                       1001
#define IDC_BUTTON_CONVERT              1002
#define IDC_EDIT2                       1003
#define IDC_BUTTON_CONVERT2             1004
#define IDC_EDIT3                       1005
#define IDC_BUTTON_CONVERTNDF           1006
#define IDC_EDIT4                       1007
#define IDC_BUTTON_CONVERTNDF2MS        1008
#define IDC_EDIT5                       1009
#define IDC_BUTTON_DFPLUS               1010
#define IDC_BUTTON_DFMINUS              1011
#define IDC_BUTTON_NDFPLUS              1012
#define IDC_BUTTON_NDFMINUS             1013
#define IDC_BUTTON_DFUSETOTAL           1014
#define IDC_BUTTON_NDFUSETOTAL          1015
#define IDC_BUTTON_ZERO                 1016
#define IDC_BUTTON_DF2NDF               1017
#define IDC_SPIN2                       1020
#define IDC_STATIC_DF_FRAMES            1021
#define IDC_STATIC_NDF_FRAMES           1022
#define IDC_BUTTON_DFFR                 1023
#define IDC_BUTTON_NDFFR                1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
