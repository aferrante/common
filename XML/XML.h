// XML.h: interface for the CXML class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XML_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_)
#define AFX_XML_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define XML_SUCCESS				0
#define XML_ERROR					-1
#define XML_INPROGRESS		-2
#define XML_BADARGS				-3
#define XML_MEMEX					-4
#define XML_UNSUPPORTED		-256


class CXML  
{
public:
	char* m_pszXMLString; 

public:
	CXML();
	virtual ~CXML();

	int SetObject(char* pszXMLString);

};

#endif // !defined(AFX_XML_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_)
