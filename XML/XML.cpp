// XML.cpp: implementation of the CXML class.
//
//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <string.h>
#include "XML.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXML::CXML()
{
	m_pszXMLString = NULL;
}

CXML::~CXML()
{
	if(m_pszXMLString) free(m_pszXMLString);
}

int CXML::SetObject(char* pszXMLString)
{
	if((pszXMLString)&&(strlen(pszXMLString)))
	{
		if(m_pszXMLString) free(m_pszXMLString);
		m_pszXMLString = (char*)malloc(strlen(pszXMLString)+1);
		if(m_pszXMLString)
		{
			strcpy(m_pszXMLString, pszXMLString);
			return XML_SUCCESS;
		}
	}
	return XML_ERROR;
}

