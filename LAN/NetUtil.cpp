// NetUtil.cpp: implementation of the CNetUtil and support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>  //remove this
#include <process.h>
#include "NetUtil.h"
#include "../TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// CNetData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNetData::CNetData()
{
	m_ucType		= NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
	m_ucCmd			= NET_CMD_NULL;       // the command byte
	m_ucSubCmd	= NET_CMD_NULL;    // the subcommand byte - if used with commmand byte gives up to 65535 possible commands - good enough for all practicality.  more than this can be accomplished with the attached data payload
	m_pucData		= NULL;     // pointer to the payload data
	m_ulDataLen = 0;  // length of the payload data buffer.
	m_pszUser   = NULL;    // pointer to the username, if the protocol supports it - zero terminated
	m_pszPassword = NULL; // pointer to the password, if the protocol supports it - zero terminated
}

CNetData::~CNetData()
{
	if(m_pucData) free(m_pucData);  //must use malloc.
	if(m_pszUser) free(m_pszUser);  //must use malloc.
	if(m_pszPassword) free(m_pszPassword);  //must use malloc.
}



//////////////////////////////////////////////////////////////////////
// CNetServer Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNetServer::CNetServer()
{
	m_usPort = NET_PORT_INVALID;					// port on which the server listens
	m_ucType = NET_TYPE_PROTOCOL1;				// defined type - indicates which protocol to use, structure of data
	m_pszName = NULL;											// name of the server, for human readability
	m_ulConnections = 0;									// number of simultaneous connections
	m_ulThreadControl = NET_CTRL_START;		// commands to the server thread
	m_ulServerStatus = NET_STATUS_NONE;		// status back from thread
	m_pszStatus = NULL;										// status buffer with error messages from thread
	m_lpfnHandler = ServerHandlerThread;	// pointer to the thread that handles the request.
	m_lpObject = NULL;										// pointer to the object passed to the handler thread.
	m_lpMsgObj = NULL;											// pointer to the Message function.

	// default parameters for TCPIP
	m_nAf=AF_INET;
	m_nType=SOCK_STREAM;
	m_nProtocol=IPPROTO_IP;
}

CNetServer::~CNetServer()
{
	m_ulThreadControl=NET_CTRL_KILL;  // kills the thread and server

	if(m_pszName) free(m_pszName);  //must use malloc.
	if(m_pszStatus) free(m_pszStatus);  //must use malloc.

	// this bit of code might be dangerous if it hangs forever.  should put in a timeout someday
	while((m_ulServerStatus&NET_STATUS_THREADSTARTED)&&(m_ulConnections>0)) Sleep(1);  // let the server thread end if necessary
}



//////////////////////////////////////////////////////////////////////
// CNetUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNetUtil::CNetUtil(bool bInitialize)
{
	m_bNetStarted = false;
	m_ppNetServers = NULL;
	m_usNumServers = 0;
	m_ulFlags=0;

	if(bInitialize)
		Initialize();
}

CNetUtil::~CNetUtil()
{
	if((m_ppNetServers)&&(m_usNumServers>0))
	{
		for(unsigned short i=0; i<m_usNumServers; i++)
		{
			if(m_ppNetServers[i]) delete m_ppNetServers[i];
		}
		delete [] m_ppNetServers;
	}

  if(m_bNetStarted)  // for this instance
		WSACleanup();  // clean up
}

CNetUtil::Initialize(char* pszInfo)
{
  int nReturnCode = NET_SUCCESS;
	// start winsock for every instance
	if(!m_bNetStarted)
	{
		WSADATA wd;
		int nError = WSAStartup(MAKEWORD(2,2), &wd);
		if ( nError != 0 )
		{
			// failed 
			if(pszInfo) strcpy(pszInfo, "Initialize: Error starting Winsock.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "Initialize: Error starting Winsock.", "CNetUtil:Initialize");
			nReturnCode = NET_ERROR;
		}
		else
		{
			m_bNetStarted = true;
			if(pszInfo) strcpy(pszInfo, "Initialize: Winsock started.");
		}
	}
	else if(pszInfo) strcpy(pszInfo, "Initialize: Winsock already started.");

	return nReturnCode;
}

int CNetUtil::OpenConnection(char* pchHost, short nPort, SOCKET* ps, unsigned long ulSendTimeoutMilliseconds, unsigned long ulReceiveTimeoutMilliseconds, char* pszInfo, int nAf, int nType, int nProtocol) 
{
  int nLen, nReturnCode = NET_SUCCESS;
	register struct hostent *hp = NULL;
	char hostnamebuf[80], hostname[100];
  SOCKET socketReturn;
	struct sockaddr_in saddr;
	char* pchHostInternal = pchHost;
	int nLine;

	nLen = strlen(pchHostInternal);
	if(nLen<=0) 
	{
		if(pszInfo) strcpy(pszInfo, "OpenConnection: Invalid host.");
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "OpenConnection: Invalid host.", "CNetUtil:OpenConnection");
		return NET_ERROR_HOST_INVALID;
	}

	//trim host name
	while((pchHostInternal<pchHostInternal+nLen)&&(isspace(pchHostInternal[0]))) {pchHostInternal++; nLen--;}  //trims left
	while((nLen>0)&&(isspace(pchHostInternal[nLen-1]))) {pchHostInternal[nLen-1]=0; nLen--;}  //trims right

	if(nLen<=0) 
	{
		if(pszInfo) strcpy(pszInfo, "OpenConnection: Invalid host.");
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "OpenConnection: Invalid host.", "CNetUtil:OpenConnection");
		return NET_ERROR_HOST_INVALID;
	}

	memset(((char *)&saddr), 0, (sizeof (saddr)));
	saddr.sin_addr.s_addr = inet_addr(pchHostInternal);
	if (saddr.sin_addr.s_addr != -1) 
	{
		saddr.sin_family = nAf;
		(void) strncpy(hostnamebuf, pchHostInternal, sizeof(hostnamebuf));
	} 
	else 
	{
		hp = gethostbyname(pchHostInternal);

		if (hp == NULL) 
		{
			nReturnCode = WSAGetLastError();
			char* pchError = WinsockEnglish(nReturnCode);
			if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection: Invalid host. %s", pchError?pchError:"");
			if(m_pmsgr)
			{
				char errorstring[NET_ERRORSTRING_LEN];
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection: Invalid host. %s", pchError?pchError:"");
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
			}
			if(pchError) LocalFree(pchError);
			*ps = NULL;
			return NET_ERROR_HOST_INVALID;
		}
		saddr.sin_family = hp->h_addrtype;
		memcpy((char*)&saddr.sin_addr, hp->h_addr_list[0], hp->h_length);
		strncpy(hostnamebuf, hp->h_name, sizeof(hostnamebuf));
	}

	strcpy(hostname, hostnamebuf);
	
	socketReturn = (SOCKET) socket(saddr.sin_family, nType, nProtocol); nLine = __LINE__;
	if (socketReturn == INVALID_SOCKET) 
	{
		nReturnCode = WSAGetLastError();
		char* pchError = WinsockEnglish(nReturnCode);
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error %d. %s", nLine, nReturnCode, pchError?pchError:"");
		if(m_pmsgr)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error %d. %s", nLine, nReturnCode, pchError?pchError:"");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
		}
		if(pchError) LocalFree(pchError);
		*ps = NULL;
		return NET_ERROR_SOCKET_CREATE;
	}

	saddr.sin_port = htons((u_short) nPort);

	while (connect((SOCKET) socketReturn, (struct sockaddr FAR *) &saddr, (int) sizeof (saddr)) != 0)
	{
		// try alias
		if (hp && hp->h_addr_list[1]) 
		{
			hp->h_addr_list++;
			memcpy((char*)&saddr.sin_addr, hp->h_addr_list[0], hp->h_length);
			shutdown (socketReturn, SD_BOTH);
			closesocket(socketReturn);
			socketReturn = (SOCKET) socket(saddr.sin_family, SOCK_STREAM, 0); nLine = __LINE__;
			if (socketReturn == INVALID_SOCKET)
			{
				shutdown (socketReturn, SD_BOTH);
				closesocket(socketReturn);
				nReturnCode = WSAGetLastError();
				char* pchError = WinsockEnglish(nReturnCode);
				if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error %d. %s", nLine, nReturnCode, pchError?pchError:"");
				if(m_pmsgr)
				{
					char errorstring[NET_ERRORSTRING_LEN];
					_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error %d. %s", nLine, nReturnCode, pchError?pchError:"");
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
				}
				if(pchError) LocalFree(pchError);
				*ps = NULL;
				return NET_ERROR_SOCKET_CREATE;
			}
			continue;
		}
		
		shutdown (socketReturn, SD_BOTH);
		closesocket(socketReturn);
		nReturnCode = WSAGetLastError();
		char* pchError = WinsockEnglish(nReturnCode);
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error %d. %s", nLine, nReturnCode, pchError?pchError:"");
		if(m_pmsgr)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error %d. %s", nLine, nReturnCode, pchError?pchError:"");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
		}
		if(pchError) LocalFree(pchError);
		*ps = NULL;
		return NET_ERROR_SOCKET_CREATE;
	}

/*  // no need to get the name
	nLen = sizeof (sockaddr_in);
	if (getsockname(socketReturn, (struct sockaddr *)&saddr, &nLen) != 0) 
	{ 
		shutdown (socketReturn, SD_BOTH);
		closesocket(socketReturn);
		*ps = NULL;
		return ERROR_SOCKET_GETSOCKNAME;
	}
*/
	*ps = socketReturn;

/*
  // timeouts are not supported 
	BSD options not supported for setsockopt are:
	SO_ACCEPTCONN	Socket is listening 
	SO_RCVLOWAT	Receive low water mark 
	SO_RCVTIMEO		Receive time-out 
	SO_SNDLOWAT		Send low water mark 
	SO_SNDTIMEO		Send time-out 
	SO_TYPE				Type of the socket 
*/

  nLen = setsockopt(socketReturn, SOL_SOCKET, SO_SNDTIMEO, (char*)&ulSendTimeoutMilliseconds, sizeof(ulSendTimeoutMilliseconds));
  if (nLen != NO_ERROR) 
	{
		nReturnCode = NET_SOFTERROR_SOCKET_SETSOCKOPTS;
	}	
  nLen = setsockopt(socketReturn, SOL_SOCKET, SO_RCVTIMEO, (char*)&ulReceiveTimeoutMilliseconds, sizeof(ulReceiveTimeoutMilliseconds));
  if (nLen != NO_ERROR) 
	{
		nReturnCode = NET_SOFTERROR_SOCKET_SETSOCKOPTS;
	}	
	return nReturnCode;
}



//  Close an open connection 
int CNetUtil::CloseConnection(SOCKET s, char* pszInfo) 
{
	if((s != INVALID_SOCKET )&&(s != SOCKET_ERROR))
	{
		shutdown (s, SD_BOTH);
		closesocket(s);
		if(pszInfo) strcpy(pszInfo, "CloseConnection: Socket closed.");
	}
	else
	{
		if(pszInfo) strcpy(pszInfo, "CloseConnection: Invalid socket.");
		Message(MSG_ICONERROR, "CloseConnection: Invalid socket.", "CNetUtil:CloseConnection");
	}
	s = NULL; //reset the socket.
  return NET_SUCCESS;
}

int CNetUtil::SendLine(unsigned char* pucBuffer, unsigned long ulBufferLen, SOCKET socket, char chEolnType, bool bDestroyBuffer, unsigned long ulTimeoutMilliseconds, char* pszInfo)
{
	if((pucBuffer == NULL)||(socket==NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode=NET_SUCCESS;

	unsigned char* pucData = pucBuffer;
	unsigned long	ulSize=0, ulLen=0;
	unsigned long ulTotalSize=0;
	int nLine;

	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}


	char errorstring[NET_ERRORSTRING_LEN];
	int nStartTime;
	do
	{
		nStartTime=clock();
		if((m_pmsgr)&&(m_ulFlags&NET_FLAGS_LOGTIMING_SEND))
		{
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "SendLine: sending %d bytes", ulBufferLen-ulTotalSize);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:SendLine");
		}
		ulSize = send(socket, (char*)(pucData+ulTotalSize), ulBufferLen-ulTotalSize,0); nLine = __LINE__;
		_ftime( &now );
		if((m_pmsgr)&&(m_ulFlags&NET_FLAGS_LOGTIMING_SEND))
		{
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "SendLine: sent %d bytes in %d ms", ulBufferLen-ulTotalSize, clock()-nStartTime);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:SendLine");
		}

		if(ulSize != SOCKET_ERROR)
		{
			ulTotalSize+=ulSize;
			// reset the timeout for the next send if necessary
			if(ulTotalSize < ulBufferLen)
			{
				_ftime( &timebuffer );

				timebuffer.time += (ulTimeoutMilliseconds/1000);
				timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
				if(timebuffer.millitm>999)
				{
					timebuffer.millitm-=1000;
					timebuffer.time++;
				}
			}
		}
		else
		{
 			nReturnCode = WSAGetLastError();
			char* pchError = WinsockEnglish(nReturnCode);
			if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "SendLine(%d): send error %d. %s", nLine, nReturnCode, pchError?pchError:"");
			if(m_pmsgr)
			{
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "SendLine(%d): send error %d. %s", nLine, nReturnCode, pchError?pchError:"");
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:SendLine");
			}
			if( (nReturnCode == WSAECONNABORTED)
				||(nReturnCode == WSAECONNRESET)
				||(nReturnCode == WSAENOTCONN)
				||(nReturnCode == WSAESHUTDOWN)
				)
				nReturnCode = NET_ERROR_CONN;
			else
				nReturnCode = NET_ERROR_SEND;
			if(pchError) LocalFree(pchError);
			return nReturnCode; 
		}
	} while ( (ulTotalSize < ulBufferLen)
					&&( (now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))) );

	char buffer[4] = {13,10,0,0};

  switch(chEolnType)
  {
	case EOLN_NULL:	send(socket, buffer+2,	1, 0); break;
	case EOLN_DNUL:	send(socket, buffer+2,	2, 0); break;
  case EOLN_CR:   send(socket, buffer,		1, 0); break;
  case EOLN_LF:   send(socket, buffer+1,	1, 0); break;
  case EOLN_HTTP: send(socket, buffer,		2, 0);
  case EOLN_CRLF: send(socket, buffer,		2, 0); break;
  case EOLN_NONE: 
  default: break;
  }

	if(bDestroyBuffer) free(pucBuffer);

	return nReturnCode;
}

// if GetLine uses EOLN_TOKEN, the token must be passed in with pszInfo
int CNetUtil::GetLine(unsigned char** ppucBuffer, unsigned long* pulBufferLen, SOCKET socket, unsigned long ulFlags, char* pszInfo)
{
	if((pulBufferLen == NULL)||(ppucBuffer == NULL)||(socket==NULL)||(socket==INVALID_SOCKET)) 
		return NET_ERROR_INVALID_PARAM;

	if((ulFlags&NET_RCV_EOLN)&&((ulFlags&EOLN_MASK)==EOLN_TOKEN)&&((pszInfo==NULL)||(strlen(pszInfo)<=0)))// end of line check
		return NET_ERROR_INVALID_PARAM;

	int nReturnCode=NET_SUCCESS;

	unsigned char* pch = NULL;
	unsigned char* pchData=NULL;
	unsigned long	ulCommBufSize = NET_COMMBUFFERSIZE; //comm buffer size
	unsigned long	ulBufLen = NET_COMMBUFFERSIZE; //initial buffer size
	unsigned long ulMulti=1;
	unsigned long	ulSize=0, ulLen=0;
	unsigned long ulTotalSize=0;
	int nLine;
	bool bDone = false;
	bool bEOLN = false;
	unsigned char buffer[6] = {13,10,13,10,0,0}; 
	CBufferUtil bu;
	char errorstring[NET_ERRORSTRING_LEN];

	if(ulFlags&NET_RCV_LENGTH) ulBufLen = *pulBufferLen;
//	if(ulFlags&NET_RCV_EOLN) ulCommBufSize = 1;  // check every char
	do
	{
		if(!(ulFlags&NET_RCV_LENGTH)) ulBufLen = (ulMulti*ulCommBufSize);
		if(ulFlags&NET_RCV_IN_BUF)
			pchData = *ppucBuffer;
		else
			pchData = (unsigned char*)malloc(ulBufLen+1);// add one for a final zero that we can disregard 
		if(pchData==NULL)
		{
			if(pszInfo) strcpy(pszInfo, "GetLine: could not allocate receive buffer.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "GetLine: could not allocate receive buffer.", "CNetUtil:GetLine");
			return NET_ERROR_BUFFER;
		}
		if(pch!=NULL)
		{
			if(ulTotalSize>0) memcpy(pchData, pch, ulTotalSize);
			free(pch);
		}
		pch = pchData;
		int nStartTime;
		do
		{
			nStartTime=clock();
			if((m_pmsgr)&&(m_ulFlags&NET_FLAGS_LOGTIMING_RECV))
			{
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "GetLine: receiving");
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:GetLine");
			}
			ulSize = recv(socket, (char*)(pch+ulTotalSize), ulBufLen-ulTotalSize, 0);  nLine = __LINE__;
			if((m_pmsgr)&&(m_ulFlags&NET_FLAGS_LOGTIMING_RECV))
			{
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "GetLine: received %d bytes in %d ms", ulSize, clock()-nStartTime);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:GetLine");
			}


			if((ulSize != 0) && (ulSize != SOCKET_ERROR))
			{
				ulTotalSize+=ulSize;
				if(ulFlags&NET_RCV_EOLN)  // end of line check
				{
					// strstr could be used, but its possible that the incoming data is not text, and a zero char exists
					switch(ulFlags&EOLN_MASK)
					{
					case EOLN_NULL:	if(bu.memrncmp(pch, ulTotalSize, buffer+4,	1)) bEOLN = true; break;
					case EOLN_DNUL:	if(bu.memrncmp(pch, ulTotalSize, buffer+4,	2)) bEOLN = true; break;	
					case EOLN_CR:		if(bu.memrncmp(pch, ulTotalSize, buffer,		1)) bEOLN = true; break;
					case EOLN_LF:		if(bu.memrncmp(pch, ulTotalSize, buffer+1,	1)) bEOLN = true; break;
					case EOLN_CRLF:	if(bu.memrncmp(pch, ulTotalSize, buffer,		2)) bEOLN = true; break;
					case EOLN_HTTP:	if(bu.memrncmp(pch, ulTotalSize, buffer,		4)) bEOLN = true; break;
					case EOLN_TOKEN:	if(bu.memrncmp(pch, ulTotalSize, (unsigned char*) pszInfo,	strlen(pszInfo))) bEOLN = true; break;
					case EOLN_NONE:
					default: break;
					}
				}

				if((ulFlags&NET_RCV_ONCE)&&(ulTotalSize<ulBufLen)) bDone = true;  // we might need more calls to recv to get the rest if the data size is large
			}
		} while ( (!bDone) && (!bEOLN) && (ulSize != 0) && (ulSize != SOCKET_ERROR) && (ulTotalSize < ulBufLen) );
		ulMulti++;

	} while ( 
						 (!bDone) && (ulSize != 0) && (ulSize != SOCKET_ERROR)
					&& (!(ulFlags&(NET_RCV_LENGTH|NET_RCV_IN_BUF)))  // dont repeat if expected value, or if a buffer is passed in
					&& (!bEOLN)
					); 

	if(ulSize == SOCKET_ERROR)
	{
 		nReturnCode = WSAGetLastError();
		if(nReturnCode!=0)  // 0 is success.  we get SOCKET_ERROR and 0 if the conncetion closes.
		{
//			char buffer[256]; sprintf(buffer, "%d rc", nReturnCode);//AfxMessageBox(buffer);
			char* pchError = WinsockEnglish(nReturnCode);//AfxMessageBox(pchError);
			if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "GetLine(%d): recv error %d. %s", nLine, nReturnCode, pchError?pchError:"");
			if(m_pmsgr)
			{
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "GetLine(%d): recv error %d. %s", nLine, nReturnCode, pchError?pchError:"");
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:GetLine");
			}

			if( (nReturnCode == WSAECONNABORTED)
				||(nReturnCode == WSAECONNRESET)
				||(nReturnCode == WSAENOTCONN)
				||(nReturnCode == WSAESHUTDOWN)
				)
				nReturnCode = NET_ERROR_CONN;
			else
				nReturnCode = NET_ERROR_INCOMPLETEDATA;
			if(pchError) LocalFree(pchError);
		}
		else nReturnCode = NET_SUCCESS;
		if((pch)&&(!(ulFlags&NET_RCV_IN_BUF))) free(pch);
		pch = NULL;
		ulTotalSize = 0;
	}
	else
	{
		if(pch)
		{
			*(pch+ulTotalSize)=0;// zero term in case, but not part of what is returned in pulBufferLen, so should be safe.
		}
		else ulTotalSize=0;			
	}
	*ppucBuffer = pch;
	*pulBufferLen = ulTotalSize;
	return nReturnCode;
}


int  CNetUtil::StartServer(CNetServer* pServer, void* pMessagingObject, unsigned long ulTimeoutMilliseconds, char* pszInfo)
{
	if((pServer == NULL)||(pServer->m_usPort == NET_PORT_INVALID)||(pServer->m_lpfnHandler == NULL)) return NET_ERROR_INVALID_PARAM;

	//check to make sure its not already started:
	if(GetServerIndex(pServer->m_usPort)>=0) return NET_SOFTERROR_ALREADYSTARTED;

	int nReturnCode=NET_SUCCESS;

	pServer->m_ulThreadControl=NET_CTRL_START;
	pServer->m_ulServerStatus=NET_STATUS_NONE;
	pServer->m_lpMsgObj = pMessagingObject;  // set to NULL to disable


	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	// try to start it first
	if(_beginthread(ServerListenerThread, 0, (void*)pServer)==-1)
	{
 		if(pszInfo) strcpy(pszInfo, "StartServer: error creating listener thread.");
		Message(MSG_PRI_CRITICAL|MSG_ICONERROR, "StartServer: error creating listener thread.", "CNetUtil:StartServer");
		return NET_ERROR;
	}

	_ftime( &now );
	
	while((!(pServer->m_ulServerStatus&NET_STATUS_SERVERSTARTED))
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		Sleep(1);
		_ftime( &now );
	}

	if(pServer->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
	{
		// add it to the register.
		CNetServer** ppNetServers = new CNetServer*[m_usNumServers+1]; // array of pointers
		if(ppNetServers!=NULL)
		{
			CNetServer** ppNetServersTemp = m_ppNetServers;
			if(m_ppNetServers!=NULL)
			{
				memcpy(ppNetServers, m_ppNetServers, sizeof(CNetServer*)*(m_usNumServers));
			}

			m_ppNetServers = ppNetServers;

			if(ppNetServersTemp!=NULL)
			{
				delete [] ppNetServersTemp;
				ppNetServersTemp = NULL;
			}


			m_ppNetServers[m_usNumServers] = pServer;

			m_usNumServers++;
		}
		else  // we couldn't add this to the register! but, let the caller deal with the object.
		{
			// send an error message out
			if(pszInfo) strcpy(pszInfo, "StartServer: Could not allocate memory for server register.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "StartServer: Could not allocate memory for server register.", "CNetUtil:StartServer");
			nReturnCode=NET_ERROR_BUFFER;
		}
	}
	else
	{
		// we have to set the thing to kill itself in case it does ever get started.
		pServer->m_ulThreadControl=NET_CTRL_KILL;

		// send an error message out
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "StartServer: Could not start server. (0x%08x) %s", pServer->m_ulServerStatus, ((pServer->m_pszStatus!=NULL)&&(strlen(pServer->m_pszStatus)>0))?pServer->m_pszStatus:"");
		if(m_pmsgr)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "StartServer: Could not start server. (0x%08x) %s", pServer->m_ulServerStatus, ((pServer->m_pszStatus!=NULL)&&(strlen(pServer->m_pszStatus)>0))?pServer->m_pszStatus:"");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:StartServer");
		}

		nReturnCode=NET_ERROR;
	}

	return nReturnCode;
}

//Stop server deletes the server object as well, but only if there is no error.
int  CNetUtil::StopServer(unsigned short usPort, unsigned long ulTimeoutMilliseconds, char* pszInfo)
{
	if(usPort == NET_PORT_INVALID) return NET_ERROR_INVALID_PARAM;

	//check to make sure its already started:
	int nReturnCode=0;
	int nIndex=GetServerIndex(usPort);
	if(nIndex<0) return NET_SOFTERROR_ALREADYSTOPPED;
	else
	if(!(m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED))
	{
		// but still remove this object from the list.
		nReturnCode = NET_SOFTERROR_ALREADYSTOPPED;
	}

	m_ppNetServers[nIndex]->m_ulThreadControl=NET_CTRL_KILL;

	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	_ftime( &now );
	while((m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		Sleep(1);
		_ftime( &now );
	}

	if(!(m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED))
	{
		// delete it from the register.

		CNetServer* pNetServerTemp = m_ppNetServers[nIndex];

		unsigned short i=nReturnCode;
		while(i<m_usNumServers-1)
		{
			m_ppNetServers[i] = m_ppNetServers[++i];
		}
		
		m_usNumServers--;

		delete pNetServerTemp;  pNetServerTemp=NULL;

		CNetServer** ppNetServers = new CNetServer*[m_usNumServers]; // array of pointers
		if(ppNetServers!=NULL)
		{
			CNetServer** ppNetServersTemp = m_ppNetServers;
			if(m_ppNetServers!=NULL)
			{
				memcpy(ppNetServers, m_ppNetServers, sizeof(CNetServer*)*(m_usNumServers));
			}

			m_ppNetServers = ppNetServers;

			if(ppNetServersTemp!=NULL)
			{
				delete [] ppNetServersTemp;
			}
		}
//		else  // we couldn't add this to the register! but, its OK, we can just let it be in the orig buffer.

		nReturnCode=NET_SUCCESS;
	}
	else
	{
		// send an error message out, and dont remove it from list (yet)
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "StopServer: Could not stop server. (0x%08x) %s", m_ppNetServers[nIndex]->m_ulServerStatus, ((m_ppNetServers[nIndex]->m_pszStatus!=NULL)&&(strlen(m_ppNetServers[nIndex]->m_pszStatus)>0))?m_ppNetServers[nIndex]->m_pszStatus:"");
		if(m_pmsgr)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "StopServer: Could not stop server. (0x%08x) %s", m_ppNetServers[nIndex]->m_ulServerStatus, ((m_ppNetServers[nIndex]->m_pszStatus!=NULL)&&(strlen(m_ppNetServers[nIndex]->m_pszStatus)>0))?m_ppNetServers[nIndex]->m_pszStatus:"");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:StopServer");
		}

		nReturnCode=NET_ERROR;
	}
	
	return nReturnCode;
}

int  CNetUtil::GetServerIndex(unsigned short usPort)
{
	int nIndex=-1;
	if((m_ppNetServers)&&(m_usNumServers>0))
	{
		nIndex=0;
		bool bFound = false;
		while((nIndex<m_usNumServers)&&(!bFound))
		{
			if((m_ppNetServers[nIndex])&&(m_ppNetServers[nIndex]->m_usPort == usPort))
			{
				bFound = true;
			}
			else
			{
				nIndex++;
			}
		}
		if(!bFound) nIndex=-1;
	}

	return nIndex;
}




// this opens a connection to a host, sends the data packet, receives a mandatory reply, acks if received, and closes the connection
// for persistent connections, use OpenConnection and the other SendData override (and ReceiveData if necessary)
int  CNetUtil::SendData(CNetData* pData, char* pchHost, unsigned short usPort, unsigned long ulTimeoutMilliseconds, unsigned char nRetries, unsigned long ulFlags, SOCKET* psocket, char* pszInfo)
{
	if((pData == NULL)||(pchHost == NULL)||(strlen(pchHost)<=0)||((ulFlags&NET_SND_KEEPOPENLCL)&&(psocket==NULL))) return NET_ERROR_INVALID_PARAM;
	if(psocket) *psocket = NULL;
	int nReturnCode=NET_ERROR;
	SOCKET socket;

	unsigned char i=0;
	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	_ftime( &now );

	while(	(nReturnCode<NET_SUCCESS)&&(i<=nRetries)
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		nReturnCode = OpenConnection(pchHost, usPort, &socket, ulTimeoutMilliseconds, ulTimeoutMilliseconds, pszInfo);  // both ways use same timeout
		if(nReturnCode<NET_SUCCESS) i++;  // dont increment i if not a failure
		_ftime( &now );
	}

	if(nReturnCode>=NET_SUCCESS)
	{
		if(psocket) *psocket = socket;
		// here we should have a valid socket
		// let's create a buffer with the appropriate data.
		unsigned long ulLen=3; //min chksum+type+cmd;

		if(((pData->m_pucData)==NULL)||((pData->m_ulDataLen)==0))
		{
			(pData->m_ucType) &= ~NET_TYPE_HASDATA; //no data to follow.
			(pData->m_ulDataLen)=0;
			if(pData->m_pucData) free(pData->m_pucData);
			(pData->m_pucData) = NULL;
		}
		else 
		{
			(pData->m_ucType) |= NET_TYPE_HASDATA; //data to follow.
			ulLen+=(pData->m_ulDataLen)+4; // 4 bytes for size.

		}

		if((pData->m_ucType)&NET_TYPE_HASSUBC) // subcommand to follow.
			ulLen+=1;

		unsigned char* pch = NULL;
		while(	(pch==NULL)&&(i<=nRetries)
					&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
		{
			pch = (unsigned char*)malloc(ulLen);
			if(pch==NULL) 
			{ 
				nReturnCode = NET_ERROR_BUFFER; 
				i++;  // dont increment i if not a failure
				_ftime( &now );
			}
		}

		if(pch==NULL) 
		{
			// cant continue, no buffer.
			if(pszInfo) strcpy(pszInfo, "SendData: could not allocate buffer");
			Message(MSG_ICONERROR, "SendData: could not allocate buffer", "CNetUtil:SendData");
			if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
			return nReturnCode;
		}
		

		if(ulFlags&NET_SND_KEEPOPENRMT)  //we want to keep the client side open.
			(pData->m_ucType) |= NET_TYPE_KEEPOPEN;  //have to request the server to keep the conn open
		else
			(pData->m_ucType) &= (~NET_TYPE_KEEPOPEN);  //have to request the server to close the connection

		unsigned char* pchInsert = pch+1;
		*(pchInsert++) = pData->m_ucType;  //second byte always gets type
		*(pchInsert++) = pData->m_ucCmd;		//third byte always gets cmd
		if((pData->m_ucType)&NET_TYPE_HASSUBC) 
		{
			*(pchInsert++) = pData->m_ucSubCmd;
		}
		if((pData->m_ucType)&NET_TYPE_HASDATA)
		{
			*(pchInsert++)	= (unsigned char) (((pData->m_ulDataLen) >> 24) & 0xff);
			*(pchInsert++)  = (unsigned char) (((pData->m_ulDataLen) >> 16) & 0xff);
			*(pchInsert++)  = (unsigned char) (((pData->m_ulDataLen) >>  8) & 0xff);
			*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen) & 0xff);
			memcpy(pchInsert, pData->m_pucData, (pData->m_ulDataLen));
		}

		pch[0] = Checksum(pch+1, ulLen-1);

		// buffer is filled, send it
		nReturnCode = NET_ERROR;

		while(	(nReturnCode<NET_SUCCESS)&&(i<=nRetries)
					&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
		{
			nReturnCode = SendLine(pch, ulLen, socket, EOLN_NONE, false, ulTimeoutMilliseconds, pszInfo);
			_ftime( &now );
			if(nReturnCode<NET_SUCCESS) i++;  // dont increment i if not a failure
		}

		if(pch!=NULL) free(pch); //the send buffer is freed

		if((nReturnCode==NET_SUCCESS)&&(!(ulFlags&NET_SND_NO_RX)))
		{
			// the send was a success, so lets get a reply
			nReturnCode = ReceiveData(socket, pData, pszInfo);
			if(nReturnCode>=NET_SUCCESS)  //soft errors OK.
			{
				if(!(ulFlags&NET_SND_NO_RXACK))
				{
					if((pData->m_ucCmd) == NET_CMD_NAK)  // the reply was no goood, but comm still up
						SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_NAK|NET_SND_NO_RX, pszInfo); // ack the nak
					else
						SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo);  //nak.
				}

				if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
			}
			else
			{
				if(!(ulFlags&NET_SND_NO_RXACK))
				{
					// try to send a NAK no matter what.
					SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo); // nak all.
				}
				if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
			}
		}
		else
		{
			if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
		}
	}

	return nReturnCode;
}

// this uses an existing socket, sends the data packet, receives a mandatory reply, and does NOT close the connection -  have to do this explicitly
int  CNetUtil::SendData(CNetData* pData, SOCKET socket, unsigned long ulTimeoutMilliseconds, unsigned char nRetries, unsigned long ulFlags, char* pszInfo)
{
	if(((pData == NULL)&&(!(ulFlags&(NET_SND_ACK|NET_SND_NAK))))||(socket == NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode=NET_ERROR;

	unsigned char i=0;
	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	_ftime( &now );


	// here we should have a valid socket

	if(ulFlags&(NET_SND_ACK|NET_SND_NAK))
	{
		CNetData* pAckData = new CNetData;
		// create an ACK/NAK buffer
		if(pData)
		{
			memcpy(pAckData, pData, sizeof(CNetData));
			(pAckData->m_ucType) &= ~(NET_TYPE_HASDATA|NET_TYPE_HASSUBC);
			(pAckData->m_ucCmd) = (ulFlags&NET_SND_ACK)?NET_CMD_ACK:NET_CMD_NAK;
			(pAckData->m_ucSubCmd) = 0;
			(pAckData->m_ulDataLen) = 0;
			(pAckData->m_pucData) = NULL;				
		}
		else
		{
			(pAckData->m_ucType) = NET_TYPE_PROTOCOL1;
			(pAckData->m_ucCmd) = (ulFlags&NET_SND_ACK)?NET_CMD_ACK:NET_CMD_NAK;
			(pAckData->m_ucSubCmd) = 0;
			(pAckData->m_ulDataLen) = 0;
			(pAckData->m_pucData) = NULL;				
		}

		pData = pAckData;  // the old pData is now ignored.

	}
	// let's create a buffer with the appropriate data.
	unsigned long ulLen=3; //min chksum+type+cmd;

	if(((pData->m_pucData)==NULL)||((pData->m_ulDataLen)==0))	pData->m_ucType&=~NET_TYPE_HASDATA; //no data to follow.
	else 
	{
		pData->m_ucType|=NET_TYPE_HASDATA; //data to follow.
		ulLen+=pData->m_ulDataLen+4; // 4 bytes for size.
	}

	if(pData->m_ucType&NET_TYPE_HASSUBC) // subcommand to follow.
		ulLen+=1;

	unsigned char* pch = NULL;
	while(	(pch==NULL)&&(i<=nRetries)
		&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		pch = (unsigned char*)malloc(ulLen);
		if(pch==NULL) 
		{ 
			nReturnCode = NET_ERROR_BUFFER; 
			i++;  // dont increment i if not a failure
			_ftime( &now );
		}
	}

	if(pch==NULL) 
	{
		// cant continue, no buffer.
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "SendData: could not allocate buffer after %d attempts.%s",
				i, ((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm)))?"":"timed out");
		if(m_pmsgr)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "SendData: could not allocate buffer after %d attempts.%s",
				i, ((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm)))?"":"timed out");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:SendData");
		}

		if(ulFlags&(NET_SND_ACK|NET_SND_NAK)) delete pData;

		return nReturnCode;
	}
	
	if(ulFlags&NET_SND_KEEPOPENRMT)
		(pData->m_ucType)|=NET_TYPE_KEEPOPEN;  //have to request the server to keep the conn open
	else
		(pData->m_ucType)&= (~NET_TYPE_KEEPOPEN);  //have to request the server to close the connection

	unsigned char* pchInsert = pch+1;
	*(pchInsert++) = pData->m_ucType;  //second byte always gets type
	*(pchInsert++) = pData->m_ucCmd;		//third byte always gets cmd
	if((pData->m_ucType)&NET_TYPE_HASSUBC) 
	{
		*(pchInsert++) = pData->m_ucSubCmd;
	}
	if((pData->m_ucType)&NET_TYPE_HASDATA)
	{
		*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen >> 24) & 0xff);
		*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen >> 16) & 0xff);
		*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen >>  8) & 0xff);
		*(pchInsert++)  = (unsigned char) (pData->m_ulDataLen & 0xff);
		memcpy(pchInsert, pData->m_pucData, pData->m_ulDataLen);
	}

	pch[0] = Checksum(pch+1, ulLen-1);

	// buffer is filled, send it
	nReturnCode = NET_ERROR;

	while(	(nReturnCode<NET_SUCCESS)&&(i<=nRetries)
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		nReturnCode = SendLine(pch, ulLen, socket, EOLN_NONE, false, ulTimeoutMilliseconds, pszInfo);
		_ftime( &now );
		if(nReturnCode<NET_SUCCESS) i++;  // dont increment i if not a failure
	}
	
	if(pch!=NULL) free(pch); //the send buffer is freed

	if((nReturnCode==NET_SUCCESS)&&(!(ulFlags&NET_SND_NO_RX)))
	{
		// the send was a success, so lets get a reply
		nReturnCode = ReceiveData(socket, pData, pszInfo);
		if(nReturnCode>=NET_SUCCESS)  //soft errors OK.
		{
			if(!(ulFlags&NET_SND_NO_RXACK))
			{
				if((pData->m_ucCmd) == NET_CMD_NAK)  // the reply was no goood, but comm still up
					SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_NAK|NET_SND_NO_RX, pszInfo); // ack the nak
				else
					SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo);  //nak.
			}
		}
		else
		{
			if(!(ulFlags&NET_SND_NO_RXACK))
			{
				// try to send a NAK no matter what.
				SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo); // nak all.
			}
		}
	
	}
	if(ulFlags&(NET_SND_ACK|NET_SND_NAK)) delete pData;  // if this is just an ack, delete the ack object.

	return nReturnCode;	
}


int  CNetUtil::SendData(
		unsigned char* pucType,				// defined type - indicates which protocol to use, structure of data
		unsigned char* pucCmd,				// the command byte
		unsigned char* pucSubCmd,		  // the subcommand byte - if used with commmand byte gives up to 65535 possible commands - good enough for all practicality.  more than this can be accomplished with the attached data payload
		unsigned char** ppucData,     // pointer to the payload data
		unsigned long* pulDataLen,		// length of the payload data buffer.
		char* pchHost,								// if port is not zero, otherwise the socket in SOCKET* psocket is used (if not NULL)
		unsigned short usPort,        // set to zero to use pchHostOrSocket as a SOCKET*
		unsigned long ulTimeoutMilliseconds, 
		unsigned char nRetries, 
		unsigned long ulFlags, 
		SOCKET* psocket, 
		char* pszInfo)
{
	CNetData data;

	data.m_ucType = *pucType;      // defined type - indicates which protocol to use, structure of data
	data.m_ucCmd = *pucCmd;       // the command byte
	data.m_ucSubCmd = *pucSubCmd;       // the subcommand byte
	data.m_pucData = *ppucData;
	data.m_ulDataLen = *pulDataLen;

	int nReturn = NET_ERROR;
	if(usPort!=0)
	{
		nReturn = SendData(&data, pchHost, usPort, ulTimeoutMilliseconds, nRetries, ulFlags, psocket, pszInfo);
	}
	else
	{
		nReturn = SendData(&data, *psocket, ulTimeoutMilliseconds, nRetries, ulFlags, pszInfo);

	}
	*pucType = data.m_ucType;      // defined type - indicates which protocol to use, structure of data
	*pucCmd = data.m_ucCmd;       // the command byte
	*pucSubCmd = data.m_ucSubCmd;       // the subcommand byte
	*ppucData = data.m_pucData;
	*pulDataLen = data.m_ulDataLen;

	return nReturn;
}



// this receives formatted data on an existing socket
int  CNetUtil::ReceiveData(SOCKET socket, CNetData* pData, char* pszInfo)
{
	if((socket == NULL)||(pData==NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode;
	unsigned char* pchTempBuffer = (unsigned char*) malloc(8);
	if(pchTempBuffer==NULL)
	{
		if(pszInfo) strcpy(pszInfo, "ReceiveData: could not allocate temp buffer.");
		Message(MSG_ICONERROR, "ReceiveData: could not allocate temp buffer.", "CNetUtil:ReceiveData");
		return NET_ERROR_BUFFER;
	}
	unsigned char* pch = NULL;
	unsigned long ulBufferLen = 3;

	if(pData->m_pucData)
	{
		free(pData->m_pucData);
		pData->m_pucData = NULL;
	}
	pData->m_ulDataLen = 0;

	nReturnCode = GetLine(&pchTempBuffer, &ulBufferLen, socket, NET_RCV_ONCE|NET_RCV_LENGTH|NET_RCV_IN_BUF, pszInfo);

	if(nReturnCode==NET_SUCCESS)
	{
		unsigned char chChkSumTotal = Checksum(pchTempBuffer+1, 2);
		unsigned char chChkSum = *pchTempBuffer;
		pData->m_ucType = *(pchTempBuffer+1);
		pData->m_ucCmd =  *(pchTempBuffer+2);
		pData->m_ucSubCmd = 0;

		if((pData->m_ucType)&NET_TYPE_HASSUBC)
		{
			ulBufferLen = 1;
			nReturnCode = GetLine(&pchTempBuffer, &ulBufferLen, socket, NET_RCV_ONCE|NET_RCV_LENGTH|NET_RCV_IN_BUF, pszInfo);

			if(nReturnCode==NET_SUCCESS)
			{
				chChkSumTotal += Checksum(pchTempBuffer, 1);
				pData->m_ucSubCmd = (*pchTempBuffer);
			}
			else
			{
				if(pchTempBuffer!=NULL) free(pchTempBuffer);
				return nReturnCode;
			}
		}

		if((pData->m_ucType)&NET_TYPE_HASDATA)
		{
			ulBufferLen = 4;
			nReturnCode = GetLine(&pchTempBuffer, &ulBufferLen, socket, NET_RCV_ONCE|NET_RCV_LENGTH|NET_RCV_IN_BUF, pszInfo);

			if(nReturnCode==NET_SUCCESS)
			{

				chChkSumTotal += Checksum(pchTempBuffer, 4);
				ulBufferLen = 0;
				for(unsigned char i=0; i<4; i++)
					ulBufferLen = (ulBufferLen<<8) + (unsigned long)((unsigned char)(*(pchTempBuffer+i)));

				pch = NULL;
				nReturnCode = GetLine(&pch, &ulBufferLen, socket, NET_RCV_LENGTH, pszInfo);
				if(nReturnCode==NET_SUCCESS)
				{
					chChkSumTotal += Checksum(pch, ulBufferLen);
					if(chChkSumTotal!=chChkSum)
					{
						if(pszInfo) 
						{
							_snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "ReceiveData: incorrect checksum; %d received, %d calculated", chChkSum, chChkSumTotal);
						}	
						nReturnCode = NET_ERROR_CHECKSUM;
					}
					else
					if((ulBufferLen>2)&&(pch!=NULL)&&(*(pch+2) == NET_CMD_NAK))
					{
						if(pszInfo) 
						{
							strcpy(pszInfo, "ReceiveData: NAK received");
						}	
						nReturnCode = NET_ERROR_NAK;
					}

					pData->m_pucData = (unsigned char*)pch;
					pData->m_ulDataLen = ulBufferLen;

				}
				else
				{
					if(pch) free(pch);
					pData->m_pucData = NULL;
					pData->m_ulDataLen = 0;
				}
			}
			else
			{
				if(pchTempBuffer!=NULL) free(pchTempBuffer);
				return nReturnCode;
			}
		}
	}
	
/*
	pData->m_ulDataLen = ulBufferLen;  // pass in the buffer length in the data object.

	if(nReturnCode==NET_SUCCESS)
		nReturnCode = InterpretData(pucBuffer, pData, true, pszInfo);
*/
	if(pchTempBuffer!=NULL) free(pchTempBuffer);
	return nReturnCode;
}

int  CNetUtil::TranslateSecurity(CNetData* pData, unsigned long ulFlags) // for protocol 2, translates from 2 to 1, then all other functions are usable
{
	if(pData == NULL) return NET_ERROR_INVALID_PARAM;

	if(ulFlags==NET_2TO1)
	{
		if((pData->m_ucType&NET_TYPE_PROTOCOLMASK)!=NET_TYPE_PROTOCOL2)  return NET_ERROR_INVALID_PARAM;

		unsigned long ulDataLen = pData->m_ulDataLen+1;  // extra term zero just in case
		if(pData->m_pszUser) ulDataLen += (strlen(pData->m_pszUser)+1);
		else ulDataLen++; // just add a zero
		if(pData->m_pszPassword) ulDataLen += (strlen(pData->m_pszPassword)+1);
		else ulDataLen++; // just add a zero

		unsigned char* pch = (unsigned char*)malloc(ulDataLen);
		if(pch)
		{
			ulDataLen=0;
			if((pData->m_pszUser)&&(strlen(pData->m_pszUser)))
			{
				memcpy(pch, pData->m_pszUser, ulDataLen);
				ulDataLen=strlen(pData->m_pszUser);
			}
			memset(pch+ulDataLen, 0, 1);
			ulDataLen++;
			if((pData->m_pszPassword)&&(strlen(pData->m_pszPassword)))
			{
				memcpy(pch+ulDataLen, pData->m_pszPassword, strlen(pData->m_pszPassword));
				ulDataLen+=strlen(pData->m_pszPassword);
			}
			memset(pch+ulDataLen, 0, 1);
			ulDataLen++;
			if((pData->m_pucData)&&(pData->m_ulDataLen>0))
			{
				memcpy(pch+ulDataLen, pData->m_pucData, pData->m_ulDataLen);
				ulDataLen+=pData->m_ulDataLen;
			}
			memset(pch+ulDataLen, 0, 1);

			if(pData->m_pszUser) free(pData->m_pszUser);
			pData->m_pszUser = NULL;

			if(pData->m_pszPassword) free(pData->m_pszPassword);
			pData->m_pszPassword = NULL;

			if(pData->m_pucData) free(pData->m_pucData);
			pData->m_pucData = pch;
			pData->m_ulDataLen = ulDataLen;  // does not include the final term 0, just the two delimiters

			pData->m_ucType &= ~NET_TYPE_PROTOCOLMASK;
			pData->m_ucType |=  NET_TYPE_PROTOCOL1;
			return NET_SUCCESS;
		}
		return NET_ERROR_BUFFER;

	}
	else  // must be NET_1TO2
	{
		if((pData->m_ucType&NET_TYPE_PROTOCOLMASK)!=NET_TYPE_PROTOCOL1) return NET_ERROR_INVALID_PARAM;
		//check for the two required delimiters first.
		if((pData->m_pucData==NULL)||(pData->m_ulDataLen<2)) return NET_ERROR_INVALID_PARAM;
		CBufferUtil bu; 
		unsigned long ulDataLen = bu.CountChar((char*)pData->m_pucData, pData->m_ulDataLen, 0);

		if(ulDataLen<2) return NET_ERROR_INCOMPLETEDATA;
		char* pchData = (char*)pData->m_pucData;
		ulDataLen = pData->m_ulDataLen;

			// decompile the thing
		if(pData->m_pszUser) free(pData->m_pszUser);
		pData->m_pszUser = NULL;
		if(pData->m_pszPassword) free(pData->m_pszPassword);
		pData->m_pszPassword = NULL;

		if(strlen(pchData))
		{
			pData->m_pszUser = (char*)malloc(strlen(pchData)+1);
			if(pData->m_pszUser==NULL) return NET_ERROR_BUFFER;
			strcpy(pData->m_pszUser, pchData);
			pchData = strchr(pchData, 0);
		}
		pchData++; // advance to next.

		if(strlen(pchData))
		{
			pData->m_pszPassword = (char*)malloc(strlen(pchData)+1);
			if(pData->m_pszPassword==NULL) return NET_ERROR_BUFFER;
			strcpy(pData->m_pszPassword, pchData);
			pchData = strchr(pchData, 0);
		}

		pchData++; // advance to next.

		ulDataLen = 0;
		unsigned char* pucData = (unsigned char*)pchData;
		while(pucData<(pData->m_pucData+pData->m_ulDataLen))
		{
			*(pData->m_pucData + ulDataLen) = *(pucData);
			pucData++;
			ulDataLen++;
		}
		pData->m_ulDataLen = ulDataLen;
		*(pData->m_pucData + ulDataLen) = 0;

		pData->m_ucType &= ~NET_TYPE_PROTOCOLMASK;
		pData->m_ucType |=  NET_TYPE_PROTOCOL2;
		return NET_SUCCESS;
	}
	
	return NET_SUCCESS;
}


// this fills a CNetData object with info from a buffer
// only destroys buffer if succeeds, and destroy flag is set
// length of data buffer is passed in with pData->m_ulDataLen, this member gets reset.
int  CNetUtil::InterpretData(unsigned char* pucBuffer, CNetData* pData, bool bDestroyBuffer, char* pszInfo)
{
	if((pData == NULL)||(pucBuffer==NULL)||(pData->m_ulDataLen<3)) return NET_ERROR_INVALID_PARAM;
//	if(sizeof(pucBuffer)<pData->m_ulDataLen) return NET_ERROR_INCOMPLETEDATA;
	if(Checksum(pucBuffer+1, pData->m_ulDataLen-1)!= (*pucBuffer)) return NET_ERROR_CHECKSUM;

	unsigned long ulExpectedDataLen = pData->m_ulDataLen-3;  // minus checksum, type, and cmd
	int nReturnCode=NET_SUCCESS;
	unsigned char* pch;
	pData->m_ucType = *(pucBuffer+1);
	pData->m_ucCmd = *(pucBuffer+2);
	if(pData->m_pucData) free(pData->m_pucData);

	if(pData->m_ucType&NET_TYPE_HASSUBC) { pData->m_ucSubCmd= *(pucBuffer+3); pch=pucBuffer+4; ulExpectedDataLen--; } //minus subcmd byte
	else { pData->m_ucSubCmd= NULL; pch=pucBuffer+3; }

	if(pData->m_ucType&NET_TYPE_HASDATA)
	{
		ulExpectedDataLen-=4; //minus size bytes
		pData->m_ulDataLen = 0;
		for(char i=0; i<4; i++)
		{
			pData->m_ulDataLen = (pData->m_ulDataLen<<8) + (unsigned long)((unsigned char)(*(pch++)));
		}

		if(pData->m_ulDataLen>0)
		{
			char* pchData = (char*)malloc(max(pData->m_ulDataLen, ulExpectedDataLen));
			if(pchData)
			{
				memcpy(pchData, pch, min(ulExpectedDataLen, pData->m_ulDataLen));
				pData->m_pucData = (unsigned char*)pchData;
				if(pData->m_ulDataLen>ulExpectedDataLen)
				{
					if(pszInfo) strcpy(pszInfo, "InterpretData: received fewer bytes than indicated.");
					Message(MSG_ICONERROR, "InterpretData: received fewer bytes than indicated", "CNetUtil:InterpretData");
				}
			}
			else
			{ 
				pData->m_pucData = NULL;
				pData->m_ulDataLen = 0;
				if(pszInfo) strcpy(pszInfo, "InterpretData: could not allocate buffer");
				Message(MSG_ICONERROR, "InterpretData: could not allocate buffer", "CNetUtil:InterpretData");
				nReturnCode = NET_ERROR_BUFFER;
			}
		}
		else
		{
			pData->m_pucData = NULL;
		}
	}
	else
	{ 
		pData->m_pucData = NULL;
		pData->m_ulDataLen = 0;
	}
	
	if((bDestroyBuffer)&&(nReturnCode>=NET_SUCCESS)) free(pucBuffer);

	return nReturnCode;

}


char* CNetUtil::WinsockEnglish(int nError)  // must free the buffer when done with it (outside).
{
	char* pch;
	char buffer[2048];
  switch (nError)
	{
	case WSAEINTR:						/*10004*/						strcpy(buffer, "Interrupted function call.  A blocking operation was interrupted by a call to WSACancelBlockingCall."); break;
	case WSAEACCES:						/*10013*/						strcpy(buffer, "Permission denied.  An attempt was made to access a socket in a way forbidden by its access permissions."); break;
	case WSAEFAULT:						/*10014*/						strcpy(buffer, "Bad address.  The system detected an invalid pointer address in attempting to use a pointer argument of a call."); break;
	case WSAEINVAL:						/*10022*/						strcpy(buffer, "Invalid argument.  An invalid argument was supplied. In some instances, this error also refers to the current state of the socket - for instance, calling accept on a socket that is not listening."); break;
	case WSAEMFILE:						/*10024*/						strcpy(buffer, "Too many open files.  Too many open sockets.  Each implementation may have a maximum number of socket handles available, either globally, per process, or per thread."); break;
	case WSAEWOULDBLOCK:			/*10035*/						strcpy(buffer, "Resource temporarily unavailable.  This error is returned from operations on nonblocking sockets that cannot be completed immediately, for example recv when no data is queued to be read from the socket. It is a nonfatal error, and the operation should be retried later. It is normal for WSAEWOULDBLOCK to be reported as the result from calling connect on a nonblocking SOCK_STREAM socket, since some time must elapse for the connection to be established."); break;
	case WSAEINPROGRESS:			/*10036*/						strcpy(buffer, "Operation now in progress.  A blocking operation is currently executing.  Windows Sockets only allows a single blocking operation - per task or per thread - to be outstanding, and if any other function call is made (whether or not it references that or any other socket) the function fails with the WSAEINPROGRESS error."); break;
	case WSAEALREADY:					/*10037*/						strcpy(buffer, "Operation already in progress.  An operation was attempted on a nonblocking socket with an operation already in progress - that is, calling connect a second time on a nonblocking socket that is already connecting, or canceling an asynchronous request (WSAAsyncGetXbyY) that has already been canceled or completed."); break;
	case WSAENOTSOCK:					/*10038*/						strcpy(buffer, "Socket operation on nonsocket.  An operation was attempted on something that is not a socket.  Either the socket handle parameter did not reference a valid socket, or for select, a member of an fd_set was not valid."); break;
	case WSAEDESTADDRREQ:			/*10039*/						strcpy(buffer, "Destination address required.  A required address was omitted from an operation on a socket. For example, this error is returned if sendto is called with the remote address of ADDR_ANY."); break;
	case WSAEMSGSIZE:					/*10040*/						strcpy(buffer, "Message too long.  A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram was smaller than the datagram itself."); break;
	case WSAEPROTOTYPE:				/*10041*/						strcpy(buffer, "Protocol wrong type for socket.  A protocol was specified in the socket function call that does not support the semantics of the socket type requested. For example, the ARPA Internet UDP protocol cannot be specified with a socket type of SOCK_STREAM."); break;
	case WSAENOPROTOOPT:			/*10042*/						strcpy(buffer, "Bad protocol option.  An unknown, invalid or unsupported option or level was specified in a getsockopt or setsockopt call."); break;
	case WSAEPROTONOSUPPORT:	/*10043*/						strcpy(buffer, "Protocol not supported.  The requested protocol has not been configured into the system, or no implementation for it exists. For example, a socket call requests a SOCK_DGRAM socket, but specifies a stream protocol."); break;
	case WSAESOCKTNOSUPPORT:	/*10044*/						strcpy(buffer, "Socket type not supported.  The support for the specified socket type does not exist in this address family.  For example, the optional type SOCK_RAW might be selected in a socket call, and the implementation does not support SOCK_RAW sockets at all."); break;
	case WSAEOPNOTSUPP:				/*10045*/						strcpy(buffer, "Operation not supported.  The attempted operation is not supported for the type of object referenced.  Usually this occurs when a socket descriptor to a socket that cannot support this operation is trying to accept a connection on a datagram socket."); break;
	case WSAEPFNOSUPPORT:			/*10046*/						strcpy(buffer, "Protocol family not supported.  The protocol family has not been configured into the system or no implementation for it exists.  This message has a slightly different meaning from WSAEAFNOSUPPORT.  However, it is interchangeable in most cases, and all Windows Sockets functions that return one of these messages also specify WSAEAFNOSUPPORT."); break;
	case WSAEAFNOSUPPORT:			/*10047*/						strcpy(buffer, "Address family not supported by protocol family.  An address incompatible with the requested protocol was used.  All sockets are created with an associated address family (that is, AF_INET for Internet Protocols) and a generic protocol type (that is, SOCK_STREAM).  This error is returned if an incorrect protocol is explicitly requested in the socket call, or if an address of the wrong family is used for a socket, for example, in sendto."); break;
	case WSAEADDRINUSE:				/*10048*/						strcpy(buffer, "Address already in use.  Typically, only one usage of each socket address (protocol/IP address/port) is permitted.  This error occurs if an application attempts to bind a socket to an IP address/port that has already been used for an existing socket, or a socket that was not closed properly, or one that is still in the process of closing.  For server applications that need to bind multiple sockets to the same port number, consider using setsockopt (SO_REUSEADDR).  Client applications usually need not call bind at all - connect chooses an unused port automatically.  When bind is called with a wildcard address (involving ADDR_ANY), a WSAEADDRINUSE error could be delayed until the specific address is committed.  This could happen with a call to another function later, including connect, listen, WSAConnect, or WSAJoinLeaf."); break;
	case WSAEADDRNOTAVAIL:		/*10049*/						strcpy(buffer, "Cannot assign requested address.  The requested address is not valid in its context.  This normally results from an attempt to bind to an address that is not valid for the local computer.  This can also result from connect, sendto, WSAConnect, WSAJoinLeaf, or WSASendTo when the remote address or port is not valid for a remote computer (for example, address or port 0)."); break;
	case WSAENETDOWN:					/*10050*/						strcpy(buffer, "Network is down.  A socket operation encountered a dead network. This could indicate a serious failure of the network system (that is, the protocol stack that the Windows Sockets DLL runs over), the network interface, or the local network itself."); break;
	case WSAENETUNREACH:			/*10051*/						strcpy(buffer, "Network is unreachable.  A socket operation was attempted to an unreachable network.  This usually means the local software knows no route to reach the remote host."); break;
	case WSAENETRESET:				/*10052*/						strcpy(buffer, "Network dropped connection on reset.  The connection has been broken due to keep-alive activity detecting a failure while the operation was in progress.  This error can also be returned by setsockopt if an attempt is made to set SO_KEEPALIVE on a connection that has already failed."); break;
	case WSAECONNABORTED:			/*10053*/						strcpy(buffer, "Software caused connection abort.  An established connection was aborted by the software in your host computer, possibly due to a data transmission time-out or protocol error."); break;
	case WSAECONNRESET:				/*10054*/						strcpy(buffer, "Connection reset by peer.  An existing connection was forcibly closed by the remote host.  This normally results if the peer application on the remote host is suddenly stopped, the host is rebooted, the host or remote network interface is disabled, or the remote host uses a hard close (see setsockopt for more information on the SO_LINGER option on the remote socket).  This error may also result if a connection was broken due to keep-alive activity detecting a failure while one or more operations are in progress.  Operations that were in progress fail with WSAENETRESET. Subsequent operations fail with WSAECONNRESET."); break;
	case WSAENOBUFS:					/*10055*/						strcpy(buffer, "No buffer space available.  An operation on a socket could not be performed because the system lacked sufficient buffer space or because a queue was full."); break;
	case WSAEISCONN:					/*10056*/						strcpy(buffer, "Socket is already connected.  A connect request was made on an already-connected socket.  Some implementations also return this error if sendto is called on a connected SOCK_DGRAM socket (for SOCK_STREAM sockets, the to parameter in sendto is ignored) although other implementations treat this as a legal occurrence."); break;
	case WSAENOTCONN:					/*10057*/						strcpy(buffer, "Socket is not connected.  A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using sendto) no address was supplied.  Any other type of operation might also return this error - for example, setsockopt setting SO_KEEPALIVE if the connection has been reset."); break;
	case WSAESHUTDOWN:				/*10058*/						strcpy(buffer, "Cannot send after socket shutdown.  A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.  By calling shutdown a partial close of a socket is requested, which is a signal that sending or receiving, or both have been discontinued."); break;
	case WSAETIMEDOUT:				/*10060*/						strcpy(buffer, "Connection timed out.  A connection attempt failed because the connected party did not properly respond after a period of time, or the established connection failed because the connected host has failed to respond."); break;
	case WSAECONNREFUSED:			/*10061*/						strcpy(buffer, "Connection refused.  No connection could be made because the target computer actively refused it.  This usually results from trying to connect to a service that is inactive on the foreign host - that is, one with no server application running."); break;
	case WSAEHOSTDOWN:				/*10064*/						strcpy(buffer, "Host is down.  A socket operation failed because the destination host is down.  A socket operation encountered a dead host.  Networking activity on the local host has not been initiated.  These conditions are more likely to be indicated by the error WSAETIMEDOUT."); break;
	case WSAEHOSTUNREACH:			/*10065*/						strcpy(buffer, "No route to host.  A socket operation was attempted to an unreachable host.  See WSAENETUNREACH."); break;
	case WSAEPROCLIM:					/*10067*/						strcpy(buffer, "Too many processes.  A Windows Sockets implementation may have a limit on the number of applications that can use it simultaneously.  WSAStartup may fail with this error if the limit has been reached."); break;
	case WSASYSNOTREADY:			/*10091*/						strcpy(buffer, "Network subsystem is unavailable.  This error is returned by WSAStartup if the Windows Sockets implementation cannot function at this time because the underlying system it uses to provide network services is currently unavailable.  Users should check:\n  * That the appropriate Windows Sockets DLL file is in the current path.\n  * That they are not trying to use more than one Windows Sockets implementation simultaneously.  If there is more than one Winsock DLL on your system, be sure the first one in the path is appropriate for the network subsystem currently loaded.\n  * The Windows Sockets implementation documentation to be sure all necessary components are currently installed and configured correctly."); break;
	case WSAVERNOTSUPPORTED:	/*10092*/						strcpy(buffer, "Winsock.dll version out of range.  The current Windows Sockets implementation does not support the Windows Sockets specification version requested by the application.  Check that no old Windows Sockets DLL files are being accessed."); break;
	case WSANOTINITIALISED:		/*10093*/						strcpy(buffer, "Successful WSAStartup not yet performed.  Either the application has not called WSAStartup or WSAStartup failed.  The application may be accessing a socket that the current active task does not own (that is, trying to share a socket between tasks), or WSACleanup has been called too many times."); break;
	case WSAEDISCON:					/*10101*/						strcpy(buffer, "Graceful shutdown in progress.  Returned by WSARecv and WSARecvFrom to indicate that the remote party has initiated a graceful shutdown sequence."); break;
	case WSATYPE_NOT_FOUND:		/*10109*/						strcpy(buffer, "Class type not found.  The specified class was not found."); break;
	case WSAHOST_NOT_FOUND:		/*11001*/						strcpy(buffer, "Host not found.  No such host is known.  The name is not an official host name or alias, or it cannot be found in the database(s) being queried.  This error may also be returned for protocol and service queries, and means that the specified name could not be found in the relevant database."); break;
	case WSATRY_AGAIN:				/*11002*/						strcpy(buffer, "Nonauthoritative host not found.  This is usually a temporary error during host name resolution and means that the local server did not receive a response from an authoritative server.  A retry at some time later may be successful."); break;
	case WSANO_RECOVERY:			/*11003*/						strcpy(buffer, "This is a nonrecoverable error.  This indicates that some sort of nonrecoverable error occurred during a database lookup.  This may be because the database files (for example, BSD-compatible HOSTS, SERVICES, or PROTOCOLS files) could not be found, or a DNS request was returned by the server with a severe error."); break;
	case WSANO_DATA:					/*11004*/						strcpy(buffer, "Valid name, no data record of requested type.  The requested name is valid and was found in the database, but it does not have the correct associated data being resolved for.  The usual example for this is a host name-to-address translation attempt (using gethostbyname or WSAAsyncGetHostByName) which uses the DNS (Domain Name Server). An MX record is returned but no A record - indicating the host itself exists, but is not directly reachable."); break;
	case WSA_INVALID_HANDLE:		/*OS dependent*/	strcpy(buffer, "Specified event object handle is invalid.  An application attempts to use an event object, but the specified handle is not valid."); break;
	case WSA_INVALID_PARAMETER:	/*OS dependent*/	strcpy(buffer, "One or more parameters are invalid.  An application used a Windows Sockets function which directly maps to a Windows function.  The Windows function is indicating a problem with one or more parameters."); break;
	case WSA_IO_INCOMPLETE:			/*OS dependent*/	strcpy(buffer, "Overlapped I/O event object not in signaled state.  The application has tried to determine the status of an overlapped operation which is not yet completed.  Applications that use WSAGetOverlappedResult (with the fWait flag set to FALSE) in a polling mode to determine when an overlapped operation has completed, get this error code until the operation is complete."); break;
	case WSA_IO_PENDING:				/*OS dependent*/	strcpy(buffer, "Overlapped operations will complete later.  The application has initiated an overlapped operation that cannot be completed immediately.  A completion indication will be given later when the operation has been completed."); break;
	case WSA_NOT_ENOUGH_MEMORY:	/*OS dependent*/	strcpy(buffer, "Insufficient memory available.  An application used a Windows Sockets function that directly maps to a Windows function.  The Windows function is indicating a lack of required memory resources."); break;
	case WSA_OPERATION_ABORTED:	/*OS dependent*/	strcpy(buffer, "Overlapped operation aborted.  An overlapped operation was canceled due to the closure of the socket, or the execution of the SIO_FLUSH command in WSAIoctl."); break;
//	case WSAINVALIDPROCTABLE:		/*OS dependent*/	strcpy(buffer, "Invalid procedure table from service provider.  A service provider returned a bogus procedure table to Ws2_32.dll.  (This is usually caused by one or more of the function pointers being null.)"); break;
//	case WSAINVALIDPROVIDER:		/*OS dependent*/	strcpy(buffer, "Invalid service provider version number.  A service provider returned a version number other than 2.0."); break;
//	case WSAPROVIDERFAILEDINIT:	/*OS dependent*/	strcpy(buffer, "Unable to initialize a service provider.  Either a service provider's DLL could not be loaded (LoadLibrary failed) or the provider's WSPStartup/NSPStartup function failed."); break;
	case WSASYSCALLFAILURE:			/*OS dependent*/	strcpy(buffer, "System call failure.  Generic error code, returned under various conditions.\n  Returned when a system call that should never fail does fail.  For example, if a call to WaitForMultipleEvents fails or one of the registry functions fails trying to manipulate the protocol/namespace catalogs.\n  Returned when a provider does not return SUCCESS and does not provide an extended error code.  Can indicate a service provider implementation error."); break;
	default:
		{
			FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL, 
					nError,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR) &pch, 0, NULL);
			return pch;
		}  break;
	}


	pch = (char*)LocalAlloc(LPTR, (strlen(buffer)+1)); // have to use LocalAlloc to match the buffer that FormatMessage returns, in order to use LocalFree uniformly.
	if(pch)	strcpy(pch, buffer);

	return pch;
}

unsigned char CNetUtil::Checksum(unsigned char* pucBuffer, unsigned long ulLen) 
{
  unsigned long i;
	unsigned char sum=0;
  for (i=0; i<ulLen; i++) sum += (unsigned char) pucBuffer[i]; 
  return sum;
}


void ServerListenerThread(void* pvArgs)
{
	CNetServer* pServer = (CNetServer*) pvArgs;
	if(pServer==NULL) { _endthread(); return; }

	pServer->m_ulServerStatus = NET_STATUS_THREADSTARTED;
	if(pServer->m_pszStatus) free(pServer->m_pszStatus);
	pServer->m_pszStatus = (char*)malloc(NET_ERRORSTRING_LEN);
	if(pServer->m_pszStatus) strcpy(pServer->m_pszStatus, "");
	
	pServer->m_ulConnections = 0;		// reset just in case

	SOCKET socketServer;
	SOCKET socketClient;
	SOCKADDR_IN siServer;
	SOCKADDR_IN siClient;
	int nClient;
	CNetUtil net(false); // local object for utility functions.


	while(!(pServer->m_ulThreadControl&NET_CTRL_KILL))
	{
		socketServer = socket(pServer->m_nAf,pServer->m_nType,pServer->m_nProtocol);
		if (socketServer == INVALID_SOCKET)
		{
			pServer->m_ulServerStatus = NET_STATUS_ERROR; // removes NET_STATUS_THREADSTARTED

 			if(pServer->m_pszStatus)
			{
				int nErrorCode = WSAGetLastError();
				char* pchError = net.WinsockEnglish(nErrorCode);
				_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: invalid socket. %s", pchError?pchError:"");
				if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_CRITICAL|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
				if(pchError) LocalFree(pchError);
			}
			_endthread();
			return;
		}
		
		siServer.sin_family = AF_INET;
		siServer.sin_port = htons(pServer->m_usPort);
		siServer.sin_addr.s_addr = htonl(INADDR_ANY);

//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "bind...","net***debug");

		if ( bind(socketServer,(struct sockaddr *) &siServer,sizeof(SOCKADDR_IN)) == SOCKET_ERROR )
		{
			pServer->m_ulServerStatus = NET_STATUS_ERROR; // removes NET_STATUS_THREADSTARTED

 			if(pServer->m_pszStatus)
			{
 				int nErrorCode = WSAGetLastError();
				char* pchError = net.WinsockEnglish(nErrorCode);
				_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in bind. %s", pchError?pchError:"");
				if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_CRITICAL|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
				if(pchError) LocalFree(pchError);
			}
			shutdown(socketServer, SD_BOTH);
			closesocket(socketServer);
			_endthread();
			return;
		}

		nClient = sizeof(SOCKADDR_IN);
//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "listen...","net***debug");


		if(pServer->m_nType == SOCK_DGRAM)  // datagram, does not require listen and accept. can still go multithreaded but need to use the single socket, and the handler thread must not shut down the server socket
		{

			pServer->m_ulServerStatus |= NET_STATUS_SERVERSTARTED;
			pServer->m_ulServerStatus &= ~NET_STATUS_ERROR;
		
			struct timeval tv;
			tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
			fd_set fds;
			int nNumSockets;
			FD_ZERO(&fds);

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "entering loop...","net***debug");

			while(!(pServer->m_ulThreadControl&NET_CTRL_KILL))
			{
				FD_SET(socketServer, &fds);

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "select","net***debug");

				nNumSockets = select(0, &fds, NULL, NULL, &tv);

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "after select","net***debug");

				if ( nNumSockets == INVALID_SOCKET )
				{
 					if(pServer->m_pszStatus)
					{
						pServer->m_ulServerStatus = (NET_STATUS_ERROR|NET_STATUS_THREADSTARTED); // removes server started, we have to try to start it again.
 						int nErrorCode = WSAGetLastError();
						char* pchError = net.WinsockEnglish(nErrorCode);
						_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in select. %s", pchError?pchError:"");
						if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_HIGH|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
						if(pchError) LocalFree(pchError);
					}
					shutdown(socketServer, SD_BOTH);
					closesocket(socketServer);
					break; // breaks out of small while, re-inits a server listener.
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(socketServer, &fds)))
					) { continue; }  // else go ahead

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "accept","net***debug");

				CNetClient* pclient = new CNetClient;
				if(pclient)
				{
					pclient->m_socket = socketServer;   // this is a datagram socket!
					// pclient->m_si = siClient; // set inside thread, or not at all
					pclient->m_lpObject = pServer->m_lpObject;				// pointer to the object passed to the handler thread.
					pclient->m_lpMsgObj	= pServer->m_lpMsgObj;					// pointer to the Message function.
					pclient->m_pulConnections = &(pServer->m_ulConnections); // pointer to connection counter
					pclient->m_pulThreadControl = &(pServer->m_ulThreadControl); // pointer to commands to the server thread, in case we need to kill persistent connection client threads.
					pclient->m_ucType = pServer->m_ucType;

					// handle every request in its own thread
					if( _beginthread((void (__cdecl *)(void *)) pServer->m_lpfnHandler, 0, (void*)pclient)<0)
					{
 						if(pServer->m_pszStatus)
						{
							_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: error creating handler thread.");
							if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
						}
					}
					else
					{
						// pause while there is still data on the pipe.
						/*
						while((!(pServer->m_ulThreadControl&NET_CTRL_KILL))&&(nNumSockets>0))
						{
							FD_ZERO(&fds);
							nNumSockets = select(0, &fds, NULL, NULL, &tv);
						}
						*/

						pServer->m_ulConnections++; // set outside the thread for datagram

						// Or, rather, just keep number of client "connections" = 1.  And wait until the spawned thread is done.
						// It almost amounts to having everything single-threaded, but allows the execution of the exchange to be in an overridable thread.
						while((!(pServer->m_ulThreadControl&NET_CTRL_KILL))&&(pServer->m_ulConnections>0))
						{
							Sleep(1);
						}
					}
				}
				else
				{
 					if(pServer->m_pszStatus)
					{
						_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: error allocating client object.");
						if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
					}
				}
			}
		}
		else  // not a datagram socket
		{
			if ( listen(socketServer,SOMAXCONN) == SOCKET_ERROR )
			{
				pServer->m_ulServerStatus = NET_STATUS_ERROR; // removes NET_STATUS_THREADSTARTED

				if(pServer->m_pszStatus)
				{
 					int nErrorCode = WSAGetLastError();
					char* pchError = net.WinsockEnglish(nErrorCode);
					_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in listen. %s", pchError?pchError:"");
					if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_CRITICAL|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
					if(pchError) LocalFree(pchError);
				}
				shutdown(socketServer, SD_BOTH);
				closesocket(socketServer);
				_endthread();
				return;
			}

			pServer->m_ulServerStatus |= NET_STATUS_SERVERSTARTED;
			pServer->m_ulServerStatus &= ~NET_STATUS_ERROR;
		
			struct timeval tv;
			tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
			fd_set fds;
			int nNumSockets;
			FD_ZERO(&fds);

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "entering loop...","net***debug");

			while(!(pServer->m_ulThreadControl&NET_CTRL_KILL))
			{
				FD_SET(socketServer, &fds);

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "select","net***debug");

				nNumSockets = select(0, &fds, NULL, NULL, &tv);

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "after select","net***debug");

				if ( nNumSockets == INVALID_SOCKET )
				{
 					if(pServer->m_pszStatus)
					{
						pServer->m_ulServerStatus = (NET_STATUS_ERROR|NET_STATUS_THREADSTARTED); // removes server started, we have to try to start it again.
 						int nErrorCode = WSAGetLastError();
						char* pchError = net.WinsockEnglish(nErrorCode);
						_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in select. %s", pchError?pchError:"");
						if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_HIGH|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
						if(pchError) LocalFree(pchError);
					}
					shutdown(socketServer, SD_BOTH);
					closesocket(socketServer);
					break; // breaks out of small while, re-inits a server listener.
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(socketServer, &fds)))
					) { continue; }  // else go ahead

	//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "accept","net***debug");

				socketClient = accept(socketServer,(struct sockaddr *)&siClient, &nClient);
				if(!(pServer->m_ulThreadControl&NET_CTRL_KILL))  // may have changed states while waiting for accept
				{
					if ( socketClient == INVALID_SOCKET )
					{
						// we should have prevented this with a check at select, but if this has happened we whould try to re-init everything
 						if(pServer->m_pszStatus)
						{
							pServer->m_ulServerStatus = (NET_STATUS_ERROR|NET_STATUS_THREADSTARTED); // removes server started, we hae to try to start it again.
 							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in accept. %s", pchError?pchError:"");
							if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_HIGH|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
							if(pchError) LocalFree(pchError);
						}
						shutdown(socketServer, SD_BOTH);
						closesocket(socketServer);
						break; // breaks out of small while, re-inits a server listener.
					}
					else
					{
						CNetClient* pclient = new CNetClient;
						if(pclient)
						{
							pclient->m_socket = socketClient;
							pclient->m_si = siClient;
							pclient->m_lpObject = pServer->m_lpObject;				// pointer to the object passed to the handler thread.
							pclient->m_lpMsgObj	= pServer->m_lpMsgObj;					// pointer to the Message function.
							pclient->m_pulConnections = &(pServer->m_ulConnections); // pointer to connection counter
							pclient->m_pulThreadControl = &(pServer->m_ulThreadControl); // pointer to commands to the server thread, in case we need to kill persistent connection client threads.
							pclient->m_ucType = pServer->m_ucType;

							// handle every request in its own thread
							if( _beginthread((void (__cdecl *)(void *)) pServer->m_lpfnHandler, 0, (void*)pclient)<0)
							{
 								if(pServer->m_pszStatus)
								{
									_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: error creating handler thread.");
									if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
								}
								shutdown(socketClient, SD_BOTH);
								closesocket(socketClient);
							}
						}
						else
						{
 							if(pServer->m_pszStatus)
							{
								_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: error allocating client object.");
								if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
							}
							shutdown(socketClient, SD_BOTH);
							closesocket(socketClient);
						}
					}
				}
			}
		}
//((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_ICONHAND, "loop","net***debug");
		Sleep(50); // wait a short while before trying to re-init the server
	}

	if(pServer)
	{
		if(pServer->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
		{
			if(socketServer!=INVALID_SOCKET)
			{
				shutdown(socketServer, SD_BOTH);
				closesocket(socketServer);
			}
			pServer->m_ulServerStatus&=~NET_STATUS_SERVERSTARTED;
		}
		
		pServer->m_ulServerStatus&=~NET_STATUS_THREADSTARTED;
		pServer->m_ulConnections = 0;
	}
	else
	{
		if(socketServer!=INVALID_SOCKET)
		{
			shutdown(socketServer, SD_BOTH);
			closesocket(socketServer);
		}
	}
}

// the following sample thread is provided as a prototype for server handler threads.
// objects of the users choice may be passed in via the pClient->m_lpObject pointer
// this sample thread accepts protocol 1 data and simply sends a protocol 1 ack with some data.
// check HTTP10.cpp for a sample thread not using the CNetData structure (free-form buffer parsing).
void ServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
}

// the following sample thread is provided as a prototype for server handler threads for use with datagram sockets
void DatagramServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		CNetUtil net(false); // local object for utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
//		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it


		CNetDatagramData** ppData=NULL;
		int nNumDataObj=0;

		struct timeval tv;
		tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
		fd_set fds;
		int nNumSockets;
		FD_ZERO(&fds);

		FD_SET(pClient->m_socket, &fds);

		nNumSockets = select(0, &fds, NULL, NULL, &tv);

		SOCKADDR_IN si;
		int nSize = sizeof(si);


		while (
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&(nNumSockets>0)
					)
		{
			char* pchRecv = (char*)malloc(NET_COMMBUFFERSIZE);
			if(pchRecv)
			{
				int nNumBytes 
					=  recvfrom(
											pClient->m_socket,
											pchRecv,
											NET_COMMBUFFERSIZE,
											0,
											(struct sockaddr*)&si,
											&nSize
										);

				if(nNumBytes>0)
				{
					BOOL bNew = FALSE;
					// first check the host list
					if(ppData)
					{
						int i=0;
						BOOL bFound = FALSE;
						while(i<nNumDataObj)
						{
							if(
									(ppData[i]->m_si.sin_port == si.sin_port) 
								&&(ppData[i]->m_si.sin_addr.S_un.S_addr == si.sin_addr.S_un.S_addr)
								) // check both port and address.
							{
								// add here
								unsigned char* pucNew = (unsigned char*)malloc(nNumBytes + ppData[i]->m_ulRecv);
								if(pucNew)
								{
									if(ppData[i]->m_pucRecv)
									{
										memcpy(pucNew, ppData[i]->m_pucRecv, ppData[i]->m_ulRecv);			
										free(ppData[i]->m_pucRecv);
									}

									memcpy(pucNew + ppData[i]->m_ulRecv, pchRecv, nNumBytes);

									ppData[i]->m_ulRecv += nNumBytes;
									ppData[i]->m_pucRecv = pucNew;
								}
								bFound = TRUE;
								break;
							}
						
							i++;
						}
						if(!bFound) bNew = TRUE;
					}
					else
					{
						bNew = TRUE;
						// add new.
					}

					if(bNew)
					{
						CNetDatagramData* pData=new CNetDatagramData;

						if(pData)
						{
							pData->m_lpObject = NULL;
							pData->m_pucRecv = NULL;
							pData->m_ulRecv = 0;

							CNetDatagramData** ppTempData = new CNetDatagramData*[nNumDataObj+1];
							if(ppTempData)
							{
								if(ppData)
								{
									memcpy(ppTempData, ppData, nNumDataObj*sizeof(CNetDatagramData*));
									delete [] ppData;
								}

								ppTempData[nNumDataObj++] = pData;
								ppData = ppTempData;

							}
							unsigned char* pucNew = (unsigned char*)malloc(nNumBytes);
							if(pucNew)
							{
								memcpy(pucNew, pchRecv, nNumBytes);

								pData->m_ulRecv = nNumBytes;
								pData->m_pucRecv = pucNew;
								memcpy(&pData->m_si, &si, sizeof(si));
								//pData->m_si.sin_addr.S_un.S_addr = si.sin_addr.S_un.S_addr;
							}

						}
					}
				}
			}

			tv.tv_sec = 0; tv.tv_usec = 1000;  // longer timeout value for data continuation
			FD_ZERO(&fds);
			FD_SET(pClient->m_socket, &fds);
			nNumSockets = select(0, &fds, NULL, NULL, &tv);

		}

		// no more data on pipe - respond!

		int q=0;
		while(q<nNumDataObj)
		{
			if(ppData[q])
			{

// for this sample, just make this an echo server.
				if(ppData[q]->m_pucRecv)
				{
					sendto(
						pClient->m_socket,
						(char*)ppData[q]->m_pucRecv, 
						ppData[q]->m_ulRecv,
						0,
						(struct sockaddr *) &(ppData[q]->m_si),
						nSize
					);
				}



				delete ppData[q];
			}
			q++;
		}

		delete [] ppData;

	}

	(*(pClient->m_pulConnections))--;

	delete pClient; // was created with new in the thread that spawned this one.
}



/*
	// calculate timeout
	_timeb timebuffer;
	_ftime( &timebuffer );

	timebuffer.time += (nTimeoutMilliseconds/1000);
	timebuffer.millitm += (nTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

*/

