// NetDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(NETDEFINES_H_INCLUDED)
#define NETDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

#define NET_CMD_NULL 0x00
#define NET_CMD_ACK	 0x06  // same as ascii
#define NET_CMD_NAK	 0x15  // same as ascii
#define NET_PORT_INVALID 0x0000

#define NET_VALIDHOSTCHARS	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-"
#define NET_VALIDIPCHARS		"0123456789."

#define NET_ERRORSTRING_LEN	512

#define NET_SUCCESS  0
#define NET_ERROR    -1  //generic
#define NET_ERROR_HOST_INVALID  -2
#define NET_ERROR_SOCKET_CREATE -3
#define NET_ERROR_INVALID_PARAM -4
#define NET_ERROR_BUFFER -5
#define NET_ERROR_NODATA -6
#define NET_ERROR_INCOMPLETEDATA -7
#define NET_ERROR_CHECKSUM -8
#define NET_ERROR_SEND -9
#define NET_ERROR_RECV -10
#define NET_ERROR_NAK -11
#define NET_ERROR_CONN -12
#define NET_SOFTERROR_SOCKET_SETSOCKOPTS 1
#define NET_SOFTERROR_ALREADYSTARTED 2
#define NET_SOFTERROR_ALREADYSTOPPED 3

// send options
#define EOLN_NULL		0x00  // NULL char
#define EOLN_DNUL		0x01  // double NULL
#define EOLN_CR			0x02  // carriage return (\r)
#define EOLN_LF			0x03  // line feed (\n)
#define EOLN_CRLF		0x04  // CRLF pair (\r\n)
#define EOLN_HTTP		0x05  // HTTP terminus = double CRLF (\r\n\r\n)
#define EOLN_NONE		0x06  // undefined, no character.
#define EOLN_TOKEN	0x07  // look for a user defined token.
#define EOLN_MASK		0x07	// bit mask

#define NET_SND_DEFAULT					0x00000000  // do expect a reply, and an ack to that reply, use passed in data.
#define NET_SND_NO_RX						0x00000001  // do not expect a reply.
#define NET_SND_NO_RXACK				0x00000002  // do not send an ack or nak to the reply.
#define NET_SND_ACK							0x00000010  // send ack, if on an open conn, ignore passed in data
#define NET_SND_NAK							0x00000020  // send nak, if on an open conn, ignore passed in data
#define NET_SND_KEEPOPENLCL			0x00000040  // dont close the connection locally (return the socket)
#define NET_SND_KEEPOPENRMT			0x00000080  // dont close the connection on the remote side
#define NET_SND_CMDTOSVR				NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK  // a command to the server (keep conn open to check reply and ack manually)
#define NET_SND_SVRREPLY				NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK  // a reply back from the server to the client, want an ack (which we dont ack back to), unless its neg...(next line)
#define NET_SND_SVRNAK					NET_SND_NAK|NET_SND_NO_RX	// a negative reply back from the server (close the conn unless keep open is specifcally added)
#define NET_SND_CLNTACK					NET_SND_ACK|NET_SND_NO_RX  // ack or nak back to server. (close the conn unless specifically kept open - use only with SendData for open connections.

// receive options
#define NET_COMMBUFFERSIZE 0x00100000	 //1048576
#define NET_RCV_ONCE			 0x00000010  // receives as many bytes as recv has for us without timing out or closure
#define NET_RCV_ALL				 0x00000020  // receives until connection closed or timed out
#define NET_RCV_LENGTH		 0x00000040  // use an expected value - receive just this number of bytes
#define NET_RCV_IN_BUF		 0x00000080  // use a passed in buffer, for safety should also use NET_RCV_LENGTH
#define NET_RCV_EOLN			 0x00000100  // receive until the EOLN check is true.

#define NET_TYPE_PROTOCOLMASK		0x1f
#define NET_TYPE_PROTOCOL1			0x00  // default protocol
#define NET_TYPE_PROTOCOL2			0x01  // default protocol plus security attribs.
#define NET_TYPE_PROTOCOL_HTTP	0x02  // webserver protocol
#define NET_TYPE_KEEPOPEN				0x20  // otherwise, one pair of RX/TX per connection - closes the connection at end of comm
#define NET_TYPE_HASDATA				0x40  // command has data
#define NET_TYPE_HASSUBC				0x80  // command has subcommand

// for use with TranslateSecurity.
#define NET_2TO1								0x00  // translate from NET_TYPE_PROTOCOL2 to NET_TYPE_PROTOCOL1
#define NET_1TO2								0x01  // translate from NET_TYPE_PROTOCOL1 to NET_TYPE_PROTOCOL2

// thread control
#define NET_CTRL_START					0x00000001
#define NET_CTRL_KILL						0x00000002

#define NET_STATUS_NONE								0x00000000
#define NET_STATUS_THREADSTARTED			0x00000001
#define NET_STATUS_SERVERSTARTED			0x00000002
#define NET_STATUS_ERROR							0x00000004

#define NET_FLAGS_LOGTIMING_SEND			0x00000001 //Log timing on send
#define NET_FLAGS_LOGTIMING_RECV			0x00000002 //Log timing on recv


#endif // !defined(NETDEFINES_H_INCLUDED)
