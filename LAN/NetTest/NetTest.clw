; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CNetTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "NetTest.h"

ClassCount=3
Class1=CNetTestApp
Class2=CNetTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_NETTEST_DIALOG

[CLS:CNetTestApp]
Type=0
HeaderFile=NetTest.h
ImplementationFile=NetTest.cpp
Filter=N

[CLS:CNetTestDlg]
Type=0
HeaderFile=NetTestDlg.h
ImplementationFile=NetTestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_BUTTON_C_SEND

[CLS:CAboutDlg]
Type=0
HeaderFile=NetTestDlg.h
ImplementationFile=NetTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_NETTEST_DIALOG]
Type=1
Class=CNetTestDlg
ControlCount=14
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_C_PORT,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_COMBO_HOST,combobox,1344340226
Control7=IDC_BUTTON_C_SEND,button,1342242816
Control8=IDC_BUTTON_TIMER,button,1342242816
Control9=IDC_EDIT_TIMERMS,edit,1350639744
Control10=IDC_EDIT_C_DATA,edit,1350635716
Control11=IDC_EDIT_C_REPLY,edit,1350633668
Control12=IDC_CHECK_PERSIST,button,1342242819
Control13=IDC_STATIC,static,1342308352
Control14=IDC_CHECK1,button,1342242819

