// NetTestDlg.h : header file
//

#if !defined(AFX_NETTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
#define AFX_NETTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\..\Common\LAN\NetUtil.h"


#define ID_CMD 0
#define ID_ACK 1
#define ID_BTN 2

#define NETTEST_NUM_MOVING_CONTROLS 3


#define NETTEST_MINSIZEX 400
#define NETTEST_MINSIZEY 350

/////////////////////////////////////////////////////////////////////////////
// CNetTestDlg dialog

class CNetTestDlg : public CDialog
{
// Construction
public:
	CNetTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CNetTestDlg)
	enum { IDD = IDD_NETTEST_DIALOG };
	CString	m_szData;
	int		m_nSendPort;
	BOOL	m_bPersist;
	int		m_nTimerMS;
	CString	m_szHost;
	BOOL	m_bXML;
	//}}AFX_DATA

	CNetUtil m_net;

	bool m_bTimerSet;
	char m_chConns[2056];

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[NETTEST_NUM_MOVING_CONTROLS];
	CSize m_sizeDlgMin;
	CString SimpleXMLIndent(CString szXML);


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CNetTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnButtonCSend();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonTimer();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETTESTDLG_H__92AC1038_28BC_49EA_8690_BB25D7B73C22__INCLUDED_)
