// NetTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NetTest.h"
#include "NetTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_MESSAGE_LENGTH 4096

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetTestDlg dialog

CNetTestDlg::CNetTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNetTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNetTestDlg)
	m_szData = _T("<test msgid=\"12345\"/>");
	m_nSendPort = 8081;
	m_bPersist = TRUE;
	m_nTimerMS = 100;
	m_szHost = _T("192.168.0.1");
	m_bXML = FALSE;
	//}}AFX_DATA_INIT

//	m_nPortSelected = 0;
	m_bTimerSet = false;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
}

void CNetTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetTestDlg)
	DDX_Text(pDX, IDC_EDIT_C_DATA, m_szData);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nSendPort);
	DDX_Check(pDX, IDC_CHECK_PERSIST, m_bPersist);
	DDX_Text(pDX, IDC_EDIT_TIMERMS, m_nTimerMS);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Check(pDX, IDC_CHECK1, m_bXML);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CNetTestDlg, CDialog)
	//{{AFX_MSG_MAP(CNetTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_C_SEND, OnButtonCSend)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_TIMER, OnButtonTimer)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetTestDlg message handlers

BOOL CNetTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	FILE* fp = fopen("nethost.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					nSel = i;
					pch++;
				}
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch);

				pch = strtok(NULL, "\r\n");
				i++;
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CNetTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNetTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNetTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CNetTestDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	FILE* fp = fopen("nethost.cfg", "wb");

	if(fp)
	{
		UpdateData(TRUE);
		CString szText;
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s\n", m_szHost);
		}
		if(nCount>0)
		{
			int i=0;
			while(i<nCount)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				if(nSel==i)
				{
					fprintf(fp, "*");
				}
				fprintf(fp, "%s\n", szText);
				i++;
			}
		}
		fclose(fp);
	}
	
}


void CNetTestDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
}


void CNetTestDlg::OnButtonCSend() 
{
	CWaitCursor cw;
	UpdateData(TRUE);

	if(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
	}


	CNetData data;

	data.m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
//	data.m_ucCmd = m_nCmd&0xff;       // the command byte
//	data.m_ucSubCmd = m_nSubCmd&0xff;       // the subcommand byte

//	if(m_nSubCmd>0) data.m_ucType |= NET_TYPE_HASSUBC; 

	
	data.m_pucData = NULL;
	data.m_ulDataLen = m_szData.GetLength();
	if(data.m_ulDataLen>0)
	{
		char* pch = (char*)malloc(data.m_ulDataLen+1);     // pointer to the payload data
		if(pch)
		{
			strcpy(pch, m_szData);
			data.m_pucData = (unsigned char*)pch;
		}
	}
	char buffer[MAX_MESSAGE_LENGTH];


	char host[256];

//	if(m_bSimple)
	{
		_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "%d: datalen:%d  data:%s", 
			clock(),
				data.m_ulDataLen,
				data.m_pucData==NULL?"":(char*)data.m_pucData				
			);
	}
/*
	else
	{
		_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "%d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
			clock(),
				data.m_ucType,
				data.m_ucCmd,
				data.m_ucSubCmd,
				data.m_ulDataLen,
				data.m_pucData==NULL?"":(char*)data.m_pucData				
			);
	}
*/
	GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

	if(m_bPersist)	
	{
		sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);

		SOCKET s;
		int nreturn = NET_SUCCESS;
		char* pch = strstr(m_chConns, host);
		if(pch==NULL)  // first time.
		{
			strcpy(host, m_szHost);
			nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer);
			if(nreturn<NET_SUCCESS)
			{
				char snarf[2000];
				sprintf(snarf, "Persist open: %s", buffer);
				AfxMessageBox(snarf);
//				AfxMessageBox(m_chConns);
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.

			}
			else
			{
				sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);
				strcat(m_chConns, host);
				sprintf(host, "%ld]", s);
				strcat(m_chConns, host);

//				AfxMessageBox(m_chConns);
			}
		}
		else
		{
			s = atol(pch+strlen(host));
			sprintf(host, "socket: %ld", s);
//				AfxMessageBox(host);
		}




		if(nreturn>=NET_SUCCESS)
		{
			unsigned long ulTime = clock();

			if(1)
			{
				if(m_net.SendLine(data.m_pucData, data.m_ulDataLen, s, EOLN_NONE, true, 5000, buffer)<NET_SUCCESS)
				{
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
				}
				else
				{
					unsigned long ulTime2 = clock();
					// receive something
					m_net.GetLine(&data.m_pucData, &data.m_ulDataLen, s, NET_RCV_ONCE, buffer);
					char* pch = NULL;
					if((data.m_ulDataLen)&&(data.m_pucData)) pch = (char*)malloc(data.m_ulDataLen+1);
					if(pch)
					{
						memcpy(pch, data.m_pucData, data.m_ulDataLen);
						*(pch+data.m_ulDataLen)=0;
					}

					if(m_bXML)
					{
						_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "received %d: datalen:%d\r\ndata:\r\n%s\r\n\r\ntransaction time %d ms, ack time %d ms, total %d ms",
							clock(),
								data.m_ulDataLen,
								pch==NULL?"":SimpleXMLIndent(pch),
								ulTime2 - ulTime,
								clock() - ulTime2,
								clock() - ulTime
							);
					}
					else
					{
						_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "received %d: datalen:%d\r\ndata:\r\n%s\r\n\r\ntransaction time %d ms, ack time %d ms, total %d ms",
							clock(),
								data.m_ulDataLen,
								pch==NULL?"":pch,
								ulTime2 - ulTime,
								clock() - ulTime2,
								clock() - ulTime
							);
					}
					if(pch) free(pch);
					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

				}
			}
/*
			else
			{
				if(m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, buffer)<NET_SUCCESS)
				{
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
					if(data.m_ucCmd != NET_CMD_NAK)  // check the comm error if NAK didnt come thru ok
					{
						char snarf[2000];
						sprintf(snarf, "Persist send: %s", buffer);
						AfxMessageBox(snarf);
						// no need to nak back
						//m_net.SendData(&data, s, 5000, 0, NET_SND_NAK|NET_SND_NO_RX, buffer);
					}
				}
				else
				{
					unsigned long ulTime2 = clock();
					// ack back
					m_net.SendData(&data, s, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT, buffer);

					_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "%d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms\r\n0x%08x", 
						clock(),
							data.m_ucType,
							data.m_ucCmd,
							data.m_ucSubCmd,
							data.m_ulDataLen,
							data.m_pucData==NULL?"":(char*)data.m_pucData		,
							ulTime2 - ulTime,
							clock() - ulTime2,
							clock() - ulTime,
							NET_RCV_EOLN|EOLN_HTTP
						);
					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

				}
			}
			*/
		}
	}
	else
	{

		// lets make sure theres no persistent connection now. if there is use it first to get rid of the conn,
		sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);

		SOCKET s;
		int nreturn = NET_SUCCESS;
		bool bDisconn = false;
		char* pch = strstr(m_chConns, host);
		if(pch!=NULL)  // it exists.
		{
			s = atol(pch+strlen(host));
//			sprintf(host, "socket: %ld", s);
//				AfxMessageBox(host);
			bDisconn = true;

			//now have to remove it.

			int nLen = strlen(m_chConns);
			char* pch2 = strstr(pch, "]");
			memcpy(pch, pch2+1, (m_chConns+nLen) - pch2);

		}


		strcpy(host, m_szHost);
		if(1)
		{
			_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "sending: %d: datalen:%d\r\ndata:\r\n%s", 
				clock(),
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);

		}
/*
		else
		{
			_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "sending: %d: checksum:%d, type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
				clock(),
				data.m_ulDataLen?(m_net.Checksum(data.m_pucData, data.m_ulDataLen)+data.m_ucType+data.m_ucCmd+data.m_ucSubCmd):(data.m_ucType+data.m_ucCmd+data.m_ucSubCmd),
					data.m_ucType,
					data.m_ucCmd,
					data.m_ucSubCmd,
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);
		}
*/
		GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

//			AfxMessageBox("sending");

			unsigned long ulTime = clock();
		if(1)
		{
			if(bDisconn)
				m_net.CloseConnection(s);

			nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer);
			if(nreturn<NET_SUCCESS)
			{
				char snarf[2000];
				sprintf(snarf, "Non-persist open: %s", buffer);
				AfxMessageBox(snarf);
//				AfxMessageBox(m_chConns);
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.

			}
			else
			{
				nreturn = m_net.SendLine(data.m_pucData, data.m_ulDataLen, s, EOLN_NONE, true, 5000, buffer);
				if(nreturn<NET_SUCCESS)
				{
					AfxMessageBox(buffer);
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
					
				}
				else
				{
					unsigned long ulTime2 = clock();
					// receive something
					m_net.GetLine(&data.m_pucData, &data.m_ulDataLen, s, NET_RCV_ONCE, buffer);
					char* pch = NULL;
					if((data.m_ulDataLen)&&(data.m_pucData)) pch = (char*)malloc(data.m_ulDataLen+1);
					if(pch)
					{
						memcpy(pch, data.m_pucData, data.m_ulDataLen);
						*(pch+data.m_ulDataLen)=0;
					}

					if(m_bXML)
					{

						_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "received %d: datalen:%d\r\ndata:\r\n%s\r\n\r\ntransaction time %d ms, ack time %d ms, total %d ms",
							clock(),
								data.m_ulDataLen,
								pch==NULL?"":SimpleXMLIndent(pch),
								ulTime2 - ulTime,
								clock() - ulTime2,
								clock() - ulTime
							);
					}
					else
					{
						_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "received %d: datalen:%d\r\ndata:\r\n%s\r\n\r\ntransaction time %d ms, ack time %d ms, total %d ms",
							clock(),
								data.m_ulDataLen,
								pch==NULL?"":pch,
								ulTime2 - ulTime,
								clock() - ulTime2,
								clock() - ulTime
							);
					}
					if(pch) free(pch);

					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);
				}
			}
		}
/*
		else
		{
			if(bDisconn)
				nreturn = m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR, buffer);
			else
				nreturn = m_net.SendData(&data, host, m_nSendPort, 5000, 0, NET_SND_CMDTOSVR, &s, buffer);
		
			if(nreturn<NET_SUCCESS)
			{
	//			AfxMessageBox("failure");
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
				
				if(data.m_ucCmd != NET_CMD_NAK) // means we received a neg reply, so comm ok, but it doesnt expect a reply.
				{
					char snarf[2000];
					sprintf(snarf, "Transient send: %s", buffer);
					AfxMessageBox(snarf);
				}
			}
			else
			{
	//			AfxMessageBox("success");

					unsigned long ulTime2 = clock();
				// send the ack
				m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK, buffer);

					_snprintf(buffer, MAX_MESSAGE_LENGTH-1, "success %d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms", 
						clock(),
							data.m_ucType,
							data.m_ucCmd,
							data.m_ucSubCmd,
							data.m_ulDataLen,
							data.m_pucData==NULL?"":(char*)data.m_pucData		,
							ulTime2 - ulTime,
							clock() - ulTime2,
							clock() - ulTime
						);
				GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);
			}
		}
*/
		m_net.CloseConnection(s);
	}
}

void CNetTestDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	if(nIDEvent == 6)
	{ 
		OnButtonCSend();
	}
	else
	CDialog::OnTimer(nIDEvent);
}

void CNetTestDlg::OnButtonTimer() 
{
	if(m_bTimerSet)
	{
		KillTimer(6);
		m_bTimerSet = false;

		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Set Timer");
	}
	else
	{
		UpdateData(TRUE);

		SetTimer(6, m_nTimerMS, NULL);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Kill Timer");
		m_bTimerSet = true;
	}

}



void CNetTestDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID=0;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<NETTEST_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_CMD: nCtrlID=IDC_EDIT_C_DATA;break;
			case ID_ACK: nCtrlID=IDC_EDIT_C_REPLY;break;
			case ID_BTN: nCtrlID=IDC_BUTTON_C_SEND;break;
			}
			if(nCtrlID>0)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
				ScreenToClient(&m_rcCtrl[i]);
			}
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void CNetTestDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	int nCtrlID=0;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_EDIT_C_DATA);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_CMD].left,  // goes with right edge
			m_rcCtrl[ID_CMD].top,
			m_rcCtrl[ID_CMD].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_CMD].Height(), 
			SWP_NOZORDER|SWP_NOMOVE
			);
		pWnd=GetDlgItem(IDC_EDIT_C_REPLY);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_ACK].left,  // goes with right edge
			m_rcCtrl[ID_ACK].top,
			m_rcCtrl[ID_ACK].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_ACK].Height()-dy, 
			SWP_NOZORDER|SWP_NOMOVE
			);
		pWnd=GetDlgItem(IDC_BUTTON_C_SEND);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BTN].left-dx,  // goes with right edge
			m_rcCtrl[ID_BTN].top,
			0,
			0,
			SWP_NOZORDER|SWP_NOSIZE
			);

		for (int i=0;i<NETTEST_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_CMD: nCtrlID=IDC_EDIT_C_DATA;break;
			case ID_ACK: nCtrlID=IDC_EDIT_C_REPLY;break;
			case ID_BTN: nCtrlID=IDC_BUTTON_C_SEND;break;
			}
			if(nCtrlID>0)
			{
				GetDlgItem(nCtrlID)->Invalidate();
			}
		}
	}		
}


CString CNetTestDlg::SimpleXMLIndent(CString szXML)
{
	CString szReturn="";
//	CString szIndent=" ";

	int n=0; 
//	int nIndent=0; 
	int nLen = szXML.GetLength();
	bool bTag = false;
	bool bEndTag = false;
	bool bFirstTag = false;
	bool bFirstNotTag = false;

	while(n<nLen)
	{
		char ch = szXML.GetAt(n);

		if(bFirstNotTag)
		{
			if(ch == '<')
			{
				szReturn+= (char)13;
				szReturn+= (char)10;

			}
		}
		bFirstNotTag = false;
		szReturn+=ch;

		if(bTag)
		{
			if(ch == '?')
			{
				bTag = false;
			}
			else
			if((ch == '/')&&(bFirstTag))
			{
				bEndTag = true;
			}
			else
			if(ch == '>')
			{
				bTag = false;
				if(bEndTag)
				{
					szReturn+= (char)13;
					szReturn+= (char)10;
				}
				else
				{
					bFirstNotTag = true;
				}
				bEndTag = false;
				
			}
			bFirstTag = false;
		}
		else
		{
			if(ch == '<')
			{

				bTag = true;
				bFirstTag = true;
			}
		}

		n++;
	}
	return szReturn;
}

