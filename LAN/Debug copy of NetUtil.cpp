// NetUtil.cpp: implementation of the CNetUtil and support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>  //remove this
#include <process.h>
#include "NetUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// CNetData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNetData::CNetData()
{
	m_ucType		= NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
	m_ucCmd			= NET_CMD_NULL;       // the command byte
	m_ucSubCmd	= NET_CMD_NULL;    // the subcommand byte - if used with commmand byte gives up to 65535 possible commands - good enough for all practicality.  more than this can be accomplished with the attached data payload
	m_pucData		= NULL;     // pointer to the payload data
	m_ulDataLen = 0;  // length of the payload data buffer.
}

CNetData::~CNetData()
{
	if(m_pucData) free(m_pucData);  //must use malloc.
}



//////////////////////////////////////////////////////////////////////
// CNetServer Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNetServer::CNetServer()
{
	m_usPort = NET_PORT_INVALID;					// port on which the server listens
	m_ucType = NET_TYPE_PROTOCOL1;				// defined type - indicates which protocol to use, structure of data
	m_pszName = NULL;											// name of the server, for human readability
	m_ulConnections = 0;									// number of simultaneous connections
	m_ulThreadControl = NET_CTRL_START;		// commands to the server thread
	m_ulServerStatus = NET_STATUS_NONE;		// status back from thread
	m_pszStatus = NULL;										// status buffer with error messages from thread
	m_lpfnHandler = ServerHandlerThread;	// pointer to the thread that handles the request.
	m_lpObject = NULL;										// pointer to the object passed to the handler thread.
	m_lpMsgObj = NULL;											// pointer to the Message function.
}

CNetServer::~CNetServer()
{
	m_ulThreadControl=NET_CTRL_KILL;  // kills the thread and server

	if(m_pszName) free(m_pszName);  //must use malloc.
	if(m_pszStatus) free(m_pszStatus);  //must use malloc.

	// this bit of code might be dangerous if it hangs forever.  should put in a timeout someday
	while((m_ulServerStatus&NET_STATUS_THREADSTARTED)&&(m_ulConnections>0)) Sleep(1);  // let the server thread end if necessary
}



//////////////////////////////////////////////////////////////////////
// CNetUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNetUtil::CNetUtil(bool bInitialize)
{
	m_bNetStarted = false;
	m_ppNetServers = NULL;
	m_usNumServers = 0;

	if(bInitialize)
		Initialize();
}

CNetUtil::~CNetUtil()
{
	if((m_ppNetServers)&&(m_usNumServers>0))
	{
		for(unsigned short i=0; i<m_usNumServers; i++)
		{
			if(m_ppNetServers[i]) delete m_ppNetServers[i];
		}
		delete [] m_ppNetServers;
	}

  if(m_bNetStarted)  // for this instance
		WSACleanup();  // clean up
}

CNetUtil::Initialize(char* pszInfo)
{
  int nReturnCode = NET_SUCCESS;
	// start winsock for every instance
	if(!m_bNetStarted)
	{
		WSADATA wd;
		int nError = WSAStartup(MAKEWORD(2,2), &wd);
		if ( nError != 0 )
		{
			// failed 
			if(pszInfo) strcpy(pszInfo, "Initialize: Error starting Winsock.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "Initialize: Error starting Winsock.", "CNetUtil:Initialize");
			nReturnCode = NET_ERROR;
		}
		else
		{
			m_bNetStarted = true;
			if(pszInfo) strcpy(pszInfo, "Initialize: Winsock started.");
		}
	}
	else if(pszInfo) strcpy(pszInfo, "Initialize: Winsock already started.");

	return nReturnCode;
}

int CNetUtil::OpenConnection(char* pchHost, short nPort, SOCKET* ps, unsigned long ulTimeoutMilliseconds, char* pszInfo) 
{
  int nLen, nReturnCode = NET_SUCCESS;
	register struct hostent *hp = NULL;
	char hostnamebuf[80], hostname[100];
  SOCKET socketReturn;
	struct sockaddr_in saddr;
	char* pchHostInternal = pchHost;
	int nLine;

	nLen = strlen(pchHostInternal);
	if(nLen<=0) 
	{
		if(pszInfo) strcpy(pszInfo, "OpenConnection: Invalid host.");
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "OpenConnection: Invalid host.", "CNetUtil:OpenConnection");
		return NET_ERROR_HOST_INVALID;
	}

	//trim host name
	while((pchHostInternal<pchHostInternal+nLen)&&(isspace(pchHostInternal[0]))) {pchHostInternal++; nLen--;}  //trims left
	while((nLen>0)&&(isspace(pchHostInternal[nLen-1]))) {pchHostInternal[nLen-1]=0; nLen--;}  //trims right

	if(nLen<=0) 
	{
		if(pszInfo) strcpy(pszInfo, "OpenConnection: Invalid host.");
		Message(MSG_PRI_HIGH|MSG_ICONERROR, "OpenConnection: Invalid host.", "CNetUtil:OpenConnection");
		return NET_ERROR_HOST_INVALID;
	}

	memset(((char *)&saddr), 0, (sizeof (saddr)));
	saddr.sin_addr.s_addr = inet_addr(pchHostInternal);
	if (saddr.sin_addr.s_addr != -1) 
	{
		saddr.sin_family = AF_INET;
		(void) strncpy(hostnamebuf, (LPCTSTR)pchHostInternal, sizeof(hostnamebuf));
	} 
	else 
	{
		hp = gethostbyname((LPCTSTR)pchHostInternal);

		if (hp == NULL) 
		{
			nReturnCode = WSAGetLastError();
			char* pchError = WinsockEnglish(nReturnCode);
			if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection: Invalid host. %s", pchError);
			if(m_lpfnDM)
			{
				char errorstring[NET_ERRORSTRING_LEN];
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection: Invalid host. %s", pchError);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
			}
			free(pchError);
			*ps = NULL;
			return NET_ERROR_HOST_INVALID;
		}
		saddr.sin_family = hp->h_addrtype;
		memcpy((char*)&saddr.sin_addr, hp->h_addr_list[0], hp->h_length);
		strncpy(hostnamebuf, hp->h_name, sizeof(hostnamebuf));
	}

	strcpy(hostname, hostnamebuf);
	
	socketReturn = (SOCKET) socket(saddr.sin_family, SOCK_STREAM, 0); nLine = __LINE__;
	if (socketReturn == INVALID_SOCKET) 
	{
		nReturnCode = WSAGetLastError();
		char* pchError = WinsockEnglish(nReturnCode);
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error. %s", nLine, pchError);
		if(m_lpfnDM)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error. %s", nLine, pchError);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
		}
		free(pchError);
		*ps = NULL;
		return NET_ERROR_SOCKET_CREATE;
	}

	saddr.sin_port = htons((u_short) nPort);

	while (connect((SOCKET) socketReturn, (struct sockaddr FAR *) &saddr, (int) sizeof (saddr)) != 0)
	{
		// try alias
		if (hp && hp->h_addr_list[1]) 
		{
			hp->h_addr_list++;
			memcpy((char*)&saddr.sin_addr, hp->h_addr_list[0], hp->h_length);
			shutdown (socketReturn, SD_BOTH);
			closesocket(socketReturn);
			socketReturn = (SOCKET) socket(saddr.sin_family, SOCK_STREAM, 0); nLine = __LINE__;
			if (socketReturn == INVALID_SOCKET)
			{
				shutdown (socketReturn, SD_BOTH);
				closesocket(socketReturn);
				nReturnCode = WSAGetLastError();
				char* pchError = WinsockEnglish(nReturnCode);
				if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error. %s", nLine, pchError);
				if(m_lpfnDM)
				{
					char errorstring[NET_ERRORSTRING_LEN];
					_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error. %s", nLine, pchError);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
				}
				free(pchError);
				*ps = NULL;
				return NET_ERROR_SOCKET_CREATE;
			}
			continue;
		}
		
		shutdown (socketReturn, SD_BOTH);
		closesocket(socketReturn);
		nReturnCode = WSAGetLastError();
		char* pchError = WinsockEnglish(nReturnCode);
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error. %s", nLine, pchError);
		if(m_lpfnDM)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "OpenConnection(%d): Socket create error. %s", nLine, pchError);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:OpenConnection");
		}
		free(pchError);
		*ps = NULL;
		return NET_ERROR_SOCKET_CREATE;
	}

/*  // no need to get the name
	nLen = sizeof (sockaddr_in);
	if (getsockname(socketReturn, (struct sockaddr *)&saddr, &nLen) != 0) 
	{ 
		shutdown (socketReturn, SD_BOTH);
		closesocket(socketReturn);
		*ps = NULL;
		return ERROR_SOCKET_GETSOCKNAME;
	}
*/
	*ps = socketReturn;

/*
  // timeouts are not supported 
	BSD options not supported for setsockopt are:
	SO_ACCEPTCONN	Socket is listening 
	SO_RCVLOWAT	Receive low water mark 
	SO_RCVTIMEO		Receive time-out 
	SO_SNDLOWAT		Send low water mark 
	SO_SNDTIMEO		Send time-out 
	SO_TYPE				Type of the socket 
*/

  nLen = setsockopt(socketReturn, SOL_SOCKET, SO_SNDTIMEO, (char*)&ulTimeoutMilliseconds, sizeof(ulTimeoutMilliseconds));
  if (nLen != NO_ERROR) 
	{
		nReturnCode = NET_SOFTERROR_SOCKET_SETSOCKOPTS;
	}	
  nLen = setsockopt(socketReturn, SOL_SOCKET, SO_RCVTIMEO, (char*)&ulTimeoutMilliseconds, sizeof(ulTimeoutMilliseconds));
  if (nLen != NO_ERROR) 
	{
		nReturnCode = NET_SOFTERROR_SOCKET_SETSOCKOPTS;
	}	
	return nReturnCode;
}



//  Close an open connection 
int CNetUtil::CloseConnection(SOCKET s, char* pszInfo) 
{
	if((s != INVALID_SOCKET )&&(s != SOCKET_ERROR))
	{
		shutdown (s, SD_BOTH);
		closesocket(s);
		if(pszInfo) strcpy(pszInfo, "CloseConnection: Socket closed.");
	}
	else
	{
		if(pszInfo) strcpy(pszInfo, "CloseConnection: Invalid socket.");
		Message(MSG_ICONERROR, "CloseConnection: Invalid socket.", "CNetUtil:CloseConnection");
	}
  return NET_SUCCESS;
}

int CNetUtil::SendLine(unsigned char* pucBuffer, unsigned long ulBufferLen, SOCKET socket, char nEolnType, bool bDestroyBuffer, unsigned long ulTimeoutMilliseconds, char* pszInfo)
{
	if((pucBuffer == NULL)||(socket==NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode=NET_SUCCESS;

	unsigned char* pucData = pucBuffer;
	unsigned long	ulSize=0, ulLen=0;
	unsigned long ulTotalSize=0;
	int nLine;

	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}


	do
	{
		ulSize = send(socket, (char*)(pucData+ulTotalSize), ulBufferLen-ulTotalSize,0); nLine = __LINE__;
		_ftime( &now );

		if(ulSize != SOCKET_ERROR)
		{
			ulTotalSize+=ulSize;
			// reset the timeout for the next send if necessary
			if(ulTotalSize < ulBufferLen)
			{
				_ftime( &timebuffer );

				timebuffer.time += (ulTimeoutMilliseconds/1000);
				timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
				if(timebuffer.millitm>999)
				{
					timebuffer.millitm-=1000;
					timebuffer.time++;
				}
			}
		}
		else
		{
 			nReturnCode = WSAGetLastError();
			char* pchError = WinsockEnglish(nReturnCode);
			if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "SendLine(%d): send error. %s", nLine, pchError);
			if(m_lpfnDM)
			{
				char errorstring[NET_ERRORSTRING_LEN];
				_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "SendLine(%d): send error. %s", nLine, pchError);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:SendLine");
			}
			nReturnCode = NET_ERROR_SEND;
			free(pchError);
			return nReturnCode; 
		}
	} while ( (ulTotalSize < ulBufferLen)
					&&( (now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))) );

	char buffer[2] = {13,10};

  switch(nEolnType)
  {
    case EOLN_CR:   send(socket, buffer, 1, 0); break;
    case EOLN_LF:   send(socket, buffer+1, 1, 0); break;
    case EOLN_CRLF: send(socket, buffer, 2, 0); break;
    case EOLN_NONE: 
    default: break;
  }

	if(bDestroyBuffer) free(pucBuffer);

	return nReturnCode;
}

int CNetUtil::GetLine(unsigned char** ppucBuffer, unsigned long* pulBufferLen, SOCKET socket, unsigned long ulFlags, char* pszInfo)
{
	if((pulBufferLen == NULL)||(ppucBuffer == NULL)||(socket==NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode=NET_SUCCESS;

	unsigned char* pch = NULL;
	unsigned char* pchData=NULL;
	unsigned long	ulBufLen = NET_COMMBUFFERSIZE; //initial buffer size
	unsigned long ulMulti=1;
	unsigned long	ulSize=0, ulLen=0;
	unsigned long ulTotalSize=0;
	int nLine;
	bool bDone = false;


	if(ulFlags&NET_RCV_LENGTH) ulBufLen = *pulBufferLen;

	do
	{
		if(!(ulFlags&NET_RCV_LENGTH)) ulBufLen = ulMulti*NET_COMMBUFFERSIZE;
		if(ulFlags&NET_RCV_IN_BUF)
			pchData = *ppucBuffer;
		else
			pchData = (unsigned char*)malloc(ulBufLen);
		if(pchData==NULL)
		{
			if(pszInfo) strcpy(pszInfo, "GetLine: could not allocate receive buffer.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "GetLine: could not allocate receive buffer.", "CNetUtil:GetLine");
			return NET_ERROR_BUFFER;
		}
		if(pch!=NULL)
		{
			memcpy(pchData, pch, ulTotalSize);
			free(pch);
		}
		pch = pchData;
		do
		{
			ulSize = recv(socket, (char*)(pch+ulTotalSize), ulBufLen-ulTotalSize, 0);  nLine = __LINE__;

			if((ulSize != 0) && (ulSize != SOCKET_ERROR))
			{
				ulTotalSize+=ulSize;
				if((ulFlags&NET_RCV_ONCE)&&(ulTotalSize<ulBufLen)) bDone = true;  // we might need more calls to recv to get the rest if the data size is large
			}
		} while ( (!bDone) && (ulSize != 0) && (ulSize != SOCKET_ERROR) && (ulTotalSize < ulBufLen) );

		ulMulti++;

	} while ( (ulSize != 0) && (ulSize != SOCKET_ERROR) && (!(ulFlags&(NET_RCV_LENGTH|NET_RCV_IN_BUF))) );  // dont repeat if expected value, or if a buffer is passed in

	if(ulSize == SOCKET_ERROR)
	{
 		nReturnCode = WSAGetLastError();
		char* pchError = WinsockEnglish(nReturnCode);
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "GetLine(%d): recv error. %s", nLine, pchError);
		if(m_lpfnDM)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "GetLine(%d): recv error. %s", nLine, pchError);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:GetLine");
		}
		nReturnCode = NET_ERROR_INCOMPLETEDATA;
		free(pchError);
		if((pch)&&(!(ulFlags&NET_RCV_IN_BUF))) free(pch);
		pch = NULL;
		ulTotalSize = 0;
	}

	*ppucBuffer = pch;
	*pulBufferLen = ulTotalSize;

	return nReturnCode;
}


int  CNetUtil::StartServer(CNetServer* pServer, void* pMessagingObject, unsigned long ulTimeoutMilliseconds, char* pszInfo)
{
	if((pServer == NULL)||(pServer->m_usPort == NET_PORT_INVALID)||(pServer->m_lpfnHandler == NULL)) return NET_ERROR_INVALID_PARAM;

	//check to make sure its not already started:
	if(GetServerIndex(pServer->m_usPort)>=0) return NET_SOFTERROR_ALREADYSTARTED;

	int nReturnCode=NET_SUCCESS;

	pServer->m_ulThreadControl=NET_CTRL_START;
	pServer->m_ulServerStatus=NET_STATUS_NONE;
	pServer->m_lpMsgObj = pMessagingObject;  // set to NULL to disable


	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	// try to start it first
	if(_beginthread(ServerListenerThread, 0, (void*)pServer)<0)
	{
 		if(pszInfo) strcpy(pszInfo, "StartServer: error creating listener thread.");
		Message(MSG_PRI_CRITICAL|MSG_ICONERROR, "StartServer: error creating listener thread.", "CNetUtil:StartServer");
		return NET_ERROR;
	}

	_ftime( &now );
	
	while((!(pServer->m_ulServerStatus&NET_STATUS_SERVERSTARTED))
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		Sleep(1);
		_ftime( &now );
	}

	if(pServer->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
	{
		// add it to the register.
		CNetServer** ppNetServers = new CNetServer*[m_usNumServers+1]; // array of pointers
		if(ppNetServers!=NULL)
		{
			if(m_ppNetServers!=NULL)
			{
				memcpy(ppNetServers, ppNetServers, sizeof(CNetServer*)*(m_usNumServers));
				delete [] m_ppNetServers;
			}

			m_ppNetServers = ppNetServers;
			m_ppNetServers[m_usNumServers] = pServer;

			m_usNumServers++;
		}
		else  // we couldn't add this to the register! but, let the caller deal with the object.
		{
			// send an error message out
			if(pszInfo) strcpy(pszInfo, "StartServer: Could not allocate memory for server register.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "StartServer: Could not allocate memory for server register.", "CNetUtil:StartServer");
			nReturnCode=NET_ERROR_BUFFER;
		}
	}
	else
	{
		// we have to set the thing to kill itself in case it does ever get started.
		pServer->m_ulThreadControl=NET_CTRL_KILL;

		// send an error message out
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "StartServer: Could not start server. (0x%08x) %s", pServer->m_ulServerStatus, ((pServer->m_pszStatus!=NULL)&&(strlen(pServer->m_pszStatus)>0))?pServer->m_pszStatus:"");
		if(m_lpfnDM)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "StartServer: Could not start server. (0x%08x) %s", pServer->m_ulServerStatus, ((pServer->m_pszStatus!=NULL)&&(strlen(pServer->m_pszStatus)>0))?pServer->m_pszStatus:"");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:StartServer");
		}

		nReturnCode=NET_ERROR;
	}

	return nReturnCode;
}

//Stop server deletes the server object as well, but only if there is no error.
int  CNetUtil::StopServer(unsigned short usPort, unsigned long ulTimeoutMilliseconds, char* pszInfo)
{
	if(usPort == NET_PORT_INVALID) return NET_ERROR_INVALID_PARAM;

	//check to make sure its already started:
	int nReturnCode=0;
	int nIndex=GetServerIndex(usPort);
	if(nIndex<0) return NET_SOFTERROR_ALREADYSTOPPED;
	else
	if(!(m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED))
	{
		// but still remove this object from the list.
		nReturnCode = NET_SOFTERROR_ALREADYSTOPPED;
	}

	m_ppNetServers[nIndex]->m_ulThreadControl=NET_CTRL_KILL;

	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	_ftime( &now );
	while((m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		Sleep(1);
		_ftime( &now );
	}

	if(!(m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED))
	{
		// delete it from the register.

		delete m_ppNetServers[nIndex];

		unsigned short i=nReturnCode;
		while(i<m_usNumServers-1)
		{
			m_ppNetServers[i] = m_ppNetServers[++i];
		}
		
		m_usNumServers--;

		CNetServer** ppNetServers = new CNetServer*[m_usNumServers]; // array of pointers
		if(ppNetServers!=NULL)
		{
			if(m_ppNetServers!=NULL)
			{
				memcpy(ppNetServers, ppNetServers, sizeof(CNetServer*)*(m_usNumServers));
				delete [] m_ppNetServers;
			}

			m_ppNetServers = ppNetServers;
		}
//		else  // we couldn't add this to the register! but, its OK, we can just let it be in the orig buffer.

		nReturnCode=NET_SUCCESS;
	}
	else
	{
		// send an error message out, and dont remove it from list (yet)
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "StopServer: Could not stop server. (0x%08x) %s", m_ppNetServers[nIndex]->m_ulServerStatus, ((m_ppNetServers[nIndex]->m_pszStatus!=NULL)&&(strlen(m_ppNetServers[nIndex]->m_pszStatus)>0))?m_ppNetServers[nIndex]->m_pszStatus:"");
		if(m_lpfnDM)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "StopServer: Could not stop server. (0x%08x) %s", m_ppNetServers[nIndex]->m_ulServerStatus, ((m_ppNetServers[nIndex]->m_pszStatus!=NULL)&&(strlen(m_ppNetServers[nIndex]->m_pszStatus)>0))?m_ppNetServers[nIndex]->m_pszStatus:"");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:StopServer");
		}

		nReturnCode=NET_ERROR;
	}
	
	return nReturnCode;
}

int  CNetUtil::GetServerIndex(unsigned short usPort)
{
	int nIndex=-1;
	if((m_ppNetServers)&&(m_usNumServers>0))
	{
		nIndex=0;
		bool bFound = false;
		while((nIndex<m_usNumServers)&&(!bFound))
		{
			if((m_ppNetServers[nIndex])&&(m_ppNetServers[nIndex]->m_usPort == usPort))
			{
				bFound = true;
			}
			else
			{
				nIndex++;
			}
		}
		if(!bFound) nIndex=-1;
	}

	return nIndex;
}




// this opens a connection to a host, sends the data packet, receives a mandatory reply, acks if received, and closes the connection
// for persistent connections, use OpenConnection and the other SendData override (and ReceiveData if necessary)
int  CNetUtil::SendData(CNetData* pData, char* pchHost, unsigned short usPort, unsigned long ulTimeoutMilliseconds, unsigned char nRetries, unsigned long ulFlags, SOCKET* psocket, char* pszInfo)
{
	if((pData == NULL)||(pchHost == NULL)||(strlen(pchHost)<=0)||((ulFlags&NET_SND_KEEPOPENLCL)&&(psocket==NULL))) return NET_ERROR_INVALID_PARAM;
	if(psocket) *psocket = NULL;
	int nReturnCode=NET_ERROR;
	SOCKET socket;

/*
		char foo[256]; //sprintf(foo, "num conn %d",*pClient->m_pulConnections);
			_snprintf(foo, 255, "data object in: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
					pData->m_ucType,
					pData->m_ucCmd,
					pData->m_ucSubCmd,
					pData->m_ulDataLen,
					pData->m_pucData==NULL?"":(char*)pData->m_pucData				
				);
		AfxMessageBox(foo);
*/

	unsigned char i=0;
	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	_ftime( &now );

	while(	(nReturnCode<NET_SUCCESS)&&(i<=nRetries)
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		nReturnCode = OpenConnection(pchHost, usPort, &socket, ulTimeoutMilliseconds, pszInfo);
		if(nReturnCode<NET_SUCCESS) i++;  // dont increment i if not a failure
		_ftime( &now );
	}

	if(nReturnCode>=NET_SUCCESS)
	{
		if(psocket) *psocket = socket;
		// here we should have a valid socket
		// let's create a buffer with the appropriate data.
		unsigned long ulLen=3; //min chksum+type+cmd;

		if(((pData->m_pucData)==NULL)||((pData->m_ulDataLen)==0))
		{
			(pData->m_ucType) &= ~NET_TYPE_HASDATA; //no data to follow.
			(pData->m_ulDataLen)=0;
			if(pData->m_pucData) free(pData->m_pucData);
			(pData->m_pucData) = NULL;
		}
		else 
		{
			(pData->m_ucType) |= NET_TYPE_HASDATA; //data to follow.
			ulLen+=(pData->m_ulDataLen)+4; // 4 bytes for size.

		}

		if((pData->m_ucType)&NET_TYPE_HASSUBC) // subcommand to follow.
			ulLen+=1;

		unsigned char* pch = NULL;
		while(	(pch==NULL)&&(i<=nRetries)
					&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
		{
			pch = (unsigned char*)malloc(ulLen);
			if(pch==NULL) 
			{ 
				nReturnCode = NET_ERROR_BUFFER; 
				i++;  // dont increment i if not a failure
				_ftime( &now );
			}
		}

		if(pch==NULL) 
		{
			// cant continue, no buffer.
			if(pszInfo) strcpy(pszInfo, "SendData: could not allocate buffer");
			Message(MSG_ICONERROR, "SendData: could not allocate buffer", "CNetUtil:SendData");
			if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
			return nReturnCode;
		}
		
//		AfxMessageBox((char*)pData->m_pucData);

		// no just dont override the instructions to the server.
		if(ulFlags&NET_SND_KEEPOPENRMT)  //we want to keep the client side open.
			(pData->m_ucType) |= NET_TYPE_KEEPOPEN;  //have to request the server to keep the conn open
		else
			(pData->m_ucType) &= (~NET_TYPE_KEEPOPEN);  //have to request the server to close the connection

		unsigned char* pchInsert = pch+1;
		*(pchInsert++) = pData->m_ucType;  //second byte always gets type
		*(pchInsert++) = pData->m_ucCmd;		//third byte always gets cmd
		if((pData->m_ucType)&NET_TYPE_HASSUBC) 
		{
			*(pchInsert++) = pData->m_ucSubCmd;
		}
		if((pData->m_ucType)&NET_TYPE_HASDATA)
		{
//		AfxMessageBox((char*)pData->m_pucData);
			*(pchInsert++)	= (unsigned char) (((pData->m_ulDataLen) >> 24) & 0xff);
			*(pchInsert++)  = (unsigned char) (((pData->m_ulDataLen) >> 16) & 0xff);
			*(pchInsert++)  = (unsigned char) (((pData->m_ulDataLen) >>  8) & 0xff);
			*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen) & 0xff);
			memcpy(pchInsert, pData->m_pucData, (pData->m_ulDataLen));
//		AfxMessageBox((char*)pchInsert);
		}

		pch[0] = Checksum(pch+1, ulLen-1);

/*
		char foo[256]; //sprintf(foo, "num conn %d",*pClient->m_pulConnections);
			_snprintf(foo, 255, "data object: chksum: 0x%02x type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
				pch[0],
					pData->m_ucType,
					pData->m_ucCmd,
					pData->m_ucSubCmd,
					pData->m_ulDataLen,
					pData->m_pucData==NULL?"":(char*)pData->m_pucData				
				);

		AfxMessageBox(foo);

			if(pData->m_ulDataLen)
			{
				if(pData->m_ucSubCmd==0)
				{
				_snprintf(foo, 255, "data object: chksum: 0x%02x type:%d, cmd:%d, datalen:0x%02x%02x%02x%02x  data:%s total:%d", 
						pch[0],
						pch[1],
						pch[2],
						pch[3],
						pch[4],
						pch[5],
						pch[6],
						pData->m_pucData==NULL?"":(char*)pData->m_pucData		,
						ulLen
					);
				}
				else
				{
				_snprintf(foo, 255, "data object: chksum: 0x%02x type:%d, cmd:%d, scmd:%d, datalen:0x%02x%02x%02x%02x  data:%s total:%d", 
						pch[0],
						pch[1],
						pch[2],
						pch[3],
						pch[4],
						pch[5],
						pch[6],
						pch[7],
						pData->m_pucData==NULL?"":(char*)pData->m_pucData	,			
						ulLen
					);
				}
			}
			else
			{
			_snprintf(foo, 255, "data object: chksum: 0x%02x type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s total:%d",  
				pch[0],
					pch[1],
					pch[2],
					pch[3],
					pData->m_ucSubCmd,
					pData->m_ulDataLen,
					pData->m_pucData==NULL?"":(char*)pData->m_pucData		,		
						ulLen
				);
			}
		AfxMessageBox(foo);

*/
		// buffer is filled, send it
		nReturnCode = NET_ERROR;

		while(	(nReturnCode<NET_SUCCESS)&&(i<=nRetries)
					&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
		{
			nReturnCode = SendLine(pch, ulLen, socket, EOLN_NONE, false, ulTimeoutMilliseconds, pszInfo);
			_ftime( &now );
			if(nReturnCode<NET_SUCCESS) i++;  // dont increment i if not a failure
		}

		if(pch!=NULL) free(pch); //the send buffer is freed

		if((nReturnCode==NET_SUCCESS)&&(!(ulFlags&NET_SND_NO_RX)))
		{
			// the send was a success, so lets get a reply
			nReturnCode = ReceiveData(socket, pData, pszInfo);
			if(nReturnCode>=NET_SUCCESS)  //soft errors OK.
			{
				if(!(ulFlags&NET_SND_NO_RXACK))
				{
					if((pData->m_ucCmd) == NET_CMD_NAK)  // the reply was no goood, but comm still up
						SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_NAK|NET_SND_NO_RX, pszInfo); // ack the nak
					else
						SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo);  //nak.
				}

				if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
			}
			else
			{
				if(!(ulFlags&NET_SND_NO_RXACK))
				{
					// try to send a NAK no matter what.
					SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo); // nak all.
				}
				if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
			}
		}
		else
		{
			if(!(ulFlags&NET_SND_KEEPOPENLCL)) CloseConnection(socket);
		}
	}

	return nReturnCode;
}

// this uses an existing socket, sends the data packet, receives a mandatory reply, and does NOT close the connection -  have to do this explicitly
int  CNetUtil::SendData(CNetData* pData, SOCKET socket, unsigned long ulTimeoutMilliseconds, unsigned char nRetries, unsigned long ulFlags, char* pszInfo)
{
	if(((pData == NULL)&&(!(ulFlags&(NET_SND_ACK|NET_SND_NAK))))||(socket == NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode=NET_ERROR;

	unsigned char i=0;
	// calculate timeout
	_timeb timebuffer, now;
	_ftime( &timebuffer );

	timebuffer.time += (ulTimeoutMilliseconds/1000);
	timebuffer.millitm += (unsigned short)(ulTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

	_ftime( &now );


	// here we should have a valid socket

	if(ulFlags&(NET_SND_ACK|NET_SND_NAK))
	{
		CNetData* pAckData = new CNetData;
		// create an ACK/NAK buffer
		if(pData)
		{
			memcpy(pAckData, pData, sizeof(CNetData));
			(pAckData->m_ucType) &= ~(NET_TYPE_HASDATA|NET_TYPE_HASSUBC);
			(pAckData->m_ucCmd) = (ulFlags&NET_SND_ACK)?NET_CMD_ACK:NET_CMD_NAK;
			(pAckData->m_ucSubCmd) = 0;
			(pAckData->m_ulDataLen) = 0;
			(pAckData->m_pucData) = NULL;				
		}
		else
		{
			(pAckData->m_ucType) = NET_TYPE_PROTOCOL1;
			(pAckData->m_ucCmd) = (ulFlags&NET_SND_ACK)?NET_CMD_ACK:NET_CMD_NAK;
			(pAckData->m_ucSubCmd) = 0;
			(pAckData->m_ulDataLen) = 0;
			(pAckData->m_pucData) = NULL;				
		}

		pData = pAckData;  // the old pData is now ignored.

	}
	// let's create a buffer with the appropriate data.
	unsigned long ulLen=3; //min chksum+type+cmd;

	if(((pData->m_pucData)==NULL)||((pData->m_ulDataLen)==0))	pData->m_ucType&=~NET_TYPE_HASDATA; //no data to follow.
	else 
	{
		pData->m_ucType|=NET_TYPE_HASDATA; //data to follow.
		ulLen+=pData->m_ulDataLen+4; // 4 bytes for size.
	}

	if(pData->m_ucType&NET_TYPE_HASSUBC) // subcommand to follow.
		ulLen+=1;

	unsigned char* pch = NULL;
	while(	(pch==NULL)&&(i<=nRetries)
		&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		pch = (unsigned char*)malloc(ulLen);
		if(pch==NULL) 
		{ 
			nReturnCode = NET_ERROR_BUFFER; 
			i++;  // dont increment i if not a failure
			_ftime( &now );
		}
	}

	if(pch==NULL) 
	{
		// cant continue, no buffer.
		if(pszInfo) _snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "SendData: could not allocate buffer after %d attempts.%s",
				i, ((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm)))?"":"timed out");
		if(m_lpfnDM)
		{
			char errorstring[NET_ERRORSTRING_LEN];
			_snprintf(errorstring, NET_ERRORSTRING_LEN-1, "SendData: could not allocate buffer after %d attempts.%s",
				i, ((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm)))?"":"timed out");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "CNetUtil:SendData");
		}

		if(ulFlags&(NET_SND_ACK|NET_SND_NAK)) delete pData;

		return nReturnCode;
	}
	
	if(ulFlags&NET_SND_KEEPOPENRMT)
		(pData->m_ucType)|=NET_TYPE_KEEPOPEN;  //have to request the server to keep the conn open
	else
		(pData->m_ucType)&= (~NET_TYPE_KEEPOPEN);  //have to request the server to close the connection

	unsigned char* pchInsert = pch+1;
	*(pchInsert++) = pData->m_ucType;  //second byte always gets type
	*(pchInsert++) = pData->m_ucCmd;		//third byte always gets cmd
	if((pData->m_ucType)&NET_TYPE_HASSUBC) 
	{
		*(pchInsert++) = pData->m_ucSubCmd;
	}
	if((pData->m_ucType)&NET_TYPE_HASDATA)
	{
		*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen >> 24) & 0xff);
		*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen >> 16) & 0xff);
		*(pchInsert++)  = (unsigned char) ((pData->m_ulDataLen >>  8) & 0xff);
		*(pchInsert++)  = (unsigned char) (pData->m_ulDataLen & 0xff);
		memcpy(pchInsert, pData->m_pucData, pData->m_ulDataLen);
	}

	pch[0] = Checksum(pch+1, ulLen-1);

	// buffer is filled, send it
	nReturnCode = NET_ERROR;

	while(	(nReturnCode<NET_SUCCESS)&&(i<=nRetries)
				&&((now.time<timebuffer.time)||((now.time==timebuffer.time)&&(now.millitm<=timebuffer.millitm))))
	{
		nReturnCode = SendLine(pch, ulLen, socket, EOLN_NONE, false, ulTimeoutMilliseconds, pszInfo);
		_ftime( &now );
		if(nReturnCode<NET_SUCCESS) i++;  // dont increment i if not a failure
	}
	
	if(pch!=NULL) free(pch); //the send buffer is freed

	if((nReturnCode==NET_SUCCESS)&&(!(ulFlags&NET_SND_NO_RX)))
	{
		// the send was a success, so lets get a reply
		nReturnCode = ReceiveData(socket, pData, pszInfo);
		if(nReturnCode>=NET_SUCCESS)  //soft errors OK.
		{
			if(!(ulFlags&NET_SND_NO_RXACK))
			{
				if((pData->m_ucCmd) == NET_CMD_NAK)  // the reply was no goood, but comm still up
					SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_NAK|NET_SND_NO_RX, pszInfo); // ack the nak
				else
					SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo);  //nak.
			}
		}
		else
		{
			if(!(ulFlags&NET_SND_NO_RXACK))
			{
				// try to send a NAK no matter what.
				SendData(NULL, socket, ulTimeoutMilliseconds, nRetries-i, NET_SND_ACK|NET_SND_NO_RX, pszInfo); // nak all.
			}
		}
	
	}
	if(ulFlags&(NET_SND_ACK|NET_SND_NAK)) delete pData;  // if this is just an ack, delete the ack object.

	return nReturnCode;	
}

// this receives formatted data on an existing socket
int  CNetUtil::ReceiveData(SOCKET socket, CNetData* pData, char* pszInfo)
{
	if((socket == NULL)||(pData==NULL)) return NET_ERROR_INVALID_PARAM;
	int nReturnCode;
	unsigned char* pchTempBuffer = (unsigned char*) malloc(8);
	if(pchTempBuffer==NULL)
	{
		if(pszInfo) strcpy(pszInfo, "ReceiveData: could not allocate temp buffer.");
		Message(MSG_ICONERROR, "ReceiveData: could not allocate temp buffer.", "CNetUtil:ReceiveData");
		return NET_ERROR_BUFFER;
	}
	unsigned char* pch = NULL;
	unsigned long ulBufferLen = 3;

	if(pData->m_pucData)
	{
		free(pData->m_pucData);
		pData->m_pucData = NULL;
	}
	pData->m_ulDataLen = 0;

	nReturnCode = GetLine(&pchTempBuffer, &ulBufferLen, socket, NET_RCV_ONCE|NET_RCV_LENGTH|NET_RCV_IN_BUF, pszInfo);

	if(nReturnCode==NET_SUCCESS)
	{
		unsigned char chChkSumTotal = Checksum(pchTempBuffer+1, 2);
		unsigned char chChkSum = *pchTempBuffer;
		pData->m_ucType = *(pchTempBuffer+1);
		pData->m_ucCmd =  *(pchTempBuffer+2);
		pData->m_ucSubCmd = 0;

//		char foo[256]; sprintf(foo, "received buffer: [%02x][%02x][%02x]",chChkSum, 	pData->m_ucType	,	pData->m_ucCmd);
//		AfxMessageBox(foo);

		if((pData->m_ucType)&NET_TYPE_HASSUBC)
		{
			ulBufferLen = 1;
			nReturnCode = GetLine(&pchTempBuffer, &ulBufferLen, socket, NET_RCV_ONCE|NET_RCV_LENGTH|NET_RCV_IN_BUF, pszInfo);

			if(nReturnCode==NET_SUCCESS)
			{
				chChkSumTotal += Checksum(pchTempBuffer, 1);
				pData->m_ucSubCmd = (*pchTempBuffer);
			}
			else
			{
				if(pchTempBuffer!=NULL) free(pchTempBuffer);
				return nReturnCode;
			}
		}

		if((pData->m_ucType)&NET_TYPE_HASDATA)
		{
			ulBufferLen = 4;
			nReturnCode = GetLine(&pchTempBuffer, &ulBufferLen, socket, NET_RCV_ONCE|NET_RCV_LENGTH|NET_RCV_IN_BUF, pszInfo);

			if(nReturnCode==NET_SUCCESS)
			{

				chChkSumTotal += Checksum(pchTempBuffer, 4);
				ulBufferLen = 0;
				for(unsigned char i=0; i<4; i++)
					ulBufferLen = (ulBufferLen<<8) + (unsigned long)((unsigned char)(*(pchTempBuffer+i)));

				pch = NULL;
				nReturnCode = GetLine(&pch, &ulBufferLen, socket, NET_RCV_LENGTH, pszInfo);
				if(nReturnCode==NET_SUCCESS)
				{
					chChkSumTotal += Checksum(pch, ulBufferLen);
					if(chChkSumTotal!=chChkSum)
					{
						if(pszInfo) 
						{
							_snprintf(pszInfo, NET_ERRORSTRING_LEN-1, "ReceiveData: incorrect checksum; %d received, %d calculated", chChkSum, chChkSumTotal);
//							AfxMessageBox(pszInfo);
						}	
						nReturnCode = NET_ERROR_CHECKSUM;
					}
					else
					if((ulBufferLen>2)&&(pch!=NULL)&&(*(pch+2) == NET_CMD_NAK))
					{
						if(pszInfo) 
						{
							strcpy(pszInfo, "ReceiveData: NAK received");
//							AfxMessageBox(pszInfo);
						}	
						nReturnCode = NET_ERROR_NAK;
					}

					pData->m_pucData = (unsigned char*)pch;
					pData->m_ulDataLen = ulBufferLen;

				}
				else
				{
					if(pch) free(pch);
					pData->m_pucData = NULL;
					pData->m_ulDataLen = 0;
				}
			}
			else
			{
				if(pchTempBuffer!=NULL) free(pchTempBuffer);
				return nReturnCode;
			}
		}
	}
	
/*
	pData->m_ulDataLen = ulBufferLen;  // pass in the buffer length in the data object.

	if(nReturnCode==NET_SUCCESS)
		nReturnCode = InterpretData(pucBuffer, pData, true, pszInfo);
*/
	if(pchTempBuffer!=NULL) free(pchTempBuffer);
	return nReturnCode;
}

// this fills a CNetData object with info from a buffer
// only destroys buffer if succeeds, and destroy flag is set
// length of data buffer is passed in with pData->m_ulDataLen, this member gets reset.
int  CNetUtil::InterpretData(unsigned char* pucBuffer, CNetData* pData, bool bDestroyBuffer, char* pszInfo)
{
	if((pData == NULL)||(pucBuffer==NULL)||(pData->m_ulDataLen<3)) return NET_ERROR_INVALID_PARAM;
	if(sizeof(pucBuffer)<pData->m_ulDataLen) return NET_ERROR_INCOMPLETEDATA;
	if(Checksum(pucBuffer+1, pData->m_ulDataLen-1)!= (*pucBuffer)) return NET_ERROR_CHECKSUM;

	unsigned long ulExpectedDataLen = pData->m_ulDataLen-3;  // minus checksum, type, and cmd
	int nReturnCode=NET_SUCCESS;
	unsigned char* pch;
	pData->m_ucType = *(pucBuffer+1);
	pData->m_ucCmd = *(pucBuffer+2);
	if(pData->m_pucData) free(pData->m_pucData);

	if(pData->m_ucType&NET_TYPE_HASSUBC) { pData->m_ucSubCmd= *(pucBuffer+3); pch=pucBuffer+4; ulExpectedDataLen--; } //minus subcmd byte
	else { pData->m_ucSubCmd= NULL; pch=pucBuffer+3; }

	if(pData->m_ucType&NET_TYPE_HASDATA)
	{
		ulExpectedDataLen-=4; //minus size bytes
		pData->m_ulDataLen = 0;
		for(char i=0; i<4; i++)
		{
			pData->m_ulDataLen = (pData->m_ulDataLen<<8) + (unsigned long)((unsigned char)(*(pch++)));
		}

		if(pData->m_ulDataLen>0)
		{
			char* pchData = (char*)malloc(max(pData->m_ulDataLen, ulExpectedDataLen));
			if(pchData)
			{
				memcpy(pchData, pch, min(ulExpectedDataLen, pData->m_ulDataLen));
				pData->m_pucData = (unsigned char*)pchData;
				if(pData->m_ulDataLen>ulExpectedDataLen)
				{
					if(pszInfo) strcpy(pszInfo, "InterpretData: received fewer bytes than indicated.");
					Message(MSG_ICONERROR, "InterpretData: received fewer bytes than indicated", "CNetUtil:InterpretData");
				}
			}
			else
			{ 
				pData->m_pucData = NULL;
				pData->m_ulDataLen = 0;
				if(pszInfo) strcpy(pszInfo, "InterpretData: could not allocate buffer");
				Message(MSG_ICONERROR, "InterpretData: could not allocate buffer", "CNetUtil:InterpretData");
				nReturnCode = NET_ERROR_BUFFER;
			}
		}
		else
		{
			pData->m_pucData = NULL;
		}
	}
	else
	{ 
		pData->m_pucData = NULL;
		pData->m_ulDataLen = 0;
	}
	
	if((bDestroyBuffer)&&(nReturnCode>=NET_SUCCESS)) free(pucBuffer);

	return nReturnCode;

}


char* CNetUtil::WinsockEnglish(int nError)  // must free the buffer when done with it (outside).
{
	char* pch;
	char buffer[512];
  switch (nError)
	{
	case WSAEACCES: sprintf(buffer, "Attempt to connect datagram socket to broadcast address failed because setsockopt option SO_BROADCAST is not enabled."); break;
	case WSAEADDRINUSE: sprintf(buffer, "The socket's local address is already in use and the socket was not marked to allow address reuse with SO_REUSEADDR. This error usually occurs when executing bind, but could be delayed until this function if the bind was to a partially wildcard address (involving ADDR_ANY) and if a specific address needs to be committed at the time of this function."); break;
	case WSAEADDRNOTAVAIL: sprintf(buffer, "The remote address is not a valid address (such as ADDR_ANY)."); break;
	case WSAEAFNOSUPPORT: sprintf(buffer, "Addresses in the specified family cannot be used with this socket."); break;
	case WSAEALREADY: sprintf(buffer, "A nonblocking connect call is in progress on the specified socket. Note In order to preserve backward compatibility, this error is reported as WSAEINVAL to Windows Sockets 1.1 applications that link to either Winsock.dll or Wsock32.dll. "); break;
	case WSAECONNABORTED: sprintf(buffer, "The virtual circuit was terminated due to a time-out or other failure. The application should close the socket as it is no longer usable. "); break;
	case WSAECONNREFUSED: sprintf(buffer, "The attempt to connect was forcefully rejected."); break;
	case WSAECONNRESET: sprintf(buffer, "The connection was reset."); break;
	case WSAEFAULT: sprintf(buffer, "The parameter is not a valid part of the user address space, the namelen parameter is too small, or the name parameter contains incorrect address format for the associated address family."); break;
	case WSAEINPROGRESS: sprintf(buffer, "A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function."); break;
	case WSAEINTR: sprintf(buffer, "The blocking Windows Socket 1.1 call was canceled through WSACancelBlockingCall."); break;
	case WSAEINVAL: sprintf(buffer, "The parameter s is a listening socket, or is not bound."); break;
	case WSAEISCONN: sprintf(buffer, "The socket is already connected (connection-oriented sockets only)."); break;
	case WSAEMSGSIZE: sprintf(buffer, " The message was too large to fit into the specified buffer and was truncated. "); break;
	case WSAENETDOWN: sprintf(buffer, "The network subsystem has failed."); break;
	case WSAENETRESET: sprintf(buffer, " The connection has been broken due to the keep-alive activity detecting a failure while the operation was in progress. "); break;
	case WSAENETUNREACH: sprintf(buffer, "The network cannot be reached from this host at this time."); break;
	case WSAENOBUFS: sprintf(buffer, "No buffer space is available. The socket cannot be connected."); break;
	case WSAENOTCONN: sprintf(buffer, "The socket is not connected. "); break;
	case WSAENOTSOCK: sprintf(buffer, "The descriptor is not a socket."); break;
	case WSAEOPNOTSUPP: sprintf(buffer, "MSG_OOB was specified, but the socket is not stream-style such as type SOCK_STREAM, OOB data is not supported in the communication domain associated with this socket, or the socket is unidirectional and supports only send operations. "); break;
	case WSAESHUTDOWN: sprintf(buffer, " The socket has been shut down; it is not possible to receive on a socket after shutdown has been invoked with how set to SD_RECEIVE or SD_BOTH. "); break;
	case WSAETIMEDOUT: sprintf(buffer, "Attempt to connect timed out without establishing a connection."); break;
	case WSAEWOULDBLOCK : sprintf(buffer, "The socket is marked as nonblocking and the operation cannot be completed immediately. "); break;
	case WSAHOST_NOT_FOUND : sprintf(buffer, "Authoritative Answer Host not found."); break;
	case WSANO_DATA: sprintf(buffer, "Valid name, no data record of requested type."); break;
	case WSANO_RECOVERY: sprintf(buffer, "Nonrecoverable error occurred."); break;
	case WSANOTINITIALISED: sprintf(buffer, "A successful WSAStartup call must occur before using this function."); break;
	case WSATRY_AGAIN: sprintf(buffer, "Non-Authoritative Host not found, or server failure."); break;
	default:
		{
			FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL, nError,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR) &pch, 0, NULL);
			return pch;
		}  break;
	}

	pch = (char*) malloc(strlen(buffer)+1);
	strcpy(pch, buffer);
	return pch;
}

unsigned char CNetUtil::Checksum(unsigned char* pucBuffer, unsigned long ulLen) 
{
  unsigned long i;
	unsigned char sum=0;
  for (i=0; i<ulLen; i++) sum += (unsigned char) pucBuffer[i]; 
  return sum;
}


void ServerListenerThread(void* pvArgs)
{
	CNetServer* pServer = (CNetServer*) pvArgs;
	if(pServer==NULL) { _endthread(); return; }

	pServer->m_ulServerStatus = NET_STATUS_THREADSTARTED;
	if(pServer->m_pszStatus) free(pServer->m_pszStatus);
	pServer->m_pszStatus = (char*)malloc(NET_ERRORSTRING_LEN);
	if(pServer->m_pszStatus) strcpy(pServer->m_pszStatus, "");
	
	pServer->m_ulConnections = 0;		// reset just in case

	SOCKET socketServer;
	SOCKET socketClient;
	SOCKADDR_IN siServer;
	SOCKADDR_IN siClient;
	int nClient;
	CNetUtil net(false); // local object for utility functions.


	while(!(pServer->m_ulThreadControl&NET_CTRL_KILL))
	{

		socketServer = socket(AF_INET,SOCK_STREAM,0);
		if (socketServer == INVALID_SOCKET)
		{
			pServer->m_ulServerStatus = NET_STATUS_ERROR; // removes NET_STATUS_THREADSTARTED

 			if(pServer->m_pszStatus)
			{
				int nErrorCode = WSAGetLastError();
				char* pchError = net.WinsockEnglish(nErrorCode);
				_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: invalid socket. %s", pchError);
				if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_CRITICAL|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
				free(pchError);
			}
			_endthread();
			return;
		}
		
		siServer.sin_family = AF_INET;
		siServer.sin_port = htons(pServer->m_usPort);
		siServer.sin_addr.s_addr = htonl(INADDR_ANY);

		if ( bind(socketServer,(struct sockaddr *) &siServer,sizeof(SOCKADDR_IN)) == SOCKET_ERROR )
		{
			pServer->m_ulServerStatus = NET_STATUS_ERROR; // removes NET_STATUS_THREADSTARTED

 			if(pServer->m_pszStatus)
			{
 				int nErrorCode = WSAGetLastError();
				char* pchError = net.WinsockEnglish(nErrorCode);
				_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in bind. %s", pchError);
				if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_CRITICAL|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
				free(pchError);
			}
			shutdown(socketServer, SD_BOTH);
			closesocket(socketServer);
			_endthread();
			return;
		}

		nClient = sizeof(SOCKADDR_IN);

		if ( listen(socketServer,SOMAXCONN) == SOCKET_ERROR )
		{
			pServer->m_ulServerStatus = NET_STATUS_ERROR; // removes NET_STATUS_THREADSTARTED

			if(pServer->m_pszStatus)
			{
 				int nErrorCode = WSAGetLastError();
				char* pchError = net.WinsockEnglish(nErrorCode);
				_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in listen. %s", pchError);
				if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_CRITICAL|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
				free(pchError);
			}
			shutdown(socketServer, SD_BOTH);
			closesocket(socketServer);
			_endthread();
			return;
		}

		pServer->m_ulServerStatus |= NET_STATUS_SERVERSTARTED;
		pServer->m_ulServerStatus &= ~NET_STATUS_ERROR;
	
//	AfxMessageBox("here");

	  struct timeval tv;
		tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
		fd_set fds;
		int nNumSockets;
		FD_ZERO(&fds);


		while(!(pServer->m_ulThreadControl&NET_CTRL_KILL))
		{
			FD_SET(socketServer, &fds);
			nNumSockets = select(0, &fds, NULL, NULL, &tv);

			if ( nNumSockets == INVALID_SOCKET )
			{
 				if(pServer->m_pszStatus)
				{
					pServer->m_ulServerStatus = (NET_STATUS_ERROR|NET_STATUS_THREADSTARTED); // removes server started, we have to try to start it again.
 					int nErrorCode = WSAGetLastError();
					char* pchError = net.WinsockEnglish(nErrorCode);
					_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in select. %s", pchError);
					if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_HIGH|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
					free(pchError);
				}
				shutdown(socketServer, SD_BOTH);
				closesocket(socketServer);
				break; // breaks out of small while, re-inits a server listener.
			}
			else
      if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(socketServer, &fds)))
				) { continue; }  // else go ahead

			socketClient = accept(socketServer,(struct sockaddr *)&siClient, &nClient);
			if(!(pServer->m_ulThreadControl&NET_CTRL_KILL))  // may have changed states while waiting for accept
			{
				if ( socketClient == INVALID_SOCKET )
				{
					// we should have prevented this with a check at select, but if this has happened we whould try to re-init everything
 					if(pServer->m_pszStatus)
					{
						pServer->m_ulServerStatus = (NET_STATUS_ERROR|NET_STATUS_THREADSTARTED); // removes server started, we hae to try to start it again.
 						int nErrorCode = WSAGetLastError();
						char* pchError = net.WinsockEnglish(nErrorCode);
						_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: socket error in accept. %s", pchError);
						if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_HIGH|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
						free(pchError);
					}
					shutdown(socketServer, SD_BOTH);
					closesocket(socketServer);
					break; // breaks out of small while, re-inits a server listener.
				}
				else
				{
					CNetClient* pclient = new CNetClient;
					if(pclient)
					{
						pclient->m_socket = socketClient;
						pclient->m_si = siClient;
						pclient->m_lpObject = pServer->m_lpObject;				// pointer to the object passed to the handler thread.
						pclient->m_lpMsgObj	= pServer->m_lpMsgObj;					// pointer to the Message function.
						pclient->m_pulConnections = &(pServer->m_ulConnections); // pointer to connection counter
						pclient->m_pulThreadControl = &(pServer->m_ulThreadControl); // pointer to commands to the server thread, in case we need to kill persistent connection client threads.
						pclient->m_ucType = pServer->m_ucType;

						// handle every request in its own thread
						if( _beginthread((void (__cdecl *)(void *)) pServer->m_lpfnHandler, 0, (void*)pclient)<0)
						{
 							if(pServer->m_pszStatus)
							{
								_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: error creating handler thread.");
								if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
							}
							shutdown(socketClient, SD_BOTH);
							closesocket(socketClient);
						}
					}
					else
					{
 						if(pServer->m_pszStatus)
						{
							_snprintf(pServer->m_pszStatus, NET_ERRORSTRING_LEN-1, "ServerListenerThread: error allocating client object.");
							if(pServer->m_lpMsgObj) ((CMessagingObject*)pServer->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pServer->m_pszStatus, "CNetUtil:ServerListenerThread");
						}
						shutdown(socketClient, SD_BOTH);
						closesocket(socketClient);
					}
				}
			}
		}
		Sleep(50); // wait a short while before trying to re-init the server
	}

	if(pServer)
	{
		if(pServer->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
		{
			if(socketServer!=INVALID_SOCKET)
			{
				shutdown(socketServer, SD_BOTH);
				closesocket(socketServer);
			}
			pServer->m_ulServerStatus&=~NET_STATUS_SERVERSTARTED;
		}
		
		pServer->m_ulServerStatus&=~NET_STATUS_THREADSTARTED;
		pServer->m_ulConnections = 0;
	}
	else
	{
		if(socketServer!=INVALID_SOCKET)
		{
			shutdown(socketServer, SD_BOTH);
			closesocket(socketServer);
		}
	}
}

// the following thread is provided as a prototype for supplied handler threads.
// objects of the users choice may be passed in via the pClient->m_lpObject pointer
// this sample thread accepts protocol 1 data and simply sends a protocol 1 ack.
void ServerHandlerThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) _endthread();

	CNetUtil net(false); // local object for utility functions.

	//char* pch;// for use with GetLine().
	//unsigned long ulBufLen;// for use with GetLine().
	int nReturn;
	char pszStatus[NET_ERRORSTRING_LEN];
	strcpy(pszStatus, "");  // clear it

	CNetData data;

	bool bCloseCommand = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

//		char foo[256]; sprintf(foo, "num conn %d",*pClient->m_pulConnections);
//		AfxMessageBox(foo);

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			// or

			// get the raw buffer and process it yourself
			//nReturn = net.GetLine(&pch, &ulBufLen, pClient->m_socket, pszStatus);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
	//		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
			{
//		AfxMessageBox(pszStatus);
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
/*
		char foo[256]; //sprintf(foo, "num conn %d",*pClient->m_pulConnections);
			_snprintf(foo, 255, "%d(%d): type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s",  clock(), *pClient->m_pulConnections,
					data.m_ucType,
					data.m_ucCmd,
					data.m_ucSubCmd,
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);
		AfxMessageBox(foo);
*/

				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
//AfxMessageBox(pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.


			// if(pch!=NULL) free(pch);  // must free the data buffer if the raw buffer was used.
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

//		AfxMessageBox("X");
	}

	delete pClient; // was created with new in the thread that spawned this one.
}


/*
	// calculate timeout
	_timeb timebuffer;
	_ftime( &timebuffer );

	timebuffer.time += (nTimeoutMilliseconds/1000);
	timebuffer.millitm += (nTimeoutMilliseconds%1000);
	if(timebuffer.millitm>999)
	{
		timebuffer.millitm-=1000;
		timebuffer.time++;
	}

*/

