// NetUtil.h: interface for the CNetUtil and support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NETUTIL_H__6F60AA3A_D15B_4A14_8A74_A56DB485ACF6__INCLUDED_)
#define AFX_NETUTIL_H__6F60AA3A_D15B_4A14_8A74_A56DB485ACF6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// the following line forces a link with the winsock lib
#pragma comment(lib, "ws2_32.lib")
#include <winsock2.h>
#include "NetDefines.h"
#include "../MSG/MessagingObject.h"


// prototypes
void ServerListenerThread(void* pvArgs);  // main server listener
void ServerHandlerThread(void* pvArgs);   // sample protocol 1 handler
void DatagramServerHandlerThread(void* pvArgs); // sample datagram handler

// support classes
class CNetData
{
public:
	CNetData();
	virtual ~CNetData();

	//command and data
	unsigned char  m_ucType;      // defined type - indicates which protocol to use, structure of data
	unsigned char  m_ucCmd;       // the command byte
	unsigned char  m_ucSubCmd;    // the subcommand byte - if used with commmand byte gives up to 65535 possible commands - good enough for all practicality.  more than this can be accomplished with the attached data payload
	unsigned char* m_pucData;     // pointer to the payload data
	unsigned long m_ulDataLen;		// length of the payload data buffer.
	char* m_pszUser;     // pointer to the username, if the protocol supports it - zero terminated
	char* m_pszPassword; // pointer to the password, if the protocol supports it - zero terminated
};


class CNetServer
{
public:
	CNetServer();
	virtual ~CNetServer();

	//command and data
	unsigned short m_usPort;  // port on which the server listens
	unsigned char  m_ucType;						// defined type - indicates which protocol to use, structure of data
	char* m_pszName;					// name of the server, for human readability
	unsigned long m_ulConnections;		// number of simultaneous connections
	unsigned long m_ulThreadControl;	// commands to the server thread
	unsigned long m_ulServerStatus;		// status back from thread
	char* m_pszStatus;				// status buffer with error messages from thread
	void* m_lpfnHandler;			// pointer to the thread that handles the request.
	void* m_lpObject;					// pointer to the object passed to the handler thread.
	void* m_lpMsgObj;					// pointer to the object with the Message function.

	// last three args added to override TCP to do UDP.
	int m_nAf; 
	int m_nType; 
	int m_nProtocol;
};

typedef struct CNetClient
{
	SOCKET m_socket;
	SOCKADDR_IN m_si;							// used for internet sockets
	unsigned char  m_ucType;						// defined type - indicates which protocol to use, structure of data
	unsigned long* m_pulConnections;		// pointer to connection counter
	unsigned long* m_pulThreadControl;	// pointer to commands to the server thread, in case we need to kill persistent connection client threads.
	void* m_lpObject;					// pointer to the object passed to the handler thread.
	void* m_lpMsgObj;					// pointer to the object with the Message function.
} CNetClient;


typedef struct CNetDatagramData
{
	SOCKADDR_IN m_si;									// used for datagram sockets
	unsigned char* m_pucRecv;					// received data
	unsigned long  m_ulRecv;					// bytes received
	void* m_lpObject;					// pointer to the object used in the handler thread.
} CNetDatagramData;


// main util class
class CNetUtil : public CMessagingObject 
{
public:
	bool m_bNetStarted;
	unsigned long m_ulFlags;

public:
	CNetUtil(bool bInitialize=true);
	virtual ~CNetUtil();

	CNetServer** m_ppNetServers;
	unsigned short m_usNumServers;

public:
	// all pszInfo args must point to a buffer of NET_ERRORSTRING_LEN bytes or more, or be NULL.

	// generic
	int  Initialize(char* pszInfo=NULL);
	int  OpenConnection(char* pchHost, short nPort, SOCKET* ps, unsigned long ulSendTimeoutMilliseconds=5000, unsigned long ulReceiveTimeoutMilliseconds=5000, char* pszInfo=NULL, int nAf=AF_INET, int nType=SOCK_STREAM, int nProtocol=IPPROTO_IP); // last three args added to override TCP to do UDP.
  int  CloseConnection(SOCKET s, char* pszInfo=NULL);
	int  SendLine(unsigned char* pucBuffer, unsigned long ulBufferLen, SOCKET socket, char chEolnType=EOLN_NONE, bool bDestroyBuffer=false, unsigned long ulTimeoutMilliseconds=30000, char* pszInfo=NULL);
	int  GetLine(unsigned char** ppucBuffer, unsigned long* pulBufferLen, SOCKET socket, unsigned long ulFlags=NET_RCV_ONCE, char* pszInfo=NULL);

	int  StartServer(CNetServer* pServer, void* pMessagingObject=NULL, unsigned long ulTimeoutMilliseconds=5000, char* pszInfo=NULL);
	int  StopServer(unsigned short usPort, unsigned long ulTimeoutMilliseconds=5000, char* pszInfo=NULL);
	int  GetServerIndex(unsigned short usPort);

	// specific to NetUtil protocols
	int  SendData(
		unsigned char* pucType,				// defined type - indicates which protocol to use, structure of data
		unsigned char* pucCmd,				// the command byte
		unsigned char* pucSubCmd,		  // the subcommand byte - if used with commmand byte gives up to 65535 possible commands - good enough for all practicality.  more than this can be accomplished with the attached data payload
		unsigned char** ppucData,     // pointer to the payload data
		unsigned long* pulDataLen,		// length of the payload data buffer.
		char* pchHost,							// if port is not zero, otherwise the socket in SOCKET* psocket is used (if not NULL)
		unsigned short usPort,      // set to zero to use pchHostOrSocket as a SOCKET*
		unsigned long ulTimeoutMilliseconds=10000, 
		unsigned char nRetries=0, 
		unsigned long ulFlags=NET_SND_DEFAULT, 
		SOCKET* psocket=NULL, 
		char* pszInfo=NULL);

	int  SendData(CNetData* pData, char* pchHost, unsigned short usPort, unsigned long ulTimeoutMilliseconds=10000, unsigned char nRetries=0, unsigned long ulFlags=NET_SND_DEFAULT, SOCKET* psocket=NULL, char* pszInfo=NULL);
	int  SendData(CNetData* pData, SOCKET socket, unsigned long ulTimeoutMilliseconds=10000, unsigned char nRetries=0, unsigned long ulFlags=NET_SND_DEFAULT, char* pszInfo=NULL);
	int  ReceiveData(SOCKET socket, CNetData* pData, char* pszInfo=NULL);
	int  InterpretData(unsigned char* pucBuffer, CNetData* pData, bool bDestroyBuffer=false, char* pszInfo=NULL);

	int  TranslateSecurity(CNetData* pData, unsigned long ulFlags=NET_2TO1); // for protocol 2, translates from 2 to 1, then all other functions are usable

	//util
  char* WinsockEnglish(int nError);  // must free the buffer when done with it (outside).
	unsigned char Checksum(unsigned char* pucBuffer, unsigned long ulLen);

};

#endif // !defined(AFX_NETUTIL_H__6F60AA3A_D15B_4A14_8A74_A56DB485ACF6__INCLUDED_)
