// DBUtil.h: interface for the CDBUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DBUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
#define AFX_DBUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../MSG/MessagingObject.h"
#include <afxdb.h>

#define DB_MAXFIELDS		USHRT_MAX
#define DB_MAXTABLES		USHRT_MAX
#define DB_MAXCONNS			UCHAR_MAX

//return codes
#define DB_EXISTS						1
#define DB_SUCCESS					0
#define DB_ERROR						-1
#define DB_ERROR_NOTEXISTS	-2
#define DB_ERROR_NOTOPEN		-3
#define DB_ERROR_DBEX				-4
#define DB_ERROR_MEMEX			-5
#define DB_ERROR_EX					-6
#define DB_ERROR_BADARG			-7



#define DB_ERRORSTRING_LEN	512
#define DB_SQLSTRING_MAXLEN	8192
#define DB_FIELDNAME_MAXLEN	80


#define DB_PRIMARY			0x00000000
#define DB_BACKUP				0x00000001
#define DB_AUX					0x00000002

#define DB_MSSQL  			0
#define DB_MYSQL  			1


//types
#define DB_TYPE_NDEF		0x00  // not defined

// integer types
#define DB_TYPE_BYTE		0x01  // unsigned char   (tiny int)     0 through 255.
#define DB_TYPE_SHORT		0x02  // signed short    (small int)   -2^15 (-32,768) through 2^15 - 1 (32,767).
#define DB_TYPE_INT			0x03  // signed long 4 bytes.  (int)   -2^31 (-2,147,483,648) through 2^31 - 1 (2,147,483,647).
#define DB_TYPE_BIGINT	0x04  // signed double long 8 bytes.  (big int, datetime,) -2^63 (-9223372036854775808) through 2^63-1 (9223372036854775807).

// string
#define DB_TYPE_STR			0x05  // fixed width array of char, max 8000.
#define DB_TYPE_VSTR		0x06  // variable array of char, max 8000.
#define DB_TYPE_TEXT		0x07  // variable array of char, max 2^31 - 1 (2,147,483,647) characters.

//floating point
#define DB_TYPE_FLOAT		0x08  // float (real), -3.40E + 38 through 3.40E + 38.    7 digit precision
#define DB_TYPE_DOUBLE	0x09  // double (float)  -1.79E + 308 through 1.79E + 308.    15 digit precision

// binary
#define DB_TYPE_BIN			0x0a  // fixed width array of bytes, max 8000.
#define DB_TYPE_VBIN		0x0b  // variable array of bytes, max 8000.
#define DB_TYPE_DATA		0x0c  // variable array of bytes, max 2^31 - 1 (2,147,483,647) bytes.


// flags
#define DB_UPDATEINFO		0x01
#define DB_FAILIFNOINFO	0x02

// operation types
#define DB_OP_IGNORE		0x00
#define DB_OP_EQUAL			0x01
#define DB_OP_GT				0x02
#define DB_OP_LT				0x04
#define DB_OP_AND				0x00
#define DB_OP_OR				0x10

//order by order
#define DB_ORD_IGNORE		0x00
#define DB_ORD_ASC			0x01
#define DB_ORD_DESC			0x02


// fields
class CDBfield
{
public:
	CDBfield();
	virtual ~CDBfield();

	char* m_pszData;
	unsigned char  m_ucType;
	unsigned short m_usSize;
};


// table spec (has field names)
class CDBtable
{
public:
	CDBtable();
	virtual ~CDBtable();

	char* m_pszName;
	CDBfield* m_pdbField;
	unsigned short m_usNumFields;
};



// connections
class CDBconn
{
public:
	CDBconn();
	virtual ~CDBconn();

	void* m_pdbutil;
	char* m_pszDSNname;
	char* m_pszLogin;
	char* m_pszPw;
	CDBtable* m_pdbTable;
	unsigned short m_usNumTables;
	CDatabase m_db;
	bool m_bConnected;
};


// criterion (has values to compare against)
class CDBCriterion
{
public:
	CDBCriterion(CDBconn* pdbConn, unsigned short usTableIndex=DB_PRIMARY);  // need record spec from table spec.
	virtual ~CDBCriterion();

	CDBconn*	m_pdbConn; // pointer to the connection
	unsigned short m_usTableIndex;
	CDBfield* m_pdbField; // m_pszData member contains data as criteria.
	// for select criteria, field name contains data value, type contains operation type
	// if order by criteria, type contains ascending/descending, size contains sort priority (zero based ascending)
};

// actual data record
class CDBRecord
{
public:
	CDBRecord();
	virtual ~CDBRecord();
	void Free();
	int EncodeToString(char** ppchString, unsigned long* pulDataLen, char chDelim=0);
	int DecodeFromString(char* pchString, unsigned long ulDataLen, char chDelim=0);

	char** m_ppszData; // pointer to an array of char pointers to strings holding the data.  Last pointer is NULL (null term array)
};

class CDBUtil : public CMessagingObject
{
public:

	CDBconn** m_ppdbConn;
	unsigned char m_ucNumConnections;

public:
	CDBUtil();
	virtual ~CDBUtil();

	// set and get internal information.
	CDBconn* CreateNewConnection(char* pszDSN, char* pszLogin, char* pszPW);
	CDBconn* ConnectionExists(char* pszDSN);
	int RemoveConnection(CDBconn* pdbConn);
	int AddTable(CDBconn* pdbConn, char* pszTable);
	int SetTableInfo(CDBconn* pdbConn, unsigned short usTableIndex, char* pszTable, CDBfield* ppdbFields, unsigned short usNumFields);
	int GetTableIndex(CDBconn* pdbConn, char* pszTable);// returns table index, -1 if not exists
	int RemoveTable(CDBconn* pdbConn, char* pszTable);

	// all pszInfo args must point to a buffer of DB_ERRORSTRING_LEN bytes or more, or be NULL.
	// manipulations based on info - actual comm over odbc
  int ConnectDatabase(CDBconn* pdbConn, char* pszInfo=NULL);
  int DisconnectDatabase(CDBconn* pdbConn, char* pszInfo=NULL);

	int GetTableInfo(CDBconn* pdbConn, char* pszTable, unsigned char ucFlags=DB_UPDATEINFO, char* pszInfo=NULL);  // get the table data from the existing table over the connection.
	int GetTableInfo(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo=NULL);  // get the table data from the existing table over the connection.
	int CreateTable(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo=NULL);
	int DropTable(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo=NULL);
	int TableExists(CDBconn* pdbConn, char* pszTable, char* pszInfo=NULL);  // means, exists in real db
	int AddColumn(CDBconn* pdbConn, unsigned short usTableIndex, CDBfield dbField, char* pszInfo=NULL);  // means, add in real db table
	int DropColumn(CDBconn* pdbConn, unsigned short usTableIndex, char* pszColumn, char* pszInfo=NULL);  // means, drop in real db table

// must Close() and use delete on returned recordset when finished with it, unless using Convert()
	CRecordset* Retrieve(CDBconn* pdbConn, char* pszSQL, char* pszInfo=NULL);
	CRecordset* RetrieveByFields(CDBCriterion* pdbcCriteria, CDBCriterion* pdbcCriteria2=NULL, CDBCriterion* pdbcOrderCriteria=NULL, char* pszInfo=NULL);

// Convert() closes and deletes the passed in recordset.  
// must explicitly call ppdbrData[i].Free() and then delete [] the array of data records that is passed out in ppdbrData.
	int Convert(CRecordset* prsIn, CDBCriterion* pdbcCriterion, CDBRecord** ppdbrData, unsigned long* pulNumRecords, char* pszInfo=NULL);
	char* GetValue(CDBCriterion* pdbcCriterion, CDBRecord* pdbrData, char* pszFieldName, char* pszInfo=NULL);

	int Insert(CDBCriterion* pdbcCriteria, char* pszInfo=NULL);  // insert a specific record
	int Delete(CDBCriterion* pdbcCriteria, char* pszInfo=NULL);  // delete a specific record
	int DeleteAll(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo=NULL);  // delete all records in table
	int DeleteByFields(CDBCriterion* pdbcCriteria, CDBCriterion* pdbcCriteria2=NULL, char* pszInfo=NULL);

	int Update(CDBCriterion* pdbcValues, CDBCriterion* pdbcCriteria, char* pszInfo=NULL);  // update a specific record
	int Update(CDBCriterion* pdbcValues, CDBCriterion* pdbcCriteria, CDBCriterion* pdbcCriteria2=NULL, char* pszInfo=NULL);

  int ExecuteSQL(CDBconn* pdbConn, char* pszSQL, char* pszInfo=NULL);
  char* EncodeQuotes(char* pszString, char* pszInfo=NULL);
  char* EncodeQuotes(CString szString, char* pszInfo=NULL);
  char* EncodeQuotes(int nType, char* pszString, char* pszInfo=NULL);
  char* EncodeQuotes(int nType, CString szString, char* pszInfo=NULL);
  char* LimitEncodeQuotes(char* pszString, unsigned long ulFieldLength, char* pszInfo=NULL);
  char* LimitEncodeQuotes(CString szString, unsigned long ulFieldLength, char* pszInfo=NULL);
  char* SQLTypeSize(unsigned char ucType, unsigned short usSize);

	int CheckConnectionError(CString szString);

// override
//	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);
	
// utility
};


#endif // !defined(AFX_DBUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
