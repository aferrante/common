// DBUtil.cpp: implementation of the CDBUtil class.
// This class is dependent on MFC
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h" 
#include "DBUtil.h"
#include "../../TXT/BufferUtil.h"
#include <time.h>
#include <direct.h>
#include <process.h>
#include <ctype.h>
#include <winbase.h> // needed to halt thread execution with Sleep.


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// support classes
//////////////////////////////////////////////////////////////////////



// fields
CDBfield::CDBfield()
{
	m_pszData = NULL;
	m_ucType = DB_TYPE_NDEF;
	m_usSize = 0;
}

CDBfield::~CDBfield()
{
	if(m_pszData) free(m_pszData); // must use malloc
}



// tables
CDBtable::CDBtable()
{
	m_pszName = NULL;
	m_pdbField = NULL;
	m_usNumFields = 0;
}

CDBtable::~CDBtable()
{
	if(m_pszName) free(m_pszName); // must use malloc
	if(m_pdbField) delete [] m_pdbField; // must use new
}


// criterion
CDBCriterion::CDBCriterion(CDBconn* pdbConn, unsigned short usTableIndex)
{
	if(pdbConn)
	{
		m_pdbConn = pdbConn; // pointer to the connection
		m_usTableIndex = usTableIndex;
		if(usTableIndex<pdbConn->m_usNumTables)
		{
//			m_usNumFields = pdbConn->m_pdbTable[usTableIndex].m_usNumFields;  // ensures right number of fields.
			m_pdbField = new CDBfield[pdbConn->m_pdbTable[usTableIndex].m_usNumFields];// creates the field object - the returned data will be malloced at assignment time.

		}
		else
		{
			pdbConn = NULL;
			m_usTableIndex = 0;
//			m_usNumFields = 0;
			m_pdbField = NULL;
		}
	}
	else
	{
		pdbConn = NULL;
		m_usTableIndex = 0;
//		m_usNumFields = 0;
		m_pdbField = NULL;
	}
}

CDBCriterion::~CDBCriterion()
{
	if(m_pdbField) delete [] m_pdbField; // must use new
}

// connections
CDBconn::CDBconn()
{
	m_pdbutil = NULL;
	m_pszDSNname = NULL;
	m_pszLogin = NULL;
	m_pszPw = NULL;
	m_pdbTable = NULL;
	m_usNumTables = 0;
	m_bConnected = false;
}

CDBconn::~CDBconn()
{
  if (m_bConnected)
  {
    try  
    {
      m_db.Close();
    } 
		catch(CException* e)// CDBException *e, CMemoryException *m)  
		{
			if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
			{
				if(m_pdbutil)
				{
					char errorstring[DB_SQLSTRING_MAXLEN];
					_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "CloseDB: Caught exception %d: %s%s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin);
					((CDBUtil*)m_pdbutil)->Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:CloseDB");
				}
			}
			else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
			{
				if(m_pdbutil)
				{
					char errorstring[DB_ERRORSTRING_LEN];
					char msgstring[DB_SQLSTRING_MAXLEN];
					e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
					// The error code is in e->m_nRetCode
					_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught memory exception: %s", errorstring);
					((CDBUtil*)m_pdbutil)->Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:CloseDB");
				}
			}
			else 
			{
				if(m_pdbutil)
				{
					char errorstring[DB_ERRORSTRING_LEN];
					char msgstring[DB_SQLSTRING_MAXLEN];
					e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
					_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught other exception: %s", errorstring);
					((CDBUtil*)m_pdbutil)->Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:CloseDB");
				}
			}
			e->Delete();
		} 
		catch(...)
		{
			if(m_pdbutil)
			{
				((CDBUtil*)m_pdbutil)->Message(MSG_PRI_HIGH|MSG_ICONERROR, "ExecuteSQL: Caught other exception", "DBUtil:CloseDB");
			}
		}
  }

	if(m_pszDSNname) free(m_pszDSNname); // must use malloc
	if(m_pszLogin) free(m_pszLogin); // must use malloc
	if(m_pszPw) free(m_pszPw); // must use malloc
	if(m_pdbTable) delete [] m_pdbTable; // must use new
	m_pdbutil = NULL;
}


//data record
CDBRecord::CDBRecord()
{
	m_ppszData = NULL;
}

CDBRecord::~CDBRecord()
{
}

void CDBRecord::Free()
{
	if(m_ppszData)
	{
		unsigned short i=0;
		while(m_ppszData[i]!=NULL)
		{
			free(m_ppszData[i]);  // use malloc
			m_ppszData[i]=NULL;
			i++;
		}
		delete [] m_ppszData;  // must use new
		m_ppszData = NULL;
	}
}

int CDBRecord::EncodeToString(char** ppchString, unsigned long* pulDataLen, char chDelim)
{
	if((m_ppszData)&&(ppchString))
	{
		unsigned long ulDataLen=0;
		unsigned short i=0;
		while(m_ppszData[i]!=NULL)
		{
			ulDataLen = (strlen(m_ppszData[i])+1);
			i++;
		}
		char* pch = (char*)malloc(ulDataLen);
		if(pch)
		{
			if(pulDataLen) *pulDataLen = ulDataLen-1; // dont include term zero.
			i=0;
			ulDataLen = 0;
			while(m_ppszData[i]!=NULL)
			{
				memcpy(pch+ulDataLen, m_ppszData[i], strlen(m_ppszData[i]));
				ulDataLen += strlen(m_ppszData[i]);
				memset(pch+ulDataLen, 1, chDelim);
				ulDataLen++;
				i++;
			}
			 // overwrite last delim with 0.
			if(ulDataLen>0) ulDataLen--; 
			memset(pch+ulDataLen, 1, 0); 
			if(pulDataLen) *pulDataLen = ulDataLen; // dont include term zero.
			*(ppchString) = pch;
			return DB_SUCCESS;
		}
		return DB_ERROR;
	}
	return DB_ERROR_BADARG;
}

int CDBRecord::DecodeFromString(char* pchString, unsigned long ulDataLen, char chDelim)
{
	if(pchString)
	{
		CSafeBufferUtil sbu;
		CBufferUtil bu;
		unsigned long nDelims = bu.CountChar(pchString, ulDataLen, chDelim);
		char pszDelim[2];
		sprintf(pszDelim, "%c", chDelim);
		char* pch = sbu.Token(pchString, ulDataLen, pszDelim, MODE_SINGLEDELIM);
		Free();  // clear out any records that may exist.

		m_ppszData = new char*[nDelims+2];
		if(m_ppszData==NULL) return DB_ERROR_MEMEX;
		nDelims = 0; // now counting fields
		while(pch!=NULL)
		{
			char* pchField = (char*)malloc(strlen(pch)+1);
			if(pchField)
			{
				strcpy(pchField, pch);
				m_ppszData[nDelims]=pchField;
			}
			else 
			{
				m_ppszData[nDelims]= NULL;
				return DB_ERROR_MEMEX;
			}
			nDelims++;

			pch = sbu.Token(NULL, NULL, pszDelim, MODE_SINGLEDELIM);
		}
		m_ppszData[nDelims]= NULL;  // final NULL ptr
		return DB_SUCCESS;
	}
	return DB_ERROR_BADARG;
}


// end support defs
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDBUtil::CDBUtil()
{
	m_ucType = MSG_DESTTYPE_DB;
	m_ppdbConn = NULL;
	m_ucNumConnections=0;
}

CDBUtil::~CDBUtil()
{
	if(m_ppdbConn)
	{
		unsigned char i=0; 
		while(i<m_ucNumConnections)
		{
			if(m_ppdbConn[i]) delete m_ppdbConn[i];
			i++;
		}
		delete [] m_ppdbConn; // must use new
	}
}

/*
int CDBUtil::HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	return 0;
}
*/

int CDBUtil::RemoveConnection(CDBconn* pdbConn)
{
	if(pdbConn)
	{
		if((m_ppdbConn)&&(m_ucNumConnections>0))
		{
			unsigned char i=0;
			while(i<m_ucNumConnections)
			{
				if((m_ppdbConn[i])&&(m_ppdbConn[i] == pdbConn))
				{
					DisconnectDatabase(pdbConn);
					delete m_ppdbConn[i];
					m_ucNumConnections--;
					while(i<m_ucNumConnections)
					{
						m_ppdbConn[i] = m_ppdbConn[i+1];
						i++;
					}
					m_ppdbConn[i] = NULL;
					if(m_ucNumConnections == 0)
					{
						delete [] m_ppdbConn;
						m_ppdbConn = NULL;
					}
					return DB_SUCCESS;
				}
				i++;
			}
			return DB_ERROR_NOTEXISTS;
		}
	}
	return DB_ERROR_BADARG;
}

// set and get internal information.
CDBconn* CDBUtil::CreateNewConnection(char* pszDSN, char* pszLogin, char* pszPW)
{
//Message(MSG_ICONHAND, "top of CreateNewConnection","db***debug");
	if(pszDSN==NULL) return NULL;  // cant connect to nothing
	if(pszLogin==NULL) return NULL;  // need a login
	if(pszPW==NULL) return NULL;  //need a pw, even if blank
	CDBconn* pdbConn = ConnectionExists(pszDSN);  //if exists just return pointer to existing.
//	AfxMessageBox("Xx");
//Message(MSG_ICONHAND, "after ConnectionExists","db***debug");
	if(pdbConn==NULL)
	{
		CDBconn** ppdbConn = new CDBconn*[m_ucNumConnections+1];
		if(ppdbConn)
		{
			if((m_ppdbConn)&&(m_ucNumConnections>0))
			{
				unsigned char i=0;
				while(i<m_ucNumConnections)
				{
					ppdbConn[i] = m_ppdbConn[i];
					i++;
				}
//				memcpy(pdbConn, m_pdbConn, m_ucNumConnections*sizeof(CDBconn));
				delete [] m_ppdbConn;
			}
//Message(MSG_ICONHAND, "CreateNewConnection","db***debug");

			m_ppdbConn = ppdbConn;
			pdbConn = new CDBconn;
			if(pdbConn)
			{
				m_ppdbConn[m_ucNumConnections] = pdbConn;
				m_ppdbConn[m_ucNumConnections]->m_pszDSNname = (char*)malloc(strlen(pszDSN)+1);
				if(m_ppdbConn[m_ucNumConnections]->m_pszDSNname)
					strcpy(m_ppdbConn[m_ucNumConnections]->m_pszDSNname, pszDSN);
				m_ppdbConn[m_ucNumConnections]->m_pszLogin = (char*)malloc(strlen(pszLogin)+1);
				if(m_ppdbConn[m_ucNumConnections]->m_pszLogin)
					strcpy(m_ppdbConn[m_ucNumConnections]->m_pszLogin, pszLogin);
				m_ppdbConn[m_ucNumConnections]->m_pszPw = (char*)malloc(strlen(pszPW)+1);
				if(m_ppdbConn[m_ucNumConnections]->m_pszPw)
					strcpy(m_ppdbConn[m_ucNumConnections]->m_pszPw, pszPW);
				
				m_ppdbConn[m_ucNumConnections]->m_pdbutil = this;
				
				m_ucNumConnections++;
			}
		}
	}
//Message(MSG_ICONHAND, "CreateNewConnection returning","db***debug");
	return pdbConn;
}

CDBconn* CDBUtil::ConnectionExists(char* pszDSN)
{
	if(pszDSN==NULL) return NULL;  // cant connect to nothing
	if(strlen(pszDSN)<=0) return NULL;  // cant connect to nothing
	if((m_ppdbConn==NULL)||(m_ucNumConnections<=0)) return NULL;  //nothing to find!
	CDBconn* pdbConn = NULL;
	bool bFound=false;
	unsigned char i=0;

	while((i<m_ucNumConnections)&&(!bFound))
	{
		if((m_ppdbConn)&&(m_ppdbConn[i])&&(m_ppdbConn[i]->m_pszDSNname)&&(strlen(m_ppdbConn[i]->m_pszDSNname)))
		{
			if(strcmp(m_ppdbConn[i]->m_pszDSNname, pszDSN)==0)
			{
				bFound = true;
				pdbConn = m_ppdbConn[i];
			}
		}
		i++;
	}
	return pdbConn;
}

int CDBUtil::AddTable(CDBconn* pdbConn, char* pszTable)
{
	int nReturnCode = DB_ERROR_BADARG;
	if((pdbConn)&&(pszTable)&&(strlen(pszTable)>0))
	{
		int nIndex = GetTableIndex(pdbConn, pszTable);
		if(nIndex>=0)
		{
			nReturnCode = DB_EXISTS;
		}
		else
		{
			// add this table to the connection info
			CDBtable* pdbTable = new CDBtable[pdbConn->m_usNumTables+1];
			if(pdbTable)
			{
				if((pdbConn->m_pdbTable!=NULL)&&(pdbConn->m_usNumTables>0))
				{
					memcpy(pdbTable, pdbConn->m_pdbTable, sizeof(CDBtable)*pdbConn->m_usNumTables);
					free(pdbConn->m_pdbTable);
				}
				pdbConn->m_pdbTable = pdbTable;
				pdbConn->m_pdbTable[pdbConn->m_usNumTables].m_pszName = (char*)malloc(strlen(pszTable)+1); //term zero
				if(pdbConn->m_pdbTable[pdbConn->m_usNumTables].m_pszName)
					strcpy(pdbConn->m_pdbTable[pdbConn->m_usNumTables].m_pszName, pszTable);

				pdbConn->m_usNumTables++;
				nReturnCode = DB_SUCCESS;

			}
			else nReturnCode = DB_ERROR_MEMEX;
		}
	}
	return nReturnCode;
}

int CDBUtil::SetTableInfo(CDBconn* pdbConn, unsigned short usTableIndex, char* pszTable, CDBfield* pdbFields, unsigned short usNumFields)
{
	int nReturnCode = DB_ERROR_BADARG;
	if((pdbConn)&&(pszTable)&&(pdbFields)&&(usTableIndex<pdbConn->m_usNumTables))
	{
		if((strcmp(pdbConn->m_pdbTable[usTableIndex].m_pszName, pszTable)!=0)&&(strlen(pszTable)))
		{
			// replace table name.
			free(pdbConn->m_pdbTable[usTableIndex].m_pszName);
			pdbConn->m_pdbTable[usTableIndex].m_pszName = (char*)malloc(strlen(pszTable)+1); //term zero
			if(pdbConn->m_pdbTable[usTableIndex].m_pszName)
				strcpy(pdbConn->m_pdbTable[usTableIndex].m_pszName, pszTable);

		}
		if(pdbConn->m_pdbTable[usTableIndex].m_pdbField) delete [] pdbConn->m_pdbTable[usTableIndex].m_pdbField;
		pdbConn->m_pdbTable[usTableIndex].m_pdbField = pdbFields;
		pdbConn->m_pdbTable[usTableIndex].m_usNumFields = usNumFields;
		nReturnCode = DB_SUCCESS;

	}
	return nReturnCode;
}

int CDBUtil::GetTableIndex(CDBconn* pdbConn, char* pszTable) // returns table index, -1 if not exists
{
	int nTableIndex = -1;
	if((pdbConn)&&(pszTable)&&(strlen(pszTable)>0))
	{
		nTableIndex=0;
		bool bFound = false;
		while((nTableIndex<pdbConn->m_usNumTables)&&(!bFound))
		{
			if(strcmp(pdbConn->m_pdbTable[nTableIndex].m_pszName, pszTable)==0)
			{
				bFound = true;
//				AfxMessageBox(pdbConn->m_pdbTable[nTableIndex].m_pszName);
//				AfxMessageBox(pszTable);
			}
			else
			{
				nTableIndex++;
			}
		}
		if(!bFound) nTableIndex=-1;
	}
	return nTableIndex;
}

int CDBUtil::RemoveTable(CDBconn* pdbConn, char* pszTable)
{
	int nReturnCode = DB_ERROR_BADARG;
	if((pdbConn)&&(pszTable)&&(strlen(pszTable)>0))
	{
		int nIndex = GetTableIndex(pdbConn, pszTable);
		if(nIndex<0) nReturnCode = DB_ERROR_NOTEXISTS;
		else
		{
			// remove this table from the connection info
			if((pdbConn->m_pdbTable!=NULL)&&(nIndex<pdbConn->m_usNumTables))
			{
				CDBtable* pdbTable = new CDBtable[pdbConn->m_usNumTables-1];
				if(pdbTable)
				{
					pdbConn->m_usNumTables--;
					if(nIndex>0)
					{
						memcpy(pdbTable, pdbConn->m_pdbTable, sizeof(CDBtable)*nIndex);
					}
					if(nIndex<pdbConn->m_usNumTables)
					{
						memcpy(pdbTable, pdbConn->m_pdbTable+(sizeof(CDBtable)*(nIndex+1)), sizeof(CDBtable)*(pdbConn->m_usNumTables-nIndex) );
					}
					delete [] pdbConn->m_pdbTable;
					pdbConn->m_pdbTable = pdbTable;
					nReturnCode = DB_SUCCESS;

				}
				else nReturnCode = DB_ERROR_MEMEX;
			}
		}
	}
	return nReturnCode;
}

// manipulations based on info - actual comm over odbc
int CDBUtil::ConnectDatabase(CDBconn* pdbConn, char* pszInfo)
{
//Message(MSG_ICONHAND, "top of ConnectDatabase","db***debug");
	int nReturnCode = DB_ERROR;

	if(	pdbConn->m_db.IsOpen()) return DB_EXISTS; // already open!
		
	if(pdbConn)
	{
		char dbstring[DB_SQLSTRING_MAXLEN];
		BOOL bConnected = FALSE;

		_snprintf(dbstring, DB_SQLSTRING_MAXLEN-1, "DSN=%s;UID=%s;PWD=%s", pdbConn->m_pszDSNname, pdbConn->m_pszLogin, pdbConn->m_pszPw);

//Message(MSG_ICONHAND, dbstring,"db***debug");
		try  
		{
			nReturnCode = DB_SUCCESS;
//Message(MSG_ICONHAND, "opening","db***debug");
bConnected = pdbConn->m_db.OpenEx(dbstring, CDatabase::noOdbcDialog);
//			bConnected = pdbConn->m_db.Open(pdbConn->m_pszDSNname, FALSE, FALSE, dbstring, FALSE);
//Message(MSG_ICONHAND, "after openex","db***debug");
			if (!bConnected)
			{
//Message(MSG_ICONHAND, "not connected","db***debug");
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "ConnectDatabase: failed to connect with %s.", dbstring);
				if(m_pmsgr)
				{
					char errorstring[DB_ERRORSTRING_LEN];
					_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "ConnectDatabase: failed to connect with %s.", dbstring);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:ConnectDatabase");
				}
				nReturnCode = DB_ERROR_NOTOPEN;
			}
		} 
		catch(CException* e)// CDBException *e, CMemoryException *m)  
		{
			bConnected = FALSE;
			if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "ConnectDatabase: Caught exception %d: %s%sConnect string: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, dbstring);
				if(m_pmsgr)
				{
					char errorstring[DB_SQLSTRING_MAXLEN];
					_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "ConnectDatabase: Caught exception %d: %s%sConnect string: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, dbstring);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:ConnectDatabase");
				}
				nReturnCode = DB_ERROR_DBEX;
				if(CheckConnectionError(((CDBException *) e)->m_strStateNativeOrigin)==DB_ERROR_NOTOPEN)
				{
					pdbConn->m_db.Close();
					pdbConn->m_bConnected = false;
				}
			}
			else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "ConnectDatabase: Caught memory exception: %s\nConnect string: %s", errorstring, dbstring);
				// The error code is in e->m_nRetCode
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ConnectDatabase: Caught memory exception: %s\nConnect string: %s", errorstring, dbstring);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:ConnectDatabase");
				nReturnCode = DB_ERROR_MEMEX;
			}
			else 
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "ConnectDatabase: Caught other exception: %s\nConnect string: %s", errorstring, dbstring);
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ConnectDatabase: Caught other exception: %s\nConnect string: %s", errorstring, dbstring);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:ConnectDatabase");
				nReturnCode = DB_ERROR_EX;
			}
			e->Delete();
		} 
		catch(...)
		{
			bConnected = FALSE;
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "ConnectDatabase: Caught other exception\nConnect string: %s", dbstring);
			char msgstring[DB_SQLSTRING_MAXLEN];
			_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ConnectDatabase: Caught other exception\nConnect string: %s", dbstring);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:ConnectDatabase");
			nReturnCode = DB_ERROR_EX;
		}
		

		if(bConnected) pdbConn->m_bConnected=true;
		else pdbConn->m_bConnected=false;
	}
//Message(MSG_ICONHAND, "returning from ConnectDatabase","db***debug");
  return nReturnCode;

}

int CDBUtil::DisconnectDatabase(CDBconn* pdbConn, char* pszInfo)
{
	int nReturnCode = DB_ERROR;
	if(pdbConn)
	{
		if (pdbConn->m_bConnected)
		{
			try  
			{
				pdbConn->m_db.Close();
				nReturnCode = DB_SUCCESS;
			} 
			catch(CException* e)// CDBException *e, CMemoryException *m)  
			{
				if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "DisconnectDatabase: Caught exception %d: %s%s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin);
					if(m_pmsgr)
					{
						char errorstring[DB_SQLSTRING_MAXLEN];
						_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "DisconnectDatabase: Caught exception %d: %s%s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin);
						Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:DisconnectDatabase");
					}
					nReturnCode = DB_ERROR_DBEX;
				}
				else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
				{
					char errorstring[DB_ERRORSTRING_LEN];
					char msgstring[DB_SQLSTRING_MAXLEN];
					e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
					if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "DisconnectDatabase: Caught memory exception: %s", errorstring);
					// The error code is in e->m_nRetCode
					_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "DisconnectDatabase: Caught memory exception: %s", errorstring);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:DisconnectDatabase");
					nReturnCode = DB_ERROR_MEMEX;
				}
				else 
				{
					char errorstring[DB_ERRORSTRING_LEN];
					char msgstring[DB_SQLSTRING_MAXLEN];
					e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
					if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "DisconnectDatabase: Caught other exception: %s", errorstring);
					_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "DisconnectDatabase: Caught other exception: %s", errorstring);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:DisconnectDatabase");
					nReturnCode = DB_ERROR_EX;
				}
				e->Delete();
			} 
			catch(...)
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "DisconnectDatabase: Caught other exception");
				Message(MSG_PRI_HIGH|MSG_ICONERROR, "DisconnectDatabase: Caught other exception", "DBUtil:DisconnectDatabase");
				nReturnCode = DB_ERROR_EX;
			}
		}
	  pdbConn->m_bConnected=false;  // set it to false anyway.
  }
  return nReturnCode;
}

int CDBUtil::GetTableInfo(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo)  // get the table data from the existing table over the connection.
{
	int nReturnCode = DB_ERROR_BADARG;
	if( (pdbConn)&&(usTableIndex<pdbConn->m_usNumTables)&&(pdbConn->m_pdbTable)
		&&(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)>0) )
	{

		char* pszTable = pdbConn->m_pdbTable[usTableIndex].m_pszName;


		if(TableExists(pdbConn, pszTable, pszInfo)!=DB_EXISTS)
		return DB_ERROR_NOTEXISTS;

		BOOL bSuccess = FALSE;
		CRecordset rs(&(pdbConn->m_db));

		char szSQL[DB_SQLSTRING_MAXLEN];

	//			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM INFORMATION_SCHEMA.Columns WHERE table_name = '%s'", pszTable);
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s", pszTable);

		try
		{
			nReturnCode = DB_SUCCESS;
			bSuccess = rs.Open(CRecordset::forwardOnly, szSQL, CRecordset::readOnly);
		}
		catch(CException* e)// CDBException *e, CMemoryException *m)  
		{
			if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "GetTableInfo: Caught exception %d: %s%sSQL: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, szSQL);
				if(m_pmsgr)
				{
					char errorstring[DB_SQLSTRING_MAXLEN];
					_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "GetTableInfo: Caught exception %d: %s%sSQL: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, szSQL);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:GetTableInfo");
				}
				nReturnCode = DB_ERROR_DBEX;
				if(CheckConnectionError(((CDBException *) e)->m_strStateNativeOrigin)==DB_ERROR_NOTOPEN)
				{
					pdbConn->m_db.Close();
					pdbConn->m_bConnected = false;
				}
			}
			else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "GetTableInfo: Caught memory exception: %s\nSQL: %s", errorstring, szSQL);
				// The error code is in e->m_nRetCode
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "GetTableInfo: Caught memory exception: %s\nSQL: %s", errorstring, szSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:GetTableInfo");
				nReturnCode = DB_ERROR_MEMEX;
			}
			else 
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "GetTableInfo: Caught other exception: %s\nSQL: %s", errorstring, szSQL);
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "GetTableInfo: Caught other exception: %s\nSQL: %s", errorstring, szSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:GetTableInfo");
				nReturnCode = DB_ERROR_EX;
			}
			e->Delete();
			return nReturnCode; 
		} 
		catch(...)
		{
			char msgstring[DB_SQLSTRING_MAXLEN];
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "GetTableInfo: Caught other exception\nSQL: %s", szSQL);
			_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "GetTableInfo: Caught other exception\nSQL: %s", szSQL);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:GetTableInfo");
			nReturnCode = DB_ERROR_EX;
			return nReturnCode; 
		}
		
		unsigned short usCount = rs.GetODBCFieldCount();

		if(pdbConn->m_pdbTable[usTableIndex].m_usNumFields!=usCount)
		{
			delete [] pdbConn->m_pdbTable[usTableIndex].m_pdbField;
			pdbConn->m_pdbTable[usTableIndex].m_usNumFields = usCount;

			pdbConn->m_pdbTable[usTableIndex].m_pdbField = new CDBfield[usCount];
		}

		if(pdbConn->m_pdbTable[usTableIndex].m_pdbField)
		{
			for(unsigned short usIndex=0; usIndex<usCount; usIndex++)
			{
				CODBCFieldInfo  fi;
				bSuccess = TRUE;
				try
				{
					rs.GetODBCFieldInfo( usIndex, fi );
				}
				catch(CException* f)// CDBException *f, CMemoryException *m)  
				{
					bSuccess = FALSE;
					if (f->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "GetTableInfo:GetODBCFieldInfo: Caught exception %d: %s%s", ((CDBException *) f)->m_nRetCode, ((CDBException *) f)->m_strError, ((CDBException *) f)->m_strStateNativeOrigin);
						if(m_pmsgr)
						{
							char errorstring[DB_SQLSTRING_MAXLEN];
							_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "GetTableInfo:GetODBCFieldInfo: Caught exception %d: %s%s", ((CDBException *) f)->m_nRetCode, ((CDBException *) f)->m_strError, ((CDBException *) f)->m_strStateNativeOrigin);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:GetTableInfo:GetODBCFieldInfo");
						}
						nReturnCode = DB_ERROR_DBEX;
						if(CheckConnectionError(((CDBException *) f)->m_strStateNativeOrigin)==DB_ERROR_NOTOPEN)
						{
							pdbConn->m_db.Close();
							pdbConn->m_bConnected = false;
						}

					}
					else if (f->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						char errorstring[DB_ERRORSTRING_LEN];
						char msgstring[DB_SQLSTRING_MAXLEN];
						f->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
						if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "GetTableInfo:GetODBCFieldInfo: Caught memory exception: %s", errorstring);
						// The error code is in f->m_nRetCode
						_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "GetTableInfo:GetODBCFieldInfo: Caught memory exception: %s", errorstring);
						Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:GetTableInfo:GetODBCFieldInfo");
						nReturnCode = DB_ERROR_MEMEX;
					}
					else 
					{
						char errorstring[DB_ERRORSTRING_LEN];
						char msgstring[DB_SQLSTRING_MAXLEN];
						f->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
						if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "GetTableInfo:GetODBCFieldInfo: Caught other exception: %s", errorstring);
						_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "GetTableInfo:GetODBCFieldInfo: Caught other exception: %s", errorstring);
						Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:GetTableInfo:GetODBCFieldInfo");
						nReturnCode = DB_ERROR_EX;
					}
					f->Delete();
				} 
				catch(...)
				{
					bSuccess = FALSE;
					char msgstring[DB_SQLSTRING_MAXLEN];
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "GetTableInfo:GetODBCFieldInfo: Caught other exception");
					_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "GetTableInfo:GetODBCFieldInfo: Caught other exception");
					Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:GetTableInfo:GetODBCFieldInfo");
				}

				if(bSuccess)
				{
					pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_pszData = (char*)malloc(fi.m_strName.GetLength()+1);
					if(pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_pszData)
						sprintf(pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_pszData, "%s", fi.m_strName);



					switch(fi.m_nSQLType)
					{
					case SQL_BIGINT:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIGINT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 8;
						} break;
					case SQL_BINARY:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIN;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = (unsigned short) fi.m_nPrecision;
						} break;
					case SQL_BIT:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BYTE;  // make it one byte
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 1;
						} break;
					case SQL_CHAR:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_STR;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = (unsigned short) fi.m_nPrecision;
						} break;
					case SQL_DATE:  // not really supported, make it a bigint
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIGINT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 8;
						} break;
					case SQL_DECIMAL: // not really supported, make it a variable binary
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIN;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 9;//(unsigned short) fi.m_nPrecision;
						} break;
					case SQL_DOUBLE:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_DOUBLE;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 8;
						} break;
					case SQL_FLOAT: // "float" is double, 8 bytes
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_DOUBLE;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 8;
						} break;
					case SQL_INTEGER:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_INT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 4;
						} break;
					case SQL_LONGVARBINARY:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_DATA;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = (unsigned short) fi.m_nPrecision;
						} break;
					case SQL_LONGVARCHAR:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_TEXT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = (unsigned short) fi.m_nPrecision;
						} break;
					case SQL_NUMERIC: // not really supported, make it a variable binary
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIN;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 9;//(unsigned short) fi.m_nPrecision;
						} break;
					case SQL_REAL:  // "real" is float, 4 bytes.
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_FLOAT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 4;
						} break;
					case SQL_SMALLINT:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_SHORT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 2;
						} break;
					case SQL_TIME: // not really supported, make it a bigint
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIGINT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 8;
						} break;
					case SQL_TIMESTAMP: // not really supported, make it a bigint
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BIGINT;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 8;
						} break;
					case SQL_TINYINT:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_BYTE;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = 1;
						} break;
					case SQL_VARBINARY:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_VBIN;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = (unsigned short) fi.m_nPrecision;
						} break;
					case SQL_VARCHAR:
						{ 
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_ucType = DB_TYPE_VSTR;
							pdbConn->m_pdbTable[usTableIndex].m_pdbField[usIndex].m_usSize = (unsigned short) fi.m_nPrecision;
						} break;
					}
				}
			}
		}

		rs.Close();
	}
	return nReturnCode;
}

int CDBUtil::GetTableInfo(CDBconn* pdbConn, char* pszTable, unsigned char ucFlags, char* pszInfo)  // get the table data from the existing table over the connection.
{
	// this sets the table info from the actual table itself.
	int nReturnCode = DB_ERROR_BADARG;
	if((pdbConn)&&(pszTable)&&(strlen(pszTable)>0))
	{
	  if (!pdbConn->m_bConnected) return DB_ERROR_NOTOPEN;

		int nIndex = GetTableIndex(pdbConn, pszTable);

		if(nIndex<0)
		{
			if(ucFlags&DB_UPDATEINFO)
			{
				AddTable(pdbConn, pszTable);
				nIndex = GetTableIndex(pdbConn, pszTable);
			}
		}


		if(nIndex<0)
		{
			nReturnCode = DB_ERROR_NOTEXISTS;
		}
		else
		{
			// overwrite table data.
			// do a query to get the info
			nReturnCode = GetTableInfo(pdbConn, nIndex, pszInfo);
		}

	}
	return nReturnCode;
}


int CDBUtil::CreateTable(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo)
{
	int nReturnCode = DB_SUCCESS;
	if(	(pdbConn==NULL)||(usTableIndex>=pdbConn->m_usNumTables)||(pdbConn->m_pdbTable==NULL)
		||(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)<=0)||(pdbConn->m_pdbTable[usTableIndex].m_usNumFields<=0)
		||(pdbConn->m_pdbTable[usTableIndex].m_pdbField==NULL)) 
	{
		if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "CreateTable: %d %d %d %d %d %d",
			(pdbConn==NULL),(usTableIndex>=pdbConn->m_usNumTables),(pdbConn->m_pdbTable==NULL),
			(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)<=0),(pdbConn->m_pdbTable[usTableIndex].m_usNumFields<=0),
			(pdbConn->m_pdbTable[usTableIndex].m_pdbField==NULL)
		);
		return DB_ERROR_BADARG;
	}
  if (!pdbConn->m_bConnected) return DB_ERROR_NOTOPEN;

  char szSQL[DB_SQLSTRING_MAXLEN];

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "CREATE TABLE %s (",pdbConn->m_pdbTable[usTableIndex].m_pszName);
	
	unsigned short usField = 0;
	char pchField[DB_FIELDNAME_MAXLEN];
	while(usField<pdbConn->m_pdbTable[usTableIndex].m_usNumFields)
	{
		char* pszName = pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_pszData;
		if((pszName!=NULL)&&(strlen(pszName)>0)&&(pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_ucType>DB_TYPE_NDEF))
		{
			// name
			if(strlen(szSQL)+strlen(pszName)+3<DB_SQLSTRING_MAXLEN) // just in case
			{
				_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "\"%s\" ", pszName);
				strcat(szSQL, pchField);
			}
			// then type (always allow NULLs)

			switch(pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_ucType)
			{
	// integer types
			case DB_TYPE_BYTE:		//0x01  // unsigned char   (tiny int)     0 through 255.
				{
					if(strlen(szSQL)+strlen("tinyint NULL")<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, "tinyint NULL");
				} break;
			case DB_TYPE_SHORT:		//0x02  // signed short    (small int)   -2^15 (-32,768) through 2^15 - 1 (32,767).
				{
					if(strlen(szSQL)+strlen("smallint NULL")<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, "smallint NULL");
				} break;
			case DB_TYPE_INT:			//0x03  // signed long 8 bytes.  (int)   -2^31 (-2,147,483,648) through 2^31 - 1 (2,147,483,647).
				{
					if(strlen(szSQL)+strlen("int NULL")<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, "int NULL");
				} break;
			case DB_TYPE_BIGINT:	//0x04  // signed double long 8 bytes.  (big int, datetime,) -2^63 (-9223372036854775808) through 2^63-1 (9223372036854775807).
				{
					if(strlen(szSQL)+strlen("bigint NULL")<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, "bigint NULL");
				} break;

	// string
			case DB_TYPE_STR:			//0x05  // fixed width array of char, max 8000.
				{
					_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "char(%d) NULL", pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_usSize);
					if(strlen(szSQL)+strlen(pchField)<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, pchField);
				} break;
			case DB_TYPE_VSTR:		//0x06  // variable array of char, max 8000.
				{
					_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "varchar(%d) NULL", pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_usSize);
					if(strlen(szSQL)+strlen(pchField)<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, pchField);
				} break;
			case DB_TYPE_TEXT:		//0x07  // variable array of char, max 2^31 - 1 (2,147,483,647) characters.
				{
					// this is untested, and may be incorrect.
					_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "text(%d) NULL", pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_usSize);
					if(strlen(szSQL)+strlen(pchField)<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, pchField);
				} break;

	//floating point
				// the meanings of float, double, and real are confusing.
				// float should be 4 bytes, double 8 bytes.
				// sql has float and real, where float = 8 bytes and real is 4 bytes.
			case DB_TYPE_FLOAT:		//0x08  // float (real), -3.40E + 38 through 3.40E + 38.
				{
					if(strlen(szSQL)+strlen("real NULL")<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, "real NULL");
				} break;
			case DB_TYPE_DOUBLE:	//0x09  // double (float)  -1.79E + 308 through 1.79E + 308.
				{
					if(strlen(szSQL)+strlen("float NULL")<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, "float NULL");
				} break;

	// binary
			case DB_TYPE_BIN:			//0x0a  // fixed width array of bytes, max 8000.
				{
					_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "binary(%d) NULL", pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_usSize);
					if(strlen(szSQL)+strlen(pchField)<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, pchField);
				} break;
			case DB_TYPE_VBIN:		//0x0b  // variable array of bytes, max 8000.
				{
					_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "varbinary(%d) NULL", pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_usSize);
					if(strlen(szSQL)+strlen(pchField)<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, pchField);
				} break;

			case DB_TYPE_DATA:		//0x0c  // variable array of bytes, max 2^31 - 1 (2,147,483,647) bytes.
				{
					// this is untested, and may be incorrect.
					_snprintf(pchField, DB_FIELDNAME_MAXLEN-1, "image(%d) NULL", pdbConn->m_pdbTable[usTableIndex].m_pdbField[usField].m_usSize);
					if(strlen(szSQL)+strlen(pchField)<DB_SQLSTRING_MAXLEN) // just in case
						strcat(szSQL, pchField);
				} break;
			}

			//delim if nec
			if(usField<pdbConn->m_pdbTable[usTableIndex].m_usNumFields-1)
			{
				if(strlen(szSQL)+strlen(", ")<DB_SQLSTRING_MAXLEN) // just in case
					strcat(szSQL, ", ");
			}

		}
		usField++;
	}

	strcat(szSQL, ");");

//	AfxMessageBox(szSQL);
	nReturnCode = ExecuteSQL(pdbConn, szSQL, pszInfo);
  return nReturnCode;
}

int CDBUtil::DropTable(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo)
{
	int nReturnCode = DB_SUCCESS;
	if(	(pdbConn==NULL)||(usTableIndex>=pdbConn->m_usNumTables)||(pdbConn->m_pdbTable==NULL)
		||(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)<=0)) 
		return DB_ERROR_BADARG;

  if (!pdbConn->m_bConnected) return DB_ERROR_NOTOPEN;

  char szSQL[DB_SQLSTRING_MAXLEN];

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DROP TABLE %s", pdbConn->m_pdbTable[usTableIndex].m_pszName);
	
	nReturnCode = ExecuteSQL(pdbConn, szSQL, pszInfo);
  return nReturnCode;
}


int CDBUtil::TableExists(CDBconn* pdbConn, char* pszTable, char* pszInfo) 
{
	int nReturnCode = DB_ERROR_BADARG;
	if((pdbConn)&&(pszTable)&&(strlen(pszTable)>0))
	{
		// check existence of table in actual DB
		nReturnCode = DB_SUCCESS;
		if (!pdbConn->m_bConnected) return DB_ERROR_NOTOPEN;

		BOOL bSuccess = FALSE;
		CRecordset rs(&(pdbConn->m_db));

		char szSQL[DB_SQLSTRING_MAXLEN];

//		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '%s'", pszTable);
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT COUNT(*) AS 'TableCount' FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '%s'", pszTable);

		try
		{
			bSuccess = rs.Open(CRecordset::forwardOnly, szSQL, CRecordset::readOnly);
		}
		catch(CException* e)// CDBException *e, CMemoryException *m)  
		{
			if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "TableExists: Caught exception %d: %s%s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin);
				if(m_pmsgr)
				{
					char errorstring[DB_SQLSTRING_MAXLEN];
					_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "TableExists: Caught exception %d: %s%s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:TableExists");
				}
				nReturnCode = DB_ERROR_DBEX;
				if(CheckConnectionError(((CDBException *) e)->m_strStateNativeOrigin)==DB_ERROR_NOTOPEN)
				{
					pdbConn->m_db.Close();
					pdbConn->m_bConnected = false;
				}

			}
			else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "TableExists: Caught memory exception: %s", errorstring);
				// The error code is in e->m_nRetCode
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "TableExists: Caught memory exception: %s", errorstring);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:TableExists");
				nReturnCode = DB_ERROR_MEMEX;
			}
			else 
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "TableExists: Caught other exception: %s", errorstring);
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "TableExists: Caught other exception: %s", errorstring);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:TableExists");
				nReturnCode = DB_ERROR_EX;
			}
			e->Delete();
			return nReturnCode; 
		} 
		catch(...)
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "TableExists: Caught other exception");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "TableExists: Caught other exception", "DBUtil:TableExists");
			nReturnCode = DB_ERROR_EX;
			return nReturnCode; 
		}
  
/*	
		if(rs.GetRecordCount()>0) 			
			nReturnCode = DB_EXISTS;
		else
		if(rs.GetRecordCount()<0) 			
			nReturnCode = DB_ERROR;
*/
		CString szTemp;
		rs.GetFieldValue("TableCount", szTemp);  
		if(atol(szTemp)>0)
			nReturnCode = DB_EXISTS;

		rs.Close();
	}
	return nReturnCode;

}

int CDBUtil::AddColumn(CDBconn* pdbConn, unsigned short usTableIndex, CDBfield dbField, char* pszInfo)  // means, add in real db table
{
	int nReturnCode = DB_ERROR_BADARG;
	if( (pdbConn)&&(usTableIndex<pdbConn->m_usNumTables)&&(pdbConn->m_pdbTable)
		&&(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)>0) 
		&&(dbField.m_pszData)&&(strlen(dbField.m_pszData)>0) )
	{

		char* pszTable = pdbConn->m_pdbTable[usTableIndex].m_pszName;

		if(TableExists(pdbConn, pszTable, pszInfo)!=DB_EXISTS)
		return DB_ERROR_NOTEXISTS;

		BOOL bSuccess = FALSE;
		CRecordset rs(&(pdbConn->m_db));

		char szSQL[DB_SQLSTRING_MAXLEN];
		char* pszType = SQLTypeSize(dbField.m_ucType, dbField.m_usSize);
		if(pszType==NULL) return DB_ERROR_BADARG; // type doesnt exist or other error

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "ALTER TABLE %s ADD \"%s\" %s NULL",
			pszTable,
			dbField.m_pszData,
			pszType			
			);

//AfxMessageBox(szSQL);
		free(pszType);  // release the memory

		nReturnCode = ExecuteSQL(pdbConn, szSQL, pszInfo);
	}
	return nReturnCode;

}

int CDBUtil::DropColumn(CDBconn* pdbConn, unsigned short usTableIndex, char* pszColumn, char* pszInfo)  // means, drop in real db table
{
	int nReturnCode = DB_ERROR_BADARG;
	if( (pdbConn)&&(usTableIndex<pdbConn->m_usNumTables)&&(pdbConn->m_pdbTable)
		&&(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)>0) 
		&&(pszColumn)&&(strlen(pszColumn)>0) )
	{

		char* pszTable = pdbConn->m_pdbTable[usTableIndex].m_pszName;

		if(TableExists(pdbConn, pszTable, pszInfo)!=DB_EXISTS)
		return DB_ERROR_NOTEXISTS;

		BOOL bSuccess = FALSE;
		CRecordset rs(&(pdbConn->m_db));

		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "ALTER TABLE %s DROP COLUMN \"%s\"",
			pszTable,
			pszColumn
			);

//AfxMessageBox(szSQL);

		nReturnCode = ExecuteSQL(pdbConn, szSQL, pszInfo);
	}
	return nReturnCode;

}

// must use delete on returned recordset
CRecordset* CDBUtil::Retrieve(CDBconn* pdbConn, char* pszSQL, char* pszInfo)
{
	//removed (pdbConn->m_bConnected==false) so we can retry.
	if((pdbConn==NULL)||(pszSQL==NULL)||(strlen(pszSQL)<=0)/*||(pdbConn->m_bConnected==false)*/) return NULL;
	ConnectDatabase(pdbConn, pszInfo);  // re-establishes connection if disconnected, else just returns
	if (!pdbConn->m_bConnected)
	{
		if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Data source %s not open.\nSQL: %s", pdbConn->m_pszDSNname, pszSQL);
		if(m_pmsgr)
		{
			char errorstring[DB_ERRORSTRING_LEN];
			_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Data source %s not open.\nSQL: %s", pdbConn->m_pszDSNname, pszSQL);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
		}
		return NULL;
	}

	CRecordset* prs = new CRecordset(&pdbConn->m_db);
	if(prs)
	{
		BOOL bSuccess = FALSE;

		try
		{
			bSuccess = prs->Open(CRecordset::forwardOnly, pszSQL, CRecordset::readOnly);
		}
		catch(CException* e)// CDBException *e, CMemoryException *m)  
		{
			if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception %d: %s%sSQL: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, pszSQL);
				if(m_pmsgr)
				{
					char errorstring[DB_SQLSTRING_MAXLEN];
					_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "Retrieve: Caught exception %d: %s%sSQL: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, pszSQL);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
				}
				if(CheckConnectionError(((CDBException *) e)->m_strStateNativeOrigin)==DB_ERROR_NOTOPEN)
				{
					pdbConn->m_db.Close();
					pdbConn->m_bConnected = false;
				}
			}
			else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "Retrieve: Caught memory exception: %s\nSQL: %s", errorstring, pszSQL);
				// The error code is in e->m_nRetCode
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught memory exception: %s\nSQL: %s", errorstring, pszSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:Retrieve");
			}
			else 
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "Retrieve: Caught other exception: %s\nSQL: %s", errorstring, pszSQL);
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception: %s\nSQL: %s", errorstring, pszSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:Retrieve");
			}
			e->Delete();
			bSuccess = FALSE;
		} 
		catch(...)
		{
			char msgstring[DB_SQLSTRING_MAXLEN];
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception\nSQL: %s", pszSQL);
			_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception\nSQL: %s", pszSQL);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:Retrieve");
			bSuccess = FALSE;
		}
  
		if(!bSuccess)
		{
			prs->Close();

			delete prs;
			prs = NULL;
		}
	}
	return prs;
}

// must use delete on returned recordset
CRecordset* CDBUtil::RetrieveByFields(CDBCriterion* pdbcCriteria, CDBCriterion* pdbcCriteria2, CDBCriterion* pdbcOrderCriteria, char* pszInfo)
{
	if( (pdbcCriteria==NULL)
		||(pdbcCriteria->m_pdbConn==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable==NULL)
		||(pdbcCriteria->m_usTableIndex>=pdbcCriteria->m_pdbConn->m_usNumTables)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField==NULL)
//		||(pdbcCriteria->m_usNumFields>pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields)
		||(strlen(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName)<=0)
		||(pdbcCriteria->m_pdbField==NULL)
		) return NULL;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char szCriterion[DB_SQLSTRING_MAXLEN];
	char szOperator[12];  // need 10 only, for possible "ORDER BY "+ term 0

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ", pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName);

	int i=0;
	bool bInitial = true;
	unsigned short usNumFields = pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields;
	while(i<usNumFields)
	{
		if( (pdbcCriteria->m_pdbField[i].m_ucType!=DB_OP_IGNORE)
			&&(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA)) // not supporting search on image type
		{
			if(!bInitial) 
			{
				if(pdbcCriteria->m_pdbField[i].m_ucType&DB_OP_OR)
					strcpy(szOperator, "OR ");
				else
					strcpy(szOperator, "AND ");
			}
			else strcpy(szOperator, "WHERE ");

			bInitial = false;

			if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
				)
			{
				char* pszEncoded = EncodeQuotes(pdbcCriteria->m_pdbField[i].m_pszData, pszInfo);
				if(pszEncoded==NULL)
				{
					Message(MSG_ICONERROR, pszInfo, "DBUtil:RetrieveByFields");
					return NULL;
				}

				switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
				{
				case DB_OP_EQUAL://			0x01
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s LIKE '%s' ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pszEncoded);
					} break;
				case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s NOT LIKE '%s' ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pszEncoded);
					} break;
				}
				
				free(pszEncoded);
			}
			else
			if(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA) // not supporting search on image type
			{

				switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
				{
				case DB_OP_EQUAL://			0x01
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_GT://			0x02
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_LT://			0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s < %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_EQUAL|DB_OP_GT://			0x01|0x02
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_EQUAL|DB_OP_LT://			0x01|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s < %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s < %s OR %s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				}
			}

			if((pdbcCriteria2)&&(pdbcCriteria->m_pdbField)&&(pdbcCriteria2->m_pdbField[i].m_ucType!=DB_OP_IGNORE))
			{
				char szCriterion2[DB_SQLSTRING_MAXLEN];
				char szOperator2[8];  // need 5 only
				if(pdbcCriteria2->m_pdbField[i].m_ucType&DB_OP_OR)
					strcpy(szOperator2, "OR ");
				else
					strcpy(szOperator2, "AND ");

				if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
					||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
					)
				{
					char* pszEncoded = EncodeQuotes(pdbcCriteria2->m_pdbField[i].m_pszData, pszInfo);
					if(pszEncoded==NULL)
					{
						Message(MSG_ICONERROR, pszInfo, "DBUtil:RetrieveByFields");
						return NULL;
					}

					switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
					{
					case DB_OP_EQUAL://			0x01
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s LIKE '%s' ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pszEncoded);
						} break;
					case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s NOT LIKE '%s' ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pszEncoded);
						} break;
					}
					
					free(pszEncoded);
				}
				else 
				if(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA) // not supporting search on image type
				{
					switch(pdbcCriteria2->m_pdbField[i].m_ucType&0x0f)
					{
					case DB_OP_EQUAL://			0x01
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_GT://			0x02
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_LT://			0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s < %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_EQUAL|DB_OP_GT://			0x01|0x02
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_EQUAL|DB_OP_LT://			0x01|0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s < %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s < %s OR %s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					}
				}
				if(strlen(szCriterion)+strlen(szOperator2)+strlen(szCriterion2)<DB_SQLSTRING_MAXLEN) // just in case
				{
					strcat(szCriterion, szOperator2);
					strcat(szCriterion, szCriterion2);
				}
			}


			if(strlen(szSQL)+strlen(szOperator)+strlen(szCriterion)<DB_SQLSTRING_MAXLEN) // just in case
			{
				strcat(szSQL, szOperator);
				strcat(szSQL, szCriterion);
			}
		}
		i++;
	}
//AfxMessageBox(szSQL);

	// then order by if nec.
	if((pdbcOrderCriteria)&&(pdbcOrderCriteria->m_pdbField))
	{
		strcpy(szOperator, "ORDER BY ");

		bInitial = true;
		unsigned short usPriority = 0;
		for(i=0; i<usNumFields; i++)
//		for(i=0; i<pdbcCriteria->m_usNumFields; i++)
		{
			int j=0;
			while((pdbcOrderCriteria->m_pdbField[j].m_usSize>usPriority)&&(j<usNumFields))
			{
				j++;
			}
			if(pdbcOrderCriteria->m_pdbField[j].m_usSize==usPriority)
			{
				if(pdbcOrderCriteria->m_pdbField[i].m_ucType!=DB_ORD_IGNORE)
				{
					usPriority++;
					if(!bInitial) strcpy(szOperator, ", ");  // else it will be "ORDER BY "
					bInitial = false;

					switch(pdbcOrderCriteria->m_pdbField[i].m_ucType)
					{
					case DB_ORD_DESC:
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s DESC", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData);
						} break;
					case DB_ORD_ASC:
					default:
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s ASC", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData);
						} break;
					}

					if(strlen(szSQL)+strlen(szOperator)+strlen(szCriterion)<DB_SQLSTRING_MAXLEN) // just in case
					{
						strcat(szSQL, szOperator);
						strcat(szSQL, szCriterion);
					}
				}
			}
		}
	}

//	AfxMessageBox(szSQL);

	return Retrieve(pdbcCriteria->m_pdbConn, szSQL, pszInfo);
}

// Convert closes and deletes the passed in recordset, if there is no error.
// the DB record must have a valid connection pointer, a single db record is passed in,
// a 2D array of char strings with values is passed out in pszData.
int CDBUtil::Convert(CRecordset* prsIn, CDBCriterion* pdbcCriterion, CDBRecord** ppdbrData, unsigned long* pulNumRecords, char* pszInfo)
{
	if( (prsIn==NULL)||(pdbcCriterion==NULL)||(pulNumRecords==NULL)
		||(pdbcCriterion->m_pdbConn==NULL)||(pdbcCriterion->m_pdbConn->m_pdbTable==NULL)
		||(pdbcCriterion->m_usTableIndex>=pdbcCriterion->m_pdbConn->m_usNumTables)
//		||(pdbcCriterion->m_pdbField==NULL)
//		||(ppdbrData==NULL)  can be NULL, we set it.
		) 
	{
		if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Convert: %d %d %d %d %d %d",
			(prsIn==NULL), 
			(pdbcCriterion==NULL),
			(pulNumRecords==NULL),
			(pdbcCriterion->m_pdbConn==NULL),
			(pdbcCriterion->m_pdbConn->m_pdbTable==NULL),
			(pdbcCriterion->m_usTableIndex>=pdbcCriterion->m_pdbConn->m_usNumTables)
		);
		return DB_ERROR_BADARG;
	}

  *pulNumRecords = 0;
	*ppdbrData = NULL;

	CDBRecord* pdbrRecords = NULL;

	int nIndex = 0;
  while ((!prsIn->IsEOF()))//&&(nIndex<nCount))
  {
		CDBRecord* pdbrArray = new CDBRecord[nIndex+1];  // keep expanding the buffer

		if(pdbrArray == NULL)
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Convert: out of memory.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "Convert: out of memory.", "DBUtil:Convert");
			*ppdbrData = NULL; //pointer to the array. of records.
			return DB_ERROR_MEMEX;
		}

		if(pdbrRecords)
		{
			memcpy(pdbrArray, pdbrRecords, sizeof(CDBRecord)*nIndex);
			delete [] pdbrRecords; // doesnt free all the malloced strings.
		}

		pdbrRecords = pdbrArray;

		pdbrRecords[nIndex].m_ppszData = new char*[(pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_usNumFields+1)];
		if(pdbrRecords[nIndex].m_ppszData)
		{
			CString szTemp;
			unsigned short x=0; 
			while(x<pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_usNumFields)
			{
				prsIn->GetFieldValue(pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_pdbField[x].m_pszData, szTemp);

//				AfxMessageBox(szTemp);

				pdbrRecords[nIndex].m_ppszData[x] = (char*)malloc(szTemp.GetLength()+1);
				if(pdbrRecords[nIndex].m_ppszData[x])
				{
					sprintf(pdbrRecords[nIndex].m_ppszData[x], "%s", szTemp);
				}
				else
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Convert: out of memory.");
					Message(MSG_PRI_HIGH|MSG_ICONERROR, "Convert: out of memory.", "DBUtil:Convert");
					*pulNumRecords = nIndex;
					*ppdbrData = pdbrRecords; //pointer to the array. of records.
					return DB_ERROR_MEMEX;
				}
				x++;
			}
			// insert null term to array
			pdbrRecords[nIndex].m_ppszData[x] = NULL;
    }
		else 
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Convert: out of memory.");
			Message(MSG_PRI_HIGH|MSG_ICONERROR, "Convert: out of memory.", "DBUtil:Convert");
		  *pulNumRecords = nIndex;
			*ppdbrData = pdbrRecords; //pointer to the array. of records.
			return DB_ERROR_MEMEX;
		}

    nIndex++;
    prsIn->MoveNext();
  }
  prsIn->Close();
	delete prsIn;   // no longer needed.
	prsIn = NULL; // reset it.

	*ppdbrData = pdbrRecords; //pointer to the array. of records.
  *pulNumRecords = nIndex;

	return DB_SUCCESS;
}

char* CDBUtil::GetValue(CDBCriterion* pdbcCriterion, CDBRecord* pdbrData, char* pszFieldName, char* pszInfo)
{
	if( (pdbcCriterion==NULL)
		||(pdbcCriterion->m_pdbConn==NULL)
		||(pdbcCriterion->m_pdbConn->m_pdbTable==NULL)
		||(pdbcCriterion->m_usTableIndex>=pdbcCriterion->m_pdbConn->m_usNumTables)
		||(pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_pszName==NULL)
		||(pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_pdbField==NULL)
		||(pszFieldName==NULL)
		||(strlen(pszFieldName)<=0)
		||(pdbrData==NULL)
		||(pdbrData->m_ppszData==NULL)
		) return NULL;

	// page thru the fields, return the appropriate pointer.
	char* pchReturn = NULL;

	unsigned short i=0;
	bool bFound = false;
	while((i<pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_usNumFields)&&(!bFound))
	{
		if(strcmp(pdbcCriterion->m_pdbConn->m_pdbTable[pdbcCriterion->m_usTableIndex].m_pdbField[i].m_pszData, pszFieldName)==0)
		{
			bFound = true;
			if(pdbrData->m_ppszData[i]==NULL)
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "GetValue: the field was found, but the pointer to the value was NULL.");
				return NULL;
			}
			else
				pchReturn = pdbrData->m_ppszData[i];
		}
		i++;
	}
	if((!bFound)&&(pszInfo))  
		_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "GetValue: a field with the specified label was not found.");
	return pchReturn;
}

int CDBUtil::Insert(CDBCriterion* pdbcCriteria, char* pszInfo)  // insert a specific record
{
	if( (pdbcCriteria==NULL)
		||(pdbcCriteria->m_pdbConn==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable==NULL)
		||(pdbcCriteria->m_usTableIndex>=pdbcCriteria->m_pdbConn->m_usNumTables)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField==NULL)
		||(strlen(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName)<=0)
		||(pdbcCriteria->m_pdbField==NULL)
		) return DB_ERROR_BADARG;

	int nReturnCode = DB_SUCCESS;
  if (!pdbcCriteria->m_pdbConn->m_bConnected) return DB_ERROR_NOTOPEN;
  
	char szSQL[DB_SQLSTRING_MAXLEN];
	char szCriterion[DB_SQLSTRING_MAXLEN];
	char szOperator[12];  // need 11 only, for possible ") VALUES ("+ term 0
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (", pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName);

 	int i=0;
	bool bInitial = true;
	unsigned short usNumFields = pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields;
	while(i<usNumFields)
	{
		if( (pdbcCriteria->m_pdbField[i].m_ucType!=DB_OP_IGNORE)
			&&(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA)) // not supporting search on image type
		{
			if(!bInitial) 
				strcpy(szOperator, ", ");
			else 
				strcpy(szOperator, "");

			bInitial = false;

			// field name
			_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s", pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData);
			if(strlen(szCriterion)+strlen(szSQL)+strlen(szOperator)<DB_SQLSTRING_MAXLEN) // just in case
			{
				strcat(szSQL, szOperator);
				strcat(szSQL, szCriterion);
			}
		}

		i++;
	}
 
 	i=0;
	bInitial = true;
	strcpy(szOperator, ") VALUES (");

	while(i<usNumFields)
	{
		if( (pdbcCriteria->m_pdbField[i].m_ucType!=DB_OP_IGNORE)
			&&(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA)) // not supporting search on image type
		{
			if(!bInitial) strcpy(szOperator, ", ");  // else it will be ") VALUES ("
			bInitial = false;

			// field value
			if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
				)
			{
				char* pszEncoded = EncodeQuotes(pdbcCriteria->m_pdbField[i].m_pszData, pszInfo);
				if(pszEncoded==NULL)
				{
					Message(MSG_ICONERROR, pszInfo, "DBUtil:Insert");
					return DB_ERROR;
				}
				_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "'%s'", pszEncoded);
				free(pszEncoded);
			}
			else
				_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s", pdbcCriteria->m_pdbField[i].m_pszData);

			if(strlen(szCriterion)+strlen(szSQL)+strlen(szOperator)<DB_SQLSTRING_MAXLEN) // just in case
			{
				strcat(szSQL, szOperator);
				strcat(szSQL, szCriterion);
			}
		}

		i++;
	}

	if(strlen(szSQL)+2<DB_SQLSTRING_MAXLEN) // just in case
	{
		strcat(szSQL, ");");
	}
//	AfxMessageBox(szSQL);
 
  nReturnCode = ExecuteSQL(pdbcCriteria->m_pdbConn, szSQL, pszInfo);
  return nReturnCode;
}

int CDBUtil::Delete(CDBCriterion* pdbcCriteria, char* pszInfo)  // delete a specific record
{
	return DeleteByFields(pdbcCriteria, NULL, pszInfo);
}

int CDBUtil::DeleteAll(CDBconn* pdbConn, unsigned short usTableIndex, char* pszInfo)  // delete all records in table
{
	int nReturnCode = DB_SUCCESS;
	if(	(pdbConn==NULL)||(usTableIndex>=pdbConn->m_usNumTables)||(pdbConn->m_pdbTable==NULL)
		||(strlen(pdbConn->m_pdbTable[usTableIndex].m_pszName)<=0)) 
		return DB_ERROR_BADARG;

  if (!pdbConn->m_bConnected) return DB_ERROR_NOTOPEN;

  char szSQL[DB_SQLSTRING_MAXLEN];

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", pdbConn->m_pdbTable[usTableIndex].m_pszName);
	
	nReturnCode = ExecuteSQL(pdbConn, szSQL, pszInfo);
  return nReturnCode;
}


int CDBUtil::DeleteByFields(CDBCriterion* pdbcCriteria, CDBCriterion* pdbcCriteria2, char* pszInfo)
{
	if( (pdbcCriteria==NULL)
		||(pdbcCriteria->m_pdbConn==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable==NULL)
		||(pdbcCriteria->m_usTableIndex>=pdbcCriteria->m_pdbConn->m_usNumTables)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField==NULL)
//		||(pdbcCriteria->m_usNumFields>pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields)
		||(strlen(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName)<=0)
		||(pdbcCriteria->m_pdbField==NULL)
		) return NULL;
	int nReturnCode = DB_ERROR;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char szCriterion[DB_SQLSTRING_MAXLEN];
	char szOperator[12];  

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s ", pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName);

	int i=0;
	bool bInitial = true;
	unsigned short usNumFields = pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields;
	while(i<usNumFields)
	{
		if( (pdbcCriteria->m_pdbField[i].m_ucType!=DB_OP_IGNORE)
			&&(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA)) // not supporting search on image type
		{
			if(!bInitial) 
			{
				if(pdbcCriteria->m_pdbField[i].m_ucType&DB_OP_OR)
					strcpy(szOperator, "OR ");
				else
					strcpy(szOperator, "AND ");
			}
			else strcpy(szOperator, "WHERE ");

			bInitial = false;


			if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
				)
			{
				char* pszEncoded = EncodeQuotes(pdbcCriteria->m_pdbField[i].m_pszData, pszInfo);
				if(pszEncoded==NULL)
				{
					Message(MSG_ICONERROR, pszInfo, "DBUtil:DeleteByFields");
					return DB_ERROR;
				}

				switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
				{
				case DB_OP_EQUAL://			0x01
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s LIKE '%s' ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pszEncoded);
					} break;
				case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s NOT LIKE '%s' ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pszEncoded);
					} break;
				}
				
				free(pszEncoded);
			}
			else
			if(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA) // not supporting search on image type
			{

				switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
				{
				case DB_OP_EQUAL://			0x01
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_GT://			0x02
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_LT://			0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s < %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_EQUAL|DB_OP_GT://			0x01|0x02
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_EQUAL|DB_OP_LT://			0x01|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s < %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s < %s OR %s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				}
			}

			if((pdbcCriteria2)&&(pdbcCriteria->m_pdbField)&&(pdbcCriteria2->m_pdbField[i].m_ucType!=DB_OP_IGNORE))
			{
				char szCriterion2[DB_SQLSTRING_MAXLEN];
				char szOperator2[8];  // need 5 only
				if(pdbcCriteria2->m_pdbField[i].m_ucType&DB_OP_OR)
					strcpy(szOperator2, "OR ");
				else
					strcpy(szOperator2, "AND ");

				if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
					||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
					)
				{
					char* pszEncoded = EncodeQuotes(pdbcCriteria2->m_pdbField[i].m_pszData, pszInfo);
					if(pszEncoded==NULL)
					{
						Message(MSG_ICONERROR, pszInfo, "DBUtil:DeleteByFields");
						return DB_ERROR;
					}

					switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
					{
					case DB_OP_EQUAL://			0x01
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s LIKE '%s' ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pszEncoded);
						} break;
					case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s NOT LIKE '%s' ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pszEncoded);
						} break;
					}
					
					free(pszEncoded);
				}
				else
				if(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA) // not supporting search on image type
				{
					switch(pdbcCriteria2->m_pdbField[i].m_ucType&0x0f)
					{
					case DB_OP_EQUAL://			0x01
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_GT://			0x02
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_LT://			0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s < %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_EQUAL|DB_OP_GT://			0x01|0x02
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_EQUAL|DB_OP_LT://			0x01|0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s < %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s < %s OR %s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					}
				}
				if(strlen(szCriterion)+strlen(szOperator2)+strlen(szCriterion2)<DB_SQLSTRING_MAXLEN) // just in case
				{
					strcat(szCriterion, szOperator2);
					strcat(szCriterion, szCriterion2);
				}
			}


			if(strlen(szSQL)+strlen(szOperator)+strlen(szCriterion)<DB_SQLSTRING_MAXLEN) // just in case
			{
				strcat(szSQL, szOperator);
				strcat(szSQL, szCriterion);
			}
		}
		i++;
	}

//	AfxMessageBox(szSQL);

  nReturnCode = ExecuteSQL(pdbcCriteria->m_pdbConn, szSQL, pszInfo);
	return nReturnCode;
}


int CDBUtil::Update(CDBCriterion* pdbcValues, CDBCriterion* pdbcCriteria, char* pszInfo)  // update a specific record
{
	return Update(pdbcValues, pdbcCriteria, NULL, pszInfo);
}

int CDBUtil::Update(CDBCriterion* pdbcValues, CDBCriterion* pdbcCriteria, CDBCriterion* pdbcCriteria2, char* pszInfo)
{
	if( (pdbcValues==NULL)
		||(pdbcCriteria==NULL)
		||(pdbcCriteria->m_pdbConn==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable==NULL)
		||(pdbcCriteria->m_usTableIndex>=pdbcCriteria->m_pdbConn->m_usNumTables)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName==NULL)
		||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField==NULL)
//		||(pdbcCriteria->m_usNumFields>pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields)
		||(strlen(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName)<=0)
		||(pdbcCriteria->m_pdbField==NULL)
		) return NULL;
	int nReturnCode = DB_ERROR;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char szCriterion[DB_SQLSTRING_MAXLEN];
	char szOperator[12]; 

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET ", pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pszName);
	int i=0;
	bool bInitial = true;
	unsigned short usNumFields = pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_usNumFields;
	while(i<usNumFields)
	{
		if( (pdbcValues->m_pdbField[i].m_ucType!=DB_OP_IGNORE)
			&&(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA)) // not supporting search on image type
		{
			if(!bInitial) strcpy(szOperator, ", ");
			else strcpy(szOperator, "");

			bInitial = false;

			if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
				)
			{
				char* pszEncoded = EncodeQuotes(pdbcValues->m_pdbField[i].m_pszData, pszInfo);
				if(pszEncoded==NULL)
				{
					Message(MSG_ICONERROR, pszInfo, "DBUtil:Update");
					return DB_ERROR;
				}
				_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = '%s'", 
					pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData,
					pszEncoded);
			
				free(pszEncoded);

			}
			else
			{
				_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s", 
				pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData,
				pdbcValues->m_pdbField[i].m_pszData);
			}
			if(strlen(szSQL)+strlen(szOperator)+strlen(szCriterion)<DB_SQLSTRING_MAXLEN) // just in case
			{
				strcat(szSQL, szOperator);
				strcat(szSQL, szCriterion);
			}

		}
		i++;
	}

	i=0;
	bInitial = true;
	while(i<usNumFields)
	{
		if(pdbcCriteria->m_pdbField[i].m_ucType!=DB_OP_IGNORE)
		{
			if(!bInitial) 
			{
				if(pdbcCriteria->m_pdbField[i].m_ucType&DB_OP_OR)
					strcpy(szOperator, "OR ");
				else
					strcpy(szOperator, "AND ");
			}
			else strcpy(szOperator, " WHERE ");

			bInitial = false;


			if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
				||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
				)
			{
				char* pszEncoded = EncodeQuotes(pdbcCriteria->m_pdbField[i].m_pszData, pszInfo);
				if(pszEncoded==NULL)
				{
					Message(MSG_ICONERROR, pszInfo, "DBUtil:Update");
					return DB_ERROR;
				}

				switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
				{
				case DB_OP_EQUAL://			0x01
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s LIKE '%s' ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pszEncoded);
					} break;
				case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s NOT LIKE '%s' ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pszEncoded);
					} break;
				}
				
				free(pszEncoded);
			}
			else
			if(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA) // not supporting search on image type
			{

				switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
				{
				case DB_OP_EQUAL://			0x01
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_GT://			0x02
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_LT://			0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s < %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_EQUAL|DB_OP_GT://			0x01|0x02
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_EQUAL|DB_OP_LT://			0x01|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s < %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
					{
						_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s < %s OR %s > %s ", 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
							pdbcCriteria->m_pdbField[i].m_pszData);
					} break;
				}
			}

			if((pdbcCriteria2)&&(pdbcCriteria->m_pdbField)&&(pdbcCriteria2->m_pdbField[i].m_ucType!=DB_OP_IGNORE))
			{
				char szCriterion2[DB_SQLSTRING_MAXLEN];
				char szOperator2[8];  // need 5 only
				if(pdbcCriteria2->m_pdbField[i].m_ucType&DB_OP_OR)
					strcpy(szOperator2, "OR ");
				else
					strcpy(szOperator2, "AND ");

				if( (pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_STR) 
					||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType==DB_TYPE_TEXT)
					)
				{
					char* pszEncoded = EncodeQuotes(pdbcCriteria2->m_pdbField[i].m_pszData, pszInfo);
					if(pszEncoded==NULL)
					{
						Message(MSG_ICONERROR, pszInfo, "DBUtil:Update");
						return DB_ERROR;
					}

					switch(pdbcCriteria->m_pdbField[i].m_ucType&0x0f)
					{
					case DB_OP_EQUAL://			0x01
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s LIKE '%s' ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pszEncoded);
						} break;
					case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
						{
							_snprintf(szCriterion, DB_SQLSTRING_MAXLEN-1, "%s NOT LIKE '%s' ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pszEncoded);
						} break;
					}
					
					free(pszEncoded);
				}
				else
				if(pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_ucType!=DB_TYPE_DATA) // not supporting search on image type
				{
					switch(pdbcCriteria2->m_pdbField[i].m_ucType&0x0f)
					{
					case DB_OP_EQUAL://			0x01
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_GT://			0x02
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_LT://			0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s < %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_EQUAL|DB_OP_GT://			0x01|0x02
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_EQUAL|DB_OP_LT://			0x01|0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s = %s OR %s < %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					case DB_OP_GT|DB_OP_LT://			=  not DB_OP_EQUAL 0x02|0x04
						{
							_snprintf(szCriterion2, DB_SQLSTRING_MAXLEN-1, "%s < %s OR %s > %s ", 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData, 
								pdbcCriteria->m_pdbConn->m_pdbTable[pdbcCriteria->m_usTableIndex].m_pdbField[i].m_pszData, 
								pdbcCriteria2->m_pdbField[i].m_pszData);
						} break;
					}
				}
				if(strlen(szCriterion)+strlen(szOperator2)+strlen(szCriterion2)<DB_SQLSTRING_MAXLEN) // just in case
				{
					strcat(szCriterion, szOperator2);
					strcat(szCriterion, szCriterion2);
				}
			}


			if(strlen(szSQL)+strlen(szOperator)+strlen(szCriterion)<DB_SQLSTRING_MAXLEN) // just in case
			{
				strcat(szSQL, szOperator);
				strcat(szSQL, szCriterion);
			}
		}
		i++;
	}

//	AfxMessageBox(szSQL);

  nReturnCode = ExecuteSQL(pdbcCriteria->m_pdbConn, szSQL, pszInfo);
	return nReturnCode;
}




int CDBUtil::ExecuteSQL(CDBconn* pdbConn, char* pszSQL, char* pszInfo)
{
	int nReturnCode = DB_ERROR_BADARG;
	if((pdbConn)&&(pszSQL)&&(strlen(pszSQL)>0))
	{
		ConnectDatabase(pdbConn, pszInfo);  // re-establishes connection if disconnected, else just returns
		if (!pdbConn->m_bConnected)
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Data source %s not open.\nSQL: %s", pdbConn->m_pszDSNname, pszSQL);
			if(m_pmsgr)
			{
				char errorstring[DB_ERRORSTRING_LEN];
				_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Data source %s not open.\nSQL: %s", pdbConn->m_pszDSNname, pszSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:ExecuteSQL");
			}
			return DB_ERROR_NOTOPEN;
		}

		try  
		{
			pdbConn->m_db.ExecuteSQL(pszSQL);
			nReturnCode = DB_SUCCESS;
		} 
		catch(CException* e)// CDBException *e, CMemoryException *m)  
		{
			if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
			{

				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught exception %d: %s%sSQL: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, pszSQL);
				if(m_pmsgr)
				{
					char errorstring[DB_SQLSTRING_MAXLEN];
					_snprintf(errorstring, DB_SQLSTRING_MAXLEN-1, "ExecuteSQL: Caught exception %d: %s%sSQL: %s", ((CDBException *) e)->m_nRetCode, ((CDBException *) e)->m_strError, ((CDBException *) e)->m_strStateNativeOrigin, pszSQL);
					Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:ExecuteSQL");
				}
				nReturnCode = DB_ERROR_DBEX;
				if(CheckConnectionError(((CDBException *) e)->m_strStateNativeOrigin)==DB_ERROR_NOTOPEN)
				{
					pdbConn->m_db.Close();
					pdbConn->m_bConnected = false;
				}
			}
			else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "ExecuteSQL: Caught memory exception: %s\nSQL: %s", errorstring, pszSQL);
				// The error code is in e->m_nRetCode
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught memory exception: %s\nSQL: %s", errorstring, pszSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:ExecuteSQL");
				nReturnCode = DB_ERROR_MEMEX;
			}
			else 
			{
				char errorstring[DB_ERRORSTRING_LEN];
				char msgstring[DB_SQLSTRING_MAXLEN];
				e->GetErrorMessage(errorstring, DB_ERRORSTRING_LEN);
				if(pszInfo) _snprintf(pszInfo, DB_SQLSTRING_MAXLEN-1, "ExecuteSQL: Caught other exception: %s\nSQL: %s", errorstring, pszSQL);
				_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught other exception: %s\nSQL: %s", errorstring, pszSQL);
				Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:ExecuteSQL");
				nReturnCode = DB_ERROR_EX;
			}
			e->Delete();
		} 
		catch(...)
		{
			char msgstring[DB_SQLSTRING_MAXLEN];
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught other exception\nSQL: %s", pszSQL);
			_snprintf(msgstring, DB_ERRORSTRING_LEN-1, "ExecuteSQL: Caught other exception\nSQL: %s", pszSQL);
			Message(MSG_PRI_HIGH|MSG_ICONERROR, msgstring, "DBUtil:ExecuteSQL");
			nReturnCode = DB_ERROR_EX;
		}
	}
	return nReturnCode; 
}

char* CDBUtil::EncodeQuotes(int nType, CString szString, char* pszInfo)
{
	char* pszReturn = EncodeQuotes(nType, szString.GetBuffer(1), pszInfo);
	szString.ReleaseBuffer();
	return pszReturn;
}

char* CDBUtil::EncodeQuotes(CString szString, char* pszInfo)
{
	char* pszReturn = EncodeQuotes(szString.GetBuffer(1), pszInfo);
	szString.ReleaseBuffer();
	return pszReturn;
}

char* CDBUtil::LimitEncodeQuotes(CString szString, unsigned long ulFieldLength, char* pszInfo)
{
	char* pszReturn = LimitEncodeQuotes(szString.GetBuffer(1), ulFieldLength, pszInfo);
	szString.ReleaseBuffer();
	return pszReturn;
}

char* CDBUtil::LimitEncodeQuotes(char* pszString, unsigned long ulFieldLength, char* pszInfo)
{
	char* pszReturn = NULL;
	if(pszString)
	{
		if(strlen(pszString)>ulFieldLength)
		{
			*(pszString+ulFieldLength) = 0; // null term
		}

		pszReturn = EncodeQuotes(pszString, pszInfo);
	}
	return pszReturn;
}

char* CDBUtil::EncodeQuotes(int nType, char* pszString, char* pszInfo)
{
	char* pszReturn = NULL;
	switch(nType)
	{
	case DB_MYSQL:
		{
			if(pszString)
			{
				CBufferUtil bu;
				int nLen = strlen(pszString);
				int i = 0;

				char ch;
				for (int j=0; j<nLen; j++)
				{
					ch=pszString[j];
					if ((ch == '\'')||(ch == '\\'))
					{
						i++;
					}
				}

				i += (++nLen);  // term zero

				pszReturn = (char*)malloc(i);
				if(pszReturn)
				{
					i=0;
					for (int j=0; j<nLen; j++)
					{
						if(( *(pszString+j) == '\'')||( *(pszString+j) == '\\'))
						{
							*(pszReturn+i) = '\\';
							i++;
							*(pszReturn+i) = *(pszString+j);
						}
						else *(pszReturn+i) = *(pszString+j);
						i++;
					}
				}
				else
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "EncodeQuotes: Could not allocate buffer.");
				}
			}
			else
			{
				if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "EncodeQuotes: NULL pointer passed in.");
			}
			return pszReturn;

		}
	case DB_MSSQL:
	default:
		{
			 return EncodeQuotes(pszString, pszInfo);
		}
	}
	return pszReturn;
}


char* CDBUtil::EncodeQuotes(char* pszString, char* pszInfo)
{
	char* pszReturn = NULL;
	if(pszString)
	{
		CBufferUtil bu;
		int nLen = strlen(pszString);
		int i = bu.CountChar(pszString, nLen, '\'');
		i += (++nLen);  // term zero

		pszReturn = (char*)malloc(i);
		if(pszReturn)
		{
			i=0;
			for (int j=0; j<nLen; j++)
			{
				if( *(pszString+j) == '\'')
				{
					*(pszReturn+i) = *(pszString+j);
					i++;
					*(pszReturn+i) = *(pszString+j);
				}
				else *(pszReturn+i) = *(pszString+j);
				i++;
			}
		}
		else
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "EncodeQuotes: Could not allocate buffer.");
		}
	}
	else
	{
		if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "EncodeQuotes: NULL pointer passed in.");
	}
	return pszReturn;
}

char* CDBUtil::SQLTypeSize(unsigned char ucType, unsigned short usSize)
{
	char* pszReturn = (char*)malloc(64); // should not take more than this.
	if(pszReturn)
	{
		switch(ucType)
		{
// integer types
		case DB_TYPE_BYTE:		//0x01  // unsigned char   (tiny int)     0 through 255.
			{
				sprintf(pszReturn, "tinyint");
			} break;
		case DB_TYPE_SHORT:		//0x02  // signed short    (small int)   -2^15 (-32,768) through 2^15 - 1 (32,767).
			{
				sprintf(pszReturn, "smallint");
			} break;
		case DB_TYPE_INT:			//0x03  // signed long 8 bytes.  (int)   -2^31 (-2,147,483,648) through 2^31 - 1 (2,147,483,647).
			{
				sprintf(pszReturn, "int");
			} break;
		case DB_TYPE_BIGINT:	//0x04  // signed double long 8 bytes.  (big int, datetime,) -2^63 (-9223372036854775808) through 2^63-1 (9223372036854775807).
			{
				sprintf(pszReturn, "bigint");
			} break;

// string
		case DB_TYPE_STR:			//0x05  // fixed width array of char, max 8000.
			{
				sprintf(pszReturn, "char(%d)", usSize);
			} break;
		case DB_TYPE_VSTR:		//0x06  // variable array of char, max 8000.
			{
				sprintf(pszReturn, "varchar(%d)", usSize);
			} break;
		case DB_TYPE_TEXT:		//0x07  // variable array of char, max 2^31 - 1 (2,147,483,647) characters.
			{
				// this is untested, and may be incorrect.
				sprintf(pszReturn, "text(%d)", usSize);
			} break;

//floating point
			// the meanings of float, double, and real are confusing.
			// float should be 4 bytes, double 8 bytes.
			// sql has float and real, where float = 8 bytes and real is 4 bytes.
		case DB_TYPE_FLOAT:		//0x08  // float (real), -3.40E + 38 through 3.40E + 38.
			{
				sprintf(pszReturn, "real NULL");
			} break;
		case DB_TYPE_DOUBLE:	//0x09  // double (float)  -1.79E + 308 through 1.79E + 308.
			{
				sprintf(pszReturn, "float NULL");
			} break;

// binary
		case DB_TYPE_BIN:			//0x0a  // fixed width array of bytes, max 8000.
			{
				sprintf(pszReturn, "binary(%d)", usSize);
			} break;
		case DB_TYPE_VBIN:		//0x0b  // variable array of bytes, max 8000.
			{
				sprintf(pszReturn, "varbinary(%d)", usSize);
			} break;

		case DB_TYPE_DATA:		//0x0c  // variable array of bytes, max 2^31 - 1 (2,147,483,647) bytes.
			{
				// this is untested, and may be incorrect.
				sprintf(pszReturn, "image(%d)", usSize);
			} break;
		default: // type not supported.
			{
				free(pszReturn); pszReturn=NULL;
			} break;
		}
	}
	return pszReturn;
}


int CDBUtil::CheckConnectionError(CString szString)
{
	//State:S0022,Native:207,Origin:[Microsoft][SQL Native Client][SQL Server]
//	if(szString.Left(8).CompareNoCase("State:08")==0)  // changed to find because can have several errors stacked into one error msg.
	if(szString.Find("State:08", 0)>=0)
	{
		//08 is connection prob state see table below.
		return DB_ERROR_NOTOPEN;
	}
	return DB_SUCCESS;


/*
00000 Success
01000 General warning
01002 Disconnect error
01004 Data truncated
01006 Privilege not revoked
01S00 Invalid connection string attribute
01S01 Error in row
01S02 Option value changed
01S03 No rows updated or deleted
01S04 More than one row updated or deleted
01S05 Cancel treated as FreeStmt/Close
01S06 Attempt to fetch before the result returned the first rowset
07001 Wrong number of parameters
07006 Restricted data type attribute violation
07S01 Invalid use of default paramater
08001 Unable to connect to data source
08002 Connection in use
08003 Connection not open
08004 Data source rejected establishment of connection
08007 Connection failure during transaction
08S01 Communication link failure
21S01 Insert value list does not match column list
21S02 Degree of derived table does not match column list
22001 String data right truncation
22002 Indicator variable required but not supplied
22003 Numeric value out of range
22005 Error in assignment
22008 Datetime field overflow
22012 Division by zero
22026 String data, length mismatch
23000 Integrity constraint violation
24000 Invalid cursor state
25000 Invalid transaction state
28000 Invalid authorization specification
34000 Invalid cursor name
37000 Syntax error or access violation
3C000 Duplicate cursor name
40001 Serialization failure
42000 Syntax error or access violation
70100 Operation aborted
IM001 Driver does not support this function
IM002 Data source name not found and no default driver specified
IM003 Specified driver could not be loaded
IM004 Driver's SQLAllocEnv failed
IM005 Driver's SQLAllocConnect failed
IM006 Driver's SQLSetConnectOption failed
IM007 No data source or driver specified; dialog prohibited
IM008 Dialog failed
IM009 Unable to load translation DLL
IM010 Data source name too long
IM011 Driver name too long
IM012 DRIVER keyword syntax error
IM013 Trace file error
S0001 Base table or view already exists
S0002 Base table not found
S0011 Index already exists
S0012 Index not found
S0021 Column already exists
S0022 Column not found
S0023 No default for column
S1000 General error
S1001 Memory allocation failure
S1002 Invalid column number
S1003 Program type out of range
S1004 SQL data type out of range
S1008 Operation canceled
S1009 Invalid argument value
S1010 Function sequence error
S1011 Operation invalid at this time
S1012 Invalid transaction operation code specified
S1015 No cursor name available
S1090 Invalid string or buffer length
S1091 Descriptor type out of range
S1092 Option type out of range
S1093 Invalid parameter number
S1094 Invalid scale value
S1095 Function type out of range
S1096 Information type out of range
S1097 Column type out of range
S1098 Scope type out of range
S1099 Nullable type out of range
S1100 Uniqueness option type out of range
S1101 Accuracy option type out of range
S1103 Direction option out of range
S1104 Invalid precision value
S1105 Invalid parameter type
S1106 Fetch type out of range
S1107 Row value out of range
S1108 Concurrency option out of range
S1109 Invalid cursor position
S1110 Invalid driver completion
S1111 Invalid bookmark value
S1C00 Driver not capable
S1DE0 No data at execution values pending
S1T00 Timeout expired
*/

}
