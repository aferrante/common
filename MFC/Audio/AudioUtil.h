// AudioUtil.h: interface for the CAudioUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUDIOUTIL_H__1F9287AD_E0F8_4F4C_8388_4846E279792C__INCLUDED_)
#define AFX_AUDIOUTIL_H__1F9287AD_E0F8_4F4C_8388_4846E279792C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// the following line forces a link with the windows multimedia lib
#pragma comment(lib, "winmm.lib")
#include <mmsystem.h>

#define AUDIO_SUCCESS				0
#define AUDIO_ERROR					-1
#define AUDIO_INPROGRESS		-2
#define AUDIO_BADARGS				-3
#define AUDIO_ALLBELOWTHRESHOLD		-4
#define AUDIO_UNSUPPORTED		-128

#define AUDIO_FORMATCHANGE	1

#define TRIM_HEAD		0x00000001
#define TRIM_TAIL		0x00000002

#define NUM_RATES_SUPPORTED 8

class CAudioUtil  
{
public:

	DWORD m_dwSupportedSamplingRates[NUM_RATES_SUPPORTED];
	int  m_nNumSamplingRates;
	int  m_nCurrentSamplingRate;
	unsigned long m_ulCurrentDevice;

	HWAVEIN m_hwi;  // the current record device handle
	WAVEFORMATEX m_wfx;  // write format
	WAVEHDR** m_ppwvhdr;  // buffer of pointers to data buffers
	
	char m_szRecordFile[MAX_PATH];
	bool m_bWavInOpen;
	bool m_bRecording;
	bool m_bStopOperations;
	bool m_bStereo;
	unsigned long m_ulMaxSecondsPerBuffer;
	unsigned long m_ulRecMS;


public:
	CAudioUtil();
	virtual ~CAudioUtil();

	int PlayWave( char* pszFile, unsigned long ulFlags = SND_ASYNC|SND_FILENAME);  // returns immediately by default.
	int StopWave();
	int TrimWave( char* pszFileSource, double dblThreshold, char* pszFileDest=NULL, unsigned long ulFlags=TRIM_HEAD|TRIM_TAIL); 

	int ClearRecordBuffers(); 
	int SetRecordDevice(unsigned long ulDevice=0xffffffff); 
	int SetRecordFormat(unsigned long ulRate=44100, bool bStereo=true, bool b16bit=true);  // cf mono, cf 8 bit, if device is valid, set it to best supported
//	int CheckRecordFormat(unsigned long ulDevice);  // check the current one;
	int BeginRecordWave( char* pszFile, unsigned long ulFlags=0, char* pszInfo=NULL);  // returns immediately by default.
	int StopRecordWave();  // returns immediately by default.

	int IsSampleRateSupported( DWORD dwSampleRate );
};

#endif // !defined(AFX_AUDIOUTIL_H__1F9287AD_E0F8_4F4C_8388_4846E279792C__INCLUDED_)


//#define WAVE_FORMAT_1M08       0x00000001       /* 11.025 kHz, Mono,   8-bit  */
//#define WAVE_FORMAT_1S08       0x00000002       /* 11.025 kHz, Stereo, 8-bit  */
//#define WAVE_FORMAT_1M16       0x00000004       /* 11.025 kHz, Mono,   16-bit */
//#define WAVE_FORMAT_1S16       0x00000008       /* 11.025 kHz, Stereo, 16-bit */
//#define WAVE_FORMAT_2M08       0x00000010       /* 22.05  kHz, Mono,   8-bit  */
//#define WAVE_FORMAT_2S08       0x00000020       /* 22.05  kHz, Stereo, 8-bit  */
//#define WAVE_FORMAT_2M16       0x00000040       /* 22.05  kHz, Mono,   16-bit */
//#define WAVE_FORMAT_2S16       0x00000080       /* 22.05  kHz, Stereo, 16-bit */
//#define WAVE_FORMAT_4M08       0x00000100       /* 44.1   kHz, Mono,   8-bit  */
//#define WAVE_FORMAT_4S08       0x00000200       /* 44.1   kHz, Stereo, 8-bit  */
//#define WAVE_FORMAT_4M16       0x00000400       /* 44.1   kHz, Mono,   16-bit */
//#define WAVE_FORMAT_4S16       0x00000800       /* 44.1   kHz, Stereo, 16-bit */
