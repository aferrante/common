// AudioUtil.cpp: implementation of the CAudio class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AudioUtil.h"
#include <process.h>
#include <sys/timeb.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void AudioRecordThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioUtil::CAudioUtil()
{
	m_hwi = NULL;

	// default settings for writing
  m_wfx.wFormatTag = WAVE_FORMAT_PCM; 
  m_wfx.nChannels = 2; 
  m_wfx.nSamplesPerSec = 44100; 
  m_wfx.wBitsPerSample = 16;  // 16 or 8 bits per sample 
  m_wfx.cbSize=0;  // ignored 
  m_wfx.nBlockAlign = (m_wfx.nChannels*m_wfx.wBitsPerSample)/8;
  m_wfx.nAvgBytesPerSec = m_wfx.nSamplesPerSec*m_wfx.nBlockAlign; 

	m_ppwvhdr = NULL;

	m_bWavInOpen = false;
	m_bRecording = false;
	m_bStopOperations = false;
	m_bStereo = true;

	m_ulMaxSecondsPerBuffer = 2;  // reasonable default value to be a few secs ahead

	m_nNumSamplingRates=0;
	m_nCurrentSamplingRate=0;
	m_ulCurrentDevice=0xffffffff;

}

CAudioUtil::~CAudioUtil()
{
	m_bStopOperations = true;
	while(m_bWavInOpen) Sleep(50); // wait for ops to finish
	ClearRecordBuffers();
}

int CAudioUtil::ClearRecordBuffers()
{
	if((m_ppwvhdr)&&(m_hwi))
	{
		int i=0;
		while(m_ppwvhdr[i]!=NULL)
		{
			MMRESULT mmrv = waveInUnprepareHeader( m_hwi, m_ppwvhdr[i], sizeof(*m_ppwvhdr[i])); 
			if(mmrv == MMSYSERR_NOERROR)
			{
				free(m_ppwvhdr[i]->lpData);
			}
			delete m_ppwvhdr[i];
			i++;
		}
		delete [] m_ppwvhdr;
		m_ppwvhdr = NULL;
	}
	return AUDIO_ERROR;
}


int CAudioUtil::PlayWave( char* pszFile, unsigned long ulFlags)  // returns immediately by default.
{
	if((pszFile)&&(strlen(pszFile)))
	{
		if(PlaySound(pszFile, NULL, ulFlags)) return AUDIO_SUCCESS;
	}
	return AUDIO_ERROR;
}

int CAudioUtil::StopWave()
{
	if(PlaySound(NULL, NULL, NULL)) return AUDIO_SUCCESS;
	return AUDIO_ERROR;
}

int CAudioUtil::SetRecordDevice(unsigned long ulDevice)
{
	m_nNumSamplingRates = 0;
	static DWORD pdwSamplingRates[] = { 11025, 22050, 32000, 44100, 48000, 64000, 88200, 96000 };
	WAVEINCAPS wic;
	MMRESULT mmrv = waveInGetDevCaps(ulDevice, &wic, sizeof(WAVEINCAPS)); 
	if(mmrv == MMSYSERR_NOERROR)
	{
			// first the standard rates based on format flag
			/*
				if( wic.dwFormats&WAVE_FORMAT_1M16|WAVE_FORMAT_1S16 )
            sampleRates[ deviceInfo->numSampleRates++ ] = 11025.;
        if( wic.dwFormats & WAVE_FORMAT_2M16 ||wic.dwFormats & WAVE_FORMAT_2S16 )
            sampleRates[ deviceInfo->numSampleRates++ ] = 22050.;
        if( wic.dwFormats & WAVE_FORMAT_4M16 ||wic.dwFormats & WAVE_FORMAT_4S16 )
            sampleRates[ deviceInfo->numSampleRates++ ] = 44100.;
*/
      
	// custom rates based on opening the device successfully with 16 bits stereo
    for(int i=0; i<8; i++ )
    {
      WAVEFORMATEX wfx;
      wfx.wFormatTag = WAVE_FORMAT_PCM;
      wfx.nChannels = 2;
      wfx.nSamplesPerSec = pdwSamplingRates[i];
      wfx.wBitsPerSample = 16;
      wfx.nAvgBytesPerSec = wfx.nChannels * wfx.nSamplesPerSec * sizeof(short);
      wfx.nBlockAlign = (WORD)(wfx.nChannels * sizeof(short));
      wfx.cbSize = 0; // ignored 
      if( waveInOpen( NULL, ulDevice, &wfx, NULL, NULL, WAVE_FORMAT_QUERY ) == MMSYSERR_NOERROR )
      {
        m_dwSupportedSamplingRates[m_nNumSamplingRates] = pdwSamplingRates[i];
				m_nNumSamplingRates++;
      }
    }
		m_ulCurrentDevice = ulDevice;
		return AUDIO_SUCCESS;
	}

	m_ulCurrentDevice = 0xffffffff;
	return AUDIO_ERROR;
}


// returns index if supported, -1 if not
int CAudioUtil::IsSampleRateSupported( DWORD dwSampleRate )
{
	int i=0;
	while(i<m_nNumSamplingRates)
	{
		if(m_dwSupportedSamplingRates[i]==dwSampleRate)
			return i;
		else
			i++;
	}
	return AUDIO_ERROR;
}


int CAudioUtil::SetRecordFormat(unsigned long ulRate, bool bStereo, bool b16bit)  // cf mono, cf 8 bit, if device is valid, set it to best supported
{
	if(m_ulCurrentDevice!=0xffffffff)
	{
		m_nCurrentSamplingRate = IsSampleRateSupported(ulRate);
		if(m_nCurrentSamplingRate<0)	return AUDIO_UNSUPPORTED;
//		WAVEINCAPS wic;
//		MMRESULT mmrv = waveInGetDevCaps(m_ulCurrentDevice, &wic, sizeof(WAVEINCAPS)); 
//		if(mmrv == MMSYSERR_NOERROR)
//		{
		int nRV = AUDIO_SUCCESS;

		// get the best supported format
		m_wfx.wFormatTag = WAVE_FORMAT_PCM; 
		m_wfx.nSamplesPerSec = ulRate; 

		if(bStereo) 
			m_wfx.nChannels = 2; 
		else
			m_wfx.nChannels = 1; 

		if(b16bit) 
			m_wfx.wBitsPerSample = 16; 
		else
			m_wfx.wBitsPerSample = 8; 

		m_wfx.cbSize=0;  // ignored 

/*
		// old stuff, used with format flags.
		// now, we have checked with waveInOpen
			// have to work backwards from best quality down...
			unsigned long ulFormat = 0;
			if(bStereo)
			{
				if(b16bit)
				{
					if(ulRate==11025)
					{
						ulFormat = WAVE_FORMAT_1S16;
					}
					else 
					if(ulRate==22050)
					{
						ulFormat = WAVE_FORMAT_2S16;
					}
					else
					if(ulRate==44100)
					{
						ulFormat = WAVE_FORMAT_4S16;
					}
				}
				else  //8bit
				{
					if(ulRate==11025)
					{
						ulFormat = WAVE_FORMAT_1S08;
					}
					else 
					if(ulRate==22050)
					{
						ulFormat = WAVE_FORMAT_2S08;
					}
					else
					if(ulRate==44100)
					{
						ulFormat = WAVE_FORMAT_4S08;
					}
				}
			}
			else  //mono
			{
				if(b16bit)
				{
					if(ulRate==11025)
					{
						ulFormat = WAVE_FORMAT_1M16;
					}
					else 
					if(ulRate==22050)
					{
						ulFormat = WAVE_FORMAT_2M16;
					}
					else
					if(ulRate==44100)
					{
						ulFormat = WAVE_FORMAT_4M16;
					}
				}
				else  //8bit
				{
					if(ulRate==11025)
					{
						ulFormat = WAVE_FORMAT_1M08;
					}
					else 
					if(ulRate==22050)
					{
						ulFormat = WAVE_FORMAT_2M08;
					}
					else
					if(ulRate==44100)
					{
						ulFormat = WAVE_FORMAT_4M08;
					}
				}
			}


			if(!(wic.dwFormats&ulFormat))
			{
				// if the format is not supported we must get the best one that is supported
				// if stereo we are going to force the best stereo, then go down the list of mono formats

				unsigned long ulCheckFormat =  ulFormat/4;
				bool bDone = false;
				while(!bDone)
				{
					while(ulCheckFormat>=1)
					{
						if(wic.dwFormats&ulCheckFormat)
						{
							switch(ulCheckFormat)
							{
							case WAVE_FORMAT_1M08://       0x00000001       /* 11.025 kHz, Mono,   8-bit  * /
								{
									m_wfx.nSamplesPerSec = 11025; 
									m_wfx.nChannels = 1; 
									m_wfx.wBitsPerSample = 8; 
								} break;
							case WAVE_FORMAT_1S08://       0x00000002       /* 11.025 kHz, Stereo, 8-bit  * /
								{
									m_wfx.nSamplesPerSec = 11025; 
									m_wfx.nChannels = 2; 
									m_wfx.wBitsPerSample = 8; 
								} break;
							case WAVE_FORMAT_1M16://       0x00000004       /* 11.025 kHz, Mono,   16-bit * /
								{
									m_wfx.nSamplesPerSec = 11025; 
									m_wfx.nChannels = 1; 
									m_wfx.wBitsPerSample = 16; 
								} break;
							case WAVE_FORMAT_1S16://       0x00000008       /* 11.025 kHz, Stereo, 16-bit * /
								{
									m_wfx.nSamplesPerSec = 11025; 
									m_wfx.nChannels = 2; 
									m_wfx.wBitsPerSample = 16; 
								} break;
							case WAVE_FORMAT_2M08://       0x00000010       /* 22.05  kHz, Mono,   8-bit  * /
								{
									m_wfx.nSamplesPerSec = 22050; 
									m_wfx.nChannels = 1; 
									m_wfx.wBitsPerSample = 8; 
								} break;
							case WAVE_FORMAT_2S08://       0x00000020       /* 22.05  kHz, Stereo, 8-bit  * /
								{
									m_wfx.nSamplesPerSec = 22050; 
									m_wfx.nChannels = 2; 
									m_wfx.wBitsPerSample = 8; 
								} break;
							case WAVE_FORMAT_2M16://       0x00000040       /* 22.05  kHz, Mono,   16-bit * /
								{
									m_wfx.nSamplesPerSec = 22050; 
									m_wfx.nChannels = 1; 
									m_wfx.wBitsPerSample = 16; 
								} break;
							case WAVE_FORMAT_2S16://       0x00000080       /* 22.05  kHz, Stereo, 16-bit * /
								{
									m_wfx.nSamplesPerSec = 22050; 
									m_wfx.nChannels = 2; 
									m_wfx.wBitsPerSample = 16; 
								} break;
							case WAVE_FORMAT_4M08://       0x00000100       /* 44.1   kHz, Mono,   8-bit  * /
								{
									m_wfx.nSamplesPerSec = 44100; 
									m_wfx.nChannels = 1; 
									m_wfx.wBitsPerSample = 8; 
								} break;
							case WAVE_FORMAT_4S08://       0x00000200       /* 44.1   kHz, Stereo, 8-bit  * /
								{
									m_wfx.nSamplesPerSec = 44100; 
									m_wfx.nChannels = 2; 
									m_wfx.wBitsPerSample = 8; 
								} break;
							case WAVE_FORMAT_4M16://       0x00000400       /* 44.1   kHz, Mono,   16-bit * /
								{
									m_wfx.nSamplesPerSec = 44100; 
									m_wfx.nChannels = 1; 
									m_wfx.wBitsPerSample = 16; 
								} break;
							case WAVE_FORMAT_4S16://       0x00000800       /* 44.1   kHz, Stereo, 16-bit * /
								{
									m_wfx.nSamplesPerSec = 44100; 
									m_wfx.nChannels = 2; 
									m_wfx.wBitsPerSample = 16; 
								} break;

							}

							bDone=true; break;
						}
						ulCheckFormat/=4;
					}

					if(bStereo) // go down the list again
					{
						ulCheckFormat =  ulFormat/2;
						bStereo = false; // we are looking for mono formats now.
					}
					else bDone = true;
				}

				nRV = AUDIO_FORMATCHANGE;
			}
*/
		m_wfx.nBlockAlign = (m_wfx.nChannels*m_wfx.wBitsPerSample)/8;
		m_wfx.nAvgBytesPerSec = m_wfx.nSamplesPerSec*m_wfx.nBlockAlign; 

		return nRV;

	}
	return AUDIO_ERROR;

}

/*
int CAudioUtil::CheckRecordFormat(unsigned long ulDevice)  // check the current format;
{	
	WAVEINCAPS wic;
	MMRESULT mmrv = waveInGetDevCaps(ulDevice, &wic, sizeof(WAVEINCAPS)); 
	if(mmrv == MMSYSERR_NOERROR)
	{

//#define WAVE_FORMAT_1M08       0x00000001       /* 11.025 kHz, Mono,   8-bit  */
//#define WAVE_FORMAT_1S08       0x00000002       /* 11.025 kHz, Stereo, 8-bit  */
//#define WAVE_FORMAT_1M16       0x00000004       /* 11.025 kHz, Mono,   16-bit */
//#define WAVE_FORMAT_1S16       0x00000008       /* 11.025 kHz, Stereo, 16-bit */
//#define WAVE_FORMAT_2M08       0x00000010       /* 22.05  kHz, Mono,   8-bit  */
//#define WAVE_FORMAT_2S08       0x00000020       /* 22.05  kHz, Stereo, 8-bit  */
//#define WAVE_FORMAT_2M16       0x00000040       /* 22.05  kHz, Mono,   16-bit */
//#define WAVE_FORMAT_2S16       0x00000080       /* 22.05  kHz, Stereo, 16-bit */
//#define WAVE_FORMAT_4M08       0x00000100       /* 44.1   kHz, Mono,   8-bit  */
//#define WAVE_FORMAT_4S08       0x00000200       /* 44.1   kHz, Stereo, 8-bit  */
//#define WAVE_FORMAT_4M16       0x00000400       /* 44.1   kHz, Mono,   16-bit */
//#define WAVE_FORMAT_4S16       0x00000800       /* 44.1   kHz, Stereo, 16-bit */

/*
// these are only the standard rates.  we must add custom rates as well
		int nRV = AUDIO_SUCCESS;

		// get the best supported format
		bool bStereo = true;
		if(m_wfx.nChannels == 1) bStereo = false;
		bool b16bit = true;
		if(m_wfx.wBitsPerSample == 8) b16bit = false; 
		unsigned long ulRate = m_wfx.nSamplesPerSec;

		// have to work backwards from best quality down...
		unsigned long ulFormat = 0;
		if(bStereo)
		{
			if(b16bit)
			{
				if(ulRate==11025)
				{
					ulFormat = WAVE_FORMAT_1S16;
				}
				else 
				if(ulRate==22050)
				{
					ulFormat = WAVE_FORMAT_2S16;
				}
				else
				if(ulRate==44100)
				{
					ulFormat = WAVE_FORMAT_4S16;
				}
			}
			else  //8bit
			{
				if(ulRate==11025)
				{
					ulFormat = WAVE_FORMAT_1S08;
				}
				else 
				if(ulRate==22050)
				{
					ulFormat = WAVE_FORMAT_2S08;
				}
				else
				if(ulRate==44100)
				{
					ulFormat = WAVE_FORMAT_4S08;
				}
			}
		}
		else  //mono
		{
			if(b16bit)
			{
				if(ulRate==11025)
				{
					ulFormat = WAVE_FORMAT_1M16;
				}
				else 
				if(ulRate==22050)
				{
					ulFormat = WAVE_FORMAT_2M16;
				}
				else
				if(ulRate==44100)
				{
					ulFormat = WAVE_FORMAT_4M16;
				}
			}
			else  //8bit
			{
				if(ulRate==11025)
				{
					ulFormat = WAVE_FORMAT_1M08;
				}
				else 
				if(ulRate==22050)
				{
					ulFormat = WAVE_FORMAT_2M08;
				}
				else
				if(ulRate==44100)
				{
					ulFormat = WAVE_FORMAT_4M08;
				}
			}
		}


		if(!(wic.dwFormats&ulFormat))
		{
			nRV = AUDIO_UNSUPPORTED;
		}
		return nRV;

	}
	return AUDIO_ERROR;
}
*/

int CAudioUtil::BeginRecordWave( char* pszFile, unsigned long ulFlags, char* pszInfo)  // returns immediately by default.
{
	if((m_bRecording)||(m_bWavInOpen))
	{
		if(pszInfo)
		{
			strcpy(pszInfo, "There is already a transfer in progress.");
		}
		return AUDIO_INPROGRESS;
	}
	if(m_ulCurrentDevice==0xffffffff)
	{
		if(pszInfo)
		{
			strcpy(pszInfo, "Invalid recording device.");
		}
		return AUDIO_ERROR;
	}
	if((m_nNumSamplingRates<=0)||(m_nCurrentSamplingRate<0)||(m_nCurrentSamplingRate>=m_nNumSamplingRates))  return AUDIO_UNSUPPORTED;

	if((pszFile)&&(strlen(pszFile)))
	{
		strcpy(m_szRecordFile, pszFile);

		// device must be set before beginrecord wave is called, 
		// SetRecordFormat also must be called
//		SetRecordFormat(44100, true, true);
/*
		WAVEINCAPS wic;
		waveInGetDevCaps(ulDevice, &wic, sizeof(WAVEINCAPS)); 

		char foo[4096];
		sprintf(foo, "formats, dev %d: 0x%x", ulDevice, wic.dwFormats);
//		sprintf(foo, "formats: %d, %d, %d", m_wfx.nSamplesPerSec, m_wfx.nChannels, m_wfx.wBitsPerSample);
		AfxMessageBox(foo);  //debug
*/

		MMRESULT mmrv = waveInOpen( &m_hwi, m_ulCurrentDevice, &m_wfx, NULL, NULL, CALLBACK_NULL); 

		if(mmrv == MMSYSERR_NOERROR)
		{
			m_bWavInOpen = true;
			m_bStopOperations = false;

			m_ulRecMS = 0;
			if(_beginthread( AudioRecordThread, 0, (void*)(this) ) != -1)  // do the writes to the buffer
			{
				return AUDIO_SUCCESS;
			}
		}
		else
		{
			if(pszInfo)
			{
				waveInGetErrorText( mmrv, pszInfo, MAXERRORLENGTH );
//				AfxMessageBox(pszInfo);  // debug
			}
		}
	}
	else
	{
		if(pszInfo)
		{
			strcpy(pszInfo, "Invalid filename.");
		}

	}
	return AUDIO_ERROR;
}

int CAudioUtil::StopRecordWave()
{
	if((!m_bRecording)&&(!m_bWavInOpen)) return AUDIO_SUCCESS;  // already stopped

	m_bStopOperations = true;
	while(m_bWavInOpen) Sleep(50); // wait for ops to finish

	if(strlen(m_szRecordFile)==0) return AUDIO_SUCCESS;  // blank it out if successful
	return AUDIO_ERROR;
}

int CAudioUtil::TrimWave( char* pszFileSource, double dblThreshold, char* pszFileDest, unsigned long ulFlags)
{
	if(ulFlags==0)	return AUDIO_SUCCESS;  //it's already not trimmed!
	if((pszFileSource)&&(strlen(pszFileSource)))
	{
		// we arent changing formats or anything like that, so what we are going to do is parse the file directly.
		FILE* pfile = fopen(pszFileSource, "rb");

		if(pfile)
		{
			unsigned char* pch;

			fseek(pfile, 0, SEEK_END);
			unsigned long ulFileLen = ftell(pfile);

//		AfxMessageBox("mallocing");
			pch = (unsigned char*) malloc(ulFileLen);
			if(pch)
			{
				fseek(pfile, 0, SEEK_SET);
				fread(pch, sizeof(char), ulFileLen, pfile);
				fclose(pfile);
				pfile = NULL;
//		AfxMessageBox("malloced");


				if(ulFileLen>44)
				{  // get first 44 bytes.

					if(
							(strncmp((char*)pch, "RIFF", 4)==0)
						&&(strncmp((char*)pch+8, "WAVEfmt ", 8)==0)
						&&(strncmp((char*)pch+36, "data", 4)==0)
						)
					{
						WAVEFORMATEX wfx;  // write format
						// make sure its uncompressed PCM
						memcpy(&wfx, pch+20, 16);  // get the wave format.
//		AfxMessageBox("hdr");

						if(
								(wfx.wFormatTag != WAVE_FORMAT_PCM)
							||((wfx.wBitsPerSample != 8 )&&(wfx.wBitsPerSample != 16))
							)
						{
							free(pch);
							return AUDIO_UNSUPPORTED;
						}

						unsigned long ulDataLen, i=0, ulThresholdData=0;
						memcpy(&ulDataLen, pch+40, 4);  // get the source datalen

						unsigned char* pucInpoint = pch+44;
						unsigned char* pucOutpoint = pch+ulFileLen;  // eof

						if(dblThreshold>100.0) dblThreshold=100.0;
						if(dblThreshold<0.0) dblThreshold=0.0;

						double dblLower, dblUpper;

						unsigned char chLower=128, chUpper=128;
						short shLower=0, shUpper=0;

						int nNumBytes;
						if(wfx.wBitsPerSample == 8)
						{
							dblLower = 128.0 + (dblThreshold*((double)(SCHAR_MIN))/100.0);
							chLower=char(dblLower);
							dblUpper = 128.0 + (dblThreshold*((double)(SCHAR_MAX))/100.0);
							chUpper=char(dblUpper);
							nNumBytes = (wfx.nChannels==1)?1:2;
						}
						else
						{
							dblLower = 0.0 + (dblThreshold*((double)(SHRT_MIN))/100.0);
							shLower=short(dblLower);
							dblUpper = 0.0 + (dblThreshold*((double)(SHRT_MAX))/100.0);
							shUpper=short(dblUpper);
							nNumBytes = (wfx.nChannels==1)?2:4;
						}


						bool bBodyFound = false;
						bool bSilence = true;
						bool bStereo = (wfx.nChannels==1)?false:true;

//						char foo[256];
//						sprintf(foo, "%d<1<%d, %d<2<%d", chLower, chUpper, shLower, shUpper);
//						AfxMessageBox(foo);


						i=44;  // start of data.
						while ((i<ulDataLen)&&(!bBodyFound))// count from front first.
						{
							if(wfx.wBitsPerSample == 8)
							{
								// unsigned char, midpoint 128 = vol 0;
								// get a pair if stero, otherwise single sample
								
								if((*(pch+i)<chLower)||(*(pch+i)>chUpper))
								{
									// above threshold
									bSilence = false;
								}
								else bSilence = true;

								if((bStereo)&&(bSilence))  // check other sample
								{
									if((*(pch+i+1)<chLower)||(*(pch+i+1)>chUpper))
									{
										// above threshold
										bSilence = false;
									}
									else bSilence = true;
								}

								if(!bSilence)
								{
									bBodyFound = true;
									pucInpoint = pch+i;
								}
							}
							else
							{
								short shLeft, shRight;
								// signed short, midpoint 0 = vol 0;
								// get a pair if stero, otherwise single sample
								memcpy(&shLeft, pch+i, 2);
								memcpy(&shRight, pch+i+2, 2);

								if((shLeft<shLower)||(shLeft>shUpper))
								{
									// above threshold
									bSilence = false;
								}
								else bSilence = true;

								if((bStereo)&&(bSilence))  // check other sample
								{
									if((shRight<shLower)||(shRight>shUpper))
									{
										// above threshold
										bSilence = false;
									}
									else bSilence = true;
								}

								if(!bSilence)
								{
									bBodyFound = true;
									pucInpoint = pch+i;
								}
							}

							i+=nNumBytes;
						}   // while ! body found.

//						sprintf(foo, "%d, %d, %d, %d", pch, pucInpoint, pucOutpoint, pch+ulFileLen);
//						AfxMessageBox(foo);


						// here either the body has been found, or we hit the end of file (all silence)
						if(bBodyFound)
						{
							bBodyFound = false;  // find body from end
							i=ulDataLen-nNumBytes;  // last sample
							while ((i>(unsigned long)(pucInpoint-pch))&&(!bBodyFound))
							{
								if(wfx.wBitsPerSample == 8)
								{
									// unsigned char, midpoint 128 = vol 0;
									// get a pair if stero, otherwise single sample
									
									if((*(pch+i)<chLower)||(*(pch+i)>chUpper))
									{
										// above threshold
										bSilence = false;
									}
									else bSilence = true;

									if((bStereo)&&(bSilence))  // check other sample
									{
										if((*(pch+i+1)<chLower)||(*(pch+i+1)>chUpper))
										{
											// above threshold
											bSilence = false;
										}
										else bSilence = true;
									}

									if(!bSilence)
									{
										bBodyFound = true;
										pucOutpoint = pch+i;  // technically it was the previously tested sample. fix later
									}
								}
								else
								{
									short shLeft, shRight;
									// signed short, midpoint 0 = vol 0;
									// get a pair if stero, otherwise single sample
									memcpy(&shLeft, pch+i, 2);
									memcpy(&shRight, pch+i+2, 2);

									if((shLeft<shLower)||(shLeft>shUpper))
									{
										// above threshold
										bSilence = false;
									}
									else bSilence = true;

									if((bStereo)&&(bSilence))  // check other sample
									{
										if((shRight<shLower)||(shRight>shUpper))
										{
											// above threshold
											bSilence = false;
										}
										else bSilence = true;
									}

									if(!bSilence)
									{
										bBodyFound = true;
										pucOutpoint = pch+i;
									}
								}

								i-=nNumBytes;
							}

						}
						else
						{
							// everything below threshold.
							pucOutpoint = pucInpoint;
						}

//						sprintf(foo, "%d, %d, %d, %d", pch, pucInpoint, pucOutpoint, pch+ulFileLen);
//						AfxMessageBox(foo);

						// here we should have inpoints and outpoints.
						// now we can manipulate the buffer
						// write the file out;

						HMMIO hmmio;

						/* Open the file for reading with buffered I/O. Let windows use its default internal buffer */
						if((pszFileDest)&&(strlen(pszFileDest)))
						{
							// if !bBodyFound, just writes an empty file
							hmmio = mmioOpen(pszFileDest, NULL, MMIO_WRITE|MMIO_CREATE|MMIO_EXCLUSIVE);
						}
						else
						{
							if(!bBodyFound)
							{
								//we dont want to overwrite the source file with empty.
								free(pch); 
								return AUDIO_ALLBELOWTHRESHOLD;
							}
							hmmio = mmioOpen(pszFileSource, NULL, MMIO_WRITE|MMIO_CREATE|MMIO_EXCLUSIVE);
						}
						if(hmmio)
						{
							unsigned long ulBytes=0;
							// first write the RIFF WAVE header.
							MMCKINFO ckr, ckf, ckd;
							ckr.cksize =1; // corrected with ascend
							ckr.fccType = mmioStringToFOURCC( "WAVE", MMIO_TOUPPER); 
 							MMRESULT mmrv = mmioCreateChunk( hmmio, &ckr, MMIO_CREATERIFF); 
 
							// then write the format chunk
							ckf.cksize =16; // corrected with ascend if nec
							ckf.ckid = mmioStringToFOURCC( "fmt ", 0); 
 							mmrv = mmioCreateChunk( hmmio, &ckf, 0);
							// add fmt data.

							char formatbuffer[32];
							sprintf(formatbuffer, 
								"%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c" ,
								WAVE_FORMAT_PCM,0, // Compression code, tag format
								wfx.nChannels, 0, //number of channels (i.e. mono, stereo...)
								//Sample rate
								(unsigned char)(wfx.nSamplesPerSec&0x000000ff),
								(unsigned char)((wfx.nSamplesPerSec&0x0000ff00)>>8),
								(unsigned char)((wfx.nSamplesPerSec&0x00ff0000)>>16),
								(unsigned char)((wfx.nSamplesPerSec&0xff000000)>>24),
								//Average bytes per second
								(unsigned char)(wfx.nAvgBytesPerSec&0x000000ff),
								(unsigned char)((wfx.nAvgBytesPerSec&0x0000ff00)>>8),
								(unsigned char)((wfx.nAvgBytesPerSec&0x00ff0000)>>16),
								(unsigned char)((wfx.nAvgBytesPerSec&0xff000000)>>24),
								//Block align
								(unsigned char)(wfx.nBlockAlign&0x00ff),
								(unsigned char)((wfx.nBlockAlign&0xff00)>>8),
								//number of bits per sample of mono data
								(unsigned char)(wfx.wBitsPerSample&0x00ff),
								(unsigned char)((wfx.wBitsPerSample&0xff00)>>8)

								);
									
							mmioWrite( hmmio, formatbuffer, 16);

							mmrv = mmioAscend( hmmio, &ckf, 0); 

							// then write the data chunk
							ckd.cksize =1; // corrected with ascend if nec
							ckd.ckid = mmioStringToFOURCC( "data", 0); 
 							mmrv = mmioCreateChunk( hmmio, &ckd, 0);


							if(!(ulFlags&TRIM_HEAD))
							{
								pucInpoint = pch+44;
							}

							if(!(ulFlags&TRIM_TAIL))
							{
								pucOutpoint = pch+ulFileLen;
							}
								
							ulThresholdData = pucOutpoint-pucInpoint;


							mmioWrite( hmmio, (char*)pucInpoint, ulThresholdData);

							mmrv = mmioAscend( hmmio, &ckd, 0); 
							mmrv = mmioAscend( hmmio, &ckr, 0); 

							mmioClose( hmmio, 0);

							free(pch);
							return AUDIO_SUCCESS;
							
						}

					}// else its not the right format file
				}  // else its not the right format file
				free(pch);
			} // else no buffer
			if(pfile) fclose(pfile);
		}
		DWORD dwError = GetLastError();
		char foo[256]; sprintf(foo, "Opening %s returned error %d",pszFileSource,dwError);
		AfxMessageBox(foo);

		return AUDIO_ERROR;
	}
	else
		return AUDIO_BADARGS;
}


void AudioRecordThread(void* pvArgs)
{
	CAudioUtil* pau = (CAudioUtil*) pvArgs;
	if(pau)
	{
		MMRESULT mmrv;
		// first we create two buffers.
		if(pau->m_ppwvhdr) pau->ClearRecordBuffers();  // empty anything, start fresh.

		pau->m_ppwvhdr = new WAVEHDR*[3];  // start with 2 + null term.

		for(int i=0; i<2; i++)
		{
			WAVEHDR* pwvhdr = new WAVEHDR;
			pwvhdr->lpData = (char*)malloc( pau->m_ulMaxSecondsPerBuffer*pau->m_wfx.nAvgBytesPerSec); 
			pwvhdr->dwBufferLength = pau->m_ulMaxSecondsPerBuffer*pau->m_wfx.nAvgBytesPerSec; 
			pwvhdr->dwBytesRecorded=0; 
			pwvhdr->dwUser=0; 
			pwvhdr->dwFlags=0; 
			pwvhdr->dwLoops=0; 
			pwvhdr->lpNext=NULL; 

			pau->m_ppwvhdr[i] = pwvhdr;
			mmrv = waveInPrepareHeader( pau->m_hwi, pwvhdr, sizeof(*pwvhdr));
			// should error check
			mmrv = waveInAddBuffer( pau->m_hwi, pwvhdr, sizeof(*pwvhdr));
			// should error check
		}

		pau->m_ppwvhdr[2] = NULL; // null term the array.

		// OK now we have 2 buffers in, so we are one ahead.
		int nCurrentBuffer = 0;
		int nNumBuffers = 2;  // number of created and prepped buffers

		//we start the recording
		_timeb timeStart;
		_timeb timeNow;
		_ftime( &timeStart );
		
		mmrv = waveInStart(pau->m_hwi);
 		if(mmrv == MMSYSERR_NOERROR)
		{
			pau->m_bRecording = true;
			while(!pau->m_bStopOperations)
			{

				// poll the current buffer
				if(pau->m_ppwvhdr[nCurrentBuffer]->dwFlags & WHDR_DONE)
				{
					// we are one ahead but now we make new.

					WAVEHDR** ppwvhdr = new WAVEHDR*[nNumBuffers+2];  // +1 + null term.
					if(ppwvhdr)
					{
						nNumBuffers++;

						int i=0;
						while(pau->m_ppwvhdr[i]!=NULL)
						{
							ppwvhdr[i] = pau->m_ppwvhdr[i];
							i++;
						}

						WAVEHDR** pptemp = pau->m_ppwvhdr;

						pau->m_ppwvhdr = ppwvhdr;

						delete [] pptemp;

						WAVEHDR* pwvhdr = new WAVEHDR;
						pwvhdr->lpData = (char*)malloc( pau->m_ulMaxSecondsPerBuffer*pau->m_wfx.nAvgBytesPerSec); 
						pwvhdr->dwBufferLength = pau->m_ulMaxSecondsPerBuffer*pau->m_wfx.nAvgBytesPerSec; 
						pwvhdr->dwBytesRecorded=0; 
						pwvhdr->dwUser=0; 
						pwvhdr->dwFlags=0; 
						pwvhdr->dwLoops=0; 
						pwvhdr->lpNext=NULL; 

						pau->m_ppwvhdr[i] = pwvhdr;
						pau->m_ppwvhdr[i+1] = NULL;  // null term
						mmrv = waveInPrepareHeader( pau->m_hwi, pwvhdr, sizeof(*pwvhdr));
						// should error check
						mmrv = waveInAddBuffer( pau->m_hwi, pwvhdr, sizeof(*pwvhdr));
						// should error check

						nCurrentBuffer++; // now look at the one currently filling up.
					}
				}

				_ftime( &timeNow );
				pau->m_ulRecMS = (timeNow.time-timeStart.time)*1000+(timeNow.millitm-timeStart.millitm);

			}  // while(!pau->m_bStopOperations)


//AfxMessageBox("stopping"); //debug
			// ok at this point, we need to close out the recording
			mmrv = waveInStop(pau->m_hwi);
			if(mmrv == MMSYSERR_NOERROR)
			{
//AfxMessageBox("stopped"); //debug
				mmrv = waveInReset( pau->m_hwi ); 
 				if(mmrv == MMSYSERR_NOERROR)
				{
//AfxMessageBox("reset"); //debug
					mmrv = waveInClose( pau->m_hwi ); 
 					if(mmrv == MMSYSERR_NOERROR)
					{
//AfxMessageBox("closed"); //debug
						//write the file out;

						HMMIO hmmio;

						/* Open the file for reading with buffered I/O. Let windows use its default internal buffer */
						hmmio = mmioOpen(pau->m_szRecordFile, NULL, MMIO_WRITE|MMIO_CREATE|MMIO_EXCLUSIVE);
						if(hmmio)
						{

							unsigned long ulBytes=0;
							// first write the RIFF WAVE header.
							MMCKINFO ckr, ckf, ckd;
							ckr.cksize =1; // corrected with ascend
							ckr.fccType = mmioStringToFOURCC( "WAVE", MMIO_TOUPPER); 
 							mmrv = mmioCreateChunk( hmmio, &ckr, MMIO_CREATERIFF); 
 
							// then write the format chunk
							ckf.cksize =16; // corrected with ascend if nec
							ckf.ckid = mmioStringToFOURCC( "fmt ", 0); 
 							mmrv = mmioCreateChunk( hmmio, &ckf, 0);
							// add fmt data.

							char formatbuffer[32];
							sprintf(formatbuffer, 
								"%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c" ,
								WAVE_FORMAT_PCM,0, // Compression code, tag format
								pau->m_wfx.nChannels, 0, //number of channels (i.e. mono, stereo...)
								//Sample rate
								(unsigned char)(pau->m_wfx.nSamplesPerSec&0x000000ff),
								(unsigned char)((pau->m_wfx.nSamplesPerSec&0x0000ff00)>>8),
								(unsigned char)((pau->m_wfx.nSamplesPerSec&0x00ff0000)>>16),
								(unsigned char)((pau->m_wfx.nSamplesPerSec&0xff000000)>>24),
								//Average bytes per second
								(unsigned char)(pau->m_wfx.nAvgBytesPerSec&0x000000ff),
								(unsigned char)((pau->m_wfx.nAvgBytesPerSec&0x0000ff00)>>8),
								(unsigned char)((pau->m_wfx.nAvgBytesPerSec&0x00ff0000)>>16),
								(unsigned char)((pau->m_wfx.nAvgBytesPerSec&0xff000000)>>24),
								//Block align
								(unsigned char)(pau->m_wfx.nBlockAlign&0x00ff),
								(unsigned char)((pau->m_wfx.nBlockAlign&0xff00)>>8),
								//number of bits per sample of mono data
								(unsigned char)(pau->m_wfx.wBitsPerSample&0x00ff),
								(unsigned char)((pau->m_wfx.wBitsPerSample&0xff00)>>8)

								);
									
							mmioWrite( hmmio, formatbuffer, 16);

							mmrv = mmioAscend( hmmio, &ckf, 0); 

							// then write the data chunk
							ckd.cksize =1; // corrected with ascend if nec
							ckd.ckid = mmioStringToFOURCC( "data", 0); 
 							mmrv = mmioCreateChunk( hmmio, &ckd, 0);


							int i=0;
							while(pau->m_ppwvhdr[i] != NULL)
							{
								if(pau->m_ppwvhdr[i]->dwBytesRecorded>0)
								{
									ulBytes+=pau->m_ppwvhdr[i]->dwBytesRecorded;
									mmioWrite( hmmio, pau->m_ppwvhdr[i]->lpData, pau->m_ppwvhdr[i]->dwBytesRecorded);
									mmioFlush( hmmio, 0); 
								}
								i++;
							}
							mmrv = mmioAscend( hmmio, &ckd, 0); 
							mmrv = mmioAscend( hmmio, &ckr, 0); 

							mmioClose( hmmio, 0);
							
							// successful, so blank the filename.
							strcpy(pau->m_szRecordFile, "");
						}

						pau->ClearRecordBuffers();

						// release
						pau->m_bRecording = false;
						pau->m_bWavInOpen = false;
					}
				}
			}

		}  //waveInStart(pau->m_hwi);

	}
}

