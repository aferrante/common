#ifndef UNIVERSAL_CUSTOM_DEFINES_AND_TYPES_INCLUDED
#define UNIVERSAL_CUSTOM_DEFINES_AND_TYPES_INCLUDED

//unidef.h
// custom defines and types.

///////////////////// 
//defines

#ifndef TRY_PROTECTED
// called with this proto: TRY_PROTECTED(LPFUNC Function(arg), LPFUNCRSLT pnRV, LPSTSTR pszError, CString szFuncName, UINT MB_OK, LPUINT)
#define TRY_PROTECTED(FUNC, LPFUNCRSLT, LPERRORSTRING, FUNCNAME, MBTYPE, LPMBRSLT) \
  try  \
  { \
		if(LPFUNCRSLT != NULL)  *LPFUNCRSLT = FUNC; \
		else FUNC;\
  } \
  catch(CException *e) \
  { \
		if(LPFUNCRSLT != NULL)  *LPFUNCRSLT = UINT(-1); \
    if (LPERRORSTRING != NULL) \
		{ \
			char buffer[1024]; \
			if(e->GetErrorMessage(buffer, 1024)) \
				LPERRORSTRING->Format("%s exception: %s", FUNCNAME, buffer); \
			else \
				LPERRORSTRING->Format("Unknown exception in %s.", FUNCNAME); \
    }\
		if(MBTYPE>=0) \
		{\
			UINT r = e->ReportError(MBTYPE);\
			if(LPMBRSLT != NULL) *LPMBRSLT = r;\
		}\
  }
#endif



// These are damned useful:
#ifndef max
#define max(a,b)            ((a > b) ? a : b)
#endif

#ifndef min
#define min(a,b)            ((a < b) ? a : b)
#endif                    

//used with FontSettings_t
//styles
#ifndef FSS_REGULAR
#define FSS_REGULAR 0
#endif
#ifndef FSS_BOLD
#define FSS_BOLD 1
#endif
#ifndef FSS_ITALIC
#define FSS_ITALIC 2
#endif
//effects
#ifndef FSE_NONE
#define FSE_NONE 0
#endif
#ifndef FSE_UNDERLINE 
#define FSE_UNDERLINE 1
#endif
#ifndef FSE_STRIKEOUT 
#define FSE_STRIKEOUT 2
#endif

///////////////////// 
//structures and other types

typedef struct FontSettings_t
{
  CString szFace;
  int nPoints; //really, tenths of a point units.
	int nStyle;
	int nEffects;
	COLORREF crBG;
	COLORREF crText;
} FontSettings_t;


///////////////////// 
//specifications

// ***************************************
// Font Settings Code String Specification
//
// The Font Settings Code String is of the form:
// <!FS"Arial"08c30ffffff000000>
// The characters "<!FS" represent the beginning of a font settings code string.
//    The '!' is included to allow for compliance with HTML.  The "FS" must be upper-case.
// The font face name follows in quotes.  The example is "Arial" font face.
// The next three chars are the size of the font in tenth-point units, represented 
//    in hexadecimal. Since the representation is three chars long, a maximum size of 4095
//    tenth-points, or 409.5 points, is implied.  Leading zeros are used if necessary, to 
//    make this size field always 3 chars long.  
//    The example has 08c = 140 tenth-points, or 14 pts.
// The next char is the hexadecimal representation of all Styles, as defined above.
//    The example has 3 = FSS_BOLD | FSS_ITALIC.   This field is exactly one char long. 
// The next char is the hexadecimal representation of all Effects, as defined above.
//    The example has 0 = FSE_NONE.   This field is exactly one char long. 
// The next six chars are the hexidecmal representation of the 24 bit RGB color of the font 
//    background color.  The example has ffffff = RGB(255,255,255) = white.  
//    Red would be ff0000, green would be 00ff00, and blue would be 0000ff.
//    This field is always exactly six chars long.
// The next six chars are the hexidecmal representation of the 24 bit RGB color of the font 
//    text color.  The example has 000000 = RGB(0,0,0) = black.  
//    This field is always exactly six chars long.
// The next char is ">" and is the final char, signifying the end of the Font Settings Code.

#endif //UNIVERSAL_CUSTOM_DEFINES_AND_TYPES_INCLUDED

                        