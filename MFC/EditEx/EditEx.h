#if !defined(AFX_EDITEX_H__7FEFA922_206B_11D5_B7CF_00C0F02D1C2E__INCLUDED_)
#define AFX_EDITEX_H__7FEFA922_206B_11D5_B7CF_00C0F02D1C2E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// EditEx.h : header file
//***************  check EditEx.htm for help on functions.
#include "../unidef.h" //for font structs and defs

#define MAX_DISALLOWED 128

#ifndef NOGGIN
#define NOGGIN
#endif

// for editex set disallow chars
#define EESD_REPLACE 0  // replace existing disallow chars with chars passed in
#define EESD_COMBINE 1  // combine existing disallow chars with chars passed in
#define EESD_DELETE -1  // remove the existing font from disallow list
#define EESD_CLEAR  -2  // remove all fonts and text from disallow list
/////////////////////////////////////////////////////////////////////////////
// CEditCtrlPlus window

class CEditEx //: public CWnd
{
// Construction
public:
	CEditEx(CRichEditCtrl* pRE);
	CEditEx();

// Attributes

public:
	CRichEditCtrl* m_pRE; // pointer to "the" rich edit ctrl
	FontSettings_t m_fsDefaultFont;
	CString m_szDisallowed[2][MAX_DISALLOWED]; //disallowed chars of font

// Operations
public:
//set
	void SetRichEdit(CRichEditCtrl* pRE);
	CString SetCodedText(CString szCodedText);
	void SetCodedTextVoidRV(CString szCodedText);
	BOOL SetFont( FontSettings_t fsFont);
	BOOL SetFont( FontSettings_t fsFont, CHARRANGE chrg);
	BOOL SetFont( FontSettings_t fsFont, int nStartIndex, int nEndIndex);
	COLORREF SetBackgroundColor( BOOL bSysColor, COLORREF cr );
	void SetDisallowedChars(CString szFontFace, CString szDisallowedChars, BOOL bInvert=FALSE , int nMode=EESD_REPLACE);

//get
	CRichEditCtrl* GetRichEdit();
	CRichEditCtrl* GetWnd();
	CString GetCodedText();
	BOOL GetCodedText(CString* szText); //override
	FontSettings_t GetFont();
	FontSettings_t GetFont(CHARRANGE chrg);
	FontSettings_t GetFont( int nStartIndex, int nEndIndex);
	COLORREF GetBackgroundColor();

//utility
	CString StripCodes(CString szCodedText, BOOL* bCodesExist=NULL);
	BOOL StripCodes(CString szCodedText, CString* szText); //override
	FontSettings_t FontCodeToData(CString szFontString);
	CString FontDataToCode(FontSettings_t fsFont);
	CString FontDataToCode(CHARFORMAT cf, COLORREF crBG=CLR_INVALID);
	CHARFORMAT FontDataToCharFormat(FontSettings_t fsFont);
	FontSettings_t  CharFormatToFontData(CHARFORMAT cf);
	BOOL IsCharFormatEquivalent(CHARFORMAT cf1, CHARFORMAT cf2);
	BOOL IsCharDisallowed(char ch, CString szFontFace);
	BOOL IsCharDisallowed(char ch);
	CString StripCR(CString szCodedText);
	CString ReplaceLFwithCRLF(CString szLFText);	
	CString ShowCRLF(CString szText);	
	BOOL InsertCharOfFont(char ch, FontSettings_t fsFont, int nIndex=-1 );
	BOOL InsertStringOfFont(CString szText, FontSettings_t fsFont, int nIndex=-1);


//For disallow chars, in the parent dialog that contains the RichEditCtrl 
//	add a method for PreTranslateMessage(), and give it the following body:
//( modify member variable name for the CEditEx object if necessary)

//BOOL CParentDlg::PreTranslateMessage(MSG* pMsg) 
//{
//	if ((pMsg->message == WM_CHAR)&&(pMsg->hwnd==m_EditEx.GetWnd()->GetSafeHwnd()))
//	{	if(m_EditEx.IsCharDisallowed(pMsg->wParam)) return TRUE; }
//	return CDialog::PreTranslateMessage(pMsg);
//}

//also ignore any MS comments for overriding CDialog::OnInitDialog()
// function to send the EM_SETEVENTMASK.  It is all set for:
//	ENM_CHANGE | ENM_UPDATE | ENM_SCROLL | ENM_KEYEVENTS | ENM_MOUSEEVENTS | ENM_REQUESTRESIZE | ENM_SELCHANGE


// Implementation
public:
	virtual ~CEditEx();

// Generated message map functions
protected:
	//{{AFX_MSG(CEditEx)
	//}}AFX_MSG
//	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITEX_H__7FEFA922_206B_11D5_B7CF_00C0F02D1C2E__INCLUDED_)
