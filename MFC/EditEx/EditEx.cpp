// EditEx.cpp : implementation file
//

#include "stdafx.h"
#include "EditEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditEx

CEditEx::CEditEx(CRichEditCtrl* pRE)
{ 
	 //defaults
	SetRichEdit(pRE);
/*
  m_fsNULL.szFace=_T("");
  m_fsNULL.nPoints =0;
	m_fsNULL.nStyle = 0;
	m_fsNULL.nEffects = 0;
	m_fsNULL.crBG = CLR_INVALID;
	m_fsNULL.crText = CLR_INVALID;
*/
}

CEditEx::CEditEx()
{ 
	 //defaults
	m_pRE = NULL;
/*
  m_fsNULL.szFace=_T("");
  m_fsNULL.nPoints =0;
	m_fsNULL.nStyle = 0;
	m_fsNULL.nEffects = 0;
	m_fsNULL.crBG = CLR_INVALID;
	m_fsNULL.crText = CLR_INVALID;
	*/
}

CEditEx::~CEditEx()
{

}

//BEGIN_MESSAGE_MAP(CEditEx, CWnd)
	//{{AFX_MSG_MAP(CEditEx)
	//}}AFX_MSG_MAP
//END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CEditEx message handlers

//set

void CEditEx::SetRichEdit(CRichEditCtrl* pRE)
{ 
	 //defaults
	m_pRE = pRE;

  m_fsDefaultFont.szFace=_T("MS Sans Serif");
  m_fsDefaultFont.nPoints =80;
	m_fsDefaultFont.nStyle = 0;
	m_fsDefaultFont.nEffects = 0;
	m_fsDefaultFont.crBG = RGB(255,255,255);
	m_fsDefaultFont.crText = RGB(0,0,0);

	SetFont(m_fsDefaultFont);
	SetBackgroundColor(FALSE, m_fsDefaultFont.crBG);
	m_pRE->SetEventMask(
		ENM_CHANGE | ENM_UPDATE | ENM_SCROLL | ENM_KEYEVENTS | 
		ENM_MOUSEEVENTS | ENM_REQUESTRESIZE | ENM_SELCHANGE);

	for (int x=0; x<MAX_DISALLOWED ; x++)
	{
		m_szDisallowed[0][x]=_T(""); //disallowed chars of font
		m_szDisallowed[1][x]=_T(""); //disallowed chars of font
	}
}

void CEditEx::SetCodedTextVoidRV(CString szCodedText)  // this was written because no return value is faster.
{
	if (m_pRE==NULL) {//AfxMessageBox("no RE");
	return ;}
//	MessageBox(m_pRE->GetSafeHwnd(), szCodedText,"text passed in from edit",MB_OK);

	CString szText;

	m_pRE->SetSel( 0,-1 );//select all
	m_pRE->ReplaceSel( "" );//clear out the ctrl

	szText=StripCodes(szCodedText);

//	MessageBox(m_pRE->GetSafeHwnd(), szText,"coded text stripped of codes",MB_OK);

//	m_pRE->SetWindowText(szCodedText);
	m_pRE->SetWindowText(szText);

	int nCodedLength = szCodedText.GetLength();
	int nCodedChar=0, nChar=0;
	char ch;
	CHARFORMAT cf;
	m_pRE->GetDefaultCharFormat(cf);

//	AfxMessageBox(cf.szFaceName);

	CString szFont;
	CHARRANGE chrg;
	chrg.cpMin=0; chrg.cpMax=0;

	while (nCodedChar<nCodedLength)
	{
		ch=szCodedText.GetAt(nCodedChar);
		if (ch==0) return ;
		if (ch=='<')
		{

			if (
					(szCodedText.GetAt(nCodedChar+1)=='!')&&
					(szCodedText.GetAt(nCodedChar+2)=='F')&&
					(szCodedText.GetAt(nCodedChar+3)=='S')&&
					(szCodedText.GetAt(nCodedChar+4)=='"')
				 )
			{
				//then we have the beginning of a font code, must make sure it is complete and valid
				//Set the format for the charrange so far.
				m_pRE->SetSel(chrg);
				m_pRE->SetSelectionCharFormat(cf);
//					AfxMessageBox(cf.szFaceName);

				chrg.cpMin=chrg.cpMax;

				// then go about resetting the chrg and the cf
				int nChecking=nCodedChar+5;
				BOOL bFound=FALSE;
				while((nChecking<nCodedLength)&&(!bFound))
				{
					if(szCodedText.GetAt(nChecking)!='>')	nChecking++;
					else bFound=TRUE;
				}
				if (bFound) // then we found a font end.
				{
					szFont=szCodedText.Mid(nCodedChar, nChecking-nCodedChar+1);
					nCodedChar=nChecking+1;
					cf=FontDataToCharFormat(FontCodeToData(szFont));
				}
				else
				{
//					m_pRE->SetSel(nChar,nChar+1);
//					m_pRE->SetSelectionCharFormat(cf);
					chrg.cpMax=nChar+1;
					nCodedChar++;
					nChar++;
				}
			}
			else
			{
//				m_pRE->SetSel(nChar,nChar+1);
//				m_pRE->SetSelectionCharFormat(cf);
				chrg.cpMax=nChar+1;
				nCodedChar++;
				nChar++;
			}
		}
		else
		{
//			m_pRE->SetSel(nChar,nChar+1);
//			m_pRE->SetSelectionCharFormat(cf);
			chrg.cpMax=nChar+1;
			nCodedChar++;
			nChar++;
		}
	}
	// then do the final one.
	m_pRE->SetSel(chrg);
	m_pRE->SetSelectionCharFormat(cf);
//	CString szFuckNT; szFuckNT.Format("setting chrg:%d,%d\nwith font:%s",chrg.cpMin,chrg.cpMax,cf.szFaceName);
//	AfxMessageBox(szFuckNT);
}

CString CEditEx::SetCodedText(CString szCodedText)
{
	if (m_pRE==NULL) {//AfxMessageBox("no RE");
	return _T("");}
//	MessageBox(m_pRE->GetSafeHwnd(), szCodedText,"text passed in from edit",MB_OK);

	CString szReturn,szText;
	szReturn=GetCodedText(); //obtain return value

	m_pRE->SetSel( 0,-1 );//select all
	m_pRE->ReplaceSel( "" );//clear out the ctrl

	szText=StripCodes(szCodedText);

//	MessageBox(m_pRE->GetSafeHwnd(), szText,"coded text stripped of codes",MB_OK);

	m_pRE->SetWindowText(szText);

	int nCodedLength = szCodedText.GetLength();
	int nCodedChar=0, nChar=0;
	char ch;
	CHARFORMAT cf;
	m_pRE->GetDefaultCharFormat(cf);

	while (nCodedChar<nCodedLength)
	{
		ch=szCodedText.GetAt(nCodedChar);
		if (ch==0) return szReturn;
		if (ch=='<')
		{
			if (
					(szCodedText.GetAt(nCodedChar+1)=='!')&&
					(szCodedText.GetAt(nCodedChar+2)=='F')&&
					(szCodedText.GetAt(nCodedChar+3)=='S')&&
					(szCodedText.GetAt(nCodedChar+4)=='"')
				 )
			{
				//then we have the beginning of a font code, must make sure it is complete and valid
				int nChecking=nCodedChar+5;
				BOOL bFound=FALSE;
				while((nChecking<nCodedLength)&&(!bFound))
				{
					if(szCodedText.GetAt(nChecking)!='>')	nChecking++;
					else bFound=TRUE;
				}
				if (bFound) // then we found a font end.
				{
					CString szFont=szCodedText.Mid(nCodedChar, nChecking-nCodedChar+1);
					nCodedChar=nChecking+1;
					cf=FontDataToCharFormat(FontCodeToData(szFont));
				}
				else
				{
					m_pRE->SetSel(nChar,nChar+1);
					m_pRE->SetSelectionCharFormat(cf);
					nCodedChar++;
					nChar++;
				}
			}
			else
			{
				m_pRE->SetSel(nChar,nChar+1);
				m_pRE->SetSelectionCharFormat(cf);
				nCodedChar++;
				nChar++;
			}
		}
		else
		{
			m_pRE->SetSel(nChar,nChar+1);
			m_pRE->SetSelectionCharFormat(cf);
			nCodedChar++;
			nChar++;
		}
	}

	return szReturn;
}

BOOL  CEditEx::SetFont( FontSettings_t fsFont, CHARRANGE chrg)
{
	if (m_pRE==NULL) return FALSE;
	if (fsFont.szFace.GetLength()<=0) return FALSE;
	CHARRANGE chrgInit;
	m_pRE->GetSel(chrgInit);
	BOOL bReturn=FALSE;
	
	m_pRE->SetSel(chrg);
	bReturn=m_pRE->SetSelectionCharFormat(FontDataToCharFormat(fsFont));
	m_pRE->SetSel(chrgInit);
	return bReturn;
}

BOOL  CEditEx::SetFont( FontSettings_t fsFont, int nStartIndex, int nEndIndex)
{
	CHARRANGE chrg;
	chrg.cpMin=nStartIndex;
	chrg.cpMax=nEndIndex;
	return SetFont( fsFont,  chrg);
}

BOOL CEditEx::SetFont( FontSettings_t fsFont)
{
	if (m_pRE==NULL) return FALSE;
	if (fsFont.szFace.GetLength()<=0) return FALSE;
	CHARFORMAT cf;

	m_pRE->GetDefaultCharFormat(cf);
	sprintf(cf.szFaceName,"%s",fsFont.szFace);

	if(fsFont.nStyle&FSS_BOLD) cf.dwEffects|=CFE_BOLD;
	if(fsFont.nStyle&FSS_ITALIC) cf.dwEffects|=CFE_ITALIC;
	if(fsFont.nEffects&FSE_UNDERLINE) cf.dwEffects|=CFE_UNDERLINE;
	if(fsFont.nEffects&FSE_STRIKEOUT) cf.dwEffects|=CFE_STRIKEOUT;

	if(fsFont.crText!=CLR_INVALID) cf.crTextColor = fsFont.crText;
	else cf.crTextColor = fsFont.crText = RGB(0,0,0); //default black
	cf.yHeight=(int)(2.0*(float)fsFont.nPoints);

	m_fsDefaultFont=fsFont;

	if(m_pRE->SetDefaultCharFormat(cf)) return TRUE;
	return FALSE;
}

COLORREF CEditEx::SetBackgroundColor( BOOL bSysColor, COLORREF cr )
{
	if (m_pRE==NULL) return CLR_INVALID;
	if ((cr==CLR_INVALID)&&(!bSysColor)) return CLR_INVALID;
	COLORREF crReturn;
	crReturn=m_pRE->SetBackgroundColor( bSysColor, cr );
	if (bSysColor) m_fsDefaultFont.crBG=GetBkColor(m_pRE->GetDC()->GetSafeHdc());
	else m_fsDefaultFont.crBG=cr;
	return crReturn;
}

void  CEditEx::SetDisallowedChars(CString szFontFace, CString szDisallowedChars, BOOL bInvert, int nMode )
{
//CString foo; foo.Format("Disallowing %s in font: %s, invert=%d, mode=%d",szDisallowedChars,szFontFace,bInvert,nMode);
//AfxMessageBox(foo);
	if(nMode==EESD_CLEAR)  //clear out list.
	{
		for (int q=0; q<MAX_DISALLOWED ; q++)
		{
			m_szDisallowed[0][q]=_T(""); //disallowed chars of font
			m_szDisallowed[1][q]=_T(""); //disallowed chars of font
		}
		return;
	}

	if (szFontFace.GetLength()==0) return;
	BOOL bSet=FALSE, bCombined=FALSE;
	int x=0;
	CString szOrigPassedInText = szDisallowedChars;

	if (bInvert)
	{
		CString szTemp=_T("");
		int i,j;
		char ch;
		for (i=32; i<127; i++) 
		{
			BOOL bFound=FALSE;
			for(j=0; j<szDisallowedChars.GetLength(); j++)
			{
				ch=szDisallowedChars.GetAt(j);
				if (ch==i) bFound=TRUE;
			}
			if (!bFound) szTemp+=(char)i;
		}
		szDisallowedChars=szTemp;
	}

	if (szDisallowedChars.GetLength()<1)
	{
		if (nMode==EESD_REPLACE) nMode=EESD_DELETE;
		else if (nMode==EESD_COMBINE) return;
	}

	switch(nMode)
	{
	case EESD_DELETE:
		x=0;bSet=FALSE;
		while (!bSet)
		{
			if (m_szDisallowed[0][x].CompareNoCase(szFontFace)==0)
			{
				//we found the font
				while(!bSet)
				{
					m_szDisallowed[0][x]=m_szDisallowed[0][x+1]; 
					m_szDisallowed[1][x]=m_szDisallowed[1][x+1];
					if (m_szDisallowed[0][x].CompareNoCase("")==0) bSet=TRUE;
					x++; if (x>=MAX_DISALLOWED-1) bSet=TRUE; 
				}
			}
			else
			{
				x++;
				if (x>=MAX_DISALLOWED) bSet=TRUE; //exits without setting, max has been reached.
			}
		}
		break;

	case EESD_COMBINE:
		x=0;bSet=FALSE;
		while (!bSet)
		{
			if (m_szDisallowed[0][x].CompareNoCase(szFontFace)==0)
			{
				//we found the font
				char ch; int i=0;
				if (bInvert) // then allow passed in chars
				{
					// first get rid of duplicate chars
					CString szTemp=_T("");
					int f=0;
					while (f<szOrigPassedInText.GetLength())
					{
						ch = szOrigPassedInText.GetAt(f);
						if(szTemp.Find(ch) <0 ) szTemp+=CString(ch);
						f++;
					}
					szOrigPassedInText=szTemp;

					while (i<szOrigPassedInText.GetLength())
					{
						ch = szOrigPassedInText.GetAt(i);
						int nPos=m_szDisallowed[1][x].Find(ch);
						if( nPos>=0 )
						{
							CString szLeft, szRight;
							szLeft=m_szDisallowed[1][x].Left(nPos);
							szRight=m_szDisallowed[1][x].Right(m_szDisallowed[1][x].GetLength()-nPos-1);
							m_szDisallowed[1][x]=szLeft+szRight;
						}
						i++;
					}
				}
				else
				{	
					while (i<szDisallowedChars.GetLength())
					{
						ch = szDisallowedChars.GetAt(i);
						if(m_szDisallowed[1][x].Find(ch) <0 )
							m_szDisallowed[1][x]+=CString(ch);
						i++;
					}
				}
				bSet=TRUE; bCombined=TRUE; break;  
			}
			else
			{
				x++;
				if (x==MAX_DISALLOWED) bSet=TRUE; //exits without setting, max has been reached., flows through to replace case, adds it.
			}
		}
		
	case EESD_REPLACE:
		if(!bCombined)
		{
			x=0; bSet=FALSE; //delete first.
			while (!bSet)
			{
				if (m_szDisallowed[0][x].CompareNoCase(szFontFace)==0)
				{
					//we found the font
					while(!bSet)
					{
						m_szDisallowed[0][x]=m_szDisallowed[0][x+1]; 
						m_szDisallowed[1][x]=m_szDisallowed[1][x+1];
						if (m_szDisallowed[0][x].CompareNoCase("")==0) bSet=TRUE;
						x++; if (x>=MAX_DISALLOWED-1) bSet=TRUE; 
					}
				}
				else
				{
					x++;
					if (x>=MAX_DISALLOWED) bSet=TRUE; //exits without setting, max has been reached.
				}
			}
			// then add.
			x=0;bSet=FALSE;
			while (!bSet)
			{
				if (m_szDisallowed[0][x].CompareNoCase("")==0)
				{
					//we found an empty
					m_szDisallowed[0][x]=szFontFace; //disallowed chars of font
					m_szDisallowed[1][x]=szDisallowedChars; //disallowed chars of font
					bSet=TRUE;
	//foo.Format("Disallowed %s in font: %s,",m_szDisallowed[1][x],m_szDisallowed[0][x]);
	//AfxMessageBox(foo);
				}
				else
				{
					x++;
					if (x>=MAX_DISALLOWED) bSet=TRUE; //exits without setting, max has been reached.
				}
			}
		}
		break;
	}

//	CString foo; foo.Format("%s: %s\n%s: %s\n%s: %s",
//		m_szDisallowed[0][0],m_szDisallowed[1][0],m_szDisallowed[0][1],m_szDisallowed[1][1],m_szDisallowed[0][2],m_szDisallowed[1][2]);
//	AfxMessageBox(foo);
}


//get

CString CEditEx::GetCodedText()
{
	if (m_pRE==NULL) return _T("");
	CString szReturn=_T("");
	CString szText, szAssemblyText, szFontCode, szChar;
//	m_pRE->GetWindowText(szText); //includes crlf
	int nChar=m_pRE->GetTextLength();
	if(nChar<=0) return FontDataToCode(GetFont());  // return a string of the "default" font

	int nCharIdx=0;

	CHARFORMAT cfCurrent, cfNext;
	CHARRANGE chrng;

	m_pRE->GetDefaultCharFormat(cfCurrent);

	while(nCharIdx<nChar)
	{
		chrng.cpMin=0; chrng.cpMax=0;
		m_pRE->SetSel(chrng);
		szAssemblyText=_T("");

		chrng.cpMin=nCharIdx; chrng.cpMax=nCharIdx+1;
		m_pRE->SetSel(chrng);
			
		m_pRE->GetSelectionCharFormat(cfNext);

		if ((IsCharFormatEquivalent(cfNext,cfCurrent))&&  //if equiv
					(nCharIdx>0)) //and if it is not the first char
		{
			szAssemblyText+=m_pRE->GetSelText( );
		}
		else
		{
			szFontCode=FontDataToCode(cfNext,m_fsDefaultFont.crBG);
			szReturn+=szAssemblyText;
			szAssemblyText=_T("");
			szReturn+=szFontCode;
			szReturn+=m_pRE->GetSelText( );
			cfCurrent=cfNext;
		}
		nCharIdx++;

		szReturn+=szAssemblyText;

	}
//	AfxMessageBox(szReturn);
	
	szReturn = ReplaceLFwithCRLF(szReturn);
//	AfxMessageBox(ShowCRLF(szReturn));
	return szReturn;
}


BOOL CEditEx::GetCodedText(CString* szText)
{
	if (m_pRE==NULL) return FALSE;
	*szText=GetCodedText();
	if (szText==NULL) return FALSE;
	return TRUE;
}

CRichEditCtrl* CEditEx::GetRichEdit() 
{return m_pRE;}

CRichEditCtrl* CEditEx::GetWnd() 
{return m_pRE;}

FontSettings_t CEditEx::GetFont()
{
	if (m_pRE==NULL) m_fsDefaultFont.szFace=_T("");
	return m_fsDefaultFont;
}

FontSettings_t  CEditEx::GetFont( int nStartIndex, int nEndIndex)
{
	CHARRANGE chrg;
	chrg.cpMin=nStartIndex;
	chrg.cpMax=nEndIndex;
	return GetFont( chrg);
}

FontSettings_t  CEditEx::GetFont(CHARRANGE chrg)
{
	FontSettings_t fsReturn;

	if (m_pRE==NULL) {fsReturn.szFace=_T("");return fsReturn;}
	CHARFORMAT cf;
	if(m_pRE->GetSelectionCharFormat(cf))
		fsReturn=CharFormatToFontData(cf);
	else fsReturn.szFace=_T("");
	return fsReturn;
}

COLORREF CEditEx::GetBackgroundColor()
{
	if (m_pRE==NULL) return CLR_INVALID;
	return m_fsDefaultFont.crBG;
}


//utility

CString CEditEx::StripCodes(CString szCodedText, BOOL* bCodesExist)
{
	if (m_pRE==NULL) return _T("");
	CString szReturn=_T("");
	if (bCodesExist!=NULL) *bCodesExist=FALSE;

//	szCodedText=StripCR(szCodedText);
	int nCodedLength = szCodedText.GetLength();
	int nCodedChar=0;
	char ch;
	while (nCodedChar<nCodedLength)
	{
		ch=szCodedText.GetAt(nCodedChar);
		if (ch==0) return szReturn;
		if (ch=='<')
		{
			if (
					(szCodedText.GetAt(nCodedChar+1)=='!')&&
					(szCodedText.GetAt(nCodedChar+2)=='F')&&
					(szCodedText.GetAt(nCodedChar+3)=='S')&&
					(szCodedText.GetAt(nCodedChar+4)=='"')
				 )
			{
				//then we have the beginning of a font code, must make sure it is complete and valid
				int nChecking=nCodedChar+5;
				BOOL bFound=FALSE;
				while((nChecking<nCodedLength)&&(!bFound))
				{
					if(szCodedText.GetAt(nChecking)!='>')	nChecking++;
					else bFound=TRUE;
				}
				if (bFound) // then we found a font end.
				{
					nCodedChar=nChecking+1;
					if (bCodesExist!=NULL) *bCodesExist=TRUE;
				}
				else
				{
					szReturn+=CString(ch);
					nCodedChar++;
				}
			}
			else
			{
				szReturn+=CString(ch);
				nCodedChar++;
			}
		}
		else
		{
			szReturn+=CString(ch);
			nCodedChar++;
		}
	}
	return szReturn;
}

BOOL  CEditEx::StripCodes(CString szCodedText, CString* szText) //override
{
	if (m_pRE==NULL) return FALSE;
	BOOL bReturn;
	*szText = StripCodes(szCodedText, &bReturn);
	return 	bReturn;
}

FontSettings_t  CEditEx::FontCodeToData(CString szFontString)
{
	// uses a Font Settings Code string according to spec in "unidef.h",
	// returns a FontSettings_t structure.  If there's a failure, szFace in FontSettings_t is _T("").
	FontSettings_t fsReturn;

	fsReturn.szFace=_T("");
	fsReturn.nPoints=0;
	fsReturn.nStyle=0;
	fsReturn.nEffects=0;
	fsReturn.crBG=CLR_INVALID;
	fsReturn.crText=CLR_INVALID;

	CString szParse;
	int nChar;
	
	szFontString.TrimLeft();	szFontString.TrimRight();
	szParse=szFontString;
	nChar=szParse.GetLength();

//	AfxMessageBox(szParse);
	if(nChar<25) return fsReturn; //requires all fields to be present
	if (szParse.Left(4).Compare("<!FS")!=0) return fsReturn; //requires proper start
//	AfxMessageBox("start");
	if (szParse.Right(1).Compare(">")!=0) return fsReturn;// requires proper finish
//	AfxMessageBox("finish");

	if (szParse.Find('"')!=4) return fsReturn; //font face must have starting quotes
	szParse=szFontString.Right(nChar-5);
//	AfxMessageBox(szParse);

	nChar=szParse.Find('"');
	if (nChar<1) return fsReturn;  //font face must have ending quotes
	
	int nBGR=0,nBGG=0,nBGB=0,nTR=0,nTG=0,nTB=0;
	fsReturn.szFace=szParse.Left(nChar);

	char buffer[17];  
	sprintf(buffer, "%s", szParse.Mid(nChar+1,17) );
	sscanf(buffer, 
		"%03x%01x%01x%02x%02x%02x%02x%02x%02x",
		&fsReturn.nPoints,
		&fsReturn.nStyle,
		&fsReturn.nEffects,
		&nBGR, &nBGG, &nBGB, &nTR, &nTG, &nTB
	);
	fsReturn.crBG=RGB(nBGR, nBGG, nBGB);
	fsReturn.crText=RGB(nTR, nTG, nTB);

/*
CString foo;
foo.Format("Face: %s\nsize: %d (%03xh)\nstyle: %x\neffect: %x\nColors: BG:%02x%02x%02x(before RGB) -> %02x%02x%02x, Text:%02x%02x%02x(before RGB) -> %02x%02x%02x",
	fsReturn.szFace,	fsReturn.nPoints,fsReturn.nPoints, fsReturn.nStyle, fsReturn.nEffects,
	nBGR, nBGG, nBGB, GetRValue(fsReturn.crBG),GetGValue(fsReturn.crBG),GetBValue(fsReturn.crBG), 
	nTR, nTG, nTB, GetRValue(fsReturn.crText),GetGValue(fsReturn.crText),GetBValue(fsReturn.crText));
AfxMessageBox(foo);
*/
	return fsReturn;
}

CString  CEditEx::FontDataToCode(FontSettings_t fsFont)
{
	// returns a Font Settings Code string according to spec in "unidef.h"
	// returns _T("") if there's a failure
	CString szReturn=_T(""), szElement;
	if (fsFont.szFace.GetLength()==0)	return _T("");
	szElement.Format("<!FS\"%s\"", fsFont.szFace);
	szReturn+=szElement;

	if (fsFont.nPoints<0) return _T("");
	szElement.Format("%03x",fsFont.nPoints);
	szReturn+=szElement;

	if (fsFont.nStyle<0) return _T("");
	szElement.Format("%01x",fsFont.nStyle);
	szReturn+=szElement;

	if (fsFont.nEffects<0) return _T("");
	szElement.Format("%01x",fsFont.nEffects);
	szReturn+=szElement;

	szElement.Format("%02x%02x%02x",GetRValue(fsFont.crBG),GetGValue(fsFont.crBG),GetBValue(fsFont.crBG));
	szReturn+=szElement;

	szElement.Format("%02x%02x%02x>",GetRValue(fsFont.crText),GetGValue(fsFont.crText),GetBValue(fsFont.crText));

	szReturn+=szElement;
	return szReturn;
}

CString  CEditEx::FontDataToCode(CHARFORMAT cf, COLORREF crBG)
{
	FontSettings_t fs;
	fs.szFace=cf.szFaceName;
	fs.nStyle=0;
	if(cf.dwEffects&CFE_BOLD) fs.nStyle|=FSS_BOLD;
	if(cf.dwEffects&CFE_ITALIC) fs.nStyle|=FSS_ITALIC;
	fs.nEffects=0;
	if(cf.dwEffects&CFE_UNDERLINE) fs.nEffects|=FSE_UNDERLINE;
	if(cf.dwEffects&CFE_STRIKEOUT) fs.nEffects|=FSE_STRIKEOUT;
	fs.nPoints=cf.yHeight/2;
	fs. crBG = crBG;
	fs. crText = cf.crTextColor;

	return FontDataToCode(fs);
}

CHARFORMAT  CEditEx::FontDataToCharFormat(FontSettings_t fsFont)
{

	CHARFORMAT cf;
	if (m_pRE==NULL)
	{
		cf.cbSize=0;
//		cf._wPad1=0;
		cf.dwMask=0;
		cf.dwEffects=0;
		cf.yHeight=0;
		cf.yOffset=0;
		cf.crTextColor=RGB(0,0,0);
		cf.bCharSet=0;
		cf.bPitchAndFamily=0;
		strcpy(cf.szFaceName,"");
//		cf._wPad2=0;

		return cf;
	}

	m_pRE->GetDefaultCharFormat(cf);

	cf.dwMask=CFM_COLOR|CFM_BOLD|CFM_FACE|CFM_ITALIC|CFM_SIZE;
	cf.dwEffects=0;
	if(fsFont.nStyle&FSS_BOLD) cf.dwEffects|=CFE_BOLD;
	if(fsFont.nStyle&FSS_ITALIC) cf.dwEffects|=CFE_ITALIC;
	if(fsFont.nEffects&FSE_UNDERLINE) cf.dwEffects|=CFE_UNDERLINE;
	if(fsFont.nEffects&FSE_STRIKEOUT) cf.dwEffects|=CFE_STRIKEOUT;
	if(fsFont.crText!=NULL) cf.crTextColor = fsFont.crText;
	cf.yHeight=(int)(2.0*(float)fsFont.nPoints);
	sprintf(cf.szFaceName,"%s",fsFont.szFace);
	return cf;
}

FontSettings_t  CEditEx::CharFormatToFontData(CHARFORMAT cf)
{

	FontSettings_t fsFont;

	fsFont.nStyle=0;
	fsFont.nEffects=0;
	if( cf.dwEffects&CFE_BOLD) fsFont.nStyle|=FSS_BOLD;
	if( cf.dwEffects&CFE_ITALIC) fsFont.nStyle|=FSS_ITALIC;
	if( cf.dwEffects&CFE_UNDERLINE) fsFont.nEffects|=FSE_UNDERLINE;
	if( cf.dwEffects&CFE_STRIKEOUT) fsFont.nEffects|=FSE_STRIKEOUT;
	fsFont.crText= cf.crTextColor;
	fsFont.crBG= GetBackgroundColor();
	fsFont.nPoints=(int)((float)cf.yHeight/2.0);
	fsFont.szFace.Format("%s",cf.szFaceName);
	return fsFont;
}

BOOL CEditEx::IsCharFormatEquivalent(CHARFORMAT cf1, CHARFORMAT cf2)
{
	if(stricmp(cf1.szFaceName,cf1.szFaceName)!=0) return FALSE;
	if((cf1.dwEffects&CFE_BOLD)!=(cf2.dwEffects&CFE_BOLD)) return FALSE;
	if((cf1.dwEffects&CFE_ITALIC)!=(cf2.dwEffects&CFE_ITALIC)) return FALSE;
	if((cf1.dwEffects&CFE_UNDERLINE)!=(cf2.dwEffects&CFE_UNDERLINE)) return FALSE;
	if((cf1.dwEffects&CFE_STRIKEOUT)!=(cf2.dwEffects&CFE_STRIKEOUT)) return FALSE;
	if(cf1.yHeight!=cf2.yHeight) return FALSE;
	if(cf1.crTextColor!=cf2.crTextColor) return FALSE;
	return TRUE;
}

BOOL CEditEx::IsCharDisallowed(char ch, CString szFontFace)
{
	int x=0;
	if (szFontFace.GetLength()==0) return TRUE;
	while ( m_szDisallowed[0][x].CompareNoCase("")!=0)
	{
		if((m_szDisallowed[1][x].Find(ch)>=0)&&(m_szDisallowed[0][x].CompareNoCase(szFontFace)==0)) return TRUE;
		x++;
		if (x>=MAX_DISALLOWED)  return FALSE;
	}
	return FALSE;
}

BOOL CEditEx::IsCharDisallowed(char ch)
{
//	This checks if the char is disallowed at the CURRENT caret pos.
	//if it is, it checks if it is allowed in the default font, if it is then it allows it, but sets the position to the default font.
	if (m_pRE==NULL) return TRUE;  // true that it is disallowed if there's no RE control
	CString szFontFace;
	CHARFORMAT cf;
	m_pRE->GetSelectionCharFormat(cf);
	szFontFace.Format("%s",cf.szFaceName);
//	AfxMessageBox(m_szDisallowed[1][0]);
	if(IsCharDisallowed(ch, szFontFace))
	{
		if(IsCharDisallowed(ch, GetFont().szFace))
		{
			return TRUE; //we're hosed, just disallow it.
		}
		else
		{
			m_pRE->SetSelectionCharFormat(FontDataToCharFormat(GetFont()));
			return FALSE;  //allow it
		}
	}
	else
	{
#ifdef NOGGIN
		// this allow a typed chippies 3 to be a "3" rather than a dingbat
			m_pRE->SetSelectionCharFormat(FontDataToCharFormat(GetFont()));
#endif
		return FALSE;
	}
}

	
CString CEditEx::StripCR(CString szCodedText)
{
	CString szReturn=_T("");
	int nLength=szCodedText.GetLength();
	int nChar=0;
	while(nChar<nLength)
	{
		if(szCodedText.GetAt(nChar)!=13)
		{
			szReturn+=CString(szCodedText.GetAt(nChar));
		}
		nChar++;
	}

//AfxMessageBox(szReturn);
	return szReturn;
}

CString CEditEx::ReplaceLFwithCRLF(CString szLFText)
{
	//leaves existing CRLF alone
	CString szReturn=_T("");
	int nLength=szLFText.GetLength();
	int nChar=0;
	while(nChar<nLength)
	{
		if(szLFText.GetAt(nChar)==10)
		{
			if(nChar>0)
			{
				if(szLFText.GetAt(nChar-1)!=13) szReturn+=CString(13);
			}
			else	szReturn+=CString(13);
		}
		szReturn+=CString(szLFText.GetAt(nChar));

		nChar++;
	}

//AfxMessageBox(szReturn);
	return szReturn;

}


CString CEditEx::ShowCRLF(CString szText)
{
	CString szReturn=_T("");
	int nLength=szText.GetLength();
	int nChar=0;
	while(nChar<nLength)
	{
		if(szText.GetAt(nChar)==10)
		{
			szReturn+=_T("<LF> ");
		}
		else
		if(szText.GetAt(nChar)==13)
		{
			szReturn+=_T("<CR>");
		}
		else
		{
			szReturn+=CString(szText.GetAt(nChar));
		}

		nChar++;
	}

//AfxMessageBox(szReturn);
	return szReturn;

}

BOOL CEditEx::InsertCharOfFont(char ch,  FontSettings_t fsFont, int nIndex)
{
	CString szText;
	szText.Format("%c",ch);
	return InsertStringOfFont(szText, fsFont, nIndex);
}

BOOL CEditEx::InsertStringOfFont(CString szText, FontSettings_t fsFont, int nIndex)
{
	int nTextLen=szText.GetLength();
	if ((m_pRE==NULL)||(nTextLen<=0)) return FALSE;
	CHARRANGE chrg;
	if (nIndex<0) 
	{ 
		m_pRE->GetSel(chrg); 
		nIndex=chrg.cpMin;
		if (chrg.cpMin!=chrg.cpMax) m_pRE->Clear(); //deletes anything selected.
	}
	m_pRE->SetSel(nIndex,nIndex);
	m_pRE->ReplaceSel(szText);

	if(fsFont.szFace.GetLength()>0)
	{
//		m_pRE->SetSel(nIndex,nIndex);//+nTextLen);
		m_pRE->SetSel(nIndex,nIndex+nTextLen);
		if(m_pRE->SetSelectionCharFormat(FontDataToCharFormat(fsFont)))
		{
//			m_pRE->ReplaceSel(szText);
			m_pRE->SetSel(nIndex+nTextLen,nIndex+nTextLen);
			return TRUE; // charformatting used
		}
	}
	else  // try default font
	{	
//		m_pRE->SetSel(nIndex,nIndex);//+nTextLen);
		m_pRE->SetSel(nIndex,nIndex+nTextLen);
		if(m_pRE->SetSelectionCharFormat(FontDataToCharFormat(GetFont())))
		{
//			m_pRE->ReplaceSel(szText);
			m_pRE->SetSel(nIndex+nTextLen,nIndex+nTextLen);
			return TRUE; // charformatting used
		}
	}
//	m_pRE->ReplaceSel(szText);
	m_pRE->SetSel(nIndex+nTextLen,nIndex+nTextLen);
	return FALSE; // no font specified, default charformatting used.
}
//	CString szFuckNT; szFuckNT.Format("setting chrg:%d,%d\nwith font:%s",nIndex,nIndex+nTextLen,fsFont.szFace);
//	AfxMessageBox(szFuckNT);

