#if !defined(AFX_SELECTDIRDLG_H__F117B589_9640_4FA0_A6C1_3ED9883B6F07__INCLUDED_)
#define AFX_SELECTDIRDLG_H__F117B589_9640_4FA0_A6C1_3ED9883B6F07__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SelectDirDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectDirDlg dialog

class CSelectDirDlg : public CDialog
{
// Construction
public:
	CSelectDirDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectDirDlg();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CSelectDirDlg)
	enum { IDD = IDD_SELECTDIR_DIALOG };
	CTreeCtrl	m_tree;
	//}}AFX_DATA

	char* m_pszStartDir;
	char  m_pszParentDir[MAX_PATH+1];
	char  m_pszCurrentDir[MAX_PATH+1];
	bool  m_bDirAsRoot;  // dont go back to the drive level
	bool  m_bAllDrives;  // list all drives, or list only the single drive if false.  no effect if m_bDirAsRoot is true

	// sizing
	CRect m_rect;
	CRect m_rectctrl[4];


	int SetInitDir(char* pszStartDir);
	int LoadInitDir(char* pszStartDir);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectDirDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectDirDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemexpandingTree1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG

	int AddDirEntries(char* pszPath, HTREEITEM htiItem=NULL);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTDIRDLG_H__F117B589_9640_4FA0_A6C1_3ED9883B6F07__INCLUDED_)
