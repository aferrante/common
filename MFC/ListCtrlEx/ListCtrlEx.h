#if !defined(AFX_LISTCTRLEX_H__001748BC_C5A0_4820_BB83_D172DA9AA336__INCLUDED_)
#define AFX_LISTCTRLEX_H__001748BC_C5A0_4820_BB83_D172DA9AA336__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ListCtrlEx.h : header file
//
#include "../../IMG/BMP/CBmpUtil_MFC.h"

#define MAX_IMAGES	128
#define MAX_COLUMNS 16
#define LCEX_PADDING 3
#define LCEX_TEXT				0x0001
#define LCEX_IMAGE			0x0002
#define LCEX_BGCLR			0x0004
#define LCEX_TXTCLR			0x0008
#define LCEX_SELBGCLR		0x0010
#define LCEX_SELTXTCLR	0x0020
#define LCEX_WIDTH			0x0040
#define LCEX_JUSTIFY		0x0080
#define LCEX_BMPOFFSET	0x0100
#define LCEX_TRANSCLR		0x0200

class CListCtrlExItemData
{
public:
	CListCtrlExItemData(int nNumColumns, COLORREF crTranscolor=CLR_INVALID, 
		COLORREF crText=CLR_INVALID, COLORREF crBG=CLR_INVALID) : 
	m_nNumColumns (nNumColumns), m_crTranscolor (crTranscolor)
	{
		for(int i=0;i<MAX_COLUMNS;i++)
		{
			m_crBG[i]=crBG;
			m_crText[i]=crText;
			m_crSelBG[i]=CLR_INVALID;
			m_crSelText[i]=CLR_INVALID;
			m_nJustify[i]=LVCFMT_LEFT;
			m_szText[i]="";
			m_hBmp[i]=NULL;
//			m_pBmp[i]=NULL;
			m_nWidth[i]=0;
			m_nBmpWidth[i]=0;
			m_dwUserData[i]=0;
			m_pvoidUserData[i]=NULL;
		}
		if(crTranscolor==CLR_INVALID)
		 m_crTranscolor=GetSysColor(COLOR_WINDOW);
		else
		 m_crTranscolor=crTranscolor; 
	}
	

	CSize GetSize(int nSubItem);
	BOOL SetItemData(int nSubItem, int nMask = LCEX_TEXT, int nWidth=15, CString szText="", HBITMAP hBmp=NULL, COLORREF crColor = CLR_INVALID, int nJustify=LVCFMT_LEFT, BOOL bSetAllSubitems=FALSE);
	void DrawItem(CDC* pDC, const CRect& rc, BOOL bSelected, BOOL bEnabled=TRUE);
	void FocusItem(CDC* pDC, const CRect& rc, BOOL bAddFocus);

	DWORD SetItemUserData(int nSubItem, DWORD dwData);
	void* SetItemUserDataPtr(int nSubItem, void* pvoidData);
	DWORD GetItemUserData(int nSubItem);
	void* GetItemUserDataPtr(int nSubItem);

protected:
	CBmpUtil m_bu;
	CString m_szText[MAX_COLUMNS];
	HBITMAP m_hBmp[MAX_COLUMNS];
//	CBitmap m_Bmp[MAX_COLUMNS];
	int m_nWidth[MAX_COLUMNS];
	int m_nBmpWidth[MAX_COLUMNS];
	int m_nJustify[MAX_COLUMNS];
	BITMAP m_bmp;
	CSize m_size;
	int m_nNumColumns;
	COLORREF m_crBG[MAX_COLUMNS];
	COLORREF m_crText[MAX_COLUMNS];
	COLORREF m_crSelBG[MAX_COLUMNS];
	COLORREF m_crSelText[MAX_COLUMNS];
	COLORREF m_crTranscolor;
	DWORD m_dwUserData[MAX_COLUMNS];
	void* m_pvoidUserData[MAX_COLUMNS];
};


/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx window

class CListCtrlEx : public CListCtrl
{
// Construction
public:
	CListCtrlEx(BOOL bDiffSizeImages=FALSE);

// Attributes
public:
	BOOL m_bDiffSizeImages;
	BOOL m_bWrapText;
	HBITMAP m_hbmpImages;
	CBitmap m_bmpImages;
	HBITMAP m_hBmp[MAX_IMAGES];
	int m_nImageWidth;
	COLORREF m_crTranscolor;
	int m_nNumColumns;
	int m_nJustify[MAX_COLUMNS];
	int m_nBitmapWidth[MAX_COLUMNS];

	CImageList m_img; // fake, just for height setting.  CListCtrlEx uses its own bitmap sets do not use CImageLists
	int m_nCellHeight;
	CBmpUtil m_bu;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListCtrlEx)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CListCtrlEx();
	void SetWrapText(BOOL bWrapText=TRUE);
	void SetItemBitmap(int nItem, int nSubItem, HBITMAP hbmp, BOOL bAllSubitems=FALSE);
	BOOL SetImages(UINT nBitmapID, int nWidth, COLORREF crTranscolor=CLR_INVALID);
	HBITMAP GetImage(int nIndex);
	int InsertColumn( int nCol, LPCTSTR lpszColumnHeading, int nFormat = LVCFMT_LEFT, int nWidth = -1, int nSubItem = -1 );
	BOOL DeleteColumn( int nCol );
	int InsertItem( int nItem, LPCTSTR lpszItem );
	int GetColumnFormat(int nCol);
	void SetItemColor(int nItem, int nSubItem, int nMask, COLORREF crColor, BOOL bAllSubitems=FALSE);
//	void DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct); 
	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) ;
	BOOL DeleteItem( int nItem );
	BOOL SetHeight(int nHeight);
	void AlignColumnItems(int nSubItem, BOOL bAllSubitems=FALSE);
	DWORD GetItemData( int nItem, int nSubItem=0 );
	DWORD SetItemData( int nItem, DWORD dwData, int nSubItem=0 );
	void* GetItemDataPtr( int nItem, int nSubItem=0 );
	void* SetItemDataPtr( int nItem, void* pvoidData, int nSubItem=0 );


	// Generated message map functions
protected:
	//{{AFX_MSG(CListCtrlEx)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTCTRLEX_H__001748BC_C5A0_4820_BB83_D172DA9AA336__INCLUDED_)
