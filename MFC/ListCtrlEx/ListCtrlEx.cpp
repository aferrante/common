// ListCtrlEx.cpp : implementation file
//

#include "stdafx.h"
#include "ListCtrlEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSize CListCtrlExItemData::GetSize(int nSubItem)
{
		//.. need to put font measurement in here.
	if(m_hBmp[nSubItem]==NULL)
	{
		m_size.cx = 0;
		m_size.cy = 0;
	}
	else
	{
		GetObject(m_hBmp[nSubItem],sizeof(BITMAP), (LPSTR)&m_bmp);
		m_size.cx = m_bmp.bmWidth;
		m_size.cy = m_bmp.bmHeight;
	}
	return m_size;
}
void CListCtrlExItemData::DrawItem(CDC* pDC, const CRect& rc, BOOL bSelected, BOOL bEnabled)
{
	COLORREF crBG, crText;
	if(bSelected)
	{
		if(m_crSelBG[0]==CLR_INVALID) crBG=GetSysColor(COLOR_HIGHLIGHT);
		else crBG=m_crSelBG[0];
	}
	else
	{
		if(m_crBG[0]==CLR_INVALID) crBG=GetSysColor(COLOR_WINDOW);
		else crBG=m_crBG[0];
	}

	pDC->SetBkColor(crBG);
	ExtTextOut(pDC->GetSafeHdc(),0,0, ETO_OPAQUE, rc, NULL,0,NULL);
	pDC->SetBkMode(TRANSPARENT);

///	AfxMessageBox("foo");
//		CString foo; foo.Format("rc:%d,%d,%d,%d",rc.left,rc.top,rc.right,rc.bottom);
//		AfxMessageBox(foo);
	CRect rect(rc);
	int i=0;
	int nLeft=rc.left;
//	foo.Format("nleft%d",nLeft); AfxMessageBox(foo);
	while (i<m_nNumColumns)
	{
//		CString foo; foo.Format("i%d,nc%d",i,m_nNumColumns);
//	AfxMessageBox(foo);
		
		rect.left = nLeft;
//	CString foo;foo.Format("m_nWidth[i]:%d",m_nWidth[i]); AfxMessageBox(foo);
		rect.right = rect.left+m_nWidth[i];
		if(bSelected)
		{
			if(m_crSelBG[i]==CLR_INVALID) crBG=GetSysColor(COLOR_HIGHLIGHT);
			else crBG=m_crSelBG[i];
			if(m_crSelText[i]==CLR_INVALID) crText=GetSysColor(COLOR_HIGHLIGHTTEXT);
			else crText=m_crSelText[i];
		}
		else
		{
			if(m_crBG[i]==CLR_INVALID) crBG=GetSysColor(COLOR_WINDOW);
			else crBG=m_crBG[i];
			if(!bEnabled) crBG=GetSysColor(COLOR_3DFACE);

			if(m_crText[i]==CLR_INVALID) crText=GetSysColor(COLOR_WINDOWTEXT);
			else crText=m_crText[i];
		}
		pDC->SetBkColor(crBG);

// need the following for subitems different colors from item,		
		ExtTextOut(pDC->GetSafeHdc(),0,0, ETO_OPAQUE, rect, NULL,0,NULL);

		if(m_hBmp[i]!=NULL)
		{
			CSize size = GetSize(i);
			if(m_crTranscolor==CLR_INVALID)
			{
				HDC hdcTemp = CreateCompatibleDC(pDC->GetSafeHdc());
				SetMapMode(hdcTemp, GetMapMode(pDC->GetSafeHdc()));

				BITMAP bm;
				GetObject(m_hBmp[i], sizeof(BITMAP), (LPSTR)&bm);
				SelectObject(hdcTemp, m_hBmp[i]);   // Select the bitmap

				BitBlt(pDC->GetSafeHdc(), 
					rect.left+LCEX_PADDING,rect.top+(rect.Height()-size.cy)/2, 
					bm.bmWidth, bm.bmWidth, 
					hdcTemp, 0, 0, SRCCOPY);
				// clean it up.
				DeleteDC(hdcTemp);
			}
			else
				m_bu.TransparentBitmap(pDC->GetSafeHdc(), m_hBmp[i], m_crTranscolor, CPoint(rect.left+LCEX_PADDING,rect.top+(rect.Height()-size.cy)/2) );
		}
		if(m_nBmpWidth[i])
		{
			rect.left+=(m_nBmpWidth[i]+LCEX_PADDING);
		}
		UINT nJustify;
		if (m_nJustify[i]==LVCFMT_LEFT) nJustify =DT_LEFT;
		if (m_nJustify[i]==LVCFMT_RIGHT) nJustify =DT_RIGHT;
		if (m_nJustify[i]==LVCFMT_CENTER) nJustify =DT_CENTER;

//		pDC->SetBkMode(OPAQUE);
		pDC->SetTextColor(crText);
//		AfxMessageBox(m_szText[i]);
//		CString foo; foo.Format("%d,%d,%d,%d",rect.left,rect.top,rect.right,rect.bottom);
//		AfxMessageBox(foo);
		rect.left+=LCEX_PADDING; rect.right-=LCEX_PADDING; 
		if (pDC->GetTextExtent(m_szText[i]).cx<rect.Width())
			pDC->DrawText(m_szText[i], rect, DT_SINGLELINE|DT_VCENTER|DT_NOPREFIX|nJustify);
		else
		{
			int nNumChar=m_szText[i].GetLength()-1;
			CString szEllipsis = _T("...");
			CString szOutText = m_szText[i].Left(nNumChar)+szEllipsis;
			BOOL bVisible=TRUE;
			if(rect.Width()<=0) bVisible=FALSE;
			while((pDC->GetTextExtent(szOutText).cx>=rect.Width())&&(bVisible))
			{
				nNumChar--;
				szOutText = m_szText[i].Left(nNumChar)+szEllipsis;
				if (nNumChar==0) bVisible=FALSE;
			}
			pDC->DrawText(szOutText, rect, DT_SINGLELINE|DT_VCENTER|DT_NOPREFIX|nJustify);

		}
		nLeft+=m_nWidth[i];
		i++;
	}

}
void CListCtrlExItemData::FocusItem(CDC* pDC, const CRect& rc, BOOL bAddFocus)
{
	pDC->DrawFocusRect(rc);
}

BOOL CListCtrlExItemData::SetItemData(int nSubItem, int nMask, int nWidth, CString szText, HBITMAP hBmp, COLORREF crColor, int nJustify, BOOL bSetAllSubitems)
{
//CString gorp; gorp.Format("Numcol:%d, Width:%d",m_nNumColumns,nWidth);
//AfxMessageBox(gorp);
	if(bSetAllSubitems)
	{
		nSubItem=0;
	}

	if((nSubItem>=m_nNumColumns)||(nSubItem<0)) return FALSE;
	while(nSubItem<m_nNumColumns)
	{
		if(nMask&LCEX_JUSTIFY)
		{
			m_nJustify[nSubItem] = nJustify;
		}
		if(nMask&LCEX_TEXT)
		{
			m_szText[nSubItem] = szText;
		}
		if(nMask&LCEX_WIDTH)
		{
	//CString gorp; gorp.Format("Setting m_nWidth[%d] to Width:%d",nSubItem,nWidth);
	//AfxMessageBox(gorp);	
			if(nWidth>=0) m_nWidth[nSubItem] = nWidth;
		}
		if(nMask&LCEX_BMPOFFSET)
		{
			m_nBmpWidth[nSubItem] = nWidth;
		}

	//AfxMessageBox("gorp");	
		
		if(nMask&LCEX_IMAGE)
		{
/*			CBitmap bmp;
//			CBitmap* pBmp;
			BITMAP bm;

			GetObject(hBmp,sizeof(BITMAP), &bm);

			m_Bmp[nSubItem].DeleteObject();
			m_Bmp[nSubItem].CreateBitmapIndirect(&bm);
			BYTE* buffer = new BYTE[bm.bmWidthBytes*bm.bmHeight];
			bmp.FromHandle(hBmp)->GetBitmapBits(bm.bmWidthBytes*bm.bmHeight, buffer);
//			CString foo; foo.Format("%s",buffer);
//			AfxMessageBox(foo);
			m_Bmp[nSubItem].SetBitmapBits(bm.bmWidthBytes*bm.bmHeight, buffer);

			DeleteObject(SelectObject(GetDC(this)->GetSafeHdc(), hBmp));
*/
//				pBmp=bmp.FromHandle(hBmp);
			m_hBmp[nSubItem]=hBmp;
		}
		if(nMask&LCEX_BGCLR)
		{
			if(crColor==CLR_INVALID)
			{
				if(m_crBG[0]==CLR_INVALID)
					m_crBG[nSubItem] = GetSysColor(COLOR_WINDOW);
				else
					m_crBG[nSubItem] = m_crBG[0];
			}	
			else
				m_crBG[nSubItem] = crColor;
		}
		if(nMask&LCEX_TXTCLR)
		{
			if(crColor==CLR_INVALID)
			{
				if(m_crText[0]==CLR_INVALID)
					m_crText[nSubItem] = GetSysColor(COLOR_WINDOWTEXT);
				else
					m_crText[nSubItem] = m_crText[0];
			}	
			else
				m_crText[nSubItem] = crColor;
		}
		if(nMask&LCEX_SELBGCLR)
		{
			if(crColor==CLR_INVALID)
			{
				if(m_crSelBG[0]==CLR_INVALID)
					m_crSelBG[nSubItem] = GetSysColor(COLOR_HIGHLIGHT);
				else
					m_crSelBG[nSubItem] = m_crSelBG[0];
			}	
			else
				m_crSelBG[nSubItem] = crColor;
		}
		if(nMask&LCEX_SELTXTCLR)
		{
			if(crColor==CLR_INVALID)
			{
				if(m_crSelText[0]==CLR_INVALID)
					m_crSelText[nSubItem] = GetSysColor(COLOR_HIGHLIGHTTEXT);
				else
					m_crSelText[nSubItem] = m_crSelText[0];
			}	
			else
				m_crSelText[nSubItem] = crColor;
		}
		if(nMask&LCEX_TRANSCLR)
		{
			m_crTranscolor = crColor;
		}
		

		if(bSetAllSubitems) nSubItem++;  //increment thru all.
		else nSubItem=m_nNumColumns; // just hit the one.

	}
//AfxMessageBox("gorp1");	

	return TRUE;
}

DWORD CListCtrlExItemData::SetItemUserData(int nSubItem, DWORD dwData)
{
	DWORD dwRV = 0;
	if((nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return dwRV;
	dwRV = m_dwUserData[nSubItem];
	m_dwUserData[nSubItem] = dwData;
	return dwRV;
}

void* CListCtrlExItemData::SetItemUserDataPtr(int nSubItem, void* pvoidData)
{
	void* pvoidRV = NULL;
	if((nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return pvoidRV;
	pvoidRV = m_pvoidUserData[nSubItem];
	m_pvoidUserData[nSubItem] = pvoidData;
	return pvoidRV;
}

DWORD CListCtrlExItemData::GetItemUserData(int nSubItem)
{
	DWORD dwRV = 0;
	if((nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return dwRV;
	dwRV = m_dwUserData[nSubItem];
	return dwRV;
}

void* CListCtrlExItemData::GetItemUserDataPtr(int nSubItem)
{
	void* pvoidRV = NULL;
	if((nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return pvoidRV;
	pvoidRV = m_pvoidUserData[nSubItem];
	return pvoidRV;
}


/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx

CListCtrlEx::CListCtrlEx(BOOL bDiffSizeImages)
{
	m_bWrapText=FALSE;
	m_bDiffSizeImages = bDiffSizeImages;
	m_nNumColumns=0;
	m_crTranscolor=CLR_INVALID;
	for (int i=0;i<MAX_COLUMNS ;i++)
	{
		m_nJustify[i]=LVCFMT_LEFT;
		m_nBitmapWidth[i]=0;
	}
	for(i=0;i<MAX_IMAGES;i++)
	{
		m_hBmp[i]=NULL;
	}

}

CListCtrlEx::~CListCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CListCtrlEx, CListCtrl)
	//{{AFX_MSG_MAP(CListCtrlEx)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListCtrlEx message handlers

/*
void CListCtrlEx::DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct) 
{
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(lpDeleteItemStruct->itemID));
//	CListCtrlExItemData *pData = (CListCtrlExItemData*) (lpDeleteItemStruct->itemData);
	ASSERT(pData);
	delete pData;
	
//	CListCtrl::OnDeleteItem(nIDCtl, lpDeleteItemStruct);
}
*/

BOOL CListCtrlEx::DeleteItem( int nItem )
{
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return FALSE;
	ASSERT(pData);
	BOOL bReturn = CListCtrl::DeleteItem(  nItem );
	if(bReturn)	delete pData;
	return bReturn;

}

void CListCtrlEx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
/*	
	CString foo; 
	CString *psz= (CString*)lpDrawItemStruct->itemData;

	foo.Format("CtlID:%d\nCtlType:0x%x\nItemID:%d\nitemAction:%d\nitemState:%d\nrcItem:%d,%d,%d,%d\nItemData:%d, %s",
		lpDrawItemStruct->CtlID, 
		lpDrawItemStruct->CtlType,
		lpDrawItemStruct->itemID, 
		lpDrawItemStruct->itemAction,
		lpDrawItemStruct->itemState,
		lpDrawItemStruct->rcItem.left,
		lpDrawItemStruct->rcItem.top,
		lpDrawItemStruct->rcItem.right,
		lpDrawItemStruct->rcItem.bottom,
		lpDrawItemStruct->itemData,
		psz
		);

	AfxMessageBox(foo);
	*/

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(lpDrawItemStruct->itemID));
	if (pData==NULL) return;
//	CListCtrlExItemData *pData = (CListCtrlExItemData*) (lpDrawItemStruct->itemData);
	ASSERT(pData);

	if(lpDrawItemStruct->itemID==LB_ERR) return;
	CRect rc=lpDrawItemStruct->rcItem;
	if(lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE|ODA_SELECT)) 
	{
		int i=0;
		while (i<m_nNumColumns)
		{
			pData->SetItemData(i,LCEX_TEXT|LCEX_WIDTH,GetColumnWidth(i),
				GetItemText(lpDrawItemStruct->itemID, i));
			pData->SetItemData(i,LCEX_TRANSCLR,NULL,"",NULL,m_crTranscolor);
			i++;
		}
		pData->DrawItem(pDC,lpDrawItemStruct->rcItem, lpDrawItemStruct->itemState&ODS_SELECTED, IsWindowEnabled());
	}
//	AfxMessageBox("foo2");
	if(lpDrawItemStruct->itemAction & (ODA_FOCUS)) 
	{
		pData->FocusItem(pDC,lpDrawItemStruct->rcItem, lpDrawItemStruct->itemState&ODS_FOCUS);
	}
}

 
void CListCtrlEx::SetWrapText(BOOL bWrapText)
{
	m_bWrapText=bWrapText;
}

void CListCtrlEx::SetItemBitmap(int nItem, int nSubItem, HBITMAP hbmp, BOOL bAllSubitems)
{
//	CString foo; foo.Format("%d",hbmp);AfxMessageBox(foo);
	if ((nItem<0)||(nSubItem<0)||(nSubItem>=MAX_COLUMNS)||(hbmp==NULL)) return;
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return;
	ASSERT(pData);
	pData->SetItemData(nSubItem,LCEX_IMAGE,NULL,"", hbmp,NULL,NULL,bAllSubitems);

	if(m_bDiffSizeImages)
	{
	// following resizes and aligns text with differently sized icons, but at a huge processing cost.
	// only use if you have differently sized bmps
		pData->SetItemData(nSubItem,LCEX_BMPOFFSET,m_nBitmapWidth[nSubItem]);
		AlignColumnItems( nSubItem,  bAllSubitems);
	}
	else
	{
	// the following only gets bigger. can't get smaller if you remove items with larger bmps.
		// doesn;t re-align all the past ones
		// perfect for image lists, and no big processing cost
		m_nBitmapWidth[nSubItem]=pData->GetSize(nSubItem).cx;
		pData->SetItemData(nSubItem,LCEX_BMPOFFSET,m_nBitmapWidth[nSubItem],"",NULL,NULL,NULL,bAllSubitems);
	}
}

void CListCtrlEx::SetItemColor(int nItem, int nSubItem, int nMask, COLORREF crColor, BOOL bAllSubitems)
{
	if ((nItem<0)||(nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return;
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return;
	ASSERT(pData); 
	pData->SetItemData(nSubItem,nMask,NULL,"", NULL, crColor,NULL,bAllSubitems);
	
}

BOOL CListCtrlEx::SetImages(UINT nBitmapID, int nWidth, COLORREF crTranscolor)
{
// doesnt de-allocate the old images ...yet
	if(!m_bmpImages.LoadBitmap(nBitmapID)) {m_bmpImages.DeleteObject(); return FALSE;}
	BITMAP bm;
	m_bmpImages.GetObject(sizeof(BITMAP), &bm);
//	CString foo; foo.Format("image width:%d, extract image width:%d",bm.bmWidth,nWidth);AfxMessageBox(foo);
	if ((bm.bmWidth<nWidth)||(nWidth<0))  {m_bmpImages.DeleteObject(); return FALSE;}
	m_hbmpImages = HBITMAP(m_bmpImages.GetSafeHandle());
	m_crTranscolor=crTranscolor;  // if CLR_INVALID, doesnt use transparency
	m_nImageWidth=nWidth;

	for(int i=0;i<(bm.bmWidth/nWidth);i++)
	{
		m_hBmp[i] = m_bu.SubsetBitmap(
			GetDC()->GetSafeHdc(), 
			HBITMAP(m_bmpImages),
			CRect(
				CPoint(m_nImageWidth*(i),0),
				CSize(m_nImageWidth, bm.bmHeight)
				)
			);
	}

	m_bDiffSizeImages=FALSE; // all the same when using SetImages.
	return TRUE;
}

HBITMAP CListCtrlEx::GetImage(int nIndex)
{
	return m_hBmp[nIndex];
}

int CListCtrlEx::InsertColumn( int nCol, LPCTSTR lpszColumnHeading, int nFormat, int nWidth, int nSubItem )
{
	int nColIdx = CListCtrl::InsertColumn(nCol, lpszColumnHeading, nFormat, nWidth, nSubItem );
	if(nColIdx>-1) m_nNumColumns++;
	m_nJustify[nCol]=nFormat;

	return nColIdx;
}

BOOL CListCtrlEx::DeleteColumn( int nCol )
{
	BOOL bDeleted = CListCtrl::DeleteColumn( nCol );
	if (bDeleted) m_nNumColumns--;
	return bDeleted;
}

int CListCtrlEx::InsertItem( int nItem, LPCTSTR lpszItem )
{
	CListCtrlExItemData* pData = new CListCtrlExItemData(m_nNumColumns,m_crTranscolor, GetTextColor(),GetBkColor());
//CString foo; foo.Format("col w:%d",GetColumnWidth(0));
//AfxMessageBox(foo);
	ASSERT(pData);
	pData->SetItemData(0, LCEX_TEXT|LCEX_WIDTH|LCEX_JUSTIFY, 
		GetColumnWidth(0), lpszItem, NULL, CLR_INVALID, GetColumnFormat(0));
	int nRV = CListCtrl::InsertItem( nItem, lpszItem); 
//	int nRV = CListCtrl::InsertItem( nItem, (LPCTSTR)pData ); 
	if (nRV ==LB_ERR) delete pData;
	else
	{
		CListCtrl::SetItemData(nItem, (DWORD)pData);
		for (int i=0; i<m_nNumColumns; i++)  // used to be just col 0
			pData->SetItemData(i,LCEX_BMPOFFSET,m_nBitmapWidth[i]);
	}
	return nRV;
}

int CListCtrlEx::GetColumnFormat(int nCol)
{
//	LV_COLUMN lvc;
//	lvc.mask=LVCF_FMT ; 
//	GetColumn(nCol, &lvc) ;
//	CString foo; foo.Format("colfmt:%d",lvc.fmt);AfxMessageBox(foo);
//  return lvc.fmt;
	return m_nJustify[nCol];
}

BOOL CListCtrlEx::SetHeight(int nHeight)
{
// windows seems to add a one-pixel high line between each cell in the list ctrl,
// so separation item to item is nHeight+1 pixels.
	CImageList m_img; // fake, just for height setting.  CListCtrlEx uses its own bitmap sets do not use CImageLists
	BOOL bRV = m_img.Create( 1,nHeight, ILC_COLOR24 , 0,1 );
	if (bRV)
	{
		SetImageList(&m_img, LVSIL_STATE);
		m_nCellHeight=nHeight;
	}
	return bRV;
}

void  CListCtrlEx::AlignColumnItems(int nSubItem, BOOL bAllSubitems)
{
	// following resizes and aligns text with differently sized icons, but at a huge processing cost.
	// only use if you have differently sized bmps
	int nMin = nSubItem, nMax=nSubItem+1;
	if (bAllSubitems) {nMin = 0, nMax = m_nNumColumns;}

	for (int r=nMin; r<nMax; r++)
	{
		int i=0;
		int nOffset=m_nBitmapWidth[nSubItem];
		BOOL bFoundDifferent=FALSE;
		m_nBitmapWidth[nSubItem]=0;
		while(i<GetItemCount())
		{
			CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(i));
			CSize size=pData->GetSize(nSubItem);
			if(size.cx!=nOffset) bFoundDifferent|=TRUE;
			if(size.cx>m_nBitmapWidth[nSubItem])
			{
				m_nBitmapWidth[nSubItem]=size.cx;
			}
			i++;
		}
		if(bFoundDifferent)
		{
			i=0;
			while(i<GetItemCount())
			{
				CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(i));
				pData->SetItemData(nSubItem,LCEX_BMPOFFSET,m_nBitmapWidth[nSubItem]);
				i++;
			}
		}
	}
}


//This function retrieves a 32-bit application-specific value associated 
//with the item specified by nItem and nSubItem.  Up to MAX_COLUMNS (16)
// values may be associated with each item.  the nSubItem value is an index
// into the array of 16 DWORDs, and are not strictly associated with the subitem,
// meaning you can use DWORD # 15 without having 16 columns, for instance.
DWORD CListCtrlEx::GetItemData( int nItem, int nSubItem )
{
	if ((nItem<0)||(nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return 0;
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return 0;
	ASSERT(pData);
	return pData->GetItemUserData(nSubItem);
}

//This function sets a 32-bit application-specific value associated 
//with the item specified by nItem and nSubItem.  Up to MAX_COLUMNS (16)
// values may be associated with each item.  the nSubItem value is an index
// into the array of 16 DWORDs, and are not strictly associated with the subitem,
// meaning you can use DWORD # 15 without having 16 columns, for instance.
// The function returns the previous value.
DWORD CListCtrlEx::SetItemData( int nItem, DWORD dwData, int nSubItem )
{
	if ((nItem<0)||(nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return 0;
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return 0;
	ASSERT(pData);
	return pData->SetItemUserData(nSubItem, dwData);
}

//This function retrieves an application-supplied 32-bit value associated with 
// the specified list-box item (specified by nItem and nSubItem) as a pointer (void*).
// Up to MAX_COLUMNS (16) values may be associated with each item.  
// The nSubItem value is an index into the array of 16 void*s, and are not 
// strictly associated with the subitem, meaning you can use void* # 15 without 
// having 16 columns, for instance.  
void* CListCtrlEx::GetItemDataPtr( int nItem, int nSubItem )
{
	if ((nItem<0)||(nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return NULL;
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return NULL;
	ASSERT(pData);
	return pData->GetItemUserDataPtr(nSubItem);
}

//This function sets an application-supplied 32-bit value associated with 
// the specified list-box item (specified by nItem and nSubItem) as a pointer (void*).
// Up to MAX_COLUMNS (16) values may be associated with each item.  
// The nSubItem value is an index into the array of 16 void*s, and are not 
// strictly associated with the subitem, meaning you can use void* # 15 without 
// having 16 columns, for instance.  The function returns the previous value.
void* CListCtrlEx::SetItemDataPtr( int nItem, void* pvoidData, int nSubItem )
{
	if ((nItem<0)||(nSubItem<0)||(nSubItem>=MAX_COLUMNS)) return NULL;
	CListCtrlExItemData *pData = (CListCtrlExItemData*) (CListCtrl::GetItemData(nItem));
	if (pData==NULL) return NULL;
	ASSERT(pData);
	return pData->SetItemUserDataPtr(nSubItem, pvoidData);
}

