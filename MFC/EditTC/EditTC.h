// EditTC.h: interface for the CEditTC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EDITTC_H__FCD0024D_5EF1_4D09_B196_C826FBF22A24__INCLUDED_)
#define AFX_EDITTC_H__FCD0024D_5EF1_4D09_B196_C826FBF22A24__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// NOTE: this control has all time params in descending order, year-month-day-hour-min-sec-(ms/frame/field) (AM/PM)

#define EDTC_DO_SHOWCALDATE		0x00000001  // display options - show calendar date (global, as opposed to show time only)
#define EDTC_DO_SHOWTIME			0x00000002  // display options - show HH:MM:SS.(FF) time (global, as opposed to show calendardate only)
#define EDTC_DO_SHOWUNIX			0x00000003  // display options - show unix time
#define EDTC_DO_SHOWJULIAN		0x00000004  // display options - show julian day, date min, sec, etc, one other define must be ORed to use this, and an offset must be set - defaults to jan 1 1900, 00:00:00.0
#define EDTC_DO_SHOWUSER			0x00000005  // display options - show user time format (requires callback pointer)
#define EDTC_DO_SHOWMASK			0x00000007  // display options mask

//date
#define EDTC_DO_SHOWYEAR			0x00000010  // display options - show year (4 digits)
#define EDTC_DO_SHOWMONTH			0x00000020  // display options - show numeric format month (2 digit)
#define EDTC_DO_SHOWTMONTH		0x00000040  // display options - show text format month (3 chars)
#define EDTC_DO_SHOWDATE			0x00000080  // display options - show date (2 digits)
#define EDTC_DO_SHOWTDAY			0x00000100  // display options - show text format day of week (3-5 chars)
#define EDTC_DO_SHOWTDAYENC		0x00000200  // display options - encapsulate day of week in paired chars like [Wed] or (Wed)

//time
#define EDTC_DO_SHOW12HOUR		0x00001000  // display options - show hour in 12hr format (2 digits)
#define EDTC_DO_SHOW24HOUR		0x00002000  // display options - show hour in 24hr format (2 digits)
#define EDTC_DO_SHOWMINUTE		0x00004000  // display options - show numeric format month (2 digit)
#define EDTC_DO_SHOWSECOND		0x00008000  // display options - show seconds (2 digits)
#define EDTC_DO_SHOWMS				0x00010000  // display options - show milliseconds (3 digits)
#define EDTC_DO_SHOWFRAMES		0x00020000  // display options - show frames (2 digits)
#define EDTC_DO_SHOWFIELDS		0x00040000  // display options - show fields (2 digits)
#define EDTC_DO_SHOWAMPM			0x00080000  // display options - show am or pm (2 digits)
#define EDTC_DO_SHOWAMPMENC		0x00100000  // display options - show am or pm (2 digits)
#define EDTC_DO_SHOWUTC				0x00200000  // display options - show UTC (local is default)

// numerical
#define EDTC_DO_TOTAL					0x01000000  // display options - show total largest denom...  as in 789:31.29  789 mins, 31 secs, 29 frames, else mod, as in 09:31.29  = (13 hrs - not shown), 9 mins, 31 secs, 29 frames
#define EDTC_DO_ROUND					0x02000000  // display options - round smallest shown denom (otherwise truncate). as in 12:10.29 -> 12:11

// alert
#define EDTC_DO_ALERT					0x10000000  // display options - alert with color if error condition
#define EDTC_DO_EXPECTSFUTURE	0x20000000  // display options - indicates expected condition if set
#define EDTC_DO_EXPECTSPAST		0x40000000  // display options - indicates expected condition if set


#define EDTC_DEFSETUP_DATE			EDTC_DO_SHOWCALDATE|EDTC_DO_SHOWYEAR|EDTC_DO_SHOWTMONTH|EDTC_DO_SHOWDATE|EDTC_CTRL_NTSC
#define EDTC_DEFSETUP_TIME			EDTC_DO_SHOW24HOUR|EDTC_DO_SHOWMINUTE|EDTC_DO_SHOWSECOND|EDTC_CTRL_NTSC
#define EDTC_DEFSETUP_TC				EDTC_DEFSETUP_TIME|EDTC_DO_SHOWFRAMES|EDTC_CTRL_NTSC
#define EDTC_DEFSETUP_DATETIME	EDTC_DEFSETUP_DATE|EDTC_DEFSETUP_TIME

#define EDTC_CTRL_ED					0x04000000  // control options - use an MFC Edit control
#define EDTC_CTRL_RE					0x08000000  // control options - use an MFC RichEdit control
#define EDTC_CTRL_NTSC				0x00400000  // control options - NTSC
#define EDTC_CTRL_PAL					0x00800000  // control options - PAL


// delimiters precede the thing, so, m_chDelimMonth preceds month.
#define EDTC_DEFDELIM_MONTH		'-'
#define EDTC_DEFDELIM_DATE		'-'
#define EDTC_DEFDELIM_DAY			' ' // Wed, etc  // if delim any of [{(<"' must append ]})>"'
#define EDTC_DEFDELIM_HOUR    ' '
#define EDTC_DEFDELIM_MINUTE	':'
#define EDTC_DEFDELIM_SECOND	':'
#define EDTC_DEFDELIM_SUBSEC	'.'
#define EDTC_DEFDELIM_AMPM		' '// am or pm.  // if delim any of [{(<"' must append ]})>"'
#define EDTC_DEFDELIM_COMMENT 0

#define EDTC_MAXFIELDS				20
#define EDTC_MAXFIELDWIDTH		16  //4294967295 is the larget 32 bit number, 6311520000000 is the number of milliseconds in 200 years.  delims are always one char.
#define EDTC_MAXCOMMENTWIDTH	80  //max comment length.  delims are always one char.

#define EDTC_TYPE_YEAR					0x00
#define EDTC_TYPE_DELIM_MON			0x01 
#define EDTC_TYPE_MONTH					0x02  
#define EDTC_TYPE_DELIM_DATE		0x03  
#define EDTC_TYPE_DATE					0x04  
#define EDTC_TYPE_DELIM_DAY			0x05  
#define EDTC_TYPE_DAY						0x06  
#define EDTC_TYPE_DELIM_HOUR		0x07  
#define EDTC_TYPE_HOUR					0x08  
#define EDTC_TYPE_DELIM_MIN			0x09  
#define EDTC_TYPE_MIN						0x0a  
#define EDTC_TYPE_DELIM_SEC			0x0b  
#define EDTC_TYPE_SEC						0x0c  
#define EDTC_TYPE_DELIM_SUBSEC	0x0d  
#define EDTC_TYPE_SUBSEC				0x0e  
#define EDTC_TYPE_DELIM_AMPM		0x0f  
#define EDTC_TYPE_AMPM					0x10  
#define EDTC_TYPE_DELIM_COMMENT	0x11  
#define EDTC_TYPE_COMMENT				0x12

#define EDTC_TYPE_JULIAN				0x80000000

#define EDTC_JULIAN_NEVER				0x00000000
#define EDTC_JULIAN_FOREVER			0xffffffff

#define EDTC_COMMENT_NEVER			"(never)"
#define EDTC_COMMENT_NOTUSED		"(not used)"
#define EDTC_COMMENT_NA					"(n/a)"
#define EDTC_COMMENT_FOREVER		"(forever)"
#define EDTC_COMMENT_NOEXP			"(no expiry)"


#define EDTC_SUCCESS	0 
#define EDTC_ERROR		-1
#define EDTC_UNKNOWN	-2


typedef struct
{
//	char chType;
	unsigned long  ulValue;			// numerical value
	unsigned long  ulMinValue;  // minimum numerical value // min char range if set
	unsigned long  ulMaxValue;  // maximum numerical value // max char range if set
	char* pszChars;							// for char types, a string.
//	char* pszAllowedChars;      // if not null, overrides min max for char types
	COLORREF crEditColor;				// if error, overrides normal font color in the charformat while editing
	CHARFORMAT cf;							// font characteristics //RichEdit only
} Field_t;


class CEditTC			// : public CRichEditCtrl  
{
public:
	unsigned long m_ulFlags;

	Field_t m_Fields[EDTC_MAXFIELDS];  // an array of fields. // last is a null pointer

	double m_dblDateTime;	// from Dec 30, 1899 by default
	unsigned long	 m_ulRefJulianDate; // from January 1, 1900 by default
	unsigned long  m_ulRefTimeMS;			// can mod 1000 this time to get the millisecond complement to the unix time (below)
	unsigned long  m_ulRefUnixTime;

	COLORREF m_crBGColor;						// default background color
	COLORREF m_crErrorBGColor;			// if error, overrides background color

	int m_nErrorIntervalMS;  // milliseconds to alternate error color (flashing) <=0 indicates no error

	CWnd* m_pWnd;  // pointer to edit or richedit control

	CString m_szLastOutput;
	bool m_bError;

public:
	CEditTC();
	virtual ~CEditTC();

	void SetWnd(CWnd* pWnd, bool bRichEdit);  // set pointer to edit or richedit control

	int SetTime(double dblDateTime); // see cpp file for notes on usage
	int SetTime(unsigned long ulMajorCounter, unsigned long ulMilliseconds); // see cpp file for notes on usage
	int SetField(int nFieldIndex, char* pchNewValue, unsigned long ulNewValue); // see cpp file for notes on usage
	int ConvertType(unsigned long ulFlags);			// change the view
	int PosToField(unsigned long ulPosition);		// character position
	int FieldToPos(unsigned long ulField);			// field index
	int Validate(int nFieldIndex, char* pchNewValue, unsigned long ulNewValue);  // either int or char*, not both (one always null)
	int OutputData(bool bSuppressError=false);				// set the text.
	int AnalyzeData(CString szText);			// analyze any new text, called by EN_CHANGE handler

// nned to set up the following to allow the EN_CHANGE handler for rich edit. (put in OnInitDialog of the dialog containing the control)
//		((CRichEditCtrl*)GetDlgItem(IDC_RICHEDIT1))->SetEventMask(
//		ENM_CHANGE | ENM_UPDATE | ENM_SCROLL | ENM_KEYEVENTS | 
//		ENM_MOUSEEVENTS | ENM_REQUESTRESIZE | ENM_SELCHANGE);


// RichEdit
 	int SetColor(unsigned long ulFieldIndices);		// change font color of a field ORed. FFFFFFFF = all
	int SetBGColor(COLORREF cr);			// change the normal bg color of control
	int SetErrorColor(COLORREF cr);			// change the error bg color of control
	int SetError(bool bError=true);			// set the error condition

};

#endif // !defined(AFX_EDITTC_H__FCD0024D_5EF1_4D09_B196_C826FBF22A24__INCLUDED_)
