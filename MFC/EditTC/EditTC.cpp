// EditTC.cpp: implementation of the CEditTC class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EditTC.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


UINT ErrorTimer( LPVOID pParam );

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEditTC::CEditTC()
{
	m_ulFlags = EDTC_DEFSETUP_TC;  // have to pick one...

	m_Fields[EDTC_MAXFIELDS];  // an array of fields. 
	for(int i=0; i<EDTC_TYPE_COMMENT; i++)
	{
		m_Fields[i].pszChars = NULL;
	}

	m_dblDateTime = 0.0;
	m_ulRefJulianDate = 0;
	m_ulRefTimeMS			= 0;
	m_ulRefUnixTime		= 0;

	m_crBGColor = CLR_INVALID;					// default background color
	m_crErrorBGColor = RGB(192,0,0);		// if error, overrides background color

	m_pWnd = NULL;  // pointer to edit or richedit control

	m_nErrorIntervalMS = -1;  // dont flash
	m_szLastOutput = "";
	m_bError = false;
}

CEditTC::~CEditTC()
{
	for(int i=0; i<EDTC_TYPE_COMMENT; i++)
	{
		if(m_Fields[i].pszChars) free(m_Fields[i].pszChars);
	}
}

int CEditTC::ConvertType(unsigned long ulFlags)			// change the view
{
	if(m_ulFlags!=ulFlags) return EDTC_SUCCESS;  // no need to do anything.

	m_ulFlags = ulFlags;
	return OutputData();  // redraw based on new type.
}

void CEditTC::SetWnd(CWnd* pWnd, bool bRichEdit)  // set pointer to edit or richedit control
{
	if(pWnd)
	{
		m_pWnd = pWnd;
		if(bRichEdit)
		{
			m_ulFlags |= EDTC_CTRL_RE;
			m_ulFlags &= ~EDTC_CTRL_ED;
		}
		else 
		{
			m_ulFlags |= EDTC_CTRL_ED;
			m_ulFlags &= ~EDTC_CTRL_RE;
		}

//AfxMessageBox("X");
		// initialize the control
		for(int i=0; i<EDTC_TYPE_COMMENT; i++)
		{
	//		m_Fields[i].chType = (char)i;  // no need, just use i
			if(m_Fields[i].pszChars) free(m_Fields[i].pszChars); 


			//RichEdit...
			if(m_ulFlags&EDTC_CTRL_RE)
			{
				m_Fields[i].crEditColor = RGB(0,0,255);	  //RichEdit only
				((CRichEditCtrl*)pWnd)->GetDefaultCharFormat(m_Fields[i].cf); // font characteristics //RichEdit only
			}
			switch(i)
			{
			case EDTC_TYPE_YEAR:					//0x00
				{
					m_Fields[i].ulValue = 1900;			// numerical value
					m_Fields[i].ulMinValue = 1900;  // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 2099;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "1900"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_MON:			//0x01 
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_MONTH;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_MONTH:					//0x02  
				{
					m_Fields[i].ulValue = 1; 		
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_ulFlags&EDTC_DO_SHOWTMONTH)
					{
						if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "Jan"); // for char types, a string.
					}
					else
					{
						if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "01"); // for char types, a string.
					}
					m_Fields[i].ulMinValue = 1; 
					m_Fields[i].ulMaxValue = 12; 
				} break;
			case EDTC_TYPE_DELIM_DATE:		//0x03  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_DATE;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_DATE:					//0x04  
				{
					m_Fields[i].ulValue = 1;			// numerical value
					m_Fields[i].ulMinValue = 1;  // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 31;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "01"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_DAY:			//0x05  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_DAY;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_DAY:						//0x06  
				{
					m_Fields[i].ulValue = '(';			// numerical value
					m_Fields[i].ulMinValue = 1;  // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 7;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = (char*)malloc(6);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "(Sun)"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_HOUR:		//0x07  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_HOUR;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_HOUR:					//0x08  
				{
					m_Fields[i].ulValue = 0;			// numerical value
					m_Fields[i].ulMinValue =0;  // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 23;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "00"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_MIN:			//0x09  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_MINUTE;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_MIN:						//0x0a  
				{
					m_Fields[i].ulValue = 0;			// numerical value
					m_Fields[i].ulMinValue =0;  // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 59;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "00"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_SEC:			//0x0b  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_SECOND;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_SEC:						//0x0c  
				{
					m_Fields[i].ulValue = 0;			// numerical value
					m_Fields[i].ulMinValue =0;  // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 59;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "00"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_SUBSEC:	//0x0d  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_SUBSEC;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_SUBSEC:				//0x0e  
				{
					m_Fields[i].ulValue = 0; 		
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXFIELDWIDTH);
					if(m_ulFlags&EDTC_DO_SHOWFRAMES)
					{
						if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "00"); // for char types, a string.
						if(m_ulFlags&EDTC_CTRL_NTSC)
							m_Fields[i].ulMaxValue = 29;  // maximum numerical value // max char range if set
						else
							m_Fields[i].ulMaxValue = 24;  // maximum numerical value // max char range if set
					}
					else
					if(m_ulFlags&EDTC_DO_SHOWFIELDS)
					{
						if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "01"); // for char types, a string.
						if(m_ulFlags&EDTC_CTRL_NTSC)
							m_Fields[i].ulMaxValue = 59;  // maximum numerical value // max char range if set
						else
							m_Fields[i].ulMaxValue = 49;  // maximum numerical value // max char range if set
					}
					else  //EDTC_DO_SHOWMS
					{
						if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "000"); // for char types, a string.
						m_Fields[i].ulMaxValue = 999;  // maximum numerical value // max char range if set
					}
					m_Fields[i].ulMinValue =0;  // minimum numerical value // min char range if set
				} break;
			case EDTC_TYPE_DELIM_AMPM:		//0x0f  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_AMPM;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value 
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value 
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_AMPM:					//0x10 
				{
					m_Fields[i].ulValue = '(';			
					m_Fields[i].ulMinValue = 0;  // minimum numerical value 
					m_Fields[i].ulMaxValue = 1;  // maximum numerical value 
					m_Fields[i].pszChars = (char*)malloc(5);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, "(AM)"); // for char types, a string.
				} break;
			case EDTC_TYPE_DELIM_COMMENT:	//0x11  
				{
					m_Fields[i].ulValue = EDTC_DEFDELIM_COMMENT;			// numerical value
					m_Fields[i].ulMinValue = 32;   // minimum numerical value // min char range if set
					m_Fields[i].ulMaxValue = 127;  // maximum numerical value // max char range if set
					m_Fields[i].pszChars = NULL;
				} break;
			case EDTC_TYPE_COMMENT:				//0x12
				{
					m_Fields[i].ulValue = 0;		
					m_Fields[i].ulMinValue = 0;
					m_Fields[i].ulMaxValue = 80;  // maximum number of chars
					m_Fields[i].pszChars = (char*)malloc(EDTC_MAXCOMMENTWIDTH);
					if(m_Fields[i].pszChars) strcpy(m_Fields[i].pszChars, ""); // for char types, a string.
				} break;
			}
		}
		OutputData();  // just draw it. based on options.
	}
}

// SetTime.
// the major counter is either a julian date from jan 1, 1900, in which case the milliseconds
// represents the number of milliseconds elapsed in that day.  Or, it is a unixtime, in which 
// case the milliseconds represents the number of milliseconds elapsed in that second.
// Unixtime is the more common usage so it is the default.
// if a Julian day is given, then the ulMilliseconds must be ORed with EDTC_TYPE_JULIAN.
int CEditTC::SetTime(unsigned long ulMajorCounter, unsigned long ulMilliseconds)
{
	if(ulMilliseconds&EDTC_TYPE_JULIAN)
	{
		ulMilliseconds&=~EDTC_TYPE_JULIAN;  // remove flag

		// calculate.
		// the following "ought" to roll and truncate to appropriate epoch
		m_ulRefUnixTime = ((ulMajorCounter - 25567)*86400) + (ulMilliseconds/1000);
		m_ulRefTimeMS = ulMilliseconds;
		m_ulRefJulianDate = ulMajorCounter;
		m_dblDateTime = (double)(m_ulRefJulianDate + 2) + (((double)ulMilliseconds)/(double)86400000.0);
	}
	else // process as unixtime.
	{
		m_ulRefUnixTime = ulMajorCounter;

		// calculate.
		m_ulRefJulianDate = (ulMajorCounter/86400)+25567;

		m_ulRefTimeMS = (m_ulRefUnixTime - ((m_ulRefJulianDate - 25567)*86400) )*1000 + ulMilliseconds;
		// must mod 1000 since possible overlap on milliseconds and unixtime (seconds);
		m_dblDateTime = (double)(m_ulRefJulianDate + 2) + (((double)m_ulRefTimeMS)/(double)86400000.0);
	}

	return EDTC_SUCCESS;
}

int CEditTC::SetTime(double dblDateTime)
{
	m_dblDateTime = dblDateTime;
	//calculate stuff:
	m_ulRefJulianDate = (unsigned long)(dblDateTime) - 2;
	m_ulRefTimeMS = (unsigned long) abs((int)((dblDateTime - ((double)((int)dblDateTime)) )*(double)86400000.0 ));
		// the following "ought" to roll and truncate to appropriate epoch
	m_ulRefUnixTime = ((m_ulRefJulianDate - 25567)*86400) + (m_ulRefTimeMS/1000);

	return EDTC_SUCCESS;
}

int CEditTC::SetField(int nFieldIndex, char* pchNewValue, unsigned long ulNewValue) 
{

	return EDTC_SUCCESS;
}

int CEditTC::PosToField(unsigned long ulPosition)  // character position
{
	// calculate the field index of the passed in character position.
	if(m_pWnd)
	{
		bool bFirstField = true;

		int i=0;
		unsigned long ulPosCounter = 0;
		while(i<=EDTC_TYPE_COMMENT)
		{
			switch(i)
			{
			case EDTC_TYPE_YEAR:					//0x00
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&EDTC_DO_SHOWYEAR)
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_MON:			//0x01 
				{
					if(m_ulFlags&(EDTC_DO_SHOWTMONTH|EDTC_DO_SHOWMONTH))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_MONTH:					//0x02  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&(EDTC_DO_SHOWTMONTH|EDTC_DO_SHOWMONTH))
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}

				} break;
			case EDTC_TYPE_DELIM_DATE:		//0x03  
				{
					if(m_ulFlags&(EDTC_DO_SHOWDATE))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_DATE:					//0x04  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&EDTC_DO_SHOWDATE)
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_DAY:			//0x05  
				{
					if(m_ulFlags&(EDTC_DO_SHOWTDAY))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_DAY:						//0x06  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&EDTC_DO_SHOWTDAY)
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_HOUR:		//0x07  
				{
					if(m_ulFlags&(EDTC_DO_SHOW24HOUR|EDTC_DO_SHOW12HOUR))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_HOUR:					//0x08  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&(EDTC_DO_SHOW24HOUR|EDTC_DO_SHOW12HOUR))
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_MIN:			//0x09  
				{
					if(m_ulFlags&(EDTC_DO_SHOWMINUTE))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_MIN:						//0x0a  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&(EDTC_DO_SHOWMINUTE))
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_SEC:			//0x0b  
				{
					if(m_ulFlags&(EDTC_DO_SHOWSECOND))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_SEC:						//0x0c  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&EDTC_DO_SHOWSECOND)
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_SUBSEC:	//0x0d  
				{
					if(m_ulFlags&(EDTC_DO_SHOWMS|EDTC_DO_SHOWFRAMES|EDTC_DO_SHOWFIELDS))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_SUBSEC:				//0x0e  
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&(EDTC_DO_SHOWMS|EDTC_DO_SHOWFRAMES|EDTC_DO_SHOWFIELDS))
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}	
				} break;
			case EDTC_TYPE_DELIM_AMPM:		//0x0f  
				{
					if(m_ulFlags&(EDTC_DO_SHOWAMPM))
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_AMPM:					//0x10 
				{
					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&(EDTC_DO_SHOWAMPM))
						{
							ulPosCounter += strlen(m_Fields[i].pszChars);
							if(ulPosition<ulPosCounter)  return i; // the field!
							bFirstField =false;
						}
					}	
				} break;
			case EDTC_TYPE_DELIM_COMMENT:	//0x11  
				{
					if((m_Fields[i+1].pszChars)&&(strlen(m_Fields[i+1].pszChars)>0)) 
					{
						if(!bFirstField) ulPosCounter++; // delimiter is one char
						if(ulPosition<ulPosCounter)  return i; // the field!
					}
				} break;
			case EDTC_TYPE_COMMENT:				//0x12
				{
					if((m_Fields[i].pszChars)&&(strlen(m_Fields[i].pszChars)>0)) 
					{
						ulPosCounter += strlen(m_Fields[i].pszChars);
						if(ulPosition<ulPosCounter)  return i; // the field!
						bFirstField =false;
					}
				} break;
			}  //switch
			i++;
		}// while
	} //	if(m_pWnd)

	return EDTC_ERROR;
}

int CEditTC::FieldToPos(unsigned long ulField)     // field index
{

	return EDTC_SUCCESS;
}

int CEditTC::Validate(int nFieldIndex, char* pchNewValue, unsigned long ulNewValue)  // either int or char*, not both (one always null)
{

	return EDTC_SUCCESS;
}

int CEditTC::OutputData(bool bSuppressError)				// set the text.
{
	if(m_pWnd)
	{
		bool bRichEdit = false;
		if(m_ulFlags&EDTC_CTRL_RE)  bRichEdit = true;
		
		CString szText=""; // the output string.

		COleDateTime datetime( (DATE)m_dblDateTime );  // this is the main reference date for everything.
		//COleDateTimeSpan


//		char foo[256]; sprintf(foo, "%f", m_dblDateTime); AfxMessageBox(foo);

		int nRV;
		bool bFirstField = true;

		for(int i=0; i<=EDTC_TYPE_COMMENT; i++)
		{
			switch(i)
			{
			case EDTC_TYPE_YEAR:					//0x00
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						nRV = int((m_dblDateTime/365.25)+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0);
						
						m_Fields[i].ulValue = (unsigned long)(abs(nRV));  // julian years (!)
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 8099;  // maximum numerical value // max char range if set
					}
					else
					{
						nRV = datetime.GetYear( );
						if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							m_Fields[i].ulValue = (unsigned long)(nRV);
						else
							m_Fields[i].ulValue = 0;
						m_Fields[i].ulMinValue = 100;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 9999;  // maximum numerical value // max char range if set
					}

					if(m_Fields[i].pszChars)
					{
						sprintf(m_Fields[i].pszChars, "%04d", m_Fields[i].ulValue); // for char types, a string.
						if(m_ulFlags&EDTC_DO_SHOWYEAR)
						{
							szText+=m_Fields[i].pszChars;
							bFirstField =false;
						}
					}

				} break;
			case EDTC_TYPE_DELIM_MON:			//0x01 
				{
				} break;
			case EDTC_TYPE_MONTH:					//0x02  
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						nRV = int((m_dblDateTime/30.4375)+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0);
						
						m_Fields[i].ulValue = (unsigned long)(abs(nRV));  // julian months (!)
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 97188;  // maximum numerical value // max char range if set
					}
					else
					{
						nRV = datetime.GetMonth( );
						if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							m_Fields[i].ulValue = (unsigned long)(nRV);
						else
							m_Fields[i].ulValue = 0;
						m_Fields[i].ulMinValue = 1;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 12;  // maximum numerical value // max char range if set
					}

					if(m_Fields[i].pszChars)
					{
						sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
						if(m_ulFlags&EDTC_DO_SHOWTMONTH)
						{
							if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							strcpy(m_Fields[i].pszChars, datetime.Format("%b"));
							szText+= m_Fields[i].pszChars;
							bFirstField =false;
						}
						else
						if(m_ulFlags&EDTC_DO_SHOWMONTH)
						{
							if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							szText+= m_Fields[i].pszChars;
							bFirstField =false;
						}
					}

				} break;
			case EDTC_TYPE_DELIM_DATE:		//0x03  
				{
				} break;
			case EDTC_TYPE_DATE:					//0x04  
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						nRV = int(m_dblDateTime+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0);
						
						m_Fields[i].ulValue = (unsigned long)(abs(nRV));  // julian days
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 2958160;  // maximum numerical value // max char range if set
					}
					else
					{
						nRV = datetime.GetDay( );
						if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							m_Fields[i].ulValue = (unsigned long)(nRV);
						else
							m_Fields[i].ulValue = 0;
						m_Fields[i].ulMinValue = 1;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 31;  // maximum numerical value // max char range if set
					}

					if(m_Fields[i].pszChars)
					{
						sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
						if(m_ulFlags&EDTC_DO_SHOWDATE)
						{
							if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							szText+= m_Fields[i].pszChars;
							bFirstField = false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_DAY:			//0x05  
				{
				} break;
			case EDTC_TYPE_DAY:						//0x06  
				{
//					sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
					if(m_ulFlags&EDTC_DO_SHOWTDAY)
					{
						if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
						if(m_ulFlags&EDTC_DO_SHOWTDAYENC)
						{
							switch(m_Fields[i].ulValue)
							{
							case '(':	sprintf(m_Fields[i].pszChars, "%s", datetime.Format("(%a)")); break;
							case '[':	sprintf(m_Fields[i].pszChars, "%s", datetime.Format("[%a]")); break;
							case '{':	sprintf(m_Fields[i].pszChars, "%s", datetime.Format("{%a}")); break;
							case '\"':	sprintf(m_Fields[i].pszChars, "%s", datetime.Format("\"%a\"")); break;
							case '\'':	sprintf(m_Fields[i].pszChars, "%s", datetime.Format("\'%a\'")); break;
							default: 	sprintf(m_Fields[i].pszChars, "%s", datetime.Format("%a")); break;
							}
						}
						else
						{
							sprintf(m_Fields[i].pszChars, "%s", datetime.Format("%a"));
						}
						szText+= m_Fields[i].pszChars;
						bFirstField = false;
					}
				} break;
			case EDTC_TYPE_DELIM_HOUR:		//0x07  
				{
				} break;
			case EDTC_TYPE_HOUR:					//0x08  
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						nRV = int((m_dblDateTime*24.0)+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0);
						
						m_Fields[i].ulValue = (unsigned long)(abs(nRV));  // julian hours
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 70995840;  // maximum numerical value // max char range if set
					}
					else
					{
						nRV = datetime.GetHour( );
						if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							m_Fields[i].ulValue = (unsigned long)(nRV);
						else
							m_Fields[i].ulValue = 0;
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 23;  // maximum numerical value // max char range if set
					}

					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&EDTC_DO_SHOW12HOUR)
						{
							if(m_Fields[i].ulValue > 12) m_Fields[i].ulValue -= 12;
							if(m_Fields[i].ulValue == 0 ) m_Fields[i].ulValue = 12;
						}
						sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
						if(m_ulFlags&(EDTC_DO_SHOW24HOUR|EDTC_DO_SHOW12HOUR))
						{
							if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							szText+= m_Fields[i].pszChars;
							bFirstField = false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_MIN:			//0x09  
				{
				} break;
			case EDTC_TYPE_MIN:						//0x0a  
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						nRV = int((m_dblDateTime*1440.0)+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0);
						
						m_Fields[i].ulValue = (unsigned long)(abs(nRV));  // julian minutes
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 4259750400;  // maximum numerical value // max char range if set
					}
					else
					{
						nRV = datetime.GetMinute( );
						if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							m_Fields[i].ulValue = (unsigned long)(nRV);
						else
							m_Fields[i].ulValue = 0;
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 59;  // maximum numerical value // max char range if set
					}

					if(m_Fields[i].pszChars)
					{
						sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
						if(m_ulFlags&EDTC_DO_SHOWMINUTE)
						{
							if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							szText+= m_Fields[i].pszChars;
							bFirstField = false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_SEC:			//0x0b  
				{
				} break;
			case EDTC_TYPE_SEC:						//0x0c  
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						double dblRV = (m_dblDateTime*86400.0)+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0;
						
						m_Fields[i].ulValue = 0;  // julian seconds
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 0xffffffff;  // maximum numerical value // max char range if set
						sprintf(m_Fields[i].pszChars, "%.0f", dblRV); // for char types, a string.
					}
					else
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWUNIX)
					{
						m_Fields[i].ulValue = m_ulRefUnixTime;  // unix
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 0xffffffff;  // maximum numerical value // max char range if set
						sprintf(m_Fields[i].pszChars, "%d", m_ulRefUnixTime); // for char types, a string.
					}
					else
					{
						nRV = datetime.GetSecond( );
//						char foo[256]; sprintf(foo, "%d", nRV); AfxMessageBox(foo);
						if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							m_Fields[i].ulValue = (unsigned long)(nRV);
						else
							m_Fields[i].ulValue = 0;
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 59;  // maximum numerical value // max char range if set
						sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
					}

					if(m_Fields[i].pszChars)
					{
						if(m_ulFlags&EDTC_DO_SHOWSECOND)
						{
							if((m_ulFlags&EDTC_DO_SHOWMASK)!=EDTC_DO_SHOWUNIX)	
							{
								if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							}
							szText+= m_Fields[i].pszChars;
							bFirstField = false;
						}
					}
				} break;
			case EDTC_TYPE_DELIM_SUBSEC:	//0x0d  
				{
				} break;
			case EDTC_TYPE_SUBSEC:				//0x0e  
				{
					if((m_ulFlags&EDTC_DO_SHOWMASK)==EDTC_DO_SHOWJULIAN)
					{
						double multi = 86400000.0; //EDTC_DO_SHOWMS
						if(m_ulFlags&EDTC_DO_SHOWFRAMES)
						{
							if(m_ulFlags&EDTC_CTRL_PAL)
								multi = 86400.0 * 25.0;
							else
								multi = 86400.0 * 30.0;
						}
						else
						if(m_ulFlags&EDTC_DO_SHOWFIELDS)
						{
							if(m_ulFlags&EDTC_CTRL_PAL)
								multi = 86400.0 * 50.0;
							else
								multi = 86400.0 * 60.0;
						}
							
						double dblRV = (m_dblDateTime*multi)+(m_ulFlags&EDTC_DO_ROUND)?0.5:0.0;
						
						m_Fields[i].ulValue = 0;  // julian sub-second-increment
						m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
						m_Fields[i].ulMaxValue = 0xffffffff;  // maximum numerical value // max char range if set
						if(m_Fields[i].pszChars)
						{
							sprintf(m_Fields[i].pszChars, "%.0f", dblRV); // for char types, a string.
							if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
							szText+= m_Fields[i].pszChars;
							bFirstField = false;
						}
					}
					else
					{
						nRV = m_ulRefTimeMS%1000;
						if(m_ulFlags&EDTC_DO_SHOWMS)
						{
							m_Fields[i].ulValue = nRV;
							m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
							m_Fields[i].ulMaxValue = 999;  // maximum numerical value // max char range if set
							if(m_Fields[i].pszChars)
							{
								sprintf(m_Fields[i].pszChars, "%03d", m_Fields[i].ulValue); // for char types, a string.
								if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
								szText+= m_Fields[i].pszChars;
								bFirstField = false;
							}
						}
						else
						if(m_ulFlags&EDTC_DO_SHOWFRAMES)
						{
							m_Fields[i].ulValue = (m_ulRefTimeMS%1000)/((m_ulFlags&EDTC_CTRL_PAL)?40:33); // ms/frame
							m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
							m_Fields[i].ulMaxValue = (m_ulFlags&EDTC_CTRL_PAL)?24:29;  // maximum numerical value // max char range if set
							if(m_Fields[i].pszChars)
							{
								sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
								if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
								szText+= m_Fields[i].pszChars;
								bFirstField = false;
							}
						}
						else
						if(m_ulFlags&EDTC_DO_SHOWFIELDS)
						{
							m_Fields[i].ulValue = (m_ulRefTimeMS%1000)/((m_ulFlags&EDTC_CTRL_PAL)?20:17); // ms/field
							m_Fields[i].ulMinValue = 0;  // minimum numerical value // min char range if set
							m_Fields[i].ulMaxValue = (m_ulFlags&EDTC_CTRL_PAL)?49:59;  // maximum numerical value // max char range if set
							if(m_Fields[i].pszChars)
							{
								sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
								if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
								szText+= m_Fields[i].pszChars;
								bFirstField = false;
							}
						}
					}
				} break;
			case EDTC_TYPE_DELIM_AMPM:		//0x0f  
				{
				} break;
			case EDTC_TYPE_AMPM:					//0x10 
				{
//					sprintf(m_Fields[i].pszChars, "%02d", m_Fields[i].ulValue); // for char types, a string.
					if(m_ulFlags&EDTC_DO_SHOWAMPM)
					{
						if(m_Fields[i].pszChars)
						{

							nRV = datetime.GetHour( );
							if(nRV!=AFX_OLE_DATETIME_ERROR)  //-1
							{
								if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
								strcpy(m_Fields[i].pszChars, "(AM)"); // for char types, a string.

								if(m_ulFlags&EDTC_DO_SHOWAMPMENC)
								{
									switch(m_Fields[i].ulValue)
									{
									case '(':	sprintf(m_Fields[i].pszChars, "(%s)", (nRV<12)?"AM":"PM"); break;
									case '[':	sprintf(m_Fields[i].pszChars, "[%s]", (nRV<12)?"AM":"PM"); break;
									case '{':	sprintf(m_Fields[i].pszChars, "{%s}", (nRV<12)?"AM":"PM"); break;
									case '\"':	sprintf(m_Fields[i].pszChars, "\"%s\"", (nRV<12)?"AM":"PM"); break;
									case '\'':	sprintf(m_Fields[i].pszChars, "\'%s\'", (nRV<12)?"AM":"PM"); break;
									default: 	sprintf(m_Fields[i].pszChars, "%s", (nRV<12)?"AM":"PM"); break;
									}
								}
								else
								{
									sprintf(m_Fields[i].pszChars, "%s", (nRV<12)?"AM":"PM"); break;
								}
								szText+= m_Fields[i].pszChars;
								bFirstField = false;
							}
						}
					}
				} break;
			case EDTC_TYPE_DELIM_COMMENT:	//0x11  
				{
				} break;
			case EDTC_TYPE_COMMENT:				//0x12
				{
					if((m_Fields[i].pszChars)&&(strlen(m_Fields[i].pszChars)>0)) 
					{
						if(!bFirstField) szText+= (char)m_Fields[i-1].ulValue; // delimiter
						szText+= m_Fields[i].pszChars;
						bFirstField = false;
					}
				} break;
			}  //switch
		}// for


		// ok the values are all set now, we should update the text and set the parameters
		if(bRichEdit)
		{
			// set bg color
			if(m_bError)
			{
				if(m_crErrorBGColor!=CLR_INVALID)
				{
					((CRichEditCtrl*)m_pWnd)->SetBackgroundColor(FALSE, m_crErrorBGColor); //RichEdit only
				}
				else
				{
					((CRichEditCtrl*)m_pWnd)->SetBackgroundColor(FALSE, RGB(192,0,0)); //RichEdit only
				}
			}
			else
			{
				if(m_crBGColor!=CLR_INVALID)
				{
					((CRichEditCtrl*)m_pWnd)->SetBackgroundColor(FALSE, m_crBGColor); //RichEdit only
				}
				else
				{
					((CRichEditCtrl*)m_pWnd)->SetBackgroundColor(FALSE, GetSysColor(COLOR_WINDOW)); //RichEdit only
				}
			}
		}
		m_pWnd->SetWindowText(szText);

		m_szLastOutput = szText;  // do this before any rich edit format changes

		if(bRichEdit)
		{ 
			// set the charrange for each field
			// set fonts and text color
			CHARRANGE chrg;
			chrg.cpMin=0;
			chrg.cpMax=0;

			bFirstField = true;

			for(int i=0; i<EDTC_TYPE_COMMENT; i++)
			{
				// go thru every field and set it up.
				int nLen = 0;
				if(m_Fields[i].pszChars) nLen = strlen(m_Fields[i].pszChars);

				unsigned long ulFlagMask=0;
				switch(i)
				{
				case EDTC_TYPE_YEAR://					0x00
					ulFlagMask = EDTC_DO_SHOWYEAR;  break;
				case EDTC_TYPE_MONTH://					0x02 
					ulFlagMask =  EDTC_DO_SHOWTMONTH|EDTC_DO_SHOWMONTH; break;
				case EDTC_TYPE_DATE://					0x04  
					ulFlagMask = EDTC_DO_SHOWDATE;  break;
				case EDTC_TYPE_DAY://						0x06  
					ulFlagMask = EDTC_DO_SHOWTDAY;  break;
				case EDTC_TYPE_HOUR://					0x08  
					ulFlagMask = EDTC_DO_SHOW12HOUR|EDTC_DO_SHOW24HOUR;  break;
				case EDTC_TYPE_MIN://						0x0a  
					ulFlagMask = EDTC_DO_SHOWMINUTE;  break;
				case EDTC_TYPE_SEC://						0x0c  
					ulFlagMask = EDTC_DO_SHOWSECOND;  break;
				case EDTC_TYPE_SUBSEC://				0x0e  
					ulFlagMask = EDTC_DO_SHOWMS|EDTC_DO_SHOWFRAMES|EDTC_DO_SHOWFIELDS;  break;
				case EDTC_TYPE_AMPM://					0x10
					ulFlagMask = EDTC_DO_SHOWAMPM;  break;
				case EDTC_TYPE_COMMENT://				0x12				
					{
						ulFlagMask = 0;  // defeats it later
						// no flag mask so just do it here
						if(nLen)
						{
							if((i>EDTC_TYPE_DELIM_MON)&&(!bFirstField))
							{
								// do the delimiter
								chrg.cpMax++;
								((CRichEditCtrl*)m_pWnd)->SetSel(chrg);
								((CRichEditCtrl*)m_pWnd)->SetSelectionCharFormat( m_Fields[i-1].cf );
								chrg.cpMin++;
							}
							chrg.cpMax+=nLen;
							((CRichEditCtrl*)m_pWnd)->SetSel(chrg);
							((CRichEditCtrl*)m_pWnd)->SetSelectionCharFormat( m_Fields[i].cf );
							bFirstField = false;
							chrg.cpMin+=nLen;
						}
					} break;
				default: break; //nuttin.
				} //switch

				if((nLen)&&(m_ulFlags&ulFlagMask))
				{
					if((i>EDTC_TYPE_DELIM_MON)&&(!bFirstField))
					{
						// do the delimiter
						chrg.cpMax++;
						((CRichEditCtrl*)m_pWnd)->SetSel(chrg);
						((CRichEditCtrl*)m_pWnd)->SetSelectionCharFormat( m_Fields[i-1].cf );
						chrg.cpMin++;
					}
					chrg.cpMax+=nLen;
					((CRichEditCtrl*)m_pWnd)->SetSel(chrg);
					((CRichEditCtrl*)m_pWnd)->SetSelectionCharFormat( m_Fields[i].cf );
					bFirstField = false;
					chrg.cpMin+=nLen;
				}
			} //for
		}

//		AfxMessageBox(szText);
//		m_szLastOutput = szText;

	}	//if(m_pWnd)

	return EDTC_SUCCESS;
}

int CEditTC::AnalyzeData(CString szText)			// analyze any new text, called by EN_CHANGE handler
{
	// compare m_szLastOutput with szText (new text)

//	AfxMessageBox(m_szLastOutput);
//	AfxMessageBox(szText);
	if(m_szLastOutput.GetLength()<=0) return EDTC_SUCCESS; // blank, so nothing.
	if(m_szLastOutput.Compare(szText)==0) return EDTC_SUCCESS; // no change

	// parse out any valid change to the time.
	int nSelBegin, nSelEnd; // get cursor position.
	int nLastLen, nNewLen;
	nNewLen = szText.GetLength();
	nLastLen = m_szLastOutput.GetLength();

	int nSizeChange = nNewLen-nLastLen;
	
	if((m_ulFlags&EDTC_CTRL_RE)&&(m_pWnd))
		((CRichEditCtrl*)m_pWnd)->GetSel((long &)nSelBegin, (long &)nSelEnd); 
	else
		((CEdit*)m_pWnd)->GetSel(nSelBegin, nSelEnd); 

//	char foo[256]; sprintf(foo, "%d, %d", nSelBegin, nSelEnd); AfxMessageBox(foo);

	// now we have to calculate where we want the cursor to be.
	// we are only going to allow one char to change at a time...
	// so, we find the first char that is different

	int i=0;
	while((i<nNewLen)&&(i<nLastLen))
	{
		if(szText.GetAt(i)!=m_szLastOutput.GetAt(i)) break; // i has the index of the first different char

		i++;
	}

	if(i<=nLastLen) // else the changes are beyond what we care about, in the comment area.
	{
		i = (int)PosToField(i);
		//char foo[256]; sprintf(foo, "field %d", i); AfxMessageBox(foo);

	}


	return EDTC_SUCCESS;
}

int CEditTC::SetColor(unsigned long ulFieldIndices)		// change font color of a field ORed. FFFFFFFF = all
{

	return EDTC_SUCCESS;
}

int CEditTC::SetBGColor(COLORREF cr)			// change the normal bg color of control
{
	if(cr==CLR_INVALID) return EDTC_ERROR;
	m_crBGColor = cr;

	if((m_ulFlags&EDTC_CTRL_RE)&&(m_pWnd))
	{
		((CRichEditCtrl*)m_pWnd)->SetBackgroundColor(FALSE, cr); //RichEdit only
	}

	return EDTC_SUCCESS;
}

int CEditTC::SetErrorColor(COLORREF cr)			// change the error bg color of control
{
	if(cr==CLR_INVALID) return EDTC_ERROR;
	m_crErrorBGColor = cr;

	return EDTC_SUCCESS;
}

int CEditTC::SetError(bool bError)		// set the error condition
{
	m_bError = bError;
	if(bError)
	{
		if(m_nErrorIntervalMS>0)
		{
			if(AfxBeginThread(ErrorTimer, (LPVOID) this) == NULL)
			{
				OutputData(); // just draw it once with the error condition
				return EDTC_ERROR; // but indicate thread not started
			}
		}
		else 		
			OutputData(); // just draw it once with the error condition
	}
	else
	{
		Sleep(100);  // let error thread finish if nec.
		OutputData(true); // redraw, no error, suppress error just in case..
	}

	return EDTC_SUCCESS;
}


UINT ErrorTimer( LPVOID pParam )
{
	CEditTC* pEditTC = (CEditTC*) pParam;
	if(pEditTC)
	{
		bool bSuppressError = false;
		while(pEditTC->m_bError)
		{
			pEditTC->OutputData(bSuppressError);  // draws error or not.
			Sleep(pEditTC->m_nErrorIntervalMS); // waits to flash.
			bSuppressError = !bSuppressError;
		}
	}
	return 0;
}

