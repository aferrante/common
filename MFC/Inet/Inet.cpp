// Inet.cpp: implementation of the CInet class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Inet.h"
#include <afxinet.h>
//#include <wininet.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInet::CInet()
{

}

CInet::~CInet()
{

}


//  The following two methods are the main entry points:
int CInet::RetrieveURLToText(CString szURL, CString* pszTextData, BOOL bSecure, BOOL bCache, CString szHeaders, CString* pszInfo)
{
  return RetrieveURLInternals(INET_URL_MODE_TEXT, NULL, szURL, pszTextData, bSecure, bCache, szHeaders, pszInfo);
}

int CInet::RetrieveURLToFile(CString szURL, CString szTempfile, BOOL bSecure, BOOL bCache, CString szHeaders, CString* pszInfo)
{
  FILE *fp;
  CString szTextData;
  fp=fopen((LPCTSTR) szTempfile, "wb");
  if (fp == NULL) return INET_ERROR_FILE_NOT_OPEN;
  return RetrieveURLInternals(INET_URL_MODE_FILE, fp, szURL, &szTextData, bSecure, bCache, szHeaders, pszInfo);
}



//handles advanced HTTP protocol requests such as secure (passworded) POST connections.  
int CInet::RetrieveURLInternalsAdvanced(int nMode, FILE* fp, 
                    CString szServer, int nPort, CString szObject, CString* pszTextData, 
                    BOOL bSecure, BOOL bCache, CString szHeaders, CString szData, 
                    CString szUser, CString szPassword, CString* pszInfo)
{
  DWORD dwFlags, dwFlagsInet;
  int i, nReturnCode=INET_SUCCESS;
  char buffer[1026];
  unsigned long nContentLength=0, nIndex=0;
  LPTSTR p;

  CInternetSession* pcisInet;

  if (bCache)
  {
    dwFlagsInet = 0;
    dwFlags = INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  else
  {
    dwFlagsInet = INTERNET_FLAG_DONT_CACHE;
    dwFlags = INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  if (bSecure) dwFlags |= INTERNET_FLAG_SECURE;


  *pszTextData = "";
  
  //(LPCTSTR pstrAgent = NULL, DWORD dwContext = 1, DWORD dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG, LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0 );
  
  //CInternetSession cisInet(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, INTERNET_FLAG_DONT_CACHE);
  pcisInet = new CInternetSession(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, 
    NULL, NULL, dwFlagsInet );
  
	if(pcisInet)
	{
		CHttpFile* pfile=NULL;
		UINT count;


		CHttpConnection *hc;
		try
		{
			hc = pcisInet->GetHttpConnection(szServer, nPort, szUser, szPassword);
		}
		catch (CInternetException* e)
		{
			if(pszInfo) pszInfo->Format("Inet: Error (%d) connecting to web server [%s:%d].", e->m_dwError, szServer, nPort);
			pcisInet->Close();
			delete pcisInet;
//			delete pfile;
			e->Delete();
			return INET_ERROR_CONNECT;
		}
		catch (...)
		{
			if(pszInfo) pszInfo->Format("Inet: Unknown error connecting to web server [%s:%d].", szServer, nPort);
			if(pcisInet)
			{
				pcisInet->Close();
				delete pcisInet;
			}
			return INET_ERROR_CONNECT;
		}

		try
		{
	/*
	CHttpFile* OpenRequest( int nVerb, LPCTSTR pstrObjectName, 
	LPCTSTR pstrReferer = NULL, DWORD dwContext = 1, LPCTSTR* pstrAcceptTypes = NULL, 
	LPCTSTR pstrVersion = NULL, DWORD dwFlags = INTERNET_FLAG_EXISTING_CONNECT );
	*/
      
			pfile =  (CHttpFile *) hc->OpenRequest(0, //=HTTP_VERB_POST
				(LPCTSTR) szObject,	NULL, 1, NULL, NULL, dwFlags	);

			if(pfile)
				pfile->SendRequest((LPCTSTR) szHeaders, szHeaders.GetLength(), (void *)((LPCTSTR) szData), szData.GetLength() );

		}
		catch (CInternetException* e)
		{
			if(pszInfo) pszInfo->Format("Inet: Error (%d) requesting from to web server [%s].", e->m_dwError, szObject);
			if(pcisInet)
			{
				pcisInet->Close();
				delete pcisInet;
			}
			if(pfile) delete pfile;
			e->Delete();
			return INET_ERROR_CONNECT;
		}
		catch (...)
		{
			if(pszInfo) pszInfo->Format("Inet: Unknown error requesting from to web server [%s].", szObject);
			if(pcisInet)
			{
				pcisInet->Close();
				delete pcisInet;
			}
			if(pfile) delete pfile;
			return INET_ERROR_CONNECT;
		}


		count=1;
		while ((pfile)&&(count > 0))
		{
			try 
			{
				count = pfile->Read(buffer, (UINT) 1024);
			}
			catch (CInternetException* e)
			{
				if(pszInfo) pszInfo->Format("Inet: Error (%d) reading from web server [%s].", e->m_dwError, szObject);
				nReturnCode=INET_ERROR_READ;
				count = -1;// escapes the while
				e->Delete();
			}
			catch (...)
			{
				if(pszInfo) pszInfo->Format("Inet: Unknown error reading from web server [%s].", szObject);
				nReturnCode=INET_ERROR_READ;
				count = -1;// escapes the while
			}

			if(count >= 0)
			{
				switch(nMode)
				{
				case 0:
					// add data to CString textData in a friendly way...
					nContentLength += count;
					p = pszTextData->GetBuffer( nContentLength );
					for (i=0; i<(int)count; i++) p[nIndex++] = buffer[i];
					pszTextData->ReleaseBuffer(nContentLength);  // Surplus memory released, p is now invalid.
					break;
				case 1:
					// write data to file
					fwrite(buffer,count,1,fp);
					break;
				}
			}
		}

		if (nMode == 1) fclose(fp);
		if(pfile) pfile->Close();
		if(pcisInet) 
		{
			pcisInet->Close();
			delete pcisInet;
		}
		if(pfile) delete pfile;
		return nReturnCode;
	}
	else
	{
		if(pszInfo) pszInfo->Format("Inet: Error creating new internet session.");
		return INET_ERROR_MEM;
	}

}


int CInet::RetrieveURLInternals(int nMode, FILE* fp, 
                    CString szURL, CString* pszTextData,  
                    BOOL bSecure, BOOL bCache, CString szHeaders, CString* pszInfo)
{
/*
	if(m_bWriteFileOps)	
	{
		LogToFile("RetrieveURLInternals");
	}
*/
  DWORD dwFlags, dwFlagsInet;
  int nReturnCode=INET_SUCCESS;
  char buffer[1026];
  unsigned long nContentLength=0, nIndex=0;
  LPTSTR p;

  CInternetSession* pcisInet;

  if (bCache)
  {
    dwFlagsInet = 0;
    dwFlags = INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  else
  {
    dwFlagsInet = INTERNET_FLAG_DONT_CACHE;
    dwFlags = INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  if (bSecure) dwFlags |= INTERNET_FLAG_SECURE;


  *pszTextData = "";
  
  //(LPCTSTR pstrAgent = NULL, DWORD dwContext = 1, DWORD dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG, LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0 );
  
  //CInternetSession cisInet(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, INTERNET_FLAG_DONT_CACHE);
  pcisInet = new CInternetSession(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, 
    NULL, NULL, dwFlagsInet );
  
	if(pcisInet)
	{
		CHttpFile* pfile=NULL;
		UINT count, i;
		BOOL bError = FALSE;
  
		try
		{
//			AfxMessageBox("opening");

			if (szHeaders == "")
			{
				pfile =  (CHttpFile*) pcisInet->OpenURL( (LPCTSTR) szURL, 1, dwFlags, NULL, 0);
			}
			else
			{
				pfile =  (CHttpFile*) pcisInet->OpenURL( (LPCTSTR) szURL, 1, dwFlags, 
					(LPCTSTR) szHeaders, szHeaders.GetLength());
			}

//			AfxMessageBox("opened");
		}
		catch (CInternetException* e)
		{
//			AfxMessageBox("exception");

			if(pszInfo) pszInfo->Format("Inet: Error (%d) connecting to web server [%s].", e->m_dwError, szURL);
			if(pcisInet)
			{
				pcisInet->Close();
				delete pcisInet;
			}
			//delete pfile;
			e->Delete();
			return INET_ERROR_CONNECT;
		}
		catch (...)
		{
//			AfxMessageBox("ex");
			if(pszInfo) pszInfo->Format("Inet: Unknown error connecting to web server [%s].", szURL);
			if(pcisInet)
			{
				pcisInet->Close();
				delete pcisInet;
			}
			return INET_ERROR_CONNECT;
		}
/*
	if(m_bWriteFileOps)	
	{
		LogToFile("Reading from web server...");
	}
*/

		count=1;
		while ((pfile)&&(count > 0)&&(!bError))
		{
			try 
			{
				count = pfile->Read(buffer, (UINT) 1024);
			}
			catch (CInternetException *e)
			{
	/*
		if(m_bWriteFileErrors)	
		{
			CString szMsg;
			szMsg.Format("Inet: Error (%d)  reading from web server.", e->m_dwError);
			LogToFile(szMsg);
		}
	*/
				if(pszInfo) pszInfo->Format("Inet: Error (%d) reading from web server [%s].", e->m_dwError, szURL);
				nReturnCode=INET_ERROR_READ;
				e->Delete();
				bError =	TRUE; //escapes the while
			}
			catch (...)
			{
				if(pszInfo) pszInfo->Format("Inet: Unknown error reading from web server [%s].", szURL);
				if(pcisInet)
				{
					pcisInet->Close();
					delete pcisInet;
				}
				nReturnCode=INET_ERROR_READ;
				bError =	TRUE; //escapes the while
			}

			if((count >= 0) &&(!bError))
			{
				switch(nMode)
				{
				case 0:
					// add data to CString textData in a friendly way...
					nContentLength += count;
					p = pszTextData->GetBuffer( nContentLength );
					for (i=0; i<count; i++) p[nIndex++] = buffer[i];
					pszTextData->ReleaseBuffer(nContentLength);  // Surplus memory released, p is now invalid.
					break;
				case 1:
					// write data to file
					fwrite(buffer,count,1,fp);
					break;
				}
			}
		}
	/*
		if(m_bWriteFileOps)	
		{
			CString szMsg;
			szMsg.Format("Received %d bytes from web server.  Exit RetrieveURLInternals.", nContentLength);
			LogToFile(szMsg);
		}
	*/

		if (nMode == 1) fclose(fp);
		if(pfile) pfile->Close();
		
		if(pcisInet)
		{
			pcisInet->Close();
			delete pcisInet;
		}
		if(pfile) delete pfile;

		return nReturnCode;
	}
	else
	{
		if(pszInfo) pszInfo->Format("Inet: Error creating new internet session.");
		return INET_ERROR_MEM;
	}
}

