// Inet.h: interface for the CInet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INET_H__7201A56E_4116_4476_936B_2E82500E5E28__INCLUDED_)
#define AFX_INET_H__7201A56E_4116_4476_936B_2E82500E5E28__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


// error codes
#define INET_SUCCESS					  	  0
#define INET_ERROR_FILE_NOT_OPEN    -1
#define INET_ERROR_CONNECT					-2
#define INET_ERROR_READ							-3
#define INET_ERROR_MEM							-4



// modes
#define INET_URL_MODE_TEXT				  	  0
#define INET_URL_MODE_FILE				  	  1

class CInet  
{
public:
	CInet();
	virtual ~CInet();

  int RetrieveURLToText(CString szURL, CString* pszTextData, BOOL bSecure=FALSE, BOOL bCache=FALSE, CString szHeaders="", CString* pszInfo=NULL);
  int RetrieveURLToFile(CString szURL, CString szTempfile, BOOL bSecure=FALSE, BOOL bCache=FALSE, CString szHeaders="", CString* pszInfo=NULL);
  int RetrieveURLInternals(int nMode, FILE* fp, CString szURL, CString* pszTextData, BOOL bSecure=FALSE, BOOL bCache=FALSE, CString szHeaders="", CString* pszInfo=NULL);
  int RetrieveURLInternalsAdvanced(int nMode, FILE* fp, 
    CString szServer, int nPort, CString szObject, CString* pszTextData, 
    BOOL bSecure, BOOL bCache, CString szHeaders, CString szData, 
    CString szUser, CString szPassword, CString* pszInfo=NULL);

};

#endif // !defined(AFX_INET_H__7201A56E_4116_4476_936B_2E82500E5E28__INCLUDED_)
