#ifndef __COLORBUTTON_H__
#define __COLORBUTTON_H__


#define WM_COLORBUTTONUP		WM_APP + 0x80
#define WM_COLORBUTTONDOWN	WM_APP + 0x81

class CColorButton : public CButton
{
DECLARE_DYNAMIC(CColorButton)
public:
	CColorButton(); 
	virtual ~CColorButton(); 

//	virtual BOOL PreTranslateMessage(MSG* pMsg);

	BOOL Attach(const UINT nID, CWnd* pParent, COLORREF clrBG=0x80000000, COLORREF clrText=0x80000000, COLORREF clrX=0x80000000, COLORREF clrTextX=0x80000000, unsigned int uiBevel=2);
	void SetColor(COLORREF clrText=0x80000000, COLORREF clrBG=0x80000000,	COLORREF clrX=0x80000000, COLORREF clrTextX=0x80000000, BOOL bRedraw=TRUE);

	void SetTextColor(		    COLORREF color=0x80000000, BOOL bRedraw=FALSE) {if(color == 0x80000000) m_clrText=GetSysColor(COLOR_BTNTEXT);			else m_clrText  = color;	if(bRedraw) InvalidateRect(NULL);} 
	void SetBGColor(			    COLORREF color=0x80000000, BOOL bRedraw=FALSE) {if(color == 0x80000000) m_clrBG=GetSysColor(COLOR_BTNFACE);				else m_clrBG    = color;	if(bRedraw) InvalidateRect(NULL);}
  void SetDisabledColor(    COLORREF color=0x80000000, BOOL bRedraw=FALSE) {if(color == 0x80000000) m_clrX= GetSysColor(COLOR_BTNFACE);				else m_clrX     = color;	if(bRedraw) InvalidateRect(NULL);}
  void SetDisabledTextColor(COLORREF color=0x80000000, BOOL bRedraw=FALSE) {if(color == 0x80000000) m_clrTextX= GetSysColor(COLOR_GRAYTEXT);	else m_clrTextX = color;	if(bRedraw) InvalidateRect(NULL);}

	bool m_bUp;

protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	void DrawFrame(CDC* DC, CRect rc, int Inset);
	void DrawFilledRect(CDC* DC, CRect rc, COLORREF clrRect);
	void DrawLine(CDC* DC, CRect EndPoints, COLORREF clrLine);
	void DrawLine(CDC* DC, long left, long top, long right, long bottom, COLORREF clrLine);
	void DrawButtonText(CDC* DC, CRect rc, const char* pchBuffer, COLORREF clrText);

	COLORREF GetTextColor() { return m_clrText; }	
	COLORREF GetBGColor() { return m_clrBG; }
	COLORREF GetDisabledColor() { return m_clrX; }
	COLORREF GetDisabledTextColor() { return m_clrTextX; }
	unsigned int GetBevel() { return m_uiBevel; }

private:
	COLORREF m_clrText;
	COLORREF m_clrBG;
	COLORREF m_clrX;
	COLORREF m_clrTextX;
	unsigned int m_uiBevel;

};
#endif //__COLORBUTTON_H__
