#include "stdafx.h"
#include "ColorButton.h"
#include <sys\timeb.h>
  
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// no automatic class substitution for this file!
#ifdef CColorButton
#undef CColorButton      CColorButton
#endif

// CColorButton
IMPLEMENT_DYNAMIC(CColorButton, CButton)

CColorButton::CColorButton() 
{  
#if (_MFC_VER < 0x0250)
  hwndOwner = NULL;  // initialize hwndOwner for GetOwner() and SetOwner() support in MFC < 2.5
#endif 
	m_clrText=GetSysColor(COLOR_BTNTEXT);
	m_clrBG=GetSysColor(COLOR_BTNFACE);
	m_clrX=GetSysColor(COLOR_BTNFACE);
	m_clrTextX=GetSysColor(COLOR_GRAYTEXT);
	m_uiBevel=2;
	m_bUp = true;
} 

CColorButton::~CColorButton()
{
} 

LRESULT CColorButton::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
/*
	if((GetDlgCtrlID()==0x0429)||(GetDlgCtrlID()==0x042B))
	{
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
				CString Q; Q.Format("%s message 0x%08x in(wparam 0x%08x lparam 0x%08x) out(wparam 0x%08x lparam 0x%08x)", GetDlgCtrlID()==0x0429?"BUTTON1":"BUTTON2", message, wParam, lParam, (WPARAM(BN_PUSHED<<16)|(GetDlgCtrlID()&0xffff)), LPARAM(this->m_hWnd));

				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(fp, "%s%03d %s%c%c", 
					logtmbuf, timestamp.millitm, 
					Q, 13, 10);
				
					fclose(fp);
			}
	}
*/
	CRect rc;
	GetClientRect(&rc);

	BOOL bInButton=TRUE;

	if((message >= WM_MOUSEMOVE)&&(message <=WM_MOUSELAST))
	{
		int xPos = (int)LOWORD(lParam); 
		int yPos = (int)HIWORD(lParam); 

		if(
			  (xPos<rc.left)
			||(xPos>rc.right)
			||(yPos<rc.top)
			||(yPos>rc.bottom)
			)
			 bInButton=FALSE;

	}


	if(
		  ((message == WM_KEYDOWN)&&(wParam==VK_SPACE))
		||(message == WM_LBUTTONDOWN)
		)
	{
		SetFocus();

		if(m_bUp)
		{
			m_bUp = false;

/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
				CString Q; Q.Format("DWP DOWN message 0x%08x in(wparam 0x%08x lparam 0x%08x) out(wparam 0x%08x lparam 0x%08x)", message, wParam, lParam, (WPARAM(BN_PUSHED<<16)|(GetDlgCtrlID()&0xffff)), LPARAM(this->m_hWnd));
				fprintf(fp,"%s\r\n", Q);
				fclose(fp);
			}
*/	
			GetParent()->SendMessage(WM_COLORBUTTONDOWN, (WPARAM(BN_PUSHED<<16)|(GetDlgCtrlID()&0xffff)), LPARAM(this->m_hWnd));
		}
	}
	else 
	if(
		  ((message == WM_KEYUP)&&(wParam==VK_SPACE))
		||(message == WM_LBUTTONUP)
		||(message == WM_CAPTURECHANGED)
//		||(message == WM_KILLFOCUS)
		||(message == WM_GETDLGCODE)
		||(!bInButton)
//		||(message == 0x164)
//		||((message == WM_IME_SETCONTEXT)&&(wParam!=0x00000001))  //window becoming inactivated
		)
	{
		ReleaseCapture(); // ensure this, in the case of a popup
		if(!m_bUp)
		{
			m_bUp = true;
/*
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
				CString Q; Q.Format("DWP UP message 0x%08x in(wparam 0x%08x lparam 0x%08x) out(wparam 0x%08x lparam 0x%08x)", message, wParam, lParam, (WPARAM(BN_UNPUSHED<<16)|(GetDlgCtrlID()&0xffff)), LPARAM(this->m_hWnd));
				fprintf(fp,"%s\r\n", Q);
				fclose(fp);
			}
*/
			GetParent()->SendMessage(WM_COLORBUTTONUP, (WPARAM(BN_UNPUSHED<<16)|(GetDlgCtrlID()&0xffff)), LPARAM(this->m_hWnd));
		}
	}
	
	return CButton::DefWindowProc(message, wParam, lParam);
}
/*

BOOL CColorButton::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if((GetDlgCtrlID()==0x0429)||(GetDlgCtrlID()==0x042B))
	{
			FILE* fp = fopen( "C:\\msglog.txt", "ab");
			if(fp)
			{
				CString Q; Q.Format("%s PTM message 0x%08x in(wparam 0x%08x lparam 0x%08x) out(wparam 0x%08x lparam 0x%08x)", GetDlgCtrlID()==0x0429?"BUTTON1":"BUTTON2",pMsg->message, pMsg->wParam, pMsg->lParam, (WPARAM(BN_PUSHED<<16)|(GetDlgCtrlID()&0xffff)), LPARAM(this->m_hWnd));

				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


//AfxMessageBox(pchSnd);

				fprintf(fp, "%s%03d %s%c%c", 
					logtmbuf, timestamp.millitm, 
					Q, 13, 10);
				
					fclose(fp);
			}
	}
	
	return CButton::PreTranslateMessage(pMsg);
}
*/


BOOL CColorButton::Attach(const UINT nID, CWnd* pParent, COLORREF clrBG, COLORREF clrText, COLORREF clrX, COLORREF clrTextX, unsigned int uiBevel)
{
	if (!SubclassDlgItem(nID, pParent))
		return FALSE;

	if(clrText == 0x80000000) m_clrText = GetSysColor(COLOR_BTNTEXT);  else m_clrText = clrText;
	if(clrBG == 0x80000000) m_clrBG = GetSysColor(COLOR_BTNFACE);  else m_clrBG = clrBG;
	if(clrX == 0x80000000) m_clrX = GetSysColor(COLOR_BTNFACE);  else m_clrX = clrX;
	if(clrTextX == 0x80000000) m_clrTextX = GetSysColor(COLOR_GRAYTEXT);  else m_clrTextX = clrTextX;
	m_uiBevel = uiBevel;

	return TRUE;
} 

void CColorButton::SetColor( COLORREF clrText, COLORREF clrBG,	COLORREF clrX, COLORREF clrTextX, BOOL bRedraw) 
{ 
	SetTextColor(clrText);
	SetBGColor(clrBG);
	SetDisabledColor(clrX);
	SetDisabledTextColor(clrTextX);
	if(bRedraw) InvalidateRect(NULL);
}


void CColorButton::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);

	UINT state = lpDIS->itemState; 
	CRect rcFocus;
	CRect	rcButton;
//	rcFocus.CopyRect(&lpDIS->rcItem); 
	rcButton.CopyRect(&lpDIS->rcItem); 

/*
	//
	// Set the focus rectangle to just past the border decoration
	//
	rcFocus.left += 4;
	rcFocus.right -= 4;
	rcFocus.top += 4;
	rcFocus.bottom -= 4;
*/	  
	// Retrieve the button's caption
	const int bufSize = 512;
	TCHAR buffer[bufSize];
	GetWindowText(buffer, bufSize);
	

	if (state & ODS_SELECTED)
	{
		DrawFilledRect(pDC, rcButton, GetBGColor()); 
		DrawFrame(pDC, rcButton, -1);

		// move the button text anf focus rect to the right and bottom if it is pressed
		rcButton.OffsetRect( 1, 1 );

		DrawButtonText(pDC, rcButton, buffer, GetTextColor());

		rcButton.left += 4;
		rcButton.right -= 4;
		rcButton.top += 4;
		rcButton.bottom -= 4;
		DrawFocusRect(lpDIS->hDC, (LPRECT)&rcButton);
	}
	else
	if (state & ODS_DISABLED) 
	{
		//COLORREF disabledColor = bg ^ 0xFFFFFF; // contrasting color
		DrawFilledRect(pDC, rcButton, GetDisabledColor()); 
		DrawFrame(pDC, rcButton, GetBevel());
		DrawButtonText(pDC, rcButton, buffer, GetDisabledTextColor());
	}
	else
	{
		DrawFilledRect(pDC, rcButton, GetBGColor()); 
		DrawFrame(pDC, rcButton, GetBevel());
		DrawButtonText(pDC, rcButton, buffer, GetTextColor());

		//
		// Now, depending upon the state, redraw the button (down image) if it is selected,
		// place a focus rectangle on it, or redisplay the caption if it is disabled
		//
		if (state & ODS_FOCUS) 
		{
			rcButton.left += 4;
			rcButton.right -= 4;
			rcButton.top += 4;
			rcButton.bottom -= 4;

			DrawFocusRect(lpDIS->hDC, (LPRECT)&rcButton);
		}
	}
} 


void CColorButton::DrawFrame(CDC *DC, CRect R, int Inset)
{ 
	COLORREF dark, light, tlColor, brColor;
	int i, m, width;
	width = (Inset < 0)? -Inset : Inset;
	
	for (i = 0; i < width; i += 1) 
	{
		m = 255 / (i + 2);
		dark = PALETTERGB(m, m, m);
		m = 192 + (63 / (i + 1));
		light = PALETTERGB(m, m, m);
		  
		if ( width == 1 ) 
		{
			light = RGB(255, 255, 255);
			dark = RGB(128, 128, 128);
		}
		
		if ( Inset < 0 ) 
		{
			tlColor = dark;
			brColor = light;
		}
		else 
		{
			tlColor = light;
			brColor = dark;
		}
		
		DrawLine(DC, R.left, R.top, R.right, R.top, tlColor);					     
	  // Across top
		DrawLine(DC, R.left, R.top, R.left, R.bottom, tlColor); 				     
	  // Down left
	  
		if ( (Inset < 0) && (i == width - 1) && (width > 1) ) 
		{
			DrawLine(DC, R.left + 1, R.bottom - 1, R.right, R.bottom - 1, RGB(1, 1, 1));// Across bottom
			DrawLine(DC, R.right - 1, R.top + 1, R.right - 1, R.bottom, RGB(1, 1, 1));	// Down right
		}
		else 
		{
			DrawLine(DC, R.left + 1, R.bottom - 1, R.right, R.bottom - 1, brColor); 	// Across bottom
			DrawLine(DC, R.right - 1, R.top + 1, R.right - 1, R.bottom, brColor);		// Down right
		}
		InflateRect(R, -1, -1);
	}
}



void CColorButton::DrawFilledRect(CDC* DC, CRect rc, COLORREF color)
{ 
	CBrush B;
	B.CreateSolidBrush(color);
	DC->FillRect(rc, &B);
}
 

void CColorButton::DrawLine(CDC* DC, CRect EndPoints, COLORREF color)
{ 
	CPen newPen;
	newPen.CreatePen(PS_SOLID, 1, color);
	CPen *oldPen = DC->SelectObject(&newPen);
	DC->MoveTo(EndPoints.left, EndPoints.top);
	DC->LineTo(EndPoints.right, EndPoints.bottom);
	DC->SelectObject(oldPen);
	newPen.DeleteObject();
}

void CColorButton::DrawLine(CDC* DC, long left, long top, long right, long bottom, COLORREF clrLine)
{ 
	CPen newPen;
	newPen.CreatePen(PS_SOLID, 1, clrLine);
	CPen *oldPen = DC->SelectObject(&newPen);
	DC->MoveTo(left, top);
	DC->LineTo(right, bottom);
	DC->SelectObject(oldPen);
	newPen.DeleteObject();
}


void CColorButton::DrawButtonText(CDC* DC, CRect rc, const char* pchBuffer, COLORREF clrText)
{
	COLORREF prevColor = DC->SetTextColor(clrText);
	DC->SetBkMode(TRANSPARENT);
	DC->DrawText(pchBuffer, strlen(pchBuffer), rc, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	DC->SetTextColor(prevColor);
}