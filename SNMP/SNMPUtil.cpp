// SNMPUtil.cpp: implementation of the CSNMPUtil class and other support classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  //temp if need AfxMessageBox

#include "SNMPUtil.h"
#include "../TXT/BufferUtil.h"
#include <string.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CSNMPMessage Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSNMPMessage::CSNMPMessage()
{
	m_ucVersion = 0x00;
	m_pchCommunity = NULL; // zero-terminated string
	m_ucPDUType = 0x00;
	m_ulRequest = 0x00;
	m_ulErrorStatus = 0x00;
	m_ulErrorIndex = 0x00;
	m_pchOID = NULL; // dotted string  // zero-terminated string
	m_pucValue = NULL; // can't be a zero-terminated string unless encoded....
	m_ulValueLength = 0;
}

CSNMPMessage::~CSNMPMessage()
{
	if(m_pchCommunity) // zero-terminated string
		free(m_pchCommunity); // use malloc
	if(m_pchOID) // dotted string  // zero-terminated string
		free(m_pchOID); // use malloc
	if(m_pucValue) // zero-terminated string
		free(m_pucValue); // use malloc
}





//////////////////////////////////////////////////////////////////////
// CSNMPTypeLengthData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSNMPTypeLengthData::CSNMPTypeLengthData()
{
	m_ucType=SNMP_NULL;
	m_ulTotalDataLength=0;
	m_ppvDataObj=NULL;
	m_ulNumDataObj=0;
}

CSNMPTypeLengthData::~CSNMPTypeLengthData()
{
	if(m_ppvDataObj)
	{
		switch(m_ucType)
		{
			// simple types, just cast the pointer and delete if not null.
		case SNMP_EOC://							0x00  should be null
		case SNMP_NULL://							0x05
			{
			} break;

		case SNMP_BOOLEAN://					0x01 should be an array of bytes
		case SNMP_INTEGER://					0x02
		case SNMP_OCTETSTRING://			0x04 
		case SNMP_OBJID://						0x06
			{
				delete [] m_ppvDataObj;
			} break;

		case SNMP_SEQUENCE://					0x30 //should be an array of TLD units
		case SNMP_GETREQUESTPDU://		0xA0
		case SNMP_GETRESPONSEPDU://		0xA2
		case SNMP_SETREQUESTPDU://		0xA3
			{
				unsigned long i=0;
				while(i<m_ulNumDataObj)
				{
					if(m_ppvDataObj[i])
					{
						delete m_ppvDataObj[i];
					}
					i++;
				}
				delete [] m_ppvDataObj;

			} break;
		}
	}
}

int CSNMPTypeLengthData::UpdateDataLengths()
{
	if(m_ppvDataObj)
	{
		switch(m_ucType)
		{
			// simple types, just cast the pointer and delete if not null.
		case SNMP_EOC://							0x00  should be null
		case SNMP_NULL://							0x05
			{
				m_ulTotalDataLength = 0;
			} break;

		case SNMP_BOOLEAN://					0x01 should be an array of bytes
		case SNMP_INTEGER://					0x02
		case SNMP_OCTETSTRING://			0x04 
		case SNMP_OBJID://						0x06
			{
				// have to have it be set at setting of the pointer time.
			} break;

			// here's the real deal
		case SNMP_SEQUENCE://					0x30 //should be an array of TLD units
		case SNMP_GETREQUESTPDU://		0xA0
		case SNMP_GETRESPONSEPDU://		0xA2
		case SNMP_SETREQUESTPDU://		0xA3
			{
				m_ulTotalDataLength = 0;
				unsigned long i=0;
				while(i<m_ulNumDataObj)
				{
					if(m_ppvDataObj[i])
					{
						m_ulTotalDataLength += ((CSNMPTypeLengthData*)(m_ppvDataObj[i]))->UpdateDataLengths();
					}
					i++;
				}
			} break;
		}
	}
	else
	{
		m_ulTotalDataLength=0;
	}
	CSNMPUtil u;
	return m_ulTotalDataLength+1+u.ReturnLength(m_ulTotalDataLength);
}



//////////////////////////////////////////////////////////////////////
// CSNMPUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////


CSNMPUtil::CSNMPUtil()
{
	m_ucLastMessageNumber = 0;
}

CSNMPUtil::~CSNMPUtil()
{

}

int CSNMPUtil::IncrementMessageNumber()
{
	m_ucLastMessageNumber++;
	if(m_ucLastMessageNumber>=254) m_ucLastMessageNumber=0; // loop around. but keep value 255 separate.  it is reserved
	return m_ucLastMessageNumber;
}

int CSNMPUtil::DecodeOID(unsigned char* pBuffer, unsigned long* pulValue) // this form takes a buffer with an OID value, and just parses the one vlaue and returns the value.
{
	int i=0;

	if((pBuffer)&&(pulValue))
	{
		*pulValue = (pBuffer[i]&0x7f);

		while(pBuffer[i]&0x80)
		{
			(*pulValue)<<=7;
			i++;
			(*pulValue) |= (pBuffer[i]&0x7f);
		}
		i++;// increment to count.

	}
	return i;
}

// this form takes a single integer and spits out a few bytes of encoded value // not valid for fist two dotted values
int CSNMPUtil::EncodeOID(unsigned char** ppBuffer, unsigned long* pulLen, unsigned long ulInputOID)
{
	if((ppBuffer)&&(pulLen))
	{
		*ppBuffer = new unsigned char[5]; // the largest representation of a 32 bit unsigned value takes 5 bytes to encode.
		if(*ppBuffer)
		{
			// 127 bytes are 0x7F groups:
			// 32 bytes as:
			// [4][7][7][7][7]
			// which are:
			// 0xf0000000
			// 0x0fe00000
			// 0x001fc000
			// 0x00003f80
			// 0x0000007f

			int i=0;
			bool bMSB = false;
			if(ulInputOID&0xf0000000)
			{
				(*ppBuffer)[i] = (unsigned char)((((ulInputOID&0xf0000000)>>28)&0x0f)|0x80);
				i++;
				bMSB = true;
			}
			if((ulInputOID&0x0fe00000)||(bMSB))
			{
				(*ppBuffer)[i] = (unsigned char)((((ulInputOID&0x0fe00000)>>21)&0x7f)|0x80);
				i++;
				bMSB = true;
			}
			if((ulInputOID&0x001fc000)||(bMSB))
			{
				(*ppBuffer)[i] = (unsigned char)((((ulInputOID&0x001fc000)>>14)&0x7f)|0x80);
				i++;
				bMSB = true;
			}
			if((ulInputOID&0x00003f80)||(bMSB))
			{
				(*ppBuffer)[i] = (unsigned char)((((ulInputOID&0x00003f80)>>7)&0x7f)|0x80);
				i++;
				bMSB = true;
			}
			if((ulInputOID&0x0000007f)||(bMSB))
			{
				(*ppBuffer)[i] = (unsigned char)(ulInputOID&0x0000007f);
				i++;
			}
			*pulLen = i;

			return i;

		}
	}
	return -1;
}


// this form takes a string of dotted integer values in the passed in buffer, parses it, and returns a new buffer with the entire encoded OID
int CSNMPUtil::EncodeOID(unsigned char** ppBuffer, unsigned long* pulLen)
{
	if((ppBuffer)&&(pulLen))
	{
		// need to figure out max length.
		// first two OID numers get combined into 1 byte, according to this formula: the first two numbers of any OID (x.y) are encoded as one value using the formula (40*x)+y.
		// then each following number is probably juse one byte, but if a large number, may be as many as 5 bytes.
		// So, using 1 byte + 5 bytes for num dots-1, we have a max buffer size. ex.  1.2.3.4.5.6  5 dots [1.2=1byte][3=5bytes][4=5bytes][5=5bytes][6=5bytes]

		CBufferUtil bu;
		unsigned long ulLen = (*pulLen);

		int nNumDots = bu.CountChar((char*)(*ppBuffer), ulLen, '.');

		unsigned char* pNewBuffer = new unsigned char[1 + 5*(nNumDots-1)];
		if(pNewBuffer)
		{
			CSafeBufferUtil sbu;

			char* pch = sbu.Token((char*)(*ppBuffer), ulLen, ".", MODE_SINGLEDELIM);

			int i=0;
			unsigned long n=0;
			if(pch)
			{
				n = atol(pch);
				n*=40;

				pch = sbu.Token(NULL, NULL, ".", MODE_SINGLEDELIM);
				if(pch)
				{
					n += atol(pch);

					pNewBuffer[i]= (unsigned char)n;
					i++;

					int q=1; // skipping the first dot.
					pch = sbu.Token(NULL, NULL, ".", MODE_SINGLEDELIM);
					while((q<nNumDots)&&(pch))
					{
						n=0;
						n = atol(pch);

						if(n>127)
						{
							unsigned char* puc=NULL;
							unsigned long ulNumLen=0;

							EncodeOID(&puc, &ulNumLen, n);

							if(puc)
							{
								memcpy(&pNewBuffer[i], puc, ulNumLen );
								i+=ulNumLen;
								free(puc);
							}
							else
							{
								pNewBuffer[i]=0; i++;
							}

						}
						else
						{
							pNewBuffer[i]=(unsigned char)n; i++;
						}
						q++;
						pch = sbu.Token(NULL, NULL, ".", MODE_SINGLEDELIM);
					}
				}
				else
				{
					delete [] pNewBuffer;
					pNewBuffer = NULL;
				}

			}
			else
			{
				delete [] pNewBuffer;
				pNewBuffer = NULL;
			}

			*pulLen = i;
			*ppBuffer = pNewBuffer;

			return i;

		}
		else
		{
			*pulLen = 0;
			*ppBuffer = NULL;
		}
	}
	return -1;
}

int CSNMPUtil::DecodeOID(unsigned char** ppBuffer, unsigned long* pulLen) // this form takes a buffer with the entire encoded OID, parses it, and returns a new buffer with a string of dotted integer values
{
	if((ppBuffer)&&(pulLen))
	{
		// first two OID numers get combined into 1 byte, according to this formula: the first two numbers of any OID (x.y) are encoded as one value using the formula (40*x)+y.
		// then each following number is probably juse one byte, but if a large number, may be as many as 5 bytes.
		// So, using 1 byte + 5 bytes for num dots-1, we have a max buffer size. ex.  1.2.3.4.5.6  5 dots [1.2=1byte][3=5bytes][4=5bytes][5=5bytes][6=5bytes]

		CBufferUtil bu;
		unsigned long ulLen = (*pulLen);

		unsigned char* pNewBuffer = new unsigned char[256]; // just use more than you would ever need realistically
		if(pNewBuffer)
		{
			unsigned char* puc = (*ppBuffer);
			int i=2;
			unsigned long ulIndex=1;
			unsigned long ulFieldLen=0;
			unsigned long ulValue=0;
			char buffer[256];
		
			ulValue = (puc[0]-(puc[0]%40))/40;

			sprintf((char*)pNewBuffer, "%d", ulValue);

			ulValue = (puc[0]%40);

			sprintf(buffer, ".%d", ulValue);
			strcat((char*)pNewBuffer, buffer);

			// Now do the rest:
			while(ulIndex<ulLen)
			{
				ulFieldLen = DecodeOID(&puc[ulIndex], &ulValue);

				ulIndex+=ulFieldLen;
				sprintf(buffer, ".%d", ulValue);
				strcat((char*)pNewBuffer, buffer);
				i++;
			}
			*pulLen = strlen((char*)pNewBuffer);
			*ppBuffer = pNewBuffer;


			return i;  // returns number of values processed.

		}
		else
		{
			*pulLen = 0;
			*ppBuffer = NULL;
		}
	}
	return -1;
}

// BER length encoding
int CSNMPUtil::EncodeLength(unsigned char** ppBuffer, unsigned long* pulLen, unsigned long ulInputLength)
{
	if((ppBuffer)&&(pulLen))
	{
		*ppBuffer = new unsigned char[5]; // the largest representation of a 32 bit unsigned value takes 5 bytes to encode.
		if(*ppBuffer)
		{
			unsigned char i=0;
			if(ulInputLength&0xff000000)
			{
				i=4;
				(*ppBuffer)[0]=(0x80|i);
				memcpy(&((*ppBuffer)[1]), &ulInputLength, i); // really sizeof(unsigned long), 32 bits here.
			}
			else
			if(ulInputLength&0x00ff0000)
			{
				i=3;
				(*ppBuffer)[0]=(0x80|i);
				memcpy(&((*ppBuffer)[1]), (&ulInputLength)+1, i);
			}
			else
			if(ulInputLength&0x0000ff00)
			{
				i=2;
				(*ppBuffer)[0]=(0x80|i);
				memcpy(&((*ppBuffer)[1]), (&ulInputLength)+2, i);
			}
			else
			if(ulInputLength&0x00000080)
			{
				i=1;
				(*ppBuffer)[0]=(0x80|i);
				memcpy(&((*ppBuffer)[1]), (&ulInputLength)+3, i);
			}
			else
			{
				i=0;
				(*ppBuffer)[0]=(unsigned char)(ulInputLength&0x7f);
			}
			
			*pulLen = i+1;

			return i;

		}
	}
	return -1;
}

int CSNMPUtil::ReturnLength(unsigned long ulInputLength) // length of BER length encoding
{
	if(ulInputLength&0xff000000) return 5;
	else if(ulInputLength&0x00ff0000) return 4;
	else if(ulInputLength&0x0000ff00) return 3;
	else if(ulInputLength&0x00000080) return 2;
	return 1;
}

unsigned long CSNMPUtil::ReturnLength(unsigned char* pchBuffer)
{
	unsigned long n = 0xffffffff;
	if(pchBuffer)
	{
//	CString Q;
//	Q.Format("not NULL length");
//	AfxMessageBox(Q);
		if(pchBuffer[0]&0x80)
		{
//	Q.Format("%02x", pchBuffer[0]);
//	AfxMessageBox(Q);
			int bytes = pchBuffer[0]&0x7f;
			if(bytes<5) // otherwise out of range for 32 bit
			{
				int i=0;
				n=0;
				while(i<bytes)
				{
					n |= (pchBuffer[i+1] << ((bytes-i-1)*8));

					i++;
//	Q.Format("now: %d len", n);
//	AfxMessageBox(Q);
				
				}
			}
		}
		else
		{
			n = (unsigned long)(pchBuffer[0]);
//	Q.Format("%d len", n);
//	AfxMessageBox(Q);
		}

	}

	return n;

}

unsigned char*	CSNMPUtil::ReturnMessageBuffer(CSNMPTypeLengthData* pMessage)
{


	return NULL;
}

unsigned char*	CSNMPUtil::ReturnReadableMessageBuffer(CSNMPMessage* pMessage)
{
	if(pMessage)
	{
		CString Q; CBufferUtil  bu;  unsigned long ulHdr=0; char* pchHdr=NULL;

		char*	p = (char*)malloc(1024); // bigger than needed.
		if(p)
		{
			memset(p, 0, 1024); // zero init

			char buffer[1024];

			strcpy(p, "SNMP version ");
		
			sprintf(buffer, "%d\r\ncommunity: %s\r\n", ((pMessage->m_ucVersion==0)?1:pMessage->m_ucVersion), pMessage->m_pchCommunity?pMessage->m_pchCommunity:"(null)");
			strcat(p, buffer);
			
			switch(pMessage->m_ucPDUType)
			{
			case  SNMP_GETREQUESTPDU:	strcat(p, "get_request\r\n"); break;
			case  SNMP_GETRESPONSEPDU:	strcat(p, "get_response\r\n"); break;
			case  SNMP_SETREQUESTPDU:	strcat(p, "set_request\r\n"); break;
			default:	sprintf(buffer, "unknown (0x%02x)\r\n",pMessage->m_ucPDUType); strcat(p, buffer); break;
			}

//			AfxMessageBox(pMessage->m_pchOID);

			if(pMessage->m_ulRequest == 0xffffffff)
			{
				sprintf(buffer, "request id: -1\r\nerror status: 0x%02x\r\nerror index: 0x%02x\r\nOID: %s\r\n", 
					pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex, 
					pMessage->m_pchOID?pMessage->m_pchOID:"(null)");
			}
			else
			{
				sprintf(buffer, "request id: 0x%02x\r\nerror status: 0x%02x\r\nerror index: 0x%02x\r\nOID: %s\r\n", 
					pMessage->m_ulRequest, pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex, 
					pMessage->m_pchOID?pMessage->m_pchOID:"(null)");
			}
			strcat(p, buffer);
			
			if(pMessage->m_pucValue)
			{
				BOOL bAlpha = TRUE;
				unsigned long i=0;
				while(i<pMessage->m_ulValueLength)
				{
					char ch = pMessage->m_pucValue[i];
					if(!((ch>=0x20)&&(ch<0x7f))) // printables.
					{
						bAlpha = FALSE; break;
					}
					i++;
				}

unsigned long ulHdr = pMessage->m_ulValueLength;
char* pchHdr = bu.ReadableHex((char*)pMessage->m_pucValue, &ulHdr, MODE_FULLHEX);
//Q.Format("buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);

				if(bAlpha)
				{
					sprintf(buffer, "value: %s\r\nstring: %s\r\n", pchHdr, (char*)pMessage->m_pucValue);
				}
				else
				{
					sprintf(buffer, "value: %s\r\n", pchHdr);
				}
				if(pchHdr) free(pchHdr);

			}
			else
			{
				if(pMessage->m_ulValueLength)
				{
					sprintf(buffer, "value: %d\r\n", pMessage->m_ulValueLength);
				}
				else
				{
					sprintf(buffer, "value: NULL\r\n");
				}
			}
			strcat(p, buffer);

			unsigned char*	pRV = (unsigned char*)malloc(strlen(p)+1); 
			if(pRV)
			{
				strcpy((char*)pRV, p);
			}


			free(p);

			return pRV;

		}
	}
	else
	{
		unsigned char*	pRV = (unsigned char*)malloc(strlen("Not valid")+1); 
		if(pRV)
		{
			strcpy((char*)pRV, "Not valid");
		}

		return pRV;
	}
	return NULL;
}


unsigned char*	CSNMPUtil::ReturnMessageBuffer(CSNMPMessage* pMessage)
{
	if(pMessage)
	{
		CString Q; CBufferUtil  bu;  unsigned long ulHdr=0; char* pchHdr=NULL;


		int nLen = 0;
		unsigned long q;
		unsigned char* pBuffer=NULL;
		unsigned long ulLen=0;
		unsigned char*	p = (unsigned char*)malloc(1024); // bigger than needed.
		if(p)
		{
			memset(p, 0, 1024); // zero init
			if(pMessage->m_pucValue)
			{
				// invert the value itself.
				q=pMessage->m_ulValueLength;
				
				ReverseBuffer((unsigned char*)pMessage->m_pucValue, &p[nLen], q);
				nLen += q;

				EncodeLength(&pBuffer, &ulLen, q);

				if(pBuffer)
				{
					ReverseBuffer(pBuffer, &p[nLen], ulLen);
					nLen += ulLen;
					delete pBuffer;
					pBuffer = NULL;
				}

				p[nLen] = SNMP_OCTETSTRING;
				nLen++;

//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)p, &ulHdr, MODE_DELIM32BIT);
//Q.Format("buffer len %d; %s", nLen, pchHdr);if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);

			}
			else 
			if(pMessage->m_ulValueLength>0)  // what if we want an integer = 0?  oh never mind
			{
				// give it an integer value
				if(pMessage->m_ulValueLength&0xff000000)
				{
					p[nLen] = (unsigned char)(pMessage->m_ulValueLength&0x000000ff);	nLen++;
					p[nLen] = (unsigned char)((pMessage->m_ulValueLength&0x0000ff00)>>8);	nLen++;
					p[nLen] = (unsigned char)((pMessage->m_ulValueLength&0x00ff0000)>>16);	nLen++;
					p[nLen] = (unsigned char)((pMessage->m_ulValueLength&0xff000000)>>24);	nLen++;
					p[nLen] = 0x04;	nLen++;
				}
				else
				if(pMessage->m_ulValueLength&0x00ff0000)
				{
					p[nLen] = (unsigned char)(pMessage->m_ulValueLength&0x000000ff);	nLen++;
					p[nLen] = (unsigned char)((pMessage->m_ulValueLength&0x0000ff00)>>8);	nLen++;
					p[nLen] = (unsigned char)((pMessage->m_ulValueLength&0x00ff0000)>>16);	nLen++;
					p[nLen] = 0x03;	nLen++;
				}
				else
				if(pMessage->m_ulValueLength&0x0000ff00)
				{
					p[nLen] = (unsigned char)(pMessage->m_ulValueLength&0x000000ff);	nLen++;
					p[nLen] = (unsigned char)((pMessage->m_ulValueLength&0x0000ff00)>>8);	nLen++;
					p[nLen] = 0x02;	nLen++;
				}
				else
				{
					p[nLen] = (unsigned char)(pMessage->m_ulValueLength&0x000000ff);	nLen++;
					p[nLen] = 0x01;	nLen++;
				}
				
				p[nLen] = SNMP_INTEGER;		nLen++;


			}
			else
			{
				// give it NULL value
				p[0] = 0x00;
				p[1] = SNMP_NULL;
				nLen = 2;
//Q.Format("buffer [%02x][%02x]", p[0], p[1]);
//AfxMessageBox(Q);
			}



			if(pMessage->m_pchOID)
			{
				// invert the value itself.
				q=strlen(pMessage->m_pchOID);

				unsigned char* poid = (unsigned char*) pMessage->m_pchOID;

				EncodeOID(&poid, &q);

				if(poid)
				{				

					ReverseBuffer(poid, &p[nLen], q);
					nLen += q;

					pBuffer=NULL;
					ulLen=0;

					EncodeLength(&pBuffer, &ulLen, q);

					if(pBuffer)
					{
						ReverseBuffer(pBuffer, &p[nLen], ulLen);
						nLen += ulLen;
						delete pBuffer;
						pBuffer = NULL;
					}

					p[nLen] = SNMP_OBJID;
					nLen++;
//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)p, &ulHdr, MODE_DELIM32BIT);
//Q.Format("buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);

					delete (poid);
				}

			}

//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)p, &ulHdr, MODE_DELIM32BIT);
//Q.Format("buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);

			// add the var bind

			pBuffer=NULL;
			q=nLen; // total length so far.

			EncodeLength(&pBuffer, &ulLen, q);

			if(pBuffer)
			{
				ReverseBuffer(pBuffer, &p[nLen], ulLen);
				nLen += ulLen;
				delete pBuffer;
				pBuffer = NULL;
			}

			p[nLen] = SNMP_SEQUENCE;
			nLen++;

			// add the var bind list

			q=nLen; // total length so far.

			EncodeLength(&pBuffer, &ulLen, q);

			if(pBuffer)
			{
				ReverseBuffer(pBuffer, &p[nLen], ulLen);
				nLen += ulLen;
				delete pBuffer;
				pBuffer = NULL;
			}

			p[nLen] = SNMP_SEQUENCE;
			nLen++;

//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)p, &ulHdr, MODE_DELIM32BIT);
//Q.Format("buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);

			// add error index, error status, request ID

		//	pMessage->m_ulErrorIndex;

			if(pMessage->m_ulErrorIndex&0xff000000)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorIndex&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorIndex&0x0000ff00)>>8);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorIndex&0x00ff0000)>>16);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorIndex&0xff000000)>>24);	nLen++;
				p[nLen] = 0x04;	nLen++;
			}
			else
			if(pMessage->m_ulErrorIndex&0x00ff0000)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorIndex&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorIndex&0x0000ff00)>>8);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorIndex&0x00ff0000)>>16);	nLen++;
				p[nLen] = 0x03;	nLen++;
			}
			else
			if(pMessage->m_ulErrorIndex&0x0000ff00)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorIndex&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorIndex&0x0000ff00)>>8);	nLen++;
				p[nLen] = 0x02;	nLen++;
			}
			else
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorIndex&0x000000ff);	nLen++;
				p[nLen] = 0x01;	nLen++;
			}
			
			p[nLen] = SNMP_INTEGER;		nLen++;

	//		pMessage->m_ulErrorStatus;

			if(pMessage->m_ulErrorStatus&0xff000000)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorStatus&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorStatus&0x0000ff00)>>8);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorStatus&0x00ff0000)>>16);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorStatus&0xff000000)>>24);	nLen++;
				p[nLen] = 0x04;	nLen++;
			}
			else
			if(pMessage->m_ulErrorStatus&0x00ff0000)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorStatus&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorStatus&0x0000ff00)>>8);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorStatus&0x00ff0000)>>16);	nLen++;
				p[nLen] = 0x03;	nLen++;
			}
			else
			if(pMessage->m_ulErrorStatus&0x0000ff00)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorStatus&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulErrorStatus&0x0000ff00)>>8);	nLen++;
				p[nLen] = 0x02;	nLen++;
			}
			else
			{
				p[nLen] = (unsigned char)(pMessage->m_ulErrorStatus&0x000000ff);	nLen++;
				p[nLen] = 0x01;	nLen++;
			}
			
			p[nLen] = SNMP_INTEGER;		nLen++;

	//		pMessage->m_ulRequest;

			if(pMessage->m_ulRequest == 0xffffffff) //special case -1
			{
				p[nLen] = 0xff;	nLen++;
				p[nLen] = 0x01;	nLen++;
			}
			else
			if(pMessage->m_ulRequest&0xff000000)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulRequest&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulRequest&0x0000ff00)>>8);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulRequest&0x00ff0000)>>16);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulRequest&0xff000000)>>24);	nLen++;
				p[nLen] = 0x04;	nLen++;
			}
			else
			if(pMessage->m_ulRequest&0x00ff0000)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulRequest&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulRequest&0x0000ff00)>>8);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulRequest&0x00ff0000)>>16);	nLen++;
				p[nLen] = 0x03;	nLen++;
			}
			else
			if(pMessage->m_ulRequest&0x0000ff00)
			{
				p[nLen] = (unsigned char)(pMessage->m_ulRequest&0x000000ff);	nLen++;
				p[nLen] = (unsigned char)((pMessage->m_ulRequest&0x0000ff00)>>8);	nLen++;
				p[nLen] = 0x02;	nLen++;
			}
			else
			{
				p[nLen] = (unsigned char)(pMessage->m_ulRequest&0x000000ff);	nLen++;
				p[nLen] = 0x01;	nLen++;
			}
			
			p[nLen] = SNMP_INTEGER;		nLen++;

			// PDU
			q=nLen; // total length so far.

			EncodeLength(&pBuffer, &ulLen, q);

			if(pBuffer)
			{
				ReverseBuffer(pBuffer, &p[nLen], ulLen);
				nLen += ulLen;
				delete pBuffer;
				pBuffer = NULL;
			}

			p[nLen] = pMessage->m_ucPDUType;
			nLen++;
//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)p, &ulHdr, MODE_DELIM32BIT);
//Q.Format("buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);
			
			// community string
			if(pMessage->m_pchCommunity)
			{
				// invert the value itself.
				q=strlen(pMessage->m_pchCommunity);
				
				ReverseBuffer((unsigned char*)pMessage->m_pchCommunity, &p[nLen], q);
				nLen += q;

				EncodeLength(&pBuffer, &ulLen, q);

				if(pBuffer)
				{
					ReverseBuffer(pBuffer, &p[nLen], ulLen);
					nLen += ulLen;
					delete pBuffer;
					pBuffer = NULL;
				}

				p[nLen] = SNMP_OCTETSTRING;
				nLen++;

			}
			else
			{
				// give it NULL value
				p[0] = 0x00;
				p[1] = SNMP_NULL; // SNMP_OCTETSTRING should it be this instead?  since it is a community string?
				nLen += 2; 
			}

			p[nLen] = pMessage->m_ucVersion; 	nLen++;
			p[nLen] = 0x01;	nLen++;
			p[nLen] = SNMP_INTEGER;		nLen++;

			q=nLen; // total length so far.

			EncodeLength(&pBuffer, &ulLen, q);

			if(pBuffer)
			{
				ReverseBuffer(pBuffer, &p[nLen], ulLen);
				nLen += ulLen;
				delete pBuffer;
				pBuffer = NULL;
			}

			p[nLen] = SNMP_SEQUENCE;
			nLen++;

			// Ok now, we reverse the whole buffer and return it.
//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)p, &ulHdr, MODE_DELIM32BIT);
//Q.Format("buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);

			unsigned char*	pRV = (unsigned char*)malloc(nLen+1); 
			if(pRV)
			{
				ReverseBuffer(&p[0], pRV, nLen);
				pRV[nLen]=0; // just zero term for safety

			
//ulHdr = nLen;
//pchHdr = bu.ReadableHex((char*)pRV, &ulHdr, MODE_DELIM32BIT);
//Q.Format("REVERSE buffer len %d; %s", nLen, pchHdr); if(pchHdr) free(pchHdr);
//AfxMessageBox(Q);
			
			}

			free(p);

			return pRV;

		}
	}
	return NULL;
}


int CSNMPUtil::ReverseBuffer(unsigned char* pBufferIn, unsigned char* pBufferOut, unsigned long ulLen)
{
	// have to pass in the length, in case there are zeros in the buffer that we want;
	// we have to assume that the out buffer is of sufficient length
	if((pBufferIn)&&(pBufferOut))
	{
		unsigned long i=0;
		while(i<ulLen)
		{
			pBufferOut[i] = pBufferIn[ulLen-1-i];
			i++;
		}

		return i;

	}

	return -1;
}

/*

CSNMPTypeLengthData* CSNMPUtil::CreateSNMPMessage(unsigned char ucType, char* pchCommunity, unsigned long ulRequest, char* pchOID, char* pchValue)
{
	CSNMPTypeLengthData* p = new CSNMPTypeLengthData; // top level SNMP message obj
	if(p)
	{
		p->m_ucType = SNMP_SEQUENCE;
		p->m_ppvDataObj = (void**) new CSNMPTypeLengthData*[3];
		if(p->m_ppvDataObj)
		{
			p->m_ppvDataObj[0] =  new CSNMPTypeLengthData; // SNMP version
			if(p->m_ppvDataObj[0])
			{
				((CSNMPTypeLengthData*)(p->m_ppvDataObj[0]))->m_ucType = SNMP_INTEGER;
				((CSNMPTypeLengthData*)(p->m_ppvDataObj[0]))->m_ulTotalDataLength = 1;
				((CSNMPTypeLengthData*)(p->m_ppvDataObj[0]))->m_ppvDataObj = (void**) new unsigned char[1];
				if(((CSNMPTypeLengthData*)(p->m_ppvDataObj[0]))->m_ppvDataObj){ *((CSNMPTypeLengthData*)(p->m_ppvDataObj[0]))->m_ppvDataObj = 0x00; }
			}

			p->m_ppvDataObj[1] =  new CSNMPTypeLengthData; // SNMP community string
			if(p->m_ppvDataObj[1])
			{
				((CSNMPTypeLengthData*)(p->m_ppvDataObj[1]))->m_ucType = SNMP_OCTETSTRING;
				((CSNMPTypeLengthData*)(p->m_ppvDataObj[1]))->m_ulTotalDataLength = 0; // leave this null for filling in later.
			}

			p->m_ppvDataObj[2] =  new CSNMPTypeLengthData; // SNMP PDU!
			if(p->m_ppvDataObj[2])
			{
				CSNMPTypeLengthData* ppdu = ((CSNMPTypeLengthData*)(p->m_ppvDataObj[2]));
				ppdu->m_ucType = ucType;
				ppdu->m_ppvDataObj = CreateTLDArray(4);
				if(ppdu->m_ppvDataObj)
				{
					ppdu->m_ulNumDataObj=4;
					CSNMPTypeLengthData** pp = (CSNMPTypeLengthData**)ppdu->m_ppvDataObj;
					pp[0]->m_ucType = SNMP_INTEGER; // request ID
					// going to leave the integer array itself null for now, to be inserted later
					pp[1]->m_ucType = SNMP_INTEGER; // error status
					// going to leave the integer array itself null for now, to be inserted later
					pp[2]->m_ucType = SNMP_INTEGER; //error index
					// going to leave the integer array itself null for now, to be inserted later
					pp[3]->m_ucType = SNMP_SEQUENCE; //varbind list
					// here we go.
					// only one varbind for snmp pdu
					pp[3]->m_ppvDataObj = CreateTLDArray(1);
					if(pp[3]->m_ppvDataObj)
					{
						pp[3]->m_ulNumDataObj=1;
						CSNMPTypeLengthData** ppvbl = (CSNMPTypeLengthData**)pp[3]->m_ppvDataObj;
						ppvbl[0]->m_ppvDataObj = CreateTLDArray(2);
						if(ppvbl[0]->m_ppvDataObj)
						{
							ppvbl[0]->m_ulNumDataObj=2;
							CSNMPTypeLengthData** ppvb = (CSNMPTypeLengthData**)ppvbl[0]->m_ppvDataObj;

							ppvb[0]->m_ucType = SNMP_OBJID;
							ppvb[1]->m_ucType = SNMP_NULL;
						}

					}



				}
			}

			p->m_ulNumDataObj=3;
		}

		return p;
	}

	return NULL;
}
*/

void**	CSNMPUtil::CreateTLDArray(unsigned long nNumObjects)
{
	CSNMPTypeLengthData** p = new CSNMPTypeLengthData*[nNumObjects];
	if(p)
	{
		unsigned long i=0;
		
		while(i<nNumObjects)
		{
			p[i] = new CSNMPTypeLengthData;
			i++;
		}
	}
	return (void**)p;
}

CSNMPMessage* CSNMPUtil::CreateSNMPMessageFromBuffer(unsigned char* pucMessage)
{
	// the buffer length begins at the second byte;
	if(pucMessage)
	{
		CSNMPMessage* p = new CSNMPMessage;
		if(p)
		{
			unsigned long ulBuflen = ReturnLength(pucMessage+1);
			unsigned long i = ReturnLength(ulBuflen)+1;
			unsigned long ulFieldlen = 0;

			// next should be version, as an integer.
			i++; //skips the integer type byte
			ulFieldlen = ReturnLength(pucMessage+i); // should be 1.
			i+=ReturnLength(ulFieldlen);
			// now we get to the value part
			p->m_ucVersion = *(pucMessage+i); // should be one byte only
			i++; // skips the value part

			//next is community string.
			i++; //skips the type byte
			ulFieldlen = ReturnLength(pucMessage+i); // can be anything.
			i+=ReturnLength(ulFieldlen);

			char* pch = (char*)malloc(ulFieldlen+1);
			if(pch) 
			{
				memset(pch, 0, ulFieldlen+1);
				memcpy(pch, pucMessage+i, ulFieldlen);
			}
			i+=ulFieldlen; // skips the value part
			p->m_pchCommunity = pch;

			//next is PDU
			p->m_ucPDUType = *(pucMessage+i); // should be one byte only
			i++; //skips the type byte
			ulFieldlen = ReturnLength(pucMessage+i);
			i+=ReturnLength(ulFieldlen);

			// next is request ID, an integer
			i++; //skips the integer type byte
			ulFieldlen = ReturnLength(pucMessage+i); // should be 1, but have seen 4.
			i+=ReturnLength(ulFieldlen);
			// now we get to the value part
			if(*(pucMessage+i) == 0xff) // special case
			{
				p->m_ulRequest = 0xffffffff;
				i++;
			}
			else
			{
				switch(ulFieldlen)
				{
				case 1: 
					{
						p->m_ulRequest = *(pucMessage+i); 
						i++; // skips the value part
					} break;
				case 2: 
					{
						p->m_ulRequest = (*(pucMessage+i)<<8)|(*(pucMessage+i+1)); 
						i+=2; // skips the value part
					} break;
				case 3: 
					{
						p->m_ulRequest = (*(pucMessage+i)<<16)|(*(pucMessage+i+1)<<8)|(*(pucMessage+i+2)); 
						i+=3; // skips the value part
					} break;
				case 4: 
					{
						p->m_ulRequest = (*(pucMessage+i)<<24)|(*(pucMessage+i+1)<<16)|(*(pucMessage+i+2)<<8)|(*(pucMessage+i+3)); 
						i+=4; // skips the value part
					} break;
				}
			}
			
			// next is error status, an integer
			i++; //skips the integer type byte
			ulFieldlen = ReturnLength(pucMessage+i); // should be 1, but have seen 4.

//CString Q; Q.Format("ulFieldlen %d; index %d", ulFieldlen, i);
//AfxMessageBox(Q);

			i+=ReturnLength(ulFieldlen);

//Q.Format("ulFieldlen %d; index %d", ulFieldlen, i);
//AfxMessageBox(Q);
			// now we get to the value part
			if(*(pucMessage+i) == 0xff) // special case
			{
				p->m_ulErrorStatus = 0xffffffff;
				i++;
			}
			else
			{
				switch(ulFieldlen)
				{
				case 1: 
					{
						p->m_ulErrorStatus = *(pucMessage+i); 
						i++; // skips the value part
					} break;
				case 2: 
					{
						p->m_ulErrorStatus = (*(pucMessage+i)<<8)|(*(pucMessage+i+1)); 
						i+=2; // skips the value part
					} break;
				case 3: 
					{
						p->m_ulErrorStatus = (*(pucMessage+i)<<16)|(*(pucMessage+i+1)<<8)|(*(pucMessage+i+2)); 
						i+=3; // skips the value part
					} break;
				case 4: 
					{
						p->m_ulErrorStatus = (*(pucMessage+i)<<24)|(*(pucMessage+i+1)<<16)|(*(pucMessage+i+2)<<8)|(*(pucMessage+i+3)); 
						i+=4; // skips the value part
					} break;
				}
			}
			
			// next is error index, an integer
			i++; //skips the integer type byte
			ulFieldlen = ReturnLength(pucMessage+i); // should be 1, but have seen 4.
			i+=ReturnLength(ulFieldlen);
			// now we get to the value part
			if(*(pucMessage+i) == 0xff) // special case
			{
				p->m_ulErrorIndex = 0xffffffff;
				i++;
			}
			else
			{
				switch(ulFieldlen)
				{
				case 1: 
					{
						p->m_ulErrorIndex = *(pucMessage+i); 
						i++; // skips the value part
					} break;
				case 2: 
					{
						p->m_ulErrorIndex = (*(pucMessage+i)<<8)|(*(pucMessage+i+1)); 
						i+=2; // skips the value part
					} break;
				case 3: 
					{
						p->m_ulErrorIndex = (*(pucMessage+i)<<16)|(*(pucMessage+i+1)<<8)|(*(pucMessage+i+2)); 
						i+=3; // skips the value part
					} break;
				case 4: 
					{
						p->m_ulErrorIndex = (*(pucMessage+i)<<24)|(*(pucMessage+i+1)<<16)|(*(pucMessage+i+2)<<8)|(*(pucMessage+i+3)); 
						i+=4; // skips the value part
					} break;
				}
			}
			
			// next is varbind list
			i++; //skips the type byte
			ulFieldlen = ReturnLength(pucMessage+i); // should be 1.
			i+=ReturnLength(ulFieldlen);


			// next is varbind
			i++; //skips the type byte
			ulFieldlen = ReturnLength(pucMessage+i); // should be 1.
			i+=ReturnLength(ulFieldlen);


			// next is the OID.
			i++; //skips the type byte
			ulFieldlen = ReturnLength(pucMessage+i); // can be anything
			i+=ReturnLength(ulFieldlen);

			unsigned char* poid = pucMessage+i;

			i+=ulFieldlen; // skip the value part

			DecodeOID(&poid, &ulFieldlen);
			if(poid)
			{
				p->m_pchOID = (char*)malloc(ulFieldlen+1);
				if(p->m_pchOID)
				{
					strcpy(p->m_pchOID, (char*)poid);
				}
				delete poid;
			}

			// next is the value.
			if((*(pucMessage+i))==SNMP_INTEGER)
			{
				i++; //skips the type byte
				ulFieldlen = ReturnLength(pucMessage+i); // should be 1.
				i+=ReturnLength(ulFieldlen);

				if(*(pucMessage+i) == 0xff) // special case
				{
					p->m_ulValueLength = 0xffffffff;
					i++;
				}
				else
				{
					switch(ulFieldlen)
					{
					case 1: 
						{
							p->m_ulValueLength = *(pucMessage+i); 
							i++; // skips the value part
						} break;
					case 2: 
						{
							p->m_ulValueLength = (*(pucMessage+i)<<8)|(*(pucMessage+i+1)); 
							i+=2; // skips the value part
						} break;
					case 3: 
						{
							p->m_ulValueLength = (*(pucMessage+i)<<16)|(*(pucMessage+i+1)<<8)|(*(pucMessage+i+2)); 
							i+=3; // skips the value part
						} break;
					case 4: 
						{
							p->m_ulValueLength = (*(pucMessage+i)<<24)|(*(pucMessage+i+1)<<16)|(*(pucMessage+i+2)<<8)|(*(pucMessage+i+3)); 
							i+=4; // skips the value part
						} break;
					}
				}

			}
			else
			if((*(pucMessage+i))==SNMP_OCTETSTRING)
			{
				i++; //skips the type byte
				ulFieldlen = ReturnLength(pucMessage+i);
				i+=ReturnLength(ulFieldlen);

				p->m_ulValueLength = ulFieldlen;
				p->m_pucValue = (unsigned char*)malloc(ulFieldlen+1);
				memcpy(p->m_pucValue, pucMessage+i, ulFieldlen);
				memset(p->m_pucValue+ulFieldlen, 0, 1);  // safety term zero in case a string

			}
			// else leave it NULL;

			return p;
		}
	}
	return NULL;
}

CSNMPMessage* CSNMPUtil::CreateSNMPMessage(unsigned char ucPDUType, char* pchCommunity, char* pchOID, unsigned char* pucValue, unsigned long ulValueLength)
{
	CSNMPMessage* p = new CSNMPMessage;
	if(p)
	{
		p->m_ucPDUType = ucPDUType;
		if(pchCommunity)
		{
			p->m_pchCommunity = (char*)malloc(strlen(pchCommunity)+1);
			if(p->m_pchCommunity) strcpy(p->m_pchCommunity, pchCommunity);
		}
		if(pchOID)
		{
			p->m_pchOID = (char*)malloc(strlen(pchOID)+1);
			if(p->m_pchOID) strcpy(p->m_pchOID, pchOID);
		}
		if(pucValue)
		{
			p->m_pucValue = (unsigned char*)malloc(ulValueLength+1);
			if(p->m_pucValue) memcpy(p->m_pucValue, pucValue, ulValueLength);
			p->m_ulValueLength = ulValueLength;
		}
		// else let's let the ulValueLength stand in for an integer value.
		p->m_ulValueLength = ulValueLength;

	}

	return p;

}
