// SNMPUtil.h: interface for the CSNMPUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SNMPUTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_)
#define AFX_SNMPUTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef NULL
#define NULL 0
#endif


#define SNMP_EOC							0x00
#define SNMP_BOOLEAN					0x01
#define SNMP_INTEGER					0x02
#define SNMP_OCTETSTRING			0x04
#define SNMP_NULL							0x05
#define SNMP_OBJID						0x06

#define SNMP_SEQUENCE					0x30
#define SNMP_GETREQUESTPDU		0xA0
#define SNMP_GETRESPONSEPDU		0xA2
#define SNMP_SETREQUESTPDU		0xA3
	

class CSNMPTypeLengthData 
{
public:
	CSNMPTypeLengthData();
	virtual ~CSNMPTypeLengthData();

	unsigned char		m_ucType;
	unsigned long		m_ulTotalDataLength;
	void**	m_ppvDataObj;
	unsigned long	m_ulNumDataObj;

	int UpdateDataLengths();
};

class CSNMPMessage 
{
public:
	CSNMPMessage();
	virtual ~CSNMPMessage();

	unsigned char		m_ucVersion;
	char*		m_pchCommunity; // zero-terminated string
	unsigned char		m_ucPDUType;
	unsigned long		m_ulRequest;
	unsigned long		m_ulErrorStatus;
	unsigned long		m_ulErrorIndex;
	char*		m_pchOID; // dotted string  // zero-terminated string
	unsigned char*	m_pucValue; // can't be a zero-terminated string unless encoded....
	unsigned long		m_ulValueLength;
};



class CSNMPUtil  
{
public:
	CSNMPUtil();
	virtual ~CSNMPUtil();

	unsigned char m_ucLastMessageNumber;

	int DecodeOID(unsigned char* pBuffer, unsigned long* pulValue); // this form takes a buffer with an OID value, and just parses the one vlaue and returns the value.
	int EncodeOID(unsigned char** ppBuffer, unsigned long* pulLen, unsigned long ulInputOID); // this form takes a single integer and spits out a few bytes of encoded value // not valid for fist two dotted values
	int EncodeOID(unsigned char** ppBuffer, unsigned long* pulLen); // this form takes a string of dotted integer values in the passed in buffer, parses it, and returns a new buffer with the entire encoded OID
	int DecodeOID(unsigned char** ppBuffer, unsigned long* pulLen); // this form takes a buffer with the entire encoded OID, parses it, and returns a new buffer with a string of dotted integer values
	int EncodeLength(unsigned char** ppBuffer, unsigned long* pulLen, unsigned long ulInputLength); // BER length encoding
	int ReturnLength(unsigned long ulInputLength); // length of BER length encoding
	unsigned long ReturnLength(unsigned char* pchBuffer);

	unsigned char*	ReturnMessageBuffer(CSNMPTypeLengthData* pMessage);
	unsigned char*	ReturnMessageBuffer(CSNMPMessage* pMessage);
	unsigned char*	ReturnReadableMessageBuffer(CSNMPMessage* pMessage);
//	CSNMPTypeLengthData* CreateSNMPMessage(unsigned char ucType);
	void**	CreateTLDArray(unsigned long nNumObjects);
	CSNMPMessage* CreateSNMPMessage(unsigned char ucPDUType, char* pchCommunity, char* pchOID, unsigned char* pucValue, unsigned long ulValueLength);
	CSNMPMessage* CreateSNMPMessageFromBuffer(unsigned char* pucMessage);
	int	ReverseBuffer(unsigned char* pBufferIn, unsigned char* pBufferOut, unsigned long ulLen);

	int IncrementMessageNumber();

};

#endif // !defined(AFX_SNMPUTIL_H__F9FAC129_1788_40B3_9D43_5832040440F9__INCLUDED_)
