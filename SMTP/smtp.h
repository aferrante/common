#ifndef SMTP_INCLUDED
#define SMTP_INCLUDED

#include "..\LAN\NetUtil.h"
#include "..\TXT\BufferUtil.h"
#include <stdio.h>

#define MAXLEN_USER    64
#define MAXLEN_DOMAIN  64
#define MAXLEN_CMD     512
#define MAXLEN_REPLY   512
#define MAXLEN_TEXT    1024
#define MAXBUF_RCPT    100
#define MAXLEN_ATTACHLINE      74 //72+2 for crlf
#define MAXLEN_ATTACHLINEPRE   54 //pre-encoded - after base 64 encode, becomes 72 chars
#define MAXNUM_ATTACHMENTS     64 


#define MAILLOG_ERROR 0
#define MAILLOG_SENDERROR 1
#define MAILLOG_RECVERROR 2
#define MAILLOG_SEND  3
#define MAILLOG_RECV  4
#define MAILLOG_CLOSE 5
#define MAILLOG_ENCODE 6

#define SMTP_SUCCESS   0
#define SMTP_ERROR	   -1
#define SMTP_REPLY_SVCREADY   1
#define SMTP_REPLY_CMDOK      0
#define SMTP_REPLY_FATAL     -1
#define SMTP_ERROR_NORCPT    -500
#define SMTP_ERROR_BUFFER    -9

class CSMTP// : public CMessagingObject
{
// Construction
public:
	CSMTP();   // standard constructor
	~CSMTP();   // standard destructor

private:
  CNetUtil m_net;
	CBufferUtil  m_bu;
	char m_szLogFile[MAX_PATH];
	bool m_bFileBusy;
	FILE* m_fp;
	bool  m_bLogActivity;
	char* m_pszDefaultDomain;

public:
  char* EnsureNoBareLFs(char* pchText, bool bFreeIncomingBuffer=false);
	bool ChangeLogFilename(char szLogFile[]);
	bool ChangeLogFileUsage(bool bLogActivity);

  int SendMail(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszMsg, char* pszAddlHeaders="", 
								char* pszAttachFiles=NULL, 
								short nPort=25, unsigned long* pulBytesWritten=NULL, unsigned long* pulBytesTotal=NULL);

  int SendMailFromTextFile(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszFilename, char* pszAddlHeaders="", 
								char* pszAttachFiles=NULL, 
								short nPort=25, unsigned long* pulBytesWritten=NULL, unsigned long* pulBytesTotal=NULL);

	// common overrides
  int SendMail(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszMsg);

  int SendMailFromTextFile(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszFilename);

private:
  int SendMailInternals(char* pszMailServer, 
												char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject,
                        char* pszData, bool bUseFile=FALSE,  
                        char* pszAddlHeaders="", 
												char* pszAttachFiles=NULL, 
												short nPort=25, unsigned long* pulBytesWritten=NULL, unsigned long* pulBytesTotal=NULL
												);

  bool SendText(char* pszInput, SOCKET s, unsigned long* pulBytesWritten=NULL, unsigned long* pulBytesTotal=NULL);
	bool SendAttachment(char* pszFileName, char* pszEncodedData, char* pszBoundary, SOCKET s, unsigned long* pulBytesWritten=NULL, unsigned long* pulBytesTotal=NULL);
	bool EncodeAttachment(char* pszFileName, char** ppszEncodedData, unsigned long* pulTotalBytes);
  void Log(char* pszText, int nType=MAILLOG_ERROR);
  int InterpretCode(unsigned char* pucText);
};

#endif
