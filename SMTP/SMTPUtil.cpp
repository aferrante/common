// LogUtil.cpp: implementation of the CSMTPUtil class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "SMTP.h"
#include "SMTPUtil.h"
#include "../TXT/BufferUtil.h"
//#include <time.h>
#include <direct.h>
#include <process.h>
//#include <winnt.h>
//#include <windef.h>
//#include <winbase.h> // needed to halt thread execution with Sleep. // now using stdafx.


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//thread process;
void SMTPHandlerThread(void* pvArgs);
void SMTPQueueMessageThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMTPUtil::CSMTPUtil()
{
	m_bThreadStarted = false;
	m_ucType = MSG_DESTTYPE_EMAIL;
	m_bServerInUse = false;
//	m_bBufferInUse = false;
	m_bAllowRepeat=false;
	m_bKillThread = true;
//	m_bVerboseFile = false;
	m_pchSendBuffer=NULL;

	smtpmsgLastMessage.ulFlags=0;
	smtpmsgLastMessage.pszMessage=NULL;
	smtpmsgLastMessage.pszCaller=NULL;
	smtpmsgLastMessage.pszDestinations=NULL;
	smtpmsgLastMessage.pszFullMessage=NULL;
	smtpmsgLastMessage.pUtil=NULL;

	m_pchNewFileBuffer=NULL;
	m_ulBufferLength=0;
	m_ulNewFileBufferLength=0;
	m_nAbsoluteTimeOffsetMS=0;
	m_ulLastUnixTime=0;
	m_usLastMillitime=0;
	m_ulLastMessageCount=0;

//	m_pszParams = NULL;
	m_pszLocalHostname = NULL;
	m_pszFromSpec=NULL;
	m_pszHostname=NULL;
	m_nPort=25;
	m_pszLogFilename=NULL;
	m_bLogErrors=false;
	m_bLogInfo=false;

//	m_pfile=NULL;
//	m_pszFileBaseName=NULL;
//	m_pszDestinationTable=NULL;
//	m_pszFunctionTable=NULL;
//	m_pszFilenameFormatSpec = NULL;
//	m_pszLastRotateKey = (char*)malloc(13);
//	if(m_pszLastRotateKey) sprintf(m_pszLastRotateKey, "000000000000");

/*
	m_chOverwritePeriod='Y';
	m_chRotatePeriod='M';
	m_nRotateOffsetMonth=1;
	m_nRotateOffsetDay=1;
	m_nRotateOffsetHours=0;
	m_nRotateOffsetMinutes=0;
*/

	m_ulReqCount=0;
	m_ulSvcCount=0;

	m_ppAddresses=NULL;
	m_nNumAddresses = 0;
	m_pszSubjectSpec=NULL;

	InitializeCriticalSection(&m_critBuffer);
	InitializeCriticalSection(&m_critAddresses);
	InitializeCriticalSection(&m_critQueue);
	m_nQueueThreads = 0;


}

CSMTPUtil::~CSMTPUtil()
{
//	if(m_pszLastRotateKey) free(m_pszLastRotateKey);
//	m_pszLastRotateKey = NULL;
	KillThread();
	DeleteCriticalSection(&m_critBuffer);
	DeleteCriticalSection(&m_critQueue);
//	if(m_pszParams) free(m_pszParams);
	if(m_pszHostname) free(m_pszHostname);
	if(m_pszLogFilename) free(m_pszLogFilename);
	if(m_pszSubjectSpec) free(m_pszSubjectSpec);
	if(m_pszLocalHostname) free(m_pszLocalHostname);
	
	if(smtpmsgLastMessage.pszMessage) free(smtpmsgLastMessage.pszMessage);
	if(smtpmsgLastMessage.pszCaller) free(smtpmsgLastMessage.pszCaller);
	if(smtpmsgLastMessage.pszDestinations) free(smtpmsgLastMessage.pszDestinations);

	EnterCriticalSection(&m_critAddresses);

	if(m_ppAddresses)
	{
		int i=0;
		while(i<m_nNumAddresses)
		{
			if(m_ppAddresses[i])
			{
				if(m_ppAddresses[i]->pszAddress) free(m_ppAddresses[i]->pszAddress);
				delete m_ppAddresses[i];
			}
			i++;
		}
		delete [] m_ppAddresses;
	}

	m_ppAddresses=NULL;
	m_nNumAddresses=0;

	LeaveCriticalSection(&m_critAddresses);
	DeleteCriticalSection(&m_critAddresses);
	
}

//////////////////////////////////////////////////////////////////////
// Design Philosophy
//////////////////////////////////////////////////////////////////////
// SMTP messager is based on the log messager.  here are those comments for now, much of it is the same.
// however, there is no compressed stuff.
// Log Files are typically time stamped ASCII text, that get large.
// To conserve space, files are written in a binary format.
// Log files have had the following information per record:
// Timestamp | Message | Severity | Calling Function | Icon | Destinations
// Timestamp has usually been the absolute machine timestamp, to the millisecond
// Message is the information to be logged.
// Severity is how severe the error (if it is an error) is.
// Calling function is the name of the function that sends the information
// Icon is a graphical icon associated with the message if any is desired
// Destinations are other places (such as email or a GUI) that this message has been sent.
// To conserve space, the following format is adopted:
// There are two types of records: table records and item records.
// A table record is reference information required to parse the records in the file.
// It has the following form:
// [1 byte - the ref data type - the first bit is 1][information, variable bytes (char 10 encoded as "|~", "|" encoded as "||")][char 10 (newline)]

// there are 3 reference data types:
// 1. reference time.  the absolute time from which the entries are timestamped with an offset.
//    The byte value is 0x80.  The reference time is recorded as 4 bytes unixtime, 2 bytes millisecond offset.
// 2. calling function name record.
//    The byte value is 0x81.  The information is one byte for 255 functions tabulated, 0x00 is not valid. followed by ascii function name, no char 10 allowed.
// 3. destination name record.
//    The byte value is 0x82.  The information is one byte for 255 destinations tabulated, 0x00 is not valid. followed by ascii destination name, no char 10 allowed.
// ...
// files should start with the reference time (or appended upon opening), and the reference time should be updated every 8388607 milliseconds.
// An item record is a logged item, and has the following form:
// [3 bytes - time in milliseconds elapsed since last table entry for reference time - the first bit is zero][1 byte, calling function][1 byte, severity and icon][1 byte, destinations][information, variable bytes (char 10 encoded as "|~", "|" encoded as "||")][char 10 (newline)]
// 
// a viewer will be required to view these files, unless they are decompiled to text.
// utilities for such decompilation should be in this code.
// one file per object.


int CSMTPUtil::InitializeThread(char* pszSMTPParams)
{
// to keep time offset as close as possible, calculate it first
	_timeb timebuffer;
  _ftime( &timebuffer );

	unsigned long ulTime=0;
	// pszSMTPParams holds the following parameters:
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

	KillThread();

	char* pszParams = NULL;
	if(pszSMTPParams)
	{
		pszParams = (char*)malloc(strlen(pszSMTPParams)+1);
		if(pszParams)
		{
			strcpy(pszParams, pszSMTPParams);
		}
	}

	if(pszParams==NULL) return SMTP_ERROR;

	// here we have to decompile the buffer into the actual settings.

	if (strlen(pszParams)>0) 
	{
		m_bLogErrors=false;
		m_bLogInfo = false;
		m_bAllowRepeat= false;

		bool bFinalParam = false;
//			char* pch = strtok( pszParams, "|" );
		char* pch = strchr( pszParams, '|' );
		if(pch!=NULL)
		{
			*pch = 0;
		}
		else bFinalParam = true;
		pch = pszParams;

		char* pchTemp = NULL;
		int j=0;
		CBufferUtil bu;
		while(pch!=NULL)
		{
			switch(j)
			{
			case 0:	// SMTP address[: override port (optional)] 
				{
					if(strlen(pch))
					{
						if(m_pszHostname) free(m_pszHostname);
						m_pszHostname = (char*)malloc(strlen(pch)+1);
						if(m_pszHostname)
						{
							strcpy(m_pszHostname, pch);
//AfxMessageBox(m_pszHostname);
							pchTemp = strchr( m_pszHostname, ':' );
							if(pchTemp)
							{
								m_nPort = atoi(pchTemp+1);
								*pchTemp = 0;  // term.
							}
						}
					}
				}
				break;
			case 1: //distribution lists
				{
//					CSafeBufferUtil sbu;
//					pchTemp = sbu.Token();
	EnterCriticalSection(&m_critAddresses);

					if(m_ppAddresses)
					{
						int i=0;
						while(i<m_nNumAddresses)
						{
							if(m_ppAddresses[i])
							{
								if(m_ppAddresses[i]->pszAddress) free(m_ppAddresses[i]->pszAddress);
								delete m_ppAddresses[i];
							}
							i++;
						}
						delete [] m_ppAddresses;
					}

					m_ppAddresses=NULL;
					m_nNumAddresses=0;

					char* pchBuffer = pch;
//AfxMessageBox(pch);
					pchTemp = pch;
					while(pchBuffer!=NULL)
					{
						pchBuffer = strchr(pchTemp, ';');
						if(pchBuffer)
						{
							*pchBuffer = 0;
						}

						if(strlen(pchTemp)>3)
						{

							int nType=-1;
							int nSeverity=-1;

							switch(*pchTemp)
							{
							case '0': nSeverity = 0; break;
							case '1': nSeverity = 1; break;
							case '2': nSeverity = 2; break;
							case '3': nSeverity = 3; break;
							case '4': nSeverity = 4; break;
							case 'F':
							case 'f': nSeverity = 16; break;
							case 'X':
							case 'x':
							default: nSeverity = -1; break;
							}

							switch(*(pchTemp+1))
							{
							case '0': nType = 0; break;
							case '1': nType = 1; break;
							case '2': nType = 2; break;
							case '3': nType = 3; break;
							case '4': nType = 4; break;
							case '5': nType = 5; break;
							case '6': nType = 6; break;
							case '7': nType = 7; break;
							case '8': nType = 8; break;
							case 'F':
							case 'f': nType = 16; break;
							case 'X':
							case 'x':
							default: nType = -1; break;
							}

							char* pchAddBuffer = pchTemp+3;
							char* pchTemp = pchAddBuffer;
							while(pchAddBuffer!=NULL)
							{
								pchAddBuffer = strchr(pchTemp, ',');
								if(pchAddBuffer)
								{
									*pchAddBuffer = 0;
								}

								if(strlen(pchTemp))
								{

									SMTPAddress_t* pAddress = new SMTPAddress_t;
									if(pAddress)
									{
										pAddress->ulFlags = MSG_ICONNONE;
										pAddress->nType=nType;
										pAddress->nSeverity=nSeverity;

										pAddress->pszAddress = (char*)malloc(strlen(pchTemp)+1);
										if(pAddress->pszAddress)
										{
											strcpy(pAddress->pszAddress, pchTemp);
//										CString foo; foo.Format("%s %d %d",pAddress->pszAddress,pAddress->nSeverity,pAddress->nType); AfxMessageBox(foo);
											InsertAddress(pAddress);
										}
										else
										{	
											delete pAddress; 
										}
									}
								}//strlen 
								if(pchAddBuffer)
								{
									*pchAddBuffer = ',';  // add back so that the strlen pch works
									pchTemp = pchAddBuffer+1;
								}

							} // while addbuf
						}//if(strlen(pchTemp)>3)  // 3 because the format is ff: then the addresses

						if(pchBuffer)
						{
							(*pchBuffer)=';'; // put this back so that strlen pch works out later.
							pchTemp = pchBuffer+1;
						}
					}
	LeaveCriticalSection(&m_critAddresses);

				}
				break;
				 
			case 2: // from email | 
				{
					if(strlen(pch))
					{
						if(m_pszFromSpec) free(m_pszFromSpec);
						m_pszFromSpec = (char*)malloc(strlen(pch)+1);
						if(m_pszFromSpec)
						{
							strcpy(m_pszFromSpec, pch);
//							AfxMessageBox(m_pszFromSpec);
						}
					}
				}
				break;
			case 3: // subject spec
				{
					if(strlen(pch))
					{
						if(m_pszSubjectSpec) free(m_pszSubjectSpec);
						m_pszSubjectSpec = (char*)malloc(strlen(pch)+1);
						if(m_pszSubjectSpec)
						{
							strcpy(m_pszSubjectSpec, pch);
//							AfxMessageBox(m_pszSubjectSpec);
						}
					}
				}
				break;
			case 4: // local host name
				{
					if(strlen(pch))
					{
						if(m_pszLocalHostname) free(m_pszLocalHostname);
						m_pszLocalHostname = (char*)malloc(strlen(pch)+1);
						if(m_pszLocalHostname)
						{
							strcpy(m_pszLocalHostname, pch);
						}
					}
				}
				break;
			case 5: // log filename
				{
					if(strlen(pch))
					{
						if(m_pszLogFilename) free(m_pszLogFilename);
						m_pszLogFilename = (char*)malloc(strlen(pch)+1);
						if(m_pszLogFilename)
						{
							strcpy(m_pszLogFilename, pch);
						}
					}
				}
				break;
			case 6: // log errors
				{
					if((strlen(pch))&&(atoi(pch)!=0)) m_bLogErrors=true;
				}
				break;
			case 7: // log non-errors
				{
					if((strlen(pch))&&(atoi(pch)!=0)) m_bLogInfo=true;
				}
				break;
			case 8: // allow repeat msgs
				{
					if((strlen(pch))&&(atoi(pch)!=0)) m_bAllowRepeat=true;
				}
				break;
			case 9: // offset unixtime
				{
					if((strlen(pch))&&(atoi(pch)!=0))
					{
						ulTime=atoi(pch);
						m_nAbsoluteTimeOffsetMS=(timebuffer.time-ulTime)*1000;
					}
				}
				break; 
			case 10: // millitime offset
				{
					if((strlen(pch))&&(atoi(pch)!=0))
					{
						m_nAbsoluteTimeOffsetMS+=(timebuffer.millitm-(atoi(pch)));
					}
				}
				break; 
			}
			j++;
			//pch = strtok( NULL, "|" );

			pszParams += (strlen(pch)+1);  // alters the params buffer pointer

			if(bFinalParam)
			{
				// detected no more delimiters last time.
				pch = NULL;
			}
			else
			if(*pszParams == 0)
			{
				// end of param string
				pch = NULL;
			}
			else
			{
				pch = strchr( pszParams, '|' );
				if(pch!=NULL)
				{
					*pch = 0;
				}
				else bFinalParam = true;
				pch = pszParams;
			}
		}
	}
	


	m_bKillThread = false;
	if(_beginthread( SMTPHandlerThread, 0, (void*)(this) )==-1)
	{
		return SMTP_ERROR;
	}
	return SMTP_SUCCESS;
	
}

char* CSMTPUtil::AssembleSubject(SMTPMessage_t* pmsg)
{
	if((m_pszSubjectSpec)&&(pmsg))
	{
		char pszOutput[MAXLEN_TEXT];
		int nInLen=strlen(m_pszSubjectSpec);
		int nOutLen=0;
		char* pch = m_pszSubjectSpec;
		char* pchEnd = min((pch+MAXLEN_TEXT),(m_pszSubjectSpec+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %m  message text - not good to use in subj
				// %c  callers
				// %d  destinations
				// %h  local hostname
				// %t  message type, numerical
				// %p  message priority, numerical
				// %T  message type, alpha
				// %P  message priority, alpha
				
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'm')
					{
						if((pmsg->pszMessage)&&(strlen(pmsg->pszMessage)))
						{
							char* pszTemp = (char*)malloc(strlen(pmsg->pszMessage)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, pmsg->pszMessage);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					if(*pch == 'c')
					{
						if((pmsg->pszCaller)&&(strlen(pmsg->pszCaller)))
						{
							char* pszTemp = (char*)malloc(strlen(pmsg->pszCaller)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, pmsg->pszCaller);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					if(*pch == 'd')
					{
						if((pmsg->pszDestinations)&&(strlen(pmsg->pszDestinations)))
						{
							char* pszTemp = (char*)malloc(strlen(pmsg->pszDestinations)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, pmsg->pszDestinations);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					if(*pch == 't')
					{
						char* pszTemp = (char*)malloc(24);
						if(pszTemp)
						{
							_snprintf(pszTemp, 24, "%d", (short)(pmsg->ulFlags&MSG_ICONMASK));
						
							char* pchPlus = pszTemp;
							while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
							{
								pszOutput[nOutLen]= *pchPlus;
								nOutLen++;
								pchPlus++;
							}
							free(pszTemp);
						}
						pch++;
					}
					else
					if(*pch == 'p')
					{
						char* pszTemp = (char*)malloc(24);
						if(pszTemp)
						{
							_snprintf(pszTemp, 24, "%d", (short)((pmsg->ulFlags&MSG_PRI_MASK)>>4));
						
							char* pchPlus = pszTemp;
							while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
							{
								pszOutput[nOutLen]= *pchPlus;
								nOutLen++;
								pchPlus++;
							}
							free(pszTemp);
						}
						pch++;
					}
					else
					if(*pch == 'T')
					{
						char* pszTemp = (char*)malloc(24);
						if(pszTemp)
						{
							switch(pmsg->ulFlags&MSG_ICONMASK)
							{
							case MSG_ICONQUESTION://						0x00000001  // ? icon
								_snprintf(pszTemp, 24, "Question"); break;
							case MSG_ICONEXCLAMATION://					0x00000002  // ! icon
								_snprintf(pszTemp, 24, "Alert"); break;
							case MSG_ICONERROR://								0x00000003  // X icon
								_snprintf(pszTemp, 24, "Error"); break;
							case MSG_ICONSTOP://								0x00000004  // stop sign icon
								_snprintf(pszTemp, 24, "Stop"); break;
							case MSG_ICONHAND://								0x00000005  // hand icon
								_snprintf(pszTemp, 24, "Signal"); break;
							case MSG_ICONINFO://								0x00000006  // (i) icon 
								_snprintf(pszTemp, 24, "Information"); break;
							case MSG_ICONUSER1://								0x00000007  
								_snprintf(pszTemp, 24, "User message"); break;
							case MSG_ICONUSER2://								0x00000008  
								_snprintf(pszTemp, 24, "User message (2)"); break;
							default:
							case MSG_ICONNONE://							0x00000000  // default, nothing
								_snprintf(pszTemp, 24, "Message"); break;
							}

							char* pchPlus = pszTemp;
							while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
							{
								pszOutput[nOutLen]= *pchPlus;
								nOutLen++;
								pchPlus++;
							}
							free(pszTemp);
						}
						pch++;
					}
					else
					if(*pch == 'P')
					{
						char* pszTemp = (char*)malloc(24);
						if(pszTemp)
						{
							switch(pmsg->ulFlags&MSG_PRI_MASK)
							{
							case MSG_PRI_LOW://                 0x00000010  // non-critical warnings
								_snprintf(pszTemp, 24, "Low Priority"); break;
							case MSG_PRI_MEDIUM://              0x00000020  // warning for something that may become critical if not corrected
								_snprintf(pszTemp, 24, "Medium Priority"); break;
							case MSG_PRI_HIGH://                0x00000030  // immediate error, but not show-stopper
								_snprintf(pszTemp, 24, "High Priority"); break;
							case MSG_PRI_CRITICAL://            0x00000040  // critical, causes exit
								_snprintf(pszTemp, 24, "Critical"); break;
							default:
							case MSG_PRI_NORMAL://              0x00000000  // default, just info
								_snprintf(pszTemp, 24, "Normal"); break;
							}

							char* pchPlus = pszTemp;
							while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
							{
								pszOutput[nOutLen]= *pchPlus;
								nOutLen++;
								pchPlus++;
							}
							free(pszTemp);
						}
						pch++;
					}
					else
					if(*pch == 'h')
					{
						if((m_pszLocalHostname)&&(strlen(m_pszLocalHostname)))
						{
							char* pszTemp = (char*)malloc(strlen(m_pszLocalHostname)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, m_pszLocalHostname);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<MAXLEN_TEXT))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
				//		pszOutput[nOutLen]= *(pch-1); // don't add the %.
				//		nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
		}
		return pch;
	}
	return NULL;
}


int CSMTPUtil::KillThread()
{
	m_bKillThread = true;
	while(m_bThreadStarted) { Sleep(5);}

	return SMTP_SUCCESS;
}

int CSMTPUtil::Synchronize(_timeb timeSynch)
{
	if(timeSynch.time==0) m_nAbsoluteTimeOffsetMS=0;
	else
	{
		_timeb timebuffer;
		_ftime( &timebuffer );

		m_nAbsoluteTimeOffsetMS=(timebuffer.time-timeSynch.time)*1000;
		m_nAbsoluteTimeOffsetMS+=(timebuffer.millitm-timeSynch.millitm);
	}
	return SMTP_SUCCESS;
}


int CSMTPUtil::InsertAddress(SMTPAddress_t* pAddress, int index)
{
	if(pAddress)
	{
		SMTPAddress_t** ppObj = new SMTPAddress_t*[m_nNumAddresses+1];
		if(ppObj)
		{
			SMTPAddress_t** ppDelObj = m_ppAddresses;
			int o=0;
			if((m_ppAddresses)&&(m_nNumAddresses>0))
			{
				if((index<0)||(index>=m_nNumAddresses))
				{
					while(o<m_nNumAddresses)
					{
						ppObj[o] = m_ppAddresses[o];
						o++;
					}
					ppObj[m_nNumAddresses] = pAddress;
					index = m_nNumAddresses;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_ppAddresses[o];
						o++;
					}
					ppObj[o] = pAddress;
					while(o<m_nNumAddresses)
					{
						ppObj[o+1] = m_ppAddresses[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nNumAddresses] = pAddress;  // just add the one
				index = m_nNumAddresses;
			}

			m_ppAddresses = ppObj;
			if(ppDelObj)
			{
				try{delete [] ppDelObj;} catch(...){}
			}
			m_nNumAddresses++;
			return index;
		}
	}

	return SMTP_ERROR;
}



int CSMTPUtil::HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	if(!m_bThreadStarted) // can't handle the messages...
	{
		InitializeThread(NULL); // try!
		return SMTP_ERROR;
	}

	unsigned long ulReqCount = m_ulReqCount; // the "ID" of this function call.
	m_ulReqCount++; // increment the request for the next possible call.

	EnterCriticalSection(&m_critBuffer);
/*
	while((m_ulSvcCount<ulReqCount)||(m_bBufferInUse))
	{
		// wait.
		Sleep(1);
	}

	m_bBufferInUse = true; // prevent access from file write thread
*/
	
	// check tabulations.
	// if destination is present, check destination table for name, if its not there, add it
	// if caller is present, check caller table for name, if its not there, add it
	// then format the message and add it to the message buffer.
	// when finished with buffer, m_ulSvcCount++;
	
	//buffer record is as follows:
	//[4 bytes unix time][2 bytes millitime][4 bytes flags][variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10]

	unsigned long ulRecordLength = 13;  // 4+2+4+1+1+1
	unsigned long ulMsgLength = 0;
	unsigned long ulCallerLength = 0;
	unsigned long ulDestLength = 0;

	ulMsgLength = strlen(pszMessage);
	char* pszEncMsg = EncodeTen(pszMessage, &ulMsgLength);
	if(!pszEncMsg)
	{
		LeaveCriticalSection(&m_critBuffer);
		return SMTP_BADBUFFER;
	}
	ulRecordLength+=ulMsgLength; // encoded length

	char* pszEncCaller = NULL;
	char* pszEncDestination = NULL;

	if(pszCaller)
	{
		ulCallerLength = strlen(pszCaller);
		if(ulCallerLength>0)
		{
			pszEncCaller = EncodeTen(pszCaller, &ulCallerLength);
			if(!pszEncCaller)
			{
	//			free(pszEncMsg);
	//			return SMTP_BADBUFFER;  // lets just continue, this is less important than the msg itself
			}
			else  ulRecordLength+=ulCallerLength; // encoded length
		}
	}

	if(pszDestinations)
	{
		ulDestLength = strlen(pszDestinations);
		if(ulDestLength>0)
		{
			pszEncDestination = EncodeTen(pszDestinations, &ulDestLength);
			if(!pszEncDestination)
			{
	//			free(pszEncMsg);
	//			return SMTP_BADBUFFER;  // lets just continue, this is less important than the msg itself
			}
			else	ulRecordLength+=ulDestLength; // encoded length
		}
	}

	//expand the buffer.

	int nReturnValue = SMTP_SUCCESS;
	char* pch = (char*) malloc(m_ulBufferLength+ulRecordLength+1);
	if(pch)
	{	
		if(m_pchSendBuffer)
		{
			memcpy(pch, m_pchSendBuffer, m_ulBufferLength);
			free(m_pchSendBuffer);
		}
		m_pchSendBuffer = pch;

		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time>>24)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time>>16)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time>>8)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.time)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.millitm>>8)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((timestamp.millitm)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags>>24)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags>>16)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags>>8)&(0xff));
		*(pch+(m_ulBufferLength++))= (unsigned char)((ulFlags)&(0xff));

		memcpy((pch+m_ulBufferLength), pszEncMsg, ulMsgLength); m_ulBufferLength+=ulMsgLength;
		*(pch+(m_ulBufferLength++))= (unsigned char)(10); //delim

		if(pszEncCaller) memcpy((pch+m_ulBufferLength), pszEncCaller, ulCallerLength); m_ulBufferLength+=ulCallerLength;
		*(pch+(m_ulBufferLength++))= (unsigned char)(10); //delim

		if(pszEncDestination) memcpy((pch+m_ulBufferLength), pszEncDestination, ulDestLength);  m_ulBufferLength+=ulDestLength;
		*(pch+(m_ulBufferLength++))= (unsigned char)(10); //delim
		memset((pch+m_ulBufferLength), (unsigned char)(0), 1); //zero term the whole thing
/*
///
		unsigned long ulBufL = m_ulBufferLength;
		CBufferUtil bu;
		char* pbuf = bu.ReadableHex(pch, &ulBufL);
		AfxMessageBox(pbuf);
		free(pbuf);
///
*/
	}
	else nReturnValue = SMTP_BADBUFFER;

	if(pszEncMsg) free(pszEncMsg);
	if(pszEncCaller) free(pszEncCaller);
	if(pszEncDestination) free(pszEncDestination);

//	m_bBufferInUse = false; // release buffer access
	m_ulSvcCount++;
	if(m_ulSvcCount == m_ulReqCount) // all requests serviced, can reset counter
		m_ulSvcCount = m_ulReqCount = 0;
	LeaveCriticalSection(&m_critBuffer);

	return nReturnValue;
}

int CSMTPUtil::Modify(char* pchParams, unsigned long* pulBufferLen)
{
	// this function MUST be overridden by objects
	return 0;
}

void SMTPQueueMessageThread(void* pvArgs)
{
//	AfxMessageBox("about to send mail");
	SMTPMessage_t* pmsg = (SMTPMessage_t*)pvArgs;
	if(pmsg)
	{
		CSMTPUtil* psmtp = (CSMTPUtil*)pmsg->pUtil;
		if(psmtp)
		{
	EnterCriticalSection(&psmtp->m_critQueue);
	psmtp->m_nQueueThreads++;
	LeaveCriticalSection(&psmtp->m_critQueue);

	// Full message is actually in message here.

			psmtp->HandleMessage(pmsg->timestamp, pmsg->ulFlags, pmsg->pszMessage, pmsg->pszCaller, pmsg->pszDestinations);
		}

		try
		{
			if(pmsg->pszMessage) free(pmsg->pszMessage);
			if(pmsg->pszCaller) free(pmsg->pszCaller);
			if(pmsg->pszDestinations) free(pmsg->pszDestinations);
			if(pmsg->pszFullMessage) free(pmsg->pszFullMessage); // this should not get hit.
			delete pmsg;
		}
		catch(...)
		{
		}

		if(psmtp)
		{
	EnterCriticalSection(&psmtp->m_critQueue);
	psmtp->m_nQueueThreads--;
	LeaveCriticalSection(&psmtp->m_critQueue);
		}

	}
}

void SMTPHandlerThread(void* pvArgs)
{
	CSMTPUtil* psmtp = (CSMTPUtil*)pvArgs;
	if(psmtp)
	{
		psmtp->m_bThreadStarted = true;
		CSMTP mail;

		if(psmtp->m_pszLogFilename)
		{
			mail.ChangeLogFilename(psmtp->m_pszLogFilename);
		}
		if((psmtp->m_bLogInfo)||(psmtp->m_bLogErrors))
		{
			mail.ChangeLogFileUsage(true);
		}

		while (!psmtp->m_bKillThread)
		{
			// check buffer for new messages, and ability to write them.
			if((psmtp->m_ulBufferLength>0)&&(!psmtp->m_bServerInUse))//&&(!psmtp->m_bBufferInUse))
			{
				EnterCriticalSection(&psmtp->m_critBuffer);
//				psmtp->m_bBufferInUse=true;
				psmtp->m_bServerInUse=true;

				// if they exist, lock the buffer, process record by record into server.
				if(psmtp->m_pchSendBuffer)
				{
					char* pch = psmtp->m_pchSendBuffer;

//					unsigned long ulBufLen = psmtp->m_ulBufferLength;	CBufferUtil bu;  char* px = bu.ReadableHex(psmtp->m_pchSendBuffer, &ulBufLen);
//					AfxMessageBox(px); free(px);

					while((pch)&&(psmtp)&&(pch<psmtp->m_pchSendBuffer+psmtp->m_ulBufferLength)) // in other words, while there are still records left
					{
//	AfxMessageBox("smtp send buffer exists");

//ulBufLen = psmtp->m_ulBufferLength - (pch-psmtp->m_pchSendBuffer);	CBufferUtil bu;  px = bu.ReadableHex(pch, &ulBufLen);
//					AfxMessageBox(px); free(px);

						//first, check the time, then rotate;
// buffer record:
//[4 bytes unix time][2 bytes millitime][4 bytes flags][variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10]
						_timeb timestamp;
						timestamp.time  = (0xff000000&((*pch&0xff)<<24)); pch++;
						timestamp.time |= (0x00ff0000&((*pch&0xff)<<16)); pch++;
						timestamp.time |= (0x0000ff00&((*pch&0xff)<<8));  pch++;
						timestamp.time |= (0x000000ff&(*pch&0xff));				pch++;

						timestamp.millitm  = (0xff00&((*pch&0xff)<<8)); pch++;
						timestamp.millitm |= (0x00ff&(*pch&0xff));			pch++;

//						psmtp->RotateLog(timestamp.time);

						unsigned long ulFlags = 0;
						ulFlags  = (0xff000000&((*pch&0xff)<<24));	pch++;
						ulFlags |= (0x00ff0000&((*pch&0xff)<<16));	pch++;
						ulFlags |= (0x0000ff00&((*pch&0xff)<<8));		pch++;
						ulFlags |= (0x000000ff&(*pch&0xff));				pch++;


						// here we can check the flags to assemble the email list
	EnterCriticalSection(&psmtp->m_critAddresses);

						unsigned long ulTempBufferLen = 0;

						char* pszAddressList = NULL;
						char* pszSwap = NULL;

						if(psmtp->m_ppAddresses)
						{
							int i=0;
							while(i<psmtp->m_nNumAddresses)
							{
								if(psmtp->m_ppAddresses[i])
								{
									psmtp->m_ppAddresses[i]->ulFlags = MSG_ICONNONE; // reset this

//									CString foo; foo.Format("testing %d, %d against %d %d (0x%08x)", psmtp->m_ppAddresses[i]->nSeverity,psmtp->m_ppAddresses[i]->nType,(int)(((ulFlags&MSG_PRI_MASK)>>4)&0x0f),(int)((ulFlags&MSG_ICONMASK)&0x0f), ulFlags);	AfxMessageBox(foo);

									if(
											( (psmtp->m_ppAddresses[i]->pszAddress)&&(strlen(psmtp->m_ppAddresses[i]->pszAddress)) )
										&&(
												((psmtp->m_ppAddresses[i]->nSeverity == 16)||(psmtp->m_ppAddresses[i]->nSeverity==(int)(((ulFlags&MSG_PRI_MASK)>>4)&0x0f)))
											// ifthis is ||, a 16 on one side will just trump the other.  && allows all with 16) on one side but filter on other.
											&&((psmtp->m_ppAddresses[i]->nType == 16)||(psmtp->m_ppAddresses[i]->nType==(int)((ulFlags&MSG_ICONMASK)&0x0f)))
											)
										)
									{
//										CString foo; foo.Format("%s supported for %d %d",psmtp->m_ppAddresses[i]->pszAddress,psmtp->m_ppAddresses[i]->nSeverity, psmtp->m_ppAddresses[i]->nType);
//										AfxMessageBox(foo);
										// check to see if we have already set this address previously.
										int j=i-1;
										while(j>=0)
										{
											if(psmtp->m_ppAddresses[j])
											{
												if(
														(psmtp->m_ppAddresses[j]->pszAddress)
													&&(strcmp(psmtp->m_ppAddresses[i]->pszAddress, psmtp->m_ppAddresses[j]->pszAddress)==0) // case sensitive
													)
												{
													if(psmtp->m_ppAddresses[j]->ulFlags&MSG_SIGNAL_REGDEST_EMAIL) break;  // exit, we already did it.
												}
											}

											j--;
										}

										if(j<0) // we didn't find something already done.
										{
											
											pszSwap = (char*)malloc(ulTempBufferLen+strlen(psmtp->m_ppAddresses[i]->pszAddress)+2);
											if(pszSwap)
											{
												psmtp->m_ppAddresses[i]->ulFlags |= MSG_SIGNAL_REGDEST_EMAIL; // mark it as done.
												if(pszAddressList)
												{
													sprintf(pszSwap, "%s,%s", pszAddressList, psmtp->m_ppAddresses[i]->pszAddress);

													free(pszAddressList);
													pszAddressList = pszSwap;
												}
												else
												{
													//first!
													strcpy(pszSwap, psmtp->m_ppAddresses[i]->pszAddress);
													pszAddressList = pszSwap;
												}
//												AfxMessageBox(pszAddressList);

												ulTempBufferLen = strlen(pszAddressList);
											}
										}
									}
								}
								i++;
							}
						}


	LeaveCriticalSection(&psmtp->m_critAddresses);


						if(pszAddressList) // need someone to send it to!
						{
//AfxMessageBox(pszAddressList);
							// full text description, dont need to check anything.
							// decompile the buffer record
							// write out timestamp, icon, message, calling func, destination.

							//first, write the timestamp and icon
							//timestamp is 2004-Jan-31 23:59:59.999 (24 chars)
							//icon and severity are 4 chars: "[!1]"
							// separators are 3 chars: " - "
							//"2004-Jan-31 23:59:59.999 [!1] "

							ulTempBufferLen = 0;

							char buffer[48]; // need 33;
							char preamblebuffer[256]; // sufficient I think.
							tm* theTime = localtime( &timestamp.time	);
							strftime( buffer, 40, "%Y-%b-%d %H:%M:%S.", theTime );

							char ch = ' ';
							if((ulFlags&MSG_PRI_MASK)!=MSG_PRI_NORMAL)
								ch = (char)((ulFlags>>4)&0x0000000f);//+48;

							switch(ulFlags&MSG_ICONMASK)
							{
							case MSG_ICONQUESTION://						0x00000001  // ? icon
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY QUESTION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY QUESTION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY QUESTION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL QUESTION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "QUESTION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}

								} break;
							case MSG_ICONEXCLAMATION://					0x00000002  // ! icon
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY ALERT at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY ALERT at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY ALERT at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL ALERT at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "ALERT at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}

								} break;
							case MSG_ICONERROR://								0x00000003  // X icon
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY ERROR at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY ERROR at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY ERROR at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL ERROR at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "ERROR at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							case MSG_ICONSTOP://								0x00000004  // stop sign icon
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY STOP at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY STOP at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY STOP at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL STOP at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "STOP at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							case MSG_ICONHAND://								0x00000005  // hand icon
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY SIGNAL at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY SIGNAL at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY SIGNAL at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL SIGNAL at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "SIGNAL at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							case MSG_ICONINFO://								0x00000006  // (i) icon 
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY INFORMATION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY INFORMATION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY INFORMATION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL INFORMATION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "INFORMATION at %s%03d\r\non %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							case MSG_ICONUSER1://								0x00000007  
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY USER MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY USER MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY USER MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL USER MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "USER MESSAGE (2) at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							case MSG_ICONUSER2://								0x00000008  
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY USER MESSAGE (2) at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY USER MESSAGE (2) at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY USER MESSAGE (2) at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL USER MESSAGE (2) at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "USER MESSAGE (2) at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							case MSG_ICONNONE://								0x00000000  // default, nothing
							default:
								{
									switch(ch)
									{
									case (MSG_PRI_LOW>>4):      sprintf(preamblebuffer, "LOW PRIORITY MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_MEDIUM>>4):   sprintf(preamblebuffer, "MEDIUM PRIORITY MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_HIGH>>4):     sprintf(preamblebuffer, "HIGH PRIORITY MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									case (MSG_PRI_CRITICAL>>4): sprintf(preamblebuffer, "CRITICAL MESSAGE at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									default:
									case (MSG_PRI_NORMAL>>4):   sprintf(preamblebuffer, "Message at %s%03d\r\nfrom %s\r\n", buffer, timestamp.millitm, ((psmtp->m_pszLocalHostname)&&(strlen(psmtp->m_pszLocalHostname)))?psmtp->m_pszLocalHostname:"[unknown host]"); break;
									}
								} break;
							}

							ulTempBufferLen = strlen(preamblebuffer);

							unsigned char ucDelim=(char)10;

							char* pchMsg = pch; //strtok( pch, &ucDelim );
							while((((*pchMsg)&0xff)!=ucDelim)&&(((*pchMsg)&0xff)!=0))	pchMsg++; // go to next delim.

							unsigned long ulCallerBufferLen=0;
							char* pchCaller = NULL;

							unsigned long ulDestBufferLen=0;
							char* pchDest = NULL;

							unsigned long ulBufferLen = (pchMsg-pch);
							char* pchDecMsg = psmtp->DecodeTen(pch, &ulBufferLen, TEN_DECODE_CRLF);  // text mode, make it crlf
/*
							if(pchDecMsg)
							{
								// free(pch);  // do not want to free pch, its in the middle somewhere!
								// need to calculate the length of the buffer we need
								// if there are CRLFs, we want to put each line on its own line, but also
								// put leading spaces to justify the text.

								pch = strstr(pchDecMsg, "\r\n");
								while(pch!=NULL)
								{
									ulBufferLen+=30; // the length of the indent.
									pch = strstr(pch+2, "\r\n");
								}

							}
*/
							ulTempBufferLen+=ulBufferLen;

							// put a delimiter
//							ulTempBufferLen+=3;

							pch = ++pchMsg;

							if(*pch!=ucDelim)
							{
								while(((*pchMsg&0xff)!=ucDelim)&&((*pchMsg&0xff)!=0))	pchMsg++; // go to next delim.
								ulCallerBufferLen = (pchMsg-pch);
								pchCaller = pch; //psmtp->DecodeTen(pch, &ulCallerBufferLen);
								ulTempBufferLen += ulCallerBufferLen+ strlen("generated by ") + 2; // 2 for CRLF

								pch = ++pchMsg;
							}
							else
							{
								pch++;
							}

							// put four CRLFs, first for after caller, three after message
							ulTempBufferLen+=8;

							if(*pch!=ucDelim)
							{
								while(((*pchMsg&0xff)!=ucDelim)&&((*pchMsg&0xff)!=0))	pchMsg++; // go to next delim.
								ulDestBufferLen = (pchMsg-pch);
								pchDest = pch; //psmtp->DecodeTen(pch, &ulDestBufferLen);
								ulTempBufferLen += ulDestBufferLen+ strlen("sent to ");

								pch = ++pchMsg;
							}
							else
							{
								pch++;
							}
//						CString foo; foo.Format("pch after delim:%d, %d (%d+%d)", pch, psmtp->m_pchSendBuffer+psmtp->m_ulBufferLength,psmtp->m_pchSendBuffer,psmtp->m_ulBufferLength); AfxMessageBox(foo);


							ulTempBufferLen+=2;


							// need to send the following format:


							//CRITICAL ERROR at	2004-Jan-31 23:59:59.999<CRLF>
							//on [hostname]<CRLF>
							//generated by [caller]<CRLF>
							//<CRLF>
							//[message]<CRLF>
							//<CRLF>
							//<CRLF>
							//sent to [destinations]

							//let's allocate separate fields for the caller and dests.

							char* pszCaller = NULL;
							if((pchCaller)&&(ulCallerBufferLen))
							{
								pszCaller = (char*)malloc(ulCallerBufferLen+1);
								if(pszCaller)
								{
									memcpy(pszCaller, pchCaller, ulCallerBufferLen);
									*(pszCaller+ulCallerBufferLen) = 0; //zero term
//									AfxMessageBox(pszCaller);
								}
							}

							char* pszDest = NULL;
							if((pchDest)&&(ulDestBufferLen))
							{
								pszDest = (char*)malloc(ulDestBufferLen+1);
								if(pszDest)
								{
									memcpy(pszDest, pchDest, ulDestBufferLen);
									*(pszDest+ulDestBufferLen) = 0; //zero term
//									AfxMessageBox(pszDest);
								}
							}


							char* pchTempBuffer = (char*)malloc(ulTempBufferLen+1);
							if(pchTempBuffer)
							{
								unsigned long ulBufPtr = 0;

//								char* pchBegin	= pchDecMsg;
								if(pszCaller)
								{
									if(pszDest)
									{
										_snprintf(pchTempBuffer, ulTempBufferLen, "%sgenerated by %s\r\n\r\n%s\r\n\r\n\r\nsent to %s", 
											preamblebuffer, 
											((pszCaller)&&(strlen(pszCaller)))?pszCaller:"[unknown]",
											pchDecMsg,
											((pszDest)&&(strlen(pszDest)))?pszDest:"[unknown]"
											);
									}
									else
									{
										_snprintf(pchTempBuffer, ulTempBufferLen, "%sgenerated by %s\r\n\r\n%s\r\n\r\n", 
											preamblebuffer, 
											((pszCaller)&&(strlen(pszCaller)))?pszCaller:"[unknown]",
											pchDecMsg
											);
									}
								}
								else
								{
									if(pszDest)
									{
										_snprintf(pchTempBuffer, ulTempBufferLen, "%s\r\n%s\r\n\r\n\r\nsent to %s", 
											preamblebuffer, 
											pchDecMsg,
											((pszDest)&&(strlen(pszDest)))?pszDest:"[unknown]"
											);
									}
									else
									{
										_snprintf(pchTempBuffer, ulTempBufferLen, "%s\r\n%s\r\n\r\n", 
											preamblebuffer, 
											pchDecMsg
											);
									}
								}
	/*
								memcpy(pchTempBuffer+ulBufPtr, pchDecMsg, ulBufferLen);
								sprintf(buffer, " - ");
								memcpy(pchTempBuffer+30+ulBufferLen, buffer, 3);
								if(pchCaller) memcpy(pchTempBuffer+33+ulBufferLen, pchCaller, ulCallerBufferLen);
								memcpy(pchTempBuffer+33+ulBufferLen+ulCallerBufferLen, buffer, 3);
								if(pchDest) memcpy(pchTempBuffer+36+ulBufferLen+ulCallerBufferLen, pchDest, ulDestBufferLen);
								*(pchTempBuffer+36+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 13;
								*(pchTempBuffer+37+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 10;
								*(pchTempBuffer+38+ulBufferLen+ulCallerBufferLen+ulDestBufferLen) = 0;
	//									sprintf(buffer, "%c%c",13,10);
	*/
								
								
//AfxMessageBox(pchTempBuffer);

								bool bRepeat=false;
								if(psmtp->smtpmsgLastMessage.pszFullMessage)
								{
//AfxMessageBox("-11");
									// need to compare everything but the times.
									char* pchCurrentTime = strstr(pchTempBuffer, " at ");
									if(pchCurrentTime)
									{
//										AfxMessageBox(psmtp->smtpmsgLastMessage.pszFullMessage+(pchCurrentTime-pchTempBuffer+30));
//										AfxMessageBox(pchCurrentTime+30);
										if(strcmp(psmtp->smtpmsgLastMessage.pszFullMessage+(pchCurrentTime-pchTempBuffer+30), pchCurrentTime+30)==0) //match last part
										{
											// now match first part:
											if(memcmp(psmtp->smtpmsgLastMessage.pszFullMessage, pchTempBuffer, (pchCurrentTime-pchTempBuffer))==0)
											{
												
//												AfxMessageBox("match");
												//match!
												psmtp->m_ulLastMessageCount++;
												bRepeat=true;
											}
										}
									}
								}

								if(bRepeat)
								{
									if(psmtp->m_bAllowRepeat)
									{
										//write it anyway
										// assemble subject

										char* pszSubj = NULL;
										SMTPMessage_t* psmtpmsg = new SMTPMessage_t;
										if(psmtpmsg)
										{
											psmtpmsg->timestamp = timestamp;
											psmtpmsg->ulFlags = ulFlags;
											psmtpmsg->pszMessage = pchDecMsg;  // do not free
											psmtpmsg->pszCaller=pszCaller; // do not free
											psmtpmsg->pszDestinations=pszDest; // do not free

											pszSubj = psmtp->AssembleSubject(psmtpmsg);
											try
											{
												delete psmtpmsg;
											}
											catch(...)
											{
											}

										}

										char* pszLFSafeMsg = mail.EnsureNoBareLFs(pchTempBuffer);

										if(pszLFSafeMsg)
										{
											mail.SendMail(psmtp->m_pszHostname, 
																		pszAddressList,
																		psmtp->m_pszFromSpec,
																		NULL,
																		pszSubj,
																		pszLFSafeMsg,
																		NULL,
																		NULL,
																		psmtp->m_nPort
																		);
											try{ free(pszLFSafeMsg);} catch(...){}
										}
										else
										{
											mail.SendMail(psmtp->m_pszHostname, 
																		pszAddressList,
																		psmtp->m_pszFromSpec,
																		NULL,
																		pszSubj,
																		pchTempBuffer,
																		NULL,
																		NULL,
																		psmtp->m_nPort
																		);
										}
										if(pszSubj)
										{
											try{ free(pszSubj);} catch(...){}
										}

/*
										// need this proto to override port
										mail.SendMail(char* pszMailServer,
																	char* pszTo, 
																	char* pszFrom, 
																	char* pszReplyTo, 
																	char* pszSubject, 
																	char* pszMsg, 
																	char* pszAddlHeaders, 
																	char* pszAttachFiles, 
																	short nPort, 
																	unsigned long* pulBytesWritten, 
																	unsigned long* pulBytesTotal
																	);
																	
							
									// this one too simple.
										mail.SendMail(char* pszMailServer,
																	char* pszTo, 
																	char* pszFrom, 
																	char* pszReplyTo, 
																	char* pszSubject, 
																	char* pszMsg
																	);
*/
//										fwrite( pchTempBuffer, 1, ulTempBufferLen, psmtp->m_pfile );
//										fflush(psmtp->m_pfile);
									}
								}
								else
								{
//AfxMessageBox("here");
									if((psmtp->m_ulLastMessageCount>0)&&(psmtp->smtpmsgLastMessage.pszMessage))
									{
										SMTPMessage_t* psmtpmsg = new SMTPMessage_t;
										if(psmtpmsg)
										{
											psmtpmsg->pUtil = (void*)psmtp;
											psmtpmsg->timestamp = timestamp;
											psmtpmsg->ulFlags = psmtp->smtpmsgLastMessage.ulFlags;
											psmtpmsg->pszFullMessage = NULL;
											psmtpmsg->pszMessage = NULL;
											psmtpmsg->pszCaller=NULL;
											if(psmtp->smtpmsgLastMessage.pszCaller)
											{
												psmtpmsg->pszCaller=(char*)malloc(strlen(psmtp->smtpmsgLastMessage.pszCaller)+1);
												if(psmtpmsg->pszCaller)
												{
													strcpy(psmtpmsg->pszCaller, psmtp->smtpmsgLastMessage.pszCaller);
												}												
											}
											psmtpmsg->pszDestinations=NULL;
											if(psmtp->smtpmsgLastMessage.pszDestinations)
											{
												psmtpmsg->pszDestinations=(char*)malloc(strlen(psmtp->smtpmsgLastMessage.pszDestinations)+1);
												if(psmtpmsg->pszDestinations)
												{
													strcpy(psmtpmsg->pszDestinations, psmtp->smtpmsgLastMessage.pszDestinations);
												}												
											}

											bool bError = true;
											char chRpt[256];

											theTime = localtime( &psmtp->smtpmsgLastMessage.timestamp.time	);
											strftime( buffer, 40, "%Y-%b-%d %H:%M:%S.", theTime );

											_snprintf(chRpt, 256, "\r\n\r\nThis message was repeated %d times since %s%03d", psmtp->m_ulLastMessageCount, buffer, psmtp->smtpmsgLastMessage.timestamp.millitm);

											if(psmtp->smtpmsgLastMessage.pszMessage)
											{
												psmtpmsg->pszMessage = (char*)malloc(strlen(psmtp->smtpmsgLastMessage.pszMessage)+strlen(chRpt)+2);
												if(psmtpmsg->pszMessage)
												{
													sprintf(psmtpmsg->pszMessage, "%s%s", psmtp->smtpmsgLastMessage.pszMessage, chRpt); 
													bError = false;
												}
											}
											else
											{
												psmtpmsg->pszMessage = (char*)malloc(strlen(pchDecMsg)+strlen(chRpt)+2);
												if(psmtpmsg->pszMessage)
												{
													sprintf(psmtpmsg->pszMessage, "%s%s", pchDecMsg, chRpt); 
													bError = false;
												}
											}

											if(!bError)
											{
												if(_beginthread( SMTPQueueMessageThread, 0, (void*)(psmtpmsg) )==-1)
												{
													bError = true;
												}
											}

											if(bError)
											{
												// have to delete.

												try
												{
													if(psmtpmsg->pszFullMessage) free(psmtpmsg->pszFullMessage);
													if(psmtpmsg->pszMessage) free(psmtpmsg->pszMessage);
													if(psmtpmsg->pszCaller) free(psmtpmsg->pszCaller);
													if(psmtpmsg->pszDestinations) free(psmtpmsg->pszDestinations);
													delete psmtpmsg;
												}
												catch(...)
												{
												}

											}

										}
										// otherwise error, nothing to do...could log it...

									}

	//					unsigned long ulBufLen = ulBufferLen+6+ulCallerLen+ulDestLen;	CBufferUtil bu;  char* px = bu.ReadableHex(pchDecMsg, &ulBufLen);
	//					AfxMessageBox(px); free(px);

									char* pszSubj = NULL;
									SMTPMessage_t* psmtpmsg = new SMTPMessage_t;
									if(psmtpmsg)
									{
										psmtpmsg->timestamp = timestamp;
										psmtpmsg->ulFlags = ulFlags;
										psmtpmsg->pszMessage = pchDecMsg;  // do not free
										psmtpmsg->pszCaller=pszCaller; // do not free
										psmtpmsg->pszDestinations=pszDest; // do not free

										pszSubj = psmtp->AssembleSubject(psmtpmsg);
										try
										{
											delete psmtpmsg;
										}
										catch(...)
										{
										}

									}

									char* pszLFSafeMsg = mail.EnsureNoBareLFs(pchTempBuffer);

									if(pszLFSafeMsg != NULL)
									{

										mail.SendMail(psmtp->m_pszHostname, 
																	pszAddressList,
																	psmtp->m_pszFromSpec,
																	NULL,
																	pszSubj,
																	pszLFSafeMsg,
																	NULL,
																	NULL,
																	psmtp->m_nPort
																	);
										try{ free(pszLFSafeMsg);} catch(...){}
										pszLFSafeMsg = NULL;
									}
									else
									{
										mail.SendMail(psmtp->m_pszHostname, 
																	pszAddressList,
																	psmtp->m_pszFromSpec,
																	NULL,
																	pszSubj,
																	pchTempBuffer,
																	NULL,
																	NULL,
																	psmtp->m_nPort
																	);
									}

									if(pszSubj)
									{
										try{ free(pszSubj);} catch(...){}
										pszSubj = NULL;
									}
/*
									// need this proto to override port
									mail.SendMail(char* pszMailServer,
																char* pszTo, 
																char* pszFrom, 
																char* pszReplyTo, 
																char* pszSubject, 
																char* pszMsg, 
																char* pszAddlHeaders, 
																char* pszAttachFiles, 
																short nPort, 
																unsigned long* pulBytesWritten, 
																unsigned long* pulBytesTotal
																);
																
						
								// this one too simple.
									mail.SendMail(char* pszMailServer,
																char* pszTo, 
																char* pszFrom, 
																char* pszReplyTo, 
																char* pszSubject, 
																char* pszMsg
																);
*/
//										fwrite( pchTempBuffer, 1, ulTempBufferLen, psmtp->m_pfile );
//										fflush(psmtp->m_pfile);

									psmtp->m_ulLastMessageCount=0;
								}

								if(psmtp->m_ulLastMessageCount==0) // don't reset the timestamp of the original, so we can report it later.
								{
									psmtp->smtpmsgLastMessage.timestamp = timestamp;
								}

								psmtp->smtpmsgLastMessage.ulFlags = ulFlags;

								if(psmtp->smtpmsgLastMessage.pszMessage) free(psmtp->smtpmsgLastMessage.pszMessage);
								psmtp->smtpmsgLastMessage.pszMessage=pchDecMsg; 

								if(psmtp->smtpmsgLastMessage.pszFullMessage) free(psmtp->smtpmsgLastMessage.pszFullMessage);
								psmtp->smtpmsgLastMessage.pszFullMessage=pchTempBuffer; // updates repeat with new time.

								if(psmtp->smtpmsgLastMessage.pszCaller) free(psmtp->smtpmsgLastMessage.pszCaller);
								psmtp->smtpmsgLastMessage.pszCaller=NULL;
								if((pchCaller)&&(ulCallerBufferLen))
								{
									psmtp->smtpmsgLastMessage.pszCaller = (char*)malloc(ulCallerBufferLen+1);
									if(psmtp->smtpmsgLastMessage.pszCaller)
									{
										strcpy(psmtp->smtpmsgLastMessage.pszCaller, pszCaller);
									}
								}

								if(psmtp->smtpmsgLastMessage.pszDestinations) free(psmtp->smtpmsgLastMessage.pszDestinations);
								psmtp->smtpmsgLastMessage.pszDestinations=NULL;
								if((pchDest)&&(ulDestBufferLen))
								{
									psmtp->smtpmsgLastMessage.pszDestinations = (char*)malloc(ulDestBufferLen+1);
									if(psmtp->smtpmsgLastMessage.pszDestinations)
									{
										strcpy(psmtp->smtpmsgLastMessage.pszDestinations, pszDest);
									}
								}

							}
	/*
							else
							{
								// go to file directly.
								fwrite( buffer, 1, 30, psmtp->m_pfile );
								fwrite( pchDecMsg, 1, ulBufferLen, psmtp->m_pfile );
								sprintf(buffer, " - ");
								fwrite(buffer, 1, 3, psmtp->m_pfile);
								if(pchCaller) fwrite( pchCaller, 1, ulCallerBufferLen, psmtp->m_pfile ); 
								fwrite(buffer, 1, 3, psmtp->m_pfile);
								if(pchDest) fwrite( pchDest, 1, ulDestBufferLen, psmtp->m_pfile ); 
								sprintf(buffer, "%c%c",13,10);
								fwrite(buffer, 1, 2, psmtp->m_pfile);
								fflush(psmtp->m_pfile);
							}
	*/

	// must not free these, they are pointers into an existing buffer
	//						if(pchCaller) { free(pchCaller);  ulCallerBufferLen=0;	} 
	//						if(pchDest)   { free(pchDest);		ulDestBufferLen=0;		}
	// must free the allocated buffer for decodede messages
//							if(pchDecMsg) { try{ free(pchDecMsg); } catch(...){} ulBufferLen=0;				}
							if(pszCaller) { try{free(pszCaller); } catch(...){} 			}
							pszCaller = NULL;
							if(pszDest)   { try{free(pszDest); } catch(...){} 			}
							pszDest = NULL;

							try{free(pszAddressList); } catch(...){} 
							pszAddressList=NULL;
						}
						else
						{
							// have to advance the pointer.
							unsigned char ucDelim=(char)10;

							char* pchMsg = pch; //strtok( pch, &ucDelim );
							while((((*pchMsg)&0xff)!=ucDelim)&&(((*pchMsg)&0xff)!=0))	pchMsg++; // go to next delim.

							pch = ++pchMsg;

							if(*pch!=ucDelim)
							{
								while(((*pchMsg&0xff)!=ucDelim)&&((*pchMsg&0xff)!=0))	pchMsg++; // go to next delim.
								pch = ++pchMsg;
							}
							else
							{
								pch++;
							}

							if(*pch!=ucDelim)
							{
								while(((*pchMsg&0xff)!=ucDelim)&&((*pchMsg&0xff)!=0))	pchMsg++; // go to next delim.
								pch = ++pchMsg;
							}
							else
							{
								pch++;
							}
//						CString foo; foo.Format("pch after delim:%d, %d (%d+%d)", pch, psmtp->m_pchSendBuffer+psmtp->m_ulBufferLength,psmtp->m_pchSendBuffer,psmtp->m_ulBufferLength); AfxMessageBox(foo);

						}
						
//						CString foo; foo.Format("%d, %d (%d+%d)", pch, psmtp->m_pchSendBuffer+psmtp->m_ulBufferLength,psmtp->m_pchSendBuffer,psmtp->m_ulBufferLength); AfxMessageBox(foo);
					} //end while
//					while((pch)&&(psmtp)&&(pch<psmtp->m_pchSendBuffer+psmtp->m_ulBufferLength)) // in other words, while there are still records left

					if(psmtp->m_pchSendBuffer) free(psmtp->m_pchSendBuffer);
					psmtp->m_pchSendBuffer=NULL;
					psmtp->m_ulBufferLength=0;
				}
				// release file and buffer
				psmtp->m_bServerInUse=false;
//				psmtp->m_bBufferInUse=false;
				LeaveCriticalSection(&psmtp->m_critBuffer);
			} // else theres nothing to do
			else
			{
				// give the processor a break for a few flops 
				Sleep(1);
			}

		}  // end while dont kill

		 
		EnterCriticalSection(&psmtp->m_critQueue);
		int nThreads = psmtp->m_nQueueThreads;
		LeaveCriticalSection(&psmtp->m_critQueue);
		while(nThreads)
		{
			EnterCriticalSection(&psmtp->m_critQueue);
			nThreads = psmtp->m_nQueueThreads;
			LeaveCriticalSection(&psmtp->m_critQueue);
		}

		psmtp->m_bThreadStarted = false;
	} // if log
}




