// LogUtil.h: interface for the CSMTPUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAILUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
#define AFX_MAILUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <iostream.h>
#include "../MSG/MessagingObject.h"

#define SMTP_SUCCESS			0
#define SMTP_BADBUFFER		1
//#define SMTP_ROTATED			2
#define SMTP_ERROR				-1


typedef struct SMTPAddress_t
{
	char* pszAddress;
	int nType;
	int nSeverity;
	unsigned long ulFlags;
} SMTPAddress_t;


typedef struct SMTPMessage_t
{
	_timeb timestamp;
	unsigned long ulFlags;
	char* pszFullMessage;
	char* pszMessage;
	char* pszCaller;
	char* pszDestinations;
	void* pUtil;
} SMTPMessage_t;


class CSMTPUtil : public CMessagingObject
{
public:
	bool m_bThreadStarted;
	bool m_bServerInUse;
//	bool m_bBufferInUse;
	bool m_bAllowRepeat;
	bool m_bKillThread;
//	bool m_bVerboseFile;
	char* m_pchSendBuffer;
	char* m_pchNewFileBuffer;

	SMTPMessage_t smtpmsgLastMessage;
//	char* m_pszLastMessage;
	long m_nAbsoluteTimeOffsetMS;
	unsigned long m_ulBufferLength;
	unsigned long m_ulNewFileBufferLength;
	unsigned long m_ulLastUnixTime;
	unsigned short m_usLastMillitime;
	unsigned long m_ulLastMessageCount;

//	FILE* m_pfile;
//	char* m_pszFileBaseName;
//	char* m_pszDestinationTable;
//	char* m_pszFunctionTable;
//	char* m_pszFilenameFormatSpec;
//	char* m_pszLastRotateKey; //forces new file.

// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs

//	char* m_pszParams;

	char* m_pszLocalHostname;

	char* m_pszHostname;  // hostname of the smtp server
	int m_nPort;
	char* m_pszLogFilename;
	char* m_pszSubjectSpec;
	char* m_pszFromSpec;
	bool m_bLogErrors;
	bool m_bLogInfo;

	SMTPAddress_t** m_ppAddresses;
	int m_nNumAddresses;

	unsigned long m_ulReqCount;
	unsigned long m_ulSvcCount;

	CRITICAL_SECTION m_critBuffer;
	CRITICAL_SECTION m_critAddresses;
	CRITICAL_SECTION m_critQueue;
	int m_nQueueThreads;

protected:
/*
	char m_chOverwritePeriod;
	char m_chRotatePeriod;
	unsigned char m_nRotateOffsetMonth;
	unsigned char m_nRotateOffsetDay;
	unsigned char m_nRotateOffsetHours;
	unsigned char m_nRotateOffsetMinutes;
*/

public:
	CSMTPUtil();
	virtual ~CSMTPUtil();

	int InitializeThread(char* pszSMTPParams); 
	int KillThread(); 
	int Synchronize(_timeb timeSynch); 
	int InsertAddress(SMTPAddress_t* pAddress, int index=0);
	char* AssembleSubject(SMTPMessage_t* pmsg);

// override
	int HandleMessage(_timeb timestamp, unsigned long ulFlags, char* pszMessage, char* pszCaller=NULL, char* pszDestinations=NULL);
	int Modify(char* pchParams, unsigned long* pulBufferLen);
	
};

#endif // !defined(AFX_MAILUTIL_H__5CB98E4A_F246_471D_A4C0_CE5A28E37BB9__INCLUDED_)
