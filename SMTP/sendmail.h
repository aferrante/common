#ifndef SENDMAIL_INCLUDED
#define SENDMAIL_INCLUDED

#include "..\LAN\Tcpip.h"
#include "..\TXT\BufferUtil.h"
#include <stdio.h>

// end of line
#define EOLN_CR   0
#define EOLN_LF   1
#define EOLN_CRLF 2
#define EOLN_NONE 3

#define LOG_ERROR 0
#define LOG_SENDERROR 1
#define LOG_RECVERROR 2
#define LOG_SEND  3
#define LOG_RECV  4
#define LOG_CLOSE 5
#define LOG_ENCODE 6

#define REPLY_SVCREADY   1
#define REPLY_CMDOK      0
#define REPLY_FATAL     -1
#define ERROR_NORCPT    -500
#define ERROR_BUFFER    -9

class CSendMail  // : public CObject
{
// Construction
public:
	CSendMail();   // standard constructor
	~CSendMail();   // standard destructor

private:
  CTcpip m_net;
	CBufferUtil  m_bu;
	char m_szLogFile[MAX_PATH];
	bool m_bFileBusy;
	FILE* m_fp;
	bool  m_bLogActivity;
	char* m_pszDefaultDomain;

public:
	bool ChangeLogFilename(char szLogFile[]);
	bool ChangeLogFileUsage(bool bLogActivity);

  int SendMail(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszMsg, char* pszAddlHeaders="", 
								char* pszAttachFiles[]=NULL, int nNumAttach=0, 
								short nPort=25, unsigned long* psizeBytesWritten[]=NULL);

  int SendMailFromTextFile(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszFilename, char* pszAddlHeaders="", 
								char* pszAttachFiles[]=NULL, int nNumAttach=0, 
								short nPort=25, unsigned long* psizeBytesWritten[]=NULL);

	// common overrides
  int SendMail(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszMsg);

  int SendMailFromTextFile(char* pszMailServer,
								char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
                char* pszFilename);

private:
  int SendMailInternals(char* pszMailServer, 
												char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject,
                        char* pszData, bool bUseFile=FALSE,  
                        char* pszAddlHeaders="", 
												char* pszAttachFiles[]=NULL, int nNumAttach=0, 
												short nPort=25, unsigned long* psizeBytesWritten[]=NULL
												);

  bool SendText(char* pszText, SOCKET s, unsigned long* psizeBytesWritten[]=NULL);
	bool SendAttachment(char* pszFileName, char* pszEncodedData, char* pszBoundary, SOCKET s, unsigned long* psizeBytesWritten[]=NULL);
	bool EncodeAttachment(char* pszFileName, char* pszEncodedData, unsigned long* pulTotalBytes);
  void Log(char* pszText, int nType=LOG_ERROR);
  int InterpretCode(char* pszText);
};

#endif
