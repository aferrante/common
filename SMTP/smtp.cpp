// smtp.cpp

#include <stdafx.h>//remove
#include <time.h>
#include <sys/timeb.h>
#include <io.h>
#include "smtp.h"


CSMTP::CSMTP()
{
	m_fp = NULL;
	m_bLogActivity = false;
	m_bFileBusy = false;
	strcpy(m_szLogFile, "vdismtpx.log");
	m_pszDefaultDomain = NULL;
}

CSMTP::~CSMTP()
{
	if(m_fp) fclose(m_fp);
}


// be sure to call EnsureNoBareLFs() on any fields that might contain newline LF characters wihtout a preceding CR
// char* pszAttachFiles shall be a zero terminated string, with char 10 delimited filenames.

int CSMTP::SendMail(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszMsg, char* pszAddlHeaders, 
							char* pszAttachFiles, 
							short nPort, unsigned long* pulBytesWritten, unsigned long* pulBytesTotal)
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszMsg, false, pszAddlHeaders, pszAttachFiles, nPort,
												pulBytesWritten, pulBytesTotal);
}

int CSMTP::SendMailFromTextFile(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszFilename, char* pszAddlHeaders, 
							char* pszAttachFiles, 
							short nPort, unsigned long* pulBytesWritten, unsigned long* pulBytesTotal)
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszFilename, true, pszAddlHeaders, pszAttachFiles, nPort,
												pulBytesWritten, pulBytesTotal);
}

// common overrides
int CSMTP::SendMail(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszMsg)
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszMsg, false, "", NULL, 25, NULL, NULL);
}

int CSMTP::SendMailFromTextFile(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszFilename)
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszFilename, true, "", NULL, 25, NULL, NULL);
}

  
int CSMTP::SendMailInternals(char* pszMailServer, 
												char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject,
                        char* pszData, bool bUseFile,  
                        char* pszAddlHeaders, 
												char* pszAttachFiles, 
												short nPort, unsigned long* pulBytesWritten, unsigned long* pulBytesTotal)
{

// RFC 821 section 4.5.3.  SIZES
//has the following limits:
	
// user     The maximum total length of a user szName is 64 characters.
// domain   The maximum total length of a domain szName or number is 64 characters.
// path     The maximum total length of a reverse-path or forward-path is 256 characters (including the punctuation and element separators).
// command line  The maximum total length of a command line including the command word and the <CRLF> is 512 characters.
// reply line    The maximum total length of a reply line including the reply code and the <CRLF> is 512 characters.
// text line     The maximum total length of a text line including the <CRLF> is 1000 characters (but not counting the leading dot duplicated for transparency).
// recipients buffer  The maximum total number of recipients that must be buffered is 100 recipients.
  
// These limits are #defined above
//#define MAXLEN_USER    64
//#define MAXLEN_DOMAIN  64
//#define MAXLEN_CMD     512
//#define MAXLEN_REPLY   512
//#define MAXLEN_TEXT    1024
//#define MAXBUF_RCPT    100

// the unsigned long* pulBytesWritten and pulByteTotal are provided to feed back bytes written and bytes total 
// they are continuously updated throughout the function, so pointers from another thread 
// can be passed in and checked for progress.

//	AfxMessageBox("sending");


	if((pszMailServer==NULL)||(strlen(pszMailServer)<=0)) return SMTP_ERROR;
	if((pszTo==NULL)||(strlen(pszTo)<=0)) return SMTP_ERROR;
	if((pszFrom==NULL)||(strlen(pszFrom)<=0)) return SMTP_ERROR;
	if((pszData==NULL)||(strlen(pszData)<=0)) return SMTP_ERROR;

	char szText[MAXLEN_TEXT], szTemp[MAXLEN_TEXT];
	unsigned long nLen;

	if(pulBytesTotal)
	{
		if(!bUseFile) // then the data is a text message (zero terminated)
			*pulBytesTotal=(strlen(pszData)+24); //init (+2 for CRLF)
		else // its a file.  length to be determined from the file size and encoding
			*pulBytesTotal=22; //init

//		*pulBytesTotal+=(strlen(pszFrom)+13); // MAIL FROM: can do this up front - not, want to pre-process the from to make sure of format.
		// the message headers
		sprintf(szText, "To:  %s", pszTo);  	
		*pulBytesTotal+=(strlen(szText)+2); //CRLF 

		sprintf(szText, "From:  %s", pszFrom); 
		*pulBytesTotal+=(strlen(szText)+2); //CRLF 

		if((pszReplyTo)&&(strlen(pszReplyTo)>0)) 
		{
			sprintf(szText, "Subject: %s", pszSubject); 
			*pulBytesTotal+=(strlen(szText)+2); //CRLF 
		}

		if((pszReplyTo)&&(strlen(pszReplyTo)>0)) 
		{
			sprintf(szText, "Reply-To: %s", pszReplyTo);  
			*pulBytesTotal+=(strlen(szText)+2); //CRLF 
		}
		if((pszAddlHeaders)&&(strlen(pszAddlHeaders)>0)) 
		{
			nLen = strlen(pszAddlHeaders);
			while((nLen>0)&&(isspace(pszAddlHeaders[nLen-1]))) {pszAddlHeaders[nLen-1]=0; nLen--;}  //trims right

			*pulBytesTotal+=(strlen(pszAddlHeaders)+2); //CRLF 
		}
	}

	if(pulBytesWritten) *pulBytesWritten=0;  //init


	unsigned long ulBytes = 0;
	SOCKET s1;
  char* pszDomain=NULL;
  char* pszSimpleFrom=NULL;
	char* pszFromRemaining=NULL;
  int i, nReturnCode=NET_SUCCESS, nInternalReturnCode=NET_SUCCESS;
  int index=0;
	int nNumSuccessfulRCPT = 0;
	char* pszEncodedAttach[MAXNUM_ATTACHMENTS];
	char* pszEncodedAttachFileName[MAXNUM_ATTACHMENTS];
	char* pch=NULL;
	unsigned char* pchRec=NULL;

	for(i=0; i<MAXNUM_ATTACHMENTS; i++)
	{
		pszEncodedAttach[i] = NULL;
		pszEncodedAttachFileName[i] = NULL;
	}

	m_bFileBusy = true; // prevents changing the logfile while writing out a messagelog
	if((m_fp == NULL)&&(m_bLogActivity))  m_fp = fopen(m_szLogFile, "at");

//	szTemp.Format("file pointer %d, bLog %d, filename %s",m_fp,m_bLogActivity,m_szLogFile);
//	AfxMessageBox(szTemp);
//	szTemp.Format("%d files\n",nNumAttach);
//	for(i=0; i<nNumAttach; i++)
//		szTemp.Format("%s%s\n", szTemp, szAttachFiles[i]);
//	AfxMessageBox(szTemp);

  // extract the first valid local domain
	pszDomain = (char*)malloc(strlen(pszFrom)+1);
	if(pszDomain==NULL) return SMTP_ERROR_BUFFER;
	pszSimpleFrom = (char*)malloc(strlen(pszFrom)+1);
	if(pszSimpleFrom==NULL)
	{
		free(pszDomain);
		return SMTP_ERROR_BUFFER;
	}
	pszFromRemaining = (char*)malloc(strlen(pszFrom)+1);
	if(pszFromRemaining==NULL)
	{
		free(pszDomain);
		free(pszSimpleFrom);
		return SMTP_ERROR_BUFFER;
	}

	strcpy(pszDomain, pszFrom);
	strcpy(pszSimpleFrom, pszFrom);
	strcpy(pszFromRemaining, pszFrom);
	bool bValidFound = false;

	while((!bValidFound)&&(strlen(pszFromRemaining)>0))
	{
		pch = strchr(pszFromRemaining, '@');
		if (pch != NULL) 
		{
			strcpy(pszDomain, pch+1);  // get domain start

			while((pch>pszFromRemaining)&&(*pch!='<'))
			{
				pch--;
			}

			if(*pch=='<') pch++;
			strcpy(pszSimpleFrom, pch);  // get from start

			pch = strchr(pszDomain, '>');
			if (pch != NULL) 
			{
				strcpy(pszFromRemaining, pch);
				pszDomain[pch-pszDomain] = 0;
			}
			else
			{
				pch = strchr(pszDomain, ',');
				if (pch != NULL) 
				{
					strcpy(pszFromRemaining, pch);
					pszDomain[pch-pszDomain] = 0;
				}
				else
				{
					//no more separators found!
					strcpy(pszFromRemaining, ""); // discontinue.
				}
			}

			pch = strchr(pszSimpleFrom, '>');
			if (pch != NULL) 
			{
				pszSimpleFrom[pch-pszSimpleFrom] = 0;
			}
			else
			{
				pch = strchr(pszSimpleFrom, ',');
				if (pch != NULL) 
				{
					pszSimpleFrom[pch-pszSimpleFrom] = 0;
				}
			}
		}
		else
		{
			free(pszDomain);
			pszDomain = (char*)malloc(strlen("VideoDesignSoftware.com")+1);
			strcpy(pszDomain, "VideoDesignSoftware.com"); //default value
			strcpy(pszSimpleFrom, "info@VideoDesignSoftware.com");
		}

		index=strlen(pszDomain);
		if(index<=MAXLEN_DOMAIN)
		{
			if(index>0)	bValidFound = true;
		}
	}
 
  if(!bValidFound)
	{
		free(pszDomain);
		pszDomain = (char*)malloc(strlen("VideoDesignSoftware.com")+1);
		strcpy(pszDomain, "VideoDesignSoftware.com"); //default value
		strcpy(pszSimpleFrom, "info@VideoDesignSoftware.com");
	}
	if(pulBytesTotal)
	{
		*pulBytesTotal+=(strlen(pszDomain)+7); 
		*pulBytesTotal+=(strlen(pszSimpleFrom)+14); // MAIL FROM: 
	}

	free(pszFromRemaining);
	pszFromRemaining = NULL;

	// pre-process the attachments.
	int nNumAttach=0;
	if((pszAttachFiles!=NULL)&&(strlen(pszAttachFiles)))
	{
		// determine nNumAttach
		bool bDone=false;

		i=0;
		CSafeBufferUtil sbu;
		char delims[3] = { 13,10,0 };
		char* pchFile = sbu.Token(pszAttachFiles, strlen(pszAttachFiles), delims);
		while((pchFile!=NULL)&&(i<MAXNUM_ATTACHMENTS))
		{
			_snprintf(szTemp, MAXLEN_TEXT-1, "Encoding \"%s\"", pchFile);
			Log(szTemp, MAILLOG_ENCODE);
			strcpy(szTemp, "");
			ulBytes = 0;
			pszEncodedAttachFileName[i] = (char*) malloc(strlen(pchFile)+1);
			if(pszEncodedAttachFileName[i]) strcpy(pszEncodedAttachFileName[i], pchFile);
			if(!EncodeAttachment(pchFile, &(pszEncodedAttach[i]), &ulBytes))
			{
				sprintf(szTemp, "Error encoding \"%s\"", pszEncodedAttachFileName[i]);
				if(pszEncodedAttachFileName[i]) free(pszEncodedAttachFileName[i]);
				Log(szTemp, MAILLOG_ERROR);
			}
			else
			{
				sprintf(szTemp, "Finished encoding \"%s\"", pchFile);
				Log(szTemp, MAILLOG_ENCODE);
				// add together the encoding length with headers etc, (real transmission length)
				if(pulBytesTotal)	*pulBytesTotal+=ulBytes; 
				i++;
				nNumAttach++;
			}
			pchFile = sbu.Token(NULL, NULL, delims);
//AfxMessageBox(pchFile);
		}
	}

	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
	{
		// add length of MIME headers.
		if(pulBytesTotal)	*pulBytesTotal+=92; 
		// add length of multipart header for body message.
		if(pulBytesTotal)	*pulBytesTotal+=189; 
		// add end boundary for body message.
		if(pulBytesTotal)	*pulBytesTotal+=33; 
		// add "--" for last boundary.
		if(pulBytesTotal)	*pulBytesTotal+=2; 
	}

  // open a connection to the remote mail server
	nReturnCode = m_net.OpenConnection(pszMailServer, nPort, &s1, 15000, 30000);  // 15 seond timeout to send, 30 to receive replies
  if (nReturnCode<NET_SUCCESS) 
  { 
		Log("OpenConnection", MAILLOG_SENDERROR);
		Log(pszMailServer, MAILLOG_SENDERROR);  // so we know what server we just had the error with...
		if(pszFromRemaining) free(pszFromRemaining);
		if(pszSimpleFrom) free(pszSimpleFrom);
		if(pszDomain) free(pszDomain);
		return nReturnCode;
  }
	else Log(pszMailServer, MAILLOG_SEND);

  nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1, NET_RCV_EOLN|EOLN_CRLF);
  if (nInternalReturnCode<NET_SUCCESS)
	{
		Log("OpenConnection recv", MAILLOG_RECVERROR);
	} else { if(pchRec){ free(pchRec); pchRec=NULL;} } // just for welcome message if it exists

	// RFC 821
  sprintf(szTemp, "HELO %s%c%c", pszDomain, 13,10); 

	nInternalReturnCode = m_net.SendLine((unsigned char*)(&szTemp), strlen(szTemp), s1, EOLN_NONE, false);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(szTemp, MAILLOG_SENDERROR);
	else
	{
		Log(szTemp, MAILLOG_SEND);
		if(pulBytesWritten)	*pulBytesWritten+=(strlen(pszDomain)+7); 
	}
	free(pszDomain);
	pszDomain = NULL;

	bValidFound=false; // have to eat up service ready replies
	i=0;
	while(!bValidFound)
	{
		nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_EOLN|EOLN_CRLF); 

		if (nInternalReturnCode<NET_SUCCESS) 
		{
			if(i>0) bValidFound=true; // not really valid, but need to exit loop.  
			// if i>0 we probably got a code <221 at some point and there are no more replies.
			Log("Service ready errors", MAILLOG_RECVERROR);
			i++; // this is just in case there is no answer at all! (causes one retry before exiting loop)
		}
		else
		{
			i++;
			Log((char*)pchRec, MAILLOG_RECV);
			nInternalReturnCode = InterpretCode(pchRec);
			if(pchRec) { free(pchRec); pchRec=NULL;}
			if(nInternalReturnCode==SMTP_REPLY_FATAL)	{ nReturnCode=nInternalReturnCode; bValidFound=true; goto bad; }
			else
			if(nInternalReturnCode==SMTP_REPLY_CMDOK)	{ bValidFound=true; }
		}
	}

  sprintf(szTemp, "MAIL FROM: %s%c%c", pszSimpleFrom, 13,10);  

	nInternalReturnCode = m_net.SendLine((unsigned char*)(&szTemp), strlen(szTemp), s1, EOLN_NONE, false);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(szTemp, MAILLOG_SENDERROR);
	else
	{
		Log(szTemp, MAILLOG_SEND);
		if(pulBytesWritten)	*pulBytesWritten+=(strlen(pszSimpleFrom)+13); 
	}

	free(pszSimpleFrom);
	pszSimpleFrom = NULL;

	nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_EOLN|EOLN_CRLF );
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(szTemp, MAILLOG_RECVERROR);
	else
	{
		Log((char*)pchRec, MAILLOG_RECV);
		nInternalReturnCode = InterpretCode(pchRec);
		if(pchRec) { free(pchRec); pchRec=NULL;}

		if(nInternalReturnCode==SMTP_REPLY_FATAL)	{ nReturnCode=nInternalReturnCode; goto bad; }
	}

	strcpy(szTemp,"");

  for (i=0; i<(int)strlen(pszTo); i++)
  {
    if (pszTo[i] == ',')
    {
			nLen = strlen(szTemp);
			while((nLen>0)&&(isspace(szTemp[nLen-1]))) {szTemp[nLen-1]=0; nLen--;}  //trims right

			sprintf(szText, "RCPT TO: %s%c%c", (LPCTSTR) szTemp, 13,10);  

			if(strlen(szText)<=(MAXLEN_CMD-2)) // -2 for CRLF
			{
				if(pulBytesTotal)	*pulBytesTotal+=(strlen(szTemp)+11); 
				nInternalReturnCode = m_net.SendLine((unsigned char*)(&szText), strlen(szText), s1, EOLN_NONE, false);
				if (nInternalReturnCode<NET_SUCCESS) 
					Log(szText, MAILLOG_SENDERROR);
				else
				{
					Log(szText, MAILLOG_SEND);
					if(pulBytesTotal)	*pulBytesWritten+=(strlen(szTemp)+11);
				}

				nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_EOLN|EOLN_CRLF);
				if (nInternalReturnCode<NET_SUCCESS) 
					Log(szText, MAILLOG_RECVERROR);
				else
				{
					Log((char*)pchRec, MAILLOG_RECV);
					if(InterpretCode(pchRec)!=SMTP_REPLY_FATAL)	{ nNumSuccessfulRCPT++; }
					if(pchRec) { free(pchRec); pchRec=NULL;}
				}
			}
			else
			{
				sprintf(szText, "Invalid recipient: %s\n", szTemp); 
				Log(szText, MAILLOG_ERROR);
			}
			strcpy(szTemp,"");
    }
    else 
    {
      if (!isspace(pszTo[i])) sprintf(szTemp, "%s%c", szTemp, (char) pszTo[i]);
    }
  }

  if (szTemp != "")
  {
		while((nLen>0)&&(isspace(szTemp[nLen-1]))) {szTemp[nLen-1]=0; nLen--;}  //trims right
    sprintf(szText, "RCPT TO: %s%c%c", (LPCTSTR) szTemp, 13,10);  

		if(strlen(szText)<=MAXLEN_CMD)
		{
			if(pulBytesTotal)	*pulBytesTotal+=(strlen(szTemp)+11); 
			nInternalReturnCode = m_net.SendLine((unsigned char*)(&szText), strlen(szText), s1, EOLN_NONE, false);
			if (nInternalReturnCode<NET_SUCCESS) 
				Log(szText, MAILLOG_SENDERROR);
			else
			{
				Log(szText, MAILLOG_SEND);
				if(pulBytesWritten)	*pulBytesWritten+=(strlen(szTemp)+11);
			}

			nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_EOLN|EOLN_CRLF);
			if (nInternalReturnCode<NET_SUCCESS) 
				Log(szText, MAILLOG_RECVERROR);
			else
			{
				Log((char*)pchRec, MAILLOG_RECV);
				if(InterpretCode(pchRec)!=SMTP_REPLY_FATAL)	{ nNumSuccessfulRCPT++; }
				if(pchRec) { free(pchRec); pchRec=NULL;}
			}
		}
		else
		{
			sprintf(szText, "Invalid recipient: %s\n", szTemp); 
			Log(szText, MAILLOG_ERROR);
		}
  }

	if(nNumSuccessfulRCPT<=0)
	{
		nReturnCode=SMTP_ERROR_NORCPT; goto bad;
	}

  sprintf(szText, "DATA%c%c", 13,10);  
	//if(pulBytesTotal)	*pulBytesTotal+=6; //constant, got it in init
	nInternalReturnCode = m_net.SendLine((unsigned char*)(&szText), strlen(szText), s1, EOLN_NONE, false);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(szText, MAILLOG_SENDERROR);
	else
	{
		Log(szText, MAILLOG_SEND);
		if(pulBytesWritten)	*pulBytesWritten+=6; 
	}

  nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_EOLN|EOLN_CRLF);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(szText, MAILLOG_RECVERROR);
	else
	{
		Log((char*)pchRec, MAILLOG_RECV);
		nInternalReturnCode = InterpretCode(pchRec);
		if(pchRec) { free(pchRec); pchRec=NULL;}
		if(nInternalReturnCode==SMTP_REPLY_FATAL)	{ nReturnCode=nInternalReturnCode; goto bad; }
	}
  

  // send the message headers
  sprintf(szText, "To:  %s", pszTo);
	SendText(szText, s1, pulBytesWritten, pulBytesTotal);

  sprintf(szText, "From:  %s", pszFrom); 
	SendText(szText, s1, pulBytesWritten, pulBytesTotal);

	if((pszSubject)&&(strlen(pszSubject)>0)) 
	{
		sprintf(szText, "Subject: %s", pszSubject); 
		SendText(szText, s1, pulBytesWritten, pulBytesTotal);
	}

	if((pszReplyTo)&&(strlen(pszReplyTo)>0)) 
  {
    sprintf(szText, "Reply-To: %s", pszReplyTo);  
		SendText(szText, s1, pulBytesWritten, pulBytesTotal);
  }
	if((pszAddlHeaders)&&(strlen(pszAddlHeaders)>0)) 
  {
		nLen = strlen(pszAddlHeaders);
		while((nLen>0)&&(isspace(pszAddlHeaders[nLen-1]))) {pszAddlHeaders[nLen-1]=0; nLen--;}  //trims right
		SendText(pszAddlHeaders, s1, pulBytesWritten, pulBytesTotal);
  }

	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
  {
    SendText("MIME-Version: 1.0", s1, pulBytesWritten, pulBytesTotal);	          // =19 char w/CRLF
		char* pchBound = m_bu.Base64Boundary();

		if(pchBound==NULL) {nReturnCode=SMTP_ERROR_BUFFER; goto bad;}// hmmm  no boundary...
		sprintf(szText, "Content-Type: multipart/mixed; boundary=\"%s\"", pchBound);	// =73 char w/CRLF
    SendText(szText, s1, pulBytesWritten, pulBytesTotal);													//  92 total

		pszDomain = (char*)malloc(strlen(pchBound)+3);
		if(pszDomain==NULL) {nReturnCode=SMTP_ERROR_BUFFER; goto bad;}// hmmm  no boundary...
		sprintf(pszDomain, "--%s", pchBound); // add preceding two dashes
		free(pchBound);
  }
//	else	strcpy(pszDomain,""); nothing, pszDomain is NULL


	// add 3 chars to to init
	//not sure why I had this as 3 chars, this results in a "bare LF", not good.
//	nInternalReturnCode = m_net.SendLine((unsigned char*)("\n"), 1, s1, EOLN_CRLF, false);

//  if (nInternalReturnCode<NET_SUCCESS) 
//		Log("<LF><CR><LF>", MAILLOG_SENDERROR);
//	else
//	{
//		Log("<LF><CR><LF>", MAILLOG_SEND);
//		if(pulBytesWritten)	*pulBytesWritten+=3; 
//	}

	// changed to 4 chars
	nInternalReturnCode = m_net.SendLine((unsigned char*)("\r\n"), 2, s1, EOLN_CRLF, false);

  if (nInternalReturnCode<NET_SUCCESS) 
		Log("<CR><LF><CR><LF>", MAILLOG_SENDERROR);
	else
	{
		Log("<CR><LF><CR><LF>", MAILLOG_SEND);
		if(pulBytesWritten)	*pulBytesWritten+=4; 
	}

	if((pszAttachFiles!=NULL)&&(nNumAttach>0)&&(pszDomain))
	{
		// make this a multipart.
		SendText("This is a multi-part message in MIME format.", s1, pulBytesWritten, pulBytesTotal);	// =46 char w/CRLF
		SendText("", s1, pulBytesWritten, pulBytesTotal);																							// = 2 char w/CRLF
		SendText(pszDomain, s1, pulBytesWritten, pulBytesTotal);																			// =33 char w/CRLF
		SendText("Content-Type: text/plain; charset=us-ascii", s1, pulBytesWritten, pulBytesTotal);		// =44 char w/CRLF
		SendText("Content-Disposition: inline", s1, pulBytesWritten, pulBytesTotal);									// =29 char w/CRLF
		SendText("Content-Transfer-Encoding: 7bit", s1, pulBytesWritten, pulBytesTotal);							// =33 char w/CRLF
		SendText("", s1, pulBytesWritten, pulBytesTotal);																							// = 2 char w/CRLF
																																										// 189 total
	}

  // body
  if (bUseFile)
  {
    FILE *fp;
    fp = fopen(pszData, "rt");
		strcpy(szText,"");
    if (fp != NULL)
    {
			unsigned long ulTotalDataLength = _filelength( _fileno( fp));
			pch = (char*) malloc(ulTotalDataLength+1);
			if (pch==NULL) {nReturnCode=SMTP_ERROR_BUFFER; goto bad;}
			if(pulBytesTotal)	*pulBytesTotal+=ulTotalDataLength+2;// final CRLF 
			fread(pch, sizeof(char), ulTotalDataLength, fp);
			fclose(fp);
			*(pch+ulTotalDataLength)=0;  // term 0!
			SendText(pch, s1, pulBytesWritten, pulBytesTotal);
			free(pch);
    }
  }
  else
  {
//		AfxMessageBox(pszData);
		SendText(pszData, s1, pulBytesWritten, pulBytesTotal);
  }

	//if(pulBytesTotal)	*pulBytesTotal+=4; //CRLFCRLF  //constant, got it in init
  SendText("", s1, pulBytesWritten, pulBytesTotal);
  SendText("", s1, pulBytesWritten, pulBytesTotal);

//  now the multipart attachments if any
	if((nNumAttach>0)&&(pszAttachFiles!=NULL)&&(pszDomain))
	{
		SendText(pszDomain, s1, pulBytesWritten, pulBytesTotal); // 33 chars
		for(i=0; i<nNumAttach; i++)
		{
			if(!SendAttachment(pszEncodedAttachFileName[i], pszEncodedAttach[i], pszDomain, s1, pulBytesWritten, pulBytesTotal))
			{
				sprintf(szTemp, "Send Attachment %s error", pszEncodedAttachFileName[i]);
				Log(szTemp, MAILLOG_SENDERROR);
			}
			else
			{
				if(i<(nNumAttach-1)) // non-final
					SendText("", s1, pulBytesWritten, pulBytesTotal);  // <CRLF>
			}

			free(pszEncodedAttach[i]); //release the buffer
			free(pszEncodedAttachFileName[i]); //release the buffer
		}
		SendText("--", s1, pulBytesWritten, pulBytesTotal);  // last multipart with <CRLF>
	}

  // end of data
	nInternalReturnCode = m_net.SendLine((unsigned char*)("."), 1, s1, EOLN_CRLF, false);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(".<CR><LF>", MAILLOG_SENDERROR);
	else
	{
		Log(".<CR><LF>", MAILLOG_SEND);
		if(pulBytesWritten)	*pulBytesWritten+=3; 
	}

  nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_ONCE|NET_RCV_EOLN|EOLN_CRLF);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log("End data", MAILLOG_RECVERROR);
	else
	{
		Log((char*)pchRec, MAILLOG_RECV);
		if(pchRec) { free(pchRec); pchRec=NULL;}
		//InterpretCode(pchRec);  // dont need to check the return code
	}
  
bad:
  sprintf(szTemp, "QUIT%c%c", 13,10);  
	nInternalReturnCode = m_net.SendLine((unsigned char*)(&szTemp), 6, s1, EOLN_NONE, false);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log(szTemp, MAILLOG_SENDERROR);
	else
	{
		Log(szTemp, MAILLOG_SEND);
		if(pulBytesWritten)	*pulBytesWritten+=6; 
	}

// the following is commented out because some servers just cut the channel
// without responding with a 221, causing us to timeout waiting for a reply.
// we dont need a reply, we can just close the connection, because we are done.
/*
  nInternalReturnCode = m_net.GetLine(&pchRec, &nLen, s1,  NET_RCV_EOLN|EOLN_CRLF);
  if (nInternalReturnCode<NET_SUCCESS) 
		Log("QUIT", MAILLOG_RECVERROR);
	else
	{
		Log((char*)pchRec, MAILLOG_RECV);
		if(pchRec) { free(pchRec); pchRec=NULL;}
		//InterpretCode(pchRec);  // dont need to check the return code
	}
*/
  // close the connection to the mail server
	nInternalReturnCode = m_net.CloseConnection(s1);
  if (nInternalReturnCode<NET_SUCCESS) 
  { 
		Log("CloseConnection" , MAILLOG_ERROR);
  }
	else
	{
		Log("CloseConnection", MAILLOG_CLOSE);
	}

	if(pszFromRemaining) free(pszFromRemaining); // should already be free
	if(pszSimpleFrom) free(pszSimpleFrom); // should already be free
	if(pszDomain) free(pszDomain);

	m_bFileBusy = false; // allows changing the logfile when finished writing out a messagelog
  return nReturnCode;
}


bool CSMTP::SendText(char* pszInput, SOCKET s, unsigned long* pulBytesWritten, unsigned long* pulBytesTotal)
{

	if(s==NULL) 
	{
		Log("Null socket.", MAILLOG_SENDERROR);
		return false;
	}

	bool bReturnCode=true;
	bool bMalloc=false;
	unsigned char line[MAXLEN_TEXT+2];
	int i=0, nLen=0, nLineLen=0;
	char* pch;
	char* pszText=NULL;

	if((pszInput)&&(strlen(pszInput)<=0)) // then just send CRLF
	{
		sprintf((char*)line, "%c%c",13,10); // CRLF
		
		i = m_net.SendLine(line, 2, s, EOLN_NONE, false);
		if (i<NET_SUCCESS) 
		{
			Log((char*)line, MAILLOG_SENDERROR);
			return false;
		}
		else
		{
			Log((char*)line, MAILLOG_SEND);
			if(pulBytesWritten)	*pulBytesWritten+=2; 
			return true;
		}
	}
	else
	{
		pszText = (char*)malloc(strlen(pszInput)+1);
		if(pszText)
		{
			bMalloc=true;
			strcpy(pszText, pszInput); // dont screw up the original buffer
		}
		else pszText=pszInput;  // else just use the input buffer...(backup plan)...
	}

	while((pszText)&&(strlen(pszText)>0))
	{
		sprintf((char*)line, "%c%c",13,10); // CRLF
		pch = strstr(pszText, (char*)line);
		if(pch!=NULL)
		{
			if((MAXLEN_TEXT-2)==((pch-pszText)+1)) // special case, breaks CRLF
				nLen = (pch-pszText);
			else
				nLen = min((pch-pszText)+2,MAXLEN_TEXT-2);
		}
		else nLen = min(MAXLEN_TEXT-2, strlen(pszText));


		if(pszText[0]=='.')
		{

// RFC 821
//      4.5.2.  TRANSPARENCY
//         Without some provision for data transparency the character
//         sequence "<CRLF>.<CRLF>" ends the mail text and cannot be sent
//         by the user.  In general, users are not aware of such
//         "forbidden" sequences.  To allow all user composed text to be
//         transmitted transparently the following procedures are used.
//
//            1. Before sending a line of mail text the sender-SMTP checks
//            the first character of the line.  If it is a period, one
//            additional period is inserted at the beginning of the line.

			sprintf((char*)line, ".");
			memcpy(line+1, pszText, nLen);
			line[nLen+1]=0; //null term
			
			nLineLen=strlen((char*)line);

			if(pulBytesTotal) *pulBytesTotal++; // for the extra dot
		}
		else
		{
//		AfxMessageBox(pszText);
			memcpy(line, pszText, nLen);
			line[nLen]=0; //null term
//		AfxMessageBox((char*)line);
			nLineLen=strlen((char*)line);
		}

		i=strlen(pszText);
		if(i>nLen)
		{
			memcpy(pszText, pszText+nLen, i-nLen+1);  //+1 for null term

			i = m_net.SendLine(line, nLineLen, s, EOLN_NONE, false);
			if (i<NET_SUCCESS) 
			{
				Log((char*)line, MAILLOG_SENDERROR);
				if((bMalloc)&&(pszText)) free(pszText);
				return false;
			}
			else
			{
				Log((char*)line, MAILLOG_SEND);
				if(pulBytesWritten)	*pulBytesWritten+=nLineLen; 
			}
		}
		else
		{
			strcpy(pszText,""); // escapes while
			i = m_net.SendLine(line, nLineLen, s, EOLN_CRLF, false);
			if (i<NET_SUCCESS) 
			{
				line[strlen((char*)line)+2]=0;
				line[strlen((char*)line)+1]=10;
				line[strlen((char*)line)]  =13;
				Log((char*)line, MAILLOG_SENDERROR);
				if((bMalloc)&&(pszText)) free(pszText);
				return false;
			}
			else
			{
				line[strlen((char*)line)+2]=0;
				line[strlen((char*)line)+1]=10;
				line[strlen((char*)line)]  =13;
				Log((char*)line, MAILLOG_SEND);
				if(pulBytesWritten)	*pulBytesWritten+=(nLineLen+2); 
			}
		}
	}
	if((bMalloc)&&(pszText)) free(pszText);
	return bReturnCode;
}

bool CSMTP::EncodeAttachment(char* pszFileName, char** ppszEncodedData, unsigned long* pulTotalBytes)
{
	char* pszText = (char*)malloc(strlen(pszFileName)+32); //extra in case need to use for error
	if((pszText==NULL)||(ppszEncodedData==NULL)) return false;
	char szFilename[MAX_PATH];
	strcpy(szFilename, pszFileName);
	sprintf(pszText, "%s", pszFileName);
	if(!m_bu.IsFileString(pszFileName, true, true)) 
	{
		sprintf(pszText, "Invalid attachment filename \"%s\"",pszFileName);
		Log(pszText, MAILLOG_SENDERROR);
		free(pszText);
		return false;
	}

	int nReturn;
	unsigned long ulBytes;
//	 	  AfxMessageBox(pszFile);
	nReturn = m_bu.Base64EncodeFromFile(&pszText, &ulBytes, true);  //true frees the input buffer with filename
	if(nReturn>=RETURN_SUCCESS)
	{
		*ppszEncodedData = pszText;
//	 	  AfxMessageBox(*ppszEncodedData);

		if(pulTotalBytes) 
		{
			*pulTotalBytes=( (ulBytes/(MAXLEN_ATTACHLINE-2))*MAXLEN_ATTACHLINE )
				+ ( (ulBytes%(MAXLEN_ATTACHLINE-2))?(ulBytes%(MAXLEN_ATTACHLINE-2)+2):0);

			char szString[_MAX_PATH+50];
			char* pszPath = NULL;
			char* pszFile = NULL;
			char* pszExt = NULL;

			m_bu.BreakFileString(szFilename, &pszPath, &pszFile, &pszExt);
			if((pszExt==NULL)||(strlen(pszExt)<=0)) // now we just check for existence, later we will add lookups for registered types
			{
				_snprintf(szString, _MAX_PATH+49, "Content-Type: application/octet-stream; name=\"%s\"", pszFile);
			}
			else
			{
				_snprintf(szString, _MAX_PATH+49, "Content-Type: application/octet-stream; name=\"%s.%s\"", pszFile, pszExt);
			}
//	 	  AfxMessageBox(szString);

			if(pszPath) free(pszPath);
			if(pszFile) free(pszFile);
			if(pszExt) free(pszExt);

			*pulTotalBytes+=strlen(szString)+107;
			//CRLF + len("Content-Disposition: attachment")=31 +CRLF 
				    //+len("Content-Transfer-Encoding: base64")=33 +CRLF+CRLF+ (omit body) +CRLF +len(Boundary)=31+CRLF
//			szPath.Format("%d bytes expected", *pnTotalBytes);
//	 	  AfxMessageBox(szPath);
		}
	}
	else
	{
		_snprintf(szFilename, MAX_PATH-1,"Encoding error, code %d", nReturn );
		if(pulTotalBytes) *pulTotalBytes=0;
		Log(szFilename, MAILLOG_ERROR);
		if(pszText) free(pszText);
		return false; // encode error
	}
	return true;
}

bool CSMTP::SendAttachment(char* pszFileName, char* pszEncodedData, char* pszBoundary, SOCKET s, unsigned long* pulBytesWritten, unsigned long* pulBytesTotal)
{
	char szText[128];
	bool bReturn = true;
	if(strlen(pszBoundary)<=0)
	{
		sprintf(szText, "Invalid boundary string \"%s\"",pszBoundary);
		Log(szText, MAILLOG_SENDERROR);
		return false;
	}
	if(s==NULL) 
	{
		Log("Null socket.", MAILLOG_SENDERROR);
		return false;
	}

//	int n=*pulBytesWritten;

	if((pszEncodedData)&&(strlen(pszEncodedData)>0))
	{
//AfxMessageBox(pszFileName);
		// OK, we managed to encode the file, so we have to put in part headers now;
		char line[MAXLEN_ATTACHLINE+1];  //null term if nec
		unsigned long nLen=0;
		char szFilename[MAX_PATH];
		char szString[MAX_PATH+50];
		char* pszPath=NULL;
		char* pszFile=NULL;
		char* pszExt=NULL;
		strcpy(szFilename, pszFileName);

		m_bu.BreakFileString(szFilename, &pszPath, &pszFile, &pszExt);
		if((pszExt==NULL)||(strlen(pszExt)<=0)) // now we just check for existence, later we will add lookups for registered types
		{
			_snprintf(szString, MAX_PATH+49, "Content-Type: application/octet-stream; name=\"%s\"", pszFile);
		}
		else
		{
			_snprintf(szString, MAX_PATH+49, "Content-Type: application/octet-stream; name=\"%s.%s\"", pszFile, pszExt);
		}

		if(pszPath) free(pszPath);
		if(pszFile) free(pszFile);
		if(pszExt) free(pszExt);

		if(!SendText(szString, s, pulBytesWritten, pulBytesTotal)) return false;
		if(!SendText("Content-Disposition: attachment", s, pulBytesWritten, pulBytesTotal)) { bReturn = false; goto boundary; }
		if(!SendText("Content-Transfer-Encoding: base64", s, pulBytesWritten, pulBytesTotal)) { bReturn = false; goto boundary; }
		SendText("", s, pulBytesWritten, pulBytesTotal);

		_snprintf(szString, MAX_PATH+49, "Sending base64 encoded data for \"%s\"", pszFileName);
		Log(szString, MAILLOG_SEND);

		while(strlen(pszEncodedData)>0)
		{
			nLen=min(MAXLEN_ATTACHLINE-2, strlen(pszEncodedData));
			memcpy(line, pszEncodedData, nLen);
			memset(line+nLen, 13, 1);
			memset(line+nLen+1, 10, 1);
			memset(line+nLen+2, 0, 1);
			nLen=strlen(pszEncodedData);
			if(nLen>(MAXLEN_ATTACHLINE-2))
			{
				memcpy(pszEncodedData, pszEncodedData+(MAXLEN_ATTACHLINE-2), nLen-(MAXLEN_ATTACHLINE-2));
				pszEncodedData[nLen-(MAXLEN_ATTACHLINE-2)]=0; //null term
			}
			else
			{
				strcpy(pszEncodedData,"");
			}
//						AfxMessageBox(line);

			nLen = m_net.SendLine((unsigned char*)(&line), strlen(line), s, EOLN_NONE, false);
			if (nLen<NET_SUCCESS) 
			{
				Log("Attachment send error", MAILLOG_SENDERROR);
				{ bReturn = false; goto boundary; }
			}
			else
			{
// not logging all the attachment source, way too long.
//				Log(line, MAILLOG_SEND);
				if(pulBytesWritten)	*pulBytesWritten+=(strlen(line)); 
			}
		}
// the following CRLF is optional

		if(m_net.SendLine((unsigned char*)(""), 0, s, EOLN_CRLF, false)<NET_SUCCESS)
		{
			Log("<CR><LF>", MAILLOG_SENDERROR);
			{ bReturn = false; goto boundary; }
		}
		else
		{
			Log("<CR><LF>", MAILLOG_SEND);
			if(pulBytesWritten)	*pulBytesWritten+=2; 
		}
boundary:
		sprintf(line, "%s", pszBoundary);  //have to send CRLF outside
		nLen = m_net.SendLine((unsigned char*)(&line), strlen(line), s, EOLN_NONE, false);
		if (nLen<NET_SUCCESS) 
		{
			Log(line, MAILLOG_SENDERROR);
			return false;
		}
		else
		{
			Log(line, MAILLOG_SEND);
			if(pulBytesWritten)	*pulBytesWritten+=(strlen(line));  
		}
	}
	else
	{
		sprintf(szText, "Encoded attachment had zero length.");
		Log(szText, MAILLOG_ERROR);
		return false; // encode error
	}
//	szText.Format("%d bytes sent", *pulBytesWritten-n);
//	AfxMessageBox(szText);

	return bReturn;
}


void CSMTP::Log(char* pszText, int nType)
{
	if((m_fp != NULL)&&(m_bLogActivity)&&(pszText))  
	{
		char* pszError = (char*) malloc(strlen(pszText)+256);
		if(pszError)
		{
			struct _timeb timebuffer;
			_ftime( &timebuffer );

			char buffer[36]; // need 33;
			tm* theTime = localtime( &timebuffer.time	);
			strftime( buffer, 30, "%Y-%b-%d %H:%M:%S.", theTime );
			
			sprintf(pszError, "%s%03d", buffer, timebuffer.millitm);
			
			switch(nType)
			{
			case MAILLOG_SEND://  3
				{
					sprintf(pszError, "%s SND: ", pszError);
				} break;
			case MAILLOG_RECV://  4
				{
					sprintf(pszError, "%s REC: ", pszError);
				} break;
			case MAILLOG_CLOSE://  5
				{
					sprintf(pszError, "%s END: ", pszError);
				} break;
			case MAILLOG_ENCODE://  6
				{
					sprintf(pszError, "%s ENC: ", pszError);
				} break;
			case MAILLOG_SENDERROR:// 1
				{
					sprintf(pszError, "%s ERR(TX): ", pszError);
				} break;
			case MAILLOG_RECVERROR:// 2
				{
					sprintf(pszError, "%s ERR(RX): ", pszError);
				} break;
			case MAILLOG_ERROR:// 0
			default:
				{
					sprintf(pszError, "%s ERR: ", pszError);
				} break;
			}

			int i=0, nLen=strlen(pszText);
			char ch;
			while(i<nLen)
			{
				ch = pszText[i];
				if(ch==10)
				{
					if(nType==MAILLOG_CLOSE)
						strcat(pszError, "%s\nEND: "); // dont replace CRLFs
					else
						strcat(pszError,"<LF>");
				}
				else
				if(ch==13)
				{
					strcat(pszError,"<CR>");
				}
				else 
				{
					pszError[strlen(pszError)+1] = 0;
					pszError[strlen(pszError)] = ch;
				}
				i++;
			}
			strcat(pszError,"\n");
			if(nType==MAILLOG_CLOSE) strcat(pszError,"\n"); // some space between emails.
			fprintf(m_fp, pszError);
			fflush(m_fp);
			free(pszError);
		}
	}
}

char* CSMTP::EnsureNoBareLFs(char* pszText, bool bFreeIncomingBuffer)
{
	char* pchReturn	= NULL;

	if(pszText)
	{
		unsigned long ulLen = strlen(pszText);
		unsigned long ulNumLFs = m_bu.CountChar(pszText, strlen(pszText), '\n');
		ulNumLFs += strlen(pszText);

		ulLen = ulNumLFs;
		pchReturn = (char*)malloc(ulNumLFs+1); //max possible size
		if(pchReturn)
		{
			ulNumLFs = 0;
			char* pch=pszText;
			char chPrev=0;
			while((*pch != 0)&&(ulLen > ulNumLFs))
			{
				if((*pch) == 10)  //LF
				{
					if (chPrev == 13)  //all set already
					{
						*(pchReturn+ulNumLFs) = (*pch);
						ulNumLFs++;
					}
					else // add the CR
					{
						*(pchReturn+ulNumLFs) = 13;
						ulNumLFs++;
						*(pchReturn+ulNumLFs) = (*pch);
						ulNumLFs++;
					}
				}
				else  // not an LF, just copy to the new buffer
				{
					*(pchReturn+ulNumLFs) = (*pch);
					ulNumLFs++;
				}

				chPrev = (*pch);
				pch++;
			}
			*(pchReturn+ulNumLFs) = 0;// zero terminate.

			if(bFreeIncomingBuffer)
			{
				try { free(pszText);} catch(...){}
			}
		}
	}

	return pchReturn;
}


int CSMTP::InterpretCode(unsigned char* pucText)
{
	/*
August 1982                                                      RFC 821
Simple Mail Transfer Protocol                                           

4.2.2.  NUMERIC ORDER LIST OF REPLY CODES

   211 System status, or system help reply
   214 Help message
      [Information on how to use the receiver or the meaning of a
      particular non-standard command; this reply is useful only
      to the human user]
   220 <domain> Service ready
   221 <domain> Service closing transmission channel
   250 Requested mail action okay, completed
   251 User not local; will forward to <forward-path>

   354 Start mail input; end with <CRLF>.<CRLF>

   421 <domain> Service not available,
       closing transmission channel
      [This may be a reply to any command if the service knows it
      must shut down]
   450 Requested mail action not taken: mailbox unavailable
      [E.g., mailbox busy]
   451 Requested action aborted: local error in processing
   452 Requested action not taken: insufficient system storage

   500 Syntax error, command unrecognized
      [This may include errors such as command line too long]
   501 Syntax error in szDataeters or arguments
   502 Command not implemented
   503 Bad sequence of commands
   504 Command szDataeter not implemented
   550 Requested action not taken: mailbox unavailable
      [E.g., mailbox not found, no access]
   551 User not local; please try <forward-path>
   552 Requested mail action aborted: exceeded storage allocation
   553 Requested action not taken: mailbox name not allowed
      [E.g., mailbox syntax incorrect]
   554 Transaction failed
*/

  int nCode = atol((char*)pucText);  
	if ((nCode >399)||(nCode ==221)) 
	{ 
		char szTemp[MAXLEN_TEXT];
		sprintf(szTemp, "Mail server error: %s\n",pucText); 
		if((m_fp != NULL)&&(m_bLogActivity))  
		{
			fprintf(m_fp, szTemp);
			fflush(m_fp);
		}
		return SMTP_REPLY_FATAL;
	}
	else
	if(nCode == 0) // non-numeric answer... 
	{
		return SMTP_REPLY_FATAL;
	}
	else
	if(nCode < 221) 
	{
		return SMTP_REPLY_SVCREADY;
	}
	return SMTP_REPLY_CMDOK;
}

bool CSMTP::ChangeLogFilename(char* pszLogFile)
{
	if(strcmp(m_szLogFile, pszLogFile)==0) return true; // already done.
	if(strlen(pszLogFile)<=0) return false;
	if(m_bFileBusy) return false; // prevents changing the logfile while writing out a messagelog
	if(m_fp)
	{
		fclose(m_fp);
	}
	m_fp = NULL;
	strcpy(m_szLogFile, pszLogFile);
	// no need to open a new file, the next send will open the new file
	return true;
}

bool CSMTP::ChangeLogFileUsage(bool bLogActivity)
{
	if(m_bLogActivity==bLogActivity) return true; // already done.
	if(m_bFileBusy) return false; // prevents changing the logfile while writing out a messagelog
	if(m_fp) // should be null if we were changing from no logging to logging
	{
		fclose(m_fp);
	}
	m_fp = NULL;
	m_bLogActivity=bLogActivity;
	// no need to open a new file, the next send will open the new file
	return true;
}

