// sendmail.cpp
// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.

#include <time.h>
#include <sys/timeb.h>
#include <io.h>
#include "sendmail.h"

#define MAXLEN_USER    64
#define MAXLEN_DOMAIN  64
#define MAXLEN_CMD     512
#define MAXLEN_REPLY   512
#define MAXLEN_TEXT    1024
#define MAXBUF_RCPT    100
#define MAXLEN_ATTACHLINE      74 //72+2 for crlf
#define MAXLEN_ATTACHLINEPRE   54 //pre-encoded - after base 64 encode, becomes 72 chars
#define MAXNUM_ATTACHMENTS     64 

CSendMail::CSendMail()
{
	m_fp = NULL;
	m_bLogActivity = false;
	m_bFileBusy = false;
	strcpy(m_szLogFile, "vdismtpx.log");
	m_pszDefaultDomain = NULL;
}

CSendMail::~CSendMail()
{
	if(m_fp) fclose(m_fp);
}


int CSendMail::SendMail(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszMsg, char* pszAddlHeaders, 
							char* pszAttachFiles[], int nNumAttach, 
							short nPort, unsigned long* psizeBytesWritten[])
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszMsg, false, pszAddlHeaders, pszAttachFiles, nNumAttach, nPort,
												psizeBytesWritten);
}

int CSendMail::SendMailFromTextFile(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszFilename, char* pszAddlHeaders, 
							char* pszAttachFiles[], int nNumAttach, 
							short nPort, unsigned long* psizeBytesWritten[])
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszFilename, true, pszAddlHeaders, pszAttachFiles, nNumAttach, nPort,
												psizeBytesWritten);
}

// common overrides
int CSendMail::SendMail(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszMsg)
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszMsg, false, "", NULL, 0, 25, NULL);
}

int CSendMail::SendMailFromTextFile(char* pszMailServer,
							char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject, 
              char* pszFilename)
{
	return SendMailInternals(pszMailServer, pszTo, pszFrom, pszReplyTo, pszSubject,
                        pszFilename, true, "", NULL, 0, 25, NULL);
}

  
int CSendMail::SendMailInternals(char* pszMailServer, 
												char* pszTo, char* pszFrom, char* pszReplyTo, char* pszSubject,
                        char* pszData, bool bUseFile,  
                        char* pszAddlHeaders, 
												char* pszAttachFiles[], int nNumAttach, 
												short nPort, unsigned long* psizeBytesWritten[])
{

// RFC 821 section 4.5.3.  SIZES
//has the following limits:
	
// user     The maximum total length of a user szName is 64 characters.
// domain   The maximum total length of a domain szName or number is 64 characters.
// path     The maximum total length of a reverse-path or forward-path is 256 characters (including the punctuation and element separators).
// command line  The maximum total length of a command line including the command word and the <CRLF> is 512 characters.
// reply line    The maximum total length of a reply line including the reply code and the <CRLF> is 512 characters.
// text line     The maximum total length of a text line including the <CRLF> is 1000 characters (but not counting the leading dot duplicated for transparency).
// recipients buffer  The maximum total number of recipients that must be buffered is 100 recipients.
  
// These limits are #defined above
//#define MAXLEN_USER    64
//#define MAXLEN_DOMAIN  64
//#define MAXLEN_CMD     512
//#define MAXLEN_REPLY   512
//#define MAXLEN_TEXT    1024
//#define MAXBUF_RCPT    100

// the unsigned long* psizeBytesWritten[] is provided to feed back bytes total [0] and bytes written [1]
// it is continuously updated throughout the function, so a pointer array from another thread 
// can be passed in and checked for progress.
	char szText[MAXLEN_TEXT], szTemp[MAXLEN_TEXT];
	unsigned long nLen;

	if(psizeBytesWritten)
	{
		if(!bUseFile) // then the data is a text message (zero terminated)
			*psizeBytesWritten[0]=(strlen(pszData)+24); //init (+2 for CRLF)
		else // its a file.  length to be determined from the file size and encoding
			*psizeBytesWritten[0]=22; //init

		*psizeBytesWritten[0]+=(strlen(pszFrom)+13); // MAIL FROM: can do this up front.
		// the message headers
		sprintf(szText, "To:  %s", pszTo);  	
		*psizeBytesWritten[0]+=(strlen(szText)+2); //CRLF 

		sprintf(szText, "From:  %s", pszFrom); 
		*psizeBytesWritten[0]+=(strlen(szText)+2); //CRLF 

		sprintf(szText, "Subject: %s", pszSubject); 
		*psizeBytesWritten[0]+=(strlen(szText)+2); //CRLF 

		if (strlen(pszReplyTo)>0)
		{
			sprintf(szText, "Reply-To: %s", pszReplyTo);  
			*psizeBytesWritten[0]+=(strlen(szText)+2); //CRLF 
		}
		if (strlen(pszAddlHeaders)>0)
		{
			nLen = strlen(pszAddlHeaders);
			while((nLen>0)&&(isspace(pszAddlHeaders[nLen-1]))) {pszAddlHeaders[nLen-1]=0; nLen--;}  //trims right

			*psizeBytesWritten[0]+=(strlen(pszAddlHeaders)+2); //CRLF 
		}
		*psizeBytesWritten[1]=0;  //init
	}

	unsigned long ulBytes = 0;
	SOCKET s1;
  char* pszDomain=NULL;
	char* pszFromRemaining=NULL;
  int i, nReturnCode=RETURN_SUCCESS, nInternalReturnCode=RETURN_SUCCESS;
  int index=0;
	int nNumSuccessfulRCPT = 0;
	char* pszEncodedAttach[MAXNUM_ATTACHMENTS];
	char* pch=NULL;
 
	m_bFileBusy = true; // prevents changing the logfile while writing out a messagelog
	if((m_fp == NULL)&&(m_bLogActivity))  m_fp = fopen(m_szLogFile, "at");

//	szTemp.Format("file pointer %d, bLog %d, filename %s",m_fp,m_bLogActivity,m_szLogFile);
//	AfxMessageBox(szTemp);
//	szTemp.Format("%d files\n",nNumAttach);
//	for(i=0; i<nNumAttach; i++)
//		szTemp.Format("%s%s\n", szTemp, szAttachFiles[i]);
//	AfxMessageBox(szTemp);

  // extract the first valid local domain
	pszDomain = (char*)malloc(strlen(pszFrom)+1);
	if(pszDomain==NULL) return ERROR_BUFFER;
	pszFromRemaining = (char*)malloc(strlen(pszFrom)+1);
	if(pszFromRemaining==NULL)
	{
		free(pszDomain);
		return ERROR_BUFFER;
	}

	strcpy(pszDomain, pszFrom);
	strcpy(pszFromRemaining, pszFrom);
	bool bValidFound = false;

	while((!bValidFound)&&(strlen(pszFromRemaining)>0))
	{
		pch = strchr(pszFromRemaining, '@');
		if (pch != NULL) 
		{
			strcpy(pszDomain, pch+1);
			pch = strchr(pszDomain, '>');
			if (pch != NULL) 
			{
				strcpy(pszFromRemaining, pch);
				pszDomain[pch-pszDomain] = 0;
			}
			else
			{
				pch = strchr(pszDomain, ',');
				if (pch != NULL) 
				{
					strcpy(pszFromRemaining, pch);
					pszDomain[pch-pszDomain] = 0;
				}
				else
				{
					//no more separators found!
					strcpy(pszFromRemaining, ""); // discontinue.
				}
			}
		}
		else strcpy(pszDomain, "VideoDesignSoftware.com"); //default value

		index=strlen(pszDomain);
		if(index<=MAXLEN_DOMAIN)
		{
			if(index>0)	bValidFound = true;
		}
	}
 
  if(!bValidFound)
	{
    strcpy(pszDomain, "VideoDesignSoftware.com"); //default value
	}
	if(psizeBytesWritten)	*psizeBytesWritten[0]+=(strlen(pszDomain)+7); 

	free(pszFromRemaining);
	free(pszDomain);

	pszDomain = NULL;
	pszFromRemaining = NULL;

	// pre-process the attachments.
	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
	{
		i=0;
		while((i<nNumAttach)&&(i<MAXNUM_ATTACHMENTS))
		{
			sprintf(szTemp, "Encoding \"%s\"", pszAttachFiles[i]);
			Log(szTemp, LOG_ENCODE);
			strcpy(szTemp, "");
			strcpy(pszEncodedAttach[i], "");
			ulBytes = 0;
			if(!EncodeAttachment(pszAttachFiles[i], pszEncodedAttach[i], &ulBytes))
			{
				sprintf(szTemp, "Error encoding \"%s\"", pszAttachFiles[i]);
				Log(szTemp, LOG_ERROR);
				int j=i;
				while(j<(nNumAttach-1))
				{
					pszAttachFiles[j]=pszAttachFiles[j+1];
					j++;
				}
				nNumAttach--; // either we escape the while by decreasing nNumAttach
			}
			else
			{
				sprintf(szTemp, "Finished encoding \"%s\"", pszAttachFiles[i]);
				Log(szTemp, LOG_ENCODE);
				// add together the encoding length with headers etc, (real transmission length)
				if(psizeBytesWritten)	*psizeBytesWritten[0]+=ulBytes; 
				i++; // or by increasing i
			}
		}
	}

	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
	{
		// add length of MIME headers.
		if(psizeBytesWritten)	*psizeBytesWritten[0]+=92; 
		// add length of multipart header for body message.
		if(psizeBytesWritten)	*psizeBytesWritten[0]+=189; 
		// add end boundary for body message.
		if(psizeBytesWritten)	*psizeBytesWritten[0]+=33; 
	}

  // open a connection to the remote mail server
	nReturnCode = m_net.OpenConnection(pszMailServer, nPort, &s1);
  if (nReturnCode<RETURN_SUCCESS) 
  { 
		Log("OpenConnection", LOG_ENCODE);
    return nReturnCode;
  }
	else Log(szTemp, LOG_SEND);

  nInternalReturnCode = m_net.GetLine(pch, &nLen, s1); 
  if (nInternalReturnCode<RETURN_SUCCESS)
	{
		Log("OpenConnection recv", LOG_RECVERROR);
	} else free(pch); // just for welcome message if it exists

	// RFC 821
  sprintf(szTemp, "HELO %s%c%c", pszDomain, 13,10); 

	nInternalReturnCode = m_net.SendLine(szTemp, strlen(szTemp), s1, EOLN_NONE, false);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szTemp, LOG_SENDERROR);
	else
	{
		Log(szTemp, LOG_SEND);
		if(psizeBytesWritten)	*psizeBytesWritten[1]+=(strlen(pszDomain)+7); 
	}

	bValidFound=false; // have to eat up service ready replies
	i=0;
	while(!bValidFound)
	{
		nInternalReturnCode = m_net.GetLine(pch, &nLen, s1);
		if (nInternalReturnCode<RETURN_SUCCESS) 
		{
			if(i>0) bValidFound=true; // not really valid, but need to exit loop.  
			// if i>0 we probably got a code <221 at some point and there are no more replies.
			Log("Service ready errors", LOG_RECVERROR);
			i++; // this is just in case there is no answer at all! (causes one retry before exiting loop)
		}
		else
		{
			i++;
			Log(pch, LOG_RECV);
			nInternalReturnCode = InterpretCode(pch);
			if(nInternalReturnCode==REPLY_FATAL)	{ nReturnCode=nInternalReturnCode; bValidFound=true; goto bad; }
			else
			if(nInternalReturnCode==REPLY_CMDOK)	{ bValidFound=true; }
		}
	}

  sprintf(szTemp, "MAIL FROM: %s%c%c", pszFrom, 13,10);  
	nInternalReturnCode = m_net.SendLine(szTemp, strlen(szTemp), s1, EOLN_NONE, false);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szTemp, LOG_SENDERROR);
	else
	{
		Log(szTemp, LOG_SEND);
		if(psizeBytesWritten)	*psizeBytesWritten[1]+=(strlen(pszFrom)+13); 
	}

	nInternalReturnCode = m_net.GetLine(pch, &nLen, s1);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szTemp, LOG_RECVERROR);
	else
	{
		Log(pch, LOG_RECV);
		nInternalReturnCode = InterpretCode(pch);
		if(nInternalReturnCode==REPLY_FATAL)	{ nReturnCode=nInternalReturnCode; goto bad; }
	}

	strcpy(szTemp,"");

  for (i=0; i<(int)strlen(pszTo); i++)
  {
    if (pszTo[i] == ',')
    {
			nLen = strlen(szTemp);
			while((nLen>0)&&(isspace(szTemp[nLen-1]))) {szTemp[nLen-1]=0; nLen--;}  //trims right

			sprintf(szText, "RCPT TO: %s%c%c", (LPCTSTR) szTemp, 13,10);  

			if(strlen(szText)<=(MAXLEN_CMD-2)) // -2 for CRLF
			{
				if(psizeBytesWritten)	*psizeBytesWritten[0]+=(strlen(szTemp)+11); 
				nInternalReturnCode = m_net.SendLine(szText, strlen(szText), s1, EOLN_NONE, false);
				if (nInternalReturnCode<RETURN_SUCCESS) 
					Log(szText, LOG_SENDERROR);
				else
				{
					Log(szText, LOG_SEND);
					if(psizeBytesWritten)	*psizeBytesWritten[1]+=(strlen(szTemp)+11);
				}

				nInternalReturnCode = m_net.GetLine(pch, &nLen, s1);
				if (nInternalReturnCode<RETURN_SUCCESS) 
					Log(szText, LOG_RECVERROR);
				else
				{
					Log(pch, LOG_RECV);
					if(InterpretCode(pch)!=REPLY_FATAL)	{ nNumSuccessfulRCPT++; }
				}
			}
			else
			{
				sprintf(szText, "Invalid recipient: %s\n", szTemp); 
				Log(szText, LOG_ERROR);
			}
			strcpy(szTemp,"");
    }
    else 
    {
      if (!isspace(pszTo[i])) sprintf(szTemp, "%s%c", szTemp, (char) pszTo[i]);
    }
  }

  if (szTemp != "")
  {
		while((nLen>0)&&(isspace(szTemp[nLen-1]))) {szTemp[nLen-1]=0; nLen--;}  //trims right
    sprintf(szText, "RCPT TO: %s%c%c", (LPCTSTR) szTemp, 13,10);  

		if(strlen(szText)<=MAXLEN_CMD)
		{
			if(psizeBytesWritten)	*psizeBytesWritten[0]+=(strlen(szTemp)+11); 
			nInternalReturnCode = m_net.SendLine(szText, strlen(szText), s1, EOLN_NONE, false);
			if (nInternalReturnCode<RETURN_SUCCESS) 
				Log(szText, LOG_SENDERROR);
			else
			{
				Log(szText, LOG_SEND);
				if(psizeBytesWritten)	*psizeBytesWritten[1]+=(strlen(szTemp)+11);
			}

			nInternalReturnCode = m_net.GetLine(pch, &nLen, s1);
			if (nInternalReturnCode<RETURN_SUCCESS) 
				Log(szText, LOG_RECVERROR);
			else
			{
				Log(pch, LOG_RECV);
				if(InterpretCode(pch)!=REPLY_FATAL)	{ nNumSuccessfulRCPT++; }
			}
		}
		else
		{
			sprintf(szText, "Invalid recipient: %s\n", szTemp); 
			Log(szText, LOG_ERROR);
		}
  }

	if(nNumSuccessfulRCPT<=0)
	{
		nReturnCode=ERROR_NORCPT; goto bad;
	}

  sprintf(szText, "DATA%c%c", 13,10);  
	//if(psizeBytesWritten)	*psizeBytesWritten[0]+=6; //constant, got it in init
	nInternalReturnCode = m_net.SendLine(szText, strlen(szText), s1, EOLN_NONE, false);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szText, LOG_SENDERROR);
	else
	{
		Log(szText, LOG_SEND);
		if(psizeBytesWritten)	*psizeBytesWritten[1]+=6; 
	}

  nInternalReturnCode = m_net.GetLine(pch, &nLen, s1);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szText, LOG_RECVERROR);
	else
	{
		Log(pch, LOG_RECV);
		nInternalReturnCode = InterpretCode(pch);
		if(nInternalReturnCode==REPLY_FATAL)	{ nReturnCode=nInternalReturnCode; goto bad; }
	}
  
  // send the message headers
  sprintf(szText, "To:  %s", pszTo);  	
	SendText(szText, s1, psizeBytesWritten);

  sprintf(szText, "From:  %s", pszFrom); 
	SendText(szText, s1, psizeBytesWritten);

  sprintf(szText, "Subject: %s", pszSubject); 
	SendText(szText, s1, psizeBytesWritten);

  if (strlen(pszReplyTo)>0)
  {
    sprintf(szText, "Reply-To: %s", pszReplyTo);  
		SendText(szText, s1, psizeBytesWritten);
  }
  if (strlen(pszAddlHeaders)>0)
  {
		nLen = strlen(pszAddlHeaders);
		while((nLen>0)&&(isspace(pszAddlHeaders[nLen-1]))) {pszAddlHeaders[nLen-1]=0; nLen--;}  //trims right
		SendText(pszAddlHeaders, s1, psizeBytesWritten);
  }

	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
  {
    SendText("MIME-Version: 1.0", s1, psizeBytesWritten);	          // =19 char w/CRLF
		pszDomain = m_bu.Base64Boundary();

		if(pszDomain==NULL) {nReturnCode=ERROR_BUFFER; goto bad;}// hmmm  no boundary...
		sprintf(szText, "Content-Type: multipart/mixed; boundary=\"%s\"", pszDomain);	// =73 char w/CRLF
    SendText(szText, s1, psizeBytesWritten);													//  92 total
		sprintf(pszDomain, "--%s", pszDomain); // add preceding two dashes
  }
	else	strcpy(pszDomain,"");

	// add 3 chars to to init
	nInternalReturnCode = m_net.SendLine("\n", 1, s1, EOLN_CRLF, false);

  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log("<LF><CR><LF>", LOG_SENDERROR);
	else
	{
		Log("<LF><CR><LF>", LOG_SEND);
		if(psizeBytesWritten)	*psizeBytesWritten[1]+=3; 
	}

	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
	{
		// make this a multipart.
		SendText("This is a multi-part message in MIME format.", s1, psizeBytesWritten);// =46 char w/CRLF
		SendText("", s1, psizeBytesWritten);																						// = 2 char w/CRLF
		SendText(pszDomain, s1, psizeBytesWritten);																			// =33 char w/CRLF
		SendText("Content-Type: text/plain; charset=us-ascii", s1, psizeBytesWritten);	// =44 char w/CRLF
		SendText("Content-Disposition: inline", s1, psizeBytesWritten);									// =29 char w/CRLF
		SendText("Content-Transfer-Encoding: 7bit", s1, psizeBytesWritten);							// =33 char w/CRLF
		SendText("", s1, psizeBytesWritten);																						// = 2 char w/CRLF
																																										// 189 total
	}

  // body
  if (bUseFile)
  {
    FILE *fp;
    fp = fopen(pszData, "rt");
		strcpy(szText,"");
    if (fp != NULL)
    {
			unsigned long ulTotalDataLength = _filelength( _fileno( fp));
			pch = (char*) malloc(ulTotalDataLength);
			if (pch==NULL) {nReturnCode=ERROR_BUFFER; goto bad;}
			if(psizeBytesWritten)	*psizeBytesWritten[0]+=ulTotalDataLength+2;// final CRLF 
			fread(pch, sizeof(char), ulTotalDataLength, fp);
			fclose(fp);
			SendText(pch, s1, psizeBytesWritten);
			free(pch);
    }
  }
  else
  {
		SendText(pszData, s1, psizeBytesWritten);
  }

	//if(psizeBytesWritten)	*psizeBytesWritten[0]+=4; //CRLFCRLF  //constant, got it in init
  SendText("", s1, psizeBytesWritten);
  SendText("", s1, psizeBytesWritten);

//  now the multipart attachments if any
	if((nNumAttach>0)&&(pszAttachFiles!=NULL))
	{
		SendText(pszDomain, s1, psizeBytesWritten); // 33 chars
		for(i=0;i<nNumAttach;i++)
		{
			if(!SendAttachment(pszAttachFiles[i], pszEncodedAttach[i], pszDomain, s1, psizeBytesWritten))
			{
				sprintf(szTemp, "Send Attachment %s error", pszAttachFiles[i]);
				Log(szTemp, LOG_SENDERROR);
			}

			free(pszEncodedAttach[i]); //release the buffer
		}
	}

  // end of data
	nInternalReturnCode = m_net.SendLine(".", 1, s1, EOLN_CRLF, false);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(".<CR><LF>", LOG_SENDERROR);
	else
	{
		Log(".<CR><LF>", LOG_SEND);
		if(psizeBytesWritten)	*psizeBytesWritten[1]+=3; 
	}

  nInternalReturnCode = m_net.GetLine(pch, &nLen, s1);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log("End data", LOG_RECVERROR);
	else
	{
		Log(pch, LOG_RECV);
		InterpretCode(pch);
	}
  
bad:
  sprintf(szTemp, "QUIT%c%c", 13,10);  
	nInternalReturnCode = m_net.SendLine(szTemp, 6, s1, EOLN_NONE, false);
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szTemp, LOG_SENDERROR);
	else
	{
		Log(szTemp, LOG_SEND);
		if(psizeBytesWritten)	*psizeBytesWritten[1]+=6; 
	}

// the following is commented out because some servers just cut the channel
// without responding with a 221, causing us to timeout waiting for a reply.
// we dont need a reply, we can just close the connection, because we are done.
/*
  nInternalReturnCode = m_net.GetLineFromSocket(temp, s1, &szTemp); 
  if (nInternalReturnCode<RETURN_SUCCESS) 
		Log(szTemp, LOG_RECVERROR, pszInfo);
	else
	{
		Log(temp, LOG_RECV);
		InterpretCode(temp, pszInfo);
	}
*/
  // close the connection to the mail server
	nInternalReturnCode = m_net.CloseConnection(s1);
  if (nInternalReturnCode<RETURN_SUCCESS) 
  { 
		Log("CloseConnection" , LOG_ERROR);
  }
	else
	{
		Log("CloseConnection", LOG_CLOSE);
	}

	if(pszFromRemaining) free(pszFromRemaining);
	if(pszDomain) free(pszDomain);

	m_bFileBusy = false; // allows changing the logfile when finished writing out a messagelog
  return nReturnCode;
}


bool CSendMail::SendText(char* pszText, SOCKET s, unsigned long* psizeBytesWritten[])
{
	if(s==NULL) 
	{
		Log("Null socket.", LOG_SENDERROR);
		return false;
	}
	bool bReturnCode=true;
	char line[MAXLEN_TEXT+2];
	int i=0, nLen=0, nLineLen=0;
	char* pch;

	if(strlen(pszText)<=0) // then just send CRLF
	{
		sprintf(line, "%c%c",13,10); // CRLF
		
		i = m_net.SendLine(line, 2, s, EOLN_NONE, false);
		if (i<RETURN_SUCCESS) 
		{
			Log(line, LOG_SENDERROR);
			return false;
		}
		else
		{
			Log(line, LOG_SEND);
			if(psizeBytesWritten)	*psizeBytesWritten[1]+=2; 
			return true;
		}
	}

	while(strlen(pszText)>0)
	{
		sprintf(line, "%c%c",13,10); // CRLF
		pch = strstr(pszText, line);
		if(pch!=NULL)
		{
			if((MAXLEN_TEXT-2)==((pszText-pch)+1)) // special case, breaks CRLF
				nLen = (pszText-pch);
			else
				nLen = min((pszText-pch)+2,MAXLEN_TEXT-2);
		}
		else nLen = MAXLEN_TEXT-2;

		if(pszText[0]=='.')
		{

// RFC 821
//      4.5.2.  TRANSPARENCY
//         Without some provision for data transparency the character
//         sequence "<CRLF>.<CRLF>" ends the mail text and cannot be sent
//         by the user.  In general, users are not aware of such
//         "forbidden" sequences.  To allow all user composed text to be
//         transmitted transparently the following procedures are used.
//
//            1. Before sending a line of mail text the sender-SMTP checks
//            the first character of the line.  If it is a period, one
//            additional period is inserted at the beginning of the line.

			sprintf(line, ".");
			memcpy(line+1, pszText, nLen);
			line[nLen+1]=0; //null term
			
			nLineLen=strlen(line);

			if(psizeBytesWritten) *psizeBytesWritten[0]++; // for the extra dot
		}
		else
		{
			memcpy(line, pszText, nLen);
			line[nLen]=0; //null term
			nLineLen=strlen(line);
		}

		i=strlen(pszText);
		if(i>nLen)
		{
			memcpy(pszText, pszText+nLen, i-nLen+1);  //+1 for null term

			i = m_net.SendLine(line, nLineLen, s, EOLN_NONE, false);
			if (i<RETURN_SUCCESS) 
			{
				Log(line, LOG_SENDERROR);
				return false;
			}
			else
			{
				Log(line);
				if(psizeBytesWritten)	*psizeBytesWritten[1]+=nLineLen; 
			}
		}
		else
		{
			strcpy(pszText,""); // escapes while
			i = m_net.SendLine(line, nLineLen, s, EOLN_CRLF, false);
			if (i<RETURN_SUCCESS) 
			{
				Log(line, LOG_SENDERROR);
				return false;
			}
			else
			{
				Log(line, LOG_SEND);
				if(psizeBytesWritten)	*psizeBytesWritten[1]+=(nLineLen+2); 
			}
		}
	}
	return bReturnCode;
}

bool CSendMail::EncodeAttachment(char* pszFileName, char* pszEncodedData, unsigned long* pulTotalBytes)
{
	char* pszText = (char*)malloc(strlen(pszFileName)+32); //extra in case need to use for error
	if((pszEncodedData==NULL)||(pszText==NULL)) return false;

	sprintf(pszText, "%s", pszFileName);
	if(!m_bu.IsFileString(pszFileName, true, true)) 
	{
		sprintf(pszText, "Invalid attachment filename \"%s\"",pszFileName);
		Log(pszText, LOG_SENDERROR);
		free(pszText);
		return false;
	}

	int nReturn;
	unsigned long ulBytes;
	nReturn = m_bu.Base64EncodeFromFile(pszText, &ulBytes, true);  //true frees the input buffer with filename
	if(nReturn>=RETURN_SUCCESS)
	{
		pszEncodedData = pszText;
		if(pulTotalBytes) 
		{
			*pulTotalBytes=( (ulBytes/(MAXLEN_ATTACHLINE-2))*MAXLEN_ATTACHLINE )
				+ ( (ulBytes%(MAXLEN_ATTACHLINE-2))?(ulBytes%(MAXLEN_ATTACHLINE-2)+2):0);

			char szPath[MAX_PATH];
			char szFile[MAX_PATH];
			char szExt[MAX_PATH];
			strcpy(szPath,"");
			strcpy(szFile,"");
			strcpy(szExt,"");

			m_bu.BreakFileString(pszFileName, szPath, szFile, szExt);
			if(strlen(szExt)<=0) // now we just check for existence, later we will add lookups for registered types
			{
				sprintf(szPath, "Content-Type: application/octet-stream; name=\"%s\"", szFile);
			}
			else
			{
				sprintf(szPath, "Content-Type: application/octet-stream; name=\"%s.%s\"", szFile, szExt);
			}
			*pulTotalBytes+=strlen(szPath)+107;
			//CRLF + len("Content-Disposition: attachment")=31 +CRLF 
				    //+len("Content-Transfer-Encoding: base64")=33 +CRLF+CRLF+ (omit body) +CRLF +len(Boundary)=31+CRLF
//			szPath.Format("%d bytes expected", *pnTotalBytes);
//	 	  AfxMessageBox(szPath);
		}
	}
	else
	{
		strcpy(pszEncodedData,"");
		sprintf(pszText, "Encoding error, code %d", nReturn );
		if(pulTotalBytes) *pulTotalBytes=0;
		Log(pszText, LOG_ERROR);
		free(pszText);
		return false; // encode error
	}
	return true;
}

bool CSendMail::SendAttachment(char* pszFileName, char* pszEncodedData, char* pszBoundary, SOCKET s, unsigned long* psizeBytesWritten[])
{
	char szText[128];
	bool bReturn = true;
	if(strlen(pszBoundary)<=0)
	{
		sprintf(szText, "Invalid boundary string \"%s\"",pszBoundary);
		Log(szText, LOG_SENDERROR);
		return false;
	}
	if(s==NULL) 
	{
		Log("Null socket.", LOG_SENDERROR);
		return false;
	}

//	int n=*psizeBytesWritten[1];

	if(strlen(pszEncodedData)>0)
	{
//AfxMessageBox(szFileName);
		// OK, we managed to encode the file, so we have to put in part headers now;
		char line[MAXLEN_ATTACHLINE+1];  //null term if nec
		unsigned long nLen=0;
		char szPath[MAX_PATH];
		char szFile[MAX_PATH];
		char szExt[MAX_PATH];
		strcpy(szPath,"");
		strcpy(szFile,"");
		strcpy(szExt,"");

		m_bu.BreakFileString(pszFileName, szPath, szFile, szExt);
		if(strlen(szExt)<=0) // now we just check for existence, later we will add lookups for registered types
		{
			sprintf(szPath, "Content-Type: application/octet-stream; name=\"%s\"", szFile);
		}
		else
		{
			sprintf(szPath, "Content-Type: application/octet-stream; name=\"%s.%s\"", szFile, szExt);
		}

		if(!SendText(szPath, s, psizeBytesWritten)) return false;
		if(!SendText("Content-Disposition: attachment", s, psizeBytesWritten)) { bReturn = false; goto boundary; }
		if(!SendText("Content-Transfer-Encoding: base64", s, psizeBytesWritten)) { bReturn = false; goto boundary; }
		SendText("", s, psizeBytesWritten);

		sprintf(szPath, "Sending base64 encoded data for \"%s\"", pszFileName);
		Log(szPath, LOG_SEND);

		while(strlen(pszEncodedData)>0)
		{
			memcpy(line, pszEncodedData, MAXLEN_ATTACHLINE-2);
			sprintf(line, "%s%c%c", line,13,10);
			nLen=strlen(pszEncodedData);
			if(nLen>(MAXLEN_ATTACHLINE-2))
			{
				memcpy(pszEncodedData, pszEncodedData+(MAXLEN_ATTACHLINE-2), nLen-(MAXLEN_ATTACHLINE-2));
				pszEncodedData[nLen-(MAXLEN_ATTACHLINE-2)]=0; //null term
			}
			else
			{
				strcpy(pszEncodedData,"");
			}
			
			nLen = m_net.SendLine(line, strlen(line), s, EOLN_NONE, false);
			if (nLen<RETURN_SUCCESS) 
			{
				Log("Attachment send error", LOG_SENDERROR);
				{ bReturn = false; goto boundary; }
			}
			else
			{
// not logging all the attachment source, way too long.
//				Log(line, LOG_SEND);
				if(psizeBytesWritten)	*psizeBytesWritten[1]+=(strlen(line)); 
			}
		}

		if(m_net.SendLine("", 0, s, EOLN_CRLF, false)<RETURN_SUCCESS)
		{
			Log("<CR><LF>", LOG_SENDERROR);
			{ bReturn = false; goto boundary; }
		}
		else
		{
			Log("<CR><LF>", LOG_SEND);
			if(psizeBytesWritten)	*psizeBytesWritten[1]+=2; 
		}
boundary:
		sprintf(line, "%s%c%c", pszBoundary,13,10);
		nLen = m_net.SendLine(line, strlen(line), s, EOLN_NONE, false);
		if (nLen<RETURN_SUCCESS) 
		{
			Log(line, LOG_SENDERROR);
			return false;
		}
		else
		{
			Log(line, LOG_SEND);
			if(psizeBytesWritten)	*psizeBytesWritten[1]+=(strlen(line));  
		}
	}
	else
	{
		sprintf(szText, "Encoded attachment had zero length.");
		Log(szText, LOG_ERROR);
		return false; // encode error
	}
//	szText.Format("%d bytes sent", *psizeBytesWritten[1]-n);
//	AfxMessageBox(szText);

	return bReturn;
}


void CSendMail::Log(char* pszText, int nType)
{
	char* pszError = (char*) malloc(strlen(pszText)+50);
	if((m_fp != NULL)&&(m_bLogActivity))  
	{
		struct _timeb timebuffer;
		_ftime( &timebuffer );
		sprintf(pszError, "%s.%03d", ctime(&timebuffer.time), timebuffer.millitm);
		
		switch(nType)
		{
		case LOG_SEND://  3
			{
				sprintf(pszError, "%s SND: ", pszError);
			} break;
		case LOG_RECV://  4
			{
				sprintf(pszError, "%s REC: ", pszError);
			} break;
		case LOG_CLOSE://  5
			{
				sprintf(pszError, "%s END: ", pszError);
			} break;
		case LOG_ENCODE://  6
			{
				sprintf(pszError, "%s ENC: ", pszError);
			} break;
		case LOG_SENDERROR:// 1
			{
				sprintf(pszError, "%s ERR(TX): ", pszError);
			} break;
		case LOG_RECVERROR:// 2
			{
				sprintf(pszError, "%s ERR(RX): ", pszError);
			} break;
		case LOG_ERROR:// 0
		default:
			{
				sprintf(pszError, "%s ERR: ", pszError);
			} break;
		}

		int i=0, nLen=strlen(pszText);
		char ch;
		while(i<nLen)
		{
			ch = pszText[i];
			if(ch==10)
			{
				if(nType==LOG_CLOSE)
					strcat(pszError, "%s\nEND: "); // dont replace CRLFs
				else
					strcat(pszError,"<LF>");
			}
			else
			if(ch==13)
			{
				strcat(pszError,"<CR>");
			}
			else
				strcat(pszError, &ch);
			i++;
		}
		strcat(pszError,"\n");
		if(nType==LOG_CLOSE) strcat(pszError,"\n"); // some space between emails.
		fprintf(m_fp, pszError);
		fflush(m_fp);
	}
}

int CSendMail::InterpretCode(char* pszText)
{
	/*
August 1982                                                      RFC 821
Simple Mail Transfer Protocol                                           

4.2.2.  NUMERIC ORDER LIST OF REPLY CODES

   211 System status, or system help reply
   214 Help message
      [Information on how to use the receiver or the meaning of a
      particular non-standard command; this reply is useful only
      to the human user]
   220 <domain> Service ready
   221 <domain> Service closing transmission channel
   250 Requested mail action okay, completed
   251 User not local; will forward to <forward-path>

   354 Start mail input; end with <CRLF>.<CRLF>

   421 <domain> Service not available,
       closing transmission channel
      [This may be a reply to any command if the service knows it
      must shut down]
   450 Requested mail action not taken: mailbox unavailable
      [E.g., mailbox busy]
   451 Requested action aborted: local error in processing
   452 Requested action not taken: insufficient system storage

   500 Syntax error, command unrecognized
      [This may include errors such as command line too long]
   501 Syntax error in szDataeters or arguments
   502 Command not implemented
   503 Bad sequence of commands
   504 Command szDataeter not implemented
   550 Requested action not taken: mailbox unavailable
      [E.g., mailbox not found, no access]
   551 User not local; please try <forward-path>
   552 Requested mail action aborted: exceeded storage allocation
   553 Requested action not taken: mailbox name not allowed
      [E.g., mailbox syntax incorrect]
   554 Transaction failed
*/

  int nCode = atol(pszText);  
	if ((nCode >399)||(nCode ==221)) 
	{ 
		char szTemp[MAXLEN_TEXT];
		sprintf(szTemp, "Mail server error: %s\n",pszText); 
		if((m_fp != NULL)&&(m_bLogActivity))  
		{
			fprintf(m_fp, szTemp);
			fflush(m_fp);
		}
		return REPLY_FATAL;
	}
	else
	if(nCode < 221) 
	{
		return REPLY_SVCREADY;
	}
	return REPLY_CMDOK;
}

bool CSendMail::ChangeLogFilename(char* pszLogFile)
{
	if(strcmp(m_szLogFile, pszLogFile)==0) return true; // already done.
	if(strlen(pszLogFile)<=0) return false;
	if(m_bFileBusy) return false; // prevents changing the logfile while writing out a messagelog
	if(m_fp)
	{
		fclose(m_fp);
	}
	m_fp = NULL;
	strcpy(m_szLogFile, pszLogFile);
	// no need to open a new file, the next send will open the new file
	return true;
}

bool CSendMail::ChangeLogFileUsage(bool bLogActivity)
{
	if(m_bLogActivity==bLogActivity) return true; // already done.
	if(m_bFileBusy) return false; // prevents changing the logfile while writing out a messagelog
	if(m_fp) // should be null if we were changing from no logging to logging
	{
		fclose(m_fp);
	}
	m_fp = NULL;
	m_bLogActivity=bLogActivity;
	// no need to open a new file, the next send will open the new file
	return true;
}

